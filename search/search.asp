<%
PageTitle = "Search"
%>

<!--#include virtual="/assets/includes/header.asp" -->

<style>
	.RangeIntro {
		border-top:0px solid orange;
		border-bottom:0px solid orange;
		xpadding:10px;
		border-radius:5px;
		margin-left:20px;
		padding:10px 10px 5px 10px;
		background:#444;
		color:white;
		font-size:9pt;
	}
	
	A.EndUseLink_ACTIVE {
		color:orange !important;
	}
	
	A.ProdCatLink_ACTIVE {
		color:orange !important;
	}	
	
	A.EndUseLink {
				
	}		
	
	A.ProdCatLink {
				
	}	
	
	.VariantTile {
	
		cursor:pointer;
		margin:0px;
		border:1px solid #333;
		border-radius:0px;
		display:inline-block;	
	
	}
	
	.VariantTile, .VariantTileImage {
		height:5px;
		width:20px;
	}
	
	@media (max-width: 960px) { 
		.VariantTile {
			border:1px solid #333;
			border-radius:2px;
		}
		.VariantTile, .VariantTileImage {
			height:20px;
			width:20px;
		}
		
	}		
	
	.SelectedCollection {
		color:white;
		font-weight:bold;
	}
	
</style>

<%
public colorBarArr, colorBarC

PageMax = 100



SearchVal = request("SearchVal")

%>

    <div id="aboutus">
		
        <div class="container">
		
		        
            <div class="row" style="min-height:400px;">
			
                <div class="section_header">
                    <h3><%=lne(PageTitle)%></h3>
                </div>			
			
				<p><%=lne("search_page_intro")%></p>
								
				<form name="SearchForm" id="SearchForm" method=post action="search.asp">
				<input name=SearchVal value="<%=SearchVal%>"> <input type=submit value="Search" class="btn btn-warning"> 
				</form>
				<%
				'jh "SearchVal = " & SearchVal
				
			
				
				if SearchVal <> "" then
					'jhInfo "BEGIN"
					
					sql = "exec [spSMLProdResults_SEARCH] 0" & EndUseID & ", '" & validstr(SearchVal) & "', 0" & SelectedLanguageID & ", '" & SelectedLocation & "'"
					'end if
					'jh sql
					x = getrs(sql, ProdArr, ProdC)
					'jh "ProdC = " & ProdC
					
					if ProdC >= 0 then
									
						HasMain = hasSearchResults("MAIN", 11) 
						HasShop = hasSearchResults("SHOP", 12) 
						HasCustom = hasSearchResults("CUSTOM", 13) 
							
						'jh "HasMain = " & HasMain
					'	jh "HasShop = " & HasShop
						'jh "HasCustom = " & HasCustom

						'jhStop
						
						if HasMain > 0 then
							SomeFound = true
						end if
						
						if HasShop > 0 then
							SomeFound = true
						end if
						
						if HasCustom > 0 then
							SomeFound = true
						end if
					
					else
						HasMain = 0
						HasShop = 0
						HasCustom = 0
					end if
					
				end if
				
				'jh "RESULTS HERE!"
				
				if SomeFound or 1 = 1 then
				
					if HasMain > 0 or SomeFound = false then
						TabActive_MAIN = "active"						
					elseif HasShop > 0 then
						TabActive_SHOP = "active"						
					elseif HasCustom > 0 then
						TabActive_CUSTOM = "active"					
					end if	
					
					if HasMain > 0 then ResultCount_MAIN = "badge-success"		
					if HasShop > 0 then ResultCount_SHOP = "badge-success"		
					if HasCustom > 0 then ResultCount_CUSTOM = "badge-success"		
											
				%>
				
				
				<ul class="nav nav-tabs" style="margin-bottom:0px !important;font-size:9pt;">
					<li class="<%=TabActive_MAIN%>"><a href="#TabMain" data-toggle="tab"><%=lne("search_tab_main_range")%> <span title="" class="badge <%=ResultCount_MAIN%>"><%=HasMain%></span></a></li>
					<li class="<%=TabActive_SHOP%>"><a href="#TabMerch" data-toggle="tab"><%=lne("search_tab_merchandise")%> <span title="" class="badge <%=ResultCount_SHOP%>"><%=HasShop%></span></a></li>				
					<li class="<%=TabActive_CUSTOM%>"><a href="#TabCustom" data-toggle="tab"><%=lne("search_tab_custom")%> <span title="" class="badge <%=ResultCount_CUSTOM%>"><%=HasCustom%></span></a></li>
				</ul>		

				<div class="tab-content" style="background:white;margin-top:0px;xpadding:10px;border-bottom:1px solid #ddd;border-left:1px solid #ddd;min-height:210px;">
						<div class="tab-pane <%=TabActive_MAIN%>" id="TabMain" style="padding-left:10px;">
							<div class=spacer20></div>
							
<%
							if HasMain > 0 then
								MainResults = getSearchResults("MAIN", 11) 'B2C Prods, column 11 for status
							else
								%>
									<p><a class="OrangeLink" href="/product-listing.asp"> <i class="icon-search"></i> <%=lne("search_tab_main_browse")%></a></p>
								<%
							end if
%>						
						</div>
						<div class="tab-pane <%=TabActive_SHOP%>" id="TabMerch" style="padding-left:10px;">
							<div class=spacer20></div>
							
<%
							if HasShop > 0 then
								ShopResults = getSearchResults("SHOP", 12) 'B2C shop prods, column 12 for status
							else
								%>
									<p><a class="OrangeLink" href="/shop/"> <i class="icon-search"></i> <%=lne("search_tab_shop_browse")%></a></p>
								<%
							end if
%>
						</div>						
						<div class="tab-pane <%=TabActive_CUSTOM%>" id="TabCustom" style="padding-left:10px;">
							<div class=spacer20></div>
<%
							if HasCustom > 0 then						
								CustomResults = getSearchResults("CUSTOM", 13) 'B2C Prods, column 11 for status
							else
								%>
									<p><a class="OrangeLink" href="/custom/products/custom-product-listing.asp"> <i class="icon-search"></i> <%=lne("search_tab_custom_browse")%></a></p>
								<%
							end if
%>
						</div>	
				</div>			
					
<%
			end if
%>					
				
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				
            </div>
        </div>
		
		
    </div>


<!--#include virtual="/assets/includes/footer.asp" -->


<script>


function swapBrowserImage(pid, pType, psrc) {
	//alert('swapBrowserImage');
	document.getElementById('ProdBrowserThumb_' + pType + '_' + pid).src = psrc;
}



</script>


<%
	
				
				function hasSearchResults(pType, pCol)
					
					hasSearchResults = 0
					for c = 0 to ProdC 
						if ProdArr(pCol, c) > 0 then
							hasSearchResults = hasSearchResults + 1
						end if
												
					next
				
					if hasSearchResults > PageMax then hasSearchResults = PageMax
				
				end function
				
				function getSearchResults(pType, pCol)
					
					dim c , RowStarted 
					
					StartPoint = 0
					
					RowMax = 4
					
					StatusCol = 11
					
					if pType = "MAIN" then 
						ResultsLabel = "Main Endura Range"
						StatusCol = 11
					elseif pType = "SHOP" then 
						ResultsLabel = "Merchandise/shop"
						StatusCol = 12
					elseif pType = "CUSTOM" then 
						ResultsLabel = "Custom Printed Products"
						StatusCol = 13
					end if
										
					RowPos = 0
					DisplayCount = 0
					
					for c = 0 to ProdC 
						
						if c >= StartPoint then Started = true
						
						if not Started then
						
						else
						
							if ProdArr(pCol, c) > 0 then
							
								DisplayCount = DisplayCount + 1
								RowPos = RowPos + 1 
								
								ThisProdID = ProdArr(0,c)
								ThisProdCode = ProdArr(1,c)
								ThisProdName = ProdArr(2,c)
								This1Liner = ProdArr(3,c)
								ThisShortDesc = ProdArr(4,c)
								
								if SelectedLanguageID <> "0" then
									'jhInfo "TRANSLATE!"
									if "" & ProdArr(6,c) <> "" then ThisProdName = ProdArr(6,c)
									if "" & ProdArr(7,c) <> "" then This1Liner = ProdArr(7,c)
									if "" & ProdArr(8,c) <> "" then ThisShortDesc = ProdArr(8,c)
								end if
								
								ThisShortDesc = replace(ThisShortDesc, "?", "&trade;" )
								ThisShortDesc = replace(ThisShortDesc, "�", "&reg;" )
								DispShortDesc = ThisShortDesc
								if ucase("" & DispShortDesc) = "MISSING" then DispShortDesc = This1Liner '& " <span style=""color:red;"">*</span>"
								
								

								if RowPos = 1 then
									RowStarted = true
								%>
									<!-- Product listing Row -->
									<div class="row spacer40" >
								<%	
								end if
								
								%>
									<!-- PRODUCT -->
									<div class="span3 feature" style="text-align:center;">
										<!--background:url(/assets/images/prodbrowser/prod_img_border.png);background-repeat:no-repeat;background-position:top right;-->
										<div class="img" style="">
										
										<%
											ThisDefImage = ProdArr(5,c)
											'jh "ThisDefImage = " & ThisDefImage
											ThisThumb = getImageProdThumb(ThisProdCode, ThisDefImage)
											'jh "ThisThumb = " & ThisThumb
											if ThisThumb = "" then ThisThumb = "/assets/images/prodbrowser/product_placeholder.jpg"			
											
											sql = "exec spSMLProdBrowser_COLORS 0" & ThisProdID & ", '" & SelectedLocation & "'"
											'jhAdmin sql
										
											vc = getrs(sql, VariantArr, VariantC)
											
											if pType = "MAIN" then
												linkURL = "/product-detail.asp?ProductID=" & ThisProdID & ""
											elseif pType = "SHOP" then
												linkURL = "/shop/?SelProductID=" & ThisProdID & "&FromSearch=1"
											elseif pType = "CUSTOM" then
												linkURL = "/custom/products/custom-product-detail.asp?ProductID=" & ThisProdID & ""
											end if
											
											'jh sql
										%>
										
											<a href="<%=linkURL%>"><img id="ProdBrowserThumb_<%=pType%>_<%=DisplayCount%>" style="height:150px;" src="<%=ThisThumb%>" /></a>
										</div>
										<div class="text">
											<div class="spacer10"></div>
											<%
											if VariantC >= 0 then
												for y = 0 to VariantC
													ThisVariant = VariantArr(0,y)
													ThisColor = "" & VariantArr(2,y)
													if ThisColor = "" then ThisColor = VariantArr(3,y)
													AltImage = getImageProdThumb(ThisProdCode, ThisVariant)
													ThisColorName = "" & VariantArr(3,y)
													ThisColorCode = "" & VariantArr(4,y)
													'jh "ThisColorCode= " & ThisColorCode
													
													'if AltImage <> "" then jh AltImage
													ThisColorCode = altColorBar(ThisColorCode, ThisVariant, OverrideColorName)
													if OverrideColorName <> "" then ThisColorName = OverrideColorName
											%>
													<div class=VariantTile onclick="swapBrowserImage(<%=DisplayCount%>, '<%=pType%>', '<%=AltImage%>')" style="">
														<img class=VariantTileImage style="" title="<%=ThisVariant & ", " & ThisColorName%>" src="/assets/images/prodbrowser/colorbars/<%=ThisColorCode%>.png">
													</div>
											<%	
												next
											end if
											%>
												
											<div style="padding-top:5px;"><strong><a class="ProductDetailLink" href="<%=LinkURL%>"><%=ThisProdName%></a> </strong></div>
											
											<p style="fxont-family:arial;font-size:10pt;">
												<%=DispShortDesc%>
											</p>
											
										
										</div>
									</div>
								<%		
								
								if RowPos = RowMax or c = ProdC or DisplayCount = PageMax then
									RowPos = 0
									'jh "ENDING ROW @ " & ThisProdName
								%>
										</div>
								<%	
									RowStarted = false
									if DisplayCount = PageMax then exit for
								end if
							end if
						end if
					next					
				 
					if RowStarted then
					%>
						</div>
					<%
					end if 
				
				end function

function altColorBar(byval pColorCode, byval pVariant, byref rOverRideColorName )
	dim x
	if not isarray(ColorBarArr) and colorBarC <> -1 then
		sql = "SELECT ColorOverrideID, Variantcode, ColorBarCode, ColorOverrideName FROM B2C_ColorOverrides "
		'jh sql
		x = getrs(sql, colorBarArr, colorBarC)
	end if
		
	altColorBar = pColorCode
	OverrideColorName = ""
	
	for x = 0 to colorBarC
		if "" & pVariant = "" & colorBarArr(1,x) then
			altColorBar = colorBarArr(2,x)
			rOverRideColorName = colorBarArr(3,x)
			exit for
		end if
	next
	
	if altColorBar = "" then altColorBar = "BK"

end function				
%>