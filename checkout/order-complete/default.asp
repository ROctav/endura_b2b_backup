<%
PageTitle = "Order Complete"

MaxInstructionLength = 50
%>

<!--#include virtual="/assets/includes/header.asp" -->

<style>
	.HomePanelLayer {
		color:white;
	}
	.header-panel {		
		color:white;
		
	}
	
	A.whitelink {
		color:white;
	}
	
	.header-title {		
		color:white;
		font-size:36pt;
		font-family:Oxygen, arial;
		text-align:left;
	}	
	
	.header-trans {
		background-image:url(/assets/images/home_panels/semi-trans.png);
		padding:20px 0 20px 0;
		xmargin-top:20px;
		
	}
	
	.header-button {
		text-align:left;
	}
	
	#slide1, #slide2, #slide3, #slide4 {
		border:0px solid red;
		padding-top:50px;
		height:200px;
	}
	
	#feature_slider {
		height:250px !important;
	}
	
	.slide {
		height:220px;
	}
	
	.header-title {
		line-height:30pt;font-size:24pt;
	}
	
	.colSpacer_LEFT {
		border-left:1px solid #f0f0f0;
		padding-left:20px;
	}
	
	.ProductGrid TD {
		font-family:arial;
		font-size:10pt;
		padding:5px;
	}
	
	.legible {
		font-family:arial;
		font-size:8pt;
	}
	
	.basketAddButton {
		border:1px solid red;
	}
	
	#CheckoutTable {
		border:1px solid silver;
	}
	
	#CheckoutTable TD {
		font-family:arial;
		font-size:9pt;
		padding:7px;
	}
	
	.CheckoutImage {
		width:50px;
	}
	
	.CheckoutQty {
		width:40px;
		text-align:right;
	}
	
	
	#OrderConfirmationTable TD, TH {
		padding:10px;
	}
	
	#OrderContentsTable TD, TH {
		padding:10px;
		
	}
	
</style>

<%

LoadOrder = cleannum(request("LoadOrder"))

'jh "OrderThreshold = " & OrderThreshold

%>

<link rel="stylesheet" href="css/index.css" type="text/css" media="screen" />

	<div class="container">      
		
<%

dbConnect()


blnIsDEConsumer =  false

If AcDetails.VATCode = "7" Then
	blnIsDEConsumer =  true
End If

    sql = "exec [spB2BOrderDetails] '" & acDetails.AcCode & "', '" & AcDetails.CostBand  & "', 0" & LoadOrder

x = getrs(sql, OrderArr, OrderC)


if OrderC = -1 then
	jhErrorNice "Could not find order", "Sorry there was a problem retrieving your order details - if you have just submitted an order, please contact us for further information"
else

		OrderFound = true
	
	
		OrderRef = OrderArr(1,0)
		CustomerRef = OrderArr(2,0)
		
		OrderDate = OrderArr(3,0)
		DeliveryDate = OrderArr(4,0)
		if isdate(DeliveryDate) then  DispDeliveryDate = formatdatetime(DeliveryDate, 2) else DispDeliveryDate = "asap"
		
			
		SpecialInstructions = "" & OrderArr(5,0)		
		if SpecialInstructions = "" then DispSpecialInstructions = "none..." else DispSpecialInstructions = SpecialInstructions
		
		DelSKU = OrderArr(6,0)
		
		XMLFileName = OrderArr(7,0)		
		XMLCreated = OrderArr(8,0)
		XMLJobSubmitted = OrderArr(9,0)
		
		ThisCostCentre = AcDetails.CostCentre
		CurrencyName = AcDetails.CurrencyName
		ExchequerCurrencyCode = AcDetails.ExchequerCurrencyCode
		
		
		NetTotal = OrderArr(11,0)		
		VATTotal = OrderArr(12,0)
        
        	DeliveryTotal = OrderArr(13,0)
				
		GrandTotal = OrderArr(14,0)		
		
		set ls = new strconcatstream
		
		ls.add "<table width=100% border=1 id=OrderContentsTable>"
		
		ls.add "<tr>"
		ls.add "<th style=""width:60px;""></th>"
		ls.add "<th>SKU</th>"
		ls.add "<th>NAME</th>"
		ls.add "<th>QTY</th>"
		ls.add "<th>EACH</th>"
		ls.add "<th>PRICE</th>"
		ls.add "</tr>"
		
			for c = 0 to OrderC
			
				ThisCode = OrderArr(17,c)
				ThisName = OrderArr(18,c)
				ThisVariant = OrderArr(19,c)
				
				imgURL = getImageDefault(ThisCode, ThisVariant)
				
				ThisColor = OrderArr(20,c)
				ThisSize = OrderArr(24,c)			
			
				ThisSKU = OrderArr(21,c)
				ThisQty = OrderArr(22,c)

                		ThisPrice = OrderArr(30,c)  ''' STORED IN BASKET, THIS WAS THE PRICE AT THE MOMENT THEY SUBMITTED!
				
				if cleannum(ThisQty) <> "" and cleannum(ThisPrice) <> "" then
					LinePrice = cdbl(ThisQty) * cdbl(ThisPrice)
				else
					LinePrice = "?"
				end if
				
				ThisSKUDesc = OrderArr(28,c)
				ThisStatusName = OrderArr(29,c)
					
				ls.add "<tr>"
				ls.add "<td class=text-center>"
				ls.add "<img style=""width:60px;"" class=""hidden-print ProdThumb60"" src=""" & ImgURL & """>"
				ls.add "</td>"				
				ls.add "<td>"
				ls.add ThisSKU
				ls.add "</td>"
				ls.add "<td>"
				ls.add ThisSKUDesc
				ls.add "</td>"	
				ls.add "<td class=text-right>"
				ls.add ThisQty
				ls.add "</td>"					
				ls.add "<td class=text-right >"
				ls.add DispCurrency(ThisPrice)
				ls.add "</td>"			
				ls.add "<td class=text-right >"
				ls.add DispCurrency(LinePrice)
				ls.add "</td>"							
				ls.add "</tr>"
			
			next
		
		
		ls.add "<tr>"
		ls.add "<td class=text-right colspan=5>NET</td>"
		ls.add "<td class=text-right colspan=1>" & DispCurrency(NetTotal) & "</td>"
		ls.add "</tr>"
    
    		
		ls.add "<tr>"
		ls.add "<td class=text-right colspan=5>DELIVERY</td>"
        
	        ls.add "<td class=text-right colspan=1>" & DispCurrency(DeliveryTotal) & "</td>"

        
		ls.add "</tr>"				
		
		if cleannum(VATTotal) <> "" then
			ls.add "<tr>"

            if AcDetails.RiderCard then
							if blnIsDEConsumer then
								ls.add "<td class=text-right colspan=5>" & lne("rider_card_de_vat") & glVATDERate & "%</td>"
							else
								ls.add "<td class=text-right colspan=5>" & lne("rider_card_vat") & glVATRate & "%</td>"
							end if 

            else
        

			if (AcDetails.VATCode = "S") Then
			        ls.add "<td class=text-right colspan=5>" & lne("vat_abbr") & " " & glVATRate & "%</td>"
			elseif (AcDetails.VATCode = "8") Then
			        ls.add "<td class=text-right colspan=5>" &  lne("vat_abbr") & " " & glVAT8Rate & "%</td>"
			else
			        ls.add "<td class=text-right colspan=5>" &  lne("vat_abbr") & " " & glVATRate & "%</td>"
			end if


            end if

			ls.add "<td class=text-right colspan=1>" & DispCurrency(VATTotal) & "</td>"
			ls.add "</tr>"				
		end if

		ls.add "<tr>"
		ls.add "<td class=text-right colspan=5>GRAND TOTAL</td>"
        
            ls.add "<td class=text-right colspan=1><strong>" & DispCurrency(GrandTotal) & "</strong></td>"

		ls.add "</tr>"		
		
		ls.add "</table>"
		
		OrderContents = ls.value
		
		set ls = nothing
		
	dbClose()
end if

if OrderFound then	

	DispCustomerRef = "YOUR_REF"

	ThisOrderDate = now
	
%>

		  <div class="row">      
			<div class="span12 spacer10">
			
				<h2>Order submitted!<br><small>Thank you - your order for <strong><%=ThisUserID%></strong> has been successfully placed for processing</small></h2>

				<hr>
			</div>
		</div>	
	<div class=row>
	
		<div class="span4">
				<H3 class=spacer20><small>OUR ORDER REF</small><br><%=OrderRef%></h3>				
				<H4 class=spacer20><small>YOUR REF</small><br><%=CustomerRef%></H4>
		</div>		
		
		<div class="span8 text-right">
				<H4 class=spacer20><small>ORDER PLACED</small><br><%=OrderDate%></H4>				
				<H4 class=spacer20><small>REQUIRED DELIVERY DATE</small><br><%=DispDeliveryDate%></H4>
				<%
				if DispSpecialInstructions <> "" then
				%>
					<H4 class=spacer20><small>SPECIAL INSTRUCTIONS</small><br><%=DispSpecialInstructions%></H4>				
				<%
				end if
				%>					
		</div>			

	</div>		
	
	<div class=row>
	
		<div class="span12">
				
				<div class=spacer40></div>
		
				<%
				=OrderContents
				%>
			
				<div class=spacer40></div>
				
				<h5>What happens next?</h5>
				<p>Your order will be placed in a queue for processing, you will receive an order confirmation email when this process has completed.</p>
				
					<div class=spacer40></div>
		
			
			
				
					<p>
						<a class=OrangeLink onclick="window.print()" href="#"><i class=icon-print></i> Print this summary</a>
					</p>					
					
					<div class=spacer20></div>
					<p>
						<a class=OrangeLink href="/"><i class=icon-chevron-left></i> Return to the homepage</a>
					</p>						
				
				<div class=Spacer10></div>
		
		</div>
	</div>

<%
end if
%>	
	</div>
	<div class=spacer40></div>			
			

<!--#include virtual="/assets/includes/footer.asp" -->

<%

dbClose()

%>

