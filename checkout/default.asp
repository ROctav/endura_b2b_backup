<%
PageTitle = "NEW B2B Checkout"

MaxInstructionLength = 50
%>

<!--#include virtual="/assets/includes/header.asp" -->


<!--#include virtual="/checkout/checkout_functions.asp" -->



<style>
	.HomePanelLayer {
		color:white;
	}
	.header-panel {		
		color:white;
		
	}
	
	A.whitelink {
		color:white;
	}
	
	.header-title {		
		color:white;
		font-size:36pt;
		font-family:Oxygen, arial;
		text-align:left;
	}	
	
	.header-trans {
		background-image:url(/assets/images/home_panels/semi-trans.png);
		padding:20px 0 20px 0;
		xmargin-top:20px;
		
	}
	
	.header-button {
		text-align:left;
	}
	
	#slide1, #slide2, #slide3, #slide4 {
		border:0px solid red;
		padding-top:50px;
		height:200px;
	}
	
	#feature_slider {
		height:250px !important;
	}
	
	.slide {
		height:220px;
	}
	
	.header-title {
		line-height:30pt;font-size:24pt;
	}
	
	.colSpacer_LEFT {
		border-left:1px solid #f0f0f0;
		padding-left:20px;
	}
	
	.ProductGrid TD {
		font-family:arial;
		font-size:10pt;
		padding:5px;
	}
	
	.legible {
		font-family:arial;
		font-size:8pt;
	}
	
	.basketAddButton {
		border:1px solid red;
	}
	
	#CheckoutTable {
		border:1px solid silver;
	}
	
	#CheckoutTable TD {
		font-family:arial;
		font-size:9pt;
		padding:7px;
	}
	
	.CheckoutImage {
		width:50px;
	}
	
	.CheckoutQty {
		width:40px;
		text-align:right;
	}
	
	#OrderProcessMessage {	
		display: none;
	}	
	
</style>

<%

blnIsDEConsumer =  false

If AcDetails.VATCode = "7" Then
	blnIsDEConsumer =  true
End If

FormAction = request("FormAction")

if FormAction = "ORDER_SUBMIT" then

	'jhInfo "BEGIN PLACING ORDER!"
	
	OrderRefYours = request.form("OrderRefYours_SUBMIT")
	'jh "OrderRefYours = " & OrderRefYours
	
	SpecialInstructions = request.form("SpecialInstructions_SUBMIT")
	'jh "SpecialInstructions = " & SpecialInstructions	
	DeliveryDate = getdate_JQ("DeliveryDate_SUBMIT")
	if not isdate(DeliveryDate) then DeliveryDate = ""
	
	if OrderRefYours <> "" then
		
		sql = "SELECT thYourRef FROM [Transactions] WHERE thYourRef = '" & validstr(OrderRefYours) & "' AND idxAcCode = '" & acDetails.AcCode & "';"	
		'jh sql
		set rs = db.execute(sql)
	
		if not rs.eof then
	
			jhAdmin "FOUND"
			ErrorMessage = "Your order ref (" & OrderRefYours & ") already exists in our database - please select another."
			ErrorsFound = true
		end if
	
		rsClose()
	
	end if

	if OrderRefYours = "" and AcDetails.MandatoryPONumber then
		ErrorMessage = "You must enter a valid purchase order number (in the 'My Order Ref' field) before submitting this order."
		ErrorsFound = true
	end if 
	
	if not errorsFound then
		
		set BasketDetails = new ClassBasketDetails	
		
        if AcDetails.RiderCard then
            sql = "exec spB2BOrderSubmitRiderCard '" & AcDetails.AcCode & "', '" & AcDetails.CostBand & "', '" & OrderRefYours & "', '" & validstr(DeliveryDate) & "', '" & validstr(BasketDetails.DeliverySKU) & "', '" & validstr(SpecialInstructions) & "' "
        else
            sql = "exec spB2BOrderSubmit '" & AcDetails.AcCode & "', '" & AcDetails.CostBand & "', '" & OrderRefYours & "', '" & validstr(DeliveryDate) & "', '" & validstr(BasketDetails.DeliverySKU) & "', '" & validstr(SpecialInstructions) & "' "
        end if
    
        sql = sql & ", " & BasketDetails.BasketCount
		sql = sql & ", " & BasketDetails.BasketTotal
		sql = sql & ", " & BasketDetails.VATTotal	
        sql = sql & ", " & BasketDetails.DeliveryTotal
		sql = sql & ", " & BasketDetails.GrandTotal		
				
		
		db.execute(sql)
		
		sql = "SELECT MAX(o.B2BOrderID) AS MaxRec FROM B2B_Orders o INNER JOIN B2B_Basket b ON b.B2BOrderID = o.B2BOrderID WHERE o.AcCode = '" & AcDetails.AcCode & "';"
		jhAdmin sql
		set rs = db.execute(sql)
		
		OrderID = ""
		
		if not rs.eof then
		
			jhAdmin "FOUND, CREATE XML INSTRUCTIONS!"
			
			OrderID = "" & rs("MaxRec")
			jhAdmin "OrderID = " & OrderID
			
		
		end if
		
		rsClose()
	
		'jhStop
		
		if OrderID <> "" then
		
			call XMLOut_B2B(OrderID)
			
			OrderComplete = true
			
			dbClose()
			
			response.redirect "/checkout/order-complete/default.asp?LoadOrder=" & OrderID
		end if
	end if
	

elseif FormAction = "BASKET_UPDATE" then
		
	'jh "BASKET UPDATE!"
	
	AllocStockSel = Request.Form("AllocStockSel")

	If AllocStockSel <> "" Then

		If AllocStockSel = "YES" Then
			Session("IsPADespatch") = "YES"
		Else
			Session("IsPADespatch") = ""
		End If 

		jhAdmin "AllocStockSel = " & AllocStockSel
		
	End If	
	
	BasketCount = cleannum(request("BasketCount"))
	if BasketCount = "" then BasketCount = 0
	
	OrderRefYours = request("OrderRefYours")
	DeliveryDate = getdate_JQ("DeliveryDate")
	SpecialInstructions = request("SpecialInstructions")
	
	OrderRefYours = request("OrderRefYours")
	
	BasketCount = cdbl(BasketCount)
	
	if BasketCount >= 0 then
		'jhInfo "BasketCount = " & BasketCount
		for c = 0 to BasketCount 
			ThisBasketID = request("BasketID_" & c)
			ThisSKU = request("SKU_" & c)
			ThisQty = cleannum(request("QTY_" & c))
			ThisDelete = request("Delete_" & c)
			if ThisDelete <> "" then ThisQty = ""
			
			
			sql = ""
				
			if ThisQty = "" then ThisQty = 0 else ThisQty = cdbl(ThisQty)
			if ThisQty < 0 then ThisQty = 0
			
			'end if
			
			sql = "exec [spB2BBasketAdd] '" & validstr(ThisSKU) & "', 0" & ThisQty & ", '" & AcDetails.AcCode & "', ''"				
			
			if sql <> "" then
				'jhAdmin sql
				db.execute(sql)
			end if
			
			
		next
	else
	
	end if
	
	BasketUpdated = true
	jhAdmin "UPDATES COMPLETED!"
	
end if

set BasketDetails = new ClassBasketDetails	


'FORWARD/BACK ORDER LOGIC

if glFOBOCheckout then

	IsConfirmedPA = False
	IsPendingPA = False

	sql = "SELECT AcCode FROM ExchequerAllocStockArrival WHERE AcCode = '" & AcDetails.AcCode & "' AND IsConfirmed = 1"

	jhAdmin sql
	set rs = db.execute(sql)
	if not rs.eof then
		IsConfirmedPA = True
	end if
	rs.Close

	if (not IsConfirmedPA) then
	
		sql = "SELECT AcCode FROM ExchequerAllocStockArrival WHERE AcCode = '" & AcDetails.AcCode & "' AND IsConfirmed = 0"
		jhAdmin sql
		set rs = db.execute(sql)
		if not rs.eof then
			IsPendingPA = True
		end if
		rs.Close

	end if
		
	If (IsConfirmedPA) and BasketDetails.DeliveryTotal > 0 Then

		CheckoutList = ""
	
		strAllocQuery = "select sa.SOROurRef, sa.SKU, sd.stDesc1 AS Descr,  1 as Quantity, sa.AllocDate, "
		strAllocQuery = strAllocQuery & "SORYourRef = (SELECT thYourRef FROM transactions WHERE thOurRef = sa.SOROurRef) "
		strAllocQuery = strAllocQuery & "from exchequerallocstockarrival sa "
		strAllocQuery = strAllocQuery & "inner join exstock sd on sd.stIdxStockCode = sa.SKU "
		strAllocQuery = strAllocQuery & "WHERE sa.AcCode = '" & AcDetails.AcCode & "'  and IsConfirmed = 1 and AllocDate < dateadd(d,1,(select MIN(Allocdate) FROM exchequerallocstockarrival WHERE AcCode = '" & AcDetails.AcCode  &  "')) and IsConfirmed = 1 ORDER BY AllocDate"
		
		set rs = db.execute(strAllocQuery)
		
		if not rs.eof then
		
			CheckoutList = CheckoutList & "<table border=1 class=""ReportTable""><tr><th>" & lne("label_checkout_backorders_our_ref") & "</th><th>" & lne("label_checkout_backorders_your_ref") & "</th><th>" & lne("label_checkout_backorders_product") & "</th><th>" & lne("label_checkout_backorders_description") & "</th><th>" & lne("label_checkout_backorders_qty") & "</th></th>"

			intCounter = 0
			While not rs.eof 
				CheckoutList = CheckoutList & "<tr class=legible><td>" & rs.Fields("SOROurRef").Value & "</td><td>" & rs.Fields("SORYourRef").Value & "</td><td>" & rs.Fields("SKU").Value & "</td><td>" & rs.Fields("Descr").Value & "</td><td>" & rs.Fields("Quantity").Value & "</td></tr>"
				If (intCounter = 0) Then
					dtAllocDate = CDate(rs.Fields("AllocDate").Value)
				End If
				intCounter = intCounter + 1												
				rs.Movenext
			Wend 		
			
			dtArriveDate = DateAdd("d",17,dtAllocDate) 
			CheckoutList = CheckoutList & "</table>" 		
				
			CheckoutList = CheckoutList & "<div class=spacer10></div>" 
			
			CheckoutList = CheckoutList & "<div class=spacer5><span class=""label label-success"">" & lne("label_checkout_backorders_option1") & "</span></div>" 	
			CheckoutList = CheckoutList & "<div class=""spacer20 legible"">"
			CheckoutList = CheckoutList & "<input class=""form-control"" type=""radio"" name=""AllocStockSel"" id=""AllocStockSel_NO"" Value=""NO""" 
			If (AllocStockSel = "" OR AllocStockSel = "NO") Then
				CheckoutList = CheckoutList & " checked "
			End If			
			CheckoutList = CheckoutList & " onclick=""SelectAllocStock(this);""> "		
			CheckoutList = CheckoutList & "<label for=""AllocStockSel_NO"">" & lne("label_checkout_backorders_send_now") & "</label>"
			CheckoutList = CheckoutList & "</div>"
			
			'CheckoutList = CheckoutList & "<hr>" 
			
			CheckoutList = CheckoutList & "<div class=spacer5><span class=""label label-warning"">" & lne("label_checkout_backorders_option2") & "</span></div>" 	
			CheckoutList = CheckoutList & "<div class=""spacer20 legible"">"
			CheckoutList = CheckoutList & "<input class=""form-control"" type=""radio"" name=""AllocStockSel"" id=""AllocStockSel_YES"" Value=""YES""" 
			If (Len(Session("IsPADespatch")) > 0) Then
				CheckoutList = CheckoutList & " checked "
			End If			
			CheckoutList = CheckoutList & " onclick=""SelectAllocStock(this);""> "		
			CheckoutList = CheckoutList & "<label for=""AllocStockSel_YES"">" & lne_REPLACE("label_checkout_backorders_hold_order", formatdatetime(dtArriveDate,1)) & "</label>"
			CheckoutList = CheckoutList & "</div>"			
			
			
		end if
		
		
	Else
		If (IsPendingPA) Then
			CheckoutList = CheckoutList & "" & GetText("CHECKOUT_PAConfirm")  & ""
		End If
	End If	
		
	rsClose()
		
end if		

'FORWARD BACK ORDER LOGIC ENDS 


 
'jh "OrderThreshold = " & OrderThreshold

%>

<link rel="stylesheet" href="css/index.css" type="text/css" media="screen" />

	<div class="container">      

		<div class=row>
			<hr>
			
			<div class=span3>
				<h3><%=lne("checkout_label_quick_add")%></h3>
								
				<input style="width:70%" type=text class=BigInput id=QASearch>	
				<div class=spacer20></div>	
				<small><%=lne("checkout_label_quick_add_explained")%></small>
				
			</div>
			<div class=span9>
				<div class=spacer20></div>
				<div class="xQAForm legible">

					
					
					<div id=QuickAddDiv>
					 
					</div>						
				</div>
			</div>
		</div>
		
<%



BasketArr = BasketDetails.BasketArr
BasketC = BasketDetails.BasketC

if BasketC < 0 then


%>
		  <div class="row">      
			<div class="span12 spacer10">
			
				<hr>
			
				<h2><%=lne("page_title_checkout")%></h2>

			
			</div>
		</div>	
	 <div class="row">      
			<div class="span9">
				<%
				jhErrorNice lne("label_basket_empty"), "<a href=""/"">" & lne("label_basket_empty_explained") & "</a>"
				%>
				<div class=spacer40></div>
				
			</div>
	</div>
<%
else
%>	
		  <div class="row">      
			<div class="span12 spacer10">
			
				<hr>
			
<%
if ErrorMessage <> "" then
	jhErrorNice lne("label_errors_found"), ErrorMessage
end if
%>			
			
				<h2><%=lne("page_title_checkout")%></h2>
				
				<%
				=lne("content_checkout_explained")
				%>				
				

				
<%
					AvailableCreditAfterOrder = acDetails.AvailableCredit - BasketDetails.GrandTotal
					
					'jh "AvailableCreditAfterOrder = " & AvailableCreditAfterOrder
					
					if AvailableCreditAfterOrder < 0 then
						AvailableClass = "red"
						CreditExceeded = true
					else
						AvailableClass = "green"
					end if		

					if CreditExceeded then	
						if AcDetails.SagePayUser then
					%>
						<div class=spacer20>
							
							<div class="alert alert-error">
								<div class=spacer5>
									<img class=pull-right style="height:50px" src="/assets/images/shop/SecuredBy_Solo.png">
								</div>								
								
								<%
								=lne("credit_limit_warning")
								%>
								

								
								<div class=spacer5>
									<a class="btn btn-small btn-primary" href="/utilities/sagepay/"><%=lne("btn_make_payment")%></a>
								</div>

								
							</div>
						</div>	
					<%
						else
					%>
						<div class=spacer20>
							
							<div class="alert alert-error">

								<%
								=lne("credit_limit_warning_no_sagepay")
								%>
							
							</div>
						</div>						
					<%		
						end if
					end if
				
%>					
				
			</div>
		</div>				
		
		
		 <div class="row">      
		 
			<form action="/checkout/default.asp" name=CheckoutForm id="CheckoutForm" method=post>
		 
			<div class="span3">	
			
				<div class=xwell>
					
					
					<h5><%=lne("checkout_header_your_ref")%><br>
						<%
						if AcDetails.MandatoryPONumber then
						%>
							<span class="label label-important"><%=lne("checkout_ref_required")%>required</span>
						<%
						else
						%>
							<small><%=lne("checkout_explained_your_ref")%></small>
						<%
						end if
						%>
					</h5>
					
					<input type=text class="CheckoutOption" name="OrderRefYours" id="OrderRefYours" value="<%=OrderRefYours%>">
					
					<hr>
					
					<h5><%=lne("checkout_header_delivery_date")%><br><small><%=lne("checkout_explained_delivery_date")%></small></h5>
					
			
					
					<%
					=drawDate_JQ("DeliveryDate", DeliveryDate)
					%>
					
					<hr>
					
					<span style="width:100%;color:orange" class="xlabel xlabel-warning">
					
					<h5><%=lne("checkout_header_special_instructions")%>
					<br><small><%=lne("checkout_explained_special_instructions")%></small>
					</h5>
					
					</span>
					
					
					
					<textarea name="SpecialInstructions" id="SpecialInstructions" class="CheckoutOption legible" style="height:120px;"><%=SpecialInstructions%></textarea>


							
				</div>	
			</div>		 
		 
		 
			<div class="span9">	
			
				<h4><%=lne("header_basket_heading")%> <span class="badge label-success"><%=BasketDetails.UnitTotal%>&nbsp;<%=lne("label_item_count")%></span></h4>	
			
				<!--<h4>Basket contents (<%=BasketCount%>)</h4>-->

<%
				if BasketUpdated then jhInfoNice lne("label_basket_updated"), lne("label_basket_review_totals_prompt")

%>					
				

				
				
				
				<table border=1 id=CheckoutTable width="100%">
						<tr>
							<th valign=top></td>
							<th valign=top><%=lne("label_product_name")%></td>
							<th valign=top><%=lne("label_color")%></td>
							<th valign=top><%=lne("label_size")%></td>
							<th valign=top><%=lne("label_qty")%></td>
							<th valign=top><%=lne("label_each")%></td>
							<th valign=top><%=lne("label_total")%></td>
							<th valign=top><i id="RemoveCheckAll" title="Remove?" class="icon-remove-circle"></i></td>
						</tr>				
				
				<%

				OrderTotal = 0
				DiscountPCT = 0
				if AcDetails.RiderCard then
					DiscountPCT = convertNum(AcDetails.DiscountPCT)
				end if
				
				SomeInvalid = false
                ClearanceQtyInvalid = false
                DiscontinuedQtyInvalid = false
				HasClearanceItems = false

				for c = 0 to BasketC
					BasketID = BasketArr(0,c)
					ProductID = BasketArr(1,c)
					ThisCode = BasketArr(2,c)
					ThisName = BasketArr(3,c)
					ThisVariant = BasketArr(4,c)
					ThisColor = BasketArr(5,c)
					ThisSKU = BasketArr(6,c)
					ThisSize = BasketArr(9,c) 
					'Hardcoded deliberately
					strOverwrittenSize = ShowOverwrittenSizeName(ThisSKU, ThisSize)
					'

					imgURL = getImageDefaultCDN(ThisCode, ThisVariant)
					
					ThisQty = cdbl(BasketArr(7,c))
					
				
					ThisMAI = cdbl(BasketArr(18,c))
					ThisBO = cdbl(BasketArr(19,c))
					
					DispMAI = dispStock_ENQUIRY(ThisMAI, ProductID, ThisVariant, ThisSKU)
					BOStr = dispBO(ThisBO)
					
					
					ThisFO = cdbl(BasketArr(22,c))
					FOStr = dispFO(ThisSKU, ThisFO)			
					
					ThisVatCode = "" & BasketArr(26,c)
					if ThisVatCode <> "S" then						
						SomeVatFree = true
					end if					
					
					
					if AcDetails.RiderCard then				
						ThisUnitPrice = "" & BasketArr(23,c)
						ThisUnitPrice = convertnum(ThisUnitPrice)
						ThisUnitPriceDispOnly = ThisUnitPrice * ((100-DiscountPCT)/100)
					else
						ThisUnitPrice = "" & BasketArr(10,c)
					end if
					
					
					if trim("" & ThisUnitPrice = "0") then ZeroPrices = true
				
					if ThisUnitPrice = "" then 
						ThisUnitPrice = 0
						ThisLinePrice = 0
						DispLinePrice = 0
					else
						ThisUnitPrice = cdbl(ThisUnitPrice)
						DispUnitPrice = dispCurrency(ThisUnitPrice)
						ThisLinePrice = ThisUnitPrice * ThisQty
						DispLinePrice = dispCurrency(ThisLinePrice)
					end if
					
					OrderTotal = OrderTotal + ThisLinePrice
					
					ThisStatusName = "" & BasketArr(21,c)
					ThisCanPurchase = "" & BasketArr(24,c)
					ThisCanPurchaseZeroMAI = "" & BasketArr(25,c)
					
					QtyDisabledStr = ""
					StatusStr = ""
                    ClearanceStr = "<span title=""Clearance item"" class=""label label-info""><i class=""icon-fire icon-white""> </i></span>"
                    DiscontinuedStr = ""
					
					
					if ThisStatusName = "CLEARANCE" and glClearanceListActive then
					    HasClearanceItems = true
                        StatusStr = "&nbsp;<span title=""Clearance item"" class=""label label-info""><i class=""icon-fire icon-white""> </i></span>"

                        if (ThisQty > ThisMAI) then
						    ClearanceQtyInvalid = true
					    end if
					end if
					
					
					if ThisStatusName = "DISCONTINUED" and ThisMAI <= 0 then
	                  elseif ThisStatusName = "DISCONTINUED" then
                        if (ThisQty > ThisMAI) then
                            DiscontinuedQtyInvalid = true
                            if DiscontinuedStr = "" then
                                DiscontinuedStr = ThisSKU
                            else
                                DiscontinuedStr = DiscontinuedStr & ", " & ThisSKU
                            end if
                        end if
					end if					
					
					DeleteCheck = false
					if (AcDetails.RiderCard and ThisMAI <= 0) or (ThisCanPurchaseZeroMAI = "0" and ThisMAI <= 0) or (ThisCanPurchase = "0") then
						xThisQty = 0
						QtyDisabledStr = "DISABLED"			
						SomeInvalid = true
						DeleteCheck = true
					end if
					
					
					%>
						<tr>
							<td style="width:60px;"><img class="CheckoutImage" title="Product image" src="<%=imgURL%>"></td>
							<td>
								<strong><%=ThisVariant%></strong><br>
								<a title="Product page" class=OrangeLink href="/products/?ProductID=<%=ProductID%>"><%=ThisName%></a>
								<div><span class=TinySKU><%=ThisSKU%></span></div>
								
								<div><%=StatusStr%></div>
								
								<input type=hidden class=FormHidden name="BasketID_<%=c%>" value="<%=BasketID%>">
								<input type=hidden class=FormHidden name="SKU_<%=c%>" value="<%=ThisSKU%>">
							</td>
							<td>
								<%=ThisColor%>
								<div>
									
									
									<%=BOStr%> <%=FOStr%>
									
								</div>
							</td>							
							
							<td title="<%=ThisSKU%>"><%=strOverwrittenSize%></td>
							
							<td align=right>
								<input <%=QtyDisabledStr%> autocomplete=OFF name="QTY_<%=c%>" id="QTY_<%=c%>" class="CheckoutQty <%=OOSClass%>" value="<%=ThisQty%>">
								<div class=spacer10></div>
								<%=DispMAI%>
								
								
							</td>
							<td align=right><%=DispUnitPrice%></td>
							<td align=right><%=DispLinePrice%></td>
							<td align=right><input <%if DeleteCheck then response.write "CHECKED"%> class=RemoveCheck type=checkbox ID="Delete_<%=c%>" name="Delete_<%=c%>"></td>
						</tr>
					<%
					
					
				next
				
				OrderTotal = BasketDetails.BasketTotal
				DeliveryTotal = BasketDetails.DeliveryTotal
				VATTotal = BasketDetails.VATTotal
				GrandTotal = BasketDetails.GrandTotal
				FreeDelGap = BasketDetails.FreeDelGap
                
		                    FreeDelGapDispOnly = FreeDelGap

				BODespBalance = BasketDetails.BODespBalance
				BONewSmallOrdersBalance = BasketDetails.BONewSmallOrdersBalance
				
				DispOrderTotal = dispCurrency(OrderTotal)

		                    GrandTotalDispOnly = GrandTotal

				%>
				
										
						
				
						<tr>
							<td colspan=4></td>
							<td class=text-right><button id=btnUpdateQty class="btn btn-small btn-inverse">update</button></td>
							<td colspan=3></td>
						</tr>		

						
				</table>
				
				<input type=hidden class=FormHidden name=BasketCount value="<%=BasketC%>">
				<input type=hidden class=FormHidden name=FormAction value="BASKET_UPDATE">
				
				
					

			
							
							
			
				<div class=spacer20></div>
					
			<div class="text-right spacer40">
			
			
<%
					if AcDetails.DeliveryTBC then
					%>
						<div class=spacer20>
							<span class="badge badge-warning">
								<%
								=lne("label_delivery_tbc_explained")
								%>
							</span>								
						</div>						
					<%	
					elseif BODespBalance > 0 then
					%>
						<div class=spacer20>
							<span class="badge badge-success">
								<%
								=lne("label_free_del")
								%>
							</span>
							<div class=legible>
							<%
							=lne("label_free_del_bo")
							%>
							</div>		
						</div>						
					<%
					elseif BONewSmallOrdersBalance > 0 and DeliveryTotal <= 0 then
					%>
						<div class=spacer20>
							<span class="badge badge-success">
								<%
								=lne("label_free_del_small_order")
								%>
							</span>
						</div>						
					<%
					elseif FreeDelGapDispOnly = 0 then
					%>
						<div class=spacer20>
							<span class="badge badge-success">
								<%
								=lne("label_free_del")
								%>
							</span>
						</div>						
					<%
					else
					%>
						<div class=spacer20>
							<span class="badge badge-important">
								<%
								=lne_REPLACE("label_add_more", dispCurrency(FreeDelGapDispOnly))
								%>
							</span>
						</div>					
					<%					
					end if
					
					
%>
					<hr>	
					<h4>
						<%=lne("label_subtotal")%>&nbsp;
						<%
							=DispOrderTotal
						%>
					</h4>
					
<%
				if  AcDetails.DealerDiscount > 0 then
		%>						
						<h4>
							<span class="label label-info"><%=lne_REPLACE("label_checkout_dealer_discount", formatpercent(AcDetails.DealerDiscount))%></span>&nbsp;							
						</h4>
						<%	
					end if
%>
<%					
					if AcDetails.DeliveryTBC then
						DeliveryStr = lne("label_delivery_TBC")
					else
					    DeliveryStr = dispCurrency(DeliveryTotal)
					end if
%>					
					
					<hr>
					<h5>
						<%=lne("label_delivery")%> &nbsp;
						<%
							=DeliveryStr
						%>
					</h5>						
					
					<%
					if VatTotal > 0 then
						if AcDetails.RiderCard then
							if blnIsDEConsumer then
								VatLabel = lne("rider_card_de_vat") 
							else
								VatLabel = lne("rider_card_vat") 
							end if 
						else
							VatLabel = "+ " & lne("vat_abbr") & "  @ "
						end if

					%>
					<hr>
					<h5>
<%
If blnIsDEConsumer Then
	Response.Write (VatLabel & glVatDERate & "% " & dispCurrency(VATTotal))
Else
			if (AcDetails.VATCode = "S") Then
				Response.Write (VatLabel & glVatRate & "% "  & dispCurrency(VATTotal))
			elseif (AcDetails.VATCode = "8") Then
				Response.Write (VatLabel & glVat8Rate & "% "  & dispCurrency(VATTotal))
			else
				Response.Write (VatLabel & glVatRate & "% "  & dispCurrency(VATTotal))
			end if
End If
%>
</h5>
					<%
					if SomeVatFree AND NOT blnIsDEConsumer then
					%>
						<span class="label label-info"><%=lne("label_checkout_some_vat_free")%></span>
					<%	
					end if
					
					end if
					%>
					<hr>
					<h3>    
                        
						<%
                            =lne("label_grand_total")%>&nbsp;<%
							=dispCurrency(GrandTotalDispOnly)
						%>
					</h3>	
					<%

					
					
			
					

					
					%>
					<div id=ChangedError style="display:none;" class="alert alert-error">
						<h4><%=lne("label_basket_modified_prompt")%></h4>
						<%=lne("label_basket_modified_prompt_explained")%> <br><a href="/checkout/default.asp"><%=lne("label_basket_modified_abandon_changes")%></a>
					</div>
					
					<%
					if SomeInvalid then
					%>
					<div id=ChangedError style="" class="alert alert-error">
						<h4><%=lne("label_basket_some_invalid")%></h4>
						<%=lne("label_basket_some_invalid_explained")%>
					</div>
					<%
					end if
					%>

                    <%
					if ClearanceQtyInvalid then
					%>
					<div id=ChangedError style="" class="alert alert-error">
						<h4><%=lne("label_basket_clearance_qty_invalid")%></h4>
						<%=lne("label_basket_clearance_qty_invalid_explained_start")%><%=ClearanceStr%><%=lne("label_basket_clearance_qty_invalid_explained_end")%>
					</div>				
					<%
					end if
					%>

                    <%
					if DiscontinuedQtyInvalid then
					%>
					<div id=ChangedError style="" class="alert alert-error">
						<h4><%=lne("label_basket_discontinued_qty_invalid")%></h4>
						<%=lne("label_basket_discontinued_qty_invalid_explained")%><br /><br /><b><%=DiscontinuedStr%></b>
					</div>
					<%
					end if
					%>
                
                    <%
					if HasClearanceItems then
					%>
                    <div class="alert">                       
                        <%=lne("label_basket_order_quantities_may_increase_explained")%>
                    </div>
					<%
					end if
					%>
					<hr>
	
					<a href="/" class="btn btn-small"><< <%=lne("btn_continue_shopping")%></a>
				
					<hr>
					<div class=text-left>
						<%
						=lne("checkout_important_information")
						%>	
						
						<p class="legible small">
						<% If (BBLDealerStatus = "") Then 
							Response.Write lne("checkout_endura_only_info")
						ElseIf (BBLDealerStatus = "BBLONLY") Then  
							Response.Write lne("checkout_bbl_only_info")
						End If
						%>	
						</p>
					
						<%
						if ZeroPrices then
							jhErrorNice "Pricing issue", "One or more items in your basket has a a zero price associated with it, this is probably an error on the pricelist on our B2B system, and will be be adjusted to the correct price when the order reaches our main backoffice ordering systems.  If you have any concerns, pleae contact us before ordering."
						end if
						
						if CheckoutList <> "" then
						%>
							<div class="alert text-left">
								<h4><%=lne("label_checkout_back_orders_important")%></h4>
								<p><%=lne("label_checkout_back_orders_select_option")%></p>
								<div class=spacer10></div>
								<%
								=CheckoutList
								%>
							</div>
						<%
						end if
						%>
						
						<%
						if BasketDetails.DeliveryTotal > 0 and Session("IsPADespatch") <> "YES" then
							FreeDelAcceptanceRequired = "1"
							
							DispDelThreshold = "<strong>" & dispcurrency(AcDetails.DeliveryThreshold) & "</strong>"
						%>
							<div class="alert alert-error">
								<h4 class=spacer20><%=lne("checkout_delivery_charge_ack_heading")%></h4>
								
								<p>
									<%=lne_REPLACE("label_checkout_below_free_threshold", DispDelThreshold)%>
								</p>
									
								<p>
									
									<strong><input type=checkbox id=FreeDelAcceptanceCheck> <label for="FreeDelAcceptanceCheck"><%=lne("label_checkout_accept_delivery_charge")%></label></strong>
								</p>
							</div>
						<%
						end if					
						%>							
						
					</div>
				</div>							
							
				
			</div>
			
			
			</form>

			<div class=text-right>	
				<span class="label label-important" id="OrderProcessMessage">.</span>&nbsp;&nbsp;<button onclick="processOrder()" <%if SomeInvalid Or ClearanceQtyInvalid Or DiscontinuedQtyInvalid then response.write "DISABLED" %> id=OrderSubmitButton class="btn btn-large btn-primary"><%=lne("btn_place_order")%></button>	
			</div>
			<form id=SubmitOrderForm method=post action="/checkout/default.asp" name=OrderSubmitForm>
				<input type=hidden class=FormHidden name=OrderRefYours_SUBMIT id=OrderRefYours_SUBMIT value="">	
				<input type=hidden class=FormHidden name=DeliveryDate_SUBMIT id=DeliveryDate_SUBMIT value="">	
				<input type=hidden class=FormHidden name=SpecialInstructions_SUBMIT id=SpecialInstructions_SUBMIT value="">	
				<input type=hidden class=FormHidden name=FormAction value="ORDER_SUBMIT">
				<input class=FormHidden type=hidden value="<%=FreeDelAcceptanceRequired%>" id=FreeDelAcceptanceRequired>
				
			</form>			
			
		 </div>
		

	
<%
end if
%>	


	</div>
	<div class=spacer40></div>			
			

<%
AutoCompleteStylesRequired = true
%>			
			

<!--#include virtual="/assets/includes/footer.asp" -->


<script>

		$( "#QASearch" ).autocomplete({		  
		  minLength: 3, 
		  source: availableStyles		  
		  ,		  
			  
			select: function (event, ui) {
					
					var selVariant = ui.item.variantCode;					
					
					//alert(selVariant);
					
					checkoutSearch(selVariant)
					$( "#QASearch" ).val('');
					
					return false;
				}
		  
		});	


</script>

<script>
	
	function processOrder() {
	
		
	
		$("#OrderRefYours_SUBMIT").val($("#OrderRefYours").val());
		$("#DeliveryDate_SUBMIT").val($("#DeliveryDate").val());
		$("#SpecialInstructions_SUBMIT").val($("#SpecialInstructions").val());
		
		var allowOrderProcess = true
		
		// CYCLE THROUGH REASONS NOT ALLOW ORDER SUBMISSION
		
		if ($("#FreeDelAcceptanceRequired").val() != '' && !$("#FreeDelAcceptanceCheck").prop('checked') && !$("#AllocStockSel_YES").prop('checked')) {
			//alert("")
			
			
			
			$('#OrderProcessMessage').html('Please indicate you accept the delivery charge notice by clicking the checkbox, otherwise continue shopping');
			$('#OrderProcessMessage').show( "fast", function() {
				// Animation complete.
			});
			
		

			
			allowOrderProcess = false;
		} 
		
		if (allowOrderProcess) {
			//alert("ok")
			$("#SubmitOrderForm").submit();
		}
	
	}

	$(".CheckoutQty").change(function () {
		//alert("CHANGED");
		
		checkoutChanged() 
		
	});
	
	//$("#QASearch").change(function () {
		//alert("CHANGED");
		
	//	checkoutSearch($(this).val())
		
	//});
	
	function checkoutChanged() {
	
		$("#btnUpdateQty").removeClass('btn-inverse');
		$("#btnUpdateQty").addClass('btn-warning');
		
		$("#OrderSubmitButton").attr("disabled", true);
		$('#ChangedError').show(100, function() {
			// Animation complete.
		  });		
	
	}
	
	function checkoutSearch(pSearchVal) {
	
		var searchVal = pSearchVal;
	
		if (pSearchVal!='') {
		
			$("#QuickAddDiv").html('<img src="/assets/images/icons/indicator.gif"> please wait...');
		
			$.ajax({			
				scriptCharset: "ISO-8859-1" ,
				contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
				url: "/basket_server.asp?CallType=CHECKOUT_ADD&SearchVal=" + searchVal,
				cache: false,			
				}).done(
					function( html ) {
					$("#QuickAddDiv").html(html);
					$("#QuickAddDiv").show(100, function() {
						// Animation complete.
					  });					
					
					var x = document.getElementById('SKUQty');
					$(x).focus()
					//alert('focus')  ;
					$(x).select();
				});	
		}
	}
	
	$('#QASearch').keypress(function (e) {
	  if (e.which == 13) {
		checkoutSearch($(this).val())
		return false;    //<---- Add this line
	  }
	});			
		
	$(document).ready(function() {
		$("#QASearch")
			.focus(function () { $(this).select(); } )
			.mouseup(function (e) {e.preventDefault(); });
			

		//select and deselect
		$("#RemoveCheckAll").click(function () {
		
			if ($( "#RemoveCheckAll" ).hasClass( "allchecked" )) {
				$('.RemoveCheck').prop('checked', false);	
				$( "#RemoveCheckAll" ).removeClass( "allchecked" )
			} else {
				$('.RemoveCheck').prop('checked', true);	
				$( "#RemoveCheckAll" ).addClass( "allchecked" )				
			}
		
			
			//alert('clicked')
		});

			
	});	
	
	
</script>

<script>

	function ba(obj, pid) {
			//	alert(pid);
			
			var thisForm = document.getElementById('productform_' + pid)
			var str = $(thisForm).serialize();
			
			$("#QuickAddProgressNotification").html('<img src="/assets/images/icons/indicator.gif"> adding to basket, please wait...');
			
			
			$.ajax({				
				scriptCharset: "ISO-8859-1" ,
				contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
				url: "/basket_server.asp?CallType=ADD&CallContext=CHECKOUT",				
				data: str,
				cache: false,			
				}).done(
					function( html ) {
						$("#ProductAjaxDiv_" + pid).html(html);						
						$('#ProductAjaxDiv_' + pid).show(100, function() {
						bCount();
						bSummary();
						$("#QASearch").val('')
						$("#QASearch").focus()
						
						$("#QuickAddProgressNotification").html('');
						
						return false;
						
					 });
			});	
			
			checkoutChanged() 
			
			//alert('Button disable')
			
			return false;		
	}
	
	
	function SelectAllocStock(objRadio)
	{
		//alert('SelectAllocStock')
		checkoutChanged() 
		$('#btnUpdateQty').click();
	}
	
</script>

<%

dbClose()

%>

