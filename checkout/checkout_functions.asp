<%

'jhInfo "CHECKOUT FUNCTIONS LOADED!"

function XMLOut_B2B(pOrderID)

	XMLOut_B2BMain = false
	
	set fs = server.CreateObject("Scripting.FileSystemObject")
	
	set f = fs.OpenTextFile(glXMLTemplate_OrderHeader)
	
	OrderHeaderStr = f.readall
	
	f.close
	set f = nothing

	set f = fs.OpenTextFile(glXMLTemplate_OrderLine)
	
	OrderLineStr = f.readall
	
	OrderLineStr = vbcrlf & OrderLineStr
	
	f.close
	set f = nothing	
	
	OutputXML = OrderHeaderStr
	
	dbConnect()
		
        sql = "exec [spB2BOrderDetails] '" & acDetails.AcCode & "', '" & AcDetails.CostBand  & "', 0" & pOrderID
	
	x = getrs(sql, OrderArr, OrderC)
	
	OrderLineNo = 0
	
	
	if OrderC >= 0 then
	
		jhAdmin "ORDER FOUND!"
	
		OrderRef = OrderArr(1,0)
		CustomerRef = OrderArr(2,0)
        CustomerRef = replace(CustomerRef, "&", "AND")
		
		OrderDate = OrderArr(3,0)
		DeliveryDate = OrderArr(4,0)
		
		jhAdmin "OrderRef = " & OrderRef
		jhAdmin "CustomerRef = " & CustomerRef
		jhAdmin "OrderDate = " & OrderDate
		jhAdmin "DeliveryDate = " & DeliveryDate
			
		SpecialInstructions = OrderArr(5,0)				
		DelSKU = OrderArr(6,0)
		DelCost = OrderArr(13,0)
		
		
		jhAdmin "SpecialInstructions = " & SpecialInstructions
		jhAdmin "DelSKU = " & DelSKU
		jhAdmin "DelCost = " & DelCost
		
		XMLFileName = OrderArr(7,0)		
		XMLCreated = "" & OrderArr(8,0)
		XMLJobSubmitted = "" & OrderArr(9,0)
		
		jhAdmin "XMLFileName = " & XMLFileName		
		jhAdmin "XMLCreated = " & XMLCreated	
		jhAdmin "XMLJobSubmitted = " & XMLJobSubmitted
		
		if XMLCreated = "1" or XMLJobSubmitted = "1" then exit function
		
		ThisCostCentre = AcDetails.CostCentre
		CurrencyName = AcDetails.CurrencyName
		ExchequerCurrencyCode = AcDetails.ExchequerCurrencyCode
		
		jhAdmin "CurrencyName = " & CurrencyName
		jhAdmin "ExchequerCurrencyCode = " & ExchequerCurrencyCode
		
		if not ErrorsFound then
		
			if not isdate(OrderDate) then OrderDate = now()
			if not isdate(DeliveryDate) then DeliveryDate = now()

			if isAdminUser then 
				AdminUserID = AdminDetails.AdminID
				SuppressConfirmationStr = "1" 
			else
				SuppressConfirmationStr = ""
				AdminUserID = ""
			end if
			SuppressConfirmationStr = ""
			
			'SuppressConfirmationStr = "1" 
			AltRef = ""
			
			OutputXML = replace(OutputXML, "***CUSTCODE***", AcDetails.AcCode)
			OutputXML = replace(OutputXML, "***INVOICETO***", AcDetails.AcCode)
			OutputXML = replace(OutputXML, "***DELIVERTO***", AcDetails.AcCode)
			
			OutputXML = replace(OutputXML, "***ORDERDATE***", XMLDate(OrderDate))	
			OutputXML = replace(OutputXML, "***DUEDATE***", XMLDate(DeliveryDate))	
			OutputXML = replace(OutputXML, "***YOURREF***", CustomerRef)
			OutputXML = replace(OutputXML, "***ALTREF***", AltRef)
			
			OutputXML = replace(OutputXML, "***ENDURA_USERID***", AdminUserID)
			OutputXML = replace(OutputXML, "***SUPPRESS_CONFIRMATION***", SuppressConfirmationStr)		
			
			ThisPADespatch = ""
			If (Len(Session("IsPADespatch")) > 0) Then 
				ThisPADespatch = "YES"
			End If	
			OutputXML = replace(OutputXML, "***PA_DESPATCH***", ThisPADespatch)			
			
			OutputXML = replace(OutputXML, "***CURRENCY_CODE***", AcDetails.ExchequerCurrencyCode)
			OutputXML = replace(OutputXML, "***CURRENCY_NAME***", AcDetails.CurrencyName)	
    
        		if AcDetails.RiderCard then
		                OutputXML = replace(OutputXML, "<BuyerType>Standard</BuyerType>", "<BuyerType>RiderCard</BuyerType><RiderCardID>0</RiderCardID>")		
		        end if
			
			'PROCESS ORDER LINES!
			
			VatStr = ""
			if not AcDetails.VATApplicable then
				ThisVatCode = "E"		
				VatStr = VatStr & "<LineTax>" & vbcrlf
				VatStr = VatStr & "<TaxRate Code=""" & ThisVATCode & """>0.00</TaxRate>" & vbcrlf
				VatStr = VatStr & "<TaxValue>0.00</TaxValue>" & vbcrlf
				VatStr = VatStr & "</LineTax>" & vbcrlf      
			end if				
			
			LineAlerts = ""	
			StdLines = ""
			ClearanceLines = ""
				
			for c = 0 to OrderC
			
				ThisSKU = OrderArr(21,c)
				ThisQty = OrderArr(22,c)
				ThisPrice = OrderArr(25,c)
				ThisPriceAtPurchase = OrderArr(30,c)

	        		if AcDetails.RiderCard then
					ThisPrice = ThisPriceAtPurchase
				end if
							
				ThisSKUDesc = OrderArr(28,c)
				ThisStatusName = OrderArr(29,c)

				ThisLineDiscount = 0
				
				jhAdmin ThisSKU & "_" & ThisDesc & "_" & ThisStatusName
				
				if glCustom = "UK" then

					sql = "SELECT TOP 1 OfferGroupID FROM ExchequerOfferSKU WHERE InputSKU = '" & validstr(ThisSKU) & "'"
					sql = sql & " AND OfferGroupID IN (select OfferGroupID from ExchequerOfferGroup WHERE IsActive = 1)"
				
					set rs = db.execute(sql)
					
					if not rs.eof then
						intOfferID = rs("OfferGroupID")
						strOffersXML = strOffersXML  & "<Offer>" & intOfferID & "</Offer>" & vbcrlf
					end if
					rsClose()
				end if
			
				OrderLineNo = OrderLineNo + 1
				StdLines = StdLines & XMLOrderLine(OrderLineStr, OrderLineNo, AcDetails.CostCentre, ThisSKU, ThisSKUDesc, ThisQty, ThisPrice, ThisLineDiscount, VATStr)
				
			next						
			
			
			if DelSKU <> "" and DelCost <> "" then
				OrderLineNo = OrderLineNo + 1						
				StdLines = StdLines & XMLOrderLine(OrderLineStr, OrderLineNo, AcDetails.CostCentre, DelSKU, "Delivery - " & DelSKU, 1, DelCost, 0, VATStr)
			end if
						
			OutputXML = replace(OutputXML, "***OFFERS***", strOffersXML)
			
			AllOrderLines = StdLines
			
			OutputXML = replace(OutputXML, "***TRANSLINES***", AllOrderLines)
			
			'END PROCESS ORDER LINES!
			
			
			'START ADDITIONAL INFO SECTION
			AdditionalInfo = ""
			
			AdditionalInfo = AdditionalInfo & "<AddInfo><![CDATA[" & vbcrlf 

			If Len(SpecialInstructions) > 0 Then
				AdditionalInfo = AdditionalInfo & "===========" & vbcrlf & "Special Instructions:" & vbcrlf & SpecialInstructions & vbcrlf			
			End If

			if 1 = 1 then
				
			elseif DelSKU = "" then
				DeliveryExplainedStr = "NO DELIVERY SKU ADDED TO THIS ORDER"
			end if
		
			If Len(DeliveryExplainedStr) > 0 Then
				AdditionalInfo = AdditionalInfo & "===========" & vbcrlf &  DeliveryExplainedStr & vbcrlf
			End If
				
		
			
			if LineAlertFound then 
				AdditionalInfo = AdditionalInfo & "===========" & vbcrlf
				AdditionalInfo = AdditionalInfo & vbcrlf & "The following order items have been flagged for review:" & vbcrlf 
				AdditionalInfo = AdditionalInfo & LineAlerts						
			end if
				
			AdditionalInfo = AdditionalInfo & vbcrlf & "]]></AddInfo>"
			
			OutputXML = replace(OutputXML, "***ADDINFO***", AdditionalInfo)
			
			'END ADDITIONAL INFO SECTION
			
			'IS THIS ACCOUNT VATTABLE?
			
		
			'jh addbr(stripXML(VatStr))
			
			'jh addbr(stripXML(OutputXML))
	
			OutPutXML = replace(OutPutXML, "***CLEARANCE_ITEM***", "")
	
			if 1 = 1 then
			
				DropFolder = glXMLDropLocation & year(now) & "_" & right("0" & month(now),2) & "\"
				if not fs.folderexists(DropFolder) then fs.createfolder DropFolder				
				Outputfilename = DropFolder  & XMLFileName
				
				FinalDestinationFN = glXMLFinalLocation & XMLFileName
			
				jhAdmin Outputfilename
			
				set f = fs.CreateTextFile(Outputfilename,true)
				
				f.write(OutPutXML)
				
				f.close
				
				fs.CopyFile Outputfilename, FinalDestinationFN
				
				set f = nothing
				set fs = nothing			
			
				sql = "UPDATE B2B_Orders SET XMLCreated = 1 WHERE AcCode = '" & AcDetails.AcCode & "' AND B2BOrderID = 0" & pOrderID
				db.execute(sql)
			end if	
			
			if 1 = 1 then
			
				jhAdmin "COMPLETE - XML CREATED!"
				
				JobStr = "<Transaction XMLDataFile=""" & XMLFileName  & """ EXNumber=""" & pOrderID & """  ExCompany=""" & glJobProcessingCompanyRef & """></Transaction>"				
				jhAdmin "JobStr = " & stripXML(JobStr)			
				InsertJob glNewOrderJobName, JobStr, 0
			
				sql = "UPDATE B2B_Orders SET XMLJobSubmitted = 1 WHERE AcCode = '" & AcDetails.AcCode & "' AND B2BOrderID = 0" & pOrderID
				db.execute(sql)			
			end if	
		end if
	end if
	
	'++++++++++++++++++++++++++++++++++++++++++++++++++
	'++++++++++++++++++++++++++++++++++++++++++++++++++
	'++++++++++++++++++++++++++++++++++++++++++++++++++
	
	exit function
	
end function


function XMLOrderLine(byval pLineStr, byval pLineNo, byval pCostCentre, byval pTextSKU, byval pLineDesc, byval pLineQty, byval pLinePrice,  byval pLineDiscount, byval pVATStr)

	XMLOrderLine = pLineStr
	
	XMLOrderLine = replace(XMLOrderLine, "***LINENO***", pLineNo)
	XMLOrderLine = replace(XMLOrderLine, "***COSTCENTRE***", pCostCentre)
	
	XMLOrderLine = replace(XMLOrderLine, "***USEALTDESC***", "")
	XMLOrderLine = replace(XMLOrderLine, "***LINESKU***", trim("" & pTextSKU))
	XMLOrderLine = replace(XMLOrderLine, "***LINEQTY***", pLineQty)
	XMLOrderLine = replace(XMLOrderLine, "***LINEPRICE***", pLinePrice)
	XMLOrderLine = replace(XMLOrderLine, "***LINEDISC***", pLineDiscount)
	XMLOrderLine = replace(XMLOrderLine, "***LINEDESC***", pLineDesc)
	XMLOrderLine = replace(XMLOrderLine, "***LINEOFFER***", "")
	XMLOrderLine = replace(XMLOrderLine, "***VAT***", pVATStr)					

end function

function XMLInstructionLine(byval pLineStr, byval pLineNo, byval pCostCentre, byval pLineDesc)

	XMLInstructionLine = pLineStr
	
	XMLInstructionLine = replace(XMLInstructionLine, "***LINENO***", pLineNo)
	XMLInstructionLine = replace(XMLInstructionLine, "***COSTCENTRE***", pCostCentre)
	
	XMLInstructionLine = replace(XMLInstructionLine, "***USEALTDESC***", "YES")
	XMLInstructionLine = replace(XMLInstructionLine, "***LINESKU***", "TEXT14")
	XMLInstructionLine = replace(XMLInstructionLine, "***LINEQTY***", 1)
	XMLInstructionLine = replace(XMLInstructionLine, "***LINEPRICE***", 0)
	XMLInstructionLine = replace(XMLInstructionLine, "***LINEDISC***", 0)
	XMLInstructionLine = replace(XMLInstructionLine, "***LINEDESC***", pLineDesc)
	
	XMLInstructionLine = replace(XMLInstructionLine, "***LINEOFFER***", "")
	XMLInstructionLine = replace(XMLInstructionLine, "***VAT***", "")					

end function

function XMLDate(pd)
	if isdate(pd) then
		XMLDate = year(pd) & "-" & right("0" & month(pd),2) & "-" & right("0" & day(pd),2)
	else
		XMLDate = ""
	end if
	'jh XMLDate
end function

function stripXML(p)
	stripXML = replace(p, "<", "&lt;")
end function

%>