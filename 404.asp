<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--#include virtual="/assets/includes/global_functions.asp" -->

<html>
<head>

<%
'jhAdmin Request.ServerVariables("QUERY_STRING")

function cleverURL()
	 
	ThisQS = Request.ServerVariables("QUERY_STRING")
	cleverURL = ""
	if right(ThisQS, 1) = "/" then
		ThisQS = left(ThisQS, len(ThisQS)-1)
	end if
	
	lf = instrrev(ThisQS, "/")
	jhAdmin "lf = " & lf 
	if lf > 0 then 
		CheckCode = right(ThisQS, len(ThisQS) - lf)
		jhAdmin "CheckCode = " & CheckCode
		if CheckCode <> "" then
		
			sql = "SELECT ProductID, "
			sql = sql & "dbo.[spB2CActive](ProductID, '" & SelectedLocation & "') AS B2CActive,  "
			sql = sql & "dbo.[spB2CCustomActive](ProductID) AS CustomActive  "
			sql = sql & "FROM SML_Products sp "
			sql = sql & "WHERE sp.ProductCode = '" & validstr(CheckCode) & "' "
			'sql = sql & "AND dbo.[spB2CActive](sp.ProductID, '') > 0 "
			jhAdmin sql
			dbConnect()
			set rs = db.execute(sql)
			if not rs.eof then	
				jhAdmin "FOUND - " & CheckCode
				if rs("B2CActive") > 0 then
					cleverURL = "/products/?ProductID=" & rs("ProductID")
				elseif rs("CustomActive") > 0 then
					cleverURL = "/custom/products/custom-product-detail.asp?ProductID=" & rs("ProductID")
				end if
			end if
			rsClose()
			
		end if
	end if
end function


cleverRedir  = cleverURL()
jhAdmin "cleverRedir = " & cleverRedir

LangQS = ucase(Request.ServerVariables("QUERY_STRING"))
jhAdmin "LangQS = " & LangQS

sql = "SELECT LanguageID, LanguageISO, ISNULL(RRP_CostBand,'') AS CostBand  FROM END_Language "
sql = sql & "LEFT JOIN END_CountryCodes ecc ON ecc.CountryCode = END_Language.LanguageISO "
if isAdmin then
	sql = sql & "WHERE B2CEditable = 1 "
else
	sql = sql & "WHERE B2CActive = 1 "
end if
x = getrs(sql, llArr, llc)

CostBandLookupFound = false

for c = 0 to llc
	LookForLangID = llArr(0,c)
	LookForLangISO = llArr(1,c)
	LookForLangCostBand = llArr(2,c)
	
	LangCheck = LangQS & "/"
	
	if instr(1, LangCheck, "/" & LookForLangISO & "/") > 0 then

		jhAdmin "FOUND - " & LookForLangISO  & " (" & LookForLangID & ")"
		response.cookies("LocaleDefaults")("SelectedLanguage") = LookForLangID
		response.cookies("LocaleDefaults")("SelectedLanguage_ISO") = LookForLangISO
		response.cookies("LocaleDefaults")("SelectedLocation") = LookForLangISO
		
		if LookForLangCostBand <> "" then
			SetCostBand = LookForLangCostBand
			CostBandLookupFound = true
			jhAdmin "HERE1"
		else
			SetCostBand = "A"
		end if
		
		SetCountryCode = LookForLangISO
		

		
	
		
		exit for
	end if	

next

if not CostBandLookupFound then

		sql = "SELECT CountryCode, ISNULL(RRP_CostBand,'') AS CostBand FROM END_CountryCodes "'WHERE ISNULL(RRP_CostBand, '') <> '' "
		jhAdmin sql
		x = getrs(sql, cArr, cc)
		for c = 0 to cc
			CheckCC = cArr(0,c)
			CheckCB = "" & cArr(1,c)
			
			if instr(1, ucase(LangQS), "/" & CheckCC & "/") > 0 then
				
				jhAdmin "CheckCC = " & CheckCC
				jhAdmin "CheckCB = " & CheckCB
				
				jhAdmin "HERE2"
				
				if CheckCB <> "" then
					SetCostBand = CheckCB
				end if
				
				SetCountryCode = CheckCC
				
			end if
		next	
end if		

if SetCostBand <> "" then
	jhAdmin "SET COST BAND!" 

	jhAdmin "SetCostBand = " & SetCostBand
		session("SelectedCostBand") = SetCostBand
		response.cookies("LocaleDefaults").expires = dateadd("d", 365, now)		
end if

AutoRedirect = false

if SetCountryCode <> "" then

			jhAdmin "SET COUNTRY!" 
			jhAdmin "SetCountryCode = " & SetCountryCode
	
			response.cookies("LocaleDefaults")("SelectedLocation") = SetCountryCode
		
			LookForPage = replace(LangQS, "/" & SetCountryCode & "/", "/")
			
			LookForPage = replace(LookForPage, "404;HTTP://WWW.ENDURASPORT.COM:80", "")
			
			jhAdmin "LookForPage = " & LookForPage
									
			set fs = server.CreateObject("Scripting.FileSystemObject")		

			QSPos = instr(1, LookForPage, "?")
			if QSPos > 0 then
				LookforPage_QS = left(LookForPage, QSPos-1)
				PageLoc = server.mappath(LookforPage_QS)
			else		
				PageLoc = server.mappath(LookForPage)
			end if			
			
			
			jhAdmin "PageLoc = " & PageLoc
			if fs.folderexists(PageLoc) then
				'jh "FOLDER FOUND!"
				cleverRedir = lcase(LookForPage)
			elseif fs.fileexists(PageLoc) then
				'jh "FILE FOUND!"
				cleverRedir = lcase(LookForPage)
			else
				select case LookForPage
					case "/ACCESSORIES/", "/ACCESSORIES"
						cleverRedir = "/products/browse/?enduseid=14"	
					case "/HARDWARE/", "/HARDWARE"
						cleverRedir = "/products/browse/?enduseid=15"		
				
				end select
				
				'jhAdmin "NFOUND!"
				
			end if			
			
			set fs = nothing
			
			response.cookies("DealerLocator")("ShopperCountry") = SetCountryCode
			response.cookies("DealerLocator")("ShopperAddress") = ""
			response.cookies("DealerLocator").expires = dateadd("d", 28, now)
			
			AutoRedirect = true
			
			if cleverRedir = "" then cleverRedir = "/"	
			
else
	
			LookForPage = replace(LangQS, "404;HTTP://WWW.ENDURASPORT.COM:80", "")
			jhAdmin "LookForPage (NO COUNTRY) = " & LookForPage
			
			select case LookForPage
				case "/ACCESSORIES/", "/ACCESSORIES"
					cleverRedir = "/products/browse/?enduseid=14"	
				case "/HARDWARE/", "/HARDWARE"
					cleverRedir = "/products/browse/?enduseid=15"	
				case "/RANGES/", "/COLLECTIONS/"
					cleverRedir = "/products/collections/"								
			end select
				
end if		


if cleverRedir = "" then 	
	'cleverRedir = "/"		
end if
		
if isjh then
	jhAdmin "band = " & session("SelectedCostBand") 
	jhAdmin "location = " & request.cookies("LocaleDefaults")("SelectedLocation")
	jhAdmin "HERE"
	jhAdmin "cleverRedir = " & cleverRedir
	'jhStop
end if

if cleverRedir <> "" then

	
	if isjh then 
		
	
		'jhAdmin "cleverRedir = " & cleverRedir
		'jhstop
	end if

	response.redirect cleverRedir
end if

%>

<style>
	A {
		color:white;
		text-decoration:none;
	} 
	A:HOVER {
		color:orange;
		text-decoration:none;
	} 	
	
</style>

	<title>Endura - Page not found</title>
	<style>
		BODY {
			font-family:arial;
			font-size:10pt;
			margin:200px;
		}
	</style>
</head>
<body>
	<div style="border:3px solid #333;text-align:center;padding:100px;border-radius:25px;color:white;background:#333;">
	<a href="/"><img border=0 src="/assets/images/furniture/nav_logo.png"></a>
	<h3>Oops, sorry the page you requested could not be found</h3>
	<p>We have recently updated our website, so your bookmarks may be out of date</p>
	
	<p>
		<a href="/">Home page</a> | 
		<a href="/products/browse/">Product browser</a> | 
		<a href="/blog/">Blog</a> | 
		<a href="/contact/">Contact Endura</a>
	</p>
		
</body>