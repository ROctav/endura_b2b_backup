<%
PageTitle = "Charity"
PageID = 10
%>

<!--#include virtual="/assets/includes/header.asp" -->

    	
        <div class="container">
		
            <div id=AboutHistory class="section_header">
                <h3><%=getLabel(PageTitle)%></h3>
								
				<div class=clearfix></div>					
            </div>	
			
          <div class="row spacer40" id="support_warranty">
			
                <div class="span7">
                   
					<%=getContent("about_charity")%>
					
					<!--<a href="#"><i class="icon-circle-arrow-up"></i> Back to top</a>    -->
					   
                </div>
				         
                <div class="span4 offset1">
                   
					
					
					<%
					
					sql = "SELECT TOP 10 BlogID, BlogTitle, BlogDate, BlogAbstract, BlogBody, BlogTypeID, ISNULL(BlogTeamAthleteID,0) "
					sql = sql & ", B2CStatusName AS BlogTypeName "
					sql = sql & "FROM B2C_Blog "
					sql = sql & "INNER JOIN (SELECT B2CStatusID, B2CStatusName FROM B2C_Status WHERE B2CTypeID = 75 ) c ON c.B2CStatusID = B2C_Blog.BlogTypeID "
					sql = sql & "WHERE BlogStatusID = 1 AND CAST(BlogDate AS DATE) <= '" & formatdatetime(now,1) & "' "
					FilterBy = 9
					if FilterBy <> "" then
						sql = sql & "AND BlogTypeID = 0" & FilterBy & " "
					end if
					sql = sql & "ORDER BY BlogDate DESC "
					'jhAdmin sql
					x = getrs(sql, BlogArr, BlogC)
					'jh "BlogC = " & BlogC
					
					if BlogC >= 0 then
						%>
						<h3><%=lne("charity_latest_news")%></h3>
						<%
						for c = 0 to BlogC
							BlogID = BlogArr(0,c)
							BlogTitle = BlogArr(1,c)
							BlogDate = BlogArr(2,c)
							BlogAbstract = BlogArr(3,c)
						%>
							<hr>
							<h5><a href="/blog/?BlogID=<%=BlogID%>&setfilter=9"><%=BlogTitle%></a></h5>
							<div style="color:orange;font-weight:bold;"><%=formatdatetime(BlogDate,2)%></div>	
							<p><%=BlogAbstract%></p>
						<%
						next
					end if	
					%>
					
					<p>
						<a href="/blog/?setfilter=9" class=OrangeLink><%=lne("charity_more_blog_link")%></a>
					</p>
					   
					 <img src="/assets/images/charity/charity1.jpg" class="img img-rounded spacer40">  
					   
                </div>
						 
            </div>					
			
			
			 <div class="spacer40">&nbsp;</div>
        </div>
	

<!--#include virtual="/assets/includes/footer.asp" -->