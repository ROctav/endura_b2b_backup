<%
PageTitle = "About us"
PageID = 2
%>

<!--#include virtual="/assets/includes/header.asp" -->

    <div id="aboutus">
		
        <div class="container">
		

		
            <div id=AboutHistory class="section_header">
                <h3><%=getLabel("About Endura")%></h3>
				
				<div class="navbar navbar-simple">
							
					<ul class="nav">
					  <li><a class=ContentLink href="#AboutHistory"><%=lne("about_caption_history")%></a></li>
					  <li><a class=ContentLink href="#AboutProduction"><%=lne("about_caption_production")%></a></li>
					  <li><a class=ContentLink href="#AboutDevelopment"><%=lne("about_caption_prod_dev")%></a></li>
					  <li><a class=ContentLink href="#AboutSponsorship"><%=lne("about_caption_sponsorship")%></a></li>
					</ul>
				  
				</div>	
				<div class=clearfix></div>					
				
            </div>
			
            <div id=AboutHistory class="section_header">
               <h3><%=lne("about_caption_history")%></h3>
            </div>			
			
            <div class="row">
			

                <div class="span7 intro spacer40">
                   
                    <%=getContent("ABOUT_HISTORY")%>
					   
					<a href="#"><i class="icon-circle-arrow-up"></i> <%=lne("nav_back_to_top")%></a>    
					   
                </div>

				<div class="span4 intro spacer40 offset1">
					 <img class="spacer40 img-rounded" src="/assets/images/temp/about_history1.png" />		
					 &nbsp;
					 <img class="spacer40 img-rounded"  src="/assets/images/temp/about_history2.png" />		
					
				</div>
				
            </div>
        </div>
		
        <div class="container">
            <div id=AboutProduction class="section_header">
               <h3><%=lne("about_caption_production")%></h3>
            </div>
            <div class="row">
                <div class="span7 intro spacer40">
                   
				   
				   <%=getContent("ABOUT_PRODUCTION")%>
				   
                       		
					<a href="#"><i class="icon-circle-arrow-up"></i> <%=lne("nav_back_to_top")%></a>    
							
                </div>
				<div class=span1>
				</div>
                <div class="span4 flexslider ">
                      <ul class="slides">
                        <li>
                          <img src="/assets/images/temp/about_slide1.png" />
                        </li>
                        <li>
                          <img src="/assets/images/temp/about_slide2.png" />
                        </li>
                        <li>
                          <img src="/assets/images/temp/about_slide3.png" />
                        </li>
                      </ul>
                </div>
            </div>
        </div>		
		
		
        <div class="container">
            <div id=AboutDevelopment class="section_header">
                <h3><%=lne("about_caption_prod_dev")%></h3>
            </div>
            <div class="row">
                <div class="span7 intro spacer40">
                   
					<%=getContent("ABOUT_PRODUCT_DEVELOPMENT")%>
                   
					<a href="#"><i class="icon-circle-arrow-up"></i> <%=lne("nav_back_to_top")%></a>    
					   
                </div>
              
				<div class="span4 intro spacer40 offset1">
					 <img class="spacer40 img-rounded"  src="/assets/images/temp/about_history3.png" />		 
				</div>			  
            </div>
        </div>		
		
        <div class="container">
            <div id=AboutSponsorship class="section_header">
				<h3><%=lne("about_caption_sponsorship")%></h3>               
            </div>
            <div class="row">
                 <div class="span7 intro spacer40">
                   
				  <%=getContent("ABOUT_SPONSORSHIP")%> 
				   
					 <a href="#"><i class="icon-circle-arrow-up"></i> <%=lne("nav_back_to_top")%></a>    
					   
                </div>
              
            </div>
        </div>			
		
		
    </div>


<!--#include virtual="/assets/includes/footer.asp" -->