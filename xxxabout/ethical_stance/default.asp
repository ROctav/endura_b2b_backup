<%
PageTitle = lne("page_title_29")
PageID = 29
%>

<!--#include virtual="/assets/includes/header.asp" -->

    	
        <div class="container">
		
            <div id=AboutHistory class="section_header">
                <h3><%=PageTitle%></h3>
								
				<div class=clearfix></div>					
            </div>	
			
			<div class="row spacer40" id="ethical_UK">
			
				<div class="span8">
				   
					<div style="padding-right:40px;">				   
						<%=getContent_ADMIN("about_ethical_stance_UK")%>
					</div>							
					
				</div>
				
				<div class="span4 flexslider ">
					  <ul class="slides">
						<li>
						  <img src="/assets/images/ethical/ethicalstance_UK01.jpg" />
						</li>
						<li>
						  <img src="/assets/images/ethical/ethicalstance_UK02.jpg" />
						</li>
						<li>
						   <img src="/assets/images/ethical/ethicalstance_UK03.jpg" />
						</li>
					  </ul>
				</div>						 
			  
						 
			</div>		

			<div class="row spacer40" id="ethical_SUPPLIER">
			
				<div class="span8">
					<div style="padding-right:40px;">				   
						<%=getContent_ADMIN("about_ethical_stance_SUPPLIERS")%>						
					</div>				
				</div>
						 
				<div class="span4 flexslider ">
					  <ul class="slides">
						<li>
						  <img src="/assets/images/ethical/ethicalstance_SUPPLIERS01.jpg" />
						</li>
						<li>
						  <img src="/assets/images/ethical/ethicalstance_SUPPLIERS02.jpg" />
						</li>
						<li>
						   <img src="/assets/images/ethical/ethicalstance_SUPPLIERS03.jpg" />
						</li>
					  </ul>
				</div>						 
			  
						 
			</div>					
			
			<div class="row spacer40" id="ethical_SUPPLIER">
			
				<div class="span8">
					<div style="padding-right:40px;">				   
						<%=getContent_ADMIN("about_ethical_stance_ENVIRONMENTAL")%>				
					</div>				
				</div>			
			
						 
			</div>				
			
			<div class=spacer40>&nbsp;</div>
        
        </div>	
	

<!--#include virtual="/assets/includes/footer.asp" -->