<%
NoRedirect_HOLDING = true
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<!--#include virtual="/assets/includes/global_functions.asp" -->

	<title>Endura - Site offline</title>
	<style>
		BODY {
			font-family:arial;
			font-size:10pt;
			margin:200px;
		}
		
		A {
			text-decoration:none;
			color:orange;
			font-weight:bold;
		}
	</style>
</head>
<body>
	<div style="border:3px solid #333;text-align:center;padding:100px;border-radius:25px;color:white;background:#333;">
	<a href="/"><img src="/assets/images/furniture/nav_logo.png"></a>
	
	
<%
if glSiteOfflineMessage <> "" then
%>
	<h3><%=glSiteOfflineMessage%></h3>	
<%
else
%>	
	
	<h3>Sorry, the Endura B2B website is currently offline for essential maintenance</h3>
	<p>Please check back soon</p>
<%
end if
%>		
	
	<p>For urgent enquiries, <a href="mailto:ukenquiry@endura.co.uk">send us an email</a> or telephone <strong>+44 (0)1506 497 749</strong></p>
	
</body>