<%
Response.ContentType = "text/html"
Response.AddHeader "Content-Type", "text/html;charset=ISO-8859-1"
'Response.CodePage = 65001
Response.CharSet = "ISO-8859-1"

Response.Expires = 0
Response.Expiresabsolute = Now() - 1442
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"

SelectedLanguageID = session("B2C_UserLanguageID")
if SelectedLanguageID = "" then SelectedLanguageID = "0"

%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	</head>
	<body>
	

<!--#include virtual="/assets/includes/global_functions.asp" -->
<%

'jh "now = " & now

BlogAction = request("BlogAction")
'jhAdmin "BLOG SERVER - " & BlogAction

FilterBy = cleannum(request("FilterBy"))

'jh request("clearfilter")

if FilterBy = "" then
	FilterBy = request.cookies("BlogFilter")("FilterBy")
end if

if FilterBy = "-1" then FilterBy = ""

response.cookies("BlogFilter")("FilterBy") = FilterBy
response.cookies("BlogFilter").expires = DateAdd("d", 2, now)
'FilterBy = ""

'jh "FilterBy = " & FilterBy

dbConnect()

'jhAdmin "SelectedLanguageID = " & SelectedLanguageID

if BlogAction = "LIST" or BlogAction = "ITEM" or BlogAction = "LATEST" then

	sql = "SELECT B2CStatusID, B2CStatusName FROM B2C_Status "
	sql = sql & "WHERE B2CTypeID = 75 AND (SELECT COUNT(*) FROM B2C_Blog "
	sql = sql & "WHERE (ISNULL(BlogLanguages, '') LIKE '%|" & SelectedLanguageID & "|%' OR ISNULL(BlogLanguages, '') = '') "
	sql = sql & "AND BlogStatusID = 1 AND BlogTypeID = B2CStatusID AND BlogDate <= '" & formatdatetime(now,1) & "') > 0 ORDER BY B2CStatusID"
	'jh sql
	x = getrs(sql, BlogTypeArr, BlogTypeC)
	'jh "BlogTypeC = " & BlogTypeC

	sql = "SELECT BlogID, BlogTitle, BlogDate, BlogAbstract, BlogBody, BlogTypeID, ISNULL(BlogTeamAthleteID,0) "
	sql = sql & ", B2CStatusName AS BlogTypeName "
	sql = sql & "FROM B2C_Blog "
	sql = sql & "INNER JOIN (SELECT B2CStatusID, B2CStatusName FROM B2C_Status WHERE B2CTypeID = 75 ) c ON c.B2CStatusID = B2C_Blog.BlogTypeID "
	sql = sql & "WHERE BlogStatusID = 1 AND CAST(BlogDate AS DATE) <= '" & formatdatetime(now,1) & "' "
	sql = sql & "AND (ISNULL(BlogLanguages, '') LIKE '%|" & SelectedLanguageID & "|%' OR ISNULL(BlogLanguages, '') = '') "
	if FilterBy <> "" then
		sql = sql & "AND BlogTypeID = 0" & FilterBy & " "
	end if
	sql = sql & "ORDER BY BlogDate DESC "

	'jhAdmin sql

	x = getrs(sql, BlogArr, BlogC)
	ActualBlogCount = BlogC + 1
	'jh "BlogC = " & BlogC
end if

if BlogAction = "LIST" then

	DisplayMax = 5
	DisplayCount = 0

	StartPoint = cleannum(request("StartPoint"))
	if StartPoint = "" then StartPoint = 0 else StartPoint = cdbl(StartPoint)
	'jh "StartPoint = " & StartPoint
	
	PrevPoint = 0
	if StartPoint > 0 then
		PrevPoint = StartPoint - DisplayMax	
	else
		PrevDisabled = "DISABLED"
	end if
	
	if PrevPoint < 0 then PrevPoint = 0
	
	MaxStartPos = ActualBlogCount - DisplayMax	
	NextPoint = StartPoint + DisplayMax
	
	if NextPoint > MaxStartPos then
		NextDisabled = "DISABLED"
		NextPoint = MaxStartPos
	end if
	
	
	
%>
	<div class="pagination pagination-small">
	  <ul>		
		<li><a class="<%=PrevDisabled%>" href="#" style="cursor:pointer;color:#333 !important;" onclick="getBlogList('<%=PrevPoint%>', '');"><%=lne_B2C("blog_newer")%></a></li>
		<li><a class="<%=NextDisabled%>" href="#" style="cursor:pointer;color:#333 !important;" onclick="getBlogList('<%=NextPoint%>', '');"><%=lne_B2C("blog_older")%></a></li>													
	  </ul>
	</div>					

	<div class="BlogPageHeading spacer10"><%=lne_B2C("blog_latest_news_title")%></div>

<%
jhAdmin "SelectedLanguageID = " & SelectedLanguageID

if FilterBy <> "" then
	for c = 0 to BlogTypeC
		BlogTypeID = BlogTypeArr(0,c)
		if "" & BlogTypeID = "" & FilterBy then
			FilterLabel = lne_B2C("blog_filtered_by")  & " " & lne_B2C("blog_filter_by_list_" & BlogTypeID)
		end if
	next
	FilterClass = "btn-warning"
else
	FilterLabel = lne_B2C("blog_filter_by_button")
	FilterClass = ""
end if
%>	

	<div class="btn-group">	
	  <button class="btn btn-small <%=FilterClass%> dropdown-toggle" data-toggle="dropdown"><%=FilterLabel%> <span class="caret"></span></button>
	  <ul class="dropdown-menu">
	  
<%
for c = 0 to BlogTypeC
	BlogTypeID = BlogTypeArr(0,c)
	BlogTypeName = BlogTypeArr(1,c)
%>
			<li><a onclick="getBlogList(0, '<%=BlogTypeID%>');getBlogItem(0, '<%=BlogTypeID%>');" href="#"><%=lne_B2C("blog_filter_by_list_" & BlogTypeID)%> </a></li>	
<%	
next
%>	  	  
			<li class=divider></li>
			<li><a onclick="getBlogList(0, '-1');getBlogItem(0, '-1');" href="#"><%=lne_B2C("blog_all_news")%></a></li>
	  </ul>
	</div>
	
<%



	BlogOS = ""
	for c = StartPoint to BlogC
		ThisID = BlogArr(0,c)
		ThisTitle = BlogArr(1,c)
		ThisDate = BlogArr(2,c)
		ThisAbstract = BlogArr(3,c)
		ThisBlogTypeID = BlogArr(5,c)
		ThisAthleteID = "" & BlogArr(6,c)
		'jh "ThisAthleteID= " & ThisAthleteID
		
		DisplayCount = DisplayCount + 1
		
		'if ThisID MOD 3 = 0 then IconStr = "blogIcon_1.png" else iconstr = "blogIcon_2.png"
		IconStr = "blogIcon_" & ThisBlogTypeID & ".png"
		IconStr = "generic.png"
		
		if ThisAthleteID <> "0" then	
			icoFN = "blogicon_team_" & ThisAthleteID & ".png"
			icoLoc = server.mappath("/assets/images/blog/") & "\"
			if imageExists(icoLoc, icoFN) then
				IconStr = icoFN
			end if
		end if
		
		%>
		<div style="border-bottom:1px solid #ddd;padding:20px 0 20px 0;">
			<div class=row>				
				<div class=span1>
					<img src="/assets/images/blog/<%=IconStr%>">
				</div>
				<div class=span4>					
					<strong><a onclick="getBlogItem('<%=ThisID%>', '');" href="#"><%=ThisTitle%></a></strong>							
					<div style="color:orange;"><strong><%=formatdatetime(ThisDate,2)%></strong></div>
					<div style="font-size:10pt;"><%=ThisAbstract%></div>
				</div>
			</div>
		</div>
		<%
		
		if DisplayCount >= DisplayMax then exit for
	next

elseif BlogAction = "ITEM" then
	
	BlogID = cleannum(request("BlogID"))
	'jh "BlogID = " & BlogID
	if BlogID = "0" and BlogC >= 0 then
		BlogID = BlogArr(0,0)		
	end if
	'jh "BlogID = " & BlogID
	
	for c = 0 to BlogC
		if "" & BlogArr(0,c) = "" & BlogID then

			BlogFound = true
			
			if c > 0 then
				PrevID = BlogArr(0,c-1)		
				PrevTitle = BlogArr(1,c-1)		
			else
				PrevID = BlogArr(0,c)				
				PrevTitle = "No newer items"
			end if
			
			if c < BlogC then
				NextID = BlogArr(0,c+1)
				NextTitle = BlogArr(1,c+1)
			else
				NextID = BlogArr(0,c)		
				NextTitle = "No older items"
			end if			
		
			'jhInfo "FOUND!"
			BlogTitle = BlogArr(1,c)
			BlogDate = BlogArr(2,c)
			BlogAbstract = BlogArr(3,c)
			BlogBody = BlogArr(4,c)
			SelBlogTypeID = BlogArr(5,c)
			
			BlogTypeName = BlogArr(7,c)
			
			PermaLink = glWebsiteURL & "/blog/?blogid=" & BlogID
			'jh PermaLink
			%>
			
			<div class="pagination pagination-small">
			  <ul>
				<li><a title="<%=PrevTitle%>" style="cursor:pointer;color:#333 !important;" href="#" onclick="getBlogItem('<%=PrevID%>', '');"><%=lne_B2C("blog_previous")%></a></li>													
				<li><a title="<%=NextTitle%>" style="cursor:pointer;color:#333 !important;" href="#" onclick="getBlogItem('<%=NextID%>', '');" ><%=lne_B2C("blog_next")%></a></li>
			  </ul>
			</div>				
					
			<div class=BlogPageHeading><a href="/blog/?BlogID=<%=BlogID%>"><%=BlogTitle%></a></div>
			<div class=spacer10 style="color:orange;"><strong><%=formatdatetime(BlogDate,2)%></strong></div>
			<span class="label label-inverse"><%=lne_B2C("blog_filter_by_list_" & SelBlogTypeID)%></span>
			
			<h5><%=BlogAbstract%></h5>
			
			<div class=BlogBody>
				<%=BlogBody%>
			</div>
			<div class=spacer20></div>
			<div style="padding-top:20px;border-top:1px solid #ddd;">
				<a href="http://twitter.com/share?url=<%=server.urlencode(PermaLink)%>&text=<%=server.urlencode(BlogTitle)%>" target="_blank">
					<img src="/img/social/twitter.png">
				</a>
				<a href="https://www.facebook.com/sharer/sharer.php?u=<%=server.urlencode(PermaLink)%>" target="_blank">
					<img src="/img/social/facebook.png">
				</a>	
				<a href="https://plus.google.com/share?url=<%=server.urlencode(PermaLink)%>" target="_blank">							
					<img src="/img/social/GooglePlus.png">
				</a>			
				
				<a class="pull-right small" title="<%=PermaLink%>" href="<%=PermaLink%>"><i class="icon-share"></i> <%=lne_B2C("blog_perma_link")%></a>
			</div>	
			<%
			
			exit for
		end if
	next
	
	if not BlogFound then
	%>
		<h3>Oops, something went wrong<br><small>That blog entry couldn't be found, choose an item from the list</small></h3>
		<a class=ExternalLink href="?clearfilter=1">View all blog entries</a>
	<%
	end if
elseif BlogAction = "LATEST" then
	'jh "HOME BLOG"
	LatestMax = 3
	
	for c = 0 to BlogC
		if c >= LatestMax then exit for
		ThisBlogID = BlogArr(0,c)
		ThisBlogTitle = BlogArr(1,c)
		ThisBlogDate = BlogArr(2,c)
		ThisBlogAbstract = BlogArr(3,c)
		%>
		<div class=spacer20>
			<div><a class="OrangeLink" href="/blog/?BlogID=<%=ThisBlogID%>"><strong><%=ThisBlogTitle%></strong></a></div>
			<div><strong><%=formatdatetime(ThisBlogDate,2)%></strong></div>						
			<div class=small><%=ThisBlogAbstract%></div>		
		</div>
		<%
	next
	
elseif BlogAction = "LATEST_REVIEWS" then
	'jh "HOME BLOG"
	LatestMax = 3
	
	CutoffReviewDate = dateadd("yyyy", -1, now)
	'jh "CutoffReviewDate = " & CutoffReviewDate

	sql= "exec [spB2CLatestReviews] 0" & SelectedLanguageID & ", '" & SelectedLocation & "', '" & formatdatetime(CutoffReviewDate, 1) & "', 0" & SingleReview

	'jhAdmin sql
	
	'jhstop

	x = getrs(sql, ReviewArr, ReviewC)
	
	for c = 0 to ReviewC
		if c >= LatestMax then exit for
		ThisReviewID = ReviewArr(0,c)
		ThisReviewTitle = ReviewArr(5,c)
		ThisReviewBody = ReviewArr(6,c)
		ThisReviewDate = ReviewArr(4,c)
		
		ThisProductID = ReviewArr(1,c)
		ThisProductCode = ReviewArr(2,c)
		ThisVariant = ReviewArr(9,c)
		ThisThumb = getImageProdThumb(ThisProductCode, ThisVariant)
		
		DispReviewBody = stripHTML(ThisReviewBody)
		DispReviewBody = left(DispReviewBody, 200) & "..."		
		
		%>
		<div class=spacer20>
			<div class=pull-right>
				<a class="OrangeLink" href="/products/?ProductID=<%=ThisProductID%>&stab=2"><img style="height:60px;" src="<%=ThisThumb%>"></a>
			</div>
			<div><a class="OrangeLink" href="/products/?ProductID=<%=ThisProductID%>&stab=2"><strong><%=ThisReviewTitle%></strong></a></div>						
			<div><strong><%=formatdatetime(ThisReviewDate,2)%></strong></div>		
			<div class=small>
				<%=DispReviewBody%>
			</div>
			<div style="clear:both;"></div>
			<hr>
		</div>
		<%
	next	
	
end if


dbClose()

'jh "ENDS"
%>				

</body>
</html>