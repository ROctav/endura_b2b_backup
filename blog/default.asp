<%
PageTitle = "Blog"
BlogPage = true
%>

<!--#include virtual="/assets/includes/header.asp" -->
<style>
	.xBlogBody A:LINK {
		color:orange;
	}
</style>
<%
BlogID = cleannum(request("BlogID"))
if BlogID = "" then BlogID = "0" else setFilterBy = "-1"
if request("clearfilter") <> "" then setFilterBy = "-1"
setfilter = cleannum(request("setfilter"))
if setfilter <> "" then setFilterBy = setfilter
%>

    <div id="" class="">
        <div class="container">
		
			<div class="section_header">
				<h3><%=lne_B2C("blog_page_title")%></h3>
			</div>		
		
			<div class=row>
				<div class=span6>
					<div id=NewsItem>
						
						<div id=AjaxBlogItem>
							...
						</div>
						
					</div>
				</div>
				<div class="span5 offset1">
									
						<div id=AjaxBlogList>
							...
						</div>
									
				</div>
			</div>
		</div>
	</div>

	<div class="spacer40">&nbsp;</div>
	</div>
    

<!--#include virtual="/assets/includes/footer.asp" -->

<script>
	function getBlogList(pStartPoint, pFilterBy) {
		//alert('in')
		//$('#ajax_prod_results').fadeout('fast')
		
		//$('#AjaxBlogList').html('<div style="padding:30px;"><img src="/assets/images/prodbrowser/indicator.gif"> <small>loading..</small></div>');				
		//$('#AjaxBlogList').show();
		
		$.ajax({
			url: "blog_server.asp?BlogAction=LIST&StartPoint=" + pStartPoint + "&FilterBy=" + pFilterBy,
			cache: false
			}).done(
				function( html ) {
				$("#AjaxBlogList").html(html);
				$('#AjaxBlogList').show(100, function() {
					// Animation complete.
				  });					
			});
		
	}
	
	function getBlogItem(pID, pFilterBy) {
		//alert('in')
		//$('#ajax_prod_results').fadeout('fast')
		//$('#AjaxBlogItem').html('<div style="padding:30px;"><img src="/assets/images/prodbrowser/indicator.gif"> <small>loading..</small></div>');
		//$('#AjaxBlogItem').show();					
		$.ajax({
			url: "blog_server.asp?BlogAction=ITEM&BlogID=" + pID + "&FilterBy=" + pFilterBy,
			cache: false
			}).done(
				function( html ) {
				$("#AjaxBlogItem").html(html);
				$('#AjaxBlogItem').show(100, function() {
					// Animation complete.
				  });					
			});
		
	}
			
	getBlogList(0, '<%=setFilterBy%>');
	getBlogItem(<%=BlogID%>, '<%=setFilterBy%>');
	
</script>