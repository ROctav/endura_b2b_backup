<%
PageTitle = "Password reminder"
NoRedirect = true


%>


<!--#include virtual="/assets/includes/header.asp" -->

<%


MaxAttempts = 20
FormAction = request.form("FormAction")
'jh "FormAction = " & FormAction

if FormAction = "SEND_REMINDER" then
	
	UserName = request("UserName")	
	jhAdmin "UserName = " & UserName
	
	if UserName <> "" then
		
		dbConnect()
			
		jhAdmin "GET PW FOR " & UserName	
		
		sql = "SELECT AcCode, ClientPassword, CompanyName, Email "
		sql = sql & "FROM ExchequerCustDetails ecd INNER JOIN END_EbusClients c on ecd.AcCode = c.EbusClientCode "
		sql = sql & "WHERE AcCode = '" & CleanSql(UserName) & "' OR Email = '" & validstr(UserName) & "';"
		jhAdmin sql
				
		UserFound = false
		
		set rs = db.execute(sql)
		
		if not rs.eof then
			'jhAdmin "FOUND CUSTOMER - " & UserName
			AccountUserName = rs("AcCode")
			ThisEmail = trim("" & rs("Email"))
			ThisPassword = rs("ClientPassword")
			UserFound = true
			ErrorMessage = ""
		else
			ErrorMessage = "Account details not found - please re-enter."			
		end if

		rsClose()
		
		if not UserFound then
			
			'''GET ADMIN LOGINS

			sql = "SELECT * FROM END_AdminUsers WHERE AdminStatusID > 0 AND (AdminEmail = '" & CleanSql(UserName) & "') "
			'jh sql
			
			set rs = db.execute(sql)
			
			if not rs.eof then
				'jhInfo "FOUND ADMIN"	
				ThisEmail = trim("" & rs("AdminEmail"))
				AccountUserName = rs("AdminName")
				UserFound = true
				ThisPassword = rs("AdminPassword")
				ErrorMessage = ""
			else
				ErrorMessage = "Account details not found - please re-enter."
			end if
		
			rsClose()
		
		end if
	
		ThisEmail = replace(ThisEmail, ";", ",")
		if ThisEmail <> "" then
			jhAdmin "ThisEmail = " & ThisEmail
			eArr = split(ThisEmail, ",")
			if ubound(eArr) >= 0 then
				ThisEmail = trim("" & eArr(0))
			end if
		end if
		
		if isjh() then
			'jhAdmin "STOP"
			'jhAdmin "ThisEmail = " & ThisEmail
			'jhStop
		end if
		
		if (not validemail(ThisEmail) and UserFound) then
			ThisEmail = glForgottenPasswordNoEmailTo
			PasswordUnable = true
			jhAdmin "ThisEmail = " & ThisEmail
		end if
		
		if isjh() then ThisEmail = "jhutton1@gmail.com"
		
		if UserFound and validemail(ThisEmail) then
		
			if PasswordUnable then
				ThisSubject = "ENDURA B2B - Forgotten password reminder (unable to send email to customer " & AccountUserName & ") "			
				ThisBody = "Customer has requested password details, but no contact email address held in Exchequer." & vbcrlf & vbcrlf & "Password for " & AccountUserName & " to access the Endura B2B system is: " & ThisPassword & " - please contact the customer directly to let them know their password."							
			else
				ThisSubject = "ENDURA B2B - Forgotten password reminder for " & AccountUserName
				ThisBody = "Your password to access the Endura B2B system is: " & ThisPassword
				ValidEmailFound = true
			end if
			
			ThisSender = glForgottenPasswordNoEmailTo
			'ThisEmail = "john@kmvcreative.com"
			
			jhAdmin "SENDING TO ThisEmail = " & ThisEmail
			jhAdmin "ThisSubject = " & ThisSubject
			jhAdmin "ThisBody = " & addbr(ThisBody)
			
			
			SendSuccess = SendMail(ThisSubject, ThisBody, ThisEmail, ThisSender, 0)
			'jhAdmin "SendSuccess = " & SendSuccess
			
			'SendSuccess = true
			if SendSuccess then 			
				ReminderSent = true		
			end if
		end if		
			
	
		dbClose()
	
	end if

end if
%>



        <div class="container">
            
		  <div class="row">      
			<div class="span12 spacer10">				
				<h3>
					<%=lne("label_password_reminder")%>
					<small><%=lne("label_password_reminder_heading")%></small>
				</h3>
			</div>
		</div>
			
			<div class="row">
			
				<div class=span12 style="">
						
						
<%
					if ReminderSent then
						jhInfoNice lne("lne_thank_you"), lne("label_password_reminder_sent")
%>
					
<%					
					else
			

						if ErrorMessage <> "" then
							jhErrorNice lne("label_errors_found"), ErrorMessage
						end if
	%>					

						<p><%=lne("label_password_reminder_intro")%></p>
						
						<form name=loginForm action="/login/password/" method=post>
							<div class="row">
								<div class="span5 box">
									<div class=well>
										<span class="clabel"><small><%=lne("label_account_code")%></small></span><br>
										<input class="input-xlarge" name="UserName" type="text" xplaceholder="Enter your account code here" value="<%=UserName%>"><br>
								
										<div class="spacer10"></div>
								
										<p>
											<input class="btn btn-success" type="submit" value="<%=lne("label_password_reminder_send_btn")%>">
										</p>
										
										<input name="FormAction" class=FormHidden type="hidden" value="SEND_REMINDER">
										
									</div>
								</div>
							</div>
											
						</form>	
<%
					end if
%>						
				</div>	
			</div>
				
			<div class="row">	
				
				<div class="span12">
					
						<p>
							<a class=OrangeLink href="/login/"><%=lne("label_password_reminder_login_return")%></a>
						</p>						
						
				</div>									
				

			
			</div>
			
            <div class="spacer40"></div>
        
		
		
    </div>

<!--#include virtual="/assets/includes/footer.asp" -->