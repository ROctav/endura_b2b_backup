<%
PageTitle = "Login to the Endura B2B system"
NoRedirect = true
NoPromo = true

FormAction = request("FormAction")

if FormAction = "LOGOUT" then
	session("B2B_AcCode") = ""
	session("B2B_AdminUserID") = ""
	session.abandon
	
	if 1 = 1 then
	
		'jh "CLEAR AUTOLOGIN COOKIES!"
		response.cookies("Persistence")("AcCode") = ""
		response.cookies("Persistence")("GUID") = ""

		response.cookies("Persistence")("A_ID") = ""
		response.cookies("Persistence")("A_GUID") = ""
	
	end if	
	
	LoggedOut = true
	
end if

%>


<!--#include virtual="/assets/includes/header.asp" -->

<%


MaxAttempts = 20

'jh "FormAction = " & FormAction

LoginAttempts = cdbl("0" & session("B2C_LoginAttempts"))

AutoLogin = cleannum(request("al"))
if AutoLogin = "1" then 
	AttemptLoginLookup = true	
end if

'jh "LoginAttempts = " & LoginAttempts

if FormAction = "DEALER_LOGIN" and LoginAttempts >= MaxAttempts then
	
	jhErrorNice "Maximum login attempts exceeded", "Contact system support if error persists"

elseif AttemptLoginLookup and LoginAttempts < MaxAttempts then
	

	
	jhx "AL"
	alLoginGUID_Persistent = request("lgp")
	alAcCode = trim("" & request("ac"))
	pwc = trim("" & request("pwc"))
	
	if alAcCode <> "" and alLoginGUID_Persistent <> "" and pwc <> "" then
		dbConnect()
		sql = "SELECT ClientPassword FROM END_EbusClients WHERE EbusClientCode = '" & validstr(alAcCode) & "' AND LoginGUID_Persistent = '" & validstr(alLoginGUID_Persistent) & "' "
		'jhx sql
		set rs = db.execute(sql)
		if not rs.eof then
			'jhx "AL FOUND!"
			UserName = ucase(alAcCode)
			UserPassword = trim("" & rs("ClientPassword"))
			
			thispwc = left(UserPassword,1) & right(UserPassword,1)
			
			if lcase(thispwc) <> lcase(pwc) then
				'jhError "PW ERROR!"
				UserPassword = ""
			end if
			
		end if
		rsClose()
	end if	
	
elseif FormAction = "DEALER_LOGIN" or FormAction = "TRANSFER" then

	UserName = request("UserName")	
	UserPassword = trim("" & request("UserPassword"))
	CheckPersist = request("CheckPersist")	
	
	if CheckPersist <> "" then SetPersist = "1" else SetPersist = ""
	jhAdmin "SetPersist = " & SetPersist
	jhAdmin "glLoginPersistDays = " & glLoginPersistDays	
	
	if UserName = "demo" then
		'UserName = "JIMB01"
		'UserPassword = "3NDURA!"
		'session("nojh") = "1" 
		'session("DEMO_LOGIN") = "1"
	end if
	
	lneError = lne("login_primer")
	
	ErrorsFound = false
	if UserName <> "" then
		
		dbConnect()
		
		LoggedIn = false		
		ErrorMessage = ""
		
		if not LoggedIn then
			'jhAdmin "TRY DEALER ACCOUNTS"
			
			sql = "SELECT ebc.EbusClientCode, ebc.ClientPassword, ebc.EbusClientStatusID, ISNULL(ebc.HidePricesOnStartup,0) AS HidePricesOnStartup, ebc.LoginGUID_PERSISTENT, ecd.AcStatus "
			sql = sql & ", erc.RiderCardID, erc.isActivate, erc.StartDate, erc.FinishDate "
			sql = sql & "FROM ExchequerCustDetails ecd "
			sql = sql & "INNER JOIN END_EbusClients ebc on ecd.AcCode = ebc.EbusClientCode "
			sql = sql & "LEFT JOIN ExchequerRiderCard erc ON erc.AcCode = ecd.AcCode "
			sql = sql & "WHERE EbusClientCode = '" & CleanSql(UserName) & "';"
						
			jhx sql
			'response.end
			
			set rs = db.execute(sql)
						
			if not rs.eof then
				EbusClientStatusID = "" & rs("EbusClientStatusID")
				RiderCardID = "" & rs("RiderCardID")
				
				AcStatus = "" & rs("AcStatus")
				jhx AcStatus
				'jh "EbusClientStatusID = " & EbusClientStatusID
				if EbusClientStatusID = "1" and AcStatus <> "CLOSED" then
					'jhInfo "FOUND"
					CheckPW = trim("" & rs("ClientPassword"))
					'jh "|" & CheckPW & "_" & UserPassword & "|"
					LoginOK = false
					
					if FormAction = "TRANSFER" then
						CheckGUID = request("TransferGUID")
						if CheckGUID = "" & rs("LoginGUID_PERSISTENT") then
							LoginOK = true
						end if					
					elseif CheckPW <> "" and lcase(CheckPW) = lcase(UserPassword) then
						LoginOK = true
					end if
					
					if RiderCardID <> "" then
						ThisRiderCardActivated = rs("isActivate")
						ThisRiderCardStart = rs("StartDate")
						ThisRiderCardFinish = rs("FinishDate")
						
						if ThisRiderCardActivated then
							'jhAdmin "ThisRiderCardStart = " & ThisRiderCardStart
							'jhAdmin datediff("d", ThisRiderCardStart, now)
							
							'jhAdmin "ThisRiderCardFinish = " & ThisRiderCardFinish
							'jhAdmin datediff("d", ThisRiderCardFinish, now)
							
							if datediff("d", ThisRiderCardStart, now) < 0 then
								LoginOK = false
								LoginError = lne("label_rider_card_error_not_started")
							end if
							
							if datediff("d", ThisRiderCardFinish, now) > 0 then
								LoginOK = false
								LoginError = lne("label_rider_card_error_expired")
							end if					
							
						else
							LoginOK = false
							LoginError = lne("label_rider_card_error_not_active")
						end if						
					else						
						LoginError = lne("label_login_error_credentials")
					end if
					
					jhx LoginError
					
					if LoginOK then
						'jhInfo "LOGIN SUCCESSFUL!"
						ThisUserID = ucase(trim("" & rs("EbusClientCode")))
						
						jhInfoNice "Logged in!", "You have successfully logged in [DEALER]"		
						LoggedIn = true
						session("B2B_AcCode") = ThisUserID
						ErrorState = ""
						
						HidePricesOnStartup = "" & rs("HidePricesOnStartup")
						
						if HidePricesOnStartup = "1" then
							session("B2B_SuppressPricing") = "1"
						end if
						
						if SetPersist = "1" then	
							response.cookies("Persistence")("AcCode") = ucase(trim("" & rs("EbusClientCode")))
							response.cookies("Persistence")("GUID") = rs("LoginGUID_PERSISTENT")						
						end if		

						call SetASPNETSession(ThisUserID)	
						
						'jhStop
						
						'jh "CREATE BO JOB!"
						
					else
						'jhError "LOGIN UNSUCCESSFUL!"
						'jhError "LOGIN UNSUCCESSFUL!"
					end if
				else
					LoginError = lne("label_login_error_credentials")
				end if
				
			end if
			
			rsClose()
						
		end if
		
		if not LoggedIn and LoginError = "" then
			'jhAdmin "NOW, TRY OTHER ACCOUNTS"

			sql = "SELECT AdminID, AdminName, AdminPassword, EditLanguage, LoginGUID_PERSISTENT FROM END_AdminUsers WHERE AdminEmail = '" & validstr(UserName) & "'"
			'jh sql
			'jhstop
			
			set rs = db.execute(sql)
			if not rs.eof then
				CheckPW = trim("" & rs("AdminPassword"))
				'jhAdmin "CheckPW = " & CheckPW & ", UserPassword = " & UserPassword
				
				if lcase(CheckPW) = lcase(UserPassword) then
					jhInfoNice "Logged in!", "You have successfully logged in [ADMIN]"		
					ErrorState = ""		
					LoggedIn = true
					AdminLoggedIn = true	
					session("B2B_AdminUserID") = rs("AdminID")
					
					if SetPersist = "1" then	
						response.cookies("Persistence")("A_ID") = rs("AdminID")
						response.cookies("Persistence")("A_GUID") = rs("LoginGUID_PERSISTENT")						
					end if
				else
					LoginError = lne("label_login_error_credentials")
				end if				
			else
				LoginError = lne("label_login_error_credentials")
			end if			
			
			rsClose()
			
		end if
		
		if LoggedIn then
					
			response.cookies("Persistence")("Allow") = SetPersist
			response.cookies("Persistence").expires = dateadd("d", glLoginPersistDays, now)
			
			'call ActivityTrail("LOGIN", "")	
			
			'jhInfo "REDIRECT TO APPROPRIATE LOCATION?"		
			dbClose
			'jh "REDIR"
			
			'jhStop
			
			response.redirect "/"
		
		end if
		
		
	else
		ErrorsFound = true
	end if
	
	dbClose()
	
	if ErrorsFound then
		'jh "ADD LOGIN COUNT"
		LoginAttempts = LoginAttempts + 1 
		session("B2C_LoginAttempts") = LoginAttempts
	end if
else
	CheckPersist = request.cookies("Persistence")("allow")
	
end if
%>



        <div class="container">
            
			<div class="row">      
				<div class="span12 spacer10">				
					<h3>
						<%=PageTitle%>
						<small>Authorised dealers only</small>
					</h3>
				</div>
			</div>
			
			<div class="row">
			
				<div class=span12 style="">
							
<%	
					BigLanguageNotification = request.cookies("BigLanguageNotification")
					
					if BigLanguageNotification = "" or request("ShowBigLanguageNotification") = "1" then
						'jhInfo "Language selector!"
						response.write DrawBigLanguageNotification	
						
					end if


					if LoggedOut then
						jhInfoNice "Logout", "You have been logged out successfully"
					elseif LoginError <> "" then
						jhErrorNice lne("label_errors_found"), LoginError									
					elseif ErrorState <> "" then
						jhErrorNice "Login failed", "You have entered an invalid username or password "
					end if
%>					

					
					
						<div class="row">
							<div class="span6 xbox">
								<form name=loginForm style="background:#f0f0f0;border:1px solid silver;padding:10px;border-radius:5px;" action="/login/" method=post>
									<div class=xwell>
										<span class="clabel"><small>Account code</small></span><br>
										<input class="input-xlarge" name="UserName" type="text" xplaceholder="Enter your account code here" value="<%=UserName%>"><br>
										<span class="clabel"><small>Password</small></span><br>
										<input class="input-xlarge" name="UserPassword" type="password" xplaceholder="Password" value="<%=UserPassword%>"><br>	
								
										<div class=spacer10></div>
										<input <%if CheckPersist <> "" or 1 = 1 then response.write "CHECKED"%> type=checkbox id="CheckPersist" name="CheckPersist"> <label for="CheckPersist"><small>Keep me logged in?</small></label>
										<div class=spacer10></div>
										<p>
											<input class="btn btn-success" type="submit" value="LOGIN">
										</p>
										
										<input name="FormAction" class=FormHidden type="hidden" value="DEALER_LOGIN">
										
									</div>
								</form>
							</div>
						</div>
										
					
				</div>	
			</div>
				
			<div class="row">	
				
				<div class="span12">
					
						<p>
							Forgotten your password? <a class=OrangeLink href="/login/password/">Click here to receive a reminder</a>
						</p>
						<p>
							Already an account holder with Endura, but not yet registered? <a class=OrangeLink href="mailto:info@endura.co.uk">Click here to request an account login</a>
						</p>
						<p>
							<a href="http://www.endurasport.com">Go to the Endura public website</a>
						</p>
						
				</div>									
				

			
			</div>
			
            <div class="spacer40">
			
               
            </div>
        
		
		
    </div>


<!--#include virtual="/assets/includes/footer.asp" -->