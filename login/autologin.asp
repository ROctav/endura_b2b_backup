<!--#include virtual="/assets/includes/global_functions.asp" -->

<%
IsByPassSuccess = False

dbConnect()

If (Request.QueryString("bp") <> "") Then

	BypassGUID = Request.QueryString("bp")

	sql = "select EbusClientCode FROM end_ebusclients WHERE BypassGUID = '" & BypassGUID & "'"
	set rs = db.execute(sql)
	if not (rs.eof AND rs.bof) then
		sql = "SELECT EbusClientCode, ClientPassword, cuCompany, cuCostCentre, cuCurrency, cuVatCode, cuDiscBand, dbo.TwoVals(cuDiscount_1,cuDiscount_2) AS DiscRate, SagePayUser, EquipeDealer, cuDepartment, EbusClientStatusID, LoginGUID_PERSISTENT "
		sql = sql & "FROM Customers INNER JOIN END_EbusClients on Customers.idxAcCode = END_EbusClients.EbusClientCode WHERE BypassGUID = '" & BypassGUID & "'"

		set rs1 = db.execute(sql)
		if not (rs1.eof and rs1.bof) then
			if rs1("EbusClientStatusID") = 1 then
				'SessionInit trim(cstr("" & rs1("EbusClientCode"))), trim(cstr("" & rs1("cuCompany"))), trim("" & rnull(rs1("cuDiscBand"))), trim("" & rnull(rs1("cuCostCentre"))), trim("" & rnull(rs1("cuDepartment"))), "" & rs1("cuCurrency"), "" & rs1("cuVatCode"), "" & rs1("SagePayUser"), "" & rs1("DiscRate"), "" & rs1("EquipeDealer")
                
                ' DL NEW
                ThisUserID = ucase(trim("" & rs("EbusClientCode")))
                session("B2B_AcCode") = ThisUserID
                response.cookies("Persistence")("AcCode") = ucase(trim("" & rs("EbusClientCode")))
				response.cookies("Persistence")("GUID") = rs1("LoginGUID_PERSISTENT")
                call SetASPNETSession(ThisUserID)

                sql = "UPDATE END_EbusClients SET BypassGUID = '' WHERE EbusClientCode = '" & ThisUserID & "'"
                db.execute(sql)
                ' END DL NEW

				IsByPassSuccess = True
			end if
		end if	
		rs1.Close
	End If

	rs.Close
End If

dbClose()

If IsByPassSuccess = True Then
	RedirPage = Request.QueryString("page") 

	If (RedirPage = "bosw") Then
		RedirURL = "https://b2b.endurasport.com/utilities/account/backorders/"
	ElseIf (RedirPage = "forem") Then
		RedirURL = "https://b2b.endurasport.com/utilities/account/reports/forward-orders/"
	Else
        RedirURL = "https://b2b.endurasport.com/utilities/account/pre-alloc/"
	End If

	Response.Redirect RedirURL
	Response.End
Else
	Response.Redirect "https://b2b.endurasport.com/login/"
	Response.End
End If

%>

