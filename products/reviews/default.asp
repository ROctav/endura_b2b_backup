<%
PageTitle = "Latest reviews"
%>

<!--#include virtual="/assets/includes/header.asp" -->

<style>
	.ReviewContainer A {
		color:red;
		[target=_blank];
	}
</style>

<%
dbConnect()

sql = "SELECT RangeID, RangeName, Range1Liner, RangeDescription, RangeImage FROM SML_Ranges ORDER BY RangeName "
'jh sql
x = getrs(sql, RangeArr, RangeC)
'jh "RangeC = " & RangeC

%>

    <div id="features" class="features_page">
        <div class="container">
            <!-- Feature Wrapper -->

			  <div class="feature_wrapper option2">

				<div class=pull-right>
					<a title="Latest reviews RSS feed" href="/rss/endura_reviews.xml" target="_blank">							
						<img src="/img/social/feed.png">
					</a>		
				</div>			  
			
                <div class="section_header">
                    <h3>Latest reviews</h3>
                </div>
				

				
<%

function PageNav_REVIEWS()

	if PageCount > 1 then
	%>

	<div class="pagination pagination-small">	
	  <ul>
		<li><a href="#" style="color:orange;background:#fff;">Page <%=CurrentPage%> of <%=PageCount%></a></li>
		<li><a style="cursor:pointer;color:#333 !important;" href="/products/reviews/?StartPoint=<%=PrevSP%>"><%=lne_b2c("search_prev")%></a></li>							
		<%
		for c = 1 to PageCount	
			ThisPageNum = c
			SetStartPoint = (c-1) * PageMax
			if c = CurrentPage then ActiveStr = "active" else ActiveStr = ""
		%>
			<li class="<%=activestr%>"><a style="cursor:pointer;color:#333 !important;" href="/products/reviews/?StartPoint=<%=SetStartPoint%>"><%=ThisPageNum%></a></li>
		<%
		next
		%>							
		<li><a style="cursor:pointer;color:#333 !important;" href="/products/reviews/?StartPoint=<%=NextSP%>"><%=lne_b2c("search_next")%></a></li>
	  </ul>
	</div>
	<%
	end if
end function

CutoffReviewDate = dateadd("yyyy", -1, now)

SingleReview = cleannum(request("id"))
'jhAdmin "SingleReview = " & SingleReview

sql= "exec [spB2CLatestReviews] 0" & SelectedLanguageID & ", '" & SelectedLocation & "', '" & formatdatetime(CutoffReviewDate, 1) & "', 0" & SingleReview

jhAdmin sql

x = getrs(sql, ReviewArr, ReviewC)

if SingleReview <> "" and ReviewC > 0 then ReviewC = 0

ActualReviewCount = ReviewC + 1

if request("gc") = "1" then
	StartPoint = cleannum(request.cookies("reviews")("StartPoint"))
else
	StartPoint = cleannum(request("StartPoint"))
end if

if StartPoint = "" then StartPoint = 0 else StartPoint = cdbl(StartPoint)
'jh "StartPoint = " & StartPoint

response.cookies("reviews")("StartPoint") = StartPoint
response.cookies("reviews").expires = dateadd("d", 2, now)

PageMax = 10
DisplayCount = 0

PageCount = 20

PageCount = ActualReviewCount \ PageMax

PageRem = ActualReviewCount MOD PageMax
'jh "PageRem = " & PageRem
if PageRem > 0 then PageCount = PageCount + 1
'jh "PageCount = " & PageCount

PrevSP = StartPoint - PageMax
NextSP = StartPoint + PageMax

CurrentPage = (StartPoint + PageMax) / PageMax

if PrevSP < 0 then PrevSP = 0
MaxAllowedSP = (PageCount * PageMax) - PageMax
'jh "MaxAllowedSP = " & MaxAllowedSP
if NextSP >= MaxAllowedSP then NextSP = MaxAllowedSP


response.write PageNav_REVIEWS()

for c = StartPoint to ReviewC 
		
				DisplayCount = DisplayCount + 1
				FullReviewURL = ""
			
				ThisProductID = ReviewArr(1,c)
				ThisReviewDate = ReviewArr(4,c)	
				if isdate(ThisReviewDate) then ThisReviewDate = formatdatetime(ThisReviewDate,2)
				
				ReviewID = ReviewArr(0,c)	
				jhAdmin "ReviewID = " & ReviewID
				
				ThisProductName = ReviewArr(3,c)				
				ThisReviewTitle = ReviewArr(5,c)
				ThisReviewBody = ReviewArr(6,c)
				ThisProductCode = ReviewArr(2,c)
				ThisLegacyFile = ReviewArr(8,c)
				DefVariant = "" & ReviewArr(9,c)
				ThisScore = "" & ReviewArr(7,c)
				
				ThisPrimaryLanguageID = "" & ReviewArr(12,c)
				jhAdmin "ThisPrimaryLanguageID = " & ThisPrimaryLanguageID
				
				ExternalURL = ReviewArr(15,c)
				jhAdmin "ExternalURL = " & ExternalURL
				
				if SelectedLanguageID <> "0" then
					'jhInfo "GET TRANSLATIONS"
					sql = "SELECT * FROM SML_ProductTranslations WHERE ProductID = 0" & ThisProductID & " AND LanguageID = 0" & SelectedLanguageID & ""
					jhAdmin sql
					set rs = db.execute(sql)
					if not rs.eof then
						ProductName_TRANS = trim("" & rs("ProductName"))
						'OneLiner_TRANS = trim("" & rs("OneLiner"))
						'ShortDescription_TRANS = trim("" & rs("ShortDescription"))
						'BulletPoints_TRANS = trim("" & rs("BulletPoints"))
						'L'ongDescription_TRANS = trim("" & rs("LongDescription"))
						
						
						if ProductName_TRANS <> "" then ThisProductName = ProductName_TRANS
						'if OneLiner_TRANS <> "" then OneLiner = OneLiner_TRANS
						'if ShortDescription_TRANS <> "" then ShortDescription = ShortDescription_TRANS
						'if BulletPoints_TRANS <> "" then BulletPoints = BulletPoints_TRANS
						'if LongDescription_TRANS <> "" then LongDescription = LongDescription_TRANS
						
					end if
					rsClose()
				end if				
				
				ThisLogoFile = "" & ReviewArr(10,c)
				ThisDownloadFile = "" & ReviewArr(11,c)
				
				ThisLogoURL = ""
				if ThisLogoFile <> "" then
					ThisLogoURL = getReviewLogoFile(ThisProductCode, ThisLogoFile, LogoURL)
				end if
				
				ThisDownloadURL = ""				
				if ExternalURL <> "" then
					ThisDownloadURL = ExternalURL
					DownloadLinkStr = ExternalURL
				elseif ThisDownloadFile <> "" then
					DownloadLinkStr = lne_b2c("latest_reviews_download_full")
					'jh "ThisDownloadFile = " & ThisDownloadFile
					ThisDownloadURL = getReviewDownload(ThisProductCode, ThisDownloadFile, DownloadURL)
					'jh "ThisDownloadURL = " & ThisDownloadURL
				end if				
				
				if ThisScore = "0/0" then ThisScore = ""
				
				ProdImage = getImageDefault(ThisProductCode, DefVariant)
				
				ShowThis = true
				
				if cdbl("0" & ThisPrimaryLanguageID) <> cdbl("0" & SelectedLanguageID) and ThisPrimaryLanguageID <> "" then
					jhAdmin "HIDE"
					Showthis = false
				end if				
				
				if ShowThis then 
				'jh "ProdImage = " & ProdImage
					jhAdmin "REVIEW_" & ReviewID
		%>
                    <!-- Review -->
                   	
					<div id="REVIEW_<%=ReviewID%>" class="row spacer40">
								
						<div class="span6">    						
							<div class="text spacer20" style="border:0px solid red;border:0px solid #ddd;border-radius:5px;">	

								
								<div style="xfont-size:9pt;">
									
									<h4>
										 <a href="/products/?ProductID=<%=ThisProductID%>&sTab=2&BackTo=REVIEWS"><%=ThisProductName%></a>								 
									</h4>							
									<div class=ReviewerName><%=ThisReviewTitle%></div>
									<div class="spacer20"><%=ThisReviewDate%></div>
									<%
									if ThisScore <> "" then
									%>
										<div class="ReviewerScore spacer20"><span class="label label-large label-warning">Score: <%=ThisScore%></span></div>
									<%	
									end if
									%>								
									
									<div class=ReviewContainer><%=ThisReviewBody%></div>	
									
									
	<%
										
										if ThisDownloadURL  <> "" then											
											%>
											<div><a class="ExternalURL" target="_BLANK" href="<%=ThisDownloadURL%>"><i class="icon-share"></i> <%=DownloadLinkStr%></a></div>
											<%
										elseif ThisLegacyFile <> "" then
											ThisLegacyURL = glLegacyWebsiteURL & "/GetFile.aspx?itemid=" & ThisLegacyFile
										%>
											<div><a class="ExternalURL" target="_BLANK" href="<%=ThisLegacyURL%>"><i class="icon-share"></i> <%=DownloadLinkStr%></a></div>
										<%
										end if	
	%>								
									<div class=spacer10></div>
									<div><a class="ExternalURL" href="/products/?ProductID=<%=ThisProductID%>&sTab=2&BackTo=REVIEWS"><i class="icon-chevron-right"></i> <%=lne_b2c("reviews_product_detail_link")%></a></div>
								</div>
							
							</div>
						</div>		

						<div class=span6>
								<div class="" style="border:0px solid red;width:400px;height:400px;background:url(<%=ProdImage%>);background-repeat:no-repeat;background-position:middle;font-size:9pt;">
									<a href="/products/?ProductID=<%=ThisProductID%>&sTab=2&BackTo=REVIEWS">
										<%
										if ThisLogoURL <> "" then
										%>
											<img style="width:150px;padding:250px 0 0 250px;" class="img-rounded" src="<%=ThisLogoURL%>" />
										<%
										else
										%>
											<div style="width:150px;padding:250px 0 0 250px;"></div>
										<%
										end if
										%>
									</a>						
								</div>
						</div>		
						
					</div>				
					<hr>
		
		<%
				end if
		
	if DisplayCount >= PageMax then exit for
next

response.write PageNav_REVIEWS()

%>				
		
		<%
		if SingleReview <> "" then
			if ReviewC = -1 then
				jhErrorNice "Review not found", "Sorry, we could not find that review"
			end if
		%>
			<div class=spacer20></div>
			<div>
				<a class="btn btn-warning btn-small" href="/products/reviews/"><%=lne_b2c("reviews_show_all")%></a> 
			</div>
		<%
		end if
		%>
		
		<div class="spacer40">&nbsp;</div>
		
		
        </div>
    </div>

<!--#include virtual="/assets/includes/footer.asp" -->