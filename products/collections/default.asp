<%
PageTitle = "Our product collections"
%>

<!--#include virtual="/assets/includes/header.asp" -->

<%
dbConnect()

sql = "SELECT RangeID, RangeName, Range1Liner, RangeDescription, RangeImage FROM SML_Ranges "
if session("B2C_AdminUserID") = "" then
	sql = sql & "WHERE RangeStatusID = 1 "
end if
sql = sql & "ORDER BY RangeZOrder, RangeName "

'jh sql
x = getrs(sql, RangeArr, RangeC)
'jh "RangeC = " & RangeC

%>

    <div id="features" class="features_page">
        <div class="container">
            <!-- Feature Wrapper -->

			  <div class="feature_wrapper option2">

			
                <div class="section_header">
                    <h3><%=lne("range_browser_title")%></h3>
                </div>
				
<%
for c = 0 to RangeC step 2
%>
	<div class="row">

		<%
		for y = c to c + 1
			if y <= RangeC then
				ThisRangeID = RangeArr(0,y)
				ThisRangeName = RangeArr(1,y)
				ThisRange1Liner = "" & RangeArr(2,y)
				ThisRangeDescription = RangeArr(3,y)
				ThisRangeImage = RangeArr(4,y)
				'jh "ThisRangeImage = " & ThisRangeImage
				
		%>
                    <!-- Feature -->
                    <div class="span5 feature">
                        <div class="box">
                            <a href="/products/browse/?showrange=<%=ThisRangeID%>">
								<img class="img-rounded" src="/assets/images/ranges/<%=ThisRangeImage%>" />
							</a>
                        </div>
                        <div class="text spacer40">
                            <h3>
								<a href="/products/browse/?showrange=<%=ThisRangeID%>"><%=ThisRangeName%></a>
								<br>
								<small><%=GetLabel("RANGE_1LINER_" & ThisRangeID)%></small>
							</h3>
                            <p style="font-size:10pt;">				
								<%=GetContent("RANGE_INTRO_SHORT_" & ThisRangeID)%>
							</p>
                        </div>
                    </div>		
					<div class="span1">
					</div>
		
		<%
			end if
		next
		%>
	
	</div>
<%
next
%>				
		
		<div class="spacer40">&nbsp;</div>
        </div>
    </div>

<!--#include virtual="/assets/includes/footer.asp" -->