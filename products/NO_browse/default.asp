
<%
PageTitle = "Products"

Response.Expires = 0
Response.Expiresabsolute = dateadd("d", -2, now)
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache, no-store"
%>

<!--#include virtual="/assets/includes/header.asp" -->

<%
jhAdmin "now = " & now

dbConnect()

sql = "SELECT CategoryID, CategoryName, "
sql = sql & "(SELECT TOP 1 TranslatedValue FROM SML_ListsItems_TRANSLATED slit WHERE IDValue = CategoryID AND slit.LanguageID = 0" & SelectedLanguageID & " AND slit.ListTypeID = 20 ) AS CategoryName_TRANSLATED "
sql = sql & "FROM SML_Categories "
sql = sql & "WHERE B2CVisibleCategory = 1 "
sql = sql & "ORDER BY CategoryZOrder "

'jh sql 

x = getrs(sql, EndUseArr, EndUseC)

%>

<style>
	.RangeIntro {
		border-top:0px solid orange;
		border-bottom:0px solid orange;
		xpadding:10px;
		border-radius:5px;
		margin-left:20px;
		padding:10px 10px 5px 10px;
		background:#444;
		color:white;
		font-size:9pt;
	}
	
	A.EndUseLink_ACTIVE {
		color:orange !important;
	}
	
	A.ProdCatLink_ACTIVE {
		color:orange !important;
	}	
	
	A.EndUseLink {
				
	}		
	
	A.ProdCatLink {
				
	}	
	
	.VariantTile {
	
		cursor:pointer;
		margin:0px;
		border:1px solid #333;
		border-radius:0px;
		display:inline-block;	
	
	}
	
	.VariantTile, .VariantTileImage {
		height:5px;
		width:20px;
	}
	
	@media (max-width: 960px) { 
		.VariantTile {
			border:1px solid #333;
			border-radius:2px;
		}
		.VariantTile, .VariantTileImage {
			height:20px;
			width:20px;
		}
		
	}		
	
	.SelectedCollection {
		color:white;
		font-weight:bold;
	}
	
</style>


<%
function linkActive(p)

end function

EndUseID = cleannum(Request("EndUseID"))
if EndUseID = "" then EndUseID = "0"

searchVal = request("searchVal")
searchVal_HEADER = request("searchVal_HEADER")
if searchVal_HEADER <> "" then 
	searchVal = searchVal_HEADER
	response.cookies("CurrentSearch") = searchVal_HEADER
	response.cookies("CurrentSearch").expires = dateadd("d", 2, now)
end if

if EnduseID = "0" and SearchVal = "" then AllLinkStr = "EndUseLink_ACTIVE"
%>

    <div class="container">
	
		<div class=row>		
			<div class=span12>
			
			<!--
				<div class="pull-right">
					
					  <input id=ProdSearchBox placeholder="<%=getLabel_NOEDIT("search_for")%>" type="text" class="input-small search-query" style="border-radius:5px;">
					  <button onclick="getResults('SEARCH', 0, document.getElementById('ProdSearchBox').value, 0)" type="button" class="btn btn-small btn-warning">go</button>
					
				</div>
			-->
				<div class="navbar navbar-simple">
							
					<ul class="nav">
					<%
					'if session("header_search") <> "" then
					%>
						<li><a class="EndUseLink <%=AllLinkStr%>" onclick="setActiveTopLink(this);getBrowser(0, '');getResults('ENDUSE', 0, 'XXX', 0);" href="#"><%=lne("ALL PRODUCTS")%></a></li>	
					<%					
					'end if
					
					for c = 0 to EndUseC
						ThisEndUseID = EndUseArr(0,c)
						ThisEndUseName = EndUseArr(1,c)
						if SelectedLanguageID <> "0" and "" & EndUseArr(2,c) <> "" then
							ThisEndUseName = EndUseArr(2,c)							
						end if
						DispEndUseName = replace(ThisEndUseName, "'", "")
						if "" & EndUseID = "" & ThisEndUseID then EndUseClassName = "EndUseLink EndUseLink_ACTIVE" else EndUseClassName = "EndUseLink"
					%>
						<li><a id="END_USE_LINK_<%=ThisEndUseID%>" class="<%=EndUseClassName%>" onclick="setActiveTopLink(this);getBrowser(<%=ThisEndUseID%>, '<%=DispEndUseName%>');getResults('ENDUSE', <%=ThisEndUseID%>, 'XXX', 0);" href="#"><%=ThisEndUseName%></a></li>	
					<%
					next
					%>					
					</ul>
					
				</div>
	
			</div>
		</div>		
	
		<div id=ajax_browser_results style="display:none;">
		
		</div>			
		
	</div>
	
	

	<div class="container" style="min-height:600px;">

		<div class=row>
			<div id=ajax_prod_results style="display:none;">				
			</div>				
		</div>				
	</div>
	

	
	<input type=hidden class=FormHidden id="CurrentBrowserSelection" value="999">
	

<%
ProductsLastVisited = request.cookies("ProductsLastVisited")
%>	
	
			
<!--#include virtual="/assets/includes/footer.asp" -->


<%
'jhAdmin "CurrentBrowserSelection = " & CurrentBrowserSelection

jhAdmin "ProductsLastVisited = " & ProductsLastVisited


'exec [spSMLProdResults_TYPEORDER] 05, 'PRODTYPE', '16', 00, 'UK'

if request("gc") = "1" or ProductsLastVisited = "1" then
	jhAdmin "GET COOKIES (ProductsLastVisited)"
	EndUseID = request.cookies("ProdBrowser")("EndUseID")
	FilterType = request.cookies("ProdBrowser")("FilterType")
	FilterVal = request.cookies("ProdBrowser")("FilterVal")
	StartPoint = cleannum(request.cookies("ProdBrowser")("StartPoint"))

	jhAdmin "EndUseID PROD LISTING = " & EndUseID
	jhAdmin "FilterVal PROD LISTING = " & FilterVal
	
	if StartPoint = "" then StartPoint = 0
elseif request("showrange") <> "" then
	FilterType = "RANGE"
	FilterVal = cleannum(request("showrange"))
	EndUseID = "0"
	StartPoint = 0
	ShowRangeID = FilterVal
elseif request("showtype") <> "" then
	FilterType = "PRODTYPE"
	FilterVal = cleannum(request("showtype"))
	EndUseID = "0"
	StartPoint = 0	
else
	StartPoint = 0
	FilterType = "ENDUSE"
	FilterVal = "XXX"
end if	

jhAdmin "EndUseID = " & EndUseID
jhAdmin "FilterType = " & FilterType
jhAdmin "FilterVal = " & FilterVal
jhAdmin "StartPoint = " & StartPoint


'jh "searchVal_HEADER = " & searchVal_HEADER

'jh "searchVal = " & searchVal
if searchVal <> "" then
	FilterType = "SEARCH"
	FilterVal = searchVal
	EndUseID = 0
end if
%>


<script>

<%
if EndUseID = "0" and SearchVal = "" then
	'jhAdmin "HERE!%"
%>

	getBrowser(0, 'ALL');

<%
end if
%>
	
getResults('<%=FilterType%>', <%=EndUseID%>, '<%=FilterVal%>', <%=StartPoint%>);


function getBrowser(pid, pName) {
	
	//alert(pid)
	var csel = document.getElementById('CurrentBrowserSelection').value
	if (csel == pid) {		
		$('#ajax_browser_results').hide('slow', function() {
					document.getElementById('CurrentBrowserSelection').value = '999';
				 });		
	} else {
	
		//alert("product_server.asp?EnduseName=" + pName + "&EndUseID=" + pid);
	
		//$('#ajax_browser_results').hide('slow')
		
		//$('#ajax_browser_results').html('<img src="/assets/images/prodbrowser/indicator.gif"> <small>getting data..</small>');
		//$('#ajax_browser_results').show();			
		
		$.ajax({
			url: "/products/product_server.asp?EnduseName=" + pName + "&EndUseID=" + pid + "&ShowRangeID=<%=ShowRangeID%>",
			cache: false
			}).done(
				function( html ) {
				$("#ajax_browser_results").html(html);
				$('#ajax_browser_results').show('slow', function() {
					// Animation complete.
				  });		
				document.getElementById('CurrentBrowserSelection').value = pid;
			});
			
	}		

	
}


function jha(pObj) {

<%
if session("B2C_AdminUserID") = "1" then
%>
	alert('jha - ' + pObj.href);
<%
end if
%>
	
}

function setActiveTopLink(pObj) {

	$(".EndUseLink").removeClass( "EndUseLink_ACTIVE" );
	$(pObj).addClass( "EndUseLink_ACTIVE" );
}

function setActiveCatLink(pObj) {
	$(".ProdCatLink").removeClass( "ProdCatLink_ACTIVE" );
	$(pObj).addClass( "ProdCatLink_ACTIVE" );
		
}

function clearActiveCat() {
	$(".ProdCatLink").removeClass( "ProdCatLink_ACTIVE" );
}

function getResults(pType, pCategory, pid, pstartpoint) {
	//alert('in')
	//$('#ajax_prod_results').fadeout('fast')
	
	$('#ajax_prod_results').html('<div style="padding:30px;"><img src="/assets/images/prodbrowser/indicator.gif"> <small>getting products..</small></div>');
	$('#ajax_prod_results').show();	
	
	$.ajax({
		
	scriptCharset: "ISO-8859-1" ,
	contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",	
		
		url: "/products/product_server.asp?CallType=PRODRESULTS&EndUseID=" + pCategory + '&FilterType=' + pType + '&FilterVal=' + pid + '&StartPoint=' + pstartpoint,
		cache: false
		}).done(
			function( html ) {
			$("#ajax_prod_results").html(html);
			$('#ajax_prod_results').show(100, function() {
				// Animation complete.
			  });					
		});
	
}

function swapBrowserImage(pid, psrc) {
	//alert('swapBrowserImage');
	document.getElementById('ProdBrowserThumb_' + pid).src = psrc;
}

$("#ProdSearchBox").keyup(function (e) {
	//alert(e.keyCode);
    if (e.keyCode == 13) {
		//alert('enter pressed');
		//return false;
        getResults('SEARCH', 0, this.value, 0)
    }
});

function setSelCollection(pVal) {	
	$('#RangeSelectorButton span:first').text(pVal);
}


<%
if EndUseID <> "0"  and EndUseID <> ""  and isjh() then
%>
	//alert('<%=EndUseID%>');
	setActiveTopLink($("#END_USE_LINK_<%=EndUseID%>"))
<%
end if
%>

</script>

