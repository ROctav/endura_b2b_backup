
<%
Response.Expires = 0
Response.Expiresabsolute = Now() - 1442
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"


ThisUserID = session("ENDURA_UserID")

PageTitle = "Clearance List"

%>


<!--#include virtual="/assets/includes/header.asp" -->


<%
dbConnect()

'jhAdmin "BEGIN!"

HideOOS = cleannum(request("HideOOS"))
NewlyAdded = cleannum(request("NewlyAdded"))

FormAction = request("FormAction")

if FormAction = "CLEARANCE_ADD" then
	
	'jhInfoNice "ADDING TO BASKET!", "Adding clearance items to the basket"
	
	SKUCount = cdbl("0" & request("SKUCount"))
	
	for c = 0 to SKUCount 
		ThisSKU = request("s_" & c)
		ThisQty = cleannum(request("q_" & c))
		ThisWas = cleannum(request("w_" & c))
		
		if ThisQty = "" and ThisWas <> "" then ThisQty = "0"
		
		if ThisQty <> "" and ThisSKU <> "" then
			if cdbl(ThisQty) < 0 then ThisQty = 0
			
			'jhInfo "PROCESS THIS - " & ThisSKU  & " (" & ThisQty & ")"			
			sql = "exec [spB2BBasketAdd] '" & validstr(ThisSKU) & "', 0" & ThisQty & ", '" & AcDetails.AcCode & "', ''"				
			'jhAdmin sql
			db.execute(sql)		
			SomeAdded = true
		end if
		
	next
	
	if SomeAdded then ClearanceAdded = true
	
end if


sql = "SELECT DISTINCT sz.SizeID, sz.SizeName, sz.SizeType, sz.SizeZorder, 1 AS ShowSize FROM SML_SizeList sz ORDER BY SizeType, SizeZorder "

x = getrs(sql, SizeArr, SizeC)	


sql = "exec spB2BClearanceList '" & acDetails.AcCode & "', '" & AcDetails.CostBand & "', 0" & AcDetails.SelectedLanguageID & ""
jhAdmin sql
x = getrs(sql, ProdArr, ProdC)
'jhAdmin "ProdC = " & ProdC



%>

<link rel="stylesheet" href="css/index.css" type="text/css" media="screen" />

	<div class="container">      

		 <div class="row">      
			<div class="span12 spacer10">
		
				<h3><%=lne("page_title_clearance")%><br><small><%=lne("page_title_clearance_subtitle")%></small></h3>	
				
				<%
				'jhAdmin "DRAW LIST!"
								
				set ls = new strconcatstream
				
				ls.add "<table class=ProductDetailSKUGrid border=1>"
								
				LastSKU = ""
				LastVariant = ""				
				LastSKU = ""
				
				WasLabel = lne("label_clearance_was")
				NowLabel = lne("label_clearance_now")
				BuyLabel = lne("label_clearance_buy")
				EachLabel = lne("label_clearance_each")
				NewlyAddedLabel = lne("label_clearance_newly_added")
				SoldOutLabel = lne("label_clearance_sold_out")
				'ProdC = 194
				
				'jhAdmin "ProdC = " & ProdC
				
				SKUCount = 0
				
				XXSFound = 0
				XXXLFound = 0
				XXXXLFound = 0
				
				ThisSizeID = SizeArr(0,z)
				ThisSizeName = ""
				
				if 1 = 1 then
					''' HIDE UNNEEDED SIZES FROM THE MAIN LIST
				
					for c = 0 to ProdC
						if ProdArr(11,c) = "XXXXL" then
							XXXXLFound = 1
							XXXLFound = 1
						end if
						if ProdArr(11,c) = "XXXL" then
							XXXLFound = 1						
						end if					
						if ProdArr(11,c) = "XXS" then
							XXSFound = 1						
						end if							
					next
					
					for c = 0 to sizeC
						ThisSizeName = SizeArr(1,c)
						if ThisSizeName = "XXXXL" and not XXXXLFound then SizeArr(4,c) = 0
						if ThisSizeName = "XXXL" and not XXXLFound then SizeArr(4,c) = 0
						if ThisSizeName = "XXS" and not XXSFound then SizeArr(4,c) = 0
					next
					
					jhAdmin "XXXXLFound = " & XXXXLFound
					jhAdmin "XXXLFound = " & XXXLFound
					jhAdmin "XXSFound = " & XXSFound
					
				end if
				
				for c = 0 to ProdC
				
					ThisProductID = ProdArr(0,c)
					ThisProductCode = ProdArr(1,c)
					
					ThisProductName = ""
					
					if AcDetails.SelectedLanguageID <> 0 then
						ThisProductName = trim("" & ProdArr(37,c))
					end if
					
					if ThisProductName = "" then
						ThisProductName = ProdArr(2,c)
					end if
					
					
					ThisColor = ProdArr(4,c)
					ThisVariant = ProdArr(5,c)
					VariantWebColor = ProdArr(6,c)
					VariantColorCode = ProdArr(8,c)
					
					ColorBar = getProductColorBar(VariantColorCode)
					ColorStr = ""
					if ColorBar <> "" then
						ColorStr = "<span class=badge style=""background-image:url(" & colorbar & ");"">&nbsp;</span>&nbsp;"
					end if
					
					
					
					imgURL = getImageDefaultCDN(ThisProductCode, ThisVariant)
					
					if ThisVariant <> LastVariant then
											
						ThisDeferred = false					
											
						ThisSizeType = ProdArr(12,c)
					
						LastVariant = ThisVariant
						
						'jh "GET SINGLE SKU - " & sss
						
						CheckSKU = ProdArr(10,c)
						BulkStr = ""
						for y = c to ProdC
							
							
							if CheckSKU = ProdArr(10, y) then
							
								WasPrice = ProdArr(33, y)
								'jh "SKU = " & ProdArr(10,y)
								BulkQty = cleannum(ProdArr(34, y))
								BulkPrice = cleannum(ProdArr(35, y))
								if BulkQty <> "" then
									if BulkQty = "999" then DispQty = "ALL" else DispQty = BulkQty & "+"
									BulkStr = BulkStr & "<div class=""ClearanceBulkBuy spacer5 "">" & BuyLabel & " " & DispQty & " @ " & dispCurrency(BulkPrice) & " " & EachLabel & "</div>"
																		
								end if
							else
								exit for
							end if
						
						next
						
						WasStr = "was..."
						NowStr = "now..."
						'BulkStr = "bulk..."
						
						NowPrice = ProdArr(13,c)
						
						ShowRow = false
						
						'jhAdmin "CHECK SOME ARE AVAILABLE?"
						
						if 1 = 1 then
							for z = c to ProdC
							
								
								if ThisVariant = ProdArr(5,z) and "" & ProdArr(29,z) <> "" then
									
									if convertnum(ProdArr(29,z)) > 0 or (isadminUser() and HideOOS = "") then 
										ShowRow = true
										'jh "SHOW ThisProductCode = " & ThisProductCode & " = " & ProdArr(10,z) & " (" & ProdArr(29,z) & ")"
										exit for
									end if
								elseif ThisVariant <> ProdArr(5,z) then
									exit for
								end if
							next
						else
							ShowRow = true
						end if

											
						NewItems = false
											
						if ShowRow then						
							for z = c to ProdC
								
								if ThisVariant <> ProdArr(5,z) then exit for
								
								ThisFirstDate = ProdArr(36,z)
								
								if isdate(ThisFirstDate) then
									CheckDD = datediff("d", ThisFirstDate, now)

									if CheckDD <= 30 then
										NewItems = true
										SomeNewItems = true
										NewItemStr = ""
										exit for
									end if
								end if						
							next
						end if
						
						if NewlyAdded = "1" and NewItems = false then ShowRow = false
						
						if ShowRow then
						
							ls.add "<tr id=""VARIANT_" & ThisVariant & """>"
							
							ls.add "<td valign=top class=text-center>"
							
							if NewItems then
								ls.add "<div><span class=""label label-important""><i class=""icon icon-star icon-white""></i> " & NewlyAddedLabel & "</span></div>"
							end if
							
							ls.add "<a href=""/products/?ProductID=" & ThisProductID & "&InitVariant=" & ThisVariant & """>"		
							ls.add "<img class=ProdThumb90 src=""" & imgURL & """>"
							ls.add "<div><strong>" & ThisVariant & "</strong></div>"
							ls.add "</a>"
							ls.add "</td>"
							
							ls.add "<td valign=top>"
							ls.add "<div><strong>" & ThisProductName & "</strong></div>"
							ls.add "<div class=spacer5>" & ColorStr & "</span>&nbsp;" & ThisColor & "</div>"
							ls.add "<div class=spacer5><span class=""badge"">" & WasLabel & " " & dispCurrency(WasPrice) & "</span></div>"
							ls.add "<div class=spacer5><span class=""badge badge-success ClearanceNow""> " & NowLabel & " " & dispCurrency(NowPrice) & "</span></div>"
							ls.add "<div>" & BulkStr & "</div>"
							'ls.add "ThisSizeType = " & ThisSizeType
							
							ls.add "</td>"						
							
							for y = 0 to SizeC
								if "" & ThisSizeType = "" & SizeArr(2,y) and SizeArr(4,y) = 1 then
									ThisSizeName = SizeArr(1,y)
									
									SKUFound = false
									SoldOut = false
									SizeLabel = ""
									
									StyleStr = "background:#f0f0f0;color:#ccc;"
									
									for z = c to ProdC
										CheckSizeName = ProdArr(11,z)
										if ThisSizeName = CheckSizeName  and ThisVariant = ProdArr(5,z) then
											SKUFound = true
											ThisSKU = ProdArr(10,z)
											StyleStr = ""
											SizeLabel = "label label-info"
											
											MAIQty = convertnum(ProdArr(29,z))
											BasketQty = ProdArr(31,z)
											
											if MAIQty <= 0 then 
												SoldOut = true
											else
												
											end if
											
											ThisNow = ProdArr(13,z)
											ThisRRP = ProdArr(23,z)
											
											exit for 
										end if
									next
									
									ls.add "<td style=""" & StyleStr & """ valign=top class=text-center width=60>"
									ls.add "<div class=spacer10><span class=""" & SizeLabel & """>" & ThisSizeName & "</span></div>"
									if SKUFound then
										if SoldOut then
											ls.add "<span class=""label label-inverse"">" & SoldOutLabel & "</span>"
										else
											
											SKUCount = SKUCount + 1
										
											ls.add "<div class=spacer5>" & DispStock(MAIQty) & "</div>"
											ls.add "<input name=""q_" & SKUCount & """ id=""q_" & SKUCount & """ class=ClearanceQty type=number value=""" & BasketQty & """>"
											ls.add "<input name=""s_" & SKUCount & """ id=""s_" & SKUCount & """ class=FormHidden type=hidden value=""" & ThisSKU & """>"
											ls.add "<input name=""w_" & SKUCount & """ id=""w_" & SKUCount & """ class=FormHidden type=hidden value=""" & BasketQty & """>"
											ls.add "<div><small>" & ThisSKU & "</small></div>"
										end if
									else
									
									end if
									ls.add "</td>"						
								end if
							next
							
							ls.add "</tr>"
						end if
					end if
				
				next
				
				ls.add "</table>"
				
				ClearanceOS = ls.value
				
				set ls = nothing
				

				
				if isAdminUser and HideOOS <> "" then
					OOSAddOnStr = "&HideOOS=1"
				end if
				
				if SomeNewItems then
				
					if NewlyAdded = "1" then
						FilterStr = "<a href=""?NewlyAdded=0" & OOSAddOnStr & """ class=OrangeLink>Click to view all current clearance lines</a>"
					else						
						FilterStr = "<a href=""?NewlyAdded=1" & OOSAddOnStr & """ class=OrangeLink>Click to filter the clearance list to only newly added lines</a>"
					end if
				
					jhInfoNice "New items added!", "New items have been added to the clearance list in the last 30 days - look for the following <span class=""label label-important""><i class=""icon icon-star icon-white""></i> NEWLY ADDED!</span><br>" & FilterStr
				end if
				
				if isAdminUser and HideOOS = "" then
					jhInfoNice "PLEASE NOTE:", "As an admin user, you will see <u>all</u> items currently listed as 'on clearance' - normal dealer logins will only see lines that have at least one sku for that colourway currently in stock - <a class=OrangeLink href=""?HideOOS=1"">click here to view only clearance items with available stock</a>"				
				end if			

				'jhAdmin "SKUCount = " & SKUCount	
				
				if ClearanceAdded then
					jhInfoNice "Clearance items added!", "You have added your chosen items to your basket - <a class=OrangeLink href=""/checkout/"">click here to go to the checkout</a>"
				%>
				
				<%
				else
								
				%>
		
				
					<div>
					
						<%=getContent("clearance_explained_top")%>
						
					</div>
				<%
				end if				
				
				%>
		
				<form id="ClearanceForm" action="/products/clearance/" method=post>
					<%
					=ClearanceOS
					%>
					
					<div class=spacer40></div>
					
					<div class=alert>						
						<button type="button" class="close" data-dismiss="alert">&times;</button>					
						<%=getContent("clearance_explained_bottom")%>
					</div>
					<p>
						<button class="btn-large btn btn-success"><%=lne("btn_add_to_basket")%></button>
						<input type=hidden name="FormAction" class="FormHidden" value="CLEARANCE_ADD">
						<input type=hidden name="SKUCount" class="FormHidden" value="<%=SKUCount%>">
						
					</p>
					
					<div id=xClearanceAddAjaxDiv>
						
					</div>
					
				</form>
		
			</div>
		</div>
	</div>

<!--#include virtual="/assets/includes/footer.asp" -->


<%


%>

<script>

	function clearanceAdd() {
		
		var str = $("#ClearanceForm").serialize();
			
//$("#serTest").html(str);

			
		$("#ClearanceAddAjaxDiv").html('<img src="/assets/images/icons/indicator.gif"> loading...');
				
		$.ajax({
			scriptCharset: "ISO-8859-1" ,
			contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
			url: "/basket_server.asp?CallType=ADD&Params=CLEARANCE_ADD",
			data: str,
			cache: false,			
			}).done(
				function( html ) {
				$("#ClearanceAddAjaxDiv").html(html);
				$('#ClearanceAddAjaxDiv').show(100, function() {
					// Animation complete.
				  });	
				  
				  
			});
		
		
	}
</script>