<%
PageTitle = "Technology"
PageID = 18

function drawTech(pid, pimage, pname)

end function

function getFeatures(pType)
	sql = "SELECT FeatureTypeID, FeatureName, FeatureImage FROM SML_ProductFeatures WHERE FeatureType = '" & pType & "' ORDER BY FeatureType, FeatureZOrder, FeatureName " 
	'jh sql
	dbConnect()
	x = getrs(Sql, FeatureArr, FeatureC)
	for c = 0 to FeatureC step 2	
	
%>
		<div class="row spacer20">		
<%	
		for x = c to c + 1
			if x <= FeatureC then
				ThisFeatureID = FeatureArr(0,x)
				ThisFN = FeatureArr(1,x)
				ThisImage = FeatureArr(2,x)
				imgStr = "<img src=""/assets/images/ProdBrowser/features/" & ThisImage & """>"		
				FeatureContent = lne_B2C("technology_feature_" & ThisFeatureID)
			else
				imgStr = ""
				ThisFN = ""
				FeatureContent = ""
			end if 
			
	%>
			
				<div id="FEATURE_<%=ThisFeatureID%>" class=span2>
					<%=imgStr%>
				</div>
				<div class=span4 style="border-bottom:0px solid red;xpadding-right:40px;">
					<h4><%=ThisFN%></h4>
					<div style="font-size:10pt;"><%=FeatureContent%></div>
				</div>						
			
	<%
		next
%>
		</div>
<%
	next	
	
end function
%>

<!--#include virtual="/assets/includes/header.asp" -->



    	
        <div class="container">
<%
BackTo = request("BackTo")
BackToID = cleannum(request("BackToID"))

if BackTo <> "" then
	if BackTo = "PRODUCTS" and BackToID <> "" then
		BackToURL = "/products/?ProductID=" & BackToID
		BackToLabel = "Back to product"
	end if
	if BackToURL <> "" then
%>		
			<div class="affix-top-right" data-spy="affix" data-offset-top="50">    				
					<div class=row>
						<div class="span2 offset10" style="font-size:10pt;padding:5px 10px;border-radius:5px;background:#333;color:white;">
							<a class=WhiteLink href="<%=BackToURL%>">				
								<i class="icon-chevron-left icon-white"></i> Back to product
							</a>
						 </div>
					</div>				
			</div>		
<%
	end if
end if
%>		
            <div class="section_header">
                <h3><%=lne(PageTitle)%></h3>
				
				<div class="navbar navbar-simple">						
					<ul class="nav">
					  <li><a class=ContentLink href="#technology_fabrics_materials"><%=lne_B2C("technology_fabrics_materials")%></a></li>
					  <li><a class=ContentLink href="#technology_technology"><%=lne_B2C("technology_technology")%></a></li>
					  <li><a class=ContentLink href="#technology_seat_pads_men"><%=lne_B2C("technology_seat_pads_men")%></a></li>
					  <li><a class=ContentLink href="#technology_seat_pads_women"><%=lne_B2C("technology_seat_pads_women")%></a></li>
					</ul>				 
				</div>					
				
				<div class=clearfix></div>					
            </div>	
					
			<div class="row spacer20" id="technology_fabrics_materials">
				<div class=span12>
					<h3><%=lne_B2C("technology_fabrics_materials")%></h3>
				</div>
			</div>
			<%=getFeatures("Fabrics & Materials")%>
			
			<hr>
			
			<div class="row spacer20" id="technology_technology">
				<div class=span12>
					<h3><%=lne_B2C("technology_technology")%></h3>
				</div>
			</div>
			<%=getFeatures("Technologies")%>

			<hr>
			
			<div class="row spacer20" id="technology_seat_pads_men">
				<div class=span12>
					<h3><%=lne("technology_seat_pads_men")%></h3>
				</div>
			</div>
			<%=getFeatures("Men�s Seat Pads")%>				
			
			<hr>
			
			<div class="row spacer20" id="technology_seat_pads_women">
				<div class=span12>
					<h3><%=lne("technology_seat_pads_women")%></h3>
				</div>
			</div>
			<%=getFeatures("Women�s Seat Pads")%>							
<%
			



%>					
			
			
			 <div class="spacer40">&nbsp;</div>
        </div>
	

<!--#include virtual="/assets/includes/footer.asp" -->