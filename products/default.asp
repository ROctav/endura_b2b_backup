
<%
PageTitle = "Products"

ProductID = cleannum(request("ProductID"))
InitCode = request("InitCode")
'jhAdmin "InitCode = " & InitCode

galleryPage = true

function getActiveTab(pid)
	if pid = sTab then
		getActiveTab = "active"
	else
		getActiveTab = ""
	end if	
end function

%>

<!--#include virtual="/assets/includes/header.asp" -->

<!--#include virtual="/assets/includes/product_functions.asp" -->

<style>
	.BackImages {
		xborder:2px solid green;
		display:none;
	}
	
	#MainImageCaption {
		text-align:left;
		padding-left:40px;
		font-size:9pt;
		font-weight:bold;
	}
	
	.ThumbHolder {
		xborder:1px solid red;
	}
	
	.galleryHolder {
		margin-right:5px;
		margin-bottom:5px;
	}
	
	.semiTrans{
		opacity:0.5;
	}
	
</style>

<%




'jh "SelectedCurrencySymbol = " & SelectedCurrencySymbol


sql = "SELECT sp.ProductID, ProductName, ProductCode, ShortDescription, BulletPoints, OneLiner, SizeChartID, Ranges, Categories, ProductTypes, TechnicalAspects, InfoMessageTypeID, InfoMessageText, LongDescription, Features, PublishOnB2B, ProductGroupingShortName, fav.AcCode "
sql = sql & "FROM SML_Products sp "
sql = sql & "LEFT JOIN (SELECT DISTINCT ProductID, AcCode FROM B2B_ProductFavourites f WHERE f.AcCode = '" & AcDetails.AcCode & "') fav ON fav.ProductID = sp.ProductID " 
sql = sql & "WHERE sp.ProductID = 0" & ProductID & " "
jhadmin sql
dbConnect()
set rs = db.execute(sql)
if not rs.eof then

	ProductCode = rs("ProductCode")
	ProductName = rs("ProductName")
	ShortDescription = rs("ShortDescription")
	BulletPoints = rs("BulletPoints")
	OneLiner = rs("OneLiner")
	SizeChartID = rs("SizeChartID")
	
	'jh "PublishOnB2C = " & rs("PublishOnB2C")
	
	if ShortDescription = "MISSING" then ShortDescription = OneLiner
	
	'LongDescription = "" & rs("LongDescription")
	
	Features = rs("Features")
	
	ProductFound = true
	
	ThisRanges = "" & rs("Ranges")
	ThisCategories = "" & rs("Categories")
	ThisProductTypes = "" & rs("ProductTypes")
	
	TechnicalAspects = "" & rs("TechnicalAspects")
	
	InfoMessageTypeID = "" & rs("InfoMessageTypeID")
	InfoMessageText = "" & rs("InfoMessageText")
	
	ProductGroupingShortName = "" & rs("ProductGroupingShortName")
	
	FavouriteAcCode = "" & rs("AcCode")
	
	if FavouriteAcCode <> "" then isFavourite = true
	
	
	call captureProductView(ProductID)
	
else
	jhErrorNice "Product details NOT found", "There was a problem finding the selected product...<br><a href=""/""><i class=""icon-circle-arrow-left""></i> Return to product browser</a>"
	dbClose()
	response.redirect "/"
end if



rsClose()

if ProductFound then
	
	'jhAdmin "GET LINKED!"
	
	ProductGroupingID = ""
	
	SelectedLanguageID = AcDetails.SelectedLanguageID
	
	sql = "SELECT ProductGroupingID FROM SML_ProductGroupingDetail WHERE ProductID = 0" & ProductID & ""
    jhadmin sql
	set rs = db.execute(sql)
	if not rs.eof then
		ProductGroupingID = "" & rs("ProductGroupingID")
		'jhInfo "ProductGroupingID = " & ProductGroupingID
		LinkedProds = true
	end if
	rsClose()
	
	if ProductGroupingID <> "" then
	
		sql = "SELECT sp.ProductID, sp.ProductCode, sp.ProductName, VariantCode, spc.ColorName, sp.ProductGroupingShortName FROM SML_Products sp "
		sql = sql & "INNER JOIN SML_ProductColorways spc ON spc.ProductID = sp.ProductID "
		sql = sql & "INNER JOIN SML_ProductGroupingDetail gr ON gr.ProductID = sp.ProductID "
		sql = sql & "WHERE gr.ProductGroupingID = 0" & ProductGroupingID & " AND dbo.spB2BActive(sp.ProductID) > 0 " 
		sql = sql & "ORDER BY sp.PRoductCode, spc.DefaultProductImage, spc.VariantCode "
		
		jhAdmin sql
				
		x = getrs(sql, LinkedArr, LinkedC)
		
		'jh "LinkedC = " & LinkedC
		if LinkedC > 0 then
			LinkedProds = true			
		end if
		
	end if	
	
	if SelectedLanguageID <> "0" then
		'jhInfo "GET TRANSLATIONS"
		sql = "SELECT * FROM SML_ProductTranslations WHERE ProductID = 0" & ProductID & " AND LanguageID = 0" & SelectedLanguageID & ""
		'jh sql
		set rs = db.execute(sql)
		if not rs.eof then
			ProductName_TRANS = trim("" & rs("ProductName"))
			OneLiner_TRANS = trim("" & rs("OneLiner"))
			ShortDescription_TRANS = trim("" & rs("ShortDescription"))
			BulletPoints_TRANS = trim("" & rs("BulletPoints"))
			LongDescription_TRANS = trim("" & rs("LongDescription"))
			
			
			if ProductName_TRANS <> "" then ProductName = ProductName_TRANS
			if OneLiner_TRANS <> "" then OneLiner = OneLiner_TRANS
			if ShortDescription_TRANS <> "" then ShortDescription = ShortDescription_TRANS
			if BulletPoints_TRANS <> "" then BulletPoints = BulletPoints_TRANS
			if LongDescription_TRANS <> "" then LongDescription = LongDescription_TRANS
			
			
		end if
		rsClose()
	end if

	'jhStop
	
	if ucase("" & ShortDescription) = "MISSING" then ShortDescription = OneLiner
	LongDescription = ""
	
	SimilarString = ""
	
	RangeArr = split(ThisRanges, "|")
	
	for c = 0 to ubound(RangeArr)
		RangeID = RangeArr(c)
		if RangeID <> "" then
			sql = "SELECT RangeName FROM SML_Ranges WHERE RangeID = 0" & RangeID
			set rs = db.execute(sql)
			if not rs.eof then
				ThisRangeName = rs("RangeName")	
				'jhInfo "ThisRangeName = " & ThisRangeName
				SimilarString = SimilarString & "<div>Range: <a href=""/"">" & ThisRangeName & "</a></div>"
				
				sql = "SELECT ProductID, ProductCode, ProductName FROM SML_Products WHERE Ranges LIKE '%|" & RangeID & "|%' AND PublishOnB2B = 1 AND ProductID <> 0" & ProductID & " ORDER BY ProductName"
				x = getrs(sql, RelatedArr, RelatedC)
				for x = 0 to RelatedC
					SimilarString = SimilarString & "<div class=infotext style=""padding-left:10px;""><a href=""/products/?ProductID=" & RelatedArr(0,x) & """>" & RelatedArr(2,x) & "</a></div>"
				
				next
				
			end if
			rsClose()
		end if
		
	next
	
	'jh "get colorways"
	
	sql = "SELECT spc.VariantCode, ISNULL(co.ColorOverrideName, ColorName) AS VariantColor "
	sql = sql & ", (SELECT COUNT(*) FROM SML_SKUS ss INNER JOIN SML_SKUStatus sss ON ss.SKUStatusID = sss.SKUStatusID WHERE B2BVisible = 1 AND ss.ProductColorWayID = spc.ProductColorWayID) AS SKUCount "
	sql = sql & ", spc.ColorWayID "
	sql = sql & "FROM SML_ProductColorways spc "
	sql = sql & "LEFT JOIN B2C_ColorOverrides co ON co.VariantCode = spc.VariantCode "
	sql = sql & "WHERE spc.ProductID = 0" & ProductID & " "	
	sql = sql & "AND (SELECT COUNT(*) FROM SML_SKUS ss INNER JOIN SML_SKUStatus sss ON ss.SKUStatusID = sss.SKUStatusID WHERE B2BVisible = 1 AND ss.ProductColorWayID = spc.ProductColorWayID) > 0 "
	sql = sql & "ORDER BY DefaultProductImage DESC, VariantCode "
	
	jhadmin sql
	x = getrs(sql, ColorArr, ColorC)
	
	if ColorC = 0 then OneColor = true
	
	'jh "GET BULLETS"
	sql = "SELECT BulletText FROM SML_ProductBullets "
	sql = sql & "WHERE ProductID = 0" & ProductID & " AND BulletLanguageID = 0" & SelectedLanguageID & " "
	sql = sql & "ORDER BY ProductBulletID "
	'jh sql
	x = getrs(sql, BulletArr, BulletC)
	if BulletC = -1 then
		'jh "GET BULLETS"
		sql = "SELECT BulletText FROM SML_ProductBullets "
		sql = sql & "WHERE ProductID = 0" & ProductID & " AND BulletLanguageID = 0 "
		sql = sql & "ORDER BY ProductBulletID "
		x = getrs(sql, BulletArr, BulletC)
	end if
		
	'jhAdmin "HidePrices = " & HidePrices
	
end if


%>

 		<div class="container">
				
				<div class=spacer10></div>
				<a class="TopNavLink OrangeLink" href="/"><i class="icon-circle-arrow-left"></i> <%=lne("product_back_to_browser")%></a>
				<%
				if request("BackTo") = "REVIEWS" then
				%>
					<div class=spacer10></div>
					<a class="TopNavLink OrangeLink" href="/products/reviews/?gc=1"><i class="icon-circle-arrow-left"></i> <%=lne_b2c("product_back_to_reviews")%></a>
				<%
				end if
				%>
				
				<div class="row">
			 
<%
if not ProductFound then
	
end if
%>				
				
			 
				 <div class="span5">
				 
					<div class=spacer20>&nbsp;</div>
								
<%
'jhInfo "VARIANT INFO"

if isarray(ColorArr) then 

	'jh "ThisVariantCode = " & ThisVariantCode

	'jhAdmin "InitCode = " & InitCode
	if InitCode <> "" then
		for x = 0 to ubound(ColorArr,2)
			if ColorArr(0,x) = InitCode then 				
				ThisVariantCode = InitCode
				
			end if	
		next
	end if
	
	if ThisVariantCode = "" then ThisVariantCode = ColorArr(0,0)
	
	'jhAdmin "ThisVariantCode = " & ThisVariantCode
	
	ColorMainInit = getImageColorMain(ProductCode, ThisVariantCode, rURL)
	ColorZoomInit = getImageColorZoom(ProductCode, ThisVariantCode, rURL)
	
	if ColorMainInit = "" then 
		ColorMainInit = "/assets/images/prodbrowser/product_placeholder.jpg"		
		'NoZoom = true
	end if	
	
	'jh "ColorZoomInit = " & ColorZoomInit
	
end if


%>		
					<div class="hidden-phone">							
						<img id="prod_01" style="width:350px;" src="<%=ColorMainInit%>" data-zoom-image="<%=ColorZoomInit%>"/>				
						<div class=hidden id="MainImageCaption"></div>				
						<p>&nbsp;</p>					
					</div>
					
<%

					ThisGalleryCount =  getGallery(ProductID, SelectedLanguageID, TeaserImage, GalleryImages)

					if ThisGalleryCount  > 0 then
%>
						<div class="hidden-phone spacer20">	
							<hr>
							<div class="spacer20"><%=TeaserImage%><i class="semiTrans icon-search"></i></a> <small><%=lne("label_product_detail_addtional_images")%> (<%=ThisGalleryCount%>)</small></div>
							
							<div style="margin-right:50px;">
								<%
								=GalleryImages
								%>
							</div>
						</div>
						
	
<%
					end if
%>			
					
				 </div>
			 
				
				
		<div class="span7">
		
<%
		if not isFavourite then
			FavouriteLabel = lne("label_favourites_add")
			FavouriteClass = "label-success"
			FavouriteAction = "ADD"
		else
			FavouriteLabel = lne("label_favourites_remove")
			FavouriteClass = "label-important"
			FavouriteAction = "REMOVE"
		end if		
					
%>		
		
			<div class=pull-right id="FavouriteDiv">	
				<a href="#" id="FavouriteLink" data-product-id="<%=ProductID%>" data-action="<%=FavouriteAction%>" class="label <%=FavouriteClass%>"><i class="icon-heart icon-white"></i> <%=FavouriteLabel%></a>	
			</div>
			
			<h4><%=ProductName%>  <div class=spacer5></div><small><%=OneLiner%></small></h4>				
				
			
							<%
							if LinkedProds then
								
								if ProductGroupingExplained = "" then ProductGroupingExplained = "Similar Products"
								
							%>
								<h5><%=ProductGroupingExplained%></h5>
								<table border=1 style="border:1px solid #ddd;">
								<tr>
															
							<%						
								LastLinked = ""
							
								for c = 0 to LinkedC
									
									LinkedID = LinkedArr(0,c)
									LinkedCode = LinkedArr(1,c)
									LinkedName = LinkedArr(2,c)
									LinkedVariant = LinkedArr(3,c)
									LinkedColor = LinkedArr(4,c)
									LinkedShortName = "" & LinkedArr(5,c)
									if LinkedShortName = "" then LinkedShortName = LinkedName
																	
									if LastLinked <> LinkedID then
										LastLinked = LinkedID
										linkedThumb = getImageDefault(LinkedCode, LinkedVariant)
								%>
										<td valign=top style="padding:5px;font-size:8pt;text-align:center;line-height:12pt;border-right:1px solid #ddd;">		
			
											<a data-linkid="<%=LinkedID%>" class=LinkedProdLink href="#">
												<img title="<%=LinkedName%>" class=ThumbHolder width=40 style="width:50px;padding-left:5px" src="<%=linkedThumb%>" />
											</a>			
											<br>
											
											<%=LinkedShortName%><br>
											<%=LinkedColor%><br>
											<span style="color:silver;"><%=LinkedVariant%></span>										
											
										</td>
							<%	
										
									end if
								next
								
								
							%>
									</tr>
								</table>
							<%								
							end if
							
							%>
							
					
						
					<div class=spacer20></div>

					
				<%
				if InfoMessageText <> "" then
					if InfoMessageTypeID = "2" then MessageClass = "alert-error" else MessageClass = "alert-success"
				
				%>
					<div class="alert <%=MessageClass%>">
					  <%=InfoMessageText%>
					</div>			
				<%
				end if
				
				
	

			
				sql = "SELECT ReviewDate, ReviewTitle, ReviewBody, ExternalURL, DownloadFile, LegacyReviewFile, ReviewScore, LogoFile "
				sql = sql & "FROM SML_ProductReviews "
				sql = sql & "WHERE ProductID = 0" & ProductID & " AND ReviewDate <= '" & formatdatetime(now, 1) & "' AND ReviewStatusID = 1 AND (ISNULL(PrimaryLanguageID, '') = '' OR PrimaryLanguageID = 0" & SelectedLanguageID & ") "
				sql = sql & "ORDER BY ReviewDate DESC, ReviewID DESC "

				CutoffReviewDate = dateadd("m", -glReviewCutoffMonths, now)
				sql = "exec [spB2CProductReviews] 0" & ProductID & ", 0" & SelectedLanguageID & ", '" & SelectedLocation & "', '" & formatdatetime(CutoffReviewDate, 1) & "', 0" & ProductGroupingID

				x = getrs(sql, ReviewArr, ReviewC)
				'jh "ReviewC = " & ReviewC
				ActualReviewCount = ReviewC + 1

				if ActualReviewCount > 0 then
					ShowReviews = true
					MostRecentDate = ReviewArr(0,0)
					'jh "MostRecentDate = " & MostRecentDate
					ReviewDD = datediff("m", MostRecentDate, now)
					'jh "ReviewDD = " & ReviewDD
					if ReviewDD <= glNewReviewTabTrigger then
						'jh "NEW REVIEWS FOUND!"
						ShowNew = true
					end if
				end if

				if Replace(ThisRanges, "|", "") <> "" then
					sql = "SELECT ProductID, ProductCode, ProductName, (SELECT TOP 1 VariantCode FROM SML_ProductColorways spc WHERE spc.ProductID = SML_Products.ProductID ORDER BY DefaultProductImage DESC, VariantCode) AS DefaultVariant "
					sql = sql & "FROM SML_Products WHERE Ranges LIKE '%" & ThisRanges & "%' AND dbo.[spB2BActive](ProductID) > 0 AND ProductID <> 0" & ProductID & " "
					sql = sql & "ORDER BY ProductName "
					'jh sql
					x = getrs(sql, OtherRangeArr, OtherRangeC)
				else
					OtherRangeC = -1
				end if

				sTab = cleannum(request("sTab"))
				
				if sTab = "" then sTab = 0
				if stab <> 1 and sTab <> 2 then sTab = 0

				if ActualReviewCount = 0 and sTab = 2 then sTab = 1



%>

				 
					<ul class="nav nav-tabs legible">					
						<li class="<%=getActiveTab(0)%>"><a href="#Tab0" data-toggle="tab"><%=lne("product_tab_grid")%></a></li>		
						
						<li class="<%=getActiveTab(1)%>"><a href="#Tab1" data-toggle="tab"><%=lne("product_tab_details")%></a></li>
						
						<%
						if ActualReviewCount > 0 then
						%>
							<li class="<%=getActiveTab(2)%>">
								<a href="#Tab2" data-toggle="tab"><%=lne_NOEDIT("product_tab_reviews")%> 
								
								<%if ShowNew then%>
								<span title="Recently added reviews" class="badge badge-warning"><%=ActualReviewCount%></span>
								<%else%>
								<span class="badge"><%=ActualReviewCount%></span> 
								<%end if%>
								</a>
							</li>
						<%
						end if
						
						if SizeChartID <> 0 then
						%>
							<li><a href="#Tab3" data-toggle="tab"><%=lne("product_tab_sizing")%></a></li>
						<%
						end if
						
						DownloadCount = 0
						if DownloadCount > 0 then
						%>
							<li><a href="#Tab4" data-toggle="tab">Downloads <span class="badge"><%=DownloadCount%></span></a></li>			
						<%
						end if
						%>
						
						<%
						if OtherRangeC >= 0 then
						%>
							<li><a href="#TabAlsoRange" data-toggle="tab"><%=lne("product_tab_related_prods")%></a></li>
						<%
						end if
						%>						
					</ul>

					<div class="tab-content" style="background:white;xmargin-top:0px;padding:10px;border-bottom:1px solid #ddd;border-left:1px solid #ddd;min-height:210px;">
						<div class="tab-pane <%=getActiveTab(0)%>" id="Tab0" style="margin-left:5px;">
						
							<h4><%=lne("label_product_detail_availability")%></h4>
								
							<div id=SKUGrid class=spacer20>
							
<%
	ProductPageZoom = true	
	GridOS = productGrid(ProductID, "")
	
	response.write GridOS
	
		if not NoZoom then
				%>
				
					<script>
						
						$("#prod_01").elevateZoom({gallery:'ProductZoomGallery', cursor: 'pointer', galleryActiveClass: 'active', zoomWindowFadeIn: 500,
								zoomWindowFadeOut: 500,
								lensFadeIn: 500,
								lensFadeOut: 500}); 
								
										
						var ez = $('#prod_01').data('elevateZoom');  

						
						
						function changezoom()
							{
								if ($(window).width() < 600)
								{
									ez.changeState('disable');
								}
								else
								{
									ez.changeState('enable');
								}
							}
							
						$(window).resize(function() {
						 changezoom();
						 }); 
						 
						 

						changezoom();				
							
						
					</script>				


<%
				end if	
	
%>							
							
							</div>						
						
						</div>
					
						<div class="tab-pane <%=getActiveTab(1)%>" id="Tab1" style="margin-left:5px;">
					
							<div class=spacer20></div>
							<div class=ProductShortDescription><%=ShortDescription%></div>
							
							<%
							if LongDescription <> "" then
							%>
								<div class=ProductLongDescription>
									<%=LongDescription%>
								</div>
							<%
							end if
							'jhInfo LongDescription
							
							
							
							if BulletC >= 0 then
								'BulletOS = "<ul>"
								BulletOS = ""
								BulletNum = 0
								for c = 0 to BulletC
									ThisBullet = BulletArr(0,c)
									BulletNum = BulletNum + 1
									
									if ThisBullet <> "" then
										
										
										HasBulletImage = getBulletImage(ProductCode, BulletNum, BulletImgURL)
										'jh ImgURL
										if BulletImgURL <> "" then
											ImgStr = " <a class='BulletGallery' title=""" & ThisBullet & """ href=""" & BulletImgURL & """><i class=""icon-search semiTrans""></i></a>"								
										else
											ImgStr = ""
										end if
										BulletOS = BulletOS & "<div class=ProductBullet><i class=icon-chevron-right></i> " & ThisBullet & ImgStr & "</div>"
									end if
								next
								'BulletOS = BulletOS & "</xul>"							
							end if
							
							
							%>
							
							<%
							=BulletOS
							%>
							
							<div class=spacer20></div>
							
							<div class=row>
								
									
							<%
						
							if AcDetails.SelectedLanguageISO = "UK" then LabelLang = "EN" else LabelLang = AcDetails.SelectedLanguageISO
							sql = "exec [spB2CLabelInfo] '" & ProductCode & "', '" & LabelLang & "' "
							
							'jhAdmin sql
							'jhStop
											
							x = getrs(sql, CompArr, CompC)
							
							if CompC = -1 then

							else
								
									ThisCOM = CompArr(4,0)
									ThisFab = CompArr(5,0)
									
									'ConStr = ConStr & "<div><strong>" & ThisCOM & "</strong></div>"
									
									LastVariant = ""
									for c = 0 to CompC
										ThisVariant = CompArr(3,c)
										ThisMaterial = CompArr(6,c)
										ThisPC = CompArr(7,c)
										
										if LastVariant <> ThisVariant and ThisVariant <> "-" and thisVariant <> "" then										
											ConStr = ConStr & "<strong>" & ThisVariant & "</strong>"										
											LastVariant = ThisVariant
										end if
										ConStr = ConStr & "<div>" & ThisMaterial & " <strong>" & ThisPC & "%</strong></div>"	
									
									next
								%>
								
								
								
								<div class=span3>
									<p><%=lne("product_construction")%></p>
									<div style="font-size:8pt;padding:10px;background:#f5f5f5;border-radius:10px;">			
										<%
										=conStr
										%>																
									</div>
								</div>
								<%
							end if
							
							'jh "TechnicalAspects = " & TechnicalAspects
							
	taosStr = ""
	



	sql = "SELECT ProductID, ProductDimType, DimNumberValue FROM SML_ProductDims WHERE ProductID = 0" & ProductID
	z = getrs(sql, DimsArr, DimsC)
	
	for c = 0 to DimsC
		ThisTAName = DimsArr(1,c)
		ThisTAValue = trim("" & DimsArr(2,c))
		
		if ThisTAName = "WATERPROOFNESS" and ThisTAValue <> "" and ThisTAValue <> "0" then
			taIcoStr = "<img src=""/assets/images/prodbrowser/symb_wproof.png""> "
			'taosStr = taosStr & "<div style=""padding-bottom:10px;"">" & taIcoStr & ThisTAName & ": <strong>" & ThisTAValue & "</strong></div>"
			taosStr = taosStr & "<div style=""padding-bottom:10px;"">" & taIcoStr & lne("product_waterproofness") & ": <strong>" & ThisTAValue & "</strong></div>"
			TAFound = true
		end if
		if ThisTAName = "BREATHABILITY" and ThisTAValue <> "" and ThisTAValue <> "0" then
			taIcoStr = "<img src=""/assets/images/prodbrowser/symb_bable.png""> "
			'taosStr = taosStr & "<div style=""padding-bottom:10px;"">" & taIcoStr & ThisTAName & ": <strong>" & ThisTAValue & "</strong></div>"
			taosStr = taosStr & "<div style=""padding-bottom:10px;"">" & taIcoStr & lne("product_breathability") & ": <strong>" & ThisTAValue & "</strong></div>"
			TAFound = true
		end if
	next
	


		'jhAdmin "LangISO = " & session("B2C_UserLanguage")
		
		
							if TAFound then
							%>									
								
								<div class=span3>
									<p><%=lne("product_tech_aspects")%></p>
									<div style="font-size:8pt;padding:10px;background:#f5f5f5;border-radius:10px;">		
									<%
									=taosStr
									%>
									</div>
								</div>
							<%
							end if
							%>
							</div>	
							
							<div class=spacer20></div>
							
							<div class=row>
							
							<%
							
								'jh "Features = " & Features
								sql = "SELECT FeatureTypeID, FeatureName, FeatureImage, FeatureType FROM SML_ProductFeatures WHERE FeatureTypeID NOT IN (13,14,15) ORDER BY FeatureType, FeatureName"
								x = getrs(sql, FeatureArr, FeatureC)
								'jh sql
								featureOS = ""
								LastType = ""
								for c = 0 to FeatureC
								
									ThisFeatureID = FeatureArr(0,c)				
									ThisFeatureName = FeatureArr(1,c)	
									ThisFeatureImg = FeatureArr(2,c)	
									ThisFeatureType = FeatureArr(3,c)	
									
									if instr(1, Features, "|" & ThisFeatureID & "|") > 0 then
										
										featureOS = featureOS & "<a href=""/products/technology/?backto=PRODUCTS&backtoid=" & ProductID & "#FEATURE_" & ThisFeatureID & """>"
										featureOS = featureOS & "<img title=""" & ThisFeatureName & """ class=spacer20 style=""width:80px;"" src=""/assets/images/prodbrowser/features/" & ThisFeatureImg & """>"
										featureOS = featureOS & "</a>&nbsp;&nbsp;"
										FeaturesFound = true

									end if
								next				
								
								if instr(1, Features, "|28|") > 0 then
									PadFeature = true											
								end if								
								
								if FeaturesFound then 
									featureOS = "<p>" & lne("product_fabrics_tech") & "</p>" & featureOS 
								end if
							
							%>
							
								<div class="span6">
										<div class=spacer20></div>
										<%
										=featureOS										
										%>
										<%
										if PadFeature then
										%>
											<div class=spacer20></div>
											<div class=spacer20>
												<a href="/support/pad_fit/" class="btn btn-warning"><i class="icon-info-sign icon-white"></i>&nbsp;<%=lne_B2C("support_pad_fit_find_out_more_link")%></a>
											</div>
										<%
										end if
										%>
								</div>
							

							
							</div>
							
																					
						</div>
						<div class="tab-pane <%=getActiveTab(2)%>" id="Tab2">
							
							<!--
								<h4 class=spacer40>Latest reviews</h4>
							-->
							&nbsp;
							<%
							for c = 0 to ReviewC
								ThisReviewDate = ReviewArr(0,c)
								if isdate(ThisReviewDate) then ThisReviewDate = formatdatetime(ThisReviewDate, 2)
								ThisReviewTitle = ReviewArr(1,c)
								ThisReviewBody = ReviewArr(2,c)
								ThisExternalURL = ReviewArr(3,c)
								ThisDownloadFile = "" & ReviewArr(4,c)
								
								ThisLegacyFile = ReviewArr(5,c)
								ThisScore = ReviewArr(6,c)
								if ThisScore = "0/0" then ThisScore = ""
								
								ThisLogoFile = "" & ReviewArr(7,c)
								ThisLogoURL = ""

								ReviewProductCode = "" & ReviewArr(9,c)
								'jhAdmin "ReviewProductCode = " & ReviewProductCode
								
								ThisDownloadURL = ""
								if ThisDownloadFile <> "" then
									'jhAdmin "ThisDownloadFile = " & ThisDownloadFile
									ThisDownloadURL = getReviewDownload(ReviewProductCode, ThisDownloadFile, DownloadURL)
									'jhAdmin "ThisDownloadURL = " & ThisDownloadURL
								end if
								
								if ThisLogoFile <> "" then
									ThisLogoURL = getReviewLogoFile(ReviewProductCode, ThisLogoFile, LogoURL)
								end if										
															
							%>
							
							<div class="row spacer20">								
								<div class="span4" style="padding-left:10px;">
									<div class=small>		
										<div><strong><%=ThisReviewDate%></strong></div>
										<div style="color:orange;"><strong><%=ThisReviewTitle%></strong></div>
										<%
										if ThisScore <> "" then
										%>
											<div><span class="label label-warning">Score: <%=ThisScore%></span></div>
										<%	
										end if
										%>
										
										<%=ThisReviewBody%>
										<%
										if ThisExternalURL <> "" then
											ThisExternalURL = fixURL(ThisExternalURL)
										%>
											<div><a class="ExternalURL" target="_BLANK" href="<%=ThisExternalURL%>"><%=ThisExternalURL%></a></div>
										<%
										end if
										
										if ThisDownloadURL  <> "" then											
											%>
											<div><a class="ExternalURL" target="_BLANK" href="<%=ThisDownloadURL%>">Download the full review</a></div>
											<%
										elseif ThisLegacyFile <> "" then
											ThisLegacyURL = glLegacyWebsiteURL & "/GetFile.aspx?itemid=" & ThisLegacyFile
										%>
											<div><a class="ExternalURL" target="_BLANK" href="<%=ThisLegacyURL%>">Download the full review</a></div>
										<%
										end if										
										%>
										
									</div>
									
								</div>	
								<%
								if ThisLogoURL <> "" then
								%>		
									<div class="span2" style="">
										<img style="width:250px" class="img-rounded" src="<%=ThisLogoURL%>" />
									</div>
								<%
								end if
								%>
												
							</div>
							<div class="spacer20" style="border-bottom:1px solid #f0f0f0;"></div>
							
							<%
							next
							%>
							
						
						</div>
						<div class="tab-pane" id="Tab3">
							<h4 class=spacer40><%=lne("label_size_chart")%></h4>	

							<!-- SIZE CHART BEGIN -->
							
							<!--#include virtual="/assets/includes/size_chart.asp" -->
							
							<!-- SIZE CHART ENDS -->
							
						</div>

																	
						<div class="tab-pane" id="TabAlsoRange">							
							<div class=row>
							<%

							
							for c = 0 to OtherRangeC
								otherID = OtherRangeArr(0,c)
								otherPC = OtherRangeArr(1,c)
								otherPN = OtherRangeArr(2,c)								
								otherDef = OtherRangeArr(3,c)
								ThisThumbURL = getImageDefaultCDN(otherPC, otherDef)
							
								
							%>
								<div class=span2 style="text-align:center;margin-top:20px;margin-bottom:20px;font-size:10pt;height:100px;border-bottom:0px solid red;">
									<a href="/products/?ProductID=<%=otherID%>">
										<img style="height:80px;" src="<%=ThisThumbURL%>"><br>
										<strong><%=otherPN%></strong>
									</a>
								</div>
							<%	
							next
							
							%>
							</div>
						</div>
						
					</div>	

					
					
				 </div>
		</div>		
		
	</div>
	
	
	
	<div class=spacer40></div>
			
<!--#include virtual="/assets/includes/footer.asp" -->





<script>
	
	function showBackImages(pid) {
		//alert('sbi' + pid);
		$('.BackImages').hide();
		$('#'+pid).show();
		//alert('done');
	}	
	
	$('.ZoomThumbLink').click(function() {
	  var tc = $(this).data('thumb-caption');
	  
	  $('#MainImageCaption').text(tc);
	});		
		
	$(document).ready(function() {	
		$(".LinkedProdLink").click(function() {
		  //alert('linkedClick!')
		  var LinkedProductID = $(this).data( "linkid" )
		  //alert('LinkedProductID = ' + LinkedProductID)
		  window.location.assign("/products/?ProductID="+LinkedProductID);
		});		
	});	
	
	
</script>

<script>
	//alert('cb')
	$(document).ready(function(){
		//Examples of how to assign the Colorbox event to elements
		$(".group1").colorbox({rel:'group1', height:"75%"});		
		
		$(".BulletGallery").colorbox({height:"75%"});		
		
	});
</script>

<%


response.cookies("ProductsLastVisited") = "1"
response.cookies("ProductsLastVisited").expires = dateadd("d", 2, now)

function getBulletImage(byval pProductCode, byval pNum, byref rImgURL)
	
	set fs = server.createobject("scripting.filesystemobject")
	rImgURL = ""
	
	GalleryOS = ""
	
	detailFloc = glProductImageLoc & pProductCode & "\detail\" 
	detailURL = "https://static.endura.co.uk/" & ProductCode & "_detail_" 

	lookfor = "bp" & right("0" & pNum,2)
	
	if fs.folderexists(detailFloc) then
	
		set f = fs.getfolder(detailFloc)
	
		for each file in f.files			
			thisFN = lcase(file.name)
			if left(thisFN, 4) = lookfor then
				getBulletImage = true
				rImgURL = detailURL & file.name
				exit for
			end if
		next	
		
		set f = nothing
	end if
	
	set fs = nothing
	
end function

function getGallery(byval pProductID, byval pLanguageID, byref rTeaser, byref rGallery)
	
	dim c, sql, x, TypeArr, TypeC, ProductTypes, GalleryOS, ProductCode, GalleryCount
	
	dim MaxImages 
	
	dim bulletText
	dim bulletText_ENGLISH
	
	MaxImages = 3
	
	dbConnect()
	
	sql = "exec [spB2CGetGallery] 0" & pProductID & ", 0" & pLanguageID
	jhAdmin sql
	
	GalleryCount = 0
	
	x = getrs(sql, galleryArr, galleryC)
	
	if galleryC = -1 then exit function
	
	ProductCode = galleryArr(1,0)
	
	set fs = server.createobject("scripting.filesystemobject")
		
	GalleryOS = ""
	
	detailFloc = glProductImageLoc & ProductCode & "\detail\" 
	detailURL = "https://static.endura.co.uk/" & ProductCode & "_detail_" 
		
	ProductMedia = "" & GalleryArr(3,0) 
	
	MediaArr = split(ProductMedia,"|")
	for c = 0 to ubound(MediaArr)
		ThisMedia = trim("" & MediaArr(c))
		if ThisMedia <> "" then
			if instr(1, ThisMedia, "PRODUCT360_") > 0 then Product360 = replace(ThisMedia, "PRODUCT360_", "")
			if instr(1, ThisMedia, "VIDEO_") > 0 then ProductVideo = replace(ThisMedia, "VIDEO_", "")
		end if
	next
	
	if Product360 <> "" then
		'jh "This360 = " & This360
		GalleryCount = GalleryCount + 1
		GalleryOS = GalleryOS & "<a class='group1 youtube' title=""" & lne_NOEDIT("product_gallery_360") & """ href=""https://www.youtube.com/embed/" & Product360 & "?rel=0&amp;wmode=transparent""><img src=""http://extranet.endura.co.uk/assets/sml/media/productimages/play360s.png"" class=galleryHolder style=""height:50px;border:1px solid silver;border-radius:5px;""></a>"		
	end if
		
	if ProductVideo <> "" then
		'jh "ProductVideo = " & ProductVideo
		GalleryCount = GalleryCount + 1
		GalleryOS = GalleryOS & "<a class='group1 youtube' title=""" & lne_NOEDIT("product_gallery_video") & """ href=""https://www.youtube.com/embed/" & ProductVideo & "?rel=0&amp;wmode=transparent""><img title=""VIDEO"" src=""http://extranet.endura.co.uk/assets/sml/media/productimages/playVIDs.png"" class=galleryHolder style=""height:50px;border:1px solid silver;border-radius:5px;""></a>"	
	end if	
	
	'jh "detailFloc = " & detailFloc
	'jh "detailURL = " & detailURL
	
	if fs.folderexists(detailFloc) then
		'jhInfo "detailFloc FOUND!"
	
		set f = fs.getfolder(detailFloc)
	
		for each file in f.files			
			
			ThisFN = lcase(file.name)
			ThisExt = right(ThisFN, 4)
			
			if ThisExt = ".jpg" or ThisExt = "jpeg" then
				GalleryCount = GalleryCount + 1
							
				FileCaption = file.name
				FileCaption = replace(FileCaption, ".jpeg", "")
				FileCaption = replace(FileCaption, ".jpg", "")
				
				ImageSrc = detailURL & ThisFN
						
				if left(ThisFN,2) = "bp" then
					BulletFound = false
					bNumber = cleannum(mid(ThisFN, 3,2)) 
					
					if bNumber <> "" then
						bNumber = cdbl(bNumber)
						'jhInfo "A BULLET! (" & bNumber & ")"
						BulletPos = 0
						for c = 0 to galleryC
							BulletPos = BulletPos + 1
							if BulletPos = bNumber then
								FileCaption = galleryArr(4,c)
								BulletFound = true
								exit for
							end if
						next
						if not BulletFound then
							FileCaption = "Product image " & GalleryCount
						end if						
					end if
				end if
				
				
				lbStr = ""
				
				lbStr = lbStr & "<a class=""group1"" href=""" & ImageSrc & """ title=""" & FileCaption & """>"
				lbStr = lbStr & "<img class=galleryHolder src=""" & ImageSrc & """ style=""height:50px;"">"
				lbStr = lbStr & "</a>"
								
				GalleryOS = GalleryOS & lbStr 
				
				
			end if
		next
		
		set f = nothing
	
	end if	

	getGallery = GalleryCount
	
	rGallery = GalleryOS
	
	set fs = nothing
	
	exit function	
	
end function




%>

<script>

			$(document).ready(function(){
				//Examples of how to assign the Colorbox event to elements
						
				$(".youtube").colorbox({iframe:true, innerWidth:640, innerHeight:390});
				
			});

</script>

	<script>

	
	
		$("#FavouriteLink").click ( function() {
			
			var p = $( this ).data('product-id');
			var a = $( this ).data('action');
			
			//alert(p + '_' + a);
			
			$.ajax({

				scriptCharset: "ISO-8859-1" ,
				contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
				url: "/misc_server.asp?CallType=ADD_FAVOURITE&ProductID=" + p + "&CallContext=" + a,				
				cache: false, 
				
				}).done( function( html ) {
						//alert('done...')						
						$("#FavouriteDiv").html(html);						
					 });			
		
			//alert('done...')						
			
		})
		
	</script>
