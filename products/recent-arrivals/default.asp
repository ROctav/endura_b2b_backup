
<%
Response.Expires = 0
Response.Expiresabsolute = Now() - 1442
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"


ThisUserID = session("ENDURA_UserID")

PageTitle = "Recent Arrivals"

%>


<!--#include virtual="/assets/includes/header.asp" -->


<style>
	.ProductContainer {
		height:250px;
	}
</style>
<%
dbConnect()

jhAdmin "BEGIN!"

HideOOS = cleannum(request("HideOOS"))
NewlyAdded = cleannum(request("NewlyAdded"))

FormAction = request("FormAction")

if FormAction = "xxxCLEARANCE_ADD" then
	
	'jhInfoNice "ADDING TO BASKET!", "Adding clearance items to the basket"
	
	SKUCount = cdbl("0" & request("SKUCount"))
	
	for c = 0 to SKUCount 
		ThisSKU = request("s_" & c)
		ThisQty = cleannum(request("q_" & c))
		ThisWas = cleannum(request("w_" & c))
		
		if ThisQty = "" and ThisWas <> "" then ThisQty = "0"
		
		if ThisQty <> "" and ThisSKU <> "" then
			if cdbl(ThisQty) < 0 then ThisQty = 0
			
			'jhInfo "PROCESS THIS - " & ThisSKU  & " (" & ThisQty & ")"			
			sql = "exec [spB2BBasketAdd] '" & validstr(ThisSKU) & "', 0" & ThisQty & ", '" & AcDetails.AcCode & "', ''"				
			'jhAdmin sql
			db.execute(sql)		
			SomeAdded = true
		end if
		
	next
	
	if SomeAdded then ClearanceAdded = true
	
end if


sql = "SELECT DISTINCT sz.SizeID, sz.SizeName, sz.SizeType, sz.SizeZorder FROM SML_SizeList sz ORDER BY SizeType, SizeZorder "

x = getrs(sql, SizeArr, SizeC)	





%>

<link rel="stylesheet" href="css/index.css" type="text/css" media="screen" />

	<div class="container">      

		 <div class="row">      
			<div class="span12 spacer10">
		
				<h3 class=spacer40><%=lne("page_title_recent_arrivals")%><br><small><%=lne("page_title_recent_arrivals_subtitle")%></small></h3>	
				
				<%
				
				sql = "exec [spB2BRecentArrivals] 0" & AcDetails.SelectedLanguageID
				
				jhAdmin sql
											
				x = getrs(sql, ProdArr, ProdC)
				jhAdmin "ProdC = " & ProdC
				'ProdC = -1

				jhAdmin "DRAW LIST!"
								
				set ls = new strconcatstream
				set lsd = new strconcatstream
				
				ls.add "<table class=ProductDetailSKUGrid border=1>"
								
				for c = 0 to ProdC
					
					ThisProductID = ProdArr(0,c)
					ThisProductCode = ProdArr(1,c)
					ThisProductName = ProdArr(2,c)
					ThisProductName_TRANS = "" & ProdArr(3,c)
					
					DispName = ThisProductName
					if ThisProductName_TRANS <> "" then
						DispName = ThisProductName_TRANS
					end if
					
					ThisVariant = ProdArr(4,c)
					
					ThisColor = ProdArr(5,c)
					ThisColor_TRANS = ProdArr(6,c)
					DispColor = ThisColor
					
					ThisMAI = ConvertNum(ProdArr(10,c))
					
					ThisDateFirstAppeared = ProdArr(9,c)
					
					WebColor = "" & ProdArr(11,c)
					if WebColor = "" then WebColor = "silver"
					LabelStyle = "background:" & WebColor
					
					imgURL = getImageDefaultCDN(ThisProductCode, ThisVariant)
					
					ShowThis = true
					
					if ThisMAI <= 0 then ShowThis = false
					
					if ShowThis then
						
						lsd.add "<div class=""span3 text-center ProductContainer"">"
						lsd.add "<a href=""/products/?ProductID=" & ThisProductID & "&InitCode=" & ThisVariant & """>"		
						lsd.add "<img style=""height:140px;"" src=""" & imgURL & """>"
						lsd.add "</a>"		
						lsd.add "<div class=xlegible><strong>" & DispName & "</strong></div>"
						lsd.add "<div class=xlegible>"
						lsd.add "</div>"	
						lsd.add "</div>"
						
					end if	
				next
								
				ls.add "</table>"

				
				if 	ProdC >= 0 then				
					osd = lsd.value
				else
					osd = "<div class=span12>" & lne("page_title_recent_arrivals_no_records") & "</div>"
				end if

				%>
				<div class=row>
					<%
					=osd
					%>
				</div>
				<hr>
				<%	
					
				'response.write os

%>		
			
		
			</div>
		</div>
	</div>

<!--#include virtual="/assets/includes/footer.asp" -->


<%


%>


<script>
	equalheight = function(container){

	var currentTallest = 0,
		 currentRowStart = 0,
		 rowDivs = new Array(),
		 $el,
		 topPosition = 0;
	 $(container).each(function() {

	   $el = $(this);
	   $($el).height('auto')
	   topPostion = $el.position().top;

	   if (currentRowStart != topPostion) {
		 for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
		   rowDivs[currentDiv].height(currentTallest);
		 }
		 rowDivs.length = 0; // empty the array
		 currentRowStart = topPostion;
		 currentTallest = $el.height();
		 rowDivs.push($el);
	   } else {
		 rowDivs.push($el);
		 currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
	  }
	   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
		 rowDivs[currentDiv].height(currentTallest);
	   }
	 });
	}

	$(document).load(function() {
	  equalheight('.NameContainer');
	});


	$(window).resize(function(){
	  equalheight('.NameContainer');
	});		
</script>