<%
PageTitle = "NEW B2B Home page"

Response.ContentType = "text/html"
Response.AddHeader "Content-Type", "text/html;charset=ISO-8859-1"
'Response.CodePage = 65001
Response.CharSet = "ISO-8859-1"

Response.Expires = 0
Response.Expiresabsolute = Now() - 1442
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"

%>

<!--#include virtual="/assets/includes/header.asp" -->

<%
if not isDealer() and isAdminUser then
	response.redirect "/admin/control/"
end if


ShowAnnouncements = cleannum(request("ShowAnnouncements"))
if ShowAnnouncements = "1" then session("B2B_ReadAnnouncements") = ""

function GetOrderDespatchStats()
	'Temporary
	if false then
		dim sql, ProcArr, ProcC, c, DespatchPct, TotUnprocSum, TotProcSum, TTTitle
		sql = "SELECT TOP 5 TotalUnprocNo, TotalProcNo FROM ExchequerProcessOrdersReport ORDER BY ReportDate DESC"
		
		'jhAdmin sql	
		x = getrs(sql, ProcArr, ProcC)
		
		for c = 0 to ProcC
			TotUnprocSum = TotUnprocSum + CInt(ProcArr(0,c))
			TotProcSum = TotProcSum + CInt(ProcArr(1,c))		
		next
		
		'jhAdmin "TotUnprocSum = " & TotUnprocSum
		'jhAdmin "TotProcSum = " & TotProcSum
			
		if TotProcSum > 0 then 
			
			HeadingStr = lne_NOEDIT("order_despatch_stats")
			TitleStr = lne_NOEDIT("order_despatch_explained")
			
			DespatchPct = Round(100 - (TotUnprocSum / TotProcSum) * 100, 0) & "%"
			GetOrderDespatchStats = "<span id=""OrderStats"" data-toggle=""tooltip"" title=""" & TitleStr & """ class=""label label-success"">" & HeadingStr & ":&nbsp;" & DespatchPct & "</span><br>"		
		end if
	end if	
end function

'if isjh() then
	
	DespatchStats =  GetOrderDespatchStats()
	
'end if

%>

<style>
	.HomePanelLayer {
		color:white;
	}
	.header-panel {		
		color:white;
		
	}
	
	A.whitelink {
		color:white;
	}
	
	.header-title {		
		color:white;
		font-size:36pt;
		font-family:Oxygen, arial;
		text-align:left;
	}	
	
	.header-trans {
		background-image:url(/assets/images/home_panels/semi-trans.png);
		padding:20px 0 20px 0;
		xmargin-top:20px;
		
	}
	
	.header-button {
		text-align:left;
	}
	
	#slide1, #slide2, #slide3, #slide4 {
		border:0px solid red;
		padding-top:50px;
		height:200px;
	}
	
	#feature_slider {
		height:250px !important;
	}
	
	.slide {
		height:220px;
	}
	
	.header-title {
		line-height:30pt;font-size:24pt;
	}
	
	.colSpacer_LEFT {
		border-left:1px solid #f0f0f0;
		padding-left:20px;
	}
	
	.ProductGrid TD {
		font-family:arial;
		font-size:10pt;
		padding:5px;
	}
	
	.legible {
		font-family:arial;
		font-size:8pt;
	}
	
	.basketAddButton {
		border:1px solid red;
	}
	
	.HomeSearch {
		border-bottom:1px solid #ddd;
		padding:10px;
		xborder-radius:5px;
	
	}
</style>

<%



%>

	<div class="container">      

		  <div class="row">      
			<div class="span12 spacer10">
				<div class=pull-right>
		<%
		AnnounceStr = getAnnouncements(announceCount)
		if announceCount > 0 then
%>			
				<a class="badge badge-important" href="/?ShowAnnouncements=1"><i class="icon-flag icon-white"></i> <%=lne("label_special_announcements")%>  <%=(announceCount)%></a>		
<%		
		end if		
		
		%>			
				</div>
				

				
				<h2><%=lne("page_title_home")%><br>
					<small><%=lne("page_title_home_subtitle")%></small>
				</h2>
				<div id="AnnounceDiv"></div>
				<div class=xpull-right>
					<%
					=DespatchStats
					%>
				</div>				
				
					<%
					'jhInfoNice "Login notification", "You are logged in as [AccountCode] [+ Admin user name if applicable]"
					
					BigLanguageNotification = request.cookies("BigLanguageNotification")
					
					if BigLanguageNotification = "" or request("ShowBigLanguageNotification") = "1" then
						'jhInfo "Language selector!"
						response.write DrawBigLanguageNotification	
						
					end if
					%>
					
					<%
					=announceStr
					%>
				
					<hr>
			</div>
		</div>	
		

		
		 <div class="row">      
			<div class="span7">	
			
				
				<h4><i class=icon-search></i> <%=lne("home_product_browser_title")%></h4>
				
				<div id="AjaxNotificationDiv"></div>
				
<ul id=SearchTabs class="nav nav-tabs small legible">
  <li id=li0 class="active"><a href="#tab1" data-toggle="tab" onclick="$('#QuickSearchVal').focus();$('#QuickSearchResults').hide();"><%=lne("home_tab_search")%></a></li>
  <li id=li2><a href="#tab2" onclick="loadRanges();$('#QuickSearchResults').hide();" data-toggle="tab"><%=lne("home_tab_collections")%></a></li>
  <li id=li5><a href="#tab5" onclick="loadTypes();$('#QuickSearchResults').hide();" data-toggle="tab"><%=lne("home_tab_type")%></a></li>
  <li id=li3><a href="#tab3" data-toggle="tab" onclick="loadEndUse();$('#QuickSearchResults').hide();"><%=lne("home_tab_enduse")%></a></li>
  <li id=li4><a href="#tab4" class=RecentSearchLink onclick=";$('#QuickSearchResults').hide();" data-toggle="tab"><%=lne("home_tab_recent")%></a></li>
  <li id=li4><a href="#tab6" class=FavouriteSearchLink onclick=";$('#QuickSearchResults').hide();" data-toggle="tab"><%=lne("label_favourites")%></a></li>
</ul>

<div class="tab-content">
	<div class="tab-pane active" id="tab1">
	
		<div class="HomeSearch spacer20">

			<div class="form-inline">
			  <input type=text class=form-control style="width:180px;" id="QuickSearchVal"> 	
			  <button id=QuickSearchButton class="btn btn-small btn-warning"><i class="icon-white icon-search"></i> <%=lne("btn_search_home")%></button>
			  <input class=FormHidden type=hidden id="AutoCompleteProdCode"> 
			</div>	
	
<%
if isjh() then
%>
				
<%
end if
%>	
	
		</div>
	</div>
	<div class="tab-pane" id="tab2">
		<div id=RangeDiv>
			
		</div>
				
	</div>
	<div class="tab-pane" id="tab3">
		<div id=EndUseDiv>
			
		</div>
	</div>
	
	<div class="tab-pane" id="tab4">
		<h4><%=lne("label_recently_viewed")%></h4>
	</div>
	
	<div class="tab-pane" id="tab6">
		<h4><%=lne("label_favourites")%></h4>
	</div>	
	
	<div class="tab-pane" id="tab5">
		
		<div id="ProductTypeDiv" class=spacer20>		
			
		</div>
		
		
		
	</div>	
</div>
				
				

					
					<div zstyle="border:1px solid #ccc;border-radius:10px;padding:10px" id="QuickSearchResults">
						<p><small>Search for a product, results will appear below:</small></p>
					</div>
				

				<div class=spacer40></div>
			</div>
			
			<div class="span5">	
			
				<div class="colSpacer_LEFT">
								
				<h4><%=lne("home_heading_import")%></h4>
				<p><a class=OrangeLink href="/utilities/import/"><%=lne("label_import_explained")%></a></p>
				
										
					
					<hr>
					<!--YOUR ACCOUNT MANAGER-->
					<h4><%=lne("home_heading_acm")%></h4>
<%
	jhNice "",  getContactInfo("ACM")

%>
					<!--YOUR ASE-->
					<h4><%=lne("home_heading_ase")%></h4>
<%
	jhNice "", getContactInfo("ASE")

%>


					
					<ul class="nav nav-tabs small legible">
						<li class=active><a href="#tabNews" data-toggle="tab"><%=lne("home_heading_news")%></a></li>
						<li><a href="#tabReviews" data-toggle="tab"><%=lne("home_heading_reviews")%></a></li>
					</ul>					

					<div class="tab-content">
						<div class="tab-pane active" id="tabNews">
							<div style="padding-left:12px;" id="AjaxBlogLatest">

							</div>  
						</div>
						<div class="tab-pane" id="tabReviews">
							<div style="padding-left:12px;" id="AjaxReviewsLatest">

							</div> 
							<div style="padding-left:12px;">								
								<a class=OrangeLink target="_BLANK" href="http://www.endurasport.com/products/reviews/"><i class="icon icon-chevron-right"></i> <%=lne("label_see_more_reviews")%></a>
							</div> 							
							
						</div>  
					</div>
					
					
					<hr>


				</div>
			</div>

			
		 </div>
		
	</div>

				

			

<!--#include virtual="/assets/includes/footer.asp" -->

<script type="text/javascript" src="js/index-slider.js"></script>	




<script>

		$( "#QuickSearchVal" ).focus( function (){
			$( "#QuickSearchVal" ).val('');			
		});
				
		$( "#QuickSearchVal" ).autocomplete({		  
		  minLength: 3, 
		  source: availableProds		  
		  ,		  
			  change: function (event, ui) {
					if (!ui.item) {
						 $('#AutoCompleteProdCode').val('');
					 }
				}			  
			  ,
			  
			select: function (event, ui) {
					
					var selProd = ui.item.prodCode;					
					$('#AutoCompleteProdCode').val(selProd);
					
					this.value = ui.item.label;
					
					if (selProd != '') {
						getProductQuickSearch(selProd,"TEXT")
					}
					
					$( "#QuickSearchVal" ).val('');		
					
					return false;
				}
		  
		});	





	$("#QuickSearchButton").click( function() {
	
			var sText = $("#QuickSearchVal").val()
			
			if (sText!='') {	
				var autocompleteText = $("#AutoCompleteProdCode").val()		
				getProductQuickSearch(sText,"TEXT")
			}
		}	
	);

	function rangeSearch(pid) {
			var clickedRange = pid
			//alert(clickedRange)
			getProductQuickSearch(clickedRange,"RANGE")
		}	
	
	
	//$(".CategorySearchLink").click( function() {
	function categorySearch(pid) {
			var clickedCategory = pid;
			getProductQuickSearch(clickedCategory,"CATEGORY")
		}	
	//);
	
	//$(".ProductTypeSearchLink").click( function() {
	function productTypeSearch(pid) {
	
			var clickedType = pid;
			getProductQuickSearch(clickedType,"PRODUCTTYPE")
		}	
	//);	
	
	$(".RecentSearchLink").click( function() {			
			getProductQuickSearch('',"RECENT")
		}	
	);
			
	$(".FavouriteSearchLink").click( function() {			
			getProductQuickSearch('',"FAVOURITES")
		}	
	);			
	
	function getProductQuickSearch(pSearchVal, pType) {
		
		$('#QuickSearchResults').show(100, function() {
			// Animation complete.
		  });				
		$("#QuickSearchResults").html('<span class="AjaxLoader"><img src="/assets/images/icons/indicator.gif"> loading...</span>');
			
		if (pType == 'TEXT') {  //check to see if autocomplete function has been used, if so, just pass code...
			var checkVal = $("#AutoCompleteProdCode").val()
			var checkValLength = $("#AutoCompleteProdCode").val().length;
			if (checkValLength > 0) {
				if (checkVal == pSearchVal.substring(0, checkValLength)) {
					pSearchVal = checkVal
				}
			}			
		}	

		$.ajax({
			scriptCharset: "ISO-8859-1" ,
			contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
			url: "/product_server.asp?CallType=SEARCH&SearchVal=" + pSearchVal + "&SearchType=" + pType + "",
			cache: false,			
			}).done(
				function( html ) {
				$("#QuickSearchResults").html(html);
				$('#QuickSearchResults').show(100, function() {
					// Animation complete.
				  });			

				var ProductCount_RESULTS = parseInt($("#ProductCount_RESULTS").val())
				
				if (ProductCount_RESULTS == 1 ) {
				
					$(".GridLink").click()
	
				}
				  
			});
		
	}
	
	function toggleGrid(pid) {
		$("#ProductTable_" + pid).toggle(100, function() {
					// Animation complete.
				  })
	}
	
	function getGrid(pProductID) {

		if ($("#GRID_" + pProductID).hasClass('GridHidden')) {
			$("#GRID_" + pProductID).show(100, function() {
				// Animation complete.
				$("#GRID_" + pProductID).addClass('GridVisible')		
				$("#GRID_" + pProductID).removeClass('GridHidden')					
			  });	
		}		
		else if ($("#GRID_" + pProductID).hasClass('GridVisible')) {
			//alert('visible');
			$("#GRID_" + pProductID).hide(100, function() {
						// Animation complete.
			});	
			
			$("#GRID_" + pProductID).addClass('GridHidden')		
			$("#GRID_" + pProductID).removeClass('GridVisible')	
			
			$("#showHideIndicator_" + pProductID).removeClass('icon-chevron-up')
			$("#showHideIndicator_" + pProductID).addClass('icon-chevron-down')			
		} else {
			
			$("#GRID_" + pProductID).html('<span class="AjaxLoader"><img src="/assets/images/icons/indicator.gif"> loading...</span>');
				
			$.ajax({
				//contentType: "application/x-www-form-urlencoded;charset=ISO-8859-1",
				//dataType: 'json',
				scriptCharset: "ISO-8859-1" ,
				contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
				url: "product_server.asp?CallType=QUICK_GRID&ProductID=" + pProductID,
				cache: false,			
				}).done(
					function( html ) {
					$("#GRID_" + pProductID).html(html);
					$("#GRID_" + pProductID).show(100, function() {
						// Animation complete.
					  });				
					$("#GRID_" + pProductID).addClass('GridVisible')
					$("#showHideIndicator_" + pProductID).removeClass('icon-chevron-down')
					$("#showHideIndicator_" + pProductID).addClass('icon-chevron-up')
				});
		
		}
	}
	
	
	function loadRanges() {
		//alert('ran')
		$.ajax({
			//contentType: "application/x-www-form-urlencoded;charset=ISO-8859-1",
			//dataType: 'json',
			scriptCharset: "ISO-8859-1" ,
			contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
			url: "product_server.asp?CallType=LOAD_RANGES",
			cache: false,			
			}).done(
				function( html ) {
				$("#RangeDiv").html(html);
				$("#RangeDiv").show(100, function() {
					// Animation complete.
				  });					
			});
	}
	
	function setSortOrder(pSortOrder) {
		$.ajax({
			scriptCharset: "ISO-8859-1" ,
			contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
			url: "product_server.asp?CallType=SET_SORT_ORDER&SetSortOrder=" + pSortOrder,
			cache: false,			
			}).done(
				function( html ) {
					
					$("#AjaxNotificationDiv").html(html);
					getProductQuickSearch('','LAST')
		});		
	}
		
	
	function loadTypes() {
	
		$.ajax({
			scriptCharset: "ISO-8859-1" ,
			contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
			url: "product_server.asp?CallType=LOAD_TYPES",
			cache: false,			
			}).done(
				function( html ) {
				$("#ProductTypeDiv").html(html);
				$("#ProductTypeDiv").show(100, function() {
					// Animation complete.
				  });					
			});
	}	
	
	function loadEndUse() {
	
		$.ajax({
			scriptCharset: "ISO-8859-1" ,
			contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
			url: "product_server.asp?CallType=END_USE",
			cache: false,			
			}).done(
				function( html ) {
				$("#EndUseDiv").html(html);
				$("#EndUseDiv").show(100, function() {
					// Animation complete.
				  });					
			});
	}	
	
	
	
</script>




<script>
	function getBlogLatest() {
		//alert('in')
		//$('#ajax_prod_results').fadeout('fast')
		$.ajax({
			//contentType: "application/x-www-form-urlencoded;charset=ISO-8859-1",
			//dataType: 'json',
scriptCharset: "ISO-8859-1" ,
contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
			url: "/blog/blog_server.asp?BlogAction=LATEST&FilterBy=-1",
			cache: false,			
			}).done(
				function( html ) {
				$("#AjaxBlogLatest").html(html);
				$('#AjaxBlogLatest').show(100, function() {
					// Animation complete.
				  });					
			});
		
	}
	
	function getReviewsLatest() {
		//alert('in')
		//$('#ajax_prod_results').fadeout('fast')
		$.ajax({
			//contentType: "application/x-www-form-urlencoded;charset=ISO-8859-1",
			//dataType: 'json',
scriptCharset: "ISO-8859-1" ,
contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
			url: "/blog/blog_server.asp?BlogAction=LATEST_REVIEWS&FilterBy=-1",
			cache: false,			
			}).done(
				function( html ) {
				$("#AjaxReviewsLatest").html(html);
				$('#AjaxReviewsLatest').show(100, function() {
					// Animation complete.
				  });					
			});
		
	}	
	
	getBlogLatest(0);
	getReviewsLatest(0);

	$('#QuickSearchVal').keypress(function (e) {
	  if (e.which == 13) {
		getProductQuickSearch($("#QuickSearchVal").val(),"TEXT")
		return false;    //<---- Add this line
	  }
	});		
	
	
	$('#OrderStats').tooltip({placement: "right"})
	
	function clearFavourites() {
		
		$.ajax({
			//contentType: "application/x-www-form-urlencoded;charset=ISO-8859-1",
			//dataType: 'json',
			scriptCharset: "ISO-8859-1" ,
			contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
			url: "/misc_server.asp?CallType=CLEAR_FAVOURITES",
			cache: false,			
			}).done(
				function( html ) {
				
					getProductQuickSearch('',"FAVOURITES")
				
				  });					
			
	}
	
	function favouriteClick(pAction, pProductID) {
		
		$.ajax({
			//contentType: "application/x-www-form-urlencoded;charset=ISO-8859-1",
			//dataType: 'json',
			scriptCharset: "ISO-8859-1" ,
			contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
			url: "/misc_server.asp?CallType=ADD_FAVOURITE&CallContext=" + pAction + "&ProductID=" + pProductID,
			cache: false,			
			}).done(
				function( html ) {
				
					//alert('HTML')
				
					var favDiv = "fav_" + pProductID
				
					if(document.getElementById(favDiv)) {
						document.getElementById(favDiv).innerHTML = html;
					}
				
				  });					
			
	}	
	
	
	
</script>


<%
		If (BBLDealerStatus = "BBLONLY") Then

			response.cookies("B2BQuickSearch")("SearchType") = "RANGE"
			response.cookies("B2BQuickSearch")("SearchVal") = BBLRangeID
			response.cookies("B2BQuickSearch").expires = dateadd("d", 5, now)	
		End If
		
	AutoSearchType = request.cookies("B2BQuickSearch")("SearchType")
	AutoSearchVal = request.cookies("B2BQuickSearch")("SearchVal")
	if AutoSearchType = "TEXT" then SearchVal = AutoSearchVal else SearchVal = ""
	
	searchVal_HEADER = request.form("searchVal_HEADER")

	if searchVal_HEADER <> "" then
		AutoSearchVal = searchVal_HEADER 
		SearchVal = searchVal_HEADER 
		AutoSearchType = "TEXT" 
	end if
	
	if AutoSearchType <> "" then 
		
		AutoTab = "0"
		
		select case AutoSearchType
			case "TEXT"
				AutoTab = "0"
			case "RANGE"
				AutoTab = "1"
			case "PRODUCTTYPE"
				AutoTab = "2"
			case "ENDUSE"
				AutoTab = "3"			
			case "RECENT"
				AutoTab = "4"
				
		end select

	end if
%>

<%
SearchVal = ucase(request("SearchVal"))
if SearchVal <> "" then	
%>
	<script>
		$("#QuickSearchVal").val('<%=SearchVal%>');
		getProductQuickSearch('<%=SearchVal%>','TEXT')
	</script>
<% else %>
	<script>
	
		$("#QuickSearchVal").val('<%=SearchVal%>');

			
		<% if AutoSearchVal <> "" then %>
			getProductQuickSearch('<%=AutoSearchVal%>','<%=AutoSearchType%>');
		<% End If %>

				
	</script>
<%
end if
%>




<%

dbClose()

%>

