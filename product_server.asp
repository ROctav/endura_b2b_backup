<%
Response.ContentType = "text/html"
Response.AddHeader "Content-Type", "text/html;charset=ISO-8859-1"
'Response.CodePage = 65001
Response.CharSet = "ISO-8859-1"

Response.Expires = 0
Response.Expiresabsolute = Now() - 1442
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"

SelectedLanguageID = getSelectedLanguage()
ThisUserID = session("B2B_AcCode")

if request("logout") = "1" then
	session("B2B_AcCode") = ""
	session("B2B_AdminUserID") = ""
end if

%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	</head>
	<body>
	
<!--#include virtual="/assets/includes/global_functions.asp" -->
<!--#include virtual="/assets/includes/product_functions.asp" -->
<%

if not isLoggedIn() then 
	'jhStop
	jhErrorNice "Time out", "Your session has timed out<br><a class=OrangeLink href=""/"">click here to log in again</a>"
	dbClose
	response.end
end if

jhAdmin "PROD SERVER"

EndUseID = cleannum(request("EndUseID"))
EndUseName = request("EndUseName")

ThisUserID = session("B2B_AcCode")

CallType = request("CallType")
if CallType = "" then CallType = "BROWSER"

CallContext = request("CallContext")
'jhAdmin "CallContext = " & CallContext

'j'hAdmin "CallType = " & CallType
'jhstop

'jhInfo "GET LAST SAVED POSITION?"

ThisQS = request.servervariables("QUERY_STRING")
'jh "ThisQS = " & ThisQS

if CallType = "SET_SORT_ORDER" then
	
	SetSortOrder = request("SetSortOrder")
	
	'jhAdmin "SetSortOrder =  " & SetSortOrder
	
	if SetSortOrder <> "" then
		
		response.cookies("SearchSortOrder") = SetSortOrder
		response.cookies("SearchSortOrder").expires = dateadd("d", 365, now())
		
		'jhInfo "SET.."
	end if
	
	

elseif CallType = "STOCK_ENQUIRY" then
	
	set acDetails = new DealerAccountDetails

	CheckProductID = cleannum(request("ProductID"))
	CheckVariantCode = request("VariantCode")
	CheckSKU = trim("" & request("SKU"))
	
	CurrentDate = ExDate(now)
	
	
    	sql = "exec [spB2BStockEnquiry] 0" & CheckProductID & ", '" & CheckVariantCode & "', '', 30, " & CurrentDate & ", 0"
	'jh sql
	dbConnect()
	
	x = getrs(sql, ProdArr, ProdC) 
	
	
	if ProdC >= 0 then
		
		ThisProductCode = ProdArr(1,0)
		ThisProductName = ProdArr(3,0)
	
%>
		<h3 class=spacer20><a href="/products/?ProductID=<%=CheckProductID%>&InitCode=<%=CheckVariantCode%>"><%=ThisProductName%></a><br><span class="badge badge-inverse"><%=ThisProductCode%></span></h3>	
		
<%
		
		intESAMonthsCutOff =  3

		set ls = new strconcatstream
		
		LastVariant = ""
		
		MaxResupplyDates = 3
		
		NoDatesLabel = lne("label_no_current_dates_explained")
		
		NowStr = lne("label_stock_enquiry_now")
		
		for c = 0 to ProdC
				
			ThisVariantCode = ProdArr(2,c)
			ThisColorName = ProdArr(4,c)
								
				if LastVariant <> ThisVariantCode then
					
					'jhAdmin "VARIANT"
					
					LastSKU = ""
					
					LastVariant = ThisVariantCode
					ThisThumb = getImageDefaultCDN(ThisProductCode, ThisVariantCode)
					
					ls.add "<hr>"	
					
					ls.add "<table class=ProductDetailSKUGrid border=1>"
					ls.add "<tr>"
					
					
					ls.add "<td style=""width:90px;"" rowspan=4 valign=top>"
					ls.add "<div>"
					ls.add "<a href=""/products/?ProductID=" & CheckProductID & "&InitCode=" & ThisVariantCode & """>"
					ls.add "<img src=""" & ThisThumb & """ class=""ProdThumb90 img-responsive""></div>"
					ls.add "</a>"
					ls.add "<h5>" & ThisColorName & "</h5>"
					ls.add "</td>"
				
					for x = c to ProdC	
						ThisSKU = ProdArr(8,x)
						
						if ProdArr(2,x) = ThisVariantCode and ThisSKU <> LastSKU then

							ThisSize = ProdArr(5,x) 
														
							'Hardcoded deliberately
							strOverwrittenSize = ShowOverwrittenSizeName(ThisSKU, ThisSize)
							'
							
							if CheckSKU = ThisSKU then SelClass = "enquiry_selected_sku" else SelClass = ""
							
							ls.add "<td class=""" & SelClass & """ align=middle><strong>" & strOverwrittenSize & "</strong></td>"
							
							LastSKU = ThisSKU
						end if
					next							
					
					ls.add "</tr>"
					
					ls.add "<tr>"
				
					for x = c to ProdC	
						ThisSKU = ProdArr(8,x)
						
						if ProdArr(2,x) = ThisVariantCode and ThisSKU <> LastSKU then
							ThisSize = ProdArr(5,x)
							
							if CheckSKU = ThisSKU then SelClass = "enquiry_selected_sku" else SelClass = ""
							
							ls.add "<td class=""" & SelClass & " subtle small"" align=middle><small>" & ThisSKU & "</small></td>"
							
							LastSKU = ThisSKU
						end if
					next							
					
					ls.add "</tr>"
					
					ls.add "<tr>"					
					
					LastSKU = ""
					
					for x = c to ProdC	
						ThisSKU = ProdArr(8,x)
						if ProdArr(2,x) = ThisVariantCode and ThisSKU <> LastSKU then
							ThisMAI = convertnum(ProdArr(7,x))
							
							if CheckSKU = ThisSKU then SelClass = "enquiry_selected_sku" else SelClass = ""							
							ls.add "<td class=""text-center " & SelClass & """><strong>"							
							ls.add  dispStock(ThisMAI)
							ls.add "<div class=spacer10>"
							ls.add "<nobr>" & NowStr & "</nobr>"
							ls.add "</div>"
							ls.add "</strong></td>"
							
							LastSKU = ThisSKU
						end if
					next							
					
					ls.add "</tr>"					
					
					ls.add "<tr>"
					'ls.add "<td valign=top>NEXT</td>"
					
					LastSKU = ""
					
					for x = c to ProdC					
												
						DatesFound = 0
					
						ThisSKU = ProdArr(8,x)
						
						if ProdArr(2,x) = ThisVariantCode and ThisSKU <> LastSKU then
						
							if CheckSKU = ThisSKU then SelClass = "enquiry_selected_sku" else SelClass = ""							
							ls.add "<td class=""text-center " & SelClass & """ valign=top><strong>"		
							
							for y = x to ProdC
								if ProdArr(8,y) = ThisSKU  then
									
									
									NextDate = ProdArr(10,y)
									NextQty = ProdArr(11,y)									

									if isdate(NextDate) then
										 
										DatesFound = DatesFound + 1

										If CDate(NextDate) > DateAdd("m",intESAMonthsCutOff,Now) AND DatesFound > 1 Then 
											Exit For
										End If
										DispDate = shortdate(NextDate)
										
										ls.add "<div class=spacer0>"
										ls.add "<span class=""label label-inverse"">" & dispArrivals(NextQty) & "</span>"										
										ls.add "</div>"																						
										ls.add "<div class=spacer10>"
										ls.add "<nobr>on <span class=""next_date"">" & shortdate(NextDate) & "</span></nobr>"
										ls.add "</div>"																																						
									end if									
									
								end if
							next
							
							if DatesFound = 0 then
								ls.add "<div title=""" & NoDatesLabel & """ class=tiny>" & lne("label_delivery_TBC") & "</div>"	
							end if							
							
							ls.add "</td>"
						
							LastSKU = ThisSKU
							'jhAdmin "SET LastSKU = " & LastSKU
						end if
					next							
					
					ls.add "</tr>"							
					
					
					ls.add "</table>"	
					
					ls.add "<div class=""clear-both spacer20""></div>"	
					
					
				end if
				
			'jh ProdArr(5,c)
			
		next
		
		os = ls.value
		
		dbClose()
		
		
	end if
	
	response.write os
	
	set ls = nothing
	
	killarray ProdArr

elseif CallType = "QUICK_GRID" then
	
	ProductID = cleannum(request("ProductID"))
	
	set acDetails = new DealerAccountDetails
						
	GridOS = productGrid (ProductID, "")
	
	response.write GridOS
	
	set ls = nothing	
	

elseif CallType = "SEARCH" then

	set acDetails = new DealerAccountDetails

	SearchVal = trim("" & request("SearchVal"))
	SearchType = request("SearchType")
		
	if SearchType = "LAST" then
		SearchVal = session("LastSearchVal")
		SearchType = session("LastSearchType")
	else
		session("LastSearchVal") = SearchVal
		session("LastSearchType") = SearchType
	end if
		
	if SearchType = "TEXT" then			
		SlashPos = instr(1, SearchVal, "/")
		if SlashPos > 0 then
			TextVal = left(SearchVal, SlashPos-1)
			'jhAdmin "Amended = " & SearchVal
		else
			TextVal = SearchVal	
		end if		
	elseif SearchType = "RANGE" then
		RangeVal = SearchVal
		RangeSearch = true
		SearchRangeID = SearchVal
		
	elseif SearchType = "CATEGORY" then
		CategoryVal = SearchVal
	elseif SearchType = "PRODUCTTYPE" then
		ProductTypeVal = SearchVal
	end if			
		
	'jh acDetails.CostBand	
		
	ThisSortOrder = request.cookies("SearchSortOrder")
	if ThisSortOrder = "" then ThisSortOrder = "PRICE_HI"	
		
	if SearchType = "RECENT" then
		sql = "exec spB2BRecentlyViewed '" & validstr(ThisUserID) & "' , " & acDetails.SelectedLanguageID  & " "
	elseif SearchType = "FAVOURITES" then
		sql = "exec spB2BQuickSearch_FAVOURITES '" & validstr(ThisUserID) & "' , " & acDetails.SelectedLanguageID  & ", '" & ThisSortOrder & "' "		
	else
	
		
		strProdLiveFilter = -1 
		strProdDiscFilter = -1
		strProdLimFilter = -1
		strProdClearFilter = -1 
		strProdForthFilter = -1

		If Session("B2B_ProductSearchCurrent") = "1" Then 
			strProdLiveFilter = 1 
			strProdDiscFilter = 4
			strProdLimFilter = 90
		End If
		
		If Session("B2B_ProductSearchClearance") = "1" Then 
			strProdClearFilter = 5 
		End If
		
		If Session("B2B_ProductSearchFuture") = "1" Then 
			strProdLimFilter = 90
			strProdForthFilter = 3 
		End If
		
		sql = "exec spB2BQuickSearch '" & validstr(AcDetails.AcCode) & "', '" & validstr(TextVal) & "', '" & validstr(RangeVal) & "', '" & validstr(CategoryVal) & "', '" & validstr(ProductTypeVal) & "', " & acDetails.SelectedLanguageID  & ", '" & ThisSortOrder & "', 0"
		sql = sql & ", " & strProdLiveFilter & ", " & strProdDiscFilter & ", " & strProdLimFilter & ", " & strProdClearFilter & ", " & strProdForthFilter 
			
	end if
	
	x = getrs(sql, ProdArr, ProdC)

	os = ""
	LastProd = ""
	
	set ls = new strconcatstream
	
	ProductCount = 0
	
	LastProd = ""
	
	ProductDetailPageStr = lne("label_go_to_product_detail") 
	ProductStockEnquiryStr = lne("label_stock_enquiry_product") 
	FavouriteAddStr = lne("label_favourites_add_explained")
	FavouriteRemoveStr = lne("label_favourites_remove_explained")
	NewReviewStr = lne("label_browser_new_reviews")
	CountReviewStr = lne("label_browser_latest_reviews")
	
	if glPromoActive then
		PromoNameStr = lne("label_promo_name")
		PromoTeaserStr = lne("label_promo_browser")
	end if
	
	LastProductTypeID = ""
	
	for c = 0 to ProdC
	
		ThisProductID = ProdArr(0,c)
		ThisProductCode = ProdArr(1,c)
		ThisVariant = ProdArr(4,c)
		ThisProductName = ProdArr(2,c)
		ThisOneLiner = ProdArr(3,c)
	
		ProductFavouriteID = "" & ProdArr(12,c)
		if ProductFavouriteID <> "" then
			ThisIsFavourite = true
		else
			ThisIsFavourite = false
		end if
	
		if AcDetails.SelectedLanguageID <> 0 then
			'jhAdmin "TRANS?"
		end if
	
		if LastProductID <> ThisProductID then
		
				if SearchType <> "RECENT" and ThisSortOrder = "PRODTYPE" then
					ThisProductTypeID = ProdArr(13,c)
					ThisProductTypeName = "" & ProdArr(14,c)
					ThisProductTypeName_LONG = "" & ProdArr(15,c)
					if AcDetails.SelectedLanguageID <> 0 then ThisProductTypeName_TRANS = "" & ProdArr(16,c)
					
					if ThisProductTypeName_TRANS  <> "" then
						DispProductTypeName = ThisProductTypeName_TRANS
					elseif ThisProductTypeName_LONG <> "" then 
						DispProductTypeName = ThisProductTypeName_LONG
					else 
						DispProductTypeName = ThisProductTypeName
					end if
					'jhAdmin "ThisProductID = " & ThisProductID
					if ThisProductTypeID <> LastProductTypeID then
						ls.add "<hr><h4 style=""border-bottom:3px solid black;padding-bottom:5px;"">" & DispProductTypeName & "</h4>"
						LastProductTypeID = ThisProductTypeID
					end if
				end if
		
			BOStr = ""
			ThisOutstanding = cdbl(ProdArr(6,c))
			BOStr = dispBO(ThisOutstanding)
			
			ThisFO = cdbl(ProdArr(9,c))
			FOStr = dispFO(ThisProductCode, ThisFO)			
			
			'jhAdmin "FOStr = " & FOStr
			LastProductID = ThisProductID
			
			ThisThumb = getImageDefaultCDN(ThisProductCode, ThisVariant)
			
			if SearchType = "RECENT" then
				LatestReviewCount = convertnum(ProdArr(13,c))
				LatestReviewDD = convertnum(ProdArr(14,c))			
			else
				LatestReviewCount = convertnum(ProdArr(17,c))
				LatestReviewDD = convertnum(ProdArr(18,c))
			end if
			
			ColorStr = ""
			for z = c to ProdC
				if ProdArr(0,z) = ThisProductID then	
					ThisCC = ProdArr(7,z)
					ColorBar = getProductColorBar(ThisCC)
					if ColorBar <> "" then
						ColorStr = ColorStr & "<span class=label style=""height:3px;;background-image:url(" & colorbar & ");"">&nbsp;</span>&nbsp;"
					end if
				else
					exit for
				end if
			next
			
					
			ls.add "<hr>"
			
			ls.add "<div class=""pull-right text-right"">"
			ls.add "<a title=""" & ProductDetailPageStr & """ class=""GridLink"" href=""/products/?ProductID=" & ThisProductID & """><img src=""" & ThisThumb & """ class=""ProdThumb40""></a>"
			
			ls.add "<div>" & ColorStr & "</div>"
			
			ls.add "</div>"
			
			ls.add "<a class=""GridLink OrangeLink"" style=""cursor:pointer;"" onclick=""getGrid('" & ThisProductID & "');""><i id=""showHideIndicator_" & ThisProductID & """ class=icon-chevron-down></i></a>&nbsp;"
			ls.add "<a class=""GridLink OrangeLink"" style=""cursor:pointer;"" onclick=""getGrid('" & ThisProductID & "');""><strong>" & ThisProductName & "</strong></a>"
			
			if BOStr <> "" then ls.add BOStr 
			if FOStr <> "" then ls.add FOStr 
			
			ls.add "&nbsp;<a title=""" & ProductDetailPageStr & """ class="""" href=""/products/?ProductID=" & ThisProductID & """><i class=icon-search></i></a>"			
			ls.add "&nbsp;<a title=""" & ProductStockEnquiryStr & """style=""cursor:pointer;"" data-product-id=""" & ThisProductID & """ data-variant-code="""" data-sku="""" onclick=""stockLevelClick_VARIANT(this)"" class=StockEnquiryLink><i class=icon-th></i></a>"
			
			'if isjh() then
				if ThisIsFavourite then 					
					ls.add "&nbsp;<span id=""fav_" & ThisProductID & """>"
					ls.add "&nbsp;<span class=""label label-important"">"
					ls.add "<a title=""" & FavouriteRemoveStr & """style=""cursor:pointer;"" onclick=""favouriteClick('REMOVE', '" & ThisProductID & "')""><i class=""icon-heart icon-white""></i></a>"
					ls.add "</span>"
					ls.add "</span>"
				else
					ls.add "&nbsp;<span id=""fav_" & ThisProductID & """>"
					ls.add "<a title=""" & FavouriteAddStr & """style=""cursor:pointer;"" onclick=""favouriteClick('ADD', '" & ThisProductID & "')""><i class=""icon-heart""></i></a>"
					ls.add "</span>"					
				end if
			'end if
			
			if LatestReviewCount > 0 then
				if LatestReviewDD <= 60 then
					ls.add "&nbsp;<span class=""label label-success"">"
					ls.add "<a class=WhiteLink title=""" & NewReviewStr & """ href=""/products/?ProductID=" & ThisProductID & "&stab=2""><i title=""RECENTLY ADDED"" class=""icon icon-white icon-asterisk""></i><i class=""icon-thumbs-up icon-white""></i> " & lricon & LatestReviewCount & "</a>"
					ls.add "</span>"					
				else
					ls.add "&nbsp;"
					ls.add "<a title=""" & CountReviewStr & """ href=""/products/?ProductID=" & ThisProductID & "&stab=2""><span class=""label label-inverse""><i class=""icon-thumbs-up icon-white""></i> " & LatestReviewCount & "</span></a>"
					ls.add ""
				end if
				
			end if
			
			if glPromoActive and glPromoCodes <> "" then
					
				if instr(1, glPromoCodes, "|" & ThisProductCode & "|") > 0 then
					
					ls.add "&nbsp;<span class=""label label-important"">"
					ls.add "<a title=""" & PromoNameStr & """ class=WhiteLink href=""/products/?ProductID=" & ThisProductID & "&promodetail=1""><i class=""icon icon-white icon-star""></i> " & PromoTeaserStr & "</a>"
					ls.add "</span>"						
					
				end if				
			end if
			
			ls.add "<div>"
			ls.add "<span class=""label label-inverse"">" & ThisProductCode & "</span> "
			if ThisOneLiner <> "" and ucase(ThisOneLiner) <> "MISSING" then
				ls.add "<small>" & ThisOneLiner & "</small>"
			end if
			ls.add "</div>"
			
			ls.add "<div id=""GRID_" & ThisProductID & """></div>"
			
			'ls.add "++++"
			
			ProductCount = ProductCount + 1
			
		end if
	next
	
	response.cookies("B2BQuickSearch")("SearchType") = SearchType
	response.cookies("B2BQuickSearch")("SearchVal") = SearchVal
	response.cookies("B2BQuickSearch").expires = dateadd("d", 5, now)	
	

	
	os = ls.value
	
	killarray ProdArr
	%>
		
		
		<%
		'if isjh() then
			SearchSortOrder = request.cookies("SearchSortOrder")
			'jhAdmin "SearchSortOrder = " & SearchSortOrder
			if SearchSortOrder = "" then SearchSortOrder = "PRICE_HI"
			
			if SearchType <> "RECENT" then
		%>
				<div class="pull-right small">
					<%=lne("label_sort_sort_by")%> <select onchange="setSortOrder(this.value)" id="SearchSortOrder" style="width:120px;" class=small>
						<option <%if SearchSortOrder = "PRICE_HI" then response.write "SELECTED"%> value="PRICE_HI"><%=lne_NOEDIT("label_sort_hi_lo")%></option>
						<option <%if SearchSortOrder = "PRICE_LO" then response.write "SELECTED"%> value="PRICE_LO"><%=lne_NOEDIT("label_sort_lo_hi")%></option>
						<option <%if SearchSortOrder = "NAME" then response.write "SELECTED"%> value="NAME"><%=lne_NOEDIT("label_sort_name")%></option>
						<option <%if SearchSortOrder = "PRODTYPE" then response.write "SELECTED"%> value="PRODTYPE"><%=lne_NOEDIT("label_sort_product_type")%></option>
					</select>
				</div>
		<%
			end if
		'end if
		%>
		
		<p>
			<span class="badge badge-inverse"><%=ProductCount%>&nbsp;<%=lne("label_product_count")%></span>
			
			<%
			if SearchType = "FAVOURITES" and ProductCount > 0 then
			%>
				<span class="badge badge-important">&nbsp;<a href="#" class=WhiteLink onclick="if (confirm('<%=lne("label_favourites_confirm_clear_all")%>')) {clearFavourites();}"><i class="icon-trash icon-white"></i> <%=lne("label_favourites_clear_all")%></a></span>
			<%	
			end if
			%>
			
		</p>
		
		<div style="clear:both;"></div>
		
<%
		if RangeSearch then
			sql = "SELECT RangeName, RangeImage FROM SML_Ranges WHERE RangeID = 0" & SearchRangeID & ""
			'jhAdmin sql
			set rs = db.execute(sql)
			if not rs.eof then
				RangeName = "" & rs("RangeName")
				RangeImage = "" & rs("RangeImage")
				'jhAdmin "RangeImage = " & RangeImage
				RangeImageStr = "<hr><img class=""pull-right img-rounded"" style=""width:300px;"" src=""http://b2b.endurasport.com/assets/images/ranges/" & RangeImage & """> <h4>" & RangeName & "</h4>" & lne_B2C("RANGE_1LINER_" & SearchRangeID) & "<div style=""clear:both;""></div>"
				
				response.write RangeImageStr
			end if
			rsClose()
		end if
		'SearchRangeID = SearchVal
%>		
		
		<%=os%>
		
		
		<input type=hidden class=FormHidden name="ProductCount_RESULTS" id="ProductCount_RESULTS" value="<%=ProductCount%>">
	
	<%
	
	
	response.cookies("B2BQuickSearch")("SearchType") = SearchType
	response.cookies("B2BQuickSearch")("SearchVal") = SearchVal
	response.cookies("B2BQuickSearch").expires = dateadd("d", 5, now)
	
	
	dbClose()
	
elseif CallType = "LOAD_RANGES" then
	'jhAdmin "LOAD RANGES - " & now
	
	
	sql = "SELECT RangeID, RangeName, RangeImage, (SELECT COUNT(*) FROM SML_Products WHERE Ranges LIKE '%|' + CAST(RangeID AS VARCHAR(10)) + '|%') AS RangeC "
	sql = sql & ",("
	sql = sql & "SELECT TOP 1 ProductCode FROM SML_Products "
	sql = sql & "WHERE Ranges LIKE '%|' + CAST(SML_Ranges.RangeID AS VARCHAR(10)) + '|%' "
	sql = sql & "AND dbo.[spB2BActive](ProductID) > 0 "
	sql = sql & ") AS TypeDefImage, RangeIcon	"
	sql = sql & "FROM SML_Ranges WHERE RangeStatusID = 1 ORDER BY RangeZOrder, RangeName"
	jhAdmin sql
	
	''jhstop
	
	dbConnect()
	
	x = getrs(sql, RangeArr, RangeC)
	RowMax = 3
	
	'jhAdmin "RangeC = " & RangeC
	
	
	for c = 0 to RangeC
		RangeID = RangeArr(0,c)
		RangeName = RangeArr(1,c)
		pCount = RangeArr(3,c)
		DefCode = RangeArr(4,c)
		
		RangeIcon = "" & RangeArr(2,c)
		
		if RangeIcon <> "" then
			imgURL = "http://b2b.endurasport.com/assets/images/ranges/" & RangeIcon
		elseif DefCode <> "" then
			imgURL = getImageDefault(DefCode, "")
		else
			ShowThis = false
		end if	
		
		%>
			<hr>
			<h4 style="min-height:40px;">
				<i class=icon-chevron-right></i> <a class=RangeSearchLink data-range="<%=RangeID%>" onclick=" $('#RangeDiv').hide();rangeSearch(<%=RangeID%>)" href="#"><%=RangeName%></a> 
				
				<a class=RangeSearchLink data-range="<%=RangeID%>" onclick=" $('#RangeDiv').hide();rangeSearch(<%=RangeID%>)" href="#">
					<!--<span class="pull-right badge badge-warning"><%=pCount%> products</span>-->
					<img class="img-responsive img-rounded pull-right" style="height:50px;" src="<%=imgURL%>">
				</a> 
			</h4>
			
		<%
	next	
	
	killarray(RangeArr)
	
elseif CallType = "LOAD_TYPES" then	
	'jh "LOAD_TYPES - " & now
	
	set acDetails = new DealerAccountDetails
	
	response.write quickAddTypes()
	
	set acDetails = nothing
	
elseif CallType = "END_USE" then	
	
	sql = "SELECT CategoryID, CategoryName, CategoryZORder, (SELECT COUNT(*) FROM SML_Products WHERE dbo.spB2BActive(ProductID) > 0 AND Categories LIKE '%|' + CAST(CategoryID AS VARCHAR(10)) + '|%') AS RangeC FROM SML_Categories WHERE CategoryZOrder <> 99 ORDER BY CategoryZOrder, CategoryName"

	set acDetails = new DealerAccountDetails
	
	sql = "exec [spB2BQuickSearch_ENDUSE] 0" & acDetails.SelectedLanguageID
	
	x = getrs(sql, CatArr, CatC)
	RowMax = 3

	ProductCountLabel = lne("label_product_count")
	
	for c = 0 to CatC
		CategoryID = CatArr(0,c)
		CategoryName = CatArr(1,c) 
		pCount = CatArr(3,c)
		%>
			<hr>
			<h4>
				<i class=icon-chevron-right></i> <a class=CategorySearchLink data-category="<%=CategoryID%>" onclick=" $('#EndUseDiv').hide();categorySearch(<%=CategoryID%>)" href="#"><%=CategoryName%> 
				<!--<span class="pull-right badge badge-warning"><%=pCount%>&nbsp;<%=ProductCountLabel%></span>--></a>
			</h4>
			
		<%
	next
	
	set acDetails = nothing
	
	killarray catarr 
	
end if

dbClose()
set acDetails = nothing
set ls = nothing
%>				

</body>
</html>
