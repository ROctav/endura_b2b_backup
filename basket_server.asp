<%
Response.ContentType = "text/html"
Response.AddHeader "Content-Type", "text/html;charset=ISO-8859-1"
'Response.CodePage = 65001
Response.CharSet = "ISO-8859-1"

Response.Expires = 0
Response.Expiresabsolute = Now() - 1442
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"

SelectedLanguageID = getSelectedLanguage()
'jh "SelectedLanguageID = " & SelectedLanguageID

if not isLoggedIn() then 
	response.end
	jhStop
end if

'jhAdmin "BASKET SERVER!"

%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	</head>
	<body>
	

<!--#include virtual="/assets/includes/global_functions.asp" -->
<!--#include virtual="/assets/includes/product_functions.asp" -->
<%




ThisScript = request.servervariables("SCRIPT_NAME") & "?" & request.servervariables("QUERY_STRING")

set acDetails = new DealerAccountDetails
ThisUserID = acDetails.AcCode

CallType = request("CallType")


if CallType = "ADD" then
	
	if glMaxBasketAddQty = "" then glMaxBasketAddQty = 1000
	
	CallContext = request("CallContext") 
	
	jhAdmin "CallContext = " & CallContext
	
	ProductID = cleannum(request("ProductID"))
	'jh "ProductID = " & ProductID
	
	dbConnect()
	
	params = request("params")
	
	ProcessBasket = true
	
	if ProcessBasket then
		
		SKUList = replace(request("SKU"), " ", "")		
		SKUQty = replace(request("SKUQty"), " ", "")
		WASQty = replace(request("WASQty"), " ", "")


		if SKUList = "" then SKUList = replace(request("s"), " ", "")		
		if SKUQty = "" then SKUQty = replace(request("q"), " ", "")	
		if WASQty = "" then WASQty = replace(request("w"), " ", "")	
		
		QtyArr = split(SKUQty, ",")
		SKUArr = split(SKUList, ",")
		if WASQty <> "" then
			WASArr = split(WASQty, ",")
		end if
		
		for x = 0 to ubound(QtyArr)
			SelQty = cleannum(QtyArr(x))
			'if SelQty = "" and WASArr(x) <> "" then SelQty = "0"
			
			if isarray(WASArr) then
				if ubound(WASArr) = ubound(QtyArr) then
					if SelQty = "" and WASArr(x) <> "" then 
						SelQty = "0"
						'jhInfo "A ZERO!"
					end if
				end if
			end if
			
			if SelQty <> "" then			
				SelSKU = trim("" & SKUArr(x))
				
				if cdbl(SelQty) > glMaxBasketAddQty then SelQty = glMaxBasketAddQty
				
				SomeAdded = true
				call basketAdd(SelSKU, SelQty, ThisUserID)
			end if
		
		next

	
		if SomeAdded then
			response.write "<div class=spacer10></div>"
			
			set BasketDetails = new ClassBasketDetails

			BasketC = BasketDetails.BasketC
			BasketCount = BasketDetails.BasketCount
			BasketArr = BasketDetails.BasketArr

			
			BasketTotalStr = "<div class=spacer10></div><h4><small>" & lne("label_updated_basket_total") & "</small> " & dispcurrency(BasketDetails.GrandTotal) & "</h4>"			
			
			BasketUpdatedStr = lne("label_basket_updated")
			
			if CallContext = "CHECKOUT" then								
				jhInfoNice BasketUpdatedStr, lne("label_basket_skus_added") & "<br><a class=OrangeLink href=""/checkout/"">" & lne("label_basket_update_totals_prompt") & "</a>" & BasketTotalStr			
			else
				jhInfoNice BasketUpdatedStr, lne("label_basket_skus_added") & "<br><a class=OrangeLink href=""/checkout/"">" & lne("label_basket_update_totals_checkout_prompt") & "</a>" & BasketTotalStr
			end if
			if ProductID <> "" then
				call captureProductView(ProductID)
			end if
			
			set BasketDetails = nothing
		end if
		
	end if		
	
elseif CallType = "BASKET_SUMMARY" then
	'jhAdmin "CallType = " & CallType
		
	set BasketDetails = new ClassBasketDetails

	BasketC = BasketDetails.BasketC
	BasketCount = BasketDetails.BasketCount
	BasketArr = BasketDetails.BasketArr
	UnitTotal = BasketDetails.UnitTotal
	
	'dbConnect()

	ThisCostBand = acDetails.CostBand	
	
	set ls = new strconcatstream
	
	ls.add "<div class=""text-right legible"">"
	ls.add "<div><h5>" & lne("header_basket_heading") & " <span class=""label label-success"" >"  & UnitTotal & " " & lne("label_item_count") & "</span></h5></div>"
	
	AvailableCreditAfterOrder = 0
	
	if BasketC >= 0 then
		
		OrderTotal = BasketDetails.BasketTotal
		DeliveryTotal = BasketDetails.DeliveryTotal
		VATTotal = BasketDetails.VATTotal
		GrandTotal = BasketDetails.GrandTotal
		
		ls.add "<div>" & lne("label_total") & " <strong>"  & dispCurrency(OrderTotal) & "</strong></div>"
		
		FreeDelGap = BasketDetails.FreeDelGap
		
		BODespBalance = BasketDetails.BODespBalance
		BONewSmallOrdersBalance = BasketDetails.BONewSmallOrdersBalance

		if VATTotal > 0 then			
			if AcDetails.RiderCard then
				VatLabel = lne("rider_card_vat")				
			else
				VatLabel = "+ " & lne("vat_abbr") & " @ "
			end if
			ls.add "<div> " & VatLabel

			if (AcDetails.VATCode = "S") Then
				ls.add glVatRate 
			elseif (AcDetails.VATCode = "8") Then
				ls.add glVat8Rate 
			else
				ls.add glVatRate 
			end if
			
			ls.add "% <strong>"  & dispCurrency(VATTotal) & "</strong></div>"
		end if
		
		if AcDetails.DeliveryTBC then
			DeliveryStr = "<span title=""" & lne("label_delivery_tbc_explained")  & """ class=""badge badge-warning"">" & lne("label_delivery_TBC") & "</span>"
		else
			DeliveryStr = dispCurrency(DeliveryTotal)
		end if
		
		ls.add "<div>" & lne("label_delivery") & " <strong>"  & DeliveryStr & "</strong></div>"	
		ls.add "<h4>" & lne("label_grand_total") & " <strong>"  & dispCurrency(GrandTotal) & "</strong></h4>"	
		
		if AcDetails.DeliveryTBC then
		
		elseif BODespBalance > 0 then
		
			ls.add "<div><span class=""badge badge-success"">"									
			ls.add lne("label_free_del")
			ls.add "</span></div>"
			ls.add "<div class=""spacer20 legible "">"
			ls.add lne("label_free_del_bo")
			ls.add "</div>"
			
		elseif BONewSmallOrdersBalance > 0 and DeliveryTotal <= 0 then
			ls.add "<div class=spacer20><span class=""badge badge-success"">"						
			ls.add lne("label_free_del_small_order")
			ls.add "</span></div>"
		elseif FreeDelGap = 0 then
			ls.add "<div class=spacer20><span class=""badge badge-success"">"						
			ls.add lne("label_free_del")
			ls.add "</span></div>"		
		else
			ls.add "<div class=spacer20><span class=""badge badge-important"">"						
			ls.add lne_REPLACE("label_add_more", dispCurrency(FreeDelGap))
			
			ls.add "</span></div>"					
									
		end if			
		
		ls.add "<div class=spacer20><a class=""btn btn-small btn-primary"" href=""/checkout/"">" & lne("btn_checkout") & "</a></div>"
		
		
		ls.add "</div>"		
		
		'ls.add "</table>"
		
	else
		ls.add "<div class=spacer20>" & lne("label_basket_empty") & "</div>"		
	end if

	if  acDetails.RiderCard then
		ls.add "<div class=spacer20></div>"	
		ls.add "<span class=""label label-info"">RIDER CARD</span>"

	else	
		ls.add "<div>" & lne("label_current_balance") & " <strong>"  & dispCurrency(acDetails.CurrentBalance) & "</strong></div>"
		ls.add "<div>" & lne("label_available_credit") & " <strong>"  & dispCurrency(acDetails.AvailableCredit) & "</strong></div>"
		ls.add "<div>" & lne("label_credit_limit") & " <strong>"  & dispCurrency(acDetails.CreditLimit) & "</strong></div>"
	
		AvailableCreditAfterOrder = acDetails.AvailableCredit - GrandTotal
	
		if AvailableCreditAfterOrder < 0 then
			AvailableClass = "badge badge-important"
		else
			AvailableClass = "badge badge-success"
		end if		
	
		if OrderTotal > 0 then
			ls.add "<div><span class=""" & AvailableClass & """>" & lne("label_available_credit_after") & " <strong>"  & dispCurrency(AvailableCreditAfterOrder) & "</strong></span></div>"		
		end if
	
		if AcDetails.SagePayUser then
			ls.add "<div><a class=""btn btn-small btn-primary"" href=""/utilities/sagepay/"">" & lne("btn_make_payment") & "</a></div>"
		end if

				if session("B2B_SuppressPricing") = "1" then
					SuppressClass = "badge-important"
					SuppressStr = acDetails.CurrencySymbol & " <i class=""icon-remove icon-white""></i>"
					SuppressTitle = "Displayed pricing is currently switched OFF, click to switch on"
					ToogleOption = "ON"
				else
					SuppressClass = "badge-success"
					SuppressStr = acDetails.CurrencySymbol & " <i class=""icon-ok icon-white""></i>"
					SuppressTitle = "Displayed pricing is currently switched ON, click to switch off"
					ToogleOption = "OFF"
				end if
					ls.add "<div class=spacer20></div>"	
					ls.add "<strong>" & lne_NOEDIT("footer_pricing_label") & "</strong>&nbsp;"
					ls.add "<a id=ProdSearchDD title=""Your account"" href=""/?TogglePricing=" & ToogleOption & """>"	
					ls.add "<span title=""" & SuppressTitle & """ class=""badge " & SuppressClass & """ style=""color:white;"">" & SuppressStr & "</span>"
					ls.add "</a>					"
	end if
	

				ls.add "<div class=spacer10></div>"

	response.write ls.value
	
elseif CallType = "BASKET_COUNT" then
	jh "CallType = " & CallType	
	set BasketDetails = new ClassBasketDetails	
	response.clear
	
	BasketCount = BasketDetails.UnitTotal 
	GrandTotal = BasketDetails.GrandTotal 
	
	BasketLabel = lne_REPLACE("label_your_basket_summary", BasketCount)
	
	response.write "<span title=""" & BasketLabel & ", " & dispCurrency(GrandTotal) & """>" & BasketCount & "</span>"
	
elseif CallType = "CHECKOUT_ADD" then
	'jh "CallType = " & CallType	
	
	SearchVal = trim("" & request("SearchVal"))
	
	'jh "SearchVal = " & SearchVal
	
	if SearchVal <> "" then
		sql = "exec spB2BQuickSearch_CHECKOUT '" & AcDetails.AcCode & "', '" & validstr(SearchVal) & "', 0" & AcDetails.SelectedLanguageID
		
		'jhAdmin sql
		
		dbConnect()
		
		x = getrs(sql, QAArr, qc)
	else
		qc = -1
	end if
	'jh "qc = " & qc

	'jhstop
	
	set ls = new strconcatstream	
	
	ProductCount = 0
	LastProductID = ""
	
	if qc = 0 then
				
		SingleSKU = true
		
		'jhStop
		
	elseif qc >= 0 then	
		for c = 0 to qc
			ThisProductID = QAArr(0,c)
			
			ThisCode = QAArr(1,c)
			ThisName = QAArr(2,c)
			ThisColor = QAArr(4,c)
			ThisSize = QAArr(5,c)
			ThisBarCode = QAArr(6,c)
			ThisVariantCode = QAArr(7,c)			
			
			ThisSKUStatusID = QAArr(13,c)			
			'jh "ThisSKUStatusID = " & ThisSKUStatusID
			
			if AcDetails.SelectedLanguageID > 0 then	
				if "" & QAArr(8,c) <> "" then ThisName = QAArr(8,c)				
			end if
			
			ProductName = ThisName & " <span class=""badge badge-inverse"">" & ThisCode & "</span>"
			
			if ucase("" & QAArr(7,0)) = ucase("" & SearchVal) then 
				VariantMatch = true
				'jh "VariantMatch = " & VariantMatch
			end if
			
			if LastProductID <> ThisProductID then
				LastProductID = ThisProductID
				ProductCount = ProductCount + 1
				
				imgURL = getImageDefault(ThisCode, ThisVariantCode)		
				
				ProductList = ProductList & "<div class=row>"
				ProductList = ProductList & "<div class=span1>"
				ProductList = ProductList & "<a class=NotLink onclick=""checkoutSearch('" & ThisCode & "')"" >"
				ProductList = ProductList & "<img class=ProdThumb40 src=""" & imgURL & """>"
				ProductList = ProductList & "</a>"
				ProductList = ProductList & "</div>"
				ProductList = ProductList & "<div class=span2>"
				ProductList = ProductList & "<a class=NotLink onclick=""checkoutSearch('" & ThisCode & "')"" >"
				ProductList = ProductList & ThisName
				ProductList = ProductList & "</a>"
				ProductList = ProductList & "</div>"
				ProductList = ProductList & "</div>"
				ProductList = ProductList & "<hr>"
			end if
		next
		
		if ProductCount = 1 then 
			SingleProduct = true
			if VariantMatch then
				GridVariant = SearchVal
			end if
		elseif ProductCount > 1 then
			MultipleProducts = true
		end if
	elseif qc = -1 then
		NotFound = true
	end if
	
	if SingleSKU then
		
		ThisProductID = QAArr(0,0)
		ThisCode = QAArr(1,0)
		ThisName = QAArr(2,0) 
		ThisSKU = QAArr(3,0)
		ThisColor = QAArr(4,0)
		ThisSize = QAArr(5,0)
		ThisBarCode = QAArr(6,0)
		ThisVariantCode = QAArr(7,0)
		
		MAIQty = cleannum(QAArr(11,0))
		BasketQty = cleannum(QAArr(12,0))
		if BasketQty <> "" and BasketQty <> "0" then InitBasketQty = BasketQty else InitBasketQty = "1"
		
		ThisSKUStatusID = QAArr(13,c)			
		
		if ThisSKUStatusID = "5" then ClearanceItem = true else ClearanceItem = false
		
		if ThisSKUStatusID = "5" and MAIQty = "0" then CanPurchase = "0"
		if acDetails.RiderCard and MAIQty = "0" then CanPurchase = "0"		
		
		StatusExpStr = ""
		if CanPurchase = "0" then 
			AllowPurchase = false
			DisStr = "DISABLED" 
			if acDetails.RiderCard then
				StatusExpStr = "Sorry, you can only purchase items currently in-stock"
			else
				StatusExpStr = "Sorry, this color/size is not currently available for purchase - please contact us for further details"
			end if
		else
			AllowPurchase = true
			DisStr = ""
		end if		
		
		if AcDetails.SelectedLanguageID > 0 then	
			if "" & QAArr(8,0) <> "" then ThisName = QAArr(8,0)		
			if "" & QAArr(10,0) <> "" then ThisColor = QAArr(10,0)					
		end if		
		
		ProductName = ThisName & " " & ThisCode
		SKUName = ThisName & ", " & ThisColor & " - " & ThisSize
		
		imgURL = getImageDefault(ThisCode, ThisVariantCode)		
		
		btnLabel = lne("btn_add_to_basket")
		

%>
		<div class="row QAForm">
			<form id="productform_<%=ThisProductID%>">
				<div class=span1>			
					<a href="/products/?productid=<%=ThisProductID%>"><img title="<%=SKUName%>" src="<%=imgURL%>" class="pull-left ProdThumb90"></a>
				</div>
				<div class=span6>			
					<h4><a href="/products/?productid=<%=ThisProductID%>"><%=SKUName%></a></h4>
					<div class="subtle spacer20">
						<span class="badge badge-inverse"><%=ThisSKU%></span>						
						<%=dispStock_ENQUIRY(MAIQty, ThisProductID, ThisVariantCode, ThisSKU)%>
						<span class="subtle tiny">EAN: <%=ThisBarCode%></span>
					</div>
					
					<div class=spacer40>
					
<%
							if not AllowPurchase then
%>
								<div class=spacer20 title="<%=StatusExpStr%>" style="padding-top:4px;"><i class=icon-exclamation-sign></i> <%=StatusExpStr%></div>
<%								
							else
%>
								<%=lne("label_quantity")%> <input name="SKUQty" id="SKUQty" value="<%=InitBasketQty%>" type="number" autocomplete=off class=BigQty>
								
<%							
								if ClearanceItem and glClearanceListActive then
%>
									<div class=spacer20 title="This SKU is on clearance, click to view" style="padding-top:4px;"><a class=OrangeLink href="/products/clearance/#VARIANT_<%=ThisVariant%>"><span class="label label-info"><i class="icon-fire icon-white"></i></span></a></div>"	
<%
								end if		
							end if
%>					
					
						
						
						<input style="width:20px;" type=hidden name=SKU value="<%=ThisSKU%>" class=FormHidden>
						<input style="width:20px;" type=hidden name=WASQty value="<%=BasketQty%>" class=FormHidden>
						
					</div>
					
					<div class=ProductAjax style="text-align:left;" id="ProductAjaxDiv_<%=ThisProductID%>">
					
					</div>					
					
					<div>
						<button onclick="ba(this, <%=ThisProductID%>);return false;" class="btn btn-large btn-success" data-product-id="<%=pProductID%>"><%=btnLabel%></button>
					</div>
					
				</div>		
			</form>
		
		
		
<%
		
		
	elseif SingleProduct then
		'jhInfo "SINGLE! (" & ThisProductID & ")"		
		response.write "<h4>" & ProductName & "</h4>"
		response.write "<hr>"
		response.write productGrid(ThisProductID, GridVariant)
	elseif MultipleProducts then
		jhErrorNice "Multiple products found", "Please enter a unique product code, or select from below"

		response.write ProductList
		
	elseif NotFound then
		jhErrorNice "Product not found", "No product matching your search was found!"
	end if	
	
	
	killarray QAArr
	
end if

dbClose()
	

set acDetails = nothing
set BasketDetails = nothing

set ls = nothing


%>				

</body>
</html>
