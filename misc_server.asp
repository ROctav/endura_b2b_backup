<%
Response.ContentType = "text/html"
Response.AddHeader "Content-Type", "text/html;charset=ISO-8859-1"
'Response.CodePage = 65001
Response.CharSet = "ISO-8859-1"

Response.Expires = 0
Response.Expiresabsolute = Now() - 1442
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"

SelectedLanguageID = getSelectedLanguage()
ThisUserID = session("B2B_AcCode")

'

%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	</head>
	<body>
	

	<!--#include virtual="/assets/includes/global_functions.asp" -->
<%

if not isLoggedIn() then jhStop

ThisScript = request.servervariables("SCRIPT_NAME") & "?" & request.servervariables("QUERY_STRING")

if isjh() then
	'jhInfo ThisScript	
	'if isLoggedIn() then
	'jhstop	
end if

CallType = request("CallType")

'jh "CallType = " & CallType

if CallType = "ON_HOLD_DISMISS" then

	jhAdmin CallType

	session("OnHoldDismissed") = "1"
	jhAdmin "DISMISSED!"

elseif CallType = "CLEAR_FAVOURITES" then

	set acDetails = new DealerAccountDetails

	sql = "DELETE FROM B2B_ProductFavourites WHERE AcCode = '" & AcDetails.AcCode & "'"
	db.execute(sql)
	
	dbClose()
	
elseif CallType = "ANNOUNCEMENT_READ" then
	
	set acDetails = new DealerAccountDetails
	
	AnnouncementID = cleannum(request("AnnouncementID"))
	'jhAdmin "MARK AS READ!"
	dontShowAgain = request("dontShowAgain")
	'jhAdmin "dontShowAgain = " & dontShowAgain
	
	ReadAnnouncements = session("B2B_ReadAnnouncements")
	if ReadAnnouncements = "" then ReadAnnouncements = "|"
	if instr(1, ReadAnnouncements, "|" & AnnouncementID & "|") = 0 then
		ReadAnnouncements = ReadAnnouncements & AnnouncementID & "|"	
	end if
	
	'jh "ReadAnnouncements = " & ReadAnnouncements
	session("B2B_ReadAnnouncements") = ReadAnnouncements
	
	if dontShowAgain = "true" then
		dbConnect()
		sql = "UPDATE END_Calendar_GLOBAL SET MarkedAsReadBy = ISNULL(MarkedAsReadBy,'') + '" & AcDetails.AcCode & "|' "
		sql = sql & "WHERE GCID = 0" & AnnouncementID & " AND ISNULL(MarkedAsReadBy,'') NOT LIKE '" & AcDetails.AcCode & "|%' "
		
		'jhAdmin sql
		db.execute(sql)
	end if
	
elseif CallType = "HINT_READ" then
	
	set acDetails = new DealerAccountDetails
	HintType = request("HintType")
	
	if HintType <> "" then
	
		response.cookies("hints")(HintType) = "1" 		
		response.cookies("hints").expires = dateadd("d", 100, now)		
		jhAdmin "HINT SET -  " & HintType
		
	end if	
elseif CallType = "ADD_FAVOURITE" then
	
	'jhAdmin "CallType = " & CallType
	
	set acDetails = new DealerAccountDetails
	
	ProductID = cleannum(request("ProductID"))
	CallContext = request("CallContext")
	
	dbConnect()
	
	if CallContext = "ADD" then
		
		sql = "INSERT INTO B2B_ProductFavourites (ProductID, AcCode) "
		sql = sql & "SELECT TOP 1 ProductID, '" & AcDetails.AcCode & "' AS AcCode FROM SML_Products WHERE ProductID = 0" & ProductID & ""
		sql = sql & "AND NOT EXISTS (SELECT * FROM B2B_ProductFavourites WHERE ProductID = 0" & ProductID & " AND AcCode = '" & AcDetails.AcCode & "') "
		'jhAdmin sql
		db.execute(sql)
		
		response.write "<span class=""label label-info""><i class=""icon-heart icon-white""></i> " & lne("label_favourites_added") & "</span>"
		
	elseif CallContext = "REMOVE" then
		
		sql = "DELETE FROM B2B_ProductFavourites WHERE ProductID = 0" & ProductID & " AND AcCode = '" & AcDetails.AcCode & "' "
		db.execute(sql)
		'jhAdmin sql		

		response.write "<span class=""label label-info""><i class=""icon-heart icon-white""></i> " & lne("label_favourites_removed") & "</span>"
			
	end if

	dbclose()
	
elseif CallType = "SET_EDIT_MODE" then
	SetEditMode = request("SetEditMode")
	session("cms_editmode") = SetEditMode
	
elseif CallType = "INVOICE_ENQUIRY" then
	InvoiceNo = request("InvoiceNo")
	
	set acDetails = new DealerAccountDetails	
	sql = "exec spB2BRecentInvoices '" & acDetails.AcCode & "', '" & validstr(InvoiceNo) & "', " & ExDate(now)
	
	x = getrs(sql, InvoiceArr, InvoiceC)
	
	if InvoiceC >= 0 then
	
		InvoiceFound = true
		ThisRef = InvoiceArr(0,0)
		ThisDate = InvoiceArr(1,0)		
		DispDate = FromExDate(ThisDate)
		YourRef = trim("" & InvoiceArr(2,0))
		ThisValue = InvoiceArr(6,0)
		
	end if
		
		
	if InvoiceFound then
	
	
						
		sql = "SELECT t.thOurRef, l.tlLinePos, l.tlStockCode, es.stDesc1,  "
		sql = sql & "dbo.TwoVals(l.tlQty_1, l.tlQty_2) AS LineQty "
		sql = sql & ", sp.ProductID , sp.ProductCode , sp.ProductName, spc.Variantcode, spc.ColorName, sz.SizeName, MAIQty "
		sql = sql & ", dbo.TwoVals(l.tlDiscount_1, l.tlDiscount_2) AS DiscRateEx, esf.Qty, esf.NetValue "
		sql = sql & "FROM         dbo.Transactions t  "
		sql = sql & "INNER JOIN dbo.TransactionLines l ON t.idxFolio = l.idxFolio  "
		sql = sql & "INNER JOIN END_SalesFigures esf ON l.tlOurRef = esf.tlOurRef AND l.tlLineNo = esf.tlLineNo "
		sql = sql & "INNER JOIN dbo.ExStock es ON l.tlStockCode = es.stStockCode "
		sql = sql & "LEFT JOIN JH_EnduraWeb.dbo.SML_SKUS ss ON ss.SKU = es.stStockCode "
		sql = sql & "LEFT JOIN JH_EnduraWeb.dbo.SML_Products sp ON sp.ProductID = ss.ProductID "

		sql = sql & "LEFT JOIN JH_EnduraWeb.dbo.SML_ProductColorways spc ON ss.ProductColorwayID = spc.ProductColorwayID "
		sql = sql & "LEFT JOIN JH_EnduraWeb.dbo.SML_ColorwayList cl  ON cl.ColorwayID = spc.ColorwayID "
		sql = sql & "LEFT JOIN JH_EnduraWeb.dbo.SML_SizeList sz ON sz.SizeID = ss.SizeID "
		sql = sql & "LEFT JOIN JH_EnduraWeb.dbo.SML_SKUStatus sta ON sta.SKUStatusID = ss.SKUStatusID "
		sql = sql & "LEFT JOIN ( "
		sql = sql & "SELECT StockCode, SUM(QtyInStock - QtyAllocated) AS MAIQty FROM ExStockLocations "
		sql = sql & "WHERE StockLocation = 'MAI' "
		sql = sql & "GROUP BY StockCode "
		sql = sql & ") MAI ON MAI.StockCode = ss.SKU "

		sql = sql & "WHERE thOurRef = '" & validstr(InvoiceNo) & "' "
		sql = sql & "ORDER BY l.tlLinePos "
				
		x = getrs(sql, DetailArr, DetailC)
		
		set ls = new strconcatstream
		set csvls = new strconcatstream

		csvls.add "INVOICE"
		csvls.add ","
		csvls.add ThisRef
		csvls.add vbcrlf	
		csvls.add "YOUR REF"
		csvls.add ","
		csvls.add YourRef
		csvls.add vbcrlf	
		csvls.add "DATE"
		csvls.add ","
		csvls.add DispDate		
		csvls.add vbcrlf	
		csvls.add "VALUE"
		csvls.add ","
		csvls.add ThisValue		
		csvls.add vbcrlf			
		
		csvls.add vbcrlf	
		csvls.add vbcrlf	
		
		csvls.add "SKU,Qty,Value,Name,Colour,Size"
		csvls.add vbcrlf	
		

		ls.add "<table width=""100%"" border=1 class=""ReportTable legible"">"	
		
		RecCount = 0
		
		for c = 0 to DetailC

			ThisSKU = DetailArr(2,c)
			
			ThisProductID = "" & DetailArr(5,c)
			if ThisProductID <> "" then
				
				ThisColorName = DetailArr(9,c)
				ThisSizeName = DetailArr(10,c)				
				ThisProductCode = "" & DetailArr(6,c)
				ThisProductName = "" & DetailArr(7,c)
				ThisVariantCode = "" & DetailArr(8,c)				
				imgURL = getImageDefaultCDN(ThisProductCode, ThisVariantCode)
			else
				
				ThisColorName = ""
				ThisSizeName = ""
				ThisProductCode = ""
				ThisProductName = "" & DetailArr(3,c)
				ThisVariantCode = ""
				
				imgURL = ""
				
			end if
			
			ThisQty = DetailArr(4,c)			
			ThisMAI = DetailArr(11,c)			
			'jh "imgURL = " & imgURL
			
			LineDisc = convertnum(DetailArr(12,c))
			LineQty = convertnum(DetailArr(13,c))
			LineNetVal = convertnum(DetailArr(14,c))
						
			LineValue = 0
			LineValue = LineQty * LineNetVal * (1-LineDisc)
			'jhAdmin "LineValue = " & dispCurrency(LineValue) 
			
			if LineNetVal <> 0 then
				ShowThis = true
			else
				ShowThis = false
			end if
			
			if ShowThis then
				
				RecCount = RecCount + 1
				
				ls.add "<tr>"
				if imgURL <> "" then
					ls.add "<td><a href=""/products/?ProductID=" & ThisProductID & """><img src=""" & imgURL & """ class=""ProdThumb30""></a>"
				else
					ls.add "<td>.</a>"	
				end if
				ls.add "<td><strong>"
				ls.add ThisSKU
				ls.add "</strong></td>"									
				ls.add "<td>"
				ls.add ThisProductName
				ls.add "</td>"	
				ls.add "<td>"
				ls.add ThisColorName
				ls.add "</td>"			
				ls.add "<td>"
				ls.add ThisSizeName
				ls.add "</td>"			
				'ls.add "<td>"
				'ls.add dispStock_ENQUIRY(ThisMAI, ThisProductID, ThisVariantCode, ThisSKU)
				'ls.add "</td>"					
				ls.add "<td class=numeric>"
				ls.add "<strong>" & ThisQty & "</strong>"
				ls.add "</td>"	
				ls.add "<td class=numeric>"
				ls.add "<strong>" & dispCurrency(LineValue)  & "</strong>"
				ls.add "</td>"					
				ls.add "</tr>"	
				
	
				csvls.add ThisSKU
				csvls.add ","
				csvls.add ThisQty		
				csvls.add ","	
				csvls.add replace(dispCurrency_PLAIN(LineValue)	, ",", "")
				csvls.add ","					
				
				csvls.add replace(ThisProductName, ",", " ")
				csvls.add ","
				csvls.add replace(ThisColorName, ",", " ")
				csvls.add ","
				csvls.add ThisSizeName
				
				csvls.add vbcrlf
				
				SomeShown = true
			end if
		next
		
		ls.add "</table>"

		InvoiceOS = ls.value	
		
		if not SomeShown then 
			InvoiceOS = "<p>" & lne("label_no_records") & "</p>"  
		else
			
			if 1 = 1 then
			
				downloadFolder = getAccountDownloadFolder()
				
				
				'jhAdmin downloadFolder
				
				InvoiceFN = "invoiceitems.csv"
				
				fn = downloadFolder & InvoiceFN
				
				'jhAdmin "fn = " & fn
				
				set fs = server.createobject("scripting.filesystemobject")
				
				set f = fs.createtextfile(fn, true)
				
				f.write csvls.value
			
				f.close
				set f = nothing
				set fs = nothing
				
				FileWritten = true
				downloadURL = getAccountDownloadURL & InvoiceFN
				'jhAdmin downloadURL
				
			end if		
		
		end if
		
		
		LabelClass = "label-inverse"
		if left(InvoiceNo,3) = "SCR" then 
			LabelClass = "label-success"
		end if
		
	%>
	
		<div class=pull-right>
			<h4><%=YourRef%></h4>
			<p><%=DispDate%></p>		
			<%
			if FileWritten then
			%>	
				<p>
					<a class=OrangeLink target="_BLANK" href="<%=downloadURL%>"><i class="icon icon-download"></i> <%=lne("label_download_excel")%></a>
				</p>
			<%
			end if
			%>
			
		</div>
		<h3><%=InvoiceNo%></h3>		

		<span class="label <%=LabelClass%>"><h4><%=dispCurrency(ThisValue)%></h4></span>		
		
	<%
			
		
		response.write InvoiceOS
		
		set ls = nothing
		set csvls = nothing
		
	end if
		
elseif CallType = "REPEAT_ORDER" then
	RepeatOrderRef = request("RepeatOrderRef")
	
	set acDetails = new DealerAccountDetails
	
	SelectedLanguageID = acDetails.SelectedLanguageID
	
	%>
	
		<h3 class=spacer40><%=RepeatOrderRef%></h3>

		
	<%
	
	'jhInfo "RepeatOrderRef = " & RepeatOrderRef
	
	sql = "SELECT t.thOurRef, t.thYourRef, t.thTransDate, esv.StockCode "
	sql = sql & ", vp.ProductID, trans.ProductName_TRANS, vp.ProductCode, vp.VariantCode, vp.SizeName, colors.ColorName_TRANS, B2BCanPurchase, OrderQty "
	sql = sql & "FROM transactions t  "
	sql = sql & "INNER JOIN END_SalesOrderLineValues esv ON esv.OrderRef = t.thOurRef "
	sql = sql & "INNER JOIN [vwSML_ProductLookup] vp ON vp.SKU = esv.StockCode "
	sql = sql & "INNER JOIN ( "
	sql = sql & "SELECT * FROM JH_EnduraWeb.dbo.[fnSML_TranslatedProduct] (0, " & SelectedLanguageID & ")  "
	sql = sql & ") trans ON trans.ProductID = vp.ProductID "

	sql = sql & "LEFT JOIN ( "
	sql = sql & "SELECT * FROM JH_EnduraWeb.dbo.[fnSML_TranslatedColor] (" & SelectedLanguageID & ") "
	sql = sql & ") colors ON colors.ColorwayID = vp.ColorwayID "

	sql = sql & "WHERE idxAcCode = '" & AcDetails.AcCode & "' "
	sql = sql & "AND thOurRef = '" & validstr(RepeatOrderRef) & "' "
	sql = sql & "AND B2BVisible = 1 "
	sql = sql & "ORDER BY thTransDate DESC, tlLineNo	"
	
	'jhAdmin sql
	
	x = getrs(sql, DetailArr, DetailC)
	
	'jhAdmin "DetailC = " & DetailC
	
	set ls = new strconcatstream

	MaxLines = 99
	
	if DetailC > MaxLines then
		DetailC = MaxLines
		
		jhErrorNice lne("label_too_many_lines") , lne_REPLACE("label_too_many_lines_explained", MaxLines+1)
	end if
	
	ls.add "<table width=""100%"" border=1 class=""ReportTable legible"">"	
	
	RecCount = 0
	
	for c = 0 to DetailC

		ThisSKU = DetailArr(3,c)
		
		ThisProductID = DetailArr(4,c)
		ThisProductName = DetailArr(5,c)
		ThisColorName = DetailArr(9,c)
		ThisSizeName = DetailArr(8,c)
		
		ThisProductCode = DetailArr(6,c)
		ThisVariantCode = DetailArr(7,c)
		
		ThisCanPurchase = ""& DetailArr(10,c)
		ThisQty = DetailArr(11,c)
		
		imgURL = getImageDefaultCDN(ThisProductCode, ThisVariantCode)
		
		ShowThis = true
		
		if ThisCanPurchase = "1" then		
			ThisMAI = getStockMAI(ThisSKU)
		else
			ShowThis = false
		end if
		
		if ShowThis then
			
			RecCount = RecCount + 1
			
			ls.add "<tr>"
			ls.add "<td><a href=""/products/?ProductID=" & ThisProductID & """><img src=""" & imgURL & """ class=""ProdThumb60""></a>"
			ls.add "<td><strong>"
			ls.add ThisSKU
			ls.add "</strong></td>"									
			ls.add "<td>"
			ls.add ThisProductName
			ls.add "</td>"	
			ls.add "<td>"
			ls.add ThisColorName
			ls.add "</td>"			
			ls.add "<td>"
			ls.add ThisSizeName
			ls.add "</td>"			
			ls.add "<td>"
			ls.add dispStock_ENQUIRY(ThisMAI, ThisProductID, ThisVariantCode, ThisSKU)
			ls.add "</td>"					
			ls.add "<td>"
			ls.add "<input type=text class=""form-control ProductDetailSKUQty"" name=""Qty_" & RecCount & """ value=""" & ThisQty & """>"
			ls.add "<input type=hidden class=""FormHidden"" name=""SKU_" & RecCount & """ value=""" & ThisSKU & """>"
			
			ls.add "</td>"				
			ls.add "</tr>"	
			
			SomeShown = true
		end if
	next
	
	ls.add "</table>"

	RepeatOS = ls.value	
	
	if not SomeShown then RepeatOS = "<p>" & lne("label_no_records") & "</p>"  
	
	'response.write RepeatOS
	
%>
			<form  action="/utilities/account/repeat/" name=RepeatForm method=post>
			
			<%
			=RepeatOS
			%>
			
			<%
			if SomeShown then  
			%>
			<hr>
			<input type="submit" class="btn btn-primary" value="<%=lne("btn_add_to_basket")%>">
			<input type="hidden" class="FormHidden" name=FormAction value="REPEAT_ORDER">
			<input type="hidden" class="FormHidden" name=RepeatOrderRef value="<%=RepeatOrderRef%>">
			<input type="hidden" class="FormHidden" name=RecCount value="<%=RecCount%>">
			
			<%
			end if
			%>
			
			</form>
			
<%	
	
	
end if

set acDetails = nothing
dbClose()

%>				

</body>
</html>
