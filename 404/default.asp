<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Endura - Page not found</title>
	<style>
		BODY {
			font-family:arial;
			font-size:10pt;
			margin:200px;
		}
		
		A {
			text-decoration:none;
			color:orange;
			font-weight:bold;
		}
	</style>
</head>
<body>
	<div style="border:3px solid #333;text-align:center;padding:100px;border-radius:25px;color:white;background:#333;">
	<img src="/assets/images/furniture/nav_logo.png">
	<h3>Oops, sorry the page you requested could not be found</h3>
	<p>We have recently updated our B2B website, so your bookmarks may be out of date</p>
	
	<p><a href="/">Click here to return to the homepage</a></p>
	<p>If the problem persists, please <a href="/contact/">contact us</a></p>
	
</body>