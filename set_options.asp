<!--#include virtual="/assets/includes/global_functions.asp" -->

<%
dbConnect()

ReturnURL = request("ReturnURL")
jh "ReturnURL = " & ReturnURL

SetLanguageID = cleannum(request("SetLanguageID"))
SetLanguageCode = request("SetLanguageCode")

jh "SetLanguageID = " & SetLanguageID
jh "SetLanguageCode = " & SetLanguageCode

if SetLanguageID <> "" then
	jhInfo "SET LANGUAGE (" & SetLanguageID & ")"
	
	call setSelectedLanguageID(SetLanguageID)
	
end if

dbClose()

'jhStop


if ReturnURL = "" then ReturnURL = "/"
jhInfo "COMPLETE"

response.redirect ReturnURL
%>