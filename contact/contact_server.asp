<%
Response.ContentType = "text/html"
Response.AddHeader "Content-Type", "text/html;charset=ISO-8859-1"
'Response.CodePage = 65001
Response.CharSet = "ISO-8859-1"

Response.Expires = 0
Response.Expiresabsolute = Now() - 1442
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"

SelectedLanguageID = session("SelectedLanguageID")
if SelectedLanguageID = "" then SelectedLanguageID = "0"

%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	</head>
	<body>
	

<!--#include virtual="/assets/includes/global_functions.asp" -->
<%

'jh "now = " & now

ContactPointID = cleannum(request("ContactPointID"))
CountryCode = request("CountryCode")

if ContactPointID <> "" then

	dbConnect()

	sql = "SELECT ContactPointID, ContactPointName, ContactPointTel, ContactPointEmail FROM B2C_ContactPoints WHERE ContactPointName <> 'General Enquiry' AND ContactPointID = 0" & ContactPointID & " AND ContactPointStatusID = 1 "
	
	'jh sql
	set rs = db.execute(sql)
	if not rs.eof then
		ContactPointFound = true
		ContactPointName = trim("" & rs("ContactPointName"))
		ContactPointTel = trim("" & rs("ContactPointTel"))
		ContactPointEmail = trim("" & rs("ContactPointEmail"))
	end if
	rsClose()
	
	if ContactPointFound then
	%>
		<hr>
		<p><strong><%=ContactPointName%></strong></p>
		<p>T: <%=ContactPointTel%></p>
		<p>E: <a class=ExternalLink href="mailto:<%=ContactPointEmail%>"><%=ContactPointEmail%></p>
	<%
	end if

elseif CountryCode <> "" then

	dbConnect()
	
	if CountryCode = "UK" then
		sql = "SELECT ContactPointName, ContactPointTel FROM B2C_ContactPoints WHERE ShowIn = 'UK' "
		x = getrs(sql, TelArr, TelC)
		if TelC >= 0 then
		%>
			<hr>	
			<h3><%=CountryName%></h3>			
		<%
			for x = 0  to TelC
				ContactPointName = TelArr(0,x)
				ContactPointTel = TelArr(1,x)
			%>
				<p><%=ContactPointName%>: <strong><%=ContactPointTel%></strong></p>
			<%
			next
		end if
	else
		sql = "SELECT CountryName, B2CContactTel FROM END_CountryCodes WHERE CountryCode = '" & validstr(CountryCode) & "' OR CountryName = '" & validstr(CountryCode) & "' "	
		'jh sql
		set rs = db.execute(sql)
		if not rs.eof then
			AltContactTel = true
			B2CContactTel = trim("" & rs("B2CContactTel"))
			CountryName = trim("" & rs("CountryName"))
		end if
		rsClose()	
		
		if B2CContactTel <> "" then
		%>
			<hr>	
			<h3><%=CountryName%></h3>
			<p>T: <strong><%=B2CContactTel%></strong></p>
		<%		
		end if
		
		sql = "SELECT DistributorRegion, DistributorAddress, DistributorContactDetails, B2CContactFormEmailsTo, DistributorDetail FROM B2C_Distributors WHERE DistributorStatusID = 1 AND DistributorCountries LIKE '%|" & CountryCode & "|%' "
		jhAdmin sql
		set rs = db.execute(sql)
		if not rs.eof then
			DistributorRegion = "" & rs("DistributorRegion")
			DistributorAddress = "" & rs("DistributorAddress")
			DistributorContactDetails = "" & rs("DistributorContactDetails")
			B2CContactFormEmailsTo = "" & rs("B2CContactFormEmailsTo")
			'DistributorDetail = "" & rs("DistributorDetail")
			ContactEmailStr = ""
			if validemail(B2CContactFormEmailsTo) then
				ContactEmailStr = "<div>Email: <a class=ExternalLink href=""mailto:" & B2CContactFormEmailsTo & """>" & B2CContactFormEmailsTo & "</a></div>"
			end if
		end if	
		rsClose()
		
		if DistributorRegion <> "" then
		%>
			<hr>
			<div>International distributor</div>	
			<h3><%=DistributorRegion%></h3>
			<div class=spacer10><%=addbr(DistributorAddress)%></div>
			<div><%=addbr(DistributorContactDetails)%></div>			
			<div class=spacer40><%=addbr(ContactEmailStr)%></div>			
			
		<%		
		end if		
		
	end if
end if

dbClose()

'jh "ENDS"
%>				

</body>
</html>