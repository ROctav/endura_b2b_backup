<%
PageTitle = "Contact Endura"
%>

<!--#include virtual="/assets/includes/header.asp" -->

<%
FormAction = request("FormAction")



if FormAction = "FEEDBACK_SEND" then
	'jh "SENDING EMAIL"
	
	ContactEmail = glFeedbackFormTo
	
	SenderName = request("SenderName")
	SenderMessage = request("SenderMessage")
	
	if SenderMessage = "" then
		'ErrorMessage = "Please enter a message to send us"		
		ErrorMessage = "contact_error_missing_message"	
	else		
		
		'jhInfoNice "Email information", "Email being sent to: " & ContactEmail
		
		FeedbackTitle = glWebsiteName & " - FEEDBACK form received (" & AcDetails.AcCode & "/" & SenderName & ")"
		FeedbackBody = "A FEEDBACK form has been received from the Endura B2B website (" & glWebsiteURL & "):" & vbcrlf & vbcrlf
		
		FeedbackBody = FeedbackBody & "ACCOUNT CODE: " & AcDetails.AcCode & " (" & AcDetails.CompanyName & ")" & vbcrlf
		FeedbackBody = FeedbackBody & "Name: " & SenderName & vbcrlf			
		FeedbackBody = FeedbackBody & "Message: " & vbcrlf & SenderMessage & vbcrlf
		FeedbackBody = FeedbackBody & vbcrlf  & "================================" & vbcrlf 
		FeedbackBody = FeedbackBody & "Sent to: " & ContactEmail & vbcrlf 
		
		if isjh() then 
			ContactEmail = "jhutton1@gmail.com"
			FeedbackTitle = FeedbackTitle & " ** REDIRECTED TO JH"
		end if
		
		if isjh() then 
						
			jhAdmin "ContactEmail = " & ContactEmail			
			jhAdmin addbr(FeedbackBody)
		
			'jhInfo "NOT SENT! (isjh)"
		end if
		
		ContactSuccess = sendMail(FeedbackTitle, FeedbackBody, ContactEmail, "webmaster@endurasport.com", false)
		
		if ContactSuccess then
			EmailSent = true		
		else
			EmailSent = false			
			ErrorMessage = "contact_error_fatal_error"	
		end if
		
	end if
	
	
else
	
	'jh "ENDS"
	
	if isDealer() then
		SenderName = acDetails.ContactName
	end if
end if
%>

    <div id="contact">
        <div class="container">
            <div class="section_header">
                <h3><%=lne("page_title_feedback")%></h3>
            </div>
			
			<div class="row">
				<div class="span6 spacer40">
								
					
						<%
						if EmailSent then							
							jhInfoNice lne_B2C("contact_heading_success"), lne_B2C("contact_success")					
							response.write "<a class=OrangeLink href=""/"">" & lne("label_home_page_link") & "</a>"
						else	
						%>
							<div class=spacer20>
							<%=lne("feedback_explained")%>
							</div>
							
							<a class=OrangeLink href="/contact/"><%=lne("label_feedback_use_contact_form")%></a>
						<%
							if ErrorMessage <> "" then 								
								jhErrorNice lne_B2C("contact_heading_error"), lne_B2C(ErrorMessage)
							end if
							
						%>	
					
							
				<%
					end if
				%>				
				
				</div>
				
				<div class="span5 offset1">
							
					
						<%
						if EmailSent then
							
						else	
							
						%>	
					
							<div class=well>
							
								<form action="" method=post name=ContactForm>
									
									<div class="row">
										<div class="span6 box">
											<h5><%=lne("label_your_name")%></h5>
											<input class="input-xlarge" type="text" name=SenderName placeholder="<%=lne("label_your_name_placeholder")%> " value="<%=SenderName%>"><br>
																																
										</div>
										<div class="span6 box box_r">
											<h5><%=lne("label_your_message")%></h5>
											<textarea name=SenderMessage class="input-xlarge" style="height:180px;" placeholder="<%=lne("label_your_message_placeholder")%> "><%=SenderMessage%></textarea>
										</div>
										
									</div>

									<div class="row submit">
									
										<div class="span3 right">
											<input class="btn btn-warning" type="submit" value="<%=lne("contact_send_message_button")%>">
											<input class=FormHidden name=FormAction type="hidden" value="FEEDBACK_SEND">
										</div>
									</div>
								</form>					

						</div>
				<%
					end if
				%>									
					
				
				</div>				
				
			</div>
			
            <div class="spacer40">
			
               
            </div>
        </div>

        
		
		
    </div>

<!--#include virtual="/assets/includes/footer.asp" -->
