<%
PageTitle = "Contact Endura"
%>

<!--#include virtual="/assets/includes/header.asp" -->

<%
FormAction = request("FormAction")


sql = "SELECT CountryName, B2CContactEmail, B2CContactTel FROM JH_EnduraWeb.dbo.END_CountryCodes ec WHERE CountryCode = '" & validstr(AcDetails.CostCentre) & "'"
set rs = db.execute(sql)
if not rs.eof then
	
	CountryName = rs("CountryName")
	ContactEmail = "" & rs("B2CContactEmail")
	ContactTel = "" & rs("B2CContactTel")

	
end if	
rsClose()

if ContactEmail = "" then ContactEmail = glContactFormTo
if ContactTel = "" then ContactTel = glContactTel

if FormAction = "CONTACT_SEND" then
	'jh "SENDING EMAIL"
	
	SenderName = request("SenderName")
	SenderEmail = request("SenderEmail")
	SenderTel = request("SenderTel")
	SenderMessage = request("SenderMessage")

	SenderMessage = CharDecoder(SenderMessage)
     
	if not validemail(SenderEmail) and SenderTel = "" then
		ErrorMessage = "contact_error_missing"	
	elseif SenderMessage = "" then
		ErrorMessage = "contact_error_missing_message"	
	else		
		
		ContactTitle = glWebsiteName & " - Contact form message received (" & AcDetails.AcCode & "/" & SenderName & ")"
		ContactBody = "A message has been received from the Endura B2B website (" & glWebsiteURL & "):" & vbcrlf & vbcrlf
		
		ContactBody = ContactBody & "ACCOUNT CODE: " & AcDetails.AcCode & " (" & CountryName & ")" & vbcrlf
		ContactBody = ContactBody & "Name: " & SenderName & vbcrlf		
		ContactBody = ContactBody & "Email: " & SenderEmail & vbcrlf
		ContactBody = ContactBody & "Tel: " & SenderTel & vbcrlf
		ContactBody = ContactBody & "Message: " & vbcrlf & SenderMessage & vbcrlf
		ContactBody = ContactBody & vbcrlf  & "================================" & vbcrlf 
		ContactBody = ContactBody & "Sent to: " & ContactEmail & vbcrlf 
		
		ContactSuccess = sendMail(ContactTitle, ContactBody, ContactEmail, "webmaster@endurasport.com", false)
		
		if ContactSuccess then
			EmailSent = true		
		else
			EmailSent = false
			ErrorMessage = "contact_error_fatal_error"	
		end if
		
	end if
	
	
	
	'jh "ENDS"
end if
%>

    <div id="contact">
        <div class="container">
            <div class="section_header">
                <h3><%=getLabel(PageTitle)%></h3>
            </div>
			
				
			<div class="row">
				<div class="span5 spacer40">
								
					<h3><%=lne("contact_form_label")%></h3>

						<%
						if EmailSent then
							jhInfoNice lne_B2C("contact_heading_success"), lne_B2C("contact_success")					
						else	
							if ErrorMessage <> "" then 
								jhErrorNice lne_B2C("contact_heading_error"), lne_B2C(ErrorMessage)
							end if
							
						%>	
					
						<div class=well>
						
							<form action="" method=post name=ContactForm>
								
								<div class="row">
									<div class="span6 box">
									
										<input class="input-xlarge" type="text" name=SenderName placeholder="Your name" value="<%=SenderName%>"><br>
										<input class="input-xlarge" type="text" name=SenderEmail placeholder="Your email" value="<%=SenderEmail%>"><br>
										<input class="input-xlarge" type="text" name=SenderTel placeholder="Your tel no" value="<%=SenderTel%>"><br>																							
									</div>
									<div class="span6 box box_r">
										<textarea name=SenderMessage class="input-xlarge" style="height:180px;" placeholder="Type your message here..."><%=SenderMessage%></textarea>
									</div>
									
								</div>

								<div class="row submit">
								
									<div class="span3 right">
										<input class="btn btn-warning" type="submit" value="<%=lne("contact_send_message_button")%>">
										<input class=FormHidden name=FormAction type="hidden" value="CONTACT_SEND">
									</div>
								</div>
							</form>					

					</div>
				<%
				end if
				%>				
				
				</div>
				
				<div class="span6 offset1">
							
						<h3><%=lne("contact_heading_general")%></h3>	

						<p>Tel: <strong><%=ContactTel%></strong></p>
						<p>Email: <strong><a href="mailto:<%=ContactEmail%>" class="OrangeLink"><%=ContactEmail%></a></strong></p>
						
					<hr>
					<!--YOUR ACCOUNT MANAGER-->
					<h4><%=lne("home_heading_acm")%></h4>
<%
	jhNice "",  getContactInfo("ACM")

%>
					<!--YOUR ASE-->
					<h4><%=lne("home_heading_ase")%></h4>
<%
	jhNice "", getContactInfo("ASE")

%>
				
				</div>				
				
			</div>
			
            <div class="spacer40">
			
               
            </div>
        </div>

        
		
		
    </div>
<% 
    function CharDecoder(pText)
	
	CharDecoder = pText


    CharDecoder = replace(CharDecoder, "&#261;", "a")
	CharDecoder = replace(CharDecoder, "&#322;", "l")
	CharDecoder = replace(CharDecoder, "&#380;", "z")
	CharDecoder = replace(CharDecoder, "&#378;", "z")
	CharDecoder = replace(CharDecoder, "&#281;", "e")
	CharDecoder = replace(CharDecoder, "&#347;", "s")
	CharDecoder = replace(CharDecoder, "&#263;", "c")
	CharDecoder = replace(CharDecoder, "&#357;", "t")
	CharDecoder = replace(CharDecoder, "&#269;", "c")
	CharDecoder = replace(CharDecoder, "&#283;", "e")
	CharDecoder = replace(CharDecoder, "&#345;", "r")
	CharDecoder = replace(CharDecoder, "&#367;", "u")
	CharDecoder = replace(CharDecoder, "&#324;", "n")
	CharDecoder = replace(CharDecoder, "&#317;", "L")
	CharDecoder = replace(CharDecoder, "&#328;", "n")
	CharDecoder = replace(CharDecoder, "&#314;", "l")
	CharDecoder = replace(CharDecoder, "&#318;", "l")
	CharDecoder = replace(CharDecoder, "&#271;", "d")
	CharDecoder = replace(CharDecoder, "&#341;", "r")
	CharDecoder = replace(CharDecoder, "&#268;", "C")
	CharDecoder = replace(CharDecoder, "&#321;", "L")
	CharDecoder = replace(CharDecoder, "&#379;", "Z")
	CharDecoder = replace(CharDecoder, "&#280;", "E")
	CharDecoder = replace(CharDecoder, "&#356;", "T")

end function
%>
<!--#include virtual="/assets/includes/footer.asp" -->
