<%
PageTitle = "Ranging proposal admin"
%>

<!--#include virtual="/assets/includes/header.asp" -->
<!--#include virtual="/assets/includes/ranging_functions.asp" -->

<%
'jh "START HERE!"

RangingID = cleannum(request("RangingID"))
FormAction = request("FormAction")

DuplicateID = cleannum(request("DuplicateID"))

if isjh() and 1 = 0 then

	jhInfo "UPDATE ALL PROPOSALS!"
	
	sql = "SELECT RangingID, RangingTitle, AcCode FROM B2B_Ranging "
	
	x = getrs(sql, uArr, uc)
	
	for c = 0 to uc
		UpdateRangingID = uArr(0,c)
		UpdateRangingName = uArr(1,c)
		UpdateRangingAcCode = uArr(2,c)
		
		jhError UpdateRangingID & "_" & UpdateRangingName & "_" & UpdateRangingAcCode
		ThisOffersMatched = getMatchedOffers(UpdateRangingID, true)
		jh ThisOffersMatched
	next
	
	jhInfo "UPDATE ALL ENDS!"

end if


if FormAction = "SAVE" then
	
	ProposalFound = true
	
	AcCode = request("AcCode")	
	RangingTitle = request("RangingTitle")
	RangingBudget = convertnum(request("RangingBudget"))
	
	SuppressReporting = convertnum(request("SuppressReporting"))
	
	Budget_ROAD = convertnum(request("Budget_ROAD"))
	Budget_MOUNTAIN = convertnum(request("Budget_MOUNTAIN"))
	Budget_A2B = convertnum(request("Budget_A2B"))
	Budget_MEN = convertnum(request("Budget_MEN"))
	Budget_WOMEN = convertnum(request("Budget_WOMEN"))
	Budget_KIDS = convertnum(request("Budget_KIDS"))
	
	Budget_ACC = convertnum(request("Budget_ACC"))
	
	BenchmarkStartDate = getdate_JQ("BenchmarkStartDate")
	BenchmarkEndDate = getdate_JQ("BenchmarkEndDate")
	
	AllowDealerView = request("AllowDealerView")
	AllowDealerChanges = request("AllowDealerChanges")
	AllowDealerPrices = request("AllowDealerPrices")
	AllowDealerBudget = request("AllowDealerBudget")
	
	SelSharedTemplate = cleannum(request("SelSharedTemplate"))
	
	SharedTemplate = request("SharedTemplate")
	
	CSNotes = request("CSNotes")
	
	RangingStatusID = request("RangingStatusID")
	
	NewSeasonCodes = request("NewSeasonCodes")
	
	ProposedDeliveryDate = getdate_JQ("ProposedDeliveryDate")
	ForwardOrderWindowID = cleannum(request("ForwardOrderWindowID"))
	
	if Budget_ROAD < 0 then Budget_ROAD = 0
	if Budget_MOUNTAIN < 0 then Budget_MOUNTAIN = 0
	if Budget_A2B < 0 then Budget_A2B = 0
	if Budget_MEN < 0 then Budget_MEN = 0
	if Budget_WOMEN < 0 then Budget_WOMEN = 0
	if Budget_KIDS < 0 then Budget_KIDS = 0
	if Budget_ACC < 0 then Budget_ACC = 0
	
	'Budget_TOTAL = Budget_ROAD + Budget_MOUNTAIN + Budget_A2B 
	'if Budget_TOTAL > 0 then RangingBudget = Budget_TOTAL
	
	if RangingBudget = 0 then RangingBudget = (Budget_ROAD + Budget_MOUNTAIN + Budget_A2B)
	if RangingBudget = 0 then RangingBudget = (Budget_MEN + Budget_WOMEN + Budget_KIDS)
	
	if RangingTitle = "" then
		ErrorMessage = ErrorMessage & "<div>Enter a name for this ranging proposal</div>"
		ErrorsFound = true
	end if
	
	if not isdate(ProposedDeliveryDate) then
		ErrorMessage = ErrorMessage & "<div>Enter a proposed delivery date this ranging proposal</div>"
		ErrorsFound = true
	end if	
	
	sql = "SELECT AcCode FROM ExchequerCustDetails WHERE AcCode = '" & validstr(AcCode) & "' "
	if not AdminDetails.isGroupMember("admin") then
		sql = sql & " AND ( DepartmentCode = '" & AdminDetails.DepartmentCode & "' OR  DepartmentCode = 'KEY') "		
	end if
	
	'jh sql
	'jhstop
	set rs = db.execute(sql)
	if rs.eof then
		ErrorMessage = ErrorMessage & "<div>Account code not found</div>"
		ErrorsFound = true
	end if
	
	if not ErrorsFound then
		
		if RangingID = "" then
			sql = "INSERT INTO B2B_Ranging (AcCode, RangedBy) VALUES ('" & validstr(AcCode) & "', 0" & AdminDetails.AdminID & ");"
			db.execute(sql)
			sql = "SELECT MAX(RangingID) AS MaxRec FROM B2B_Ranging WHERE AcCode = '" & validstr(AcCode) & "' "
			set rs = db.execute(sql)
			if not rs.eof then
				RangingID = rs("MaxRec")
			end if
			rsClose()
		end if
		
		if RangingID <> "" then
		
		
			DelCheck = request("DelCheck")
			jhAdmin DelCheck

			if DelCheck <> "" then
					
					jhAdmin "DelCheck = " & DelCheck
					
					sql = "DELETE FROM B2B_RangingProducts WHERE RangingID = 0" & RangingID & " AND ProductID IN (" & DelCheck & ")"
					jhAdmin sql
					db.execute(sql)
					sql = "DELETE FROM B2B_RangingColorways WHERE RangingID = 0" & RangingID & " AND ProductID IN (" & DelCheck & ")"
					jhAdmin sql
					db.execute(sql)		
					sql = "DELETE FROM B2B_RangingSKUS WHERE RangingID = 0" & RangingID & " AND ProductID IN (" & DelCheck & ")"
					jhAdmin sql
					db.execute(sql)						
					'jhStop
								
			end if		
		
		
			ProposalFound = true
		
			ThisOffersMatched = getMatchedOffers(RangingID, true)
		
			sql = "UPDATE B2B_Ranging SET "
			
			sql = sql & "RangingStatusID = 0" & RangingStatusID & ", "		
			
			sql = sql & "SuppressReporting = 0" & SuppressReporting & ", "		
			
			
			sql = sql & "AcCode = '" & validstr(AcCode) & "', "
			sql = sql & "RangingTitle = '" & validstr(RangingTitle) & "', "
			sql = sql & "RangingBudget = 0" & RangingBudget & ", "	

			sql = sql & "ForwardOrderWindowID = 0" & ForwardOrderWindowID	 & ", "	
			
			sql = sql & "CSNotes = '" & validstr(CSNotes) & "', "
			
			sql = sql & "Budget_ROAD = 0" & Budget_ROAD & ", "		
			sql = sql & "Budget_MOUNTAIN = 0" & Budget_MOUNTAIN & ", "		
			sql = sql & "Budget_A2B = 0" & Budget_A2B & ", "		
			sql = sql & "Budget_MEN = 0" & Budget_MEN & ", "		
			sql = sql & "Budget_WOMEN = 0" & Budget_WOMEN & ", "		
			sql = sql & "Budget_KIDS = 0" & Budget_KIDS & ", "
			sql = sql & "Budget_ACC = 0" & Budget_ACC & ", "
			
			sql = sql & "AllowDealerView = 0" & AllowDealerView & ", "
			sql = sql & "AllowDealerChanges = 0" & AllowDealerChanges & ", "
			sql = sql & "AllowDealerPrices = 0" & AllowDealerPrices & ", "
			sql = sql & "AllowDealerBudget = 0" & AllowDealerBudget & ", "
			
			sql = sql & "SharedTemplate = 0" & SharedTemplate & ", "
			
			
			sql = sql & "NewSeasonCodes = '" & validstr(NewSeasonCodes) & "', "
			
		
			if isdate(BenchmarkStartDate) then
				sql = sql & "BenchmarkStartDate = '" & sqlDate(BenchmarkStartDate) & "', "
			else
				sql = sql & "BenchmarkStartDate = NULL, "
			end if			
			if isdate(BenchmarkEndDate) then
				sql = sql & "BenchmarkEndDate = '" & sqlDate(BenchmarkEndDate) & "', "
			else
				sql = sql & "BenchmarkEndDate = NULL, "
			end if
			if isdate(ProposedDeliveryDate) then
				sql = sql & "ProposedDeliveryDate = '" & sqlDate(ProposedDeliveryDate) & "', "
			else
				sql = sql & "ProposedDeliveryDate = NULL, "
			end if			
			
			sql = sql & "OffersMatched = '" & validstr(ThisOffersMatched) & "', "
			
			sql = sql & "DateUpdated = GETDATE() "
			sql = sql & "WHERE RangingID = 0" & RangingID & ""
			
			jhAdmin sql
			db.execute(sql)
			'
		end if
			
		DetailsUpdated = true
		
		ImportOption = request("ImportOption")
		
		'jh "ImportOption = " & ImportOption
		
		ImportList = request.form("ImportList")
		'jh "ImportList = " & ImportList
		
		ImportClear = request("ImportClear")
		
		if isjh() or glBackupRangingSummarys then
			call processRangingBackup(RangingID) 
		end if
		
		
		if ImportClear <> "" and ImportList <> "" then
			sql = "DELETE FROM B2B_RangingProducts WHERE RangingID = 0" & RangingID & ""
			'jh sql
			db.execute(sql)
			sql = "DELETE FROM B2B_RangingColorways WHERE RangingID = 0" & RangingID & ""
			'jh sql
			db.execute(sql)		
			sql = "DELETE FROM B2B_RangingSKUS WHERE RangingID = 0" & RangingID & ""
			'jh sql
			db.execute(sql)		
		end if		
		
		if SelSharedTemplate <> "" then
		
			sql = "DELETE FROM B2B_RangingProducts WHERE RangingID = 0" & RangingID & ""
			'jh sql
			db.execute(sql)
			sql = "DELETE FROM B2B_RangingColorways WHERE RangingID = 0" & RangingID & ""
			'jh sql
			db.execute(sql)		
			sql = "DELETE FROM B2B_RangingSKUS WHERE RangingID = 0" & RangingID & ""
			'jh sql
			db.execute(sql)				
		
			sql = "INSERT INTO B2B_RangingProducts (RangingID, ProductID) "
			sql = sql & "SELECT 0" & RangingID & ", ProductID FROM B2B_RangingProducts WHERE RangingID = 0" & SelSharedTemplate
			
			jhAdmin sql
			db.execute(sql)
			
			sql = "INSERT INTO B2B_RangingColorways (RangingID, ProductColorwayID, ProductID, ColorwayQty) "
			sql = sql & "SELECT 0" & RangingID & ", ProductColorwayID, ProductID, ColorwayQty FROM B2B_RangingColorways WHERE RangingID = 0" & SelSharedTemplate
			
			jhAdmin sql
			db.execute(sql)
			
			sql = "INSERT INTO B2B_RangingSKUS (RangingID, SKU, ProductColorwayID, ProductID, SKUQty) "
			sql = sql & "SELECT 0" & RangingID & ", SKU, ProductColorwayID, ProductID, SKUQty FROM B2B_RangingSKUS WHERE RangingID = 0" & SelSharedTemplate
			
			jhAdmin sql
			db.execute(sql)
			
		
		elseif DuplicateID <> "" then
			
			sql = "INSERT INTO B2B_RangingProducts (RangingID, ProductID) "
			sql = sql & "SELECT 0" & RangingID & ", ProductID FROM B2B_RangingProducts WHERE RangingID = 0" & DuplicateID
			
			jhAdmin sql
			db.execute(sql)
			
			sql = "INSERT INTO B2B_RangingColorways (RangingID, ProductColorwayID, ProductID, ColorwayQty) "
			sql = sql & "SELECT 0" & RangingID & ", ProductColorwayID, ProductID, ColorwayQty FROM B2B_RangingColorways WHERE RangingID = 0" & DuplicateID
			
			jhAdmin sql
			db.execute(sql)
			
			sql = "INSERT INTO B2B_RangingSKUS (RangingID, SKU, ProductColorwayID, ProductID, SKUQty) "
			sql = sql & "SELECT 0" & RangingID & ", SKU, ProductColorwayID, ProductID, SKUQty FROM B2B_RangingSKUS WHERE RangingID = 0" & DuplicateID
			
			jhAdmin sql
			db.execute(sql)
			
		elseif ImportOption <> "" then
		
			DelimChar = "~"
		
			ProcessList = replace(ImportList, chr(10), "|")	
			ProcessList = replace(ProcessList, chr(13), "")
			ProcessList = replace(ProcessList, chr(9), DelimChar)
			ProcessList = replace(ProcessList, ",", DelimChar)
			ProcessList = replace(ProcessList, ";", DelimChar)
			
			'jh "ProcessList = " & ProcessList
			
			ProcessArr = split(ProcessList, "|")
			
			ProcessC = ubound(ProcessArr)

			select case ImportOption
			
				case "SKU"
					QtyRequired = true	
					validsql = "SELECT ProductID, ProductColorwayID, SKU FROM SML_SKUS ss INNER JOIN END_PriceBandLookup rrpl ON rrpl.StockCode = ss.SKU WHERE ss.SKU = '***LOOKUP***' AND A_FO > 0 "				
				case "VARIANT"
					QtyRequired = true
					validsql = "SELECT ProductID, ProductColorwayID FROM SML_ProductColorways WHERE VariantCode = '***LOOKUP***' AND dbo.spB2BRangingActive(ProductID) > 0 "
				case "PRODUCT"
					QtyRequired = false 
					validsql = "SELECT ProductID FROM SML_Products WHERE ProductCode = '***LOOKUP***' AND dbo.spB2BRangingActive(ProductID) > 0 "
			end select
						
			'call deleteImportHolding()
			
			for c = 0 to ProcessC
				ProcessLine = ProcessArr(c)
				LineCount = LineCount + 1
				
				if ProcessLine <> "" then
					'jh "ProcessLine = " & ProcessLine
					
					ProcessItem = ""
					ProcessQty = 0
					
					LineArr = split(ProcessLine, DelimChar)
								
					if ubound(LineArr) >= 0 then			
						ProcessItem = ucase(trim(LineArr(0)))							
					end if
					
					if ubound(LineArr) >= 1 then
						'jh "VALID LINE"
						
						ProcessQty = convertnum(LineArr(1))				
						
					end if
					
					if ProcessItem <> "" and (ProcessQty > 0 or not QtyRequired) then
					
						CheckSQL = replace(validsql, "***LOOKUP***",  ProcessItem)
						'jh sql
						InsertThis = false
						if CheckSQL <> "" then
							
							set rs = db.execute(CheckSQL)
							
							if not rs.eof then
								InsertVal = "" & rs.fields(0).value				
								InsertThis = true	
								'jhInfo "FOUND!"
							else
								'jhError "NOT FOUND... " & InsertVal
								ErrorList = ErrorList & ProcessItem & ", "
								
							end if
							'jhstop
							'jh "InsertVal = " & InsertVal
							
							rsClose()
							
							if InsertThis then

								if ImportOption = "SKU" then
																
									sql = "INSERT INTO B2B_RangingSKUS (RangingID, SKU, ProductColorwayID, ProductID, SKUQty) "
									sql = sql & "SELECT 0" & RangingID & ", SKU, ProductColorwayID, ProductID, 0" & ProcessQty & " FROM SML_SKUS WHERE SKU = '" & validstr(ProcessItem) & "' "
									sql = sql & "AND NOT EXISTS (SELECT * FROM B2B_RangingSKUS r WHERE r.RangingID = 0" & RangingID & " AND r.SKU = SML_SKUS.SKU )"
									jh sql	
									db.execute(sql)								
									SomeImported = true	
									
								elseif ImportOption = "VARIANT" then
									
									sql = "INSERT INTO B2B_RangingColorways (RangingID, ProductColorwayID, ProductID, ColorwayQty) "
									sql = sql & "SELECT 0" & RangingID & ", ProductColorwayID, ProductID, 0" & ProcessQty & " FROM SML_ProductColorways WHERE VariantCode = '" & validstr( ProcessItem) & "' "
									sql = sql & "AND NOT EXISTS (SELECT * FROM B2B_RangingColorways rc WHERE rc.RangingID = 0" & RangingID & " AND rc.ProductColorwayID = SML_ProductColorways.ProductColorwayID )"
									jh sql		
									db.execute(sql)								
									SomeImported = true								
								
								elseif ImportOption = "PRODUCT" then
								
									sql = "INSERT INTO B2B_RangingProducts (RangingID, ProductID) "
									sql = sql & "SELECT 0" & RangingID & ", ProductID FROM SML_Products WHERE ProductCode = '" & validstr(ProcessItem) & "' "
									sql = sql & "AND NOT EXISTS (SELECT * FROM B2B_RangingProducts rp WHERE rp.RangingID = 0" & RangingID & " AND rp.ProductID = SML_Products.ProductID )"
									jh sql									
									db.execute(sql)
									SomeImported = true
								end if
								
							end if
							
						end if
					end if
					
				end if
			next	
			
			if SomeImported then
			
				if ImportOption = "SKU" then
					
					jh "INSERT COLORWAY QTYS FROM SKU INFO"
					
					sql = "INSERT INTO B2B_RangingColorways (RangingID, ProductID, ProductColorwayID, ColorwayQty) "
					sql = sql & "SELECT RangingID, ProductID, ProductColorwayID, SUM(SKUQty) AS ColorwaySum "
					sql = sql & "FROM B2B_RangingSKUS "
					sql = sql & "WHERE RangingID = 0" & RangingID & " " 
					
					sql = sql & "GROUP BY RangingID, ProductID, ProductColorwayID "
					db.execute(sql)
					
					'jhInfo sql		
					ImportProds = true
									
				elseif ImportOption = "VARIANT" then
					
					ImportProds = true
							
				end if		
				
				if ImportProds then
					sql = "INSERT INTO B2B_RangingProducts (RangingID, ProductID) "
					sql = sql & "SELECT DISTINCT 0" & RangingID & ", ProductID FROM B2B_RangingColorways WHERE RangingID = 0" & validstr(RangingID) & " "
					'jh sql	
					db.execute(sql)		
				end if
				
			end if
			
		end if
		
		'jhstop
		
		if ErrorList <> "" then
		
			ErrorsFound = true
			ErrorMessage = ErrorMessage & "<h5>Product import error!</h5><div class=spacer10>The following items were NOT imported - please check status of these items - are they valid for forward order ranging?</div><small>(Products must be listed in the MPL as valid for forward orders - ie forward order pricing exists; if in doubt check with Simon R)</small><div><strong>"& ErrorList & "</strong></div>"
		
		else
			dbClose()
			'jhstop
			response.redirect "/admin/ranging/"
		end if	
	end if

else
	
end if

if RangingID <> "" and not ErrorsFound then
	
	sql = "SELECT * FROM B2B_Ranging WHERE RangingID = 0" & RangingID & " "
	if not isjh() or 1 = 1 then
		sql = sql & " AND RangedBy = 0" & AdminDetails.AdminID
	end if
	
	'jhAdmin sql
	
	set rs = db.execute(sql)
	if not rs.eof then
		'jhInfo "FOUND!"
		RangingStatusID = rs("RangingStatusID")
		
		RangingTitle = rs("RangingTitle")
		AcCode = rs("AcCode")
		RangingBudget = rs("RangingBudget")
		
		Budget_ROAD = rs("Budget_ROAD")
		Budget_MOUNTAIN = rs("Budget_MOUNTAIN")
		Budget_A2B = rs("Budget_A2B")
		Budget_MEN = rs("Budget_MEN")
		Budget_WOMEN = rs("Budget_WOMEN")
		Budget_KIDS = rs("Budget_KIDS")	
		Budget_ACC = rs("Budget_ACC")	
		
		BenchmarkStartDate = rs("BenchmarkStartDate")
		BenchmarkEndDate = rs("BenchmarkEndDate")
		
		AllowDealerView = "" & rs("AllowDealerView")
		AllowDealerChanges = "" & rs("AllowDealerChanges")
		AllowDealerPrices = "" & rs("AllowDealerPrices")
		AllowDealerBudget = "" & rs("AllowDealerBudget")	
		SharedTemplate = "" & rs("SharedTemplate")	
		
		SuppressReporting = "" & rs("SuppressReporting")
		
		NewSeasonCodes = ""& rs("NewSeasonCodes")
		
		ProposedDeliveryDate = rs("ProposedDeliveryDate")
		
		CSNotes = rs("CSNotes")
		ForwardOrderWindowID = rs("ForwardOrderWindowID")
		
		ProposalFound = true
	else
		
	end if
	rsClose()
		
	'jhAdmin "ContactDetails = " & ContactDetails	
		
else
	
end if

if DuplicateID <> "" and RangingID = "" then
	
	sql = "SELECT * FROM B2B_Ranging WHERE RangingID = 0" & DuplicateID & ""
	'jhAdmin sql
	
	set rs = db.execute(sql)
	if not rs.eof then
		'jhInfo "FOUND!"
		RangingTitle = rs("RangingTitle") & " [COPY]"
		'AcCode = rs("AcCode")
		'RangingBudget = rs("RangingBudget")
		
		AllowDealerView = "" & rs("AllowDealerView")
		AllowDealerChanges = "" & rs("AllowDealerChanges")
		AllowDealerPrices = "" & rs("AllowDealerPrices")
		AllowDealerBudget = "" & rs("AllowDealerBudget")		
		
		BenchmarkStartDate = rs("BenchmarkStartDate")
		BenchmarkEndDate = rs("BenchmarkEndDate")		
		
		
	else
		
	end if
	rsClose()
		
	'jhAdmin "ContactDetails = " & ContactDetails	
	DuplicateFound = true
	'jhInfo "GET DUPLICATES!"
	
else
	
end if

if RangingID <> "" and not ProposalFound then
	redir = true
end if	

DelProductID = cleannum(request("DelProductID"))



if DelProductID <> "" then

	sql = "DELETE FROM B2B_RangingProducts WHERE RangingID = 0" & RangingID & " AND ProductID = 0" & DelProductID & ""
	jhAdmin sql
	db.execute(sql)
	sql = "DELETE FROM B2B_RangingColorways WHERE RangingID = 0" & RangingID & " AND ProductID = 0" & DelProductID & ""
	jhAdmin sql
	db.execute(sql)		
	sql = "DELETE FROM B2B_RangingSKUS WHERE RangingID = 0" & RangingID & " AND ProductID = 0" & DelProductID & ""
	jhAdmin sql
	db.execute(sql)		
end if

if redir then
	dbClose()
	response.redirect "/"
end if

'jhInfo "GET CURRENT STATUS OF ENTERED PRODUCTS?"

sql = "	SELECT rp.RangingID, sp.ProductID, sp.ProductCode, sp.ProductName, sp.Ranges, sp.ClassificationTags, sp.ProductTypes, rcw.cWAySum, Skus.SKUSum "
sql = sql & "FROM SML_Products sp "
sql = sql & "INNER JOIN B2B_RangingProducts rp ON rp.ProductID = sp.ProductID "
sql = sql & "LEFT JOIN (SELECT ProductID, Sum(ColorwayQty) AS cWAySum FROM B2B_RangingColorways WHERE RangingID = 52 GROUP BY ProductID) rcw ON rcw.ProductID = rp.ProductID "
sql = sql & "LEFT JOIN (SELECT ProductID, Sum(SKUQty) AS SKUSum FROM B2B_RangingSKUS WHERE RangingID = 0" & RangingID & " GROUP BY ProductID) skus ON skus.ProductID = rp.ProductID "
sql = sql & "WHERE rp.RangingID = 0" & RangingID & " " 					
'jh sql

x = getrs(sql, qtyArr, qtyC)

'jh "qtyC = " & qtyC

set errls = new strconcatstream
AllowConfirm = true

for c = 0 to qtyC
	
	ThisProductID = QtyArr(1,c)
	ThisProductCode = QtyArr(2,c)
	ThisProductName = QtyArr(3,c)
	ThisSKUQty = convertnum(QtyArr(8,c))
	if ThisSKUQty = 0 then
		AllowConfirm = false
		RangingStatusID = 0
		ProdErrorList = ProdErrorList & "<div class=""legible spacer5""><strong>" & ThisProductCode & "</strong> " & ThisProductName & " <a href=""?RangingID=" & RangingID & "&DelProductID=" & ThisProductID & """><i class=""icon icon-trash""></i></a></div>"
		
		ProdErrorList_CHECK = ProdErrorList_CHECK & "<div class=""legible spacer5""><input class=DelCheck type=checkbox name=DelCheck id=""DelCheck_" & ThisProductID & """ value=""" & ThisProductID & """> <label class=legible for=""DelCheck_" & ThisProductID & """><i class=""icon icon-trash""></i> <strong>" & ThisProductCode & "</strong> " & ThisProductName & "</label></div>"
		
	end if
	
next

'jh ProdErrorList

'jh adminDetails.isGroupMember("admin")
%>

    <div id="" class="">
        <div class="container">
		
			<div class="section_header">
				<h3><i title="Admin function" class="icon-cog"></i> <%=lne(PageTitle)%></h3>
			</div>		
		
			<div class=row>
				<div class=span12>
						
						<h4><%=AdminDetails.UserName%></h4>
						
						<p>Enter your ranging proposal details using the form below:</p>
						
						<%
						if ErrorsFound then jhErrorNice "Errors found", ErrorMessage
						%>						
						<%
						if DetailsUpdated then jhInfoNice "Ranging proposal has been updated", "<a href=""/admin/ranging/"">Back to previous page</a>"
						%>
						
						<form name=ACMContactDetailsForm method=post class=legible action="/admin/ranging/ranging-detail/">
							
							
							<div class=spacer10>
								<div>Name for this proposal (eg AW 2016)</div>
								<input type=text class=form-control name="RangingTitle" value="<%=RangingTitle%>">
							</div>
							<div class=spacer10>
								<div>Account code (required)</div>
								<input type=text class=form-control name="AcCode" value="<%=AcCode%>">
							</div>		
							<div class=spacer10>
								<div>Forward order season</div>
								
								<%
								sql = "SELECT ForwardOrderWindowID, ForwardOrderWindowName, ForwardOrderWindowStart FROM END_ForwardOrderWindow WHERE ExtremelyLateDate >= GETDATE() ORDER BY  ForwardOrderWindowStart "
								'jh sql 
								x = getrs(sql, foarr, fc)
								'jh "fc = " & fc
								%>		
								
								<%
								os = ""
								for c = 0 to fc
									ThisFOID = foarr(0,c)
									ThisFOName = foarr(1,c)
									ThisFOWindowStart = foarr(2,c)
									if ThisFOID = ForwardOrderWindowID then SelStr = "SELECTED" else SelStr = ""
									'ThisFOStartDate = dateadd("m", 5, ThisFOWindowStart)
									'jh "--->" & ThisFOName & " ThisFOWindowStart = " & ThisFOWindowStart 
									SetFODel = sqldate(dateadd("m", 7, ThisFOWindowStart))
									'jhInfo "SetFODel = " & SetFODel
									os = os & "<option data-fodate=""" & SetFODel & """ " & SelStr & " value=""" & ThisFOID & """>" & ThisFOName & "</option>"
									
									if not isdate(ProposedDeliveryDate) then ProposedDeliveryDate = sqldate(SetFODel)
									%>
									
									<%
								next
								%>
								<select class=form-control name=ForwardOrderWindowID  id=ForwardOrderWindowID>
								<%
								=os
								%>
								</select>
								<xhr>
								<%
								'=DrawCombo("ForwardOrderWindowID", "END_ForwardOrderWindow", "ForwardOrderWindowID", "ForwardOrderWindowName", "ForwardOrderWindowEnd >= GETDATE()", ForwardOrderWindowID, "ForwardOrderWindowStart" , false, true, "---")
								%>
							</div>									
							 
							<div class=spacer10>
								<div>Proposed delivery date</div>
								
								<%=drawDate_JQ("ProposedDeliveryDate", ProposedDeliveryDate)%>
							</div>								
							
							
							<hr>
							<h4>Status </h4>
							<%
							if xProdErrorList <> "" then
								jhErrorNice "Some products are missing SKU quantities - see below:", ProdErrorList & "<p>Click <a class=OrangeLink href=""/utilities/ranging/ranging-report/?RangingID=" & RangingID & """>here to view the summary report</a> and enter quantities before confirming the proposal"							
							end if
							
							if ProdErrorList_CHECK <> "" then
							
							%>
								<div class="alert alert-error">
									
									<h4>Some products are missing SKU quantities - see below:</h4>
									<div class=spacer10></div>
									<%
									=ProdErrorList_CHECK
									%>
									<p><input type="checkbox" id=CheckSelectAll> <label class=legible for="CheckSelectAll">Select all/none</label></p>
									
								</div>
							<%
							
								'jhErrorNice "Some products are missing SKU quantities - see below:", ProdErrorList_CHECK & "<p><a class=""btn btn-warning pointer"" id=DelSelected>Delete selected</a></p><p>Click <a class=OrangeLink href=""/utilities/ranging/ranging-report/?RangingID=" & RangingID & """>here to view the summary report</a> and enter quantities before confirming the proposal</p>"
								
								
							end if	
							%>
							
							
							<div>Once the proposal is set to confirmed, ready for CS import or complete, it will become read-only to both ACM and dealer.  Only proposals marked draft/template are editable</div>
							<div>When the proposal changes to 'ready for CS import', it will become visible to CS on their systems, and they can then import the order for processing and the proposal status will change to 'complete'</div>
							<hr>	
							<div class=spacer10>
								<%
								if RangingStatusID = "" then RangingStatusID = "0"
								sql = "SELECT SMLStatusID, SMLStatusName FROM SML_Status WHERE SMLTypeID = 376 ORDER BY SMLStatusID"
								z = getrs(sql, StatusArr, StatusC)
								for z = 0 to StatusC
									
									ThisStatusID = StatusArr(0,z)
									ThisStatusName = StatusArr(1,z)
									if ProdErrorList <> "" and ThisStatusID > 0 then 
										DisStr = "DISABLED" 
										StyleStr = "color:silver;"
									else 
										DisStr = ""
										StyleStr = ""
									end if
									
									if "" & ThisStatusID = "" & RangingStatusID then CheckStr = "CHECKED" else CheckStr = ""
								%>
									<div style="<%=StyleStr%>"> <input <%=CheckStr%> <%=DisStr%> type=radio name="RangingStatusID" value="<%=ThisStatusID%>"> <%=ThisStatusName%> </div>																
								<%
								next
								%>
												
								
							</div>
							<hr>	
							<div id="CSNotesDiv">
								<h4>Notes for customer services (optional - these will appear to CS when they are ready to process this proposal)</h4>	
								<textarea name="CSNotes" id="CSNotes" style="width:600px;height:60px;"><%=CSNotes%></textarea>	
							</div>	
							<hr>
							<h4>Options </h4>							
							<div class=spacer10>
							
								<div> <input <%if SuppressReporting = "1" then response.write "CHECKED"%> type=checkbox name="SuppressReporting" value=1> DO NOT include this ranging proposals in extranet reporting totals? </div>
							
								<div> <input <%if AllowDealerView = "1" then response.write "CHECKED"%> type=checkbox name="AllowDealerView" value=1> Allow dealer to view ranging proposal? </div>																
								<div> <input <%if AllowDealerChanges = "1" then response.write "CHECKED"%> type=checkbox name="AllowDealerChanges" value=1> Allow dealer to alter products/quantities? </div>								
								<div> <input <%if AllowDealerPrices = "1" then response.write "CHECKED"%> type=checkbox name="AllowDealerPrices" value=1> Prices & total values visible to dealer? </div>								
								<div> <input <%if AllowDealerBudget = "1" then response.write "CHECKED"%> type=checkbox name="AllowDealerBudget" value=1> Budget information, if entered, visible to dealer? </div>								
								
								<%
								if AdminDetails.isGroupMember("admin") or AdminDetails.isGroupMember("b2b_ranging_admin") then
								%>
									<hr>
									<div> <input <%if SharedTemplate = "1" then response.write "CHECKED"%> type=checkbox name="SharedTemplate" id="SharedTemplate" value=1> <label for="SharedTemplate"><span class="label label-success">SHARED TEMPLATE?</span></label> Allow the products/colorways/SKUs in this proposal to be used as a template by other ACMs?</div>								
								<%
								end if
								%>
							</div>	

							<hr>
							<H4>New products (optional)</h4>
							<div>Enter season code(s) eg SS16,AW16 to identify which products are to be displayed as 'NEW'</div>
							<input type=text style="width:90px;" class=form-control name="NewSeasonCodes" value="<%=NewSeasonCodes%>">
								
							<hr>
							<h4>Benchmarking information (optional) </h4>							
							<div class=spacer10>
								<div>Compare with sales from <%=drawDate_JQ("BenchmarkStartDate", BenchmarkStartDate)%> to <%=drawDate_JQ("BenchmarkEndDate", BenchmarkEndDate)%></div>								
							</div>								
							<hr>
							<h4>Budgeting information (optional) </h4>							
							<div class=spacer10>
								<div>Budget (TOTAL)</div>
								<input type=text class=form-control name="RangingBudget" value="<%=RangingBudget%>">
							</div>									
							
							<%
							if isjh() or 1 = 1 then

							%>

							<div class=spacer10>
								<div>Budget (ROAD)</div>
								<input type=text class=form-control name="Budget_ROAD" value="<%=Budget_ROAD%>">
							</div>	
							<div class=spacer10>
								<div>Budget (MOUNTAIN)</div>
								<input type=text class=form-control name="Budget_MOUNTAIN" value="<%=Budget_MOUNTAIN%>">
							</div>	
							<div class=spacer10>
								<div>Budget (A2B)</div>
								<input type=text class=form-control name="Budget_A2B" value="<%=Budget_A2B%>">
							</div>								
							<hr>
							<div class=spacer10>
								<div>Budget (MEN)</div>
								<input type=text class=form-control name="Budget_MEN" value="<%=Budget_MEN%>">
							</div>	
							<div class=spacer10>
								<div>Budget (WOMEN)</div>
								<input type=text class=form-control name="Budget_WOMEN" value="<%=Budget_WOMEN%>">
							</div>	
							<div class=spacer10>
								<div>Budget (KIDS)</div>
								<input type=text class=form-control name="Budget_KIDS" value="<%=Budget_KIDS%>">
							</div>							
							<hr>
							<div class=spacer10>
								<div>Budget (ACCESSORIES)</div>
								<input type=text class=form-control name="Budget_ACC" value="<%=Budget_ACC%>">
							</div>							
							<hr>							
							<%
							end if
							%>
							
							<%
							if DuplicateFound then
								jhInfoNice "Duplicate ranging proposal", "Click save to duplicate the contents of the selected ranging proposal"
							%>
							
							<%
							else
							%>
							
							<%
							if isjh() or 1 = 1 then
								sql = "SELECT RangingID, RangingTitle, AdminName FROM B2B_Ranging r INNER JOIN END_AdminUsers u ON r.RangedBy = u.AdminID WHERE SharedTemplate = 1 ORDER BY RangingTitle"
								'jh sql
								x = getrs(sql, templateArr, templateC)								
								'jh "templateC = " & templateC
								if templateC >= 0 then
								
							%>
									<h4>SELECT A RANGING TEMPLATE</h4>
									<div class=spacer5><span class="label label-important">NB Selecting a shared template to populate your proposal will clear all existing products</span></div>
									<div class=spacer5><input name=SelSharedTemplate type=radio CHECKED value=""> <em>None...</em></div>
									<%
									for c = 0 to templateC
										SharedRangingID = templateArr(0,c)
										SharedRangingName = templateArr(1,c)
										SharedRangingBy = "" & templateArr(2,c)
										
										if "" & SharedRangingID = "" & SelSharedTemplate then CheckStr = "CHECKED" else CheckStr = ""
									%>
										<div class=spacer5><input <%=CheckStr%> name=SelSharedTemplate type=radio value="<%=SharedRangingID%>"> <span class="label label-success"><%=SharedRangingName%></span> <small><%=SharedRangingBy%></small></div>
									<%
									next
									%>
									
									<hr>
							<%
								end if
							end if
							%>								
								
								<div><h4>IMPORT A LIST OF PRODUCTS</h4>
							
								<div class=spacer10>
									<strong>IMPORT LIST CONTAINS:</strong>								
									<input <%if ImportOption = "SKU" or ImportOption = "" then response.write "CHECKED"%> type=radio name="ImportOption" value="SKU"> SKUs (eg E9023R/5)
									<input <%if ImportOption = "VARIANT" then response.write "CHECKED"%> type=radio name="ImportOption" value="VARIANT"> Variants  (eg E9023R)
									<input <%if ImportOption = "PRODUCT" then response.write "CHECKED"%> type=radio name="ImportOption" value="PRODUCT"> Products (E9023, qtys not required)
								</div>
								
								<textarea name="ImportList" id="ImportList" style="width:600px;height:200px;"><%=ImportList%></textarea>
								

								<%
								if RangingID <> "" then	
									if 1 = 0 then
									%>
										<input type=hidden class=FormHidden value="1" name="ImportClear"> <span class="label label-important">NB Importing products to your proposal will clear all existing products</span>
									<%
									else
								%>
									<div class=spacer10>
										<strong>CLEAR EXISTING PRODUCTS?</strong>								
										<input type=checkbox name="ImportClear"> 								
									</div>					
								<%
									end if
								end if
							end if
							%>
								
							
							<div class=spacer20></div>
							
							<input name=SubmitButton type=submit class="btn-large btn-primary" value="Save changes"> 						
							<input type=hidden class=FormHidden name="FormAction" value="SAVE">	
							<input type=hidden class=FormHidden name="RangingID" name="RangingID" value="<%=RangingID%>">	
							<input type=hidden class=FormHidden name="DuplicateID" value="<%=DuplicateID%>">	
						
						</form>
				</div>

				
				
			</div>
		</div>
	</div>

	<div class="spacer40">&nbsp;</div>
	</div>
    

<!--#include virtual="/assets/includes/footer.asp" -->


<script>
	$("#DelSelected").click( function() {
		//var
	});
	
    $("#CheckSelectAll").change(function(){
      $(".DelCheck").prop('checked', $(this).prop("checked"));
     });	
	
	$("#ForwardOrderWindowID").change( function () {
		var fodate = $(this).find(':selected').data('fodate')
		//alert('fodate: '+ fodate)
		$("#ProposedDeliveryDate").val(fodate);
	});
	
</script>