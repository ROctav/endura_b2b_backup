<%
PageTitle = "Ranging proposals"

%>

<!--#include virtual="/assets/includes/header.asp" -->

<%

jhInfo "HERE!"


sql = "SELECT r.RangingID, r.AcCode, ecd.CompanyName, r.RangedBy, AdminName, RangingStatusID, RangingTitle, RangingDescription, AllowDealerChanges "
sql = sql & "FROM B2B_Ranging r "
sql = sql & "INNER JOIN ExchequerCustDetails ecd ON ecd.AcCode = r.AcCode "
sql = sql & "INNER JOIN END_Adminusers u ON u.AdminID = r.RangedBy "
sql = sql & "WHERE RangedBy =  0" & AdminDetails.AdminID & " "
sql = sql & "ORDER BY DateAdded DESC "

jh sql

x = getrs(sql, RangingArr, RangingC)

jhAdmin "RangingC = " & RangingC

'jhstop

set ls = new strconcatstream

ls.add "<table border=1 class=""ReportTable legible"">"

	ls.add "<tr id=""RepeatLine_" & c & """ class=""RepeatLine"">"
	ls.add "<th colspan=2></th>"									
	ls.add "<th>Proposal name</th>"									
	ls.add "<th>Account</th>"																			
	ls.add "<th>By</th>"	
	ls.add "</tr>"

for c = 0 to RangingC

	ThisRangingID = RangingArr(0,c)
	ThisRangingName = RangingArr(6,c)
	
	ThisAcCode = RangingArr(1,c)
	ThisCompanyName = RangingArr(2,c)
		
	ThisRangedBy = RangingArr(4,c)	

	ls.add "<tr id=""RepeatLine_" & c & """ class=""RepeatLine"">"
	ls.add "<td><a href=""/admin/control/?impCode=JIMB01&RangingID=" & ThisRangingID & """><i class=""pointer icon-search""></i></a>"
	ls.add "<td><a href=""/admin/ranging/ranging-detail/?RangingID=" & ThisRangingID & """><i class=""pointer icon-cog""></i></a>"
	ls.add "<td><strong>"
	ls.add ThisRangingName
	ls.add "</strong></td>"	
	ls.add "<td>"
	ls.add ThisAcCode
	ls.add "<div><small>"
	ls.add ThisCompanyName
	ls.add "</small></div>"		
	ls.add "</td>"		
	ls.add "<td>"
	ls.add ThisRangedBy
	ls.add "</td>"					
	ls.add "</td>"
	
	ls.add "</tr>"

next					
					
ls.add "</table>"

RepeatOS = ls.value

%>

<style>
	.ReportTable TD {
		padding:5px;
	}
	
	.RepeatLineSelected{
		background-color:<%=glDarkColor%>;
	}
	
</style>

<link rel="stylesheet" href="css/index.css" type="text/css" media="screen" />

	<div class="container">      

		<div class="row">      
			
		
			<div class="span8 spacer10">
		
					<h3 class=spacer20><%=PageTitle%></h3>
					
					<div class=well>
						Select a date <%
						=DrawDate_JQ("FilterDate", FilterDate)
						%>
					</div>
					&nbsp;		
					
					<%
					=RepeatOS
					%>
					
			</div>	
			
			<div class="span4 spacer10">
				
					<div id="RepeatAjaxDiv">
						<div class=spacer20></div>
						<%
						if OrderRepeated then						
							jhInfoNice "Selected items added to basket!", "Your chosen items have been added to your basket - <a href=""/checkout/"">click here to go to the checkout</a>"
						elseif OrderRepeatError then
							jhErrorNice "No items selected", "You did not select any valid quantitys - please re-enter"
						end if
						%>
						
					</div>
							
			</div>
		</div>		
	</div>		

<script>
	$(document).ready(function() {
		
		//select and deselect
		$("#btnCheckAll").click(function () {
			$('.RemoveCheck').prop('checked', true);
			//alert('clicked')
			$('.QuickSearchSKUQty').val('0');
			$('.QuickSearchSKUQty').prop('disabled', true);
			
		});
		
		$("#FilterDate").change( function() {
			//alert(	$( this ).val() )
			window.location.assign("/admin/recent/?FilterDate=" + $( this ).val())
		});

			
	});	
</script>	
	
<script>

	function getRepeatDetails(pRef, pRow) {
		//alert('in')
		//$('#ajax_prod_results').fadeout('fast')
		
		$("#RepeatAjaxDiv").html('<span class="AjaxLoader"><img src="/assets/images/icons/indicator.gif"> loading...</span>');
		
		var ThisID = pRef
		
		//alert(pRef)
		
		$.ajax({
			//contentType: "application/x-www-form-urlencoded;charset=ISO-8859-1",
			//dataType: 'json',
			scriptCharset: "ISO-8859-1" ,
			contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
			url: "/admin/admin_server.asp?CallType=RECENT_ORDERS&B2BOrderID=" + ThisID,
			cache: false,			
			}).done(
				function( html ) {
				$("#RepeatAjaxDiv").html(html);
				$('#RepeatAjaxDiv').show(100, function() {
					// Animation complete.
					$('.RepeatLine').removeClass('RepeatLineSelected');
					$('#RepeatLine_' + pRow).addClass('RepeatLineSelected');
					
					
				  });					
			});
		
	}

</script>	
	
<!--#include virtual="/assets/includes/footer.asp" -->

