<%
PageTitle = "Ranging proposal admin"
%>

<!--#include virtual="/assets/includes/header.asp" -->
<!--#include virtual="/assets/includes/ranging_functions.asp" -->

<style>
	.QtyExists {
		color:green;
		font-weight:bold;
		background:lightgreen !important;
	}
	.QtyChanged {
		color:white !important;
		font-weight:bold;
		background:green !important;
	}	
	
</style>
<%
'jh "START HERE!"

RangingID = cleannum(request("RangingID"))
FormAction = request("FormAction")

if RangingID <> "" and not ErrorsFound then
	
	sql = "SELECT * FROM B2B_Ranging WHERE RangingID = 0" & RangingID & " "
	if not isjh() or 1 = 1 then
		sql = sql & " AND RangedBy = 0" & AdminDetails.AdminID
	end if
	
	'jhAdmin sql
	
	set rs = db.execute(sql)
	if not rs.eof then
		'jhInfo "FOUND!"
		RangingStatusID = convertnum(rs("RangingStatusID"))
		'jh "RangingStatusID = " & RangingStatusID
		
		RangingTitle = rs("RangingTitle")
		AcCode = rs("AcCode")
		RangingBudget = rs("RangingBudget")
		
		Budget_ROAD = rs("Budget_ROAD")
		Budget_MOUNTAIN = rs("Budget_MOUNTAIN")
		Budget_A2B = rs("Budget_A2B")
		Budget_MEN = rs("Budget_MEN")
		Budget_WOMEN = rs("Budget_WOMEN")
		Budget_KIDS = rs("Budget_KIDS")	
		Budget_ACC = rs("Budget_ACC")	
		
		BenchmarkStartDate = rs("BenchmarkStartDate")
		BenchmarkEndDate = rs("BenchmarkEndDate")
		
		AllowDealerView = "" & rs("AllowDealerView")
		AllowDealerChanges = "" & rs("AllowDealerChanges")
		AllowDealerPrices = "" & rs("AllowDealerPrices")
		AllowDealerBudget = "" & rs("AllowDealerBudget")		
		
		NewSeasonCodes = ""& rs("NewSeasonCodes")
		DispNewSeasonCodes = NewSeasonCodes
		
		ProposedDeliveryDate = rs("ProposedDeliveryDate")
		
		CSNotes = rs("CSNotes")
		ForwardOrderWindowID = rs("ForwardOrderWindowID")
		
		if isAdminUser() and RangingStatusID = 0 then AllowAdminChanges = 1 else AllowAdminChanges = 0
		
		ProposalFound = true
	else
		
	end if
	rsClose()
		
	'jhAdmin "ContactDetails = " & ContactDetails	
		
else
	
	dbClose
	
	response.redirect "/admin/ranging/"
	
end if

sql = "SELECT spt.ProductTypeID, spt.ProductTypeName, spt.ProductTypeName_FULL, nc.NavClassificationName, NavClassificationTopLevel, spt.NavClassificationID "
sql = sql & "FROM SML_ProductTypes spt "
sql = sql & "INNER JOIN SML_NavClassifications nc ON nc.NavClassificationID = spt.NavClassificationID "
sql = sql & "WHERE ISNULL(NavClassificationTopLevel, '') <> '' "
sql = sql & "ORDER BY NavClassificationZOrder "

x = getrs(sql, sptArr, sptc)

sql = "SELECT SimpleColorID, SimpleColorName FROM SML_ColorwayList_SIMPLE ORDER BY SimpleColorName "
x = getrs(sql, SimpleColorArr, SimpleColorC)
'jh "SimpleColorC = " & SimpleColorC

sql = "SELECT OutfitID, OutfitName FROM SML_ProductOutfits spod WHERE OutfitStatusID = 1 ORDER BY OutfitZOrder, OutfitID"
x = getrs(sql, OutfitArr, OutfitC)	
jhAdmin "OutfitC = " & OutfitC

sql = "SELECT ProductTypeID, ISNULL(ProductTypeName_FULL, ProductTypeName), ParentID AS PRoductTypeName FROM SML_ProductTypes ORDER BY PRoductTypeZORder "
x = getrs(sql, ProductTypeArr, ProductTypeC)
'jh "ProductTypeC = " & ProductTypeC

CheckNewProducts = request("CheckNewProducts")

SummarySearchVal = trim("" & request("SummarySearchVal"))

	if SummarySearchVal <> "" and 1 = 1 then
		SummarySearchVal_ORIG = SummarySearchVal
		sql = "SELECT TOP 1 p.ProductID, p.ProductCode FROM SML_Products p "
		sql = sql & "INNER JOIN SML_ProductColorways spc ON spc.ProductID = p.ProductID "
		sql = sql & "INNER JOIN SML_SKUS ss ON ss.ProductColorwayID = spc.ProductColorwayID "
		sql = sql & "WHERE spc.VariantCode = '" & validstr(SummarySearchVal) & "' OR ss.SKU = '" & validstr(SummarySearchVal) & "' "
		'jh sql
		x = getrs(sql, vararr, varc)
		'jh varc
		if varc = 0 then
			'jhInfo "FOUND FROM VARIANT/SKU - " & vararr(1,0)
			SummarySearchVal = vararr(1,0)
		end if
		
	end if	

FilterEndUse = convertnum(request("FilterEndUse"))
'jh "FilterEndUse = " & FilterEndUse
FilterDemoGraphic = convertnum(request("FilterDemoGraphic"))
'jh "FilterDemoGraphic = " & FilterDemoGraphic	

FilterNavClassification = convertnum(request("FilterNavClassification"))

'jh request("FilterProductType")
FilterProductType = convertnum(request("FilterProductType"))

FilterColor = convertnum(request("FilterColor"))

FormAction = request("FormAction")

FilterOutfitID = convertnum(request("FilterOutfitID"))


CheckShowBenchmarking = "1" ' request("CheckShowBenchmarking")
CheckShowAdded = convertnum(request("CheckShowAdded"))
CheckPreviousPurchases = request("CheckPreviousPurchases")
CheckShowRanged = convertnum(request("CheckShowRanged"))

if CheckNewProducts = "1" then
	npSql = ""
	NewProductsOnly = true
	
	NewSeasonCodes = cleancheck(NewSeasonCodes)
	npArr = split(NewSeasonCodes, "|")
	for z = 0 to ubound(npArr)
		ThisSeason = trim("" & npArr(z))
		if ThisSeason <> "" then
			'jh "ThisSeason = " & ThisSeason
			npSql = npSql & "'" & ThisSeason & "', "
		end if
	next
	if npSql <> "" then
		npSql = "sp.ProductSeason IN (" & npSql & " 'XXXX')"
	end if
end if

'jh npSql
'jh ProdErrorList

'jh adminDetails.isGroupMember("admin")

	if 1 = 0 or CheckShowBenchmarking = "1" or CheckPreviousPurchases = "1" then

		BenchmarkAcCode = AcCode
		
		BenchC = -1
		RotationC = -1	
		
		if isdate(BenchmarkStartDate) and isdate(BenchmarkEndDate)  then
			BenchmarkStartDate_EX = exDate(BenchmarkStartDate)
			BenchmarkEndDate_EX = exDate(BenchmarkEndDate)
			
			sql = ""
			sql = sql & "SELECT ss.ProductID, SUM(QtyDelivered) AS BenchMarkSum "
			sql = sql & "FROM END_SalesOrderLineValues esv INNER JOIN SML_SKUS ss ON ss.SKU = esv.stockcode "
			sql = sql & "WHERE AccountCode = '" & BenchmarkAcCode & "' "
			sql = sql & "AND DueDate > " & BenchmarkStartDate_EX & " AND DueDate < " & BenchmarkEndDate_EX & " "
			sql = sql & "GROUP BY ss.ProductID "
			'jhInfo sql
			
			x = getrs(sql, BenchArr, BenchC)
			'jhAdmin "BenchC = " & BenchC
			
		end if
		
		if isjh() or glShowRotationFigures then
		
			RotationWindowStartDate = dateadd("m", -6, now)	
			RotationWindowStartDate_EX = ExDate(RotationWindowStartDate)
			
			sql = "SELECT ss.ProductID, SUM(esf.Qty) AS RotationSum " 
			sql = sql & "FROM END_SalesFigures esf "
			sql = sql & "INNER JOIN transactions t ON t.thOurRef = esf.tlOurRef "
			sql = sql & "INNER JOIN SML_SKUS ss ON ss.SKU = esf.idxStockCode "
			sql = sql & "WHERE esf.idxAcCode = '" & BenchmarkAcCode & "' AND (t.thYourRef = 'STOCK ROTATION' OR t.thYourRefLong = 'STOCK ROTATION/B2B')  "
			sql = sql & "AND Transdate >= " & RotationWindowStartDate_EX & " "
			sql = sql & "GROUP BY ss.ProductID "
			
			jhAdmin sql
			x = getrs(sql, RotationArr, RotationC)
			jhAdmin "RotationC = " & RotationC
		else	
			
		end if		
	
	end if
	
jhAdmin "FormAction = " & FormAction	
	
if FormAction <> "" then RunReport = true	
	
if RunReport then	
	
	sql = "SELECT sp.ProductID, sp.ProductCode, sp.ProductName, VariantCode, ColorName, ss.SKU, sz.SizeName, DefaultProductImage, ColorCode, ProductName_TRANSLATED "
	sql = sql & ", SKUSum, 0 AS BenchMarkSum, ProductSeason, CatalogueZOrder, " & AcDetails.CostBand & "_FO, SKUQty, spc.ProductColorwayID, AdminNotes, DealerNotes, DealerUpdatedDate "
	sql = sql & "FROM SML_Products sp "
	sql = sql & "INNER JOIN SML_ProductTypes spt ON spt.ProductTypeID = sp.ProductTypeID "
	sql = sql & "INNER JOIN SML_SKUS ss ON ss.ProductID = sp.ProductID "
	sql = sql & "INNER JOIN SML_ProductColorways spc ON spc.ProductColorwayID = ss.ProductColorwayID "
	sql = sql & "INNER JOIN SML_SKUStatus sta ON sta.SKUStatusID = ss.SKUStatusID "
	sql = sql & "INNER JOIN SML_SizeList sz ON sz.SizeID = ss.SizeID "
	sql = sql & "INNER JOIN END_PriceBandLookup rl ON rl.StockCode = ss.SKU "
	sql = sql & "LEFT JOIN (SELECT ProductID, ProductName AS ProductName_TRANSLATED FROM SML_ProductTranslations WHERE LAnguageID = 0" & AcDetails.SelectedLanguageID & ") tra ON tra.ProductID = sp.ProductID "
	sql = sql & "LEFT JOIN (SELECT SUM(SKUQty) AS SKUSum, ProductID FROM B2B_RangingSKUS WHERE RangingID = 0" & RangingID & " GROUP BY ProductID) skuQtys ON skuQtys.ProductID = sp.ProductID "
	sql = sql & "LEFT JOIN (SELECT SKU, SKUQty FROM B2B_RangingSKUS WHERE RangingID = 0" & RangingID & ") rskus ON rskus.SKU = ss.SKU "
	sql = sql & "LEFT JOIN (SELECT ProductID AS RangedProductID FROM B2B_RangingProducts WHERE RangingID = 0" & RangingID & ") rprods ON rprods.RangedProductID = sp.ProductID "
	
	sql = sql & "LEFT JOIN SML_ProductOutfitDetails spod ON spod.ProductColorwayID = spc.ProductColorwayID "
	sql = sql & "LEFT JOIN (SELECT ProductID, AdminNotes, DealerNotes, DealerUpdatedDate FROM B2B_RangingNotes WHERE RangingID = 0" & RangingID & ") rn ON rn.ProductID = sp.ProductID "
	
	sql = sql & "WHERE 1 = 1 "
	'sql = sql & "AND PRoductName LIKE '%JACKET%' "
		
	sql = sql & " AND sta.B2BRangingVisible = 1  AND " & AcDetails.CostBand & "_FO > 0 "  	
	'sql = sql & " AND sp.PublishOnB2B = 1 "
		
	if SummarySearchVal <> "" then
		sql = sql & "AND (ProductCode LIKE '%" & validstr(SummarySearchVal) & "%' OR ProductName LIKE '%" & validstr(SummarySearchVal) & "%' OR BulletPoints LIKE '%" & validstr(SummarySearchVal) & "%' OR ProductName_TRANSLATED LIKE '%" & validstr(SummarySearchVal) & "%' OR ProductKeywords LIKE '%" & validstr(SummarySearchVal) & "%' OR ProductTypeName LIKE '%" & validstr(SummarySearchVal) & "%') "
	end if	

	if FilterEndUse <> 0 then
		sql = sql & "AND sp.Categories LIKE '%|" & FilterEndUse & "|%' "
	end if	
	if FilterDemoGraphic <> 0 then
		sql = sql & "AND sp.Demographics LIKE '%|" & FilterDemoGraphic & "|%' "
	end if	
	if FilterOutfitID > 0 then
		sql = sql & "AND spod.OutfitID = 0" & FilterOutfitID & " "
	end if		
	
	
	if FilterColor <> 0 then
		sql = sql & "AND spc.ColorCode IN (SELECT ColorCode FROM SML_ColorwayList_SIMPLE_MATRIX WHERE SimpleColorID = 0" & FilterColor & ")"
	end if
	if FilterProductType <> 0 then
		'sql = sql & "AND sp.ProductTypes LIKE '%|" & FilterProductType & "|%' "
	end if
	if FilterNavClassification <> 0 then
		sql = sql & "AND spt.NavClassificationID = 0" & FilterNavClassification
	end if	
	
	if CheckShowAdded = 1 then
		
		sql = sql & "AND SKUSum > 0 "
		
	end if
	
	if CheckShowRanged =  1 then
		
		sql = sql & "AND ISNULL(RangedProductID,0) > 0 "
		
	end if	
	
	if CheckNewProducts = "1" and npSql <> "" then
		'jhAdmin "NewProductsOnly = " & NewProductsOnly
		'jhStop
		sql = sql & "AND (" & npSql & ") "
	end if	

	sql = sql & "ORDER BY ISNULL(CatalogueZOrder,9999), DefaultProductImage DESC, VariantCode, sz.SizeZOrder  "
	
	
	jhadmin sql
	'jhstop 
	
	x = getrs(sql, ProdArr, ProdC)	
	'jhAdmin "ProdC = " & ProdC

		set ls = new strconcatstream
		set sizels = new strconcatstream

		MaxProducts = 1000

		ls.add "<table border=1 class=""legible ReportTable"">"

		for c = 0 to ProdC

			ThisProductID = ProdArr(0,c)
			ThisProductCode = ProdArr(1,c)
			ThisProductName = ProdArr(2,c)
			ThisProductColorwayID = ProdArr(16,c)
			ThisVariantCode = ProdArr(3,c)
			ThisColor = ProdArr(4,c)
			
			ThisAdminNotes = ProdArr(17,c)
			ThisDealerNotes = ProdArr(18,c)
			ThisDealerNotesUpdated = ProdArr(19,c)
			'jh ThisDealerNotesUpdated
			
			if ThisProductID <> LastProductID then
			
				ThisSeason = ProdArr(12,c)
				DispSeason = ""
				if ThisSeason <> "" and CheckNewProducts = "1" then DispSeason = " <span title=""Season code"" class=""label label-info"">" & ThisSeason & "</span>"
				
				ThisBenchmarkSum = 0	
				ThisRotationSum = 0
				
				MinPrice = 9999
				MaxPrice = 0
				
				for z = c to ProdC
					if ThisProductID = ProdArr(0,z) then
						ThisPrice = convertnum(ProdArr(14,z))
						if ThisPrice > 0 and ThisPrice < MinPrice then
							MinPrice = ThisPrice
						end if	
						if ThisPrice > 0 and ThisPrice > MaxPrice then
							MaxPrice = ThisPrice
						end if					
					else
						exit for
					end if
				next		
				
				PriceStr = ""
				
				if MaxPrice > 0 then
					if MaxPrice <> MinPrice then
						PriceStr = "<span class=""label label-success"">" & DispCurrency(MinPrice) & " - " & DispCurrency(MaxPrice) & "</span>"
						'jh "++++" & ThisProductCode
					else
						PriceStr = "<span class=""label label-success"">" & DispCurrency(MaxPrice) & "</span>"
					end if
				end if
				
				if CheckShowBenchmarking = "1" or CheckPreviousPurchases = "1" then
					if BenchC >= 0 then
						for y = 0 to BenchC
							if ThisProductID = BenchArr(0,y) then
								'jh "Found! " & ThisProductCode
								ThisBenchmarkSum = convertNum(BenchArr(1,y))
								exit for
							end if
						next			
					end if	
					if RotationC >= 0 then
						for y = 0 to RotationC
							if RotationArr(0,y) = ThisProductID then
								ThisRotationSum = convertNum(RotationArr(1,y))
								exit for
							end if
						next
					end if
				end if	
			
				ShowThis = true
			
				if isjh() then
					if ThisBenchmarkSum = 0 and CheckPreviousPurchases = "1" then
						ShowThis = false
					end if
				end if
				
				if ShowThis then
			
					ProductCount = ProductCount + 1
				
					ls.add "<tr style=""background:lightblue;color:#333;"">"		
					
					ls.add "<td colspan=10>"
					ls.add "<div class=pull-right>"
					if ThisAdminNotes <> "" or ThisDealerNotes <> "" then
						ls.add "&nbsp;<span data-c=""" & c & """ class=""label label-info pointer NoteLink ""><i class=""icon icon-white icon-comment""></i> *</span>"
					else
						ls.add "&nbsp;<i data-c=""" & c & """ class=""pointer NoteLink icon icon-comment""></i>"
					end if
					ls.add "</div>"
					
					if ThisBenchmarkSum > 0 then					
						ls.add "<span title=""Qty ordered in prior window"" class=""label label-info"" style=""font-size:120%;"">" & ThisBenchmarkSum & "</span>&nbsp;"			
					end if	
					if ThisRotationSum > 0 then					
						ls.add "<span title=""Qty rotated in last 6 months"" class=""label label-important"" style=""font-size:120%;"">" & ThisRotationSum & "</span>&nbsp;"			
					end if				
					
					
					ls.add ThisProductName
					ls.add DispSeason
					ls.add "<sdiv class=pull-right><span class=""label label-default"">" & ThisProductCode & "</span> " & PriceStr & "</div>"
				
					
					ls.add "<span class=AjaxFeedbackContainer id=""AjaxFeedback_" & ThisProductID & """></span>"
					
					
					ls.add "</td>"		
					ls.add "</tr>"
					
					ls.add "<tr id=""NoteDiv_" & c & """ style=""display:none;background:#f0f0f0;color:#333;"">"		
					
					ls.add "<td colspan=10>"
					ls.add "<div class=small>ACM Comments</div>"
					ls.add "<div><textarea class=""NotesArea legible control-sm"" data-pid=""" & ThisProductID & """ title=""Enter notes for " & ThisProductName & """ style=""width:550px;height:80px;"" name=""AdminNotes_" & c & """ id=""AdminNotes_" & c & """>" & ThisAdminNotes & "</textarea></div>"
					ls.add "<div class=small></div>"
					if ThisDealerNotes <> "" then 
						ls.add "<span class=""label label-default"">DEALER: " & ThisDealerNotesUpdated & "</span>"
						ls.add "<div style=""width:550px;"">""" & ThisDealerNotes & """</div>"
					else
						ls.add "<em>None...</em>"
					end if
					ls.add "</td>"		
					ls.add "</tr>"					
					
					LastProductID = ThisProductID
					
					if ProductCount > MaxProducts then
						exit for
					end if
				end if
			else

			end if
			
			if ShowThis then 
			
				if ThisVariantCode <> LastVariantCode then
				
					ThisImg = getImageDefault(ThisProductCode, ThisVariantCode)
				
					ls.add "<tr>"		
					ls.add "<td>"
					ls.add "<img src=""" & ThisImg & """ style=""height:40px;"">"
					ls.add "</td>"

					
					SizesFound = false
					sizels.clear
					
					for z = c to ProdC
						if ThisVariantCode = ProdArr(3,z) then
							SizesFound = true
							ThisSKU = ProdArr(5,z)
							SizeName = ProdArr(6,z)
							ThisSKUQty = convertnum(ProdArr(15,z))

							
							if z < ProdC then
								targetid = z+1
							else
								targetid = ""
							end if
							
							if ThisSKUQty > 0 then
								'sizels.add "<span class=""label label-warning"">" & ThisSKUQty & "</span>"
								DispSKUQty = ThisSKUQty
								ClassStr = "QtyExists"
							else
								DispSKUQty = ""
								ClassStr = ""
							end if				
							
							if AllowAdminChanges <> 1 then disStr = "DISABLED" else disStr = ""
							
							sizels.add "<div class=""input-prepend"">"
							sizels.add "<span title=""" & ThisSKU & """ class=""add-on legible " & ClassStr & """>" & SizeName & " <i id=""statusIcon_" & z & """ class=""icon""></i></span>"
							sizels.add "<input " & disStr & " AUTOCOMPLETE=OFF data-zpos=" & z & " data-productid=" & ThisProductID & " data-productcolorwayid=" & ThisProductColorwayID & "  data-variantcode=" & ThisVariantCode & " data-targetnext=""" & targetid & """ data-sku=""" & ThisSKU & """ class=""span1 legible SKUInput"" style=""width:30px !important;"" id=""Qty_" & z & """ name=""Qty_" & z & """ type=""number"" value=""" & DispSKUQty & """>"
							sizels.add "</div>&nbsp;"

							ls.add "<div><input class=FormHidden type=hidden id=""SKU_" & z & """ name=""SKU_" & z & """></div>"
							

						else
							exit for
						end if
					next
					
					ls.add "<td>"
					ls.add ThisColor
					if SizesFound then
						'ls.add "<icon data-productcolorwayid=""" & ThisProductColorwayID & """ class=""icon icon-remove pointer removeColorway""></i>"
					end if
					ls.add "&nbsp;"
					ls.add "<div><span class=""label label-default"">"
					ls.add ThisVariantCode
					ls.add "</span></div>"
					ls.add "</td>"		
					ls.add "<td>"		
					
					if SizesFound then
						SizeStr = sizels.value
						ls.add SizeStr	
					end if
					
					ls.add "</td>"
					ls.add "</tr>"
					
					LastVariantCode = ThisVariantCode
					
					SomeFound = true
				else

				end if	
			end if
		next
		ls.add "</table>"

		os = ls.value 

		if not SomeFound then os = "<span class=""label label-default"">No products found!</span>"
	end if

%>

    <div id="" class="">
        <div class="container">
		
			<div class="xsection_header">
				<h3><i title="Admin function" class="icon-cog"></i> Product Quantity Entry Form</h3>
			</div>		
		
			<div class=row>
				<div class=span12>		
				
					<hr>
						<div class=spacer20>
							<span class="label label-inverse"><%=AcDetails.AcCode%></span> <span class=legible><%=AcDetails.CompanyName%></span>
							<h3><%=RangingTitle%><br><small><%=PageTitle%></small></h3>
							<%
							if isAdminUser() then
							%>
								<a class=OrangeLink href="/admin/ranging/">Ranging Admin</a> | 
								<a class=OrangeLink href="/admin/ranging/ranging-detail/?RangingID=<%=RangingID%>"><i class="icon icon-cog"></i></a> | 
							<%
							end if
							%>					
							<a class=OrangeLink href="/utilities/ranging/">Back to ranging proposal</a> |
							<a class=OrangeLink href="/utilities/ranging/ranging-report/?RangingID=<%=RangingID%>">Summary report</a>
						</div>				
					<hr>
				
				
				</div>
			</div>
		

			<div class=row>
				<div class=span12>
					
			<form name=cwayForm id=cwayForm action="/admin/ranging/price-list/" method=post>
					

				<div class=well>
						
					<div class=row>
						<div class="span6 legible">
						
							<%
							if DispNewSeasonCodes <> "" then
							%>
							
							<div class=spacer5><input type=checkbox name=CheckNewProducts id=CheckNewProducts <%if "" & CheckNewProducts = "1" then response.write "CHECKED" %> value="1"> <label for="CheckNewProducts" class="legible">New products only</label>&nbsp;<span class="label label-info"><%=DispNewSeasonCodes%></span></div>
							
							<%
							end if
							%>
							
							<input class=form-control type=text name=SummarySearchVal id=SummarySearchVal value="<%=SummarySearchVal_ORIG%>">
							
						</div>		

						<div class="span5 legible">
							<%
							if 1 = 1 then
								if isdate(BenchmarkStartDate) and isdate(BenchmarkEndDate)  then
							%>
									<!--<div class=spacer5><input type=checkbox name=CheckShowBenchmarking id=CheckShowBenchmarking <%if "" & CheckShowBenchmarking = "1" then response.write "CHECKED" %> value="1"> <label for="CheckShowBenchmarking" class="legible">Show previous sales/returns (will slow the page load down slightly)</label> </div>-->
									<div class=spacer5><input type=checkbox name=CheckPreviousPurchases id=CheckPreviousPurchases <%if "" & CheckPreviousPurchases = "1" then response.write "CHECKED" %> value="1"> <label for="CheckPreviousPurchases" class="legible">Show ONLY products purchased in previous window</label> </div>
							<%
								end if
							%>
								<div class=spacer5><input type=checkbox name=CheckShowAdded id=CheckShowAdded <%if "" & CheckShowAdded = "1" then response.write "CHECKED" %> value="1"> <label for="CheckShowAdded" class="legible">Show products with entered qtys only</label> </div>
								<div class=spacer5><input type=checkbox name=CheckShowRanged id=CheckShowRanged <%if "" & CheckShowRanged = "1" then response.write "CHECKED" %> value="1"> <label for="CheckShowRanged" class="legible">Show ranged products only</label> </div>
							<%
							end if
							%>
							
						</div>						
						
					</div>							
						
					<div class=row>
						<div class=span3>
						
	<%
	
	
	for c = 0 to OutfitC

		ThisOutfitID = OutfitArr(0,c)
		ThisOutfitName = OutfitArr(1,c)
		
		
		if "" & ThisOutfitID = "" & FilterOutfitID then
			SelStr = "SELECTED" 
		else
			SelStr = "" 
		end if
		
		OutfitOS = OutfitOS & "<option " & SelStr & " value=""" & ThisOutfitID & """>" & ThisOutfitName & "</option>"
		
	next

	
	for c = 0 to sptc

		ThisClassificationID = sptArr(5,c)
		ThisClassificationName = sptArr(3,c)
		
		if ThisClassificationID <> LastClassificationID then
			
			if "" & ThisClassificationID = "" & FilterNavClassification then
				SelStr = "SELECTED" 
			else
				SelStr = "" 
			end if
			
			TypeOS = TypeOS & "<option " & SelStr & " value=""" & ThisClassificationID & """>" & ThisClassificationName & "</option>"
			LastClassificationID = ThisClassificationID
			
		end if	
	next
	%>
							<select class="FilterOption legible" name="FilterNavClassification">
								<option value="">--Product type filter---</option>
								<%=TypeOS%>
							</select>
							
						</div>			
						<div class=span3>
							<select class="FilterOption legible" name=FilterEndUse id=FilterEndUse>
								<option value="">---end use---</option>
								<option <%if FilterEndUse = "1" then response.write "SELECTED"%> value="1">ROAD</option>
								<option <%if FilterEndUse = "2" then response.write "SELECTED"%> value="2">MOUNTAIN</option>
								<option <%if FilterEndUse = "18" then response.write "SELECTED"%> value="18">A2B</option>
								<option <%if FilterEndUse = "14" then response.write "SELECTED"%> value="14">OTHER</option>
							</select>	
						</div>	
<%
if OutfitC > 0 then
%>						
						<div class=span3>
							<select class="FilterOption legible" name=FilterOutfitID id=FilterOutfitID>
								<option value="">---Coordinated Outfits---</option>
								<%
								=OutfitOS
								%>
							</select>	
						</div>	
<%
end if
%>						
					</div>			
					<div class=row>

						<div class=span3>
							<select class="FilterOption legible" name=FilterDemographic id=FilterDemographic>
								<option value="">---demographic---</option>
								<option <%if FilterDemoGraphic = "3" then response.write "SELECTED"%> value="3">MEN</option>
								<option <%if FilterDemoGraphic = "1" then response.write "SELECTED"%> value="1">WOMEN</option>
								<option <%if FilterDemoGraphic = "2" then response.write "SELECTED"%> value="2">KIDS</option>
							</select>	
						</div>	
						<div class=span3>
							<select class="FilterOption legible" name=FilterColor id=FilterColor>
								<option value="">---color---</option>
								<%
								for c = 0 to SimpleColorC
									ThisSimpleColorID = SimpleColorArr(0,c)
									ThisSimpleColorName = SimpleColorArr(1,c)
								%>
									<option <%if "" & ThisSimpleColorID = "" & FilterColor then response.write "SELECTED"%> value="<%=ThisSimpleColorID%>"><%=ThisSimpleColorName%></option>
								<%
								next
								%>
								
							</select>	
						</div>	
						<div class=span1>
							
							<input class="btn btn-success" type="submit" value="GO">	
							
							<input type=hidden class=FormHidden name="RangingID" value="<%=RangingID%>">	
							
							<input type=hidden class=FormHidden name="FormAction" value="RUN_REPORT">	
						</div>						
					</div>
				</div>								
			</form>		
					
					
						<%
						=os
						%>
								
							
					
				</div>

				
				
			</div>
		</div>
	</div>

	<div class="spacer40">&nbsp;</div>
	</div>
    
<script>

	$(".NotesArea").change( function () {
		
		var pid = $( this ).data("pid");
		var NoteText = $( this ).val()
		
		NoteText = NoteText.substr(0,1000)
		//alert(NoteText);
		$.ajax({
			scriptCharset: "ISO-8859-1" ,
					contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
					url: "/utilities/ranging/ranging_server.asp?CallType=SAVE_NOTES&CallContext=ACM&ProductID=" + pid + "&NoteText=" + NoteText + "&RangingID=<%=RangingID%>",				
					cache: false, 
					
					}).done( function( html ) {
							//alert('done...' + html)						
							$("#AjaxFeedback_" + pid).html(html);	
				
		
		});
		//alert("done.")
	});

	$(".NoteLink").click( function () {
		//alert('click');
		var rowpos = $( this ).data("c");
		$("#NoteDiv_"+rowpos).toggle("fast");
	})
	
	$(".SKUInput").keypress(function(e) {
		if(e.which == 13) {
			var targetnext = $( this ).data("targetnext");
			if (targetnext!='') {
				//alert(targetnext);
				$("#Qty_" + targetnext).focus();
				$("#Qty_" + targetnext).select();
			} 
			
		}
	});


	$(".SKUInput").change( function () {
		
		var zpos = $( this ).data("zpos");
		var setSKU = $( this ).data("sku");
		var setQty = $( this ).val();
		var productID = $( this ).data("productid");
		var variantCode = $( this ).data("variantcode");
		var ProductColorwayID = $( this ).data("productcolorwayid");
		//alert('change');
		$("#SKU_" + zpos).val(setSKU)
		
		//$( this ).removeClass("QtyExists");
		//$( this ).addClass("QtyChanged");
		
		$( this ).addClass("QtyChanged");
		
		$(".AjaxFeedbackContainer").html('');
		
		$.ajax({

			scriptCharset: "ISO-8859-1" ,
			contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
			url: "/utilities/ranging/ranging_server.asp?CallType=PL_UPDATE&ProductID=" + productID + "&ProductColorwayID=" + ProductColorwayID + "&SetSKU=" + setSKU + "&setQty=" + setQty + "&RangingID=<%=RangingID%>",				
			cache: false, 
			
			}).done( function( html ) {
					//alert('done...')						
					$("#AjaxFeedback_" + productID).html(html);						
					//$("#AjaxModalDiv").show();
					//drawProds("PAGE");
					
					$( "#statusIcon_" + zpos ).addClass("icon-ok");
		});				
		
	});
	
	function updateSKU(pSKU, pQty)	{

	}	
	
</script>
	
<!--#include virtual="/assets/includes/footer.asp" -->


<%
public HierarchyComboCurrentLevel, HComboLS

function padHCombo(pLevel)
	dim c
	padHCombo = ""
	for c = 2 to pLevel
		padHCombo = padHCombo & "----"
	next
end function

function popHCombo(byVal pParent, byVal pArr, byVal pRC, byVal pCurrentVal)
	
	dim c
	'jh "looking for = " & pParent
	
	HierarchyComboCurrentLevel = HierarchyComboCurrentLevel + 1
	pParent = "" & pParent
	for c = 0 to pRC
		
		ThisID = "" & pArr(0,c)
		ThisParent = "" & pArr(2,c)
		
		if ThisParent = pParent then
			'jh padHCombo(HierarchyComboCurrentLevel) & "Found = " & ThisID & " (" & pArr(1,c) & ")"
			HComboLS.add "<option value=""" & pArr(0,c) & """"
			HComboLS.add pArr(0,c)
			HComboLS.add """"
			
			if "" & pArr(0,c) = trim(cstr("" & pCurrentVal)) then
				HComboLS.add " SELECTED"
			end if
			
			'DrawCombo = DrawCombo & ">" & pArr(1,c) & "</option>"			
			HComboLS.add ">"
			padStr = padHCombo(HierarchyComboCurrentLevel)
			HComboLS.add padStr
			HComboLS.add pArr(1,c)
			
			HComboLS.add "</option>"		
			
			call popHCombo(ThisID, pArr, pRC, pCurrentVal)
			
		end if
		
	next
	
	HierarchyComboCurrentLevel = HierarchyComboCurrentLevel - 1
	
end function

function DrawCombo_HIERARCHY(SelectName, TableName, IDField, TextField, WhereStr, ComboVal, OrderField , Descending, LimitToList, NotInListText, ParentField, StartParent)


	'jh "UPDATE DRAWCOMBO"
		'jh ChangeSubmit
		dim x, sql 
		set HComboLS = new strConcatStream
		dim RecArr
		'jh TableName
		
		if IDField = TextField and instr(1, IDField, "AS ") = 0 then 
			IDFIeld = IDField  & " AS ComboIDField"
			TextField = TextField  & " AS ComboTextField"
		end if
		
		if UseDistinct then
			DistinctStr = "DISTINCT "
		end if
		
		UseDistinct = false
		sql = "SELECT " & DistinctStr & " " & IDField & "," & TextField & ", " & ParentField & " " & DefField & " FROM " & TableName 
		if WhereStr <> "" then
			sql = sql & " WHERE " & WhereStr
		end if
		if OrderField <> "" then
			sql = sql & " ORDER BY " & OrderField
			if Descending then
				sql = sql & " DESC"
			end if
		end if		
		sql = sql & ";"
		
		'jhAdmin sql
		
		x = getrs(sql, RecArr, rc)
		'jh "rc = " & rc
		
		DrawCombo_HIERARCHY = ""
		
		if ChangeSubmit <> "" then ChangeStr = "onchange=""document." & ChangeSubmit & ".submit();"""
		if EventStr <> "" then ChangeStr = EventStr
		EventStr = ""

		pInd = pInd+1
		'DrawCombo = DrawCombo & "<select TABINDEX=" & pInd & " " & ChangeStr & " class=FormSelect style="";"" name=""" & SelectName & """ id=""" & SelectName & """>"
		HComboLS.add "<select xTABINDEX="
		HComboLS.add pInd
		HComboLS.add " " 
		HComboLS.add ChangeStr
		HComboLS.add " class=FormSelect style=""font-size:9pt;;"" name="""
		HComboLS.add SelectName
		HComboLS.add """ id="""
		HComboLS.add SelectName
		HComboLS.add """>"	
		
		if not LimitToList then
			if NotInListText = "" then
				'DrawCombo = DrawCombo & "<option value="""">Not in List...</option>"
				HComboLS.add "<option value="""">Not in List...</option>"
			else
				'DrawCombo = DrawCombo & "<option value="""">" & NotInListText & "</option>"			
				HComboLS.add "<option value="""">" & NotInListText & "</option>"
			end if
		end if		
		
		call popHCombo(StartParent, RecArr, rc, ComboVal)
		
		'DrawCombo = DrawCombo & "</select>."		
		HComboLS.add "</select>."		
		DrawCombo_HIERARCHY = HComboLS.value
		
		ChangeSubmit = ""
		set HComboLS = nothing		
		
		exit function
		
		'========================================
		'========================================
		'========================================
		'========================================
	
end function

%>