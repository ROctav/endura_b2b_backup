<%
PageTitle = "Import ranging proposal product qtys "
%>

<!--#include virtual="/assets/includes/header.asp" -->

<style>
	.HomePanelLayer {
		color:white;
	}
	.header-panel {		
		color:white;
		
	}
	
	A.whitelink {
		color:white;
	}
	
	.header-title {		
		color:white;
		font-size:36pt;
		font-family:Oxygen, arial;
		text-align:left;
	}	
	
	.header-trans {
		background-image:url(/assets/images/home_panels/semi-trans.png);
		padding:20px 0 20px 0;
		xmargin-top:20px;
		
	}
	
	.header-button {
		text-align:left;
	}
	
	#slide1, #slide2, #slide3, #slide4 {
		border:0px solid red;
		padding-top:50px;
		height:200px;
	}
	
	#feature_slider {
		height:250px !important;
	}
	
	.slide {
		height:220px;
	}
	
	.header-title {
		line-height:30pt;font-size:24pt;
	}
	
	.colSpacer_LEFT {
		border-left:1px solid #f0f0f0;
		padding-left:20px;
	}
	
	.ProductGrid TD {
		font-family:arial;
		font-size:10pt;
		padding:5px;
	}
	
	.legible {
		font-family:arial;
		font-size:8pt;
	}
	
	.basketAddButton {
		border:1px solid red;
	}
	
	#CheckoutTable {
		border:1px solid silver;
	}
	
	#CheckoutTable TD {
		font-family:arial;
		font-size:9pt;
		padding:7px;
	}
	
	.CheckoutImage {
		width:50px;
	}
	
	.CheckoutQty {
		width:40px;
		text-align:right;
	}
	
</style>

<%

FormAction = request("FormAction")
ImportArea = request("ImportArea")
ImportOptions = request("ImportOptions")

ImportMax = 500
DelimChar = "~"


if FormAction = "xIMPORT_CHECK" or FormAction = "IxMPORT_SUBMIT" then
	'jh "ImportArea = " & ImportArea

end if

RangingID = session("RangingID")

%>



<link rel="stylesheet" href="css/index.css" type="text/css" media="screen" />

<style>
	#ImportArea {
		width:200px;
		height:250px;
	}
</style>

	<div class="container">      

		  <div class="row">      
			<div class="span12 spacer10">
			
			
				<h2><%=PageTitle%></h2>

						
				<%
				=announceStr
				%>
			
				
				<hr>
			</div>
		</div>	
		
	
		 
		 <div class=row>
			<div class="span3">	
							
				<div class=xspacer10>
					<strong><%=lne("import_upload_a_file")%></strong>					
				</div>
				
				<form ENCTYPE="MULTIPART/FORM-DATA"  id="CSVForm" method=post action="/admin/ranging/import/upload_server.asp?CallType=IMPORT_CSV">
					<p><small><%=lne("import_upload_a_file_explained")%></small></p>
				
					
					<input type="file" style="visibility:hidden;" id="ImportFile" name="ImportFile" /><br/>
					<div class="input-append">
						<input type="text" name="subfile" id="subfile" class="input-small">
						<a class="btn" onclick="$('#ImportFile').click();"><%=lne("label_import_browse")%></a>
					</div>
					<input type=submit name=btnCheckFile name=btnCheckFile class="btn btn-small btn-success" value="<%=lne("btn_import_check_now")%>">
					
				</form>
			
				<form name=formImportText id=formImportText method=post action="/admin/ranging/import/upload_server.asp?CallType=IMPORT_TEXT">
			
					<hr>
				
					<p><small><%=lne("import_input_area_title_explained")%></small></p>
					
					<textarea style="width:100%;" placeholder="[Paste your order items here]" class=legible id=ImportArea name=ImportArea><%=ImportArea%></textarea>
										
					<p><small><%=lne_REPLACE("label_import_max_explained", ImportMax)%></small></p>
					
					<input type=submit name=btnCheck name=btnCheck class="btn btn-small btn-success" value="<%=lne("btn_import_check_now")%>">
					
				</form>
				
				
			</div>
			<div class="span8 offset1">	
			
					
			
<%
				ReturnCode = request("ReturnCode")
				
				if ReturnCode = "IMPORT_COMPLETE" then
					UnitTotal = convertnum(request("UnitTotal"))
				
%>
					<div class="alert alert-success">
						
						<h4 class=spacer20><%=lne("label_import_complete")%></h4>						
						<p><a class=OrangeLink href="/utilities/ranging/?RangingID=<%=RangingID%>">Back to ranging proposal</a></p>
						
					</div>
<%				
				
				elseif ReturnCode = "NONE_IMPORTED" then
				
					jhErrorNice lne("label_errors_found"), lne("label_import_none_imported_error")
				
				elseif ReturnCode = "TEXT_ERROR" then

					jhErrorNice lne("label_errors_found"), lne("label_import_text_error")
				
				elseif ReturnCode = "FILE_ERROR" then

					ErrFile = request("ErrFile")	
					jhErrorNice lne("label_errors_found"), lne_REPLACE("label_import_file_error", ErrFile)
				
				elseif ReturnCode = "FILE_OK" or ReturnCode = "TEXT_OK" then

					'jhInfo "NEW CHECK"
					
					sql = "exec spB2BImportRanging_CHECK '" & AcDetails.AcCode & "', " & RangingID & " "
					jhAdmin sql
	
					
					x = getrs(sql, ImpArr, ImpC)
					
					jhAdmin "ImpC = " & ImpC
					SomeValid = false
					
					
					
					set ls = new strconcatstream
					
					ls.add "<table class=""legible ReportTable"" border=1>"
					
					ls.add "<tr>"
					ls.add "<th colspan=2>" & lne("label_sku") & "</th>"
					ls.add "<th>" & lne("label_qty") & "</th>"	
					ls.add "<th>" & lne("label_product_name") & "</th>"
					ls.add "<th>" & lne("label_color") & "</th>"
					ls.add "<th>" & lne("label_size") & "</th>"		
					ls.add "<th>" & lne("label_notes") & "</th>"					
					ls.add "<th>" & lne("label_import") & "</th>"
					ls.add "</tr>"
					
					DuplicateError = lne("label_import_duplicate_line")
					LimitedStockError = lne("label_limited_stock")
					InvalidSKUError= lne("label_sku_not_found")
					CannotBuyError= lne("label_sku_not_for_sale")
					
					for c = 0 to ImpC
					
						ThisInstanceCount = convertNum(ImpArr(0,c))
						
						ThisSKU = ImpArr(2,c)
						ThisQty = convertnum(ImpArr(3,c))						
	
						ThisName = ImpArr(9,c)
						ThisColor = ImpArr(10,c)					

						ThisSize = ImpArr(11,c)
					
					
					
						DisabledStr = ""
						DisabledReason = ""
						RowStyle = ""
						CheckStr = "CHECKED"
					
						ThisSKUStatusID = "" & ImpArr(6,c)
						ThisCanPurchase = "" & ImpArr(7,c)
						
						ThisProductID = ImpArr(12,c)
						ThisVariantcode = ImpArr(8,c)
						GetStock = false
												
												
												
						if ThisSKUStatusID = "" then
							DisabledStr = "DISABLED"
							DisabledReason = InvalidSKUError
							RowStyle = "color:silver;"
							
							CheckStr = ""
							GetStock = false
						elseif ThisCanPurchase = "0" then
							DisabledStr = "DISABLED"
							DisabledReason = CannotBuyError
							RowStyle = "color:silver;"						
							CheckStr = ""
							'SomeValid = true						
						else
							SomeValid = true
						end if
						
						SomeFound = true
						
						if ThisInstanceCount > 1 then
							DisabledReason = DisabledReason & "<span class=""label label-warning"">" & DuplicateError & "</span>"
						end if
						
						if DisabledReason = "" then DisabledReason = "<span class=""label label-success"">OK</span>"
					
						ls.add "<tr style=""" & RowStyle & """>"
						ls.add "<td valign=top>"
						ls.add "<a href=""/products/?ProductID=" & ThisProductID & """><i class=""icon icon-search""></i></a>"
						ls.add "</td>"					
						ls.add "<td valign=top>"
						ls.add ThisSKU
						ls.add "</td>"
						ls.add "<td class=numeric valign=top><h4 style=""padding:0;margin:0;"">"
						ls.add ThisQty
						ls.add "</h4></td>"							
						ls.add "<td valign=top>"
						ls.add ThisName
						ls.add "</td>"
						ls.add "<td valign=top>"
						ls.add ThisColor
						ls.add "</td>"
						ls.add "<td valign=top>"
						ls.add ThisSize
						ls.add "</td>"
				
						
						ls.add "<td valign=top>"
						ls.add DisabledReason
						ls.add "</td>"

						ls.add "<td class=text-center valign=top>"
						ls.add "<input " & DisabledStr & " " & CheckStr & " type=checkbox name=""ImpList"" value=""" & ThisSKU & """>"
						ls.add "</td>"							
						
						ls.add "</tr>"
					next
					
					ls.add "</table>"
					
					os = ls.value
					
					if ImpC = -1 then os = ""
				
					set ls = nothing
				end if
%>

				
				<%
				if SomeFound then
				%>
					
					<form name=formImportText id=formImportText method=post action="/admin/ranging/import/upload_server.asp?CallType=IMPORT_CONFIRM">
					
						<%
						=os
						%>
						
						<div class=spacer20></div>
						<%
						if SomeValid then
						%>
							
							<p><input <%if ImportOptions = "CLEAR" then response.write "CHECKED"%> value="CLEAR" name="ImportOptions" type=checkbox> <label for="ImportOptions">Clear all current products from this proposal?</label></p>			
							<div class=spacer20></div>						
							<input type=submit name=btnConfirm name=btnConfirm class="btn btn-large btn-success" value="<%=lne("btn_import_now")%>">
						<%
						else
						
							jhErrorNice lne("label_errors_found"), lne("label_import_no_valid_skus")
						
						end if
						%>						
					</form>
					
					<hr>
	<%
				else
				
				
				
				end if
				
				if NotImportComplete then	
%>
								
					<h4><%=lne("label_import_how_to")%></h4>
					<p><small><%=lne("label_import_how_to_explained")%></small></p>				
					<div class=well>
						<small>E9023R/6, 1</small><br>
						<small>EU3060BU/3, 10</small><br>
					</div>				
					
				
<%
				end if
%>				
				
			</div>			
					
			
		 </div>

</form>
		 
	</div>
	
	
	<div class=spacer40></div>			
			

<!--#include virtual="/assets/includes/footer.asp" -->


<script>
	$(".CheckoutQty").change(function () {
		//alert("CHANGED");
		$("#CheckoutContinue").attr("disabled", true);
		$('#ChangedError').show(100, function() {
			// Animation complete.
		  });		
		
	});

	$("#btnImport").click(function () {
		//alert("changed!")
		$("#FormAction").val("IMPORT_SUBMIT")
		
	})	
	
	$("#ImportArea").change(function () {
		//alert("changed!")
		$("#btnImport").attr("disabled", true)
		$("#btnImport").attr("title", "Import values have been updated, please re-validate")
		
		$('#importDisabledMessage').removeClass('hidden')			
	})

	$('#ImportFile').change(function(){
		 $('#subfile').val($(this).val());
		 //alert('submitting...');
		// $('#CSVForm').submit();
		 //alert('ends')
	});

	
</script>

<script>

	
</script>

<%

dbClose()

%>
