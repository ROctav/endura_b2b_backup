<%
PageTitle = "Import order "
%>

<!--#include virtual="/assets/includes/header.asp" -->

<style>
	.HomePanelLayer {
		color:white;
	}
	.header-panel {		
		color:white;
		
	}
	
	A.whitelink {
		color:white;
	}
	
	.header-title {		
		color:white;
		font-size:36pt;
		font-family:Oxygen, arial;
		text-align:left;
	}	
	
	.header-trans {
		background-image:url(/assets/images/home_panels/semi-trans.png);
		padding:20px 0 20px 0;
		xmargin-top:20px;
		
	}
	
	.header-button {
		text-align:left;
	}
	
	#slide1, #slide2, #slide3, #slide4 {
		border:0px solid red;
		padding-top:50px;
		height:200px;
	}
	
	#feature_slider {
		height:250px !important;
	}
	
	.slide {
		height:220px;
	}
	
	.header-title {
		line-height:30pt;font-size:24pt;
	}
	
	.colSpacer_LEFT {
		border-left:1px solid #f0f0f0;
		padding-left:20px;
	}
	
	.ProductGrid TD {
		font-family:arial;
		font-size:10pt;
		padding:5px;
	}
	
	.legible {
		font-family:arial;
		font-size:8pt;
	}
	
	.basketAddButton {
		border:1px solid red;
	}
	
	#CheckoutTable {
		border:1px solid silver;
	}
	
	#CheckoutTable TD {
		font-family:arial;
		font-size:9pt;
		padding:7px;
	}
	
	.CheckoutImage {
		width:50px;
	}
	
	.CheckoutQty {
		width:40px;
		text-align:right;
	}
	
</style>

<%

FormAction = request("FormAction")
ImportArea = request("ImportArea")
ImportOptions = request("ImportOptions")

ImportMax = 500
DelimChar = "~"


if FormAction = "IMPORT_CHECK" or FormAction = "IMPORT_SUBMIT" then
	'jh "ImportArea = " & ImportArea
	
	sql = "exec [spB2BImport] '" & AcDetails.CostBand & "' "
	
	jhAdmin sql
	
	x = getrs(sql, CheckArr, CheckC)
	
		for y = 0 to  500				
			os = os & CheckArr(10,y) & ", " & 3& vbcrlf
		next
			
	
	'jh "CheckC = " & CheckC
	
	ProcessList = replace(ImportArea, chr(10), "|")
	
	ProcessList = replace(ProcessList, chr(9), DelimChar)
	ProcessList = replace(ProcessList, ",", DelimChar)
	ProcessList = replace(ProcessList, ";", DelimChar)
	
	'jh "ProcessList = " & ProcessList
	
	ProcessArr = split(ProcessList, "|")
	
	ProcessC = ubound(ProcessArr)
	
	dim its
	its = 0
	
	ErrorCount = 0
	LineCount = 0
	ErrorOut = ""
	
	for c = 0 to ProcessC
		ProcessLine = ProcessArr(c)
		LineCount = LineCount + 1
		
		if ProcessLine <> "" then
			'jh "ProcessLine = " & ProcessLine
			LineArr = split(ProcessLine, DelimChar)
			
			
			if ubound(LineArr) >= 1 then
				'jh "VALID LINE"
				ProcessSKU = ucase(trim(LineArr(0)))
				ProcessQty = cleannum(LineArr(1))
				
				if ProcessSKU <> "" and ProcessQty <> "" then
				
					SKUFound = false
					
					for y = 0 to CheckC
						its = its + 1

						CompSKU = CheckArr(10,y)
						if CompSKU = ProcessSKU then
							'jhInfo "ProcessSKU = " & ProcessSKU & " (" & ProcessQty & ")"
							SKUFound = true
							
							ThisPrice = cleannum(CheckArr(13,y))
							if ThisPrice = "" then ThisPrice = 0 else ThisPrice = cdbl(ThisPrice)
							'jh "ThisPrice = " & ThisPrice
							OrderValueTotal = OrderValueTotal + (ThisPrice * ProcessQty)
							
							ThisMAIQty = convertnum(CheckArr(14,y))
							ThisStatusName = CheckArr(15,y)
							B2BCanPurchase = "" & CheckArr(16,y)
							
							SKUAllowed = true
							ErrorReason = ""
							
							if B2BCanPurchase = "0" then
								SKUAllowed = false
								ErrorReason = "NOT CURRENTLY FOR SALE"
							elseif ThisStatusName = "CLEARANCE" and ThisMAIQty = 0 then
								SKUAllowed = false
								ErrorReason = "CLEARANCE/SOLD OUT"
							end if
							
							exit for
						end if
					next	
					
					if SKUFound and SKUAllowed then
						
						ValidCount = ValidCount + 1
						UnitTotal = UnitTotal + cdbl(ProcessQty)
						
						redim preserve validArr(2, ValidCount)
						validArr(0, ValidCount) = ProcessSKU
						validArr(1, ValidCount) = ProcessQty
												
					else
						ErrorCount = ErrorCount + 1
						ErrorOut = ErrorOut & "<div>" & lne_REPLACE("label_import_sku_error", LineCount) & " (" & ProcessSKU & ")"
						if ErrorReason <> "" then
							ErrorOut = ErrorOut & "<br><small> - " & ErrorReason & "</small>"
						end if
						ErrorOut = ErrorOut & "</div>"
					end if
					
				elseif ProcessQty = "" and LineArr(1) <> "" then
					ErrorCount = ErrorCount + 1
					ErrorOut = ErrorOut & "<div>" & lne_REPLACE("label_import_line_error", LineCount) & "</div>"
				end if
			elseif ubound(LineArr) = 0 then
				ErrorCount = ErrorCount + 1
				ErrorOut = ErrorOut & "<div>" & lne_REPLACE("label_import_line_error", LineCount) & "</div>"
			end if
		end if
	next
	
	if ValidCount = 0 then ErrorOut = lne("label_import_no_items")
	if ErrorOut <> "" then ErrorsFound = true
	
	if not ErrorsFound and FormAction = "IMPORT_SUBMIT" then
	
		'jhInfo "IMPORTING!"
		
		if ImportOptions = "CLEAR" then
				sql = "DELETE FROM B2B_Basket WHERE B2BOrderID = 0 AND AcCode = '" & AcDetails.AcCode & "'"
				'jhAdmin sql
				db.execute(sql)	
		end if
		
		ImportCount = 0
		
		for c = 1 to ValidCount
			'jhInfo "ValidArr = " & ValidArr(0,c) & " - " & ValidArr(1,c)
			
			ImportSKU = ValidArr(0,c)
			ImportQty = cleannum(ValidArr(1,c))
			
			if ImportSKU <> "" and ImportQty <> "" then
				if cdbl(ImportQty) < 0 then ImportQty = 0
				
				sql = "exec [spB2BBasketAdd] '" & validstr(ImportSKU) & "', 0" & ImportQty & ", '" & AcDetails.AcCode & "', ''"				
				'jhAdmin sql
				db.execute(sql)				
				ImportCount = ImportCount + 1
			end if

		next
		
		ImportComplete = true
		ImportArea = ""
	end if
end if





%>



<link rel="stylesheet" href="css/index.css" type="text/css" media="screen" />

<style>
	#ImportArea {
		width:200px;
		height:250px;
	}
</style>

	<div class="container">      

		  <div class="row">      
			<div class="span12 spacer10">
			
				
	
			
				<h2><%=lne("import_page_title")%><br><small><%=lne("import_page_title_explained")%></small></h2>

			
				<%
				'jhInfoNice "Login notification", "You are logged in as [AccountCode] [+ Admin user name if applicable]"
				%>
				
				<%
				=announceStr
				%>
			
				
				<hr>
			</div>
		</div>	
		
<form name=formImport id=formImport method=post action="/utilities/import/">
		 
		 <div class=row>
			<div class="span3">	
				<h4><%=lne("import_input_area_title")%></h4>
			
				<p><small><%=lne("import_input_area_title_explained")%></small></p>
			
				
				<textarea style="width:100%;" placeholder="[Paste your order items here]" class=legible id=ImportArea name=ImportArea><%=ImportArea%></textarea>
				
				<p><small><%=lne_REPLACE("label_import_max_explained", ImportMax)%></small></p>
				
			</div>
			<div class="span5 offset1">	
			
<%
if ErrorsFound then
	jhErrorNice lne("label_errors_found"), ErrorOut
elseif ImportComplete then
%>
							
		<div class="alert alert-success">
			
			<h4 class=spacer20>Import complete!</h4>
			<div class=spacer5>All items (<%=ImportCount%>) have been added to your basket!</div>
			<p><a class=OrangeLink href="/checkout/">Click here to go to the checkout</a></p>
			
		</div>
<%
elseif ValidCount > 0 then
	'if ImportOptions = "" then ImportOptions = "CLEAR"
%>
							
		<div class="alert alert-success">
			
			<h4 class=spacer20><%=lne("label_import_check_complete")%></h4>
			<div class=spacer5><%=lne("label_import_check_complete_explained")%>:</div>
			<div class=spacer20><%=lne("label_lines")%>: <strong><%=LineCount%></strong>, # <%=lne("label_units")%>: <strong><%=UnitTotal%></strong>, <%=lne("label_value")%>: <strong><%=dispCurrency(OrderValueTotal)%></strong></div>
			

			<div class=spacer20></div>	
		
			<p><input <%if ImportOptions = "CLEAR" then response.write "CHECKED"%> value="CLEAR" name="ImportOptions" type=checkbox> <%=lne("label_import_check_clear_existing")%></p>			
						
			
			<button id=btnImport class="btn btn-large btn-success"><%=lne("btn_import_now")%></button>

		</div>
<%
	
end if
%>			
			
				<h4><%=lne("label_import_check_contents")%></h4>
				<p><%=lne("import_check_explained")%></p>
				<button name=btnCheck name=btnCheck class="btn btn-large btn-success"><%=lne("btn_import_check_now")%></button>
				<div class=spacer5></div>	
				<p><small><%=lne("import_check_validate_explained")%></small></p>
				
				<hr>
								
				<div class=spacer20></div>		
				
				
				<input name="FormAction" id="FormAction" type=hidden class=FormHidden value="IMPORT_CHECK">
				
				
			</div>			
			
			<div class="span2 offset1">	

				<h4><%=lne("label_import_how_to")%></h4>
				<p><small><%=lne("label_import_how_to_explained")%></small></p>				
				<div class=well>
					<small>E9023R/6, 1</small><br>
					<small>EU3060BU/3, 10</small><br>
				</div>
						
				
			</div>			
			
		 </div>

</form>
		 
	</div>
	
	
	<div class=spacer40></div>			
			

<!--#include virtual="/assets/includes/footer.asp" -->


<script>
	$(".CheckoutQty").change(function () {
		//alert("CHANGED");
		$("#CheckoutContinue").attr("disabled", true);
		$('#ChangedError').show(100, function() {
			// Animation complete.
		  });		
		
	});

	
	$("#btnImport").click(function () {
		//alert("changed!")
		$("#FormAction").val("IMPORT_SUBMIT")
		
	})	
	
	$("#ImportArea").change(function () {
		//alert("changed!")
		$("#btnImport").attr("disabled", true)
		$("#btnImport").attr("title", "Import values have been updated, please re-validate")
	})
	
</script>

<%

dbClose()

%>

<textarea>
<%
=os
%>
</textarea>