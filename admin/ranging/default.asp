<%
PageTitle = "Ranging proposals"

%>

<!--#include virtual="/assets/includes/header.asp" -->

<%

'jhInfo "HERE!"

DelID = cleannum(request("DelID"))

if DelID <> "" then

	sql = "DELETE FROM B2B_Ranging WHERE RangingID = 0" & DelID
	jhAdmin sql
	db.execute(sql)
	sql = "DELETE FROM B2B_RangingProducts WHERE RangingID = 0" & DelID
	jhAdmin sql
	db.execute(sql)
	sql = "DELETE FROM B2B_RangingColorways WHERE RangingID = 0" & DelID
	jhAdmin sql
	db.execute(sql)
	sql = "DELETE FROM B2B_RangingSKUS WHERE RangingID = 0" & DelID
	jhAdmin sql
	db.execute(sql)

	jhErrorNice "", "Ranging proposal has been deleted!" 

end if

sql = "SELECT fow.ForwardOrderWindowID, fow.ForwardOrderWindowName, fow.ForwardOrderWindowStart, fow.ForwardOrderWindowEnd, r.RangingCount "
sql = sql & "FROM END_ForwardOrderWindow fow "
sql = sql & "LEFT JOIN (SELECT ForwardOrderWindowID, COUNT(*) AS RangingCount FROM B2B_Ranging WHERE RangingStatusID <> 99 AND RangedBy = 0" & AdminDetails.AdminID & " GROUP BY ForwardOrderWindowID) r ON r.ForwardOrderWindowID = fow.ForwardOrderWindowID " 
sql = sql & "WHERE fow.ForwardOrderWindowEnd >= '" & sqldate(DateAdd("d",-90,now)) & "' "
sql = sql & "AND fow.ForwardOrderWindowStart <= '" & sqldate(DateAdd("yyyy",2,now)) & "' "

sql = sql & "ORDER BY fow.ForwardOrderWindowStart "

jhAdmin sql

x = getrs(sql, WindowArr, WindowC)

SelWindowID = convertnum(request("SelWindowID"))

if SelWindowID = 0 then
	for c = 0 to WindowC
		ThisRangingCount = WindowArr(4,c)
		if ThisRangingCount > 0 then
			SelWindowID = WindowArr(0,c)
			SelWindowName = WindowArr(1,c)
		end if
	next		
end if

for c = 0 to WindowC
	if SelWindowID = WindowArr(0,c) then
		SelWindowName = WindowArr(1,c)
	end if
next	

UseFOPrices = 0

if glRangingUseFOPrices then UseFOPrices = 1 end if

sql = "SELECT r.RangingID, r.AcCode, ecd.CompanyName, r.RangedBy, AdminName, RangingStatusID, RangingTitle, RangingDescription, AllowDealerChanges, ecd.DepartmentCode, ecd.CostCentre, StyleCount, RangingGUID, AllowDealerView, AllowDealerChanges, AllowDealerPrices, AllowDealerBudget, RangingStatusName, cso.OrderRef, ests.SOROurRef, SharedTemplate "
sql = sql & ",( SELECT dbo.fnB2BRangingTotalValueForProposal(r.rangingid, " & UseFOPrices & ", '') ) AS totalvalue "
sql = sql & "FROM B2B_Ranging r "
sql = sql & "INNER JOIN ExchequerCustDetails ecd ON ecd.AcCode = r.AcCode "
sql = sql & "INNER JOIN END_Adminusers u ON u.AdminID = r.RangedBy "
sql = sql & "INNER JOIN (SELECT SMLStatusID, SMLStatusName AS RangingStatusName FROM SML_Status WHERE SMLTypeID = 376) sta ON sta.SMLStatusID = r.RangingStatusID "
sql = sql & "LEFT JOIN (SELECT RangingID, COUNT(*) AS StyleCount FROM B2B_RangingProducts GROUP BY RangingID) styles ON styles.rangingid = r.rangingid "
sql = sql & "LEFT JOIN CS_Orders cso ON cso.CSOrderID = r.CSOrderID "
sql = sql & "LEFT JOIN ExchequerSORTimeStamps ests ON ests.XMLFileName = cso.XMLFileName "

sql = sql & "WHERE RangedBy =  0" & AdminDetails.AdminID & " "
if SelWindowID > 0 then
	sql = sql & "AND r.ForwardOrderWindowID = 0" & SelWindowID & " "
end if
sql = sql & "ORDER BY RangingStatusID, DateAdded DESC "
	
jhAdmin sql

x = getrs(sql, RangingArr, RangingC)

jhAdmin "RangingC = " & RangingC

'jhstop

set ls = new strconcatstream

ls.add "<table border=1 class=""ReportTable legible"">"

	ls.add "<tr id=""RepeatLine_" & c & """ class=""RepeatLine"">"
	ls.add "<th colspan=7></th>"									
	ls.add "<th>Proposal name</th>"									
	ls.add "<th>Account</th>"																			
	ls.add "<th>By</th>"
	ls.add "<th>Styles</th>"	
	ls.add "<th>Status</th>"
    ls.add "<th>Total Value</th>"
	ls.add "<th>Refs</th>"	
	ls.add "</tr>"

for c = 0 to RangingC

	ThisRangingID = RangingArr(0,c)
	ThisRangingName = RangingArr(6,c)
	
	ThisAcCode = RangingArr(1,c)
	ThisCompanyName = RangingArr(2,c)
		
	ThisRangedBy = RangingArr(4,c)	
	
	ThisStyleCount = RangingArr(11,c)	
	ThisGUID = RangingArr(12,c)	
	ThisAllowDealerView = RangingArr(13,c)	
	
	ThisRangingStatus = RangingArr(17,c)	
	ThisTotalValue = RangingArr(21,c)
	CSOrderRef = "" & RangingArr(18,c)	
	SORNumber =  "" & RangingArr(19,c)	
	ExchequerStr = ""
	
	SharedTemplate = "" & RangingArr(20,c)	
	if SharedTemplate = "1" then DispShared = "<span class=""label label-success"">SHARED</span>&nbsp;" else DispShared = ""
	
	if CSOrderRef <> "" then
		ExchequerStr = ExchequerStr & "<div class=spacer5><span class=""label label-default"">" & CSOrderRef & "</span></div>"
	end if
	if SORNumber <> "" then
		ExchequerStr = ExchequerStr & "<div class=spacer5><span class=""label label-info"">" & SORNumber & "</span></div>"
	end if	

	ls.add "<tr id=""RepeatLine_" & c & """ class=""RepeatLine"">"
		
	ls.add "<td><a title=""View ranging proposal"" href=""/admin/control/?impCode=" & ThisAcCode & "&RangingID=" & ThisRangingID & """><i class=""pointer icon-search""></i></a></td>"
	ls.add "<td><a title=""View proposal summary report"" href=""/admin/control/?impCode=" & ThisAcCode & "&RangingID=" & ThisRangingID & "&ViewReport=1""><i class=""pointer icon-file""></i></a>"
	ls.add "<td><a title=""View entire product form"" href=""/admin/control/?impCode=" & ThisAcCode & "&RangingID=" & ThisRangingID & "&ViewList=1""><i class=""pointer icon-list""></i></a>"
	
	ls.add "<td><a title=""Edit proposal details"" href=""/admin/ranging/ranging-detail/?RangingID=" & ThisRangingID & """><i class=""pointer icon-cog""></i></a>"
	
	ls.add "<td><a title=""Duplicate this ranging proposal"" href=""/admin/ranging/ranging-detail/?DuplicateID=" & ThisRangingID & """><i class=""pointer icon-plus""></i></a>"
	ls.add "<td><a title=""Delete"" onclick=""if (!confirm('Delete - Are you sure?')) return false;"" href=""/admin/ranging/?DelID=" & ThisRangingID & """><i class=""pointer icon-trash""></i></a></td>"
	ls.add "<td>"
	if ThisAllowDealerView = 1 then
		ls.add "<a class=""label label-warning"" title=""Link for dealer to view ranging proposal"" href=""/utilities/ranging/?RangingGUID=" & ThisGUID & "&RangingID=" & ThisRangingID & """>link</a>"
	else
		ls.add "<span title=""Set proposal 'visible to dealer' using the edit proposal function (cog icon) first"" class=""label label-default"">link</span>"
	end if
	ls.add "</td>"	
	
	
	ls.add "<td><strong>"
	ls.add ThisRangingName
	ls.add "</strong></td>"	
	ls.add "<td>"
	ls.add ThisAcCode
	ls.add "<div><small>"
	ls.add ThisCompanyName
	ls.add "</small></div>"		
	ls.add "</td>"		
	ls.add "<td>"
	ls.add ThisRangedBy
	ls.add "</td>"		
	ls.add "<td class=numeric>"
	ls.add ThisStyleCount
	ls.add "</td>"				
	ls.add "<td><strong>"
	ls.add ThisRangingStatus
	ls.add "</strong></td>"	
    ls.add "<td class='numeric'><span class='label label-success'>"
    ls.add  dispCurrency(ThisTotalValue)
    ls.add "</span></td>"
	ls.add "<td>" & DispShared & ExchequerStr & "</td>"		
	
	ls.add "</tr>"

next					
					
ls.add "</table>"

RepeatOS = ls.value

%>

<style>
	.ReportTable TD {
		padding:5px;
	}
	
	.RepeatLineSelected{
		background-color:<%=glDarkColor%>;
	}
	
</style>

<link rel="stylesheet" href="css/index.css" type="text/css" media="screen" />

	<div class="container">      

		<div class="row">      
			
		
			<div class="span8 spacer10">
		
					<h3 class=spacer20><%=PageTitle%></h3>
					
					
<%
if 1 = 1 then
%>
<div class=pull-right>
	<div class="btn-group">
	  <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
		Season: <strong><%=SelWindowName%></strong>
		<span class="caret"></span>
	  </a>
	  <ul class="dropdown-menu">
		
		<%
		
		for c = 0 to WindowC
			ThisWindowID = WindowArr(0,c)
			ThisWindowName = WindowArr(1,c)
			ThisCount = WindowArr(4,c)
			if ThisCount > 0 then CountStr = "&nbsp;<span class=""badge badge-success"">" & ThisCount & "</span>" else CountStr = ""
			if "" & ThisWindowID = "" & SelWindowID then SelStr = "<i class=""icon icon-ok""></i>&nbsp;" else SelStr = ""
		%>
			<li><a href="?SelWindowID=<%=ThisWindowID%>"><%=SelStr%><%=ThisWindowName%> <%=CountStr%></a></li>
		<%	
		next
		%>
	  </ul>
	</div>
</div>
<%
end if
%>					
					
					<a class=OrangeLink href="/admin/ranging/ranging-detail/">Create a new ranging proposal</a> | <a class=OrangeLink href="/admin/ranging/usage-report/">Activity report</a>
					<hr>
					
					<%
					=RepeatOS
					%>
					
			</div>	

<%

%>
			
			<div class="span4 spacer10">
				
				
							
			</div>
		</div>		
	</div>		

<script>
	$(document).ready(function() {
		
		//select and deselect
		$("#btnCheckAll").click(function () {
			$('.RemoveCheck').prop('checked', true);
			//alert('clicked')
			$('.QuickSearchSKUQty').val('0');
			$('.QuickSearchSKUQty').prop('disabled', true);
			
		});
		
		$("#FilterDate").change( function() {
			//alert(	$( this ).val() )
			window.location.assign("/admin/recent/?FilterDate=" + $( this ).val())
		});

			
	});	
</script>	
	
<script>

	function getRepeatDetails(pRef, pRow) {
		//alert('in')
		//$('#ajax_prod_results').fadeout('fast')
		
		$("#RepeatAjaxDiv").html('<span class="AjaxLoader"><img src="/assets/images/icons/indicator.gif"> loading...</span>');
		
		var ThisID = pRef
		
		//alert(pRef)
		
		$.ajax({
			//contentType: "application/x-www-form-urlencoded;charset=ISO-8859-1",
			//dataType: 'json',
			scriptCharset: "ISO-8859-1" ,
			contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
			url: "/admin/admin_server.asp?CallType=RECENT_ORDERS&B2BOrderID=" + ThisID,
			cache: false,			
			}).done(
				function( html ) {
				$("#RepeatAjaxDiv").html(html);
				$('#RepeatAjaxDiv').show(100, function() {
					// Animation complete.
					$('.RepeatLine').removeClass('RepeatLineSelected');
					$('#RepeatLine_' + pRow).addClass('RepeatLineSelected');
					
					
				  });					
			});
		
	}

</script>	
	
<!--#include virtual="/assets/includes/footer.asp" -->

