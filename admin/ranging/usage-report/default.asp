<%
PageTitle = "Ranging proposals - Activity Report"

%>

<!--#include virtual="/assets/includes/header.asp" -->

<%

SortOption = request("SortOption")
if SortOption = "" then SortOption = "UPDATED"
	
sql = "SELECT r.RangingID, r.AcCode, ecd.CompanyName, r.RangedBy, AdminName, RangingStatusID, RangingTitle, RangingStatusName "
sql = sql & ", DealerLastViewed, DealerLastUpdated, RangingGUID "
sql = sql & "FROM B2B_Ranging r "	
sql = sql & "INNER JOIN ExchequerCustDetails ecd ON ecd.AcCode = r.AcCode "
sql = sql & "INNER JOIN END_Adminusers u ON u.AdminID = r.RangedBy "
sql = sql & "INNER JOIN (SELECT SMLStatusID, SMLStatusName AS RangingStatusName FROM SML_Status WHERE SMLTypeID = 376) sta ON sta.SMLStatusID = r.RangingStatusID "

sql = sql & "WHERE RangedBy =  0" & AdminDetails.AdminID & " AND RangingStatusID <> 10 "

if SortOption = "UPDATED" then
	sql = sql & "ORDER BY r.DealerLastUpdated DESC, r.DealerLastViewed DESC, r.RangingID "
else
	sql = sql & "ORDER BY r.DealerLastViewed DESC, r.DealerLastUpdated DESC, r.RangingID "
end if
	
'jhAdmin sql

x = getrs(sql, RangingArr, RangingC)
'jhAdmin "RangingC = " & RangingC

sql = "SELECT n.RangingID, n.AdminNotes, n.DealerNotes, n.AdminUpdatedDate, n.DealerUpdatedDate, n.ProductID, sp.ProductCode , sp.ProductName "
sql = sql & "FROM B2B_Ranging r "
sql = sql & "INNER JOIN B2B_RangingNotes n ON r.RangingID = n.RangingID "
sql = sql & "INNER JOIN SML_Products sp ON sp.ProductID = n.ProductID "
sql = sql & "INNER JOIN B2B_RangingProducts rp ON rp.RangingID = r.RangingID AND rp.ProductID = sp.ProductID "

sql = sql & "WHERE r.RangedBy = 0" & AdminDetails.AdminID & " "

sql = sql & "ORDER BY n.DealerUpdatedDate DESC, n.AdminUpdatedDate DESC "

'jhInfo sql
x = getrs(sql, notearr, notec)

'jh "notec = " & notec

sql = "SELECT r.RangingID, sp.ProductID, sp.ProductCode, spc.VariantCode, rc.ColorwayQty FROM b2b_rangingcolorways rc "
sql = sql & "INNER JOIN B2B_Ranging r ON r.RangingID = rc.RangingID "
sql = sql & "INNER JOIN SML_ProductColorways spc ON spc.ProductColorwayID = rc.ProductColorwayID "
sql = sql & "INNER JOIN SML_Products sp ON sp.ProductID = spc.ProductID "
sql = sql & "WHERE r.RangedBy = 0" & AdminDetails.AdminID & " "
sql = sql & "ORDER BY DefaultProductImage, VariantCode "

'j'hInfo sql
x = getrs(sql, colorarr, colorc)
'jh "colorc = " & colorc

'jhstop

set ls = new strconcatstream
set nls = new strconcatstream

ls.add "<table border=1 class=""ReportTable legible"">"

	ls.add "<tr id=""RepeatLine_" & c & """ class=""RepeatLine"">"
	'ls.add "<th colspan=7></th>"									
	ls.add "<th>Proposal name</th>"									
	ls.add "<th>Account</th>"																			
	'ls.add "<th>By</th>"
	ls.add "<th>Status</th>"
	ls.add "<th>Last viewed</th>"	
	ls.add "<th>Last updated</th>"	
    ls.add "<th>Notes</th>"
	ls.add "</tr>"

for c = 0 to RangingC

	ThisRangingID = convertnum(RangingArr(0,c))
	ThisRangingName = RangingArr(6,c)
	
	ThisAcCode = RangingArr(1,c)
	ThisCompanyName = RangingArr(2,c)
		
	ThisRangedBy = RangingArr(4,c)	
	
	'ThisStyleCount = RangingArr(11,c)	
	ThisGUID = RangingArr(10,c)	
	'ThisAllowDealerView = RangingArr(13,c)	
	
	ThisRangingStatusID = RangingArr(5,c)	

	ThisRangingStatus = RangingArr(7,c)	
	ThisLastViewed = RangingArr(8,c)	
	ThisLastUpdated = RangingArr(9,c)	
	
	if isdate(ThisLastViewed) then ThisLastViewed = formatdatetime(ThisLastViewed, 2)
	if isdate(ThisLastUpdated) then ThisLastUpdated = formatdatetime(ThisLastUpdated, 2)

	ls.add "<tr id=""RepeatLine_" & c & """ class=""RepeatLine"">"
	
	ls.add "<td><strong>"
	ls.add "<a class=OrangeLink href=""/utilities/ranging/?RangingID=" & ThisRangingID & """>" & ThisRangingName & "</a>"
	ls.add "<a class="""" href=""/admin/ranging/ranging-detail/?RangingID=" & ThisRangingID & """><i class=""icon icon-cog""></i></a>"
	ls.add "</strong></td>"	
	ls.add "<td>"
	ls.add ThisAcCode
	ls.add "<div><small>"
	ls.add ThisCompanyName
	ls.add "</small></div>"		
	ls.add "</td>"		
	ls.add "<td><strong>"
	ls.add ThisRangingStatus
	ls.add "</strong></td>"		
	ls.add "<td style=""" & ViewedStyle & """>"
	ls.add ThisLastViewed
	ls.add "</td>"		
	ls.add "<td style=""" & UpdatedStyle & """>"
	ls.add ThisLastUpdated
	ls.add "</td>"				

	NoteList = ""
	nls.clear
	NotesFound = false
	
	for y = 0 to NoteC
		
		if convertnum(NoteArr(0,y)) = ThisRangingID then
			NoteStr = "NOTES!"
			
			ThisProductCode = NoteArr(6,y)
			imgurl = ""
			
			for z = 0 to colorc
				'jh ThisProductCode
				if colorarr(2,z) = ThisProductCode then
					ThisVariant = colorarr(3,z)
					imgurl = getImageDefault(ThisProductCode, ThisVariant)
					'jh "imgurl = " & imgurl
					exit for
				end if
			next
			
			ThisProductName = NoteArr(7,y)
			AdminNotes = trim("" & NoteArr(1,y))
			DealerNotes = trim("" & NoteArr(2,y))
			
			AdminNotesUpdated = NoteArr(3,y)
			DealerNotesUpdated = NoteArr(4,y)
			
			if isdate(AdminNotesUpdated) then AdminNotesUpdated = formatdatetime(AdminNotesUpdated,2) 
			if isdate(DealerNotesUpdated) then DealerNotesUpdated = formatdatetime(DealerNotesUpdated) 
			
			if AdminNotes <> "" or DealerNotes <> "" then
			
				
				if DealerNotes <> "" then DealerNotes = "<div><span class=""label label-warning""><small>DEALER COMMENTS</small> " & DealerNotesUpdated & "</span></div>" & DealerNotes
				if AdminNotes <> "" then AdminNotes = "<div><span class=""label label-success""><small>ACM COMMENTS</small> " & AdminNotesUpdated & "</span></div>" & AdminNotes
			
				NotesFound = true
				nls.add "<tr>"
				nls.add "<td width=150 class=text-center valign=top>"
				if imgurl<> "" then
					nls.add "<img src=""" & imgurl & """ class=""img img-responsive ProdThumb60""> "
				end if
				nls.add "<div><small><strong>" & ThisProductCode & "</strong></small></div>"
				nls.add "" & ThisProductName & ""
				nls.add "</td>"

				nls.add "<td style=""background:lightyellow;"" valign=top width=300>"
				nls.add DealerNotes
				nls.add "</td>"					
				nls.add "<td style=""background:lightgreen;"" valign=top width=300>"
				nls.add AdminNotes
				nls.add "</td>"
	
				nls.add "<tr>"
			end if
			
			
			
		end if
		
	next	
	
    ls.add "<td class=''>"
	if NotesFound then
		ls.add "<a data-c=""" & c & """ class=""pointer NoteLink label label-info""><i class=""icon icon-white icon-comment""></i> NOTES <i class=""icon icon-white icon-chevron-down""></i></a>"
	end if
    ls.add "</td>"
	
	ls.add "</tr>"

	if NotesFound then
		NoteOS = "<table width=100% border=1 class=""legible"">" & nls.value & "</table>"
		
		ls.add "<tr style=""display:none;"" id=""Notes_" & c & """>"
		ls.add "<td colspan=6>"
		ls.add NoteOS
		ls.add "</td>"
		ls.add "</tr>"
	end if
	
next					
					
ls.add "</table>"

RepeatOS = ls.value

%>

<style>
	.ReportTable TD {
		padding:5px;
	}
	
</style>

<link rel="stylesheet" href="css/index.css" type="text/css" media="screen" />

	<div class="container">      

		<div class="row">      
			
		
			<div class="span12 spacer10">
		
					<a class=OrangeLink href="/admin/ranging/">Back to ranging admin</a>
		
					<h3 class=spacer20><%=PageTitle%></h3>
				
					<p>Sort by: <a class=OrangeLink href="?SortOption=UPDATED">Date UPDATED</a> | <a class=OrangeLink href="?SortOption=VIEWED">Date VIEWED</a>  
					
					<%
					=RepeatOS
					%>
					
			</div>	

<%

%>
			
		</div>
	</div>		

<script>
	$(document).ready(function() {
		
		//select and deselect
		$("#btnCheckAll").click(function () {
			$('.RemoveCheck').prop('checked', true);
			//alert('clicked')
			$('.QuickSearchSKUQty').val('0');
			$('.QuickSearchSKUQty').prop('disabled', true);
			
		});
		
		//select and deselect
		$(".NoteLink").click(function () {
			var c = $( this ).data("c")
			$("#Notes_" + c).toggle("fast")
		});		
		
		
		
		$("#FilterDate").change( function() {
			//alert(	$( this ).val() )
			window.location.assign("/admin/recent/?FilterDate=" + $( this ).val())
		});

			
	});	
</script>	
	
<script>

	function getRepeatDetails(pRef, pRow) {
		//alert('in')
		//$('#ajax_prod_results').fadeout('fast')
		
		$("#RepeatAjaxDiv").html('<span class="AjaxLoader"><img src="/assets/images/icons/indicator.gif"> loading...</span>');
		
		var ThisID = pRef
		
		//alert(pRef)
		
		$.ajax({
			//contentType: "application/x-www-form-urlencoded;charset=ISO-8859-1",
			//dataType: 'json',
			scriptCharset: "ISO-8859-1" ,
			contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
			url: "/admin/admin_server.asp?CallType=RECENT_ORDERS&B2BOrderID=" + ThisID,
			cache: false,			
			}).done(
				function( html ) {
				$("#RepeatAjaxDiv").html(html);
				$('#RepeatAjaxDiv').show(100, function() {
					// Animation complete.
					$('.RepeatLine').removeClass('RepeatLineSelected');
					$('#RepeatLine_' + pRow).addClass('RepeatLineSelected');
					
					
				  });					
			});
		
	}

</script>	
	
<!--#include virtual="/assets/includes/footer.asp" -->

