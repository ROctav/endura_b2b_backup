<%
PageTitle = "Recent orders"

%>

<!--#include virtual="/assets/includes/header.asp" -->

<%


FilterDate = request("FilterDate")
jhAdmin "FilterDate = " & FilterDate

sql = "SELECT TOP 500 o.OrderRef, o.OrderDate, o.DeliveryDate, o.CustomerRef, o.SpecialInstructions, c.AcCode, c.CompanyName "
sql = sql & ", NETTotal, VATTotal, DeliveryTotal, GrandTotal, mon.CurrencySymbol, o.B2BOrderID  "
sql = sql & "FROM B2B_Orders o  "
sql = sql & "INNER JOIN ExchequerCustDetails c ON c.AcCode = o.AcCode  "
sql = sql & "INNER JOIN ExchequerCurrency mon ON mon.CurrencyID = c.Currency  "

sql = sql & "WHERE 1 = 1 "
if 1 = 0 then
	sql = sql & "WHERE AccountCode = '" & AcDetails.AcCode & "' AND DueDate <= " & CheckDate & " "
end if

if isdate(FilterDate) then
	sql = sql & "AND o.OrderDate >= '" & formatdatetime(FilterDate,1) & "' "
	sql = sql & "AND o.OrderDate <= '" & formatdatetime(dateadd("d", 1, FilterDate),1) & "' "
end if

if adminDetails.isGroupMember("account_man") and adminDetails.DepartmentCode <> "" then
	sql = sql & "AND (c.DepartmentCode = '" & adminDetails.DepartmentCode & "') " 'ADDED JIMB01 to the list of possible matches for ACMs 		
elseif not adminDetails.isGroupMember("admin") and not adminDetails.isGroupMember("cust_serv_admin") and not adminDetails.isGroupMember("cust_serv") then
	sql = sql & "AND c.DepartmentCode = '" & adminDetails.DepartmentCode & "' "		
end if

sql = sql & "ORDER BY OrderDate DESC "

jhAdmin sql

x = getrs(sql, RecArr, rc)

'jhAdmin "rc = " & rc

set ls = new strconcatstream

ls.add "<table border=1 class=""ReportTable legible"">"

	ls.add "<tr id=""RepeatLine_" & c & """ class=""RepeatLine"">"
	ls.add "<th colspan=2>Ref</th>"									
	ls.add "<th>Account</th>"										
	ls.add "<th>Customer Ref</th>"										
	ls.add "<th>Date</th>"
	ls.add "<th>Special instructions</th>"
	ls.add "<th>VALUE</th>"
	ls.add "</tr>"

for c = 0 to rc
	ThisRef = RecArr(0,c)
	ThisDate = RecArr(1,c)
	
	ThisAcCode = RecArr(5,c)
	ThisCompanyName = RecArr(6,c)
	
	DispDate = formatdatetime(ThisDate,2)
	ThisYourRef = RecArr(2,c)
	
	ThisSpecial = RecArr(4,c)
	
	ThisNET = RecArr(7,c)
	ThisVAT = RecArr(8,c)
	ThisDelivery = RecArr(9,c)
	ThisGrand = RecArr(10,c)
	ThisSymbol = RecArr(11,c)
	ThisID = RecArr(12,c)

	ls.add "<tr id=""RepeatLine_" & c & """ class=""RepeatLine"">"
	ls.add "<td><i data-row-id=""" & c & """ onclick=""getRepeatDetails('" & ThisID & "', '" & c & "');"" class=""pointer icon-search""></i>"
	ls.add "<td><strong>"
	ls.add ThisRef
	ls.add "</strong></td>"	
	ls.add "<td>"
	ls.add ThisAcCode
	ls.add "<div><small>"
	ls.add ThisCompanyName
	ls.add "</small></div>"		
	ls.add "</td>"		
	ls.add "<td>"
	ls.add ThisYourRef
	ls.add "</td>"					
	ls.add "<td>"
	ls.add DispDate
	ls.add "</td>"		
	ls.add "<td>"
	ls.add ThisSpecial
	ls.add "</td>"			
	ls.add "<td class=numeric>"
	ls.add "<div><strong>"
	ls.add ThisSymbol & dispCurrency_PLAIN(ThisGrand)
	ls.add "</strong></div>"	
	ls.add "</td>"
	
	ls.add "</tr>"

next					
					
ls.add "</table>"

RepeatOS = ls.value

%>

<style>
	.ReportTable TD {
		padding:5px;
	}
	
	.RepeatLineSelected{
		background-color:<%=glDarkColor%>;
	}
	
</style>

<link rel="stylesheet" href="css/index.css" type="text/css" media="screen" />

	<div class="container">      

		<div class="row">      
			
		
			<div class="span8 spacer10">
		
					<h3 class=spacer20>Recent orders</h3>
					
					<div class=well>
						Select a date <%
						=DrawDate_JQ("FilterDate", FilterDate)
						%>
					</div>
					&nbsp;		
					
					<%
					=RepeatOS
					%>
					
			</div>	
			
			<div class="span4 spacer10">
				
					<div id="RepeatAjaxDiv">
						<div class=spacer20></div>
						<%
						if OrderRepeated then						
							jhInfoNice "Selected items added to basket!", "Your chosen items have been added to your basket - <a href=""/checkout/"">click here to go to the checkout</a>"
						elseif OrderRepeatError then
							jhErrorNice "No items selected", "You did not select any valid quantitys - please re-enter"
						end if
						%>
						
					</div>
							
			</div>
		</div>		
	</div>		

<script>
	$(document).ready(function() {
		
		//select and deselect
		$("#btnCheckAll").click(function () {
			$('.RemoveCheck').prop('checked', true);
			//alert('clicked')
			$('.QuickSearchSKUQty').val('0');
			$('.QuickSearchSKUQty').prop('disabled', true);
			
		});
		
		$("#FilterDate").change( function() {
			//alert(	$( this ).val() )
			window.location.assign("/admin/recent/?FilterDate=" + $( this ).val())
		});

			
	});	
</script>	
	
<script>

	function getRepeatDetails(pRef, pRow) {
		//alert('in')
		//$('#ajax_prod_results').fadeout('fast')
		
		$("#RepeatAjaxDiv").html('<span class="AjaxLoader"><img src="/assets/images/icons/indicator.gif"> loading...</span>');
		
		var ThisID = pRef
		
		//alert(pRef)
		
		$.ajax({
			//contentType: "application/x-www-form-urlencoded;charset=ISO-8859-1",
			//dataType: 'json',
			scriptCharset: "ISO-8859-1" ,
			contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
			url: "/admin/admin_server.asp?CallType=RECENT_ORDERS&B2BOrderID=" + ThisID,
			cache: false,			
			}).done(
				function( html ) {
				$("#RepeatAjaxDiv").html(html);
				$('#RepeatAjaxDiv').show(100, function() {
					// Animation complete.
					$('.RepeatLine').removeClass('RepeatLineSelected');
					$('#RepeatLine_' + pRow).addClass('RepeatLineSelected');
					
					
				  });					
			});
		
	}

</script>	
	
<!--#include virtual="/assets/includes/footer.asp" -->

