<%
PageTitle = "Admin functions"
%>

<!--#include virtual="/assets/includes/header.asp" -->

<%
'jh "START HERE!"

CurrentAcCode = AcDetails.AcCode
CurrentDepartmentCode = AcDetails.DepartmentCode

if CurrentAcCode = "" then CurrentAcCode = "none..."

SearchVal = request.cookies("ADMIN")("ImpersonateSearchVal")

if isAdminUser() then
	
	ImpCode = request("ImpCode")
	
	if ImpCode <> "" then
		dbConnect()
		
			strAgentList = ""

			sql = "select ISNULL(AgentDepartments, '') AS AgentDepartments FROM  end_adminusers where AdminID = " & adminDetails.AdminID
			set rs = db.execute(sql)
			if not rs.eof then
				strAgentList = rs("AgentDepartments").value
			End If	
			rs.Close

		
		sql = "exec spB2BAccountDetails '" & validstr(ImpCode) & "'"
		set rs = db.execute(sql)
		if not rs.eof then
		
			
		AllowImpersonate = false
		
			
	if  adminDetails.isGroupMember("admin") or adminDetails.isGroupMember("cust_serv_admin") or adminDetails.isGroupMember("cust_serv") or adminDetails.isGroupMember("returns") then
		AllowImpersonate = true	
	elseif adminDetails.isGroupMember("country_man") then
				if adminDetails.DepartmentCode <> rs("DepartmentCode") AND InStr(strAgentList,rs("DepartmentCode")) = 0 then
					jhErrorNice "Error", "Not a valid account for  account manager - " & ImpCode
				else
					AllowImpersonate = true
				end if
	elseif (adminDetails.isGroupMember("account_man") or adminDetails.isGroupMember("distrib") ) and adminDetails.DepartmentCode <> "" then
				if adminDetails.DepartmentCode <> rs("DepartmentCode") then
					jhErrorNice "Error", "Not a valid account for  account manager - " & ImpCode
				else
					AllowImpersonate = true
				end if
	end if

			if "" & rs("AcStatus") = "CLOSED" then
				AllowImpersonate = false	
				jhErrorNice "Error", "Closed account - " & ImpCode	
			end if
	
			if AllowImpersonate then 
				session("B2B_AcCode") = ImpCode
				AccountSet = true
				if request.cookies("Persistence")("Allow") = "1" then
					response.cookies("Persistence")("AcCode") = ucase(trim("" & rs("AcCode")))
					response.cookies("Persistence")("GUID") = rs("LoginGUID_PERSISTENT")						
				end if						
				
				call SetASPNETSession(ImpCode)	
				
			end if				
			'jhStop
			
		end if
		rsClose()
		
		if AccountSet then

			sql = "DELETE FROM B2B_RecentImpersonations WHERE AcCode = '" & ucase(trim(ImpCode)) & "' AND AdminID = 0" & AdminDetails.AdminID & ""
			db.execute(sql)
		
			sql = "INSERT INTO B2B_RecentImpersonations (AcCode, AdminID) VALUES ('" & ucase(trim(ImpCode)) & "', 0" & AdminDetails.AdminID & ")"
			db.execute(sql)
		
			RangingID = cleannum(request("RangingID"))
			
			if RangingID <> "" then
				if request("ViewReport") = "1" then
					redirto = "/utilities/ranging/ranging-report/?RangingID=" & RangingID
				elseif request("ViewList") = "1" then
					redirto = "/admin/ranging/price-list/?RangingID=" & RangingID & "&CheckShowAdded=1"					
				else
					redirto = "/utilities/ranging/?RangingID=" & RangingID
				end if
			else
				redirto = "/"
			end if
		
			jhAdmin "REDIRECT!"
			dbClose()
			response.redirect redirto
		end if
	end if
	
	sql = "SELECT TOP 20 i.AcCode, c.CompanyName, c.AcStatus FROM B2B_RecentImpersonations i INNER JOIN ExchequerCustDetails c ON c.AcCode = i.AcCode "
	sql = sql & "WHERE AdminID = 0" & AdminDetails.AdminID & " ORDER BY RecentlyImpersonatedID DESC "
	
	x = getrs(sql, RecentArr, RecentC)
	
	set ls = new strconcatstream
	
	if RecentC >= 0 then
	
		ls.add "<table class=AdminTable border=1>"	

		for c = 0 to RecentC	
			AcCode = RecentArr(0,c)
			AcName = RecentArr(1,c)
		
			ls.add "<tr>"
			ls.add "<td valign=top><strong>"
			ls.add "<a class=OrangeLink href=""?impCode=" & AcCode & """>"
			ls.add AcCode
			ls.add "</a>"
			ls.add "</td>"
			ls.add "<td valign=top>"
			ls.add AcName
			ls.add "</td>"						
			ls.add "</tr>"		
			
		next
		
		ls.add "</table>"
		
		RecentOS = ls.value
	else
		
		RecentOS = "Not found"
	
	end if

else
	dbClose()
	response.redirect "/"
end if

%>

    <div id="" class="">
        <div class="container">
		
			<div class="section_header">
				<h3><i title="Admin function" class="icon-cog"></i> <%=lne(PageTitle)%></h3>
			</div>		
		
			<div class=row>
				<div class=span4>
					
						<h4><%=AdminDetails.UserName%></h4>
						
					
						<p>
							Impersonating: <span class="badge badge-default"><%=CurrentAcCode%> / <%=CurrentDepartmentCode%></span>
						</p>					
<%

if AdminDetails.isGroupMember("admin") then 
	jhAdmin "IS ADMIN"
	
elseif AdminDetails.DepartmentCode <> "" then
	sql = "SELECT COUNT(*) AS ACMCount FROM ExchequerCustDetails WHERE DepartmentCode = '" & AdminDetails.DepartmentCode & "' "
	'jhAdmin sql
	set rs = db.execute(sql)
	if not rs.eof then
		ACMCount = rs("ACMCount")
		if ACMCount > 0 then isACM = true
		jhAdmin "isACM = " & isACM
	end if
	rsClose()
end if

if AdminDetails.isGroupMember("admin") then
%>
						<form name=DeptForm method=post action="contact-details.asp">
						<p>
							<div><strong>Update department code contact details</strong></div>
							<input type=text placeholder="Enter a department/ACM code" class=form-control name=SelDept value="<%=SelDept%>"> <input type=submit class="btn btn-primary btn-small" value="GO">
						</p>
						</form>
<%
elseif isACM then
%>
						<p>
							<a class=OrangeLink href="contact-details.asp">Click here to edit your ACM contact details</a>
						</p>				
<%
end if
%>					
						<p>
							<a class=OrangeLink href="/admin/recent/">Click here to view recent orders</a>
						</p>	
						
						<%
						if isjh() or glRangingActive then
						%>
						<p>
							<a class=OrangeLink href="/admin/ranging/">Click here to view ranging proposals</a>
						</p>							
						<%
						end if
						%>

				</div>

				<div class="span5">
							
						<h4>Dealer Search</h4>	
							
						<div class=xwell>
							<p>								
								Search for:	<br>
								<input onclick="" name="SearchVal" type=text id=SearchVal value="<%=SearchVal%>">
								<br>
								<button onclick="getAccountList()" id=searchB class="xpull-right btn btn-primary btn-small">Search</button>
							</p>
						</div>									
									
						<div id=AjaxAccountDiv>
							.
						</div>
									
				</div>
				
				<div class=span3>
						<h4>Recently Used</h4>		
						
						<%
						=RecentOS
						%>
				</div>				
				
			</div>
		</div>
	</div>

	<div class="spacer40">&nbsp;</div>
	</div>
    

<!--#include virtual="/assets/includes/footer.asp" -->

<script>
	function getAccountList() {
	
		var SearchVal = $("#SearchVal").val();
		$("#AjaxAccountDiv").html('<img src="/assets/images/icons/indicator.gif"> loading...');
	
		$.ajax({
			url: "/admin/admin_server.asp?CallType=IMPERSONATE_LIST&SearchVal=" + SearchVal,
			cache: false
			}).done(
				function( html ) {
				$("#AjaxAccountDiv").html(html);
				$('#AjaxAccountDiv').show(100, function() {
					// Animation complete.
				  });					
			});
		
	}
	
	$( document ).ready(function() {
		
		getAccountList()
		
	});
	
	$("#SearchVal").keyup(function (e) {
		//alert(e.keyCode);
		if (e.keyCode == 13) {
			getAccountList()
		}
	});			
	
</script>