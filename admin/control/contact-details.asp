<%
PageTitle = "Admin functions"
%>

<!--#include virtual="/assets/includes/header.asp" -->

<%
'jh "START HERE!"

FormAction = request.form("FormAction")

if AdminDetails.isGroupMember("admin") then 
	SelDept = request("SelDept")
else
	SelDept = AdminDetails.DepartmentCode
end if

jhAdmin "SelDept = " & SelDept

if FormAction = "UPDATE_CONTACT_DETAILS" then
		
	DepartmentContactDetails = request("DepartmentContactDetails")
	
	sql = "UPDATE B2B_DepartmentContacts SET DepartmentContactDetails = '" & validstr(DepartmentContactDetails) & "', UpdatedBy = 0" & AdminDetails.AdminID & ", DateUpdated = GetDate() WHERE DepartmentCode = '" & SelDept & "'"
	jhAdmin sql
	db.execute(sql)
	
	DetailsUpdated = true

else
	if AdminDetails.isGroupMember("admin") then 
		sql = "INSERT INTO B2B_DepartmentContacts (DepartmentCode) SELECT DISTINCT DepartmentCode FROM ExchequerCustDetails ecd "
		sql = sql & "WHERE ISNULL(DepartmentCode, '') <> '' AND NOT EXISTS (SELECT * FROM B2B_DepartmentContacts c WHERE c.DepartmentCode = ecd.DepartmentCode)"
		jhAdmin sql
		db.execute(sql)
	end if
end if

if SelDept <> "" then
	
	sql = "SELECT DepartmentContactDetails FROM B2B_DepartmentContacts WHERE DepartmentCode = '" & SelDept & "'"
	jhAdmin sql
	
	set rs = db.execute(sql)
	if not rs.eof then
		DepartmentContactDetails = trim("" & rs("DepartmentContactDetails"))
	else
		redir = true
	end if
	rsClose()
		
	'jhAdmin "ContactDetails = " & ContactDetails	
		
else
	redir = true
end if

if redir then
	dbClose()
	response.redirect "/"
end if

'jh adminDetails.isGroupMember("admin")
%>

    <div id="" class="">
        <div class="container">
		
			<div class="section_header">
				<h3><i title="Admin function" class="icon-cog"></i> <%=lne(PageTitle)%></h3>
			</div>		
		
			<div class=row>
				<div class=span12>
					
						<h4><%=AdminDetails.UserName%></h4>
						
						<p>Update ACM contact details here (this is what dealers will see on the B2B home page and contact form)</p>
						
						<%
						if DetailsUpdated then jhInfoNice "Contact details have been updated", "<a href=""/admin/control/"">Back to previous page</a>"
						%>
						
						<form name=ACMContactDetailsForm method=post action="/admin/control/contact-details.asp">
							
							<div><span class="label label-warning"><%=SelDept%></span></div>
							<textarea name="DepartmentContactDetails" id="DepartmentContactDetails" style="width:400px;height:300px;"><%=DepartmentContactDetails%></textarea>
							
							<div class=spacer20></div>
							
							<input name=SubmitButton type=submit class="btn-large btn-primary" value="Save changes"> 						
							<input type=hidden class=FormHidden name="FormAction" value="UPDATE_CONTACT_DETAILS">	
							<input type=hidden class=FormHidden name="SelDept" value="<%=SelDept%>">	
						
						</form>
				</div>

				
				
			</div>
		</div>
	</div>

	<div class="spacer40">&nbsp;</div>
	</div>
    

<!--#include virtual="/assets/includes/footer.asp" -->

<script type="text/javascript">
   window.onload = function()
   {
      CKEDITOR.replace( 'DepartmentContactDetails', {
                  height:'300px',width:'400px',                   
					allowedContent: true,
					contentsCss: ['/css/bootstrap.css', '/css/theme.css']
					}
					
				  );
	  
     // CKEDITOR.instances.ContentHolder.on('blur', function() {
         //alert('onblur fired');
      //});
   };
   
	
</script>
