<%
Response.ContentType = "text/html"
Response.AddHeader "Content-Type", "text/html;charset=ISO-8859-1"
'Response.CodePage = 65001
Response.CharSet = "ISO-8859-1"

Response.Expires = 0
Response.Expiresabsolute = Now() - 1442
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"

SelectedLanguageID = getSelectedLanguage()
ThisUserID = session("B2B_AcCode")
if not isLoggedIn() then jhStop



%>

<style>
	.AccountClosed TD {
		xbackground:#f0f0f0;
		color:silver;
	}
</style>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	</head>
	<body>
	

	<!--#include virtual="/assets/includes/global_functions.asp" -->
<%

if not isAdminUser() then jhStop



set adminDetails = new AdminAccountDetails

CallType = request("CallType")

'jh "CallType = " & CallType

if CallType = "RECENT_ORDERS" then

	B2BOrderID = cleannum(request("B2BOrderID"))
	
	set acDetails = new DealerAccountDetails
	
	SelectedLanguageID = acDetails.SelectedLanguageID
	
	%>
	
		<h3 class=spacer40><%=RepeatOrderRef%></h3>
		
	<%
	
	'jhInfo "RepeatOrderRef = " & RepeatOrderRef
	

sql = "SELECT "
sql = sql & "o.B2BOrderID, o.OrderRef, o.OrderDate, o.DeliveryDate "
sql = sql & ", c.AcCode, c.CompanyName, mon.CurrencySymbol, o.B2BOrderID  "
sql = sql & ", b.SKU, vp.ProductID, vp.ProductName, vp.ProductCode, vp.VariantCode, vp.SizeName, vp.ColorName, b.Qty "
sql = sql & "FROM B2B_Orders o "
sql = sql & "INNER JOIN ExchequerCustDetails c ON c.AcCode = o.AcCode "
sql = sql & "INNER JOIN ExchequerCurrency mon ON mon.CurrencyID = c.Currency "
sql = sql & "INNER JOIN B2B_Basket b ON b.B2BOrderID = o.B2BOrderID "
sql = sql & "INNER JOIN [vwSML_ProductLookup] vp ON vp.SKU = b.SKU "

sql = sql & "WHERE o.B2BOrderID = 0" &  B2BOrderID & " " 

if adminDetails.isGroupMember("account_man") and adminDetails.DepartmentCode <> "" then
	sql = sql & "AND (DepartmentCode = '" & adminDetails.DepartmentCode & "') " 'ADDED JIMB01 to the list of possible matches for ACMs 		
elseif not adminDetails.isGroupMember("admin") and not adminDetails.isGroupMember("cust_serv_admin") and not adminDetails.isGroupMember("cust_serv") and not adminDetails.isGroupMember("returns") then
	sql = sql & "AND DepartmentCode = '" & adminDetails.DepartmentCode & "' "		
end if

sql = sql & "ORDER BY b.B2BBasketID " 
	
	'jhAdmin sql
	
	
	x = getrs(sql, DetailArr, DetailC)
	
	'jhAdmin "DetailC = " & DetailC
	
	set ls = new strconcatstream
	
	ls.add "<table width=""100%"" border=1 class=""ReportTable legible"">"	
	
	RecCount = 0
	
	for c = 0 to DetailC

		ThisRef = DetailArr(1,c)
		ThisAc = DetailArr(4,c)
		ThisSKU = DetailArr(8,c)
		
		ThisProductID = DetailArr(9,c)
		ThisProductName = DetailArr(10,c)
		ThisColorName = DetailArr(14,c)
		ThisSizeName = DetailArr(13,c)
		
		ThisProductCode = DetailArr(11,c)
		ThisVariantCode = DetailArr(12,c)
		
		ThisQty = DetailArr(15,c)
		
		imgURL = getImageDefault(ThisProductCode, ThisVariantCode)
		'jh "imgURL = " & imgURL
		
		ShowThis = true
		
		if ShowThis then
			
			RecCount = RecCount + 1
			
			ls.add "<tr>"
			'ls.add "<td>"
			'ls.add "<a href=""/products/?ProductID=" & ThisProductID & """><img src=""" & imgURL & """ class=""ProdThumb60""></a>"
			'ls.add "</td>"			
			ls.add "<td>"

			
			ls.add ThisProductName
			
			ls.add "<div><small>"
			ls.add "<a class=OrangeLink href=""/products/?ProductID=" & ThisProductID & """>"
			ls.add ThisSKU			
			ls.add "</a>"			
			ls.add "</small></div>"
			ls.add "</td>"									
			ls.add "<td valign=top>"
			ls.add ThisColorName
			ls.add "</td>"			
			ls.add "<td valign=top>"
			ls.add ThisSizeName
			ls.add "</td>"			
						
			ls.add "<td valign=top>"
			ls.add "<strong>"
			ls.add ThisQty
			ls.add "</strong>"
			ls.add "</td>"				
			ls.add "</tr>"	
			
			SomeShown = true
		end if
	next
	
	ls.add "</table>"

	RepeatOS = ls.value	
	
	if DetailC = -1 then 
		RepeatOS = "Order not found!"
	else
		RepeatOS = "<h3>" & ThisRef & "/" & ThisAc & "</h3>" & RepeatOS
	end if
	
	'response.write RepeatOS
	
%>
			
			<%
			=RepeatOS
			%>
			
<%	
	

elseif CallType = "IMPERSONATE_LIST" then
	
	SearchVal = trim("" & request("SearchVal"))
	
		
	response.cookies("ADMIN")("ImpersonateSearchVal") = SearchVal
	response.cookies("ADMIN").expires = dateadd("d", 28, now)
	
	dbConnect()
	
	strAgentList = ""

	sql = "select ISNULL(AgentDepartments, '') AS AgentDepartments FROM  end_adminusers where AdminID = " & adminDetails.AdminID
	set rs = db.execute(sql)
	if not rs.eof then
		 If rs("AgentDepartments").value <> "" Then 
			a = Split(rs("AgentDepartments").value, "|")
			for each x in a
				If x <> "" Then
					strAgentList = strAgentList & ",'" & x & "'"
				End If
			next
		End If

	End If	
	rs.Close

	
	sql = "select TOP 500 ecd.AcCode, ecd.CompanyName, ecd.OnHoldStatus, ecd.CreditLimit, ecd.Balance, ecd.CostCentre, ecd.Email, ecd.DepartmentCode, ecd.Contact, ecd.AcStatus, c.ClientPassword "
	sql = sql & ", erc.RiderCardID, erc.isActivate, erc.StartDate, erc.FinishDate "
	sql = sql & "FROM ExchequerCustDetails ecd INNER JOIN END_EbusClients c ON c.EbusClientCode = ecd.AcCode "
	sql = sql & "LEFT JOIN ExchequerRiderCard erc ON erc.AcCode = ecd.AcCode "
	sql = sql & "WHERE (1 = 1) "
	

	if  adminDetails.isGroupMember("admin") or adminDetails.isGroupMember("cust_serv_admin") or adminDetails.isGroupMember("cust_serv") or adminDetails.isGroupMember("returns") then
		sql = sql & ""	
	elseif adminDetails.isGroupMember("country_man") then
		sql = sql & "AND (DepartmentCode IN ('" & adminDetails.DepartmentCode & "'"  & strAgentList	& "))"	
	elseif (adminDetails.isGroupMember("account_man") or adminDetails.isGroupMember("distrib") ) and adminDetails.DepartmentCode <> "" then
		sql = sql & "AND (DepartmentCode  = '" & adminDetails.DepartmentCode & "')"	
	else
		sql = sql & "AND (1=0)"	
	end if

	
	if SearchVal <> "" then
		sql = sql & " AND (CompanyName LIKE '%" & validstr(SearchVal) & "%' OR ecd.AcCode LIKE '%" & validstr(SearchVal) & "%' OR ecd.Email LIKE '%" & validstr(SearchVal) & "%' OR Contact LIKE '%" & validstr(SearchVal) & "%' ) "		
	end if
	
	sql = sql & " ORDER BY ecd.AcCode "

	
	x = getrs(sql, DealerArr, DealerC)
	
	
	set ls = new strconcatstream
	
	if DealerC >= 0 then
	
		ls.add "<table class=AdminTable border=1>"	

		for c = 0 to DealerC	
			AcCode = DealerArr(0,c)
			AcName = DealerArr(1,c)
			ContactName = DealerArr(8,c)
			ContactEmail = "" & DealerArr(6,c)
			ThisBalance = DealerArr(4,c)
			
			ThisOnHoldStatus = "" & DealerArr(2,c)
			ThisAcStatus = "" & DealerArr(9,c)
			
			ThisRiderCardID = "" & DealerArr(11,c)
			
			if ThisRiderCardID <> "" then
				ThisRiderCard = true
			else
				ThisRiderCard = false
			end if
			
			if isjh() then ThisPassword = DealerArr(10,c)
			
			OnHoldStr = ""
			AcStatusStr = ""
			AcStatusClass = ""
			ThisClosed = false
			if ThisOnHoldStatus <> "OK" then
				'OnHoldStr = "<span class=""label label-important"">ON HOLD STATUS: " & ThisOnHoldStatus & "</span>&nbsp;"
			end if
			
			if ThisAcStatus <> "OK" then
				AcStatusStr = "<span class=""label label-important"">status: " & ThisAcStatus & "</span>&nbsp;"
				if ThisAcStatus = "CLOSED" then
					AcStatusClass = "AccountClosed"
					ThisClosed = true
				end if
			end if			
			
			ImpNote = ""
			ImpError = ""
			
			AllowImp = true
			if ThisClosed then AllowImp = false
			if ThisRiderCard then
				
				ImpNote = "RIDER CARD"
				
				ThisRiderCardActivated = DealerArr(12,c)
				ThisRiderCardStart = DealerArr(13,c)
				ThisRiderCardFinish = DealerArr(14,c)
				
				if ThisRiderCardActivated then
					
					if datediff("d", ThisRiderCardStart, now) < 0 then
						AllowImp = false
						ImpError = "RIDER CARD NOT STARTED"
					end if
					
					if datediff("d", ThisRiderCardFinish, now) > 0 then
						AllowImp = false
						ImpError = "RIDER CARD EXPIRED"
					end if					
					
				else
					AllowImp = false
					ImpError = "RIDER CARD NOT ACTIVE"
				end if
				
			end if
			
			ContactEmail = replace(ContactEmail, ";", ",")
			EmailStr = ""
			
			EmailArr = split(ContactEmail, ",")
			for x = 0 to ubound(EmailArr)
				ThisEmail = EmailArr(x)
				if ThisEmail <> "" then
					EmailStr = EmailStr & "<div><a href=""mailto:" & ThisEmail & """>" & ThisEmail & "</a></div>"
				end if
			next
			
			ls.add "<tr class=""" & AcStatusClass & """>"
			ls.add "<td valign=top><strong>"
			if not AllowImp then
				ls.add AcCode
				if ImpError <> "" then
					ls.add "<div><span class=""label label-error"">" & ImpError & "</span></div>"
				end if				
			else
				ls.add "<a class=OrangeLink href=""?impCode=" & AcCode & """>"
				ls.add AcCode
				ls.add "</a>"
				if ImpNote <> "" then
					ls.add "<div><span class=""label label-info"">" & ImpNote & "</span></div>"
				end if	
			end if
			ls.add "<div>"
			ls.add OnHoldStr & AcStatusStr
			ls.add "</div>"
			ls.add "<small>" & ThisPassword & "</small>"
			ls.add "</td>"
			ls.add "<td valign=top>"
			ls.add AcName
			ls.add "</td>"			
			ls.add "<td valign=top>"
			ls.add "<strong>" & ContactName & "</strong>"
			ls.add "<div><small>"
			ls.add EmailStr
			ls.add "</small></div>"
			ls.add "</td>"							
		
			'ls.add "<td valign=top align=right>"
			'ls.add ThisBalance
			'ls.add "</td>"					
			
			ls.add "</tr>"		
		next
		
		ls.add "</table>"
		
		os = ls.value
	else
		
		os = "Not found"
	
	end if

	response.write os
	
else


end if

dbClose()

%>			
	

</body>
</html>
