<%
PageTitle = "CMS Label Editor"

session.timeout = 900
%>

<!--#include virtual="/assets/includes/header.asp" -->

<%
MaxAttempts = 20
FormAction = request.form("FormAction")
'jh "FormAction = " & FormAction

UntranslatedCheck = request("UntranslatedCheck")

'jhAdmin "AUTLOGIN!"

if not isAdminUser() then
	dbClose()
	response.redirect "/"
end if
'jh "LoginAttempts = " & LoginAttempts

%>

        <div class="container">
            <div class="section_header">
                <h3>Label editor</h3>
            </div>
			
			<div class="row">
			
<%



	if ErrorsFound then
		jhErrorNice "Login failed", "You have entered an invalid username or password "
	end if
	
	dbConnect()
	
	EditLanguageID = cleannum(request("EditLanguageID"))
	if EditLanguageID = "" then EditLanguageID = getSelectedLanguage()
	'jh "AdminDetails.SelectedLanguageID = " & getSelectedLanguage()
	
	'jh "EditLanguageID = " & EditLanguageID
	'EditLanguageID = 40	
	
	if FormAction = "SAVE" then
		'jhInfo "SAVING"
		RecCount = cdbl("" & request("RecCount"))
		'jh "RecCount = " & RecCount
		
		for c = 0 to RecCount
			ThisText = request.form("Translated_" & c)
			ThisID = request.form("ID_" & c)
			
			ThisChanged = request.form("hasChanged_" & c)
			'jhAdmin "ThisChanged = " & ThisChanged
			
			if ThisChanged <> "" then
				
				sql = "DELETE FROM B2B_ContentTranslations WHERE LanguageID = 0" & EditLanguageID & " AND B2BContentID = 0" & ThisID & ""
				jhAdmin sql
				db.execute(sql)
				
				if ThisText <> "" then
					sql = "INSERT INTO B2B_ContentTranslations (B2BContentID, LanguageID, TranslatedText) VALUES ("
					sql = sql & "0" & ThisID & ", "
					sql = sql & "0" & EditLanguageID & ", "
					sql = sql & "'" & validstr(ThisText) & "') "
					jhAdmin sql
					db.execute(sql)
				end if	
			end if
		next
		jhInfoNice "Saving labels...", "Your changes have been saved!"
	end if
	
	
	sql = "SELECT LanguageID, LanguageName, LanguageISO, SpecialChars FROM END_Language "
	sql = sql & "WHERE B2BEditable = 1 AND LanguageID >= 0 ORDER BY LanguageID "
	
	'jh sql
	x = getrs(sql, EditLangArr, EditLangC)
	'jh "EditLangC = " & EditLangC
	
	SelEditLangName = "Select..."
	
	'jh session("B2B_AllowedLanguages")
	ThisAllowed = AdminDetails.AllowedLanguages
	
	CheckCurrentLang = session("B2B_UserLanguageID") 
	if instr(1, ThisAllowed, "|" & CheckCurrentLang & "|") > 0 and EditLanguageID = "" and CheckCurrentLang <> "0" then
		EditLanguageID = CheckCurrentLang
	end if
	
	for x = 0 to EditLangC
		

		ThisLanguage = EditLangArr(0,x)	
		LanguageName = EditLangArr(1,x)
		ThisFlag = EditLangArr(2,x)
		ThisSpecialChars = "" & EditLangArr(3,x)
		
		'jh "ThisLanguage = " & ThisLanguage
	
		if instr(1, ThisAllowed, "|" & ThisLanguage & "|") > 0 then
			SomeAllowed = true
			langos = langos & "<li><a onclick=""changeLang(" & ThisLanguage & ")"" href=""#"">" & LanguageName & "</a></li>"
			if "" & EditLangArr(0,x) = "" & EditLanguageID or EditLanguageID = "" then
				
				if ThisSpecialChars = "1" then 
					AllowInlineEditing = false
				else
					AllowInlineEditing = true
				end if
				
				'jh "AllowInlineEditing = " & AllowInlineEditing
				
				EditLanguageID = EditLangArr(0,x) 
				SelEditLangName = LanguageName
				'jhInfo "EditLanguageID = " & EditLanguageID
				EditLabel = ucase(LanguageName)
			end if
		end if		
		
	next
	

	if not SomeAllowed then
		jhErrorNice "Editing not allowed", "Your user account is not set up to translate any website content - contact Falk to gain access"
	else
	
%>			
						
			<%
			if not AllowInlineEditing then 
			%>			
				<form name=loginForm action="label_editor.asp" method=post>
			<%
			end if
			%>
				
					<h3>Select a language</h3> 
						<div class="btn-group spacer10">	
							<button id="LabelLangSelectorButton" class="btn btn-small btn-warning dropdown-toggle" data-toggle="dropdown"><span><%=SelEditLangName%></span> <span class="caret"></span></button>
								 <ul class="dropdown-menu">
								 
									<%									
									=LangOS
									%>										
								  </ul>		
								</div>
					
							<div>
								<input type=checkbox <%if UntranslatedCheck <> "" then response.write "CHECKED"%> class=form-control name="UntranslatedCheck" id="UntranslatedCheck"> <label for="UntranslatedCheck">Show only UNTRANSLATED items?</label>
							</div>
					<table border=0>
					<%
					'jh "GET LABELS"
					
					sql = "SELECT bc.B2BContentID, bc.LookupName, bc.ContentText, tr.TranslatedText, bc.LabelDescription, LabelEditable, AllowHTML "
					sql = sql & "FROM B2B_Content bc "
					sql = sql & "LEFT JOIN (SELECT B2BContentID, TranslatedText FROM B2B_ContentTranslations WHERE LanguageID = 0" & EditLanguageID & ") tr  ON tr.B2BContentID = bc.B2BContentID "					
										
					sql = sql & "WHERE LabelEditable = 1 "'AND AllowHTML = 0 "
					
					LookFor = request("lf")
					if LookFor <> "" then
						sql = sql & "AND (ContentText LIKE '%" & validstr(lookfor) & "%' OR TranslatedText LIKE '%" & validstr(lookfor) & "%' OR LookupName LIKE '%" & validstr(lookfor) & "%')"
					end if
					
					'sql = sql & "AND ContentText LIKE '%story%' "
					sql = sql & "ORDER BY bc.ContentText"
					'jh sql
					x = getrs(sql, RecArr, rc)
					
					'jh "rc = " & rc
					
					for c = 0 to rc
						'jh RecArr(1,c) & " ____ <input value=""" & replace(RecArr(2,c), """", "&quot;") & """> ____ <input xvalue=""" & replace(RecArr(2,c), """", "&quot;") & """>"
						ThisB2BContentID = RecArr(0,c)
						ThisLN = RecArr(1,c)
						'jh "ThisLN = " & ThisLN
						
						EnglishVersion = RecArr(2,c)
						TranslatedVersion = "" & RecArr(3,c)
						LabelDescription = "" & RecArr(4,c)
						
						ShowThis = true
						
						if EditLanguageID = "0" then
							labelStyle = "alert-success" 
							TranslatedVersion = EnglishVersion
						elseif TranslatedVersion <> "" then 
							labelStyle = "alert-success" 
							if UntranslatedCheck <> "" then ShowThis = false
						else 							
							labelStyle = "alert-error"
						end if
						
						ThisLabelEditable = "" & RecArr(5,c)
						ThisAllowHTML = "" & RecArr(6,c)
						
						if ShowThis then
						%>

							
<%
						if ThisAllowHTML = "1" then
										
							
%>
							<tr id="<%=ThisLN%>" style="">
								<td style="padding-top:20px;">
									
									
									
									<div class=alert>
									
										<div title="<%=LabelDescription%>" class="alert <%=labelStyle%>"><small><%=EnglishVersion%></small></div>			
									
										<div class=pull-right title="<%=ThisB2BContentID%>" style="font-size:6pt;color:silver;"><%=ThisLN%></div>
										
										<h5 class=spacer20>You must use the rich text editor for this content item</h5>										
										<div>
										<a class="btn btn-small btn-success" href="/admin/b2bcontent/content_edit.asp?SelectedLanguageID=<%=EditLanguageID%>&content_lookup=<%=ThisLN%>&BackTo=LABEL_EDITOR&BackToID=<%=ThisLN%>">Click to edit this item in the HTML text editor</a>
										</div>
										
									</div>
								</td>
							</tr>
<%											
						else									
%>							
							<tr id="<%=ThisLN%>" style="">
								<td style="padding-top:20px;">
									<div title="<%=LabelDescription%>" class="alert <%=labelStyle%>"><%=EnglishVersion%></div>								
								</td>
							</tr>
							
							<tr>
								<td valign=top>
									
									<input name="hasChanged_<%=c%>" id="hasChanged_<%=c%>" class=FormHidden type="hidden" value="">
									
									<textarea onchange="setChanged('<%=c%>');" name="Translated_<%=c%>" id="Translated_<%=c%>" title="Enter the translated version of the label here" style="height:50px;width:600px;"><%=TranslatedVersion%></textarea><input name="ID_<%=c%>" class=FormHidden type="hidden" value="<%=ThisB2BContentID%>">			
									<div class="pull-right text-right">
										<span class="label label-success">In <%=EditLabel%></span>
										
										<%
										' IF CHINESE FOR EXAMPLE, THEN CONTENT HAS TO BE HTML ENCODED, SO NO AJAX SAVING...
										
										'jhAdmin "ThisAllowHTML = " & ThisAllowHTML
										if AllowInlineEditing then
			
										%>
										
											<button title="<%=ThisLN%>" onclick="ajaxSave(<%=EditLanguageID%>, '<%=c%>', '<%=ThisB2BContentID%>');" id="SAYG_<%=c%>" class="btn btn-small btn-info">save</button>
										
										<%
											if EditLanguageID = "0" then
											%>
												<div class="small legible">
													<input id="clear_existing_<%=c%>" type="checkbox"> clear existing?
												</div>
											<%
											end if									
										end if
										%>
										
										<div title="<%=ThisB2BContentID%>" style="font-size:6pt;color:silver;"><%=ThisLN%></div>
									</div>
									<div class=LabelResults id="ajax_results_<%=c%>"></div>
								</td>							
							</tr>
						<%
							end if
						
						end if
					next
					
					%>
					</table>
					<div class="row submit">
					
						<div class="span3 right">
						
						<%
						if not AllowInlineEditing then 
						%>
						
							<input class="btn btn-large btn-info" type="submit" value="SAVE">
							
						
							<input name="FormAction" class=FormHidden type="hidden" value="SAVE">
							<input name="RecCount" class=FormHidden type="hidden" value="<%=rc%>">
							<input name="EditLanguageID" class=FormHidden type="hidden" value="<%=EditLanguageID%>">
						
						<%
						end if
						%>	
						
						</div>
					</div>					
					
					
					
			<%
			if not AllowInlineEditing then 
			%>			
				</form>
			<%
			end if
			%>				
			
				
		<%
		end if
		%>
			
			</div>
			
            <div class="spacer40" id="AJR">
				
            </div>
        
		
    </div>

<!--#include virtual="/assets/includes/footer.asp" -->

<script>
	function setChanged(pid) {
		
		$('#hasChanged_'+pid).val('1');
	
	}	
	
</script>

<script>
	function ajaxSave (pLang, pid, pContent) {
		//alert(pid);
		//alert(pContent);
		
		$('#ajax_results_'+pid).html('<div style="padding:30px;"><img src="/assets/images/prodbrowser/indicator.gif"> <small>saving..</small></div>');
		//$('#ajax_prod_results').show();	
		
		$('.LabelResults').html('');
		
		var tt = document.getElementById('Translated_'+pid).value;
		
		
		var clearexisting = ""
		if ( $('#clear_existing_'+pid).checked ) {
			clearexisting = "on"
		}
		
		
		//alert(tt)
		//alert(tt);
		//tt = escape(tt) ;
		//alert(tt);
		//tt = escape(tt);
		
		//alert(pLang);
		
		$.ajax({
			url: "label_server.asp?TransText=" + tt,
			scriptCharset: "ISO-8859-1" ,
			contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",	
			data: {EditLanguageID: pLang, TransID: pContent, ClearExisting: clearexisting},
			cache: false
			}).done(
				function( html ) {
				$('#ajax_results_'+pid).html(html);
				$('#ajax_results_'+pid).show('slow', function() {
					// Animation complete.
				  });		
			
			});	

	}
	
	$("#UntranslatedCheck").click( function() {
	
		changeLang('<%=EditLanguageID%>');
	});
	
	function changeLang(pLanguage) {
		var URL = 'label_editor.asp?EditLanguageID=' + pLanguage + ''
		
		var onlyUntranslated = false
		
		if ($("#UntranslatedCheck").prop('checked')) {
			onlyUntranslated = true
		} 
		
		if (onlyUntranslated) {
			URL = URL + '&UntranslatedCheck=1'
		}
		
		//alert($('#UntranslatedCheck').prop('checked'));
		
		window.location.assign(URL)
		
	}

</script>