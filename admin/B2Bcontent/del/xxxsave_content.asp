<!--#include virtual="/assets/includes/global_functions.asp" -->

<%


jhInfo "SAVING CONTENT"

xxxx
jh "CHECK LOGIN VALIDATION"

ThisUserID = session("B2C_AdminUserID")

jh "GET LANGUAGE"

ContentLanguageID = cdbl("0" & request("ContentLanguageID"))
jh "ContentLanguageID = " & ContentLanguageID
  
jh "GET VALUES"

ContentEditCount = cdbl("0" & request("ContentEditCount"))
jh "ContentEditCount = " & ContentEditCount
FormAction = request("FormAction")
SubmitButton = ucase("" & request("SubmitButton"))
jh "FormAction = " & FormAction
jh "SubmitButton = " & SubmitButton

if FormAction = "SAVE" and SubmitButton <> "CANCEL" then
	dbConnect()

	for c = 1 to ContentEditCount
	
		jh "CID = " & request("ContentID_" & c)
		ThisID = cleannum(request("ContentID_" & c))
		ThisLookup = cleannum(request("ContentLookup_" & c))
		if ThisID = "" then ThisID = 0
		ThisText = request("ContentHolder_" & c)
		ThisOrig = request("ContentOriginal_" & c)
		
		jh "ThisText = " & ThisText
		
		if ThisText <> ThisOrig or 1 = 1 then
			jh "CHANGED - " & ThisID
			jh ThisText
			
			sql = "exec [spB2CContentChange] " & ThisID & ", '" & validstr(ThisLookup) & "','" & validstr(ThisText) & "', '" & validstr(ThisOrig) & "', 0" & ContentLanguageID & ", 0" & ThisUserID
			jh replace(sql, "<", "&lt;")
			db.execute(sql)
		end if
	next

	dbClose()
	returnStr = "ContentSaved=1"
	
elseif FormAction = "CANCEL" or SubmitButton = "CANCEL" then
	returnStr = "ContentCancelled=1"
elseif FormAction = "LOGOUT" then
	jh "LOGOUT"
	xxx
elseif FormAction = "TOGGLE_EDIT" then
	jh "TOGGLE EDIT"
	xxx	
end if

jh "GET RETURN URL"
ReturnURL = request("ContentReturnURL")
ReturnURL = replace(ReturnURL, "?ContentSaved=1", "")
ReturnURL = replace(ReturnURL, "?ContentCancelled=1", "")
ReturnURL = replace(ReturnURL, "&ContentSaved=1", "")
ReturnURL = replace(ReturnURL, "&ContentCancelled=1", "")
if ReturnURL = "" then 
	jh "NO URL"
	'xxx
	ReturnURL = "/"
end if

if not NoRedirect then
	if instr(1, ReturnURL, "?") = 0 then ReturnURL = ReturnURL & "?" & returnStr else ReturnURL = ReturnURL & "&" & returnStr
	'response.redirect ReturnURL 
end if
jhInfo "ENDS"
%>