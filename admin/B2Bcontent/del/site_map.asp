<%
PageTitle = "CMS Homepage Sitemap Update"

session.timeout = 900
%>

<!--#include virtual="/assets/includes/header.asp" -->

<%

FormAction = request.form("FormAction")
'jh "FormAction = " & FormAction

if session("B2C_AdminUserID") = "" then
	dbClose()
	response.redirect "/"
end if
'jh "LoginAttempts = " & LoginAttempts

MaxBanners = 4

OrderID = cleannum(request("orderid"))
NewOrder = cleannum(request("NewOrder"))	

if OrderID <> "" and NewOrder <> "" then
	
	'jhInfo "SET ORDER! (" & OrderID & ")"
	
	sql = "UPDATE B2C_Banners SET BannerZOrder = " & NewOrder & " WHERE BannerID = 0" & OrderID
	'jh sql
	db.execute(sql)
		
	sql = "SELECT BannerID FROM B2C_Banners ORDER BY BannerZOrder "
	x = getrs(sql, bArr, bc)
	
	NewOrder = 0
	for c = 0 to bc
		NewOrder = NewOrder + 10
		sql = "UPDATE B2C_Banners SET BannerZOrder = " & NewOrder & " WHERE BannerID = 0" & bArr(0,c)
		'jh sql
		db.execute(sql)
	next		
	
	dbClose()
	response.redirect "banner_links_dynamic.asp?orderchanged=1"
	
elseif request("orderchanged") = "1" then
	jhInfoNice "Banner order updated!", "You have changed the banner order!"
	
end if

%>

        <div class="container">
            <div class="section_header">
                <h3><%=PageTitle%></h3>
            </div>
			
			<div class="row">
			
<%
	
	if FormAction = "SAVE_SITEMAP" then
		'jhInfo "SAVING"		
		
		'ErrorsFound = true
		
		for c = 0 to MaxBanners
		
			'jh "c = " & c
		
		next
		
		
		if ErrorsFound then
			jhErrorNice "Errors found", "Please enter a value for all banners/buttons/links"
		else
		
			dbConnect()
		
			for c = 0 to MaxBanners
			
				BannerID = request("BannerID_" & c)
				BannerTitle = request("BannerTitle_" & c)
				BannerButton = request("BannerButton_" & c)
				BannerLink = request("BannerLink_" & c)
				BannerImage = request("BannerImage_" & c)
				BannerStatusID = request("BannerStatusID_" & c)
				if BannerStatusID <> "" then BannerStatusID = "1" else BannerStatusID = "0"
				
				sql = "UPDATE B2C_Banners SET "
				sql = sql & "BannerTitle = '" & validstr(BannerTitle) & "', "
				sql = sql & "BannerButton = '" & validstr(BannerButton) & "', "
				sql = sql & "BannerLink = '" & validstr(BannerLink) & "', "
				'sql = sql & "BannerImage = '" & validstr(BannerImage) & "', "
				sql = sql & "BannerStatusID = " & validstr(BannerStatusID) & " "
				
				sql = sql & "WHERE BannerID = 0" & BannerID & " "
				
				jhAdmin sql
				db.execute(sql)
				
				sql = "UPDATE B2C_Content SET ContentText = '" & validstr(BannerTitle) & "' WHERE LookupName = 'home_banner" & BannerID & "_text'"
				jhAdmin sql
				db.execute(sql)
				
				sql = "UPDATE B2C_Content SET ContentText = '" & validstr(BannerButton) & "' WHERE LookupName = 'B2C_HOME_BANNER_BUTTON_" & BannerID & "'"
				jhAdmin sql		
				db.execute(sql)				
				
				ClearCheck = request("ClearCheck_" & c)
 
				if ClearCheck <> "" then
					sql = "DELETE FROM B2C_ContentTranslations WHERE B2CContentID IN (SELECT B2CContentID FROM B2C_Content WHERE LookupName IN ('home_banner" & BannerID & "_text', 'B2C_HOME_BANNER_BUTTON_" & BannerID & "')) "
					jhAdmin sql
				end if
			
			next
		
			'sql = "UPDATE B2C_Content SET ContentText = '" & validstr(BannerTitle1) & "' WHERE LookupName = 'home_banner_tnae_news' "
			'jh sql
			'db.execute(sql)
			
			jhInfoNice "Saving banners...", "Your changes have been saved!"
		
			ContentSaved = true
		end if
		
	end if

	SomeAllowed = true
	
	if not SomeAllowed then
		jhErrorNice "Editing not allowed", "Your user account is not set up to translate any website content - contact Falk to gain access"
	else
	
	if ContentSaved then
	%>			
				<p><a class=OrangeLink href="/">Back to home page</a></p>
				<p><a class=OrangeLink href="/admin/content/banner_links_dynamic.asp">Make more changes</a></p>
				<div class=spacer40></div>
	
	<%
	else
%>			

            <div class="spacer40" id="dz1">
				
            </div>

						
				<form name=linkform action="site_map.asp" method=post>
				
					<%
					
					dbConnect()
					'jh "GET LINKS
					
					sql = "SELECT BannerID, BannerTitle, BannerButton, BannerLink, BannerImage, BannerStatusID, ISNULL(BannerZOrder,0) AS Z "
					sql = sql & "FROM B2C_Banners "
					sql = sql & "ORDER BY BannerZOrder, BannerID "
					
					'jh sql
					x = getrs(sql, BannerArr, BannerC)
					
					'jh "BannerC = " & BannerC
					
					
					'jh "GET LABELS"
				
						for c = 0 to MaxBanners
						
							BannerTitle = ""
							BannerButton = ""
							BannerLink = ""
							BannerImage = ""
							BannerEnabled = ""
							BannerID = ""
						
							if c <= BannerC then
							
								BannerID = BannerArr(0,c)
								BannerTitle = BannerArr(1,c)
								BannerButton = BannerArr(2,c)
								BannerLink = BannerArr(3,c)
								BannerImage = BannerArr(4,c)
								BannerImage = "P" & BannerID & ".jpg"
								
								ThisZOrder = cdbl(BannerArr(6,c))
								
								SetUp = ThisZOrder - 15
								SetDown = ThisZOrder + 15
								
								BannerEnabled = "" & BannerArr(5,c)
								if BannerEnabled = "1" then checkStr = "CHECKED" else checkStr = ""
								
								if isjh() then
									upLink = "<a title=""Move UP"" href=""?orderid=" & BannerID & "&NewOrder=" & SetUp & """><i class=icon-chevron-up></i></a>"
									downLink = "<a title=""Move DOWN"" href=""?orderid=" & BannerID & "&NewOrder=" & SetDown & """><i class=icon-chevron-down></i></a>"
									
									'jh upLink
									'jh downLink
									
								end if
								
							end if
							
						%>
							<hr>
							<div class=spacer40>
								<h3>Banner [<%=c+1%>] <%=UpLink%> <%=DownLink%></h3>
								<%
								if BannerImage <> "" then
								%>
									<img class=pull-right style="width:300px;height:70px;" src="/assets/images/home_panels_new/<%=BannerImage%>">
								<%	
								else
								%>
									<!--No image...-->
								<%
								end if
								%>
								<p>
									<input <%=CheckStr%> type=checkbox name="BannerStatusID_<%=c%>"> Enabled?
								</p>
								<table cellpadding=8 border=1>
									<tr>
										<td>
											Title
										</td>
										<td>
											Button text
										</td>			
										<td>
											Link
										</td>													
									</tr>
									<tr>
										<td>
											<input style="width:500px;" class="input-xlarge" name="BannerTitle_<%=c%>" value="<%=BannerTitle%>">
										</td>
										<td>
											<input class=input name="BannerButton_<%=c%>" value="<%=BannerButton%>">	
										</td>			
										<td>
											<input class=input name="BannerLink_<%=c%>" value="<%=BannerLink%>">
										</td>													
									</tr>	
									<!--<tr>
										<td align=right>
											Image file name
										</td>									
										<td colspan=2>
											<input style="width:500px;" class="input-xlarge" name="BannerImage_<%=c%>" value="<%=BannerImage%>">
										</td>
																						
									</tr>									
									-->
								</table>
								<p style="text-align:right;">							
									<input type=checkbox name="ClearCheck_<%=c%>"> Clear all current translations for this banner on save?
								</p>
								<input class=FormHidden type=hidden name="BannerID_<%=c%>" value="<%=BannerID%>">
								<%
								'=BannerImage
								%>
							</div>
						<%
						
						next
					
						%>


					
					<div class="row submit">
					
						<div class="span8 right">
							<%
							if isjh() then
							%>
							
							<%
							end if
							%>
							<input class="btn btn-large btn-info" type="submit" value="SAVE">
							<input name="FormAction" class=FormHidden type="hidden" value="SAVE_BANNERS">							
						</div>
					</div>					
					
					
					
				</form>					
			
				
		<%
			end if
		end if
		%>
			
			</div>
			

        
		
    </div>

<!--#include virtual="/assets/includes/footer.asp" -->

<script>
	function ajaxSave (pLang, pid, pContent) {
		//alert(pid);
		//alert(pContent);
		
		$('#ajax_results_'+pid).html('<div style="padding:30px;"><img src="/assets/images/prodbrowser/indicator.gif"> <small>saving..</small></div>');
		//$('#ajax_prod_results').show();	
		
		$('.LabelResults').html('');
		
		var tt = document.getElementById('Translated_'+pid).value;
		//alert(tt)
		//alert(tt);
		tt = escape(tt) ;
		//alert(tt);
		//tt = escape(tt);
		
		//alert(pLang);
		
		$.ajax({
			url: "label_server.asp?TransText=" + tt,
			scriptCharset: "ISO-8859-1" ,
			contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",	
			data: {EditLanguageID: pLang, TransID: pContent},
			cache: false
			}).done(
				function( html ) {
				$('#ajax_results_'+pid).html(html);
				$('#ajax_results_'+pid).show('slow', function() {
					// Animation complete.
				  });		
			
			});	

	}
	
	$("div#dz1").dropzone({ url: "/assets/images/home_panels_new/" });
	
</script>