<%
PageTitle = "CMS Label Editor"

session.timeout = 900
%>

<!--#include virtual="/assets/includes/header.asp" -->

<%
MaxAttempts = 20
FormAction = request.form("FormAction")
'jh "FormAction = " & FormAction

if session("B2C_AdminUserID") = "" then
	dbClose()
	response.redirect "/"
else
	response.redirect "banner_links_dynamic.asp"
end if
'jh "LoginAttempts = " & LoginAttempts


%>

        <div class="container">
            <div class="section_header">
                <h3><%=PageTitle%></h3>
            </div>
			
			<div class="row">
			
<%



	
	
	if FormAction = "SAVE_BANNERS" then
		'jhInfo "SAVING"		
		
		'ErrorsFound = true
		
		BannerText1 = request("BannerText1")
		BannerText2 = request("BannerText2")
		BannerText3 = request("BannerText3")
		
		BannerLink1 = request("BannerLink1")
		BannerLink2 = request("BannerLink2")
		BannerLink3 = request("BannerLink3")
		
		ButtonText1 = request("ButtonText1")
		ButtonText2 = request("ButtonText2")
		ButtonText3 = request("ButtonText3")		
		
		if BannerText1 = "" or BannerText2 = "" or BannerText3 = "" then ErrorsFound = true
		if BannerLink1 = "" or BannerLink2 = "" or BannerLink3 = "" then ErrorsFound = true
		if ButtonText1 = "" or ButtonText2 = "" or ButtonText3 = "" then ErrorsFound = true
		
		if ErrorsFound then
			jhErrorNice "Errors found", "Please enter a value for all banners/buttons/links"
		else
			sql = "UPDATE B2C_Content SET ContentText = '" & validstr(BannerText1) & "' WHERE LookupName = 'home_banner_tnae_news' "
			'jh sql
			db.execute(sql)
			sql = "UPDATE B2C_Content SET ContentText = '" & validstr(BannerText2) & "' WHERE LookupName = 'home_banner2_text' "
			'jh sql
			db.execute(sql)			
			sql = "UPDATE B2C_Content SET ContentText = '" & validstr(BannerText3) & "' WHERE LookupName = 'home_banner3_text' "
			'jh sql
			db.execute(sql)		
			
			sql = "UPDATE B2C_Content SET ContentText = '" & validstr(ButtonText1) & "' WHERE LookupName = 'B2C_HOME_BANNER_BUTTON_1' "
			'jh sql
			db.execute(sql)
			sql = "UPDATE B2C_Content SET ContentText = '" & validstr(ButtonText2) & "' WHERE LookupName = 'B2C_HOME_BANNER_BUTTON_2' "
			'jh sql
			db.execute(sql)			
			sql = "UPDATE B2C_Content SET ContentText = '" & validstr(ButtonText3) & "' WHERE LookupName = 'B2C_HOME_BANNER_BUTTON_3' "
			'jh sql
			db.execute(sql)					
						
			x = SetSysVarValue("B2C_HOME_BANNER_LINK_1", BannerLink1)
			x = SetSysVarValue("B2C_HOME_BANNER_LINK_2", BannerLink2)
			x = SetSysVarValue("B2C_HOME_BANNER_LINK_3", BannerLink3)
			
			jhInfoNice "Saving banners...", "Your changes have been saved!"
			
			ClearCheck = request("ClearCheck")
			
			if instr(1, ClearCheck, "|1|") > 0 then
				sql = "DELETE FROM B2C_ContentTranslations WHERE B2CContentID IN (249, 250) "
				jhAdmin sql
				db.execute(sql)
			end if	
			if instr(1, ClearCheck, "|2|") > 0 then
				sql = "DELETE FROM B2C_ContentTranslations WHERE B2CContentID IN (251, 253) "
				jhAdmin sql
				db.execute(sql)
			end if	
			if instr(1, ClearCheck, "|3|") > 0 then
				sql = "DELETE FROM B2C_ContentTranslations WHERE B2CContentID IN (252, 254) "
				jhAdmin sql
				db.execute(sql)
			end if					
			
			ContentSaved = true
		end if
		
	end if

	SomeAllowed = true
	
	if not SomeAllowed then
		jhErrorNice "Editing not allowed", "Your user account is not set up to translate any website content - contact Falk to gain access"
	else
	
	if ContentSaved then
	%>
				<p><a href="/">Back to home page</a></p>
				<div class=spacer40></div>
	
	<%
	else
%>			
						
				<form name=linkform action="banner_links.asp" method=post>
				
					<%
					'jh "GET LINKS"
					
					BannerLink1 = GetSysVarValue("B2C_HOME_BANNER_LINK_1")
					'jh "BannerLink1 = " & BannerLink1
					BannerLink2 = GetSysVarValue("B2C_HOME_BANNER_LINK_2")
					'jh "BannerLink2 = " & BannerLink2
					BannerLink3 = GetSysVarValue("B2C_HOME_BANNER_LINK_3")
					''jh "BannerLink3 = " & BannerLink3
					
					'jh "GET LABELS"
					
					BannerText1 = getLabel_NOEDIT("home_banner_tnae_news")
					'jh "BannerText1 = " & BannerText1
					BannerText2 = getLabel_NOEDIT("home_banner2_text")
					'jh "BannerText2 = " & BannerText2
					BannerText3 = getLabel_NOEDIT("home_banner3_text")
					'jh "BannerText3 = " & BannerText3					
					
					'jh "GET BUTTON TEXT"
					
					ButtonText1 = getLabel_NOEDIT("B2C_HOME_BANNER_BUTTON_1")
					jhAdmin "ButtonText1 = " & ButtonText1
					ButtonText2 = getLabel_NOEDIT("B2C_HOME_BANNER_BUTTON_2")
					jhAdmin "ButtonText2 = " & ButtonText2
					ButtonText3 = getLabel_NOEDIT("B2C_HOME_BANNER_BUTTON_3")
					jhAdmin "ButtonText3 = " & ButtonText3										
					
					
						%>

						<hr>
						<div class=spacer40>								
							<h3>Banner 1</h3>
							TEXT: <input style="width:700px;" class="input-xlarge" name="BannerText1" value="<%=BannerText1%>">
							<div class=spacer10></div>
							BUTTON TEXT: <input class=input name="ButtonText1" value="<%=ButtonText1%>">								
							<div class=spacer10></div>
							LINK: <input class=input name="BannerLink1" value="<%=BannerLink1%>">		
							<div class=spacer10></div>
							<input type=checkbox name="ClearCheck" value="|1|"> Clear all current translations for this banner?
						</div>
						<hr>
						<div class=spacer40>								
							<h3>Banner 2</h3>
							TEXT: <input style="width:700px;" class="input-xlarge" name="BannerText2" value="<%=BannerText2%>">
							<div class=spacer10></div>
							BUTTON TEXT: <input class=input name="ButtonText2" value="<%=ButtonText2%>">								
							<div class=spacer10></div>
							LINK: <input class=input name="BannerLink2" value="<%=BannerLink2%>">	
							<div class=spacer10></div>
							<input type=checkbox name="ClearCheck" value="|2|"> Clear all current translations for this banner?							
						</div>
						<hr>
						<div class=spacer40>								
							<h3>Banner 3</h3>
							TEXT: <input style="width:700px;" class="input-xlarge" name="BannerText3" value="<%=BannerText3%>">
							<div class=spacer10></div>
							BUTTON TEXT: <input class=input name="ButtonText3" value="<%=ButtonText3%>">									
							<div class=spacer10></div>
							LINK: <input class=input name="BannerLink3" value="<%=BannerLink3%>">	
							<div class=spacer10></div>
							<input type=checkbox name="ClearCheck" value="|3|"> Clear all current translations for this banner?
								
						</div>						
					
					<div class="row submit">
					
						<div class="span8 right">
							<%
							if isjh() then
							%>
							
							<%
							end if
							%>
							<input class="btn btn-large btn-info" type="submit" value="SAVE">
							<input name="FormAction" class=FormHidden type="hidden" value="SAVE_BANNERS">							
						</div>
					</div>					
					
					
					
				</form>					
			
				
		<%
			end if
		end if
		%>
			
			</div>
			
            <div class="spacer40" id="AJR">
				
            </div>
        
		
    </div>

<!--#include virtual="/assets/includes/footer.asp" -->

<script>
	function ajaxSave (pLang, pid, pContent) {
		//alert(pid);
		//alert(pContent);
		
		$('#ajax_results_'+pid).html('<div style="padding:30px;"><img src="/assets/images/prodbrowser/indicator.gif"> <small>saving..</small></div>');
		//$('#ajax_prod_results').show();	
		
		$('.LabelResults').html('');
		
		var tt = document.getElementById('Translated_'+pid).value;
		//alert(tt)
		//alert(tt);
		tt = escape(tt) ;
		//alert(tt);
		//tt = escape(tt);
		
		//alert(pLang);
		
		$.ajax({
			url: "label_server.asp?TransText=" + tt,
			scriptCharset: "ISO-8859-1" ,
			contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",	
			data: {EditLanguageID: pLang, TransID: pContent},
			cache: false
			}).done(
				function( html ) {
				$('#ajax_results_'+pid).html(html);
				$('#ajax_results_'+pid).show('slow', function() {
					// Animation complete.
				  });		
			
			});	

	}
</script>