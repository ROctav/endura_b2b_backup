<%
PageTitle = "CMS Label Editor"

session.timeout = 900
%>

<!--#include virtual="/assets/includes/header.asp" -->

<%
MaxAttempts = 20
FormAction = request.form("FormAction")
'jh "FormAction = " & FormAction

if session("B2C_AdminUserID") = "" then
	dbClose()
	response.redirect "/"
end if
'jh "LoginAttempts = " & LoginAttempts


%>

        <div class="container">
            <div class="section_header">
                <h3>Label editor</h3>
            </div>
			
			<div class="row">
			
<%



	if ErrorsFound then
		jhErrorNice "Login failed", "You have entered an invalid username or password "
	end if
	
	dbConnect()
	
	EditLanguageID = cleannum(request("EditLanguageID"))
	'jh "EditLanguageID = " & EditLanguageID
	'EditLanguageID = 40	
	
	if FormAction = "SAxVE" then
		jhInfo "SAVING"
		RecCount = cdbl("" & request("RecCount"))
		jh "RecCount = " & RecCount
		for c = 0 to RecCount
			ThisText = request.form("Translated_" & c)
			ThisID = request.form("ID_" & c)
			sql = "DEL ETE FROM B2C_ContentTranslations WHERE LanguageID = 0" & EditLanguageID & " AND B2CContentID = 0" & ThisID & ""
			'jh sql
			
			if ThisText <> "" then
				sql = "INS ERT INTO B2C_ContentTranslations (B2CContentID, LanguageID, TranslatedText) VALUES ("
				sql = sql & "0" & ThisID & ", "
				sql = sql & "0" & EditLanguageID & ", "
				sql = sql & "'" & validstr(ThisText) & "') "
				jh sql
			end if	
		next
		jhInfoNice "Saving labels...", "Your changes have been saved!"
	end if
	
	
	sql = "SELECT LanguageID, LanguageName, LanguageISO FROM END_Language WHERE B2CActive = 1 AND LAnguageID > 0 ORDER BY LanguageID "
	'jh sql
	x = getrs(sql, EditLangArr, EditLangC)
	'jh "EditLangC = " & EditLangC
	
	SelEditLangName = "Select..."
	
	'jh session("B2C_AllowedLanguages")
	ThisAllowed = session("B2C_AllowedLanguages")	
	
	CheckCurrentLang = session("B2C_UserLanguageID") 
	if instr(1, ThisAllowed, "|" & CheckCurrentLang & "|") > 0 and EditLanguageID = "" and CheckCurrentLang <> "0" then
		EditLanguageID = CheckCurrentLang
	end if
	
	for x = 0 to EditLangC
		

		ThisLanguage = EditLangArr(0,x)	
		LanguageName = EditLangArr(1,x)
		ThisFlag = EditLangArr(2,x)
		
		'jh "ThisLanguage = " & ThisLanguage
	
		if instr(1, ThisAllowed, "|" & ThisLanguage & "|") > 0 then
			SomeAllowed = true
			langos = langos & "<li><a onclick="""" href=""label_editor.asp?EditLanguageID=" & ThisLanguage & """>" & LanguageName & "</a></li>"
			if "" & EditLangArr(0,x) = "" & EditLanguageID or EditLanguageID = "" then
				
				EditLanguageID = EditLangArr(0,x) 
				SelEditLangName = LanguageName
				'jhInfo "EditLanguageID = " & EditLanguageID
				EditLabel = ucase(LanguageName)
			end if
		end if		
		
	next
	

	if not SomeAllowed then
		jhErrorNice "Editing not allowed", "Your user account is not set up to translate any website content - contact Falk to gain access"
	else
	
%>			
						
				<for m name=loginForm action="label_editor.asp" method=post>
				
					<h3>Select a language</h3> 
						<div class="btn-group">	
							<button id="LabelLangSelectorButton" class="btn btn-small btn-warning dropdown-toggle" data-toggle="dropdown"><span><%=SelEditLangName%></span> <span class="caret"></span></button>
								 <ul class="dropdown-menu">
								 
									<%									
									=LangOS
									%>										
								  </ul>		
								</div>
				
					<table border=0>
					<%
					'jh "GET LABELS"
					

					
					
					sql = "SELECT bc.B2CContentID, bc.LookupName, bc.ContentText, tr.TranslatedText, bc.LabelDescription "
					sql = sql & "FROM B2C_Content bc "
					sql = sql & "LEFT JOIN (SELECT B2CContentID, TranslatedText FROM B2C_ContentTranslations WHERE LanguageID = 0" & EditLanguageID & ") tr  ON tr.B2CContentID = bc.B2CContentID "					
					sql = sql & "WHERE AdminEditable = 1 "
					'sql = sql & "AND ContentText LIKE '%story%' "
					sql = sql & "ORDER BY bc.ContentText"
					'jh sql
					x = getrs(sql, RecArr, rc)
					
					'jh "rc = " & rc
					
					for c = 0 to rc
						'jh RecArr(1,c) & " ____ <input value=""" & replace(RecArr(2,c), """", "&quot;") & """> ____ <input xvalue=""" & replace(RecArr(2,c), """", "&quot;") & """>"
						ThisB2CContentID = RecArr(0,c)
						ThisLN = RecArr(1,c)
						EnglishVersion = RecArr(2,c)
						TranslatedVersion = "" & RecArr(3,c)
						LabelDescription = "" & RecArr(4,c)
						
						if TranslatedVersion <> "" then labelStyle = "alert-success" else labelStyle = "alert-error"
						
						%>
						<tr id="la_<%=ThisB2CContentID%>" style="">
							<td style="padding-top:20px;">
								<div title="<%=LabelDescription%>" class="alert <%=labelStyle%>"><%=EnglishVersion%></div>								
							</td>
						</tr>
						</tr>
							<td valign=top>
								
								<textarea name="Translated_<%=c%>" id="Translated_<%=c%>" title="Enter the translated version of the label here" style="height:50px;width:600px;"><%=TranslatedVersion%></textarea><input name="ID_<%=c%>" class=FormHidden type="hidden" value="<%=ThisB2CContentID%>">			
								<div class=pull-right>
									<span class="label label-success">In <%=EditLabel%></span>
									<button title="<%=ThisLN%>" onclick="ajaxSave(<%=EditLanguageID%>, '<%=c%>', '<%=ThisB2CContentID%>');" id="SAYG_<%=c%>" class="btn btn-small btn-info">save</button>
									<div style="font-size:6pt;color:silver;"><%=ThisLN%></div>
								</div>
								<div class=LabelResults id="ajax_results_<%=c%>"></div>
							</td>							
						</tr>
						<%
					next
					
					%>
					</table>
					<div class="row submit">
					
						<div class="span3 right">
							<input class="btn btn-large btn-info" type="submit" value="SAVE">
							<input name="FormAction" class=FormHidden type="hidden" value="SAVE">
							<input name="RecCount" class=FormHidden type="hidden" value="<%=rc%>">
							<input name="EditLanguageID" class=FormHidden type="hidden" value="<%=EditLanguageID%>">
							
						</div>
					</div>					
					
					
					
				</form>					
			
				
		<%
		end if
		%>
			
			</div>
			
            <div class="spacer40" id="AJR">
				
            </div>
        
		
    </div>

<!--#include virtual="/assets/includes/footer.asp" -->

<script>
	function ajaxSave (pLang, pid, pContent) {
		//alert(pid);
		//alert(pContent);
		
		$('#ajax_results_'+pid).html('<div style="padding:30px;"><img src="/assets/images/prodbrowser/indicator.gif"> <small>saving..</small></div>');
		//$('#ajax_prod_results').show();	
		
		$('.LabelResults').html('');
		
		var tt = document.getElementById('Translated_'+pid).value;
		//alert(tt)
		//alert(tt);
		tt = escape(tt) ;
		//alert(tt);
		//tt = escape(tt);
		
		//alert(pLang);
		
		$.ajax({
			url: "label_server.asp?TransText=" + tt,
			scriptCharset: "ISO-8859-1" ,
			contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",	
			data: {EditLanguageID: pLang, TransID: pContent},
			cache: false
			}).done(
				function( html ) {
				$('#ajax_results_'+pid).html(html);
				$('#ajax_results_'+pid).show('slow', function() {
					// Animation complete.
				  });		
			
			});	

	}
</script>