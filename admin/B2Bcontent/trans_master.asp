<%
PageTitle = "Content source edit"

SuppressInlineEditor = true
%>

<!--#include virtual="/assets/includes/header.asp" -->



<style>
	.ReviewContainer A {
		color:red;
		[target=_blank];
	}
	
</style>

<%

SelectedLanguageID = cleannum(request("SelectedLanguageID"))

if SelectedLanguageID = "" then
	SelectedLanguageID = getSelectedLanguage()
end if

jhAdmin "SelectedLanguageID = " & SelectedLanguageID

dbConnect()

'jhInfo "GET CONTENT!"

LookupName = request("LookupName")

FormAction = request.form("FormAction")
if FormAction = "MULTI_CONTENT_SAVE" then
	
	AllowHTML = request("HTMLCheck")
	if AllowHTML <> "" then AllowHTML = 1 else AllowHTML = 0
	
	jhInfoNice "SAVING", "Content has been saved!"
	
	sql = "SELECT * FROM B2B_Content WHERE LookupName = '" & validstr(LookupName) & "'"
	jh sql
	set rs = db.execute(sql)
	if not rs.eof then		
		B2BContentID = rs("B2BContentID")
	else
		InsertRequired = true			
	end if
	rsClose()
	if InsertRequired then 
		sql = "INSERT INTO B2B_Content (LookupName, LabelEditable, AllowHTML)  values ('" & validstr(LookupName) & "', 1, 0);"
		jh sql
		db.execute(sql)
		sql = "SELECT MAX(B2BContentID) AS MaxRec FROM B2B_Content WHERE LookupName = '" & validstr(LookupName) & "' "
		jh sql
		set rs = db.execute(sql)
		if not rs.eof then
			B2BContentID = rs("MaxRec")
		end if
		rsClose()
	end if
	
	EnglishText = request("ContentText_0")
	
	sql = "UPDATE B2B_Content SET "
	sql = sql & "ContentText = '" & validstr(EnglishText) & "', "
	sql = sql & "AllowHTML = 0" & AllowHTML & " "
	
	sql = sql & "WHERE LookupName = '" & validstr(LookupName) & "' "
		
	jh sql
	db.execute(sql)
	
	RecCount = cdbl("0" & request("RecCount"))
	
	for c = 1 to RecCount
		ThisContent = request("ContentText_" & c)
		ThisLanguageID = request("LanguageID_" & c)
		
		sql = "DELETE FROM B2B_ContentTranslations WHERE LanguageID = 0" & ThisLanguageID & " AND B2BContentID = 0" & B2BContentID
		jh sql
		db.execute(sql)
		
		if ThisContent <> "" then
			sql = "INSERT INTO B2B_ContentTranslations (B2BContentID, LanguageID, TranslatedText) VALUES (0" & B2BContentID & ", 0" & ThisLanguageID & ", '" & validstr(ThisContent) & "' )"
			jh sql
			db.execute(sql)
		end if
	next
	
end if

dbConnect()


if FormAction <> "" then

else
	if LookupName <> "" then
	
		sql = "SELECT b.B2BContentID, b.LookupName, b.ContentText, t.LanguageID, t.TranslatedText "		
		sql = sql & "FROM B2B_Content b LEFT JOIN B2B_ContentTranslations t ON b.B2BContentID = t.B2BContentID "
		sql = sql & "WHERE b.LookupName = '" & validstr(LookupName) & "' "
		
		jh sql
		
		x = getrs(sql, tArr, tc)
		jh "tc = " & tc
		
		if tc>= 0 then FoundLookupName = tArr(1,0)
		
	else
		tc = -1
	end if
end if

sql = "SELECT * FROM END_Language WHERE B2BActive = 1 ORDER BY LanguageID"
x = getrs(sql, RecArr, rc)

ContentEditPage = true

%>

    <div id="features" class="features_page">
        <div class="container">
            
            <div id=AboutHistory class="section_header">
                <h3><%=PageTitle%></h3>
				<p>Enter the edited content in the editor below, press save once complete</p>	
			
				<div class=clearfix></div>					
            </div>				

<%
'jh "RICH TEXT?"
%>	
			
			<hr>
			
			<form action="trans_master.asp" method=post name=ContentEditForm id=ContentEditForm>	
			<div class=row>
				
				<h4>lookup_name<h4>
				<div><input type=text class=form-control name="LookupName" value="<%=LookupName%>"></div>
				<div><input type=checkbox name="HTMLCheck"> Allow HTML?</div>
			
<%
				

				for c = 0 to rc
					ThisLanguageID = RecArr(0,c)
					ThisLanguageName = RecArr(1,c)	
					
					ThisContentText = ""
					
					if FormAction = "" then
						for x = 0 to tc
							if "" & tArr(3,x) = "" & ThisLanguageID or (c = 0 and "" & tArr(3,x) = "") then
								if "" & ThisLanguageID = "0" then
									ThisContentText = tArr(2,x)
									jh "ThisContentText = " & ThisContentText
								else
									ThisContentText = tArr(4,x)
								end if
								exit for
							end if
						next
					else
						ThisContentText = request("ContentText_" & c)
					end if
				%>
					<hr>
					<h3><%=ThisLanguageName%></h3>
					<textarea style="width:900px;height:100px;" name="ContentText_<%=c%>"><%=ThisContentText%></textarea>
					<input type=chidden class=FormHidden name="LanguageID_<%=c%>" value="<%=ThisLanguageID%>">
				<%
				next
%>			
			
			
				<div class=<%=DispContentCols%>>
				
					<div>&nbsp;</div>
					<input name=SubmitButton type=submit class="btn-large btn-primary" value="Save changes">
					<input type=hidden class=FormHidden name="RecCount" value="<%=rc%>">
					<input type=hidden class=FormHidden name="FoundLookupName" value="<%=FoundLookupName%>">
					<input type=hidden class=FormHidden name="FormAction" value="MULTI_CONTENT_SAVE">
						
				</div>
			</div>
			</form>

			
		<div class="spacer40">&nbsp;</div>
        </div>
    </div>

<!--#include virtual="/assets/includes/footer.asp" -->

<%
if AllowHTML then
	'jh "AllowHTML = " & AllowHTML
%>

<script type="text/javascript">
   window.onload = function()
   {
      CKEDITOR.replace( 'ContentHolder', {
                  height:'600px',width:'900px',                   
					allowedContent: true,
					contentsCss: ['/css/bootstrap.css', '/css/theme.css']
					}
					
				  );
	  
     // CKEDITOR.instances.ContentHolder.on('blur', function() {
         //alert('onblur fired');
      //});
   };
   
	$('#btnCopyEnglish').click(function() {
		var SetValue = $("#EnglishVersion").html();
		
		//editor.getData();
		//alert(SetValue)
		CKEDITOR.instances.ContentHolder.setData(SetValue);
		// send your ajax request with value
		// profit!
		return false;
	});
   
</script>

<%
end if
%>