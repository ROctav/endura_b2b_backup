<%
PageTitle = "Content source edit"

SuppressInlineEditor = true
%>

<!--#include virtual="/assets/includes/header.asp" -->



<style>
	.ReviewContainer A {
		color:red;
		[target=_blank];
	}
	
</style>

<%

SelectedLanguageID = cleannum(request("SelectedLanguageID"))

if SelectedLanguageID = "" then
	SelectedLanguageID = getSelectedLanguage()
end if

jhAdmin "SelectedLanguageID = " & SelectedLanguageID

dbConnect()

'jhInfo "GET CONTENT!"

content_lookup = request("content_lookup")
ContentReturnURL = request("ContentReturnURL")
'jh "content_lookup = " & content_lookup
'jh "ReturnURL = " & ReturnURL
'jh "Selected Language = " & SelectedLanguageID

BackTo = request("BackTo")
BackToID = request("BackToID")

ContentText = "[NOT ENTERED YET]"
EditMode = "EDIT"

FormAction = request.form("FormAction")
if FormAction = "ADMIN_CONTENT_SAVE" then
	
	jhInfoNice "SAVING", "Content has been saved!"
	
end if

dbConnect()

sql = "SELECT B2BContentID, ContentText, AllowHTML, ContentCols FROM B2B_Content WHERE LookupName = '" & validstr(content_lookup) & "'"
'jh sql
set rs = db.execute(sql)
if not rs.eof then 
	B2BContentID = rs("B2BContentID")
	ContentText = rs("ContentText")
	EnglishVersion = ContentText
	AllowHTML = rs("AllowHTML")
	
	ContentCols = cdbl("0" & rs("ContentCols"))
	if ContentCols = 0 then DispContentCols = "span12" else DispContentCols = "span" & ContentCols
	
	jhAdmin "AllowHTML = " & AllowHTML
	
end if
rsClose()

if SelectedLanguageID <> "0" then

	sql = "SELECT LanguageName FROM END_Language WHERE LanguageID = 0" & SelectedLanguageID & ""
	set rs = db.execute(sql)
	if not rs.eof then
		SelLanguageName = rs("LanguageName")
		jhAdmin SelLanguageName
	end if	
	rsClose()

	sql = "SELECT TOP 1 TranslatedText FROM B2B_ContentTranslations WHERE B2BContentID = 0" & B2BContentID & " AND LanguageID = 0" & SelectedLanguageID & " ORDER BY B2BContentTranslationID DESC"
	jhAdmin sql
	set rs = db.execute(sql)
	if not rs.eof then
		TranslatedText = "" & rs("TranslatedText")
	end if
	rsClose()
	if TranslatedText <> "" then ContentText = TranslatedText else ContentText = ""
else
	SelLanguageName = "English"
end if

ContentEditPage = true

%>

    <div id="features" class="features_page">
        <div class="container">
            
            <div id=AboutHistory class="section_header">
                <h3><%=PageTitle%></h3>
				<p>Enter the edited content in the editor below, press save once complete</p>	
			
				<div class=clearfix></div>					
            </div>				
<%
if SelectedLanguageID <> "0" then
%>
			<h4>ENGLISH</h3>
			<div class=well>
				<strong></strong>
				<div id="EnglishVersion"><%=EnglishVersion%></div>
			</div>
			
			<a href="#" id=btnCopyEnglish class="btn-success btn btn-small">Copy english version to editor</a>
<%
end if
%>		
			<small class="subtle pull-right"><%=content_lookup%></small>
<%
'jh "RICH TEXT?"
%>	
			
			<hr>
			
			<form action="admin_actions.asp" method=post name=ContentEditForm id=ContentEditForm>	
			<div class=row>
				<div class=<%=DispContentCols%>>
					<h4><%=ucase(SelLanguageName)%></h3>
					<textarea name="ContentHolder" id="ContentHolder" style="width:900px;height:500px;"><%=ContentText%></textarea>
					
					<div>&nbsp;</div>
					<input name=SubmitButton type=submit class="btn-large btn-primary" value="Save changes"> <input type=submit name=SubmitButton class="btn-large btn-primary" value="Cancel"> 
					<input type=hidden class=FormHidden name="ContentLookup" value="<%=content_lookup%>">
					<input type=hidden class=FormHidden name="ContentID" value="<%=B2BContentID%>">
					<input type=hidden class=FormHidden name="ContentLanguageID" value="<%=SelectedLanguageID%>">					
					<input type=hidden class=FormHidden name="FormAction" value="SAVE">					
					<input type=hidden class=FormHidden name="ContentReturnURL" value="<%=ContentReturnURL%>">
					
					<input type=hidden class=FormHidden name="ContentOriginal" value="<%=XXX_ContentText%>">
					<input type=hidden class=FormHidden name="BackTo" value="<%=BackTo%>">
					<input type=hidden class=FormHidden name="BackToID" value="<%=BackToID%>">
					
				</div>
			</div>
			</form>

			
		<div class="spacer40">&nbsp;</div>
        </div>
    </div>

<!--#include virtual="/assets/includes/footer.asp" -->

<%
if AllowHTML then
	'jh "AllowHTML = " & AllowHTML
%>

<script type="text/javascript">
   window.onload = function()
   {
      CKEDITOR.replace( 'ContentHolder', {
                  height:'600px',width:'900px',                   
					allowedContent: true,
					contentsCss: ['/css/bootstrap.css', '/css/theme.css']
					}
					
				  );
	  
     // CKEDITOR.instances.ContentHolder.on('blur', function() {
         //alert('onblur fired');
      //});
   };
   
	$('#btnCopyEnglish').click(function() {
		var SetValue = $("#EnglishVersion").html();
		
		//editor.getData();
		//alert(SetValue)
		CKEDITOR.instances.ContentHolder.setData(SetValue);
		// send your ajax request with value
		// profit!
		return false;
	});
   
</script>

<%
end if
%>