<!--#include virtual="/assets/includes/global_functions.asp" -->

<%


jhInfo "SAVING CONTENT"

set AdminDetails = new AdminAccountDetails

jh "CHECK LOGIN VALIDATION"

if not isAdminUser() then
	dbClose()
	response.redirect "/"
end if

jh "GET LANGUAGE"

ContentLanguageID = cdbl("0" & request("ContentLanguageID"))
jh "ContentLanguageID = " & ContentLanguageID

jh "GET VALUES"

FormAction = request("FormAction")
SubmitButton = ucase("" & request("SubmitButton"))

jh "FormAction = " & FormAction
jh "SubmitButton = " & SubmitButton

BackTo = request("BackTo")
BackToID = request("BackToID")

if FormAction = "SAVE" and SubmitButton <> "CANCEL" then
	dbConnect()

	ThisID = cleannum(request("ContentID"))
	ThisLookup = request("ContentLookup")
	if ThisID = "" then ThisID = 0
	ThisText = request("ContentHolder")
	ThisOrig = request("ContentOriginal")
	
	if ThisText <> ThisOrig or SubmitButton = "SAVE CHANGES" then
		jh "CHANGED - " & ThisID
		jh ThisText
		
		sql = "exec [spB2BContentChange] " & ThisID & ", '" & validstr(ThisLookup) & "','" & validstr(ThisText) & "', '" & validstr(ThisOrig) & "', 0" & ContentLanguageID & ", 0" & ThisUserID
		
		jhAdmin replace(sql, "<", "&lt;")
		db.execute(sql)
		
		
	end if

	'jhstop
	
	dbClose()
	returnStr = "ContentSaved=1"
elseif FormAction = "CANCEL" or SubmitButton = "CANCEL" then
	returnStr = "ContentCancelled=1"
end if

jh "GET RETURN URL"

if BackTo = "LABEL_EDITOR" then 
	ReturnURL = "/admin/b2bcontent/label_editor.asp?EditLanguageID=" & ContentLanguageID & "#" & BackToID
else
	ReturnURL = request("ContentReturnURL")
	ReturnURL = replace(ReturnURL, "ContentSaved=1", "")
	ReturnURL = replace(ReturnURL, "ContentCancelled=1", "")
	ReturnURL = replace(ReturnURL, "?&", "?")
	ReturnURL = replace(ReturnURL, "&&", "")	
end if

if ReturnURL = "" then 
	'jh "NO URL"
	'xxx
	ReturnURL = "/"
end if

if returnStr <> "" and BackTo = "" then
	if instr(1, ReturnURL, "?") = 0 then ReturnURL = ReturnURL & "?" & returnStr else ReturnURL = ReturnURL & "&" & returnStr	
end if

'jhstop

response.redirect ReturnURL 

jhInfo "ENDS"
%>