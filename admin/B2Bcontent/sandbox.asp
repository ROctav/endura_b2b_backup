<%
PageTitle = "Content test area"

SuppressInlineEditor = true
%>

<!--#include virtual="/assets/includes/header.asp" -->



<style>
	.ReviewContainer A {
		color:red;
		[target=_blank];
	}
	
</style>

<%

jhAdmin "SelectedLanguageID = " & SelectedLanguageID

if session("B2C_AdminUserID") <> "" and canEdit(session("B2C_AdminUserID"), SelectedLanguageID) then
	
else
	dbClose()
	response.redirect "/"
end if


dbConnect()

'jhInfo "GET CONTENT!"

content_lookup = request("content_lookup")
ContentReturnURL = request("ContentReturnURL")
'jh "content_lookup = " & content_lookup
'jh "ReturnURL = " & ReturnURL
'jh "Selected Language = " & SelectedLanguageID

ContentText = ""
EditMode = "EDIT"

FormAction = request.form("FormAction")
if FormAction = "ADMIN_CONTENT_PREVIEW" then
	jhInfoNice "PREVIEW", "Content has been modified, check below"
	SourceContent = request("SourceContent")
end if

dbConnect()

ContentEditPage = true

LoadFrom = request("LoadFrom")
jhAdmin "LoadFrom = " & LoadFrom

select case LoadFrom 

	case "HOME_PANEL"
		SourceContent = "home_page_feature"
	

end select

if FormAction = "ADMIN_CONTENT_PREVIEW" then
	jhInfo "GET SANDBOX!"
	ContentText = request.form("ContentText")
elseif SourceContent <> "" then
	jhAdmin "GET SOURCE CONTENT - " & SourceContent
	'jhstop
	
	dbConnect()
	
	if FormAction = "" then 
		sql = "SELECT ContentText FROM B2C_Content WHERE LookupName = '" & validstr(SourceContent) & "'"
		set rs = db.execute(sql)
		
		if not rs.eof then
			ContentText = rs("ContentText")
		end if
		
		rsclose()

	end if
	
end if

%>

    <div id="features" class="features_page">
        <div class="container">
            
            <div id=AboutHistory class="section_header">
                <h3><%=PageTitle%></h3>
				<p>Enter test content in the editor below, press preview to see changes</p>	
				
				<div class=clearfix></div>					
            </div>				
<%
if SelectedLanguageID <> "0" then
%>
			<h4>ENGLISH</h3>
			<div class=well>
				<strong></strong>
				<%=EnglishVersion%>
			</div>
<%
end if
%>			
			
			<form action="sandbox.asp" method=post name=ContentEditForm id=ContentEditForm>	
			<div class=row>
				<div class=span12>
				
					<textarea name="ContentText" id="ContentText" style="width:900px;height:500px;"><%=ContentText%></textarea>
					
					<div>&nbsp;</div>
					<input name=SubmitButton type=submit class="btn-large btn-primary" value="Preview changes">
					
					<input type=hidden class=FormHidden name="ContentLookup_1" value="<%=content_lookup%>">
					<input type=hidden class=FormHidden name="ContentID_1" value="<%=B2CContentID%>">
					<input type=hidden class=FormHidden name="ContentLanguageID" value="<%=SelectedLanguageID%>">					
					<input type=hidden class=FormHidden name="FormAction" value="ADMIN_CONTENT_PREVIEW">
					<input type=hidden class=FormHidden name="ContentEditCount" value="1">
					<input type=hidden class=FormHidden name="ContentReturnURL" value="<%=ContentReturnURL%>">
					<input type=hidden class=FormHidden name="SourceContent" value="<%=SourceContent%>">
				</div>
			</div>
			</form>
		
			<hr>
			<div class="row">	
				<div class="span12">	
				<%
				=ContentText
				%>
				</div>
			</div>
			<hr>
			
		<div class="spacer40">&nbsp;</div>
        </div>
    </div>

<!--#include virtual="/assets/includes/footer.asp" -->

<script type="text/javascript">
   window.onload = function()
   {
      CKEDITOR.replace( 'ContentText', {
                  height:'400px',width:'900px',                   
					allowedContent: true
					}
				  );
	  
     // CKEDITOR.instances.ContentHolder_1.on('blur', function() {
         //alert('onblur fired');
      //});
   };
</script>