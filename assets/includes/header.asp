
<!--#include virtual="/assets/includes/global_functions.asp" -->

<%

public acDetails

ThisUserID = session("B2B_AcCode")
set acDetails = new DealerAccountDetails
set AdminDetails = new AdminAccountDetails

if not isLoggedIn() and not NoRedirect then
	jh "REDIRECT"
	dbClose()
	response.redirect "/login/"
elseif isAdminPage() and not isAdminUser() then
	jhError "ADMIN PAGE - NOT LOGGED IN"
	dbClose()
	response.redirect "/"	
end if

alang = cleannum(request("alang"))
if alang <> "" and alang <> "0" then	
	'PART OF THE AUTO LOGIN FROM OLD B2B FACILITY!
	call setSelectedLanguageID(alang)
	'jhx "SelectedLanguage SET! (" & alang & ")"
end if

SelectedLanguageID = getSelectedLanguage()

if request("action") = "TOGGLE_EDIT" and session("B2B_AdminUserID") <> "" then
	'jh "TOGGLE EDIT"
	if session("B2B_EditMode") = "EDIT" then 
		session("B2B_EditMode") = "" 
		'returnStr = "EditMode=OFF"
	else
		session("B2B_EditMode") = "EDIT" 
		'returnStr = "EditMode=ON"		
	end if	
	dbClose()
	response.redirect getCurrentPage() & "?actioncomplete=TOGGLE_EDIT"
	'jh "EditMode = " & EditMode
end if

if session("B2B_AdminUserID") <> "" then
	if request("TagToggle") = "TOGGLE" then
		if session("B2B_LabelTags") <> "" then session("B2B_LabelTags") = "" else session("B2B_LabelTags") = "ON"
	end if	
end if

session.lcid = 2057

TogglePricing = request("TogglePricing")
if TogglePricing = "OFF" then
	session("B2B_SuppressPricing") = "1"
elseif TogglePricing = "ON" then
	session("B2B_SuppressPricing") = ""
end if

ProductSearchCurrent = request("ProductSearchCurrent")
If (ProductSearchCurrent <> "") Then 
	if ProductSearchCurrent = "OFF" then
		session("B2B_ProductSearchCurrent") = "0"
	elseif ProductSearchCurrent = "ON" then
		session("B2B_ProductSearchCurrent") = "1"
	end if

End If

ProductSearchClearance = request("ProductSearchClearance")
If (ProductSearchClearance <> "") Then 
	if ProductSearchClearance = "OFF" then
		session("B2B_ProductSearchClearance") = ""
	elseif ProductSearchClearance = "ON" then
		session("B2B_ProductSearchClearance") = "1"
	end if

End If

ProductSearchFuture = request("ProductSearchFuture")
If (ProductSearchFuture <> "") Then 
	if ProductSearchFuture = "OFF" then
		session("B2B_ProductSearchFuture") = ""
	elseif ProductSearchFuture = "ON" then
		session("B2B_ProductSearchFuture") = "1"
	end if
End If


Dim BBLDealerStatus, BBLRangeID
BBLDealerStatus = acDetails.BBLDealerStatus
BBLRangeID = 27

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	
    <meta http-equiv="Content-Type" content="text/html;">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	
	<!-- Styles -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="/css/bootstrap-overrides.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/theme.css">
    <link href='https://fonts.googleapis.com/css?family=Oxygen:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css" href="/css/lib/animate.css" media="screen, projection">
    
    <link rel="stylesheet" href="/css/lib/flexslider.css" type="text/css" media="screen" />
	
	<link rel="stylesheet" href="/css/jquery-ui.css" />
	
	<link rel="stylesheet" href="/css/b2b.css" type="text/css" media="screen" />

	<link rel="SHORTCUT ICON" href="/favicon.ico"/>
	<link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico" />	
	
	<link rel="shortcut icon" href="/favicon.ico" />
	<link rel="apple-touch-icon" href="/Icon-76@2x.png" />
	
    <!--[if lt IE 9]>
      <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
	

	<script src="/admin/ckeditor/ckeditor.js"></script>
	<script src="/admin/ckeditor/config.js"></script>
	
	<script src="/admin/ckeditor/adapters/jquery.js"></script>
		
	
	
	<script src="/js/jquery-1.10.2.js"></script>
	<script src="/js/jquery-ui.js"></script>

    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/theme.js"></script>
    <script type="text/javascript" src="/js/flexslider.js"></script>
	<script type="text/javascript" src="/assets/js/jquery.hotkeys.js"></script>	
	
	<script src='/assets/elevatezoom-master/jquery.elevateZoom-3.0.8.min.js'></script>
	
	<script src="/js/lazyload/jquery.lazyload.min.js" type="text/javascript"></script>
	
    <script type="text/javascript" src="/js/index-slider.js"></script>	

	
<%

ThisPageName = request.ServerVariables("SCRIPT_NAME")
DispPageTitle = glWebsiteName & " - " & PageTitle
'jhAdmin "ThisPageName= " & ThisPageName

%>	
	
	<title><%=DispPageTitle%></title>
	
	<meta name="description" content="<%=MetaDescriptionStr%>" />
	
	<style>
		.promoHolder, .promoHolder H4 {
			color: Crimson !important;
		}
		.promoHolder A {
			color: Crimson !important;
		}	
	</style>    

     <style>
    /* Main Slider
-------------------------------------------------- */
#feature_slider {
    height:720px;
    height: 380px;
    background: #262626;
    z-index: 1;
    margin-bottom: 40px;
}
#feature_slider h1 {
    font-weight: 100;
    color: #ccc;
    font-size: 60px;
    line-height: 66px;
    text-align: center;
    position: absolute;
    top: 180px;
    width: 100%;
}
#feature_slider h1 strong {
    text-transform: uppercase;
    font-size: 14px;
    margin: 0 0 20px;
}
#pagination {
    position: absolute;
    top: 505px;
    z-index: 3;
    margin: 0;
    padding: 0;
    display: none;
    list-style-type: none;
}
#pagination li {
    text-indent: -99999em;
    float: left;
    padding: 0 6px;
    cursor: pointer;
	display:none;
}
#pagination li a {
    width: 7px;
    height: 7px;
    box-shadow: 0px 0px 1px 2px #ccc;
    background: rgb(0, 0, 0);
    border-radius: 10px;
    display: block;
    /*border: 1px solid #fff;*/
    opacity: .7;
    -webkit-transition: opacity .1s linear;
    -moz-transition: opacity .1s linear;
    transition: opacity .1s linear;
}
#pagination li.active a {
    background: rgb(46, 92, 177);
    box-shadow: none;
    width: 8px;
    height: 8px;
}

#pagination li:hover a {
    opacity: 1;
}
.slide {
    width: 100%;
    /*height: 720px;*/
    height: 380px;
    position: absolute;
    display: none;
    overflow: hidden;
    z-index: 1;
    -webkit-background-size: cover !important;
       -moz-background-size: cover !important;
         -o-background-size: cover !important;
            background-size: cover !important;
}
.slide.previous {
    z-index: 1;
}
.slide.active {
    z-index: 2;
}
.slide.hiddden {
    display: none;
}

/* Custom styles for each slide */

/* Slide 1 */
.slide#showcasing .info {
    position: absolute;
    top: 160px;
    left: 13%;
    width: 30%;
    display: none;
    z-index: 10;
}
.slide#showcasing .info h2 {
    color: #fff;
    font-size: 50px;
    font-weight: normal;
    line-height: 56px;
    text-shadow: 1px 1px 1px rgb(49, 57, 61);
}
/* Slide 2 */
.slide#ideas .info {
    position: relative;
    top: 110px;
    text-align: center;
}
.slide#ideas .info h2 {
    color: #fff;
    font-weight: normal;
}

/* Slide 3 */
.slide#tour .info {
    position: absolute;
    top: 150px;
    right: 6%;
    width: 24%;
}
.slide#tour .info h2 {
    color: #fff;
    font-size: 45px;
    font-weight: normal;
    text-shadow: 1px 1px 1px #3b3262;
    line-height: 57px;
}
.slide#tour .info a {
    color: #fff;
    font-weight: bold;
    background-color: #2a205d;
    padding: 13px 30px;
    font-size: 19px;
    border-radius: 5px;
    margin-top: 40px;
    display: inline-block;
    -webkit-transition: opacity .1s linear;
    -moz-transition: opacity .1s linear;
    transition: opacity .1s linear;
}
.slide#tour .info a:hover { }

/* Slide 4 */
.slide#responsive .info {
    position: absolute;
    top: 170px;
    right: 9%;
    width: 25%;
}
.slide#responsive .info h2 {
    color: #fff;
    font-size: 47px;
    font-weight: normal;
    text-shadow: 1px 1px 1px #3b3262;
    line-height: 56px;
}
.slide#responsive .info h2 strong {
    font-size: 42px;
}

.slide .info h2 { }
.slide .info {
    display: none;
    z-index: 10;
}
.slide .info a:hover {
    opacity: .8;
}
#slide-left,
#slide-right {
    width: 31px;
    height: 37px;
    display: block;
    position: absolute;
    top: 150px;
    left: 0;
    background: url(/img/leftright-arrows.png) no-repeat;
    opacity: .5;
    -webkit-transition: opacity .1s linear;
    -moz-transition: opacity .1s linear;
    transition: opacity .1s linear;
    display: none;
    z-index: 2;
}
#slide-right {
    left: auto;
    right: 0;
    background-position: -31px 0;
}
#slide-left:hover,
#slide-right:hover {
    opacity: 1;
}

    </style>   
</head>




<%


public CurrentNavLevel, PageC, PageArr

'NavStr = topPageNav()
'jh NavStr

CurrentURL = getCurrentURL()

'jhAdmin "CurrentURL = " & CurrentURL

dbConnect()

if glMultiLang then

	sql = "SELECT LanguageID, ISNULL(LanguageName_NATIVE, LanguageName) AS LanguageName, LanguageISO FROM END_Language "
	if isAdminUser() or session("test_lang") = "1" then
		sql = sql & "WHERE B2BEditable = 1 "
	else
		sql = sql & "WHERE B2BActive = 1 "
	end if
	sql = sql & "ORDER BY LanguageID "

	'jhAdmin sql

	x = getrs(sql, LangArr, LangC)

	'jh "CountryC = " & CountryC
	set LocLS = new strconcatstream
	'jh "SelectedLanguageISO = " & SelectedLanguageISO


	for x = 0 to LangC
		ThisLanguage = LangArr(0,x)	
		LanguageName = LangArr(1,x)
		ThisFlag = LangArr(2,x)
		'jh "ThisFlag = " & ThisFlag
		if "" & ThisLanguage = "" & SelectedLanguageID then 
			SelStr = "SELECTED" 
			LanguageFlag = ThisFlag
			SelectedLanguageName = LanguageName
		else 
			SelStr = ""
		end if
		LocLS.add "<option " & SelStr & " value=""" & ThisLanguage & """>" & LanguageName & "</option>"
	next
	LangDDOS = LocLS.value

end if

dbConnect()

flagloc = server.mappath("/assets/images/icons/country/") & "\"
	if OrderThreshold = 0 then OrderThreshold = glOrderThreshold

	if acDetails.AvailableCredit <= 100 then 
		AvailableCreditClass = "badge-important"
	elseif acDetails.AvailableCredit <= 200 then 
		AvailableCreditClass = "badge-warning"
	else
		AvailableCreditClass = "badge-inverse"
	end if

	if glClearanceListActive then 
		'jhAdmin "GET NEW CLEARANCE LIST ITEMS "
		sql = "[spB2BClearanceList_CHECK] "
		'jhAdmin sql
		set rs = db.execute(sql)
		
		if not rs.eof then
			NewClearanceCount = rs("NewClearanceCount")
			if NewClearanceCount > 0 then 
				NewClearanceItems = true
			end if
		end if
		
		rsClose()
	end if

	bodystyle = ""
	showpromo = cleannum(request("ShowPromo"))

	if showpromo = "1" then session("showpromo") = "1"
	if showpromo = "0" then session("showpromo") = ""

	if (session("showpromo") = "1" or glPromoActive) and glPromoBG <> "" and isLoggedIn() and not isjh() then

%>
		<style>
		
			@media (min-width: 1600px) {
			
				BODY {
					background-image:url(/assets/images/promo/<%=glPromoBG%>);background-position:top center;background-repeat:no-repeat;
				}
				.containerStyle {
					background:#fff;
				}
			
			}
			
		</style>
<%
	
	end if	
	
%>

<body style="<%=bodystyle%>">	
    <div class="navbar navbar-inverse navbar-static-top hidden-print">
      <div class="navbar-inner">
        <div class="container" style="">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="brand" href="/">
                <img style="width:140px;" src="/assets/images/furniture/nav_logo.png">
            </a>
            <div class="nav-collapse collapse">
                                
				<%=NavStr%>	<!--DB NAV ENABLED-->			
				
				<ul class="nav pull-right">

				
<%
if isDealer() then

					if session("B2B_ProductSearchCurrent") = "1" then
						CurrentSuppressClass = "badge-success"
						CurrentSuppressTitle = "Search of Current products is currently switched ON, click to switch off"
						CurrentOption = "OFF"
					else

					CurrentSuppressClass = "badge-important"
						CurrentSuppressTitle = "Search of Current products is currently switched OFF, click to switch on"
						CurrentOption = "ON"

					end if

					if session("B2B_ProductSearchClearance") = "1" then

					ClearanceSuppressClass = "badge-success"
						ClearanceSuppressTitle = "Search of Clearance products is currently switched ON, click to switch off"
						ClearanceOption = "OFF"

					else
						ClearanceSuppressClass = "badge-important"
						ClearanceSuppressTitle = "Search of Clearance products is currently switched OFF, click to switch on"
						ClearanceOption = "ON"
					end if

					if session("B2B_ProductSearchFuture") = "1" then

					FutureSuppressClass = "badge-success"
						FutureSuppressTitle = "Search of Future products is currently switched ON, click to switch off"
						FutureOption = "OFF"

					else
						FutureSuppressClass = "badge-important"
						FutureSuppressTitle = "Search of Future products is currently switched OFF, click to switch on"
						FutureOption = "ON"
					end if
					if session("B2B_SuppressPricing") = "1" then
						SuppressClass = "badge-important"
						SuppressStr = acDetails.CurrencySymbol & " <i class=""icon-remove icon-white""></i>"
						SuppressTitle = "Displayed pricing is currently switched OFF, click to switch on"
						ToogleOption = "ON"
					else
						SuppressClass = "badge-success"
						SuppressStr = acDetails.CurrencySymbol & " <i class=""icon-ok icon-white""></i>"
						SuppressTitle = "Displayed pricing is currently switched ON, click to switch off"
						ToogleOption = "OFF"
					end if

%>
						<li>
						  <form id=HeaderQuickSearchForm method=post action="<%=SearchAction%>">											 										
							  <i class="icon-search icon-white"></i>&nbsp;<input autocomplete=off class=HeaderSearchInput maxlength=30 name="searchVal_HEADER" id="searchVal_HEADER" placeholder="<%=lne_NOEDIT("header_search_for")%>" type="text" xclass="input-large search-query" style="font-size:9pt !important;width:120px;padding:5px; margin-right: 10px;" value="<%=CurrentSearch%>" >
						<span style="color: #777E83; font-size: smaller; font-weight: bold;"><%=lne_NOEDIT("product_search")%>:</span>
						</form>
						</li>

					<li class="dropdown">
						<a  title="<%=lne_NOEDIT("product_search_current")%>&nbsp;-&nbsp;<%=CurrentSuppressTitle%>" href="/?ProductSearchCurrent=<%=CurrentOption%>">	
							<span class="badge <%=CurrentSuppressClass%>" ><i class="icon-white icon-ok-circle"></i></span>
						</a>
					</li>	            
						
					<li class="dropdown">
						<a  title="<%=lne_NOEDIT("label_clearance")%>&nbsp;-&nbsp;<%=ClearanceSuppressTitle%>" href="/?ProductSearchClearance=<%=ClearanceOption%>">	
							<span class="badge <%=ClearanceSuppressClass%>" ><i class="icon-white icon-fire"></i></span>
						</a>
					</li>	            

					<li class="dropdown">
						<a  title="<%=lne_NOEDIT("product_search_future")%>&nbsp;-&nbsp;<%=FutureSuppressTitle%>" href="/?ProductSearchFuture=<%=FutureOption%>">	
							<span class="badge <%=FutureSuppressClass%>" ><i class="icon-white icon-time"></i></span>
						</a>
					</li>

				         

					
<%
end if

if glMultiLang then
	if SelectedLocation = "XX" then DispSelectedLocation = "Rest of World" else DispSelectedLocation = SelectedLocation

%>
                    
					<li class="dropdown">
                        <a title="<%=lne("label_select_language")%>" href="#" class="dropdown-toggle" data-toggle="dropdown">
							
							<img style="border:1px solid white" src="/assets/images/icons/country/<%=LanguageFlag%>.png">
                            <b class="caret"></b></a>
							<ul class="dropdown-menu" style="width:200px;font-size:9pt;">
								<li class="dropdown">            
									<div style="color:white;padding: 20px 5px 0 5px;">	
										<form id=LocationOptionForm method=post action="/set_options.asp">											
											<i title="Select a language" class="icon-align-left icon-white"></i> <select onchange="document.forms.LocationOptionForm.submit()" title="Select a language" name="SetLanguageID" style="font-size:9pt;width:170px;"><%=LangDDOS%></select>									
											
											<input class=FormHidden type=hidden name="ReturnURL" value="<%=CurrentURL%>">											
										</form>
									</div>
								</li>		
								
							</ul>							
                        </a>
					</li>  	
					
<%
end if

if isLoggedIn() and not AcDetails.RiderCard then
%>					
					
					<li class="dropdown">
						<a id=ProdSearchDD title="<%=lne("label_your_account_code")%>" href="/utilities/account/" xclass="dropdown-toggle" xdata-toggle="dropdown">	
							<span class="badge badge-white" style="color:white;"><%=ThisUserID%></span>
						</a>
					</li>			
<%
end if

if isDealer() then
%>					
                    <li class="dropdown">
                        <a id=ProdSearchDD href="#" class="dropdown-toggle" data-toggle="dropdown">							
                            <i class="icon-shopping-cart icon-white"> </i> <span id="BasketCount" class="basketCountHolder badge badge-success">...</span> <b class="caret"></b></a>
							<ul class="dropdown-menu" style="width:350px;font-size:9pt;background:#f0f0f0;">
								<li class="dropdown">            
									<div style="color:#333;padding: 20px 5px 0 20px;">	
													
									  <div id=BasketTop class="text-right" style="padding:10px;">
											...
									  </div>
										
									</div>
								</li>		
								
							</ul>							
                        </a>
					</li>  			

<%
end if

if isLoggedIn() then
%>					
					
					<li class="dropdown">
						<a id=LogoutLink title="<%=lne("label_logout")%>" href="/login/?formaction=LOGOUT">	
							<span class="badge badge-important" style="color:white;"><i class="icon-white icon-share"></i></span>
						</a>
					</li>	               
			   
<%
end if

if isAdminUser then
%>
					<li class="dropdown">
						<a id=AdminLink title="ADMIN LOGGED IN AS <%=AdminDetails.UserName%>" href="/admin/control/">	
							<span class="label label-info" style="color:white;"><i class="icon-white icon-cog"></i></span>							
						</a>
					</li>
					
<%
	if glCustom = "UK" then
%>					
					
					<li class="dropdown">
						<a id=EditModeLink title="Edit labels" href="/admin/b2bcontent/label_editor.asp">	
							<span class="label label-info" style="color:white;"><i class="icon-white icon-pencil"></i></span>
						</a>
					</li>					
<%
	end if
end if
%>			   
				</ul>
            </div>
        </div>
      </div>
    </div>
	
<%

if isDealer() then

%>	

<div class="container hidden-print " style="<%=containerStyle%>">

<%

promodetail = cleannum(request("promodetail"))

if (session("showpromo") = "1" or glPromoActive) and isDealer() then
	PromoDismissed = request.cookies("hints")("promo_banner")
	PromoProductID1 = "735"
	PromoProdName1 = getProdName_TRANSLATED(PromoProductID1)
	PromoLink1 = "/products/clearance/"
	PromoProductID2 = "734"
	PromoProdName2 = getProdName_TRANSLATED(PromoProductID2)	
	PromoLink2 = "/products/clearance/"
	
	if PromoDismissed = "1" and promodetail <> "1" then
		PromoStyleStr = "display:none;"
%>
		<div id=PromoTeaseRow class=row>	
			<div class="legible span12 spacer20">
				<a onclick="$('#PromoTeaseRow').hide('fast');$('#PromoRow').show('slow');" href="#" id=PromoTeaser class="label label-important"><i class="icon icon-fire icon-white"></i> <%=lne("label_promo_teaser")%></a>
			</div>
		</div>
<%	
	end if
	
%>

		<div id=PromoRow class=row style="<%=PromoStyleStr%>">	
			<div class="span12">					
					<div class="alert alert-error" style="background:#161a35;">
					
						<button type="button" class="close" onclick="hintClose('promo_banner');" data-dismiss="alert">&times;</button>
						
						<div class=row>	
					
					
							<div class="span5 text-center" style="color:#7ac044">		
									
								<a style="color:#7ac044" href="<%=PromoLink1%>">
									<%
									=lne("label_promo_details")
									%>
								</a>
									

											
							</div>

							<div class=span6>
							
									<div class=pull-right>
										<div class=text-center style="xwidth:90%;margin-right:10px;border:1px solid silver;background:#fff;padding:10px;border-radius:3px;">
											<a href="<%=PromoLink1%>">
												<img style="width:100px;" src="http://extranet.endura.co.uk/assets/sml/media/productimages/ET6054/ET6054MV.jpg">									
											<br>
											<span class="label label-inverse"><%=PromoProdName1%></span>	
											</a>
																			
											
										</div>	
										
									</div>
									<div class=pull-right>									
							
										<div class=text-center style="xwidth:90%;margin-right:10px;border:1px solid silver;background:#fff;padding:10px;border-radius:3px;">
											<a href="<%=PromoLink2%>">
												<img style="width:100px;" src="http://extranet.endura.co.uk/assets/sml/media/productimages/ET6055/ET6055MV.jpg">									
											<br>
											<span class="label label-inverse"><%=PromoProdName2%></span>	
											</a>
																			
											
										</div>	
									</div>
										
							</div>
							
							
						</div>
						
						<button onclick="hintClose('promo_banner');" data-dismiss="alert" class="btn btn-danger btn-small" style="background:#7ac044"><%=lne("btn_dismiss_alert")%></button>
						
					</div>
			</div>

		</div>
<%

end if
%>	
	
	<div class=row>
		<div class="span12">	

			<%
			session("AccountOnHold") = AcDetails.OnHold
			
			if AcDetails.OnHold and session("OnHoldDismissed") <> "1" then
				
			%>
				<div class="alert alert-error">
					<%=lne_URL("label_account_on_hold_explained", "<a style=""font-weight:bold;"" href=""/contact/"">")%>
					<a href="#" id=OnHoldDismiss class="close" >&times;</a>
				</div>
				<div id="OnHoldResultsDiv">
				</div>
			<%	
			end if
			%>
			
			<%		If (BBLDealerStatus = "BBLONLY") Then %>
				<div class="alert alert-info">
				<%=lne("bbl_only_header") %>
				</div>
			<% end if %>
			
			<ul class="nav nav-pills legible">
				<li><a href="/"><i class="icon-home"> </i> <%=lne("label_home")%></a></li>
				<li><a href="/checkout/"><i class="icon-shopping-cart"> </i> <%=lne("label_checkout")%> <span id="BasketCount" class="basketCountHolder badge badge-success">...</span></a></li>
								
					<li><a href="/products/recent-arrivals/"><i class="icon icon-asterisk"> </i> <%=lne("page_title_recent_arrivals")%></a></li>
								
				<%
				if not AcDetails.RiderCard and NOT BBLDealerStatus = "BBLONLY" and glClearanceListActive then
					if NewClearanceItems then
					%>
						<li><a href="/products/clearance/"><span title="<%=lne("clearance_header_new_lines")%>" class="label label-important"><i class="icon icon-white icon-fire"> </i> <%=lne("label_clearance_new")%></span> </a></li>
					<%
					else
					%>
						<li><a href="/products/clearance/"><i class="icon-fire"> </i> <%=lne("label_clearance")%> </a></li>
					<%
					end if
					
				
				end if				
				
				if not AcDetails.RiderCard then
				%>					
					
					<li><a href="/utilities/account/"><i class="icon-cog"> </i> <%=lne("label_your_account")%></a></li>		
				<%
				end if
				
				
				if AcDetails.SagePayUser and not AcDetails.RiderCard then
				%>
					<li><a href="/utilities/sagepay/"><i class="icon-check"> </i> <%=lne("label_make_payment")%></a></li>		
				<%
				end if
				if glAllowFeedback or isjh() then
				%>
					<li><a href="/contact/"><i class="icon-envelope"> </i> <%=lne("label_nav_send_feedback")%></a></li>		
				<%
				end if
				if (glFAQEnabled or isjh()) and not AcDetails.RiderCard then
				%>
					<li><a href="/utilities/faq/"><i class="icon-info-sign"> </i> <%=lne("label_faq")%></a></li>		
				<%
				end if			
				%>
			</ul>
			
		</div>	
		
	</div>	

</div>

<%
end if
%>