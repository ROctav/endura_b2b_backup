<%

function getMatchedOffers(byval pRangingID, byval pUpdate)
	dim sql, x, c, z, Matched, OffersMatched, ProductIDwsql
	dim OfferArr, OfferC, RangedArr, RangedC
	dim ThisOfferID, ThisProductID, ThisOfferRanges, ThisOfferTags, ThisOfferCodes, ThisRequiredStyles, ThisRequiredUnits, ThisOfferProdTypes, LastOfferID, OfferMatched, MatchedStyles, MatchedUnits
	dim ProductsMatchedStr
	
	'jhInfo "getMatchedOffers() FUNCTION STARTS"
	
	if pUpdate then
		sql = "DELETE FROM B2B_RangingOffersMatched WHERE RangingID = 0" & pRangingID
		db.execute(sql)
		
		sql = "UPDATE B2B_RangingProducts SET RangingOfferDetailID = 0 WHERE RangingID = 0" & pRangingID
		db.execute(sql)		
		
		sql = "UPDATE B2B_RangingSKUS SET RangingOfferDetailID = 0 WHERE RangingID = 0" & pRangingID
		db.execute(sql)				
	end if
	
	sql = "SELECT ro.RangingOfferID, ro.RangingOfferName, ro.RangingOfferStatusID, ro.ProductTypeID, ro.ClassificationTags, ro.Ranges, ro.ProductCodes"
	sql = sql & ", RequiredStyles , RequiredSKUS , DiscountPercentage , OfferDescription, SmallPrint, ro.ProductTypes, rod.RangingOfferDetailID, ro.ClassificationTags_STYLES  "
	sql = sql & "FROM B2B_RangingOffers ro "
	sql = sql & "INNER JOIN B2B_RangingOfferDetails rod ON ro.RangingOfferID = rod.RangingOfferID "
	sql = sql & "WHERE ro.RangingOfferStatusID = 1 "
	sql = sql & "ORDER BY ro.RangingOfferID, rod.RangingOfferDetailsZOrder DESC, rod.RangingOfferDetailID DESC "
	
	'jhinfo sql
	x = getrs(sql, OfferArr, OfferC)
	
	'jh OfferC
	
	OffersMatched = "|"
	
	if OfferC >= 0 then
					
		sql = "	SELECT rp.RangingID, sp.ProductID, sp.ProductCode, sp.ProductName, sp.Ranges, sp.ClassificationTags, sp.ProductTypes, rcw.cWAySum, Skus.SKUSum, sp.ProductTypeID "
		sql = sql & "FROM SML_Products sp "
		sql = sql & "INNER JOIN B2B_RangingProducts rp ON rp.ProductID = sp.ProductID "
		sql = sql & "LEFT JOIN (SELECT ProductID, Sum(ColorwayQty) AS cWAySum FROM B2B_RangingColorways WHERE RangingID = 52 GROUP BY ProductID) rcw ON rcw.ProductID = rp.ProductID "
		sql = sql & "LEFT JOIN (SELECT ProductID, Sum(SKUQty) AS SKUSum FROM B2B_RangingSKUS WHERE RangingID = 0" & pRangingID & " GROUP BY ProductID) skus ON skus.ProductID = rp.ProductID "
		sql = sql & "WHERE rp.RangingID = 0" & pRangingID & " AND (cWAySum > 0 OR SKUSum > 0) " 					
							
		'jhAdmin sql
		x = getrs(sql, RangedArr, RangedC)
		'jh "RangedC = " & RangedC
							
		for c = 0 to OfferC
		
			ThisOfferID = OfferArr(0,c)
			ThisOfferDetailID = OfferArr(13,c)
		
			ThisOfferRanges = trim("" & OfferArr(3,c))	
			ThisOfferTags = trim("" & OfferArr(4,c))
			ThisOfferCodes = trim("" & OfferArr(6,c))
			
			ThisOfferTags_STYLES = trim("" & OfferArr(14,c))
			
			ThisRequiredStyles = convertNum(OfferArr(7,c))
			ThisRequiredUnits = convertNum(OfferArr(8,c))
			
			ThisOfferProdTypes = trim("" & OfferArr(12,c))
			
			if LastOfferID <> ThisOfferID then
				LastOfferID = ThisOfferID				
				OfferMatched = false
			end if
					
			MatchedStyles = 0
			MatchedUnits = 0		
					
			ProductsMatchedIDs = "|"		
			ProductsMatchedCodes = "|"		
			ProductIDwsql = ""
					
			for z = 0 to RangedC
			
				Matched = false
				isMatchedStyle = false
				isMatchedUnits = false
				
				ThisProductID = RangedArr(1,z)
				ThisProductTags = RangedArr(5,z)
				ThisUnits = convertnum(RangedArr(8,z))
				ThisProductCode = RangedArr(2,z)
				ThisProductTypeID = RangedArr(9,z)

				CheckUnitsOnly = false	
				if ThisOfferTags_STYLES <> "" then
					CheckUnitsOnly = true
					if instr(1, ThisProductTags, ThisOfferTags_STYLES) > 0 and ThisUnits > 0 then
						isMatchedStyle = true											
					end if				
				elseif ThisOfferTags <> "" then
					if instr(1, ThisProductTags, ThisOfferTags) > 0 and ThisUnits > 0 then
						if not CheckUnitsOnly then isMatchedStyle = true
						isMatchedUnits = true							
					end if
				end if
				
				if ThisOfferCodes <> "" then
					if instr(1, ThisOfferCodes, ThisProductCode) > 0 and ThisUnits > 0 then
						if not CheckUnitsOnly then isMatchedStyle = true
						isMatchedUnits = true								
					end if
				end if								
				
				if ThisOfferProdTypes <> "" then					
					if instr(1, ThisOfferProdTypes, "|" & ThisProductTypeID & "|") > 0 and ThisUnits > 0 then						
						if not CheckUnitsOnly then isMatchedStyle = true
						isMatchedUnits = true									
					end if
				end if														
				
				'jhAdmin ThisProductCode & ": " & isMatchedStyle & "_" & isMatchedUnits
				
				if isMatchedStyle or isMatchedUnits then					
					if isMatchedStyle then MatchedStyles = MatchedStyles + 1
					if isMatchedUnits then MatchedUnits = MatchedUnits + ThisUnits	

					if instr(1, ProductsMatchedIDs, "|" & ThisProductID & "|") = 0 then
						ProductsMatchedIDs = ProductsMatchedIDs & ThisProductID & "|"
						ProductsMatchedCodes = ProductsMatchedCodes & ThisProductCode & "|"
						ProductIDwsql = ProductIDwsql & ThisProductID & ", "
					end if					
				end if
			next
			
			if isjh() and not OfferMatched then
				'jhInfo "OFFER NAME: " & OfferArr(1,c)
				'jhAdmin "MatchedStyles = " & MatchedStyles
				'jhAdmin "MatchedUnits = " & MatchedUnits
			end if
			
			if MatchedStyles >= ThisRequiredStyles and MatchedUnits >= ThisRequiredUnits and not OfferMatched then
			
				'jh "MatchedStyles = " & MatchedStyles
				'jh "MatchedUnits = " & MatchedUnits
			
				OfferMatched = true				
				OffersMatched = OffersMatched & ThisOfferDetailID & "|"				
				
				jhAdmin "OFFER NAME: " & OfferArr(1,c)
				sql = "INSERT INTO B2B_RangingOffersMatched (RangingID, RangingOfferID, RangingOfferDetailID, ProductIDsMatched, ProductCodesMatched) VALUES ("
				sql = sql & "0" & pRangingID & ", "
				sql = sql & "0" & ThisOfferID & ", "
				sql = sql & "0" & ThisOfferDetailID & ", "
				sql = sql & "'" & ProductsMatchedIDs & "', "
				sql = sql & "'" & ProductsMatchedCodes & "' "
				sql = sql & ")"
				
				jhAdmin sql
				
				if pUpdate then
					db.execute(sql)
				end if
		
				if right(ProductIDwsql, 2) = ", " then ProductIDwsql = left(ProductIDwsql, len(ProductIDwsql)-2)
		
				sql = "UPDATE B2B_RangingProducts SET RangingOfferDetailID = 0" & ThisOfferDetailID & " WHERE ProductID IN(" & ProductIDwsql & ") AND RangingID = 0" & pRangingID
				if pUpdate then
					jhAdmin sql
					db.execute(sql)
				end if	
				
				sql = "UPDATE B2B_RangingSKUS SET RangingOfferDetailID = 0" & ThisOfferDetailID & " WHERE ProductID IN(" & ProductIDwsql & ") AND RangingID = 0" & pRangingID
				if pUpdate then
					jhAdmin sql
					db.execute(sql)
				end if							
				
			end if
		next
	end if
	
	'jhAdmin "OffersMatched = " & OffersMatched
	
	sql = "UPDATE B2B_Ranging SET OffersMatched = '" & OffersMatched & "' WHERE RangingID = 0" & pRangingID
	jhAdmin sql
	if pUpdate then 
		db.execute(sql)
	end if
	
	getMatchedOffers = OffersMatched
	
	'jhInfo "getMatchedOffers() FUNCTION ENDS"
end function

function processRangingBackup(pRangingID)
	dim sql
	sql = "exec [spB2BRangingSummaryBackup] 0" & pRangingID & ""
	jhAdmin sql
	db.execute(sql)
end function

function formatPercentage(value, totalValue)
	if value > 0 and totalValue > 0 then
		formatPercentage = Round((value / totalValue) * 100, 1) & "%"
	else
		formatPercentage = "0%"
	end if
	
end function

%>