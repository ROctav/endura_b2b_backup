
<style>
	.SizeChartTable TD, .SizeChartTable TH {
		font-family:arial;
		font-size:9pt;
	}
</style>

<%
if SizeChartID = "" then SizeChartID = "1"

if SizeChartID = "1" or SizeChartID = "ALL" then


%>

<table class="SizeChartTable" border="1px" cellpadding="5px" caption="Endura Men's Sizing Overview" style="text-align:center;border:1px solid silver;">
<thead>
 <tr>
  <th id="itemcolumn" scope="col" style="text-align:left; color:black;"><%=lne_B2C("size_chart_mens")%></th>
  <th id="XS1">XS</th>
  <th id="S1">S</th>
  <th id="M1">M</th>
  <th id="L1">L</th>
  <th id="XL1">XL</th>
  <th id="XXL1">XXL</th>
 </tr>
</thead>

<tbody>
 <tr>
  <th style="text-align:left">Chest (inches)</th>
  <td>35-37"</td>
  <td>37-39"</td>
  <td>39-41"</td>
  <td>41-43"</td>
  <td>43-45"</td>
  <td>45-47"</td>
 </tr>
 <tr>
  <th style="text-align:left">Chest (cm)</th>
  <td>89-94cm</td>
  <td>94-99cm</td>
  <td>99-104cm</td>
  <td>104-109cm</td>
  <td>109-114cm</td>
  <td>114-119cm</td>
 </tr>

 <tr>
  <th style="text-align:left">Waist (inches)</th>
  <td>29-31"</td>
  <td>31-33"</td>
  <td>33-35"</td>
  <td>35-37"</td>
  <td>37-39"</td>
  <td>39-41"</td>
 </tr>

 <tr>
  <th style="text-align:left">Waist (cm)</th>
  <td>74-79cm</td>
  <td>79-84cm</td>
  <td>84-89cm</td>
  <td>89-94cm</td>
  <td>94-99cm</td>
  <td>99-104cm</td>
 </tr>
 
 <tr>
  <th style="text-align:left">Gloves (cm)</th>
  <td>7cm</td>
  <td>8cm</td>
  <td>9cm</td>
  <td>10cm</td>
  <td>11cm</td>
  <td>12cm</td>
 </tr>
</tbody>
</table>

<br />

<%
end if

if SizeChartID = "2" or SizeChartID = "ALL" then
%>

<table class="SizeChartTable" id="size_wmn" border="1px" cellpadding="5px" caption="Endura Women's Sizing Overview" style="text-align:center;border:1px solid silver;">
<thead>
 <tr>
  <th id="itemcolumn" scope="col" style="text-align:left; color:black;"><%=lne_B2C("size_chart_womens")%></th>
  <th id="XS1">XS</th>
  <th id="S1">S</th>
  <th id="M1">M</th>
  <th id="L1">L</th>
  <th id="XL1">XL</th>
 </tr>
</thead>

<tbody>
 <tr>
  <th style="text-align:left">Chest (inches)</th>
  <td>32-33"</td>
  <td>34-35"</td>
  <td>36-37"</td>
  <td>38-39"</td>
  <td>40-41"</td>
 </tr>
 <tr>
  <th style="text-align:left">Chest (cm)</th>
  <td>81-86cm</td>
  <td>87-90cm</td>
  <td>91-95cm</td>
  <td>96-100cm</td>
  <td>101-105cm</td>
 </tr>
 <tr>
  <th style="text-align:left">Waist (inches)</th>
  <td>27-28"</td>
  <td>29-30"</td>
  <td>31-32"</td>
  <td>33-34"</td>
  <td>35-36"</td>
 </tr>
 <tr>
  <th style="text-align:left">Waist (cm)</th>
  <td>68-72cm</td>
  <td>73-78cm</td>
  <td>79-83cm</td>
  <td>84-87cm</td>
  <td>88-92cm</td>
 </tr>
 <tr>
  <th style="text-align:left">Dress Size (UK)</th>
  <td>8/10</td>
  <td>10/12</td>
  <td>12/14</td>
  <td>14/16</td>
  <td>16/18</td>
 </tr>
 <tr>
  <th style="text-align:left">Gloves (cm)</th>
  <td>7cm</td>
  <td>7.5cm</td>
  <td>8cm</td>
  <td>8.5cm</td>
  <td>9cm</td>
 </tr>
</tbody>
</table>

<br />

<%
end if

if SizeChartID = "3" or SizeChartID = "ALL" then
%>

<table class="SizeChartTable" id="size_feet" border="1px" cellpadding="5px" caption="Endura Footwear" style="text-align:center;border:1px solid silver;">
<thead>
 <tr>
  <th id="itemcolumn" scope="col" style="text-align:left; color:black;"><%=lne_B2C("size_chart_footwear")%></th>
  <th id="S1">S</th>
  <th id="M1">M</th>
  <th id="L1">L</th>
  <th id="XL1">XL</th>
  <th id="XXL1">XXL</th>
 </tr>
</thead>

<tbody>
 <tr>
  <th style="text-align:left">UK Size</th>
  <td>5-6.5</td>
  <td>7-8.5"</td>
  <td>9-10.5"</td>
  <td>11-12.5</td>
  <td>13-14.5</td>
 </tr>
 <tr>
  <th style="text-align:left">EU Size</th>
  <td>37-39.5</td>
  <td>40-42</td>
  <td>42.5-44.5</td>
  <td>45-47</td>
  <td>47.5-49.5</td>
 </tr>
 <tr>
  <th style="text-align:left">US Size</th>
  <td>5.5-7</td>
  <td>7.5-9</td>
  <td>9.5-11</td>
  <td>11.5-13</td>
  <td>13.5-15</td>
 </tr>

</tbody>
</table>

<br />

<%
end if

if SizeChartID = "4" or SizeChartID = "ALL" then
%>

<table class="SizeChartTable" id="size_kids" border="1px" cellpadding="5px" caption="Endura Children's Sizing Overview" style="text-align:center;border:1px solid silver;">
<thead>
 <tr>
  <th id="itemcolumn" scope="col" style="text-align:left; color:black;"><%=lne_B2C("size_chart_childrens")%></th>
 </tr>
</thead>

<tbody>
 <tr>
  <th style="text-align:left">EU Size</th>
  <td>122-133cm</td>
  <td>134-145cm</td>
  <td>146-157cm</td>
 </tr>
 <tr>
  <th style="text-align:left">UK Size</th>
  <td>7-8</td>
  <td>9-10</td>
  <td>11-12</td>
 </tr>

</tbody>
</table>

<br />

<%
end if

if SizeChartID = "5" or SizeChartID = "ALL" then
%>

<table id="size_helmet" class="SizeChartTable" border="1px" cellpadding="5px" caption="Endura Helmet Sizing Overview" style="text-align:center;border:1px solid silver;">
<thead>
 <tr>
  <th id="itemcolumn" scope="col" style="text-align:left; color:black;"><%=lne_B2C("size_chart_helmets")%></th>
  <th id="S/M1">S/M</th>
  <th id="M/L1">M/L</th>
  <th id="L/XL1">L/XL</th>
 </tr>
</thead>

<tbody>
 <tr>
  <th style="text-align:left">Circumference (inches)</th>
  <td>20-22"</td>
  <td>21.5-23.5"</td>
  <td>23-25"</td>
 </tr>
 <tr>
  <th style="text-align:left">Circumference (cm)</th>
  <td>51-56cm</td>
  <td>55-59cm</td>
  <td>58-63cm</td>
 </tr>


</tbody>
</table>

<%
end if
%>
