<!--#include virtual="/assets/includes/environment.asp" -->
<!--#include virtual="/assets/includes/stringhandler.asp" -->
<!--#include virtual="/assets/includes/classes.asp" -->

<%

public labelDefault, BGSwapColor

if glSiteOffline and not NoRedirect_HOLDING then 
	response.redirect  "/holding/"
end if

if isAdminUser()  then 
	EditMode = session("B2B_EditMode")
	'jh "EDIT MODE = " & session("B2B_EditMode")
else
	EditMode = ""
	session("B2B_EditMode") = ""
	'jh "CheckSN = " & CheckSN
	if isAdminPage() and not NoRedirect then
		jh "ADMIN?"
		jhStop
		dbClose()
		response.redirect "/"
	end if
	
end if

if request("jhX") <> "" then
	if request("jhX") = "off" then
		session("jhX") = ""	
	else		
		session("jhX") = "1"	
		jhx "JHX"
	end if
end if

public db, DrawDateLabel, ComboSize, ComboMulti, dbStatus, ContentEditList, ContentEditCount
ContentEditCount = 0

dim glWebHoldingImage
glWebHoldingImage = "/assets/images/prodbrowser/product_placeholder.jpg"	

function isLoggedIn()
	if isDealer() or isAdminUser() then 
		isLoggedIn = true
	else
		isLoggedIn = false
	end if
end function

function isAdminPage()
	dim checkLoc
	checkLoc = request.servervariables("SCRIPT_NAME")
	'jh "checkLoc = " & checkLoc
	if instr(1, checkLoc, "/admin/") > 0 then
		isAdminPage = true
	end if
end function

function isjh()	
    'if request("Debug") = 1 then
    if session("B2B_AdminUserID") = "1" then 
		isjh = true
	else
		isjh = false
	end if
end function

function isRider()	
	if AcDetails.RiderCard then 
		isRider = true
	else
		isRider = false
	end if
end function

function issh()
	issh = false
	if request.querystring("sh") = "1" then
		issh = true
	end if
end function

function jhKill()	
	if isjh() then
		jhError "STOPPING!"
		dbClose()
		response.end 		
	end if
end function


function getVatComponent(byval pValue, byval pVatRate)

	if pValue = 0 then 
		getVatComponent = 0
		exit function
	end if
	
	ThisDivider = cdbl((100+pVatRate)/100)
	'jh "ThisDivider = " & ThisDivider
	getVatComponent = pValue - (pValue / ThisDivider)
	getVatComponent = formatnumber(getVatComponent,2)
	getVatComponent = cdbl(getVatComponent)
	'jh "getVatComponent = " & getVatComponent
	
end function

function jhStop()
	if isjh() or 1 = 0 then
		jhError "STOPPING"
		dbClose()
		response.end
	end if
end function

function AllStop()
	dbclose()	
	response.end
end function

function jhTrap()
	if err.number <> 0 then
		jhError "err.number = " & err.number
		jhError "err.description = " & err.description
		jhStop
	end if
end function

function getCurrentPage()
	dim CurrentDomain, CurrentPage, CurrentQS, CurrentURL
	CurrentDomain = request.servervariables("SERVER_NAME")
	CurrentPage = request.servervariables("SCRIPT_NAME")	
	CurrentURL = "http://" & CurrentDomain & CurrentPage 
	getCurrentPage = CurrentURL
end function

function getCurrentURL()
	dim CurrentDomain, CurrentPage, CurrentQS, CurrentURL
	CurrentDomain = request.servervariables("SERVER_NAME")
	CurrentPage = request.servervariables("SCRIPT_NAME")
	CurrentQS = request.servervariables("QUERY_STRING")
	CurrentURL = "http://" & CurrentDomain & CurrentPage 
	if CurrentQS <> "" then
		CurrentURL = CurrentURL & "?" & CurrentQS
	end if
	getCurrentURL = CurrentURL
end function

function jhInfo(string)
	jhColor string, "black", "lawngreen"
end function	

function jhAdmin(string)
	'response.write "nj = " & session("nojh") 
	if request.querystring("nojh") = "1" then 
		session("nojh") = "1" 
	elseif request.querystring("nojh") = "0" then
		session("nojh") = "" 
	end if	
	if isjh() and session("nojh") = "" then
		jhColor string, "blue", "lightblue"
	end if
end function

function jhX(string)	
	if request("jhX") = "1" then session("jhX") = "1"
	if request("jhX") = "0" then session("jhX") = ""
	if session("jhX") = "1" then
		jhColor string, "yellow", "black"
	end if
end function

function jhXML(string)	
	
	if isjh() then
		jhColor "<div style=""font-family:times new roman"">" & replace(string, "<", "&lt;") & "</div>", "black", "yellow"
	end if
end function

public B2BTranslationArr, B2BTranslationC, B2CTranslationArr, B2CTranslationC

function loadTrans()

	dim SelectedLanguageID, x
	'jhAdmin "SelectedLanguageID = " & SelectedLanguageID	
	SelectedLanguageID = getSelectedLanguage()

	if not isarray(B2BTranslationArr) and B2BTranslationC <> -1 then
		'jh "TRY AND POPULATE LOOKUP ARRAY"
		sql = "exec [spB2BGetTranslatedContent] 0" & SelectedLanguageID
		'jhAdmin sql
		x = getrs(sql, B2BTranslationArr, B2BTranslationC)
		'jh "B2BTranslationC = " & B2BTranslationC			
	end if

end function

function loadTrans_B2C()

	dim SelectedLanguageID, x
	'jhAdmin "SelectedLanguageID = " & SelectedLanguageID	
	SelectedLanguageID = getSelectedLanguage()

	if not isarray(B2CTranslationArr) and B2CTranslationC <> -1 then
		'jh "TRY AND POPULATE LOOKUP ARRAY"
		sql = "exec [spB2CGetTranslatedContent] 0" & SelectedLanguageID
		'jhAdmin sql
		x = getrs(sql, B2CTranslationArr, B2CTranslationC)
		'jh "B2BTranslationC = " & B2BTranslationC			
	end if

end function


function dLink()
	if session("B2B_EditMode") = "EDIT" then
		dLink = " class=""Label_EDIT_DISABLE"" " 'disables any link to allow in-line editing
	end if
end function

function canEdit(pUser, pLanguage)
	
	exit function
	
	'jh "pUser = " & pUser
	'jh "pLanguage = " & pLanguage
	'jh "Check to see if editing allowed in this language?"
	canEdit = false
	'AllowedLanguages = "|0|20|"
	AllowedLanguages = session("B2B_AllowedLanguages")
	'jh "AllowedLanguages = " & AllowedLanguages
	
	if instr(1, AllowedLanguages, "|" & pLanguage & "|") > 0 or AllowedLanguages = "|ALL|" then
		canEdit = true
	end if
	
end function

function findContent_B2C(pLookup)
	'''LOADS INFO FROM B2C DATABASE
	
	pLookup = trim("" & lcase(pLookup))
	dim TransWalker, ThisText, ThisTranslated, returnText, SelContentID
	SelectedLanguageID = getSelectedLanguage()
	
	loadTrans_B2C()
	
	findContent_B2C = pLookup
	
	if isarray(B2CTranslationArr) then
	
		for TransWalker = 0 to B2CTranslationC
			ThisContentID = B2CTranslationArr(0, TransWalker) 
			ThisLookup = "" & B2CTranslationArr(1, TransWalker) 
			ThisText = "" & B2CTranslationArr(2, TransWalker) 
			if ThisText = "" then ThisText = ThisLookup
			ThisTranslatedText = B2CTranslationArr(3, TransWalker) 
			'jh "ThisTranslatedText = " & ThisTranslatedText	
			
			if lcase(ThisLookup) = lcase(pLookup) then
				'jh "FOUND B2C CONTENT!"
				if SelectedLanguageID = "0" then
					findContent_B2C = ThisText
				else
					findContent_B2C = ThisTranslatedText
				end if	
				
				'jh "findContent_B2C = " & findContent_B2C
				exit for
			end if
		next
	end if
	
end function

function findContent(pLookup, pType)

	'if not isjh() then exit function
	dim SelectedLanguageID

	FoundContentID = ""
	dim TransWalker, ThisText, ThisTranslated, returnText, SelContentID
	
	SelContentID = "" 
	
	SelectedLanguageID = getSelectedLanguage()
	
	pLookup = trim("" & pLookup)
	'jhAdmin "looking for = " & pLookup
	'jhAdmin "SelectedLanguageID = " & SelectedLanguageID
	findContent = pLookup 
	
	loadTrans()
	LookupFound = false
	if isarray(B2BTranslationArr) then
	
		for TransWalker = 0 to B2BTranslationC
			ThisContentID = B2BTranslationArr(0, TransWalker) 
			ThisLookup = "" & B2BTranslationArr(1, TransWalker) 
			ThisText = "" & B2BTranslationArr(2, TransWalker) 
			if ThisText = "" then ThisText = ThisLookup
			ThisTranslatedText = B2BTranslationArr(3, TransWalker) 
			'jh "ThisTranslatedText = " & ThisTranslatedText
			
			ThisPageID = B2BTranslationArr(4, TransWalker) 
			
			'jh "ThisTranslatedText = " & ThisTranslatedText
			if lcase(ThisLookup) = lcase(pLookup) then
				'jhInfo "FOUND"
				LookupFound = true
				if SelectedLanguageID = "0" then
					returnText = ThisText 
					rSuccess = true
					'jh "herex"
				elseif ThisTranslatedText <> "" then
					returnText = ThisTranslatedText		
					rSuccess = true
					'jh "here2"
				else
					returnText = ThisText
					rSuccess = false
					'jh "here3"
				end if
				
				SelContentID = ThisContentID
				FoundContentID = ThisContentID
			
				exit for
			end if
		next
	end if		

	if returnText = "" then returnText = pLookup

	'jhAdmin "returnText = " & returnText
	'jhAdmin "EditMode = " & EditMode
	
	if pType = "LABEL" then
		if returnText <> "" then findContent = returnText
		LabelType = ""
		if SelContentID = "" then SelContentID = "-1"
		NoLabels = true
	elseif EditMode = "CONTENT" then
		jh "CONTENT EDITOR "
	else
		findContent = returnText
	end if
	
	
end function

function getLabel(pContent)
	getLabel = findContent(pContent, "LABEL")
end function

function getContent(pContent)
	'jh "getContent, pContent = " & pContent
	getContent = findContent(pContent, "CONTENT")
	getContent = "" & getContent & cmsLink("CONTENT", pContent)
	
end function

function getContent_ADMIN(pContent)
	'jh "getContent, pContent = " & pContent
	getContent_ADMIN = findContent(pContent, "ADMIN")
	
end function

function lne_NOEDIT(pContent)
	lne_NOEDIT = findContent(pContent, "LABEL")
end function

function lne_REPLACE(pContent, pReplace)
	dim c, replaceArr
	replaceArr = split(pReplace, "|")
	lne_REPLACE = findContent(pContent, "LABEL")
	for c = 0 to ubound(replaceArr) 
		lne_REPLACE = replace(lne_REPLACE, "{" & c & "}", replaceArr(c))
	next
	
	lne_REPLACE = lne_REPLACE & cmsLink("LABEL", pContent)
	
end function

function lne_URL(byval pVal, byval pURL)
	lne_URL = lne(pVal)
	lne_URL = replace(lne_URL, "{a}", pURL)
	lne_URL = replace(lne_URL, "{/a}", "</a>")
end function


function lne_INS(pContent, pInitVal)
	if 1 = 0 then
		dim rs
		sql = "SELECT B2BContentID FROM B2B_Content WHERE LookupName = '" & pContent & "'"
		set rs = db.execute(sql)
		if rs.eof then
			jhAdmin "NOT FOUND - INSERTING "
			sql = "INSERT INTO B2B_Content (LookupName, ContentText) VALUES ('" & validstr(pContent) & "', '" & validstr(pInitVal) & "');"
			jhAdmin sql
			db.execute(sql)
		end if
	end if
	lne_INS = lne(pContent)
end function

public FoundContentID

function lne(pContent)
	
	lne = pContent
	'if not isjh then exit function

	if isjh() then session("B2B_LabelTags") = "1"
	
	lne = findContent(pContent, "LABEL") & cmsLink("LABEL", pContent)
	
	if isjh() then
		if request("jhlne") = "1" then
			session("jhlne") = "1"
		elseif request("jhlne") = "0" then
			session("jhlne") = ""
		end if

		if session("jhlne") = "1" then				
			lne = "^" & lne & "^"
		end if
	end if
	
end function

function lne_B2C(pContent)
	lne_B2C = findContent_B2C(pContent)
	'jh "lne_B2C = " & lne_B2C
end function

function cmsActive()
	cmsActive = false
	if session("cms_editmode") = "on" then
		cmsActive = true
	end if
end function

function cmsLink(pLinkType, pLookupName)

	dim classStr
	
	exit function
	
	if not isAdminUser then exit function
	
	if cmsActive() then
		classStr = "CMSLink"
	else
		classStr = "CMSLink CMSHidden"
	end if
			
	if pLinkType = "CONTENT" then
		ContentReturnURL = getCurrentPage
	
		cmsLink = "<div class=spacer10><a class=""" & classStr & " label label-info"" href=""/admin/b2bcontent/content_edit.asp?content_lookup=" & pLookupName & "&ContentReturnURL=" & ContentReturnURL & """><i class=""icon-white icon-bookmark""></i> editor</a></div>"	
	else
		cmsLink = "<a class=""" & classStr & """ href=""/admin/b2bcontent/label_editor.asp#" & pLookupName & """><i class=icon-bookmark></i></a>"	
	end if

end function

function lne_nl(pContent)
	lne_nl = findContent(pContent, "LABEL")
	if session("B2B_AdminUserID") = "1" then 
		'lne = lne & "<i class=icon-bookmark></i>"
	end if
end function

function jhSubtle(string)
	jhColor string, "silver", "#fff"
end function	

function jhError(string)
	jhColor string, "pink", "red"
end function	

function jhColor(string, pFore, pBG)
	response.Write "<div style=""margin:2px 5px 5px 5px;border-radius:3px;padding:3px;border:0px solid " & pFore & ";background-color:" & pBG & ";font-size:11px;;color:" & pFore & ";"">" & string & "</div>"
end function	

function jhInfoNice(pintro, pdetail)
	dim os
	os = "<div class=""alert alert-success"">"
	if pintro <> "" then os = os & "<h4>" & pIntro & "</h4>"
	os = os & pdetail
	os = os & "</div>"
	response.write os
end function

function jhNice(pintro, pdetail)
	dim os
	os = "<div class=""alert alert-default"">"
	if pintro <> "" then os = os & "<h4>" & pIntro & "</h4>"
	os = os & pdetail
	os = os & "</div>"
	response.write os
end function

public listItemArr, listItemC
function getTransItem(pid, pListType, pLanguage)
	dim c, arrSuccess
	if not isarray(listItemArr) and listItemC <> -1 then
		'jh "LOAD ITEMS"
		sql = "SELECT ListTypeID, IDValue, TranslatedValue FROM SML_ListsItems_TRANSLATED WHERE LanguageID = 0" & pLanguage
		'jh sql
		arrSuccess = getrs(sql, listItemArr, listItemC)
	end if
	for c = 0 to listItemC
		if listItemArr(0,c) = pListType and listItemArr(1,c) = pid then
			getTransItem = listItemArr(2,c)
			exit for 
		end if
	next
end function

function jhErrorNice(pintro, pdetail)
	dim os
	os = "<div class=""alert alert-error"">"
	if pintro <> "" then os = os & "<h4>" & pIntro & "</h4>"
	os = os & pdetail
	os = os & "</div>"
	response.write os
end function

function jh(string)
	jhColor string, "lightgreen", "blue"
end function	

function DBConnect()
	WebFolder = "website"
	RootDir = server.MapPath("/") 
	'jh Rootdir
	db_file_location = RootDir & "\"	
	ThisServer = request.servervariables("SERVER_NAME")	
	db_conn_str = glDBConnStr
	if dbStatus <> 1 then
		set db = server.CreateObject("ADODB.Connection")
		
		db.open(db_conn_str)		
		'jhAdmin "+++ OPEN THE DB +++"
		dbStatus = 1
	else
		'jhAdmin "+++ ALREADY OPEN +++"
	end if
end function

function dbClose()

	on error resume next
	
	rs.close
	set rs = nothing
	db.close
	set db = nothing

	on error goto 0
	
	dbStatus = 0
	
	'jh "+++DB CLOSE+++"
	
end function

function ValidEmailList(pList)
	dim c, ThisEmail, CheckValid, SomeChecked
	
	ValidEmailList = true
	'plist = replace(pList, " ", "")
	plist = replace(pList, ";", ",")
	pListArr = split(pList, ",")
	for c = 0 to ubound(pListArr)
		ThisEmail = trim("" & pListArr(c))
		if ThisEmail <> "" then
			CheckValid = validemail(ThisEmail)
			
			jhAdmin "CheckValid = " & CheckValid & " (" & ThisEmail & ")"
			
			if not CheckValid then 
				ValidEmailList = false
			else
				SomeChecked = true
			end if
		end if
	next
	
	if not SomeChecked then 
		ValidEmailList = false
	end if 
	
	jhAdmin "ValidEmailList = " & ValidEmailList
	
end function

function ValidEmail(PassedEmail)
	ValidEmail = true
	PassedEmail = "" & PassedEmail
	if len(PassedEmail) < 6 then
		ValidEmail = false
	elseif instr(1, PassedEmail, "@") = 0 then
		ValidEmail = false
	elseif instr(1, PassedEmail, " ") > 0 then
		ValidEmail = false
	elseif instr(1, PassedEmail, ".") = 0 then
		ValidEmail = false
	end if	
end function

function validstr(p)
	validstr = replace("" & p, "'","''")
end function

function cleannum(p)
	p = trim("" & p)
	if isnumeric(p) and p <> "" then 	
		cleannum = cdbl(p)
	else
		cleannum = ""
	end if
	
end function

function convertnum(pNum)
	pNum = trim("" & pNum)
	if isnumeric(pNum) and pNum <> "" then 	
		convertnum = cdbl(pNum)
	else
		convertnum = 0
	end if
	
end function

function isNumber(p)
	if cleannum(p) = "" then isNumber = false else isNumber = true
end function

Function stripNonNumeric(byval inputString)
	Set regEx = New RegExp
	regEx.Global = True
	regEx.Pattern = "\W"
	stripNonNumeric = regEx.Replace(inputString,"")
End Function

function GetRs(byval pSql, byref pArr, byref pRc)
	dim rs
	set rs = db.execute(pSql)
	
	if not rs.eof then
		pArr = rs.getrows()
		pRc = ubound(pArr,2)
	else
		pRc = -1
	end if
	
	rsClose()
	
end function

function rsClose()
	on error resume next
	
	rs.close
	set rs = nothing

	on error goto 0
end function	

public ImageRoot, ImageRoot_URL, ImageCDN_URL
ImageRoot = server.mappath("/") & "\assets\sml\media\productimages\"

ImageRoot = "E:\inetpub\extranet\website\assets\SML\media\ProductImages"

ImageRoot_URL = "http://extranet.endura.co.uk/assets/sml/media/productimages/"

ImageCDN_URL = "https://static.endura.co.uk/"


function fixURL(byval pURL)
	'jh "pURL = " & pURL
	pURL = trim("" & pURL)
	if left(pURL, 7) <> "http://" and instr(1, pURL, "https:") = 0 and pURL <> "" then
		pURL = "http://" & pURL
	end if
	fixURL = pURL	
	'jh "fixURL = " & fixURL
end function

function imageExists(pLoc, pImg)
	dim fs
	set fs = server.createobject("scripting.filesystemobject")	
	
	if fs.fileexists(pLoc & pImg) then
		'jh "THUMB FOUND - " & pColorway
		imageExists = true
	end if

	set fs = nothing
end function

function getReviewLogoFile(byval pProductCode, byVal pLogoFile, byref rLogoURL)
	searchFolder = ImageRoot & "\" & pProductCode & "\reviews\"	
	if imageExists(searchFolder, pLogoFile) then
		rLogoURL = ImageRoot_URL & pProductCode & "/reviews/" & pLogoFile			
		getReviewLogoFile = rLogoURL	
	end if
	
end function

function getReviewDownload(byval pProductCode, byVal pDownloadFile, byref rDownloadURL)
	searchFolder = ImageRoot & "\" & pProductCode & "\reviews\"	
	if imageExists(searchFolder, pDownloadFile) then
		rDownloadURL = ImageRoot_URL & pProductCode & "/reviews/" & pDownloadFile			
		getReviewDownload = rDownloadURL	
	end if
	
end function



function getImageColorThumb(byval pProductCode, byval pColorWay, byRef rImageURL)
	

	searchFolder = ImageRoot & "\" & pProductCode & "\"
	searchFile = pColorWay & ".jpg"
	if imageExists(searchFolder, searchFile) then
		getImageColorThumb = ImageCDN_URL & searchFile			
	end if
	
end function

function getImageColorMain(byval pProductCode, byval pColorWay, byRef rImageURL)
	
	searchFolder = ImageRoot & "\" & pProductCode & "\"
	searchFile = pColorWay & ".jpg"
	if imageExists(searchFolder, searchFile) then
		getImageColorMain = ImageCDN_URL & searchFile			
	end if
	
end function


function getImageColorZoom(byval pProductCode, byval pColorWay, byRef rImageURL)
	

	searchFolder = ImageRoot & "\" & pProductCode & "\"
	searchFile = pColorWay & "_lg.jpg"
	if imageExists(searchFolder, searchFile) then
		getImageColorZoom = ImageCDN_URL & searchFile			
	end if
	
end function

function getImageColorBack(byval pProductCode, byval pColorWay, byRef rImageURL)
	
	searchFolder = ImageRoot & "\" & pProductCode & "\"
	searchFile = pColorWay & "_back.jpg"
	if imageExists(searchFolder, searchFile) then
		getImageColorBack = ImageCDN_URL & searchFile			
	end if
	
end function

function getImageColorBackZoom(byval pProductCode, byval pColorWay, byRef rImageURL)
	
	searchFolder = ImageRoot & "\" & pProductCode & "\"
	searchFile = pColorWay & "_back_lg.jpg"
	if imageExists(searchFolder, searchFile) then
		getImageColorBackZoom = ImageCDN_URL & searchFile		
	end if
end function




function getImageDefaultCDN(byval pProductCode, byVal pVariantCode)

	searchFolder = ImageRoot & "\" & pProductCode & "\"
	
	if pVariantCode = "" then
		sql = "SELECT TOP 1 VariantCode FROM SML_Products sp INNER JOIN JH_EnduraWeb.dbo.SML_ProductColorways spc ON sp.ProductID = spc.ProductID "
		sql = sql & "WHERE ProductCode = '" & pProductCode & "' "
		sql = sql & "ORDER BY DefaultProductImage DESC, VariantCode"

		set rs = db.execute(sql)
		if not rs.eof then
			pVariantCode = rs("VariantCode")
		end if
		rsClose()
	end if

	searchFile = pVariantCode & ".jpg"
	
	if imageExists(searchFolder, searchFile) then
		getImageDefaultCDN = ImageCDN_URL & searchFile			
	else
		getImageDefaultCDN = glWebHoldingImage
	end if
end function

function getImageDefault(byval pProductCode, byVal pVariantCode)

	searchFolder = ImageRoot & "\" & pProductCode & "\"
	'jhAdmin searchFolder
	
	if pVariantCode = "" then
		sql = "SELECT TOP 1 VariantCode FROM SML_Products sp INNER JOIN JH_EnduraWeb.dbo.SML_ProductColorways spc ON sp.ProductID = spc.ProductID "
		sql = sql & "WHERE ProductCode = '" & pProductCode & "' "
		sql = sql & "ORDER BY DefaultProductImage DESC, VariantCode"
		'jh sql
		set rs = db.execute(sql)
		if not rs.eof then
			pVariantCode = rs("VariantCode")
		end if
		rsClose()
	end if
	searchFile = pVariantCode & ".jpg"
	'jhAdmin "searchFile = " & searchFile
	
	if imageExists(searchFolder, searchFile) then
		getImageDefault = ImageRoot_URL & pProductCode & "/" & searchFile			
	else
		getImageDefault = glWebHoldingImage
	end if
end function

function getImageProdThumb(byval pProductCode, byVal pVariantCode)
	

	searchFolder = ImageRoot & "\" & pProductCode & "\"
	searchFile = pVariantCode & ".jpg"
	if imageExists(searchFolder, searchFile) then
		getImageProdThumb = ImageRoot_URL & pProductCode & "/" & searchFile			
	else
		getImageProdThumb = glWebHoldingImage
	end if
	
	'if not ThumbFound then getColorThumb = "xxx"
	
end function

function getImageRighthand(byval pProductCode, byRef rImageURL)

	searchFolder = ImageRoot & "\" & pProductCode & "\"
	searchFile = pProductCode & "_life.jpg"
	if imageExists(searchFolder, searchFile) then
		getImageRighthand = ImageRoot_URL & pProductCode & "/" & searchFile			
	end if
	
	'if not ThumbFound then getColorThumb = "xxx"
	
end function


function getProductImages(byval pProductCode, byval ColorWay, byRef rImageURL, byRef rZoomURL)
	
end function


const pi = 3.14159265358979323846
 
Function distance(lat1, lon1, lat2, lon2, unit)
  Dim theta, dist
  theta = lon1 - lon2
  dist = sin(deg2rad(lat1)) * sin(deg2rad(lat2)) + cos(deg2rad(lat1)) * cos(deg2rad(lat2)) * cos(deg2rad(theta))
  dist = acos(dist)
  dist = rad2deg(dist)
  distance = dist * 60 * 1.1515
  Select Case ucase(unit)
    Case "K"
      distance = distance * 1.609344
    Case "N"
      distance = distance * 0.8684
  End Select
End Function 
 
 
'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
':::  This function get the arccos function from arctan function    :::
'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
Function acos(rad)
  If Abs(rad) <> 1 Then
    acos = pi/2 - Atn(rad / Sqr(1 - rad * rad))
  ElseIf rad = -1 Then
    acos = pi
  End If
End function
 
 
'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
':::  This function converts decimal degrees to radians             :::
'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
Function deg2rad(Deg)
	deg2rad = cdbl(Deg * pi / 180)
End Function
 
'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
':::  This function converts radians to decimal degrees             :::
'::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
Function rad2deg(Rad)
	rad2deg = cdbl(Rad * 180 / pi)
End Function

function addbr(p)
	if isnull(p) then p = ""
	addbr = replace(p, chr(10), "<br>")
end function

function SendMail(pSubject, pBody, pRecipient, pSender, pHTML)

	set Jmail = server.CreateObject("JMail.SMTPMail")
	dim c, CheckEmail, RecipList
		
	jmail.Subject = pSubject
	
	jmail.Priority = 1
			
	jmail.serveraddress = glMailServer
	'if pSubject <> "ENDURA Supplier Notification" then pSender = ""
	if not validemail(pSender) then pSender = "info@endura.co.uk"		
	jmail.sender = pSender

	ThisServer = Request.ServerVariables("SERVER_NAME")
	
	if instr(pRecipient, ";") > 0 then
		recipArr = split(pRecipient, ";")
		for c = 0 to ubound(recipArr) 
			CheckEmail = recipArr(c)
			if CheckEmail <> "" then
				if validemail(CheckEmail) then 
					'jhInfo "ADDING - " & CheckEmail
					jmail.AddRecipient CheckEmail
					RecipList = RecipList & CheckEmail & ", "
				end if
			end if
		next
	elseif validemail(pRecipient) then 
		jmail.AddRecipient pRecipient
		RecipList = RecipList & pRecipient & ", "
	end if
	
	jmail.AddRecipientbcc "automated@kmvcreative.com"
	
	if pHTML then	
		jmail.HTMLbody = pBody			
	else
		jmail.body = pBody			
	end if
	
	on error resume next

	jmail.lazysend = true	
	if not glEmailDisabled then 
		jmail.execute
		'jhInfo "SENT - " & pRecipient
		SendMail = true
	end if
	
	jhAdmin "SENT TO - " & RecipList
	
	'call recordEmail(pSubject, pBody, "Samples Reminder - " & pRecipient)
	
	on error goto 0
			
	set jmail = nothing	
end function

function SendMailWithAttachment(pSubject, pBody, pRecipient, pSender, pHTML, pAttachment)

	set Jmail = server.CreateObject("JMail.SMTPMail")
	dim c, CheckEmail, RecipList
		
	jmail.Subject = pSubject
	
	jmail.Priority = 0
			
	jmail.serveraddress = glMailServer
	'if pSubject <> "ENDURA Supplier Notification" then pSender = ""
	if not validemail(pSender) then pSender = "ukcustomerservices@endura.co.uk"		
	jmail.sender = pSender

	ThisServer = Request.ServerVariables("SERVER_NAME")
	
	if instr(pRecipient, ";") > 0 then
		recipArr = split(pRecipient, ";")
		for c = 0 to ubound(recipArr) 
			CheckEmail = recipArr(c)
			if CheckEmail <> "" then
				if validemail(CheckEmail) then 
					'jhInfo "ADDING - " & CheckEmail
					jmail.AddRecipient CheckEmail
					RecipList = RecipList & CheckEmail & ", "
				end if
			end if
		next
	elseif validemail(pRecipient) then 
		jmail.AddRecipient pRecipient
		RecipList = RecipList & pRecipient & ", "
	end if

    jmail.AddAttachment pAttachment
	
	if pHTML then	
		jmail.HTMLbody = pBody			
	else
		jmail.body = pBody			
	end if
	
	on error resume next

	jmail.lazysend = true	
	if not glEmailDisabled then 
		jmail.execute
		'jhInfo "SENT - " & pRecipient
		SendMail = true
	end if
	
	jhAdmin "SENT TO - " & RecipList
	
	'call recordEmail(pSubject, pBody, "Samples Reminder - " & pRecipient)
	
	on error goto 0
			
	set jmail = nothing	
end function

function GetSysVar(pvar)
	dim sql,rs
	dbConnect()
	GetSysVar = 0
	sql = "SELECT * FROM END_SystemVariables WHERE SystemVariableName = '" & cleansql(pVar) & "'"
	'jh sql
	set rs = db.Execute(Sql)
	
	if not rs.eof then
		GetSysVar = rs("SystemVariableStatus")
	end if
	
	rsClose()
	
end function	

function SetSysVar(pvar, pVal)
	dim sql,rs
	dbConnect()
	sql = "UPDATE END_SystemVariables SET SystemVariableStatus = " & pVal & " WHERE SystemVariableName = '" & cleansql(pVar) & "'"
	'jh sql
	db.Execute(Sql)
	
end function	

function SetSysVarDate(pvar, pDate)
	dim sql,rs
	if isdate(pDate) then
		dbConnect()
		sql = "UPDATE END_SystemVariables SET SystemVariableDate = '" & pDate & "' WHERE SystemVariableName = '" & cleansql(pVar) & "'"
		'jh sql
		db.Execute(Sql)
	else
		jhError "Not a date!"
	end if	
end function	

function SetSysVarValue(pvar, pVal)
	dim sql,rs
	
	dbConnect()
	sql = "UPDATE END_SystemVariables SET SystemVariableTextValue = '" & validstr(pVal) & "' WHERE SystemVariableName = '" & validstr(pVar) & "'"
	'jh sql
	db.Execute(Sql)

end function	

function GetSysVarDate(pvar)
	dim sql,rs
	dbConnect()
	GetSysVarDate = ""
	sql = "SELECT SystemVariableDate FROM END_SystemVariables WHERE SystemVariableName = '" & cleansql(pVar) & "'"
	'jh sql
	set rs = db.Execute(Sql)	
	if not rs.eof then
		GetSysVarDate = rs("SystemVariableDate")
	end if	
	rsClose()
end function	

function GetSysVarValue(pvar)
	
	GetSysVarValue = ""

	dim sql,rs
	dbConnect()	
	sql = "SELECT SystemVariableTextValue FROM END_SystemVariables WHERE SystemVariableName = '" & validstr(pVar) & "'"
	'jh sql
	set rs = db.Execute(Sql)	
	if not rs.eof then
		GetSysVarValue = rs("SystemVariableTextValue")
	end if	
	rsClose()

end function	

function inSplit(pString, pNumeric)
	dim c, SplitArr, SplitC, ThisItem
	SplitArr = split(pString, "|")
	SplitC = ubound(SplitArr)
	'jh "SplitC = " & SplitC
	
	inSplit = ""
	if pNumeric then
		delim = ""
	else
		delim = "'"
	end if
	
	for c = 0 to SplitC
		ThisItem = SplitArr(c)
		if ThisItem <> "" then			
			inSplit = inSplit & delim & validstr(ThisItem) & delim & ", "						
		end if
	next

	if right(inSplit, 2) = ", " then inSplit = left(inSplit, len(inSplit)-2)

end function

function captureProductView(pProduct)
	dim sql
	pProduct = cleannum(pProduct)
	if pProduct <> "" then
		ThisAcCode = session("B2B_AcCode")
		sql = "DELETE FROM B2B_ProductViews WHERE ProductID = 0" & pProduct & " AND AcCode = '" & validstr(ThisAcCode) & "'"
		''jh sql
		db.execute(sql)		
		sql = "INSERT INTO B2B_ProductViews (ProductID, AcCode) VALUES (0" & pProduct & ", '" & validstr(ThisAcCode) & "')"
		'jh sql
		db.execute(sql)
	end if
end function

function getStockMAI(pSKU)

	dim rs
	getStockMAI = 0
	
	sql = "SELECT SUM(QtyInStock - QtyAllocated) AS MAIQty FROM ExStockLocations l "
	sql = sql & "WHERE l.StockLocation = 'MAI' AND StockCode = '" & validstr(pSKU) & "' "
	
	'jh sql
	
	set rs = db.execute(sql)
	if not rs.eof then
		getStockMAI = convertNum(rs("MAIQty"))
	end if
	rsClose()
				
end function

function dispStock(pQty)
	
	dim QtySimple, StockClass
	
	pQty = cleannum(pQty)
	if pQty = "" then pQty = 0 else pQty = cdbl(pQty)
		
	QtySimple = pQty						
	if pQty <= glStockRed then 
		StockClass = "label-important"
	elseif pQty <= glStockAmber then 
		StockClass = "label-warning"
	else
		StockClass = "label-success"
		if pQty > glStockUpperDisplayQty then
			QtySimple = glStockUpperDisplayQty & "+"
		end if
	end if
	
	dispStock = "<span class=""label " & StockClass & """>" & QtySimple & "</span>"				
	
end function

function dispArrivals(pQty)
	
	dim QtySimple, StockClass
	
	pQty = cleannum(pQty)
	if pQty = "" then pQty = 0 else pQty = cdbl(pQty)
		
	QtySimple = pQty						
	
	if pQty > glNextArrivalUpperDisplayQty then
		QtySimple = glNextArrivalUpperDisplayQty & "+"
	end if
	
	dispArrivals = "<span class=""label label-inverse"">" & QtySimple & "</span>"				
	
end function

function dispStock_ENQUIRY(pQty, pProductID, pVariant, pSKU)
	
	dispStock_ENQUIRY = "<a data-product-id=""" & pProductID & """ data-variant-code=""" & pVariant & """ data-sku=""" & pSKU & """ onclick=""stockLevelClick_VARIANT(this)"" class=StockEnquiryLink>" & dispStock(pQty) & "</a>"
	
end function


function dispBO (pQty)
	pQty = cleannum(pQty)
	if pQty = "" then pQty = 0 else pQty = cdbl(pQty)
	
	if pQty > 0 then
		dispBO = "&nbsp;<span title=""" & lne("label_on_backorder") & """ class=""label label-bo""><a class=WhiteLink href=""/utilities/account/backorders/""><i class=""icon-time icon-white""></i>BO " & pQty & "</a></span>"	
	end if
	
end function

function dispFO (pCode, pQty)
	pQty = cleannum(pQty)
	if pQty = "" then pQty = 0 else pQty = cdbl(pQty)
	
	if pQty > 0 then
		dispFO = "&nbsp;<span title=""" & lne("label_on_forward_order") & """ class=""label label-fo""><a class=WhiteLink href=""/utilities/account/reports/forward-orders/?SearchVal=" & pCode & """><i class=""icon-time icon-white""></i>FO " & pQty & "</a></span>"	
	end if
	
end function

function dispCurrency(pValue)
	
	dim cSymbol, cPlaces

	pValue = cleannum(pValue)
	if pValue = "" then 
		pValue = "?"
		exit function
	end if
	
	cSymbol = AcDetails.CurrencySymbol
	cPlaces = AcDetails.CurrencyDecimals	
	
	if cSymbol = "" then cSymbol = ""
	if cPlaces = "" then cPlaces = "2"
	
	if session("B2B_SuppressPricing") = "1" then
		'dispCurrency = "<i class=icon-ok></i> " & cSymbol & "..."
		dispCurrency = cSymbol & "..."
	else
		'dispCurrency = "<i class=icon-ok></i> " & cSymbol & "" & formatnumber(pValue, cPlaces)  & ""
		dispCurrency = cSymbol & "" & formatnumber(pValue, cPlaces)  & ""
	end if
	
end function

function dispCurrencyRounded(pValue)
	pValue = convertnum(pValue)
	pValue = clng(pValue)
	dispCurrencyRounded = dispCurrency(pValue)
end function

function dispCurrencyDash(pValue)
	if convertnum(pValue) = 0 then  
		dispCurrencyDash = "-"
	else
		dispCurrencyDash = dispCurrency(pValue)
	end if
end function

function dispCurrency_ALWAYS(pValue)
	
	dim cSymbol, cPlaces

	pValue = cleannum(pValue)
	if pValue = "" then 
		pValue = "?"
		exit function
	end if
	
	cSymbol = AcDetails.CurrencySymbol
	cPlaces = AcDetails.CurrencyDecimals	
	
	if cSymbol = "" then cSymbol = ""
	if cPlaces = "" then cPlaces = "2"
	
	dispCurrency_ALWAYS = cSymbol & "" & formatnumber(pValue, cPlaces)  & ""
	
end function

function pricingActive()
	if session("B2B_SuppressPricing") = "1" then pricingActive = false else pricingActive = true
end function

function dispCurrency_PLAIN(pValue)
	
	dim cPlaces

	pValue = cleannum(pValue)
	if pValue = "" then 
		pValue = "?"
		exit function
	end if
	
	cPlaces = AcDetails.CurrencyDecimals	
	if cPlaces = "" then cPlaces = "2"
	
	if session("B2B_SuppressPricing") = "1" then
		dispCurrency_PLAIN = "..."
	else
		dispCurrency_PLAIN = formatnumber(pValue, cPlaces)  & ""
	end if
	
end function

function GetCurrencyDetails(byval pCurrency, byref rCurrencySymbol, byref rDecimalPlaces)
	
	dim sql, rs 
	'jhAdmin "GetCurrency pCurrency = " & pCurrency
	
	rDecimalPlaces = 2
	GetCurrencyDetails = true
	
	sql = "SELECT TOP 1 ISNULL(CurrencySymbolHTML, CurrencySymbol) AS cSymbol FROM ExchequerCurrency WHERE CurrencyID = 0" & cleannum(pCurrency)
	'jh sql
	set rs = db.execute(sql)
	
	if not rs.eof then
		rCurrencySymbol = "" & rs("cSymbol")
	else	
		rCurrencySymbol = "?"
	end if
	
	rsClose()
	
	pCurrency = "" & pCurrency
	select case pCurrency		
		case "8" 			
			rDecimalPlaces = 0	
	end select	
	
end function

function getSKUPRice(pSKU, pBand)

	sql = "exec spB2BSKUPrice '" & validstr(pSKU) & "'" 
	jhAdmin sql
	getSKUPRice = 999

end function

function GetVatValue(pValue)

		if session("ENDURA_Vat") = "S" then	
			sBar "VAT APPLICABLE?"
			VatCalc = (pValue*glVATRate)/100		
		else
			VatCalc = 0
		end if

	if SuppressRounding then
		GetVatCost = VatCalc
	else
		GetVatCost = formatnumber(VatCalc,2)
	end if
	
end function


function canSee(pSKU)
	
	sql = "SELECT ss.SKU, sp.ProductID, sp.ProductCode, Ranges, Categories "
	sql = sql & "FROM SML_SKUS ss INNER JOIN SML_Products sp ON sp.PRoductID = ss.SKU "
	sql = sql & "WHERE ss.SKU = '" & validstr(pSKU) & "' "
	
	jh sql

		
end function

function DrawDate_JQ(strName, defval)

	if DLabel <> "" then LabelStr = DLAbel else LabelStr = StrName
	if not isdate(defVal) then defVal = "" else defVal = formatdatetime(defVal,2)
	'jh defVal
	
	pInd = pInd+1
	'if DrawDateAddP then DrawDate_JQ = DrawDate_JQ & "<p>"

	'DrawDate_JQ = DrawDate_JQ & " " & DrawDateLabel & ""
	
	DateClass = "DatePicker"
	if DatePickerPastOnly then DateClass = "DatePicker_PAST"
	DatePickerPastOnly = false
	
	DrawDate_JQ = DrawDate_JQ & "<input value=""" & defVal & """ class=""" & DateClass & """ type=""text"" name=""" & strName & """ id=""" & strName & """ /> "
	
	'if DrawDateAddP then DrawDate_JQ = DrawDate_JQ & "</p>"
	
	DrawDateLabel = ""
end function

function GetDate_JQ(strname)
	dim d,m,y, ThisDate
	if UsingFileUp then
		ThisDate = upl.form(strname)
	else
		ThisDate = request.form(strname)
	end if
	'jhInfo ThisDate
	if isdate(ThisDate) then ThisDate = formatdatetime(ThisDate,1) else ThisDate = ""
	GetDate_JQ = ThisDate
	
end function






function getContactInfo(strType)

	dim ThisHeadContactInfo
	ThisHeadContactInfo = ""
	
	If (strType = "ACM") Then
		strContact = "DepartmentContactDetails"
	ElseIf (strType = "ASE") Then
		strContact = "DepartmentASEDetails"
	Else
		Exit Function
	End If

	dbConnect()

		
	sql = "SELECT TOP 1 "  & strContact & " AS ContactDetails FROM B2B_DepartmentContacts WHERE DepartmentCode = '" & acDetails.DepartmentCode & "'"

	set rs = db.execute(sql)
	if not rs.eof then
		ThisContactDetails = trim("" & rs("ContactDetails"))
	end if
	rsClose()
	
	if ThisContactDetails <> "" then
		ThisContactInfo = ThisContactDetails
	else
		ThisContactInfo = glACMDefaultContactDetails
	end if
	
	getContactInfo = ThisContactInfo

end function

function getAnnouncements(byref rCount)

	dim SelLang, c

	if glSystemAnnouncements then
	
		'session("B2B_ReadAnnouncements") = ""
		
		'HOLIDAY MESSAGES
		DispStart = formatdatetime(now, 1)
		DispEnd = formatdatetime(now, 1)
		SelLang = AcDetails.SelectedLanguageID
		'jhAdmin "SelLang = " & SelLang
		
		sql = "exec [spB2BNotifications] '" & formatdatetime(now,1) & "', 0" & SelLang & ", '" & AcDetails.AcCode & "'"
		
		jhAdmin sql
		x = getrs(sql, GCArr, GCCount)
		'jhAdmin "GCCount = " & GCCount

		gsStr = ""
		if GCCount >= 0 then
		
			'rCount = GCCount+1 
		
			for c = 0 to gcCount
					
				ThisID = GCArr(0,c)
				SDate = GCArr(1,c)
				EDate = GCArr(3,c)
				
				ReadAnnouncements = session("B2B_ReadAnnouncements")
				
				if instr(1, ReadAnnouncements, "|" & ThisID & "|") > 0 then
				
				else
					
					GCStandardMessageID = cdbl(GCArr(4,c))

					if "" & GCArr(6,c) = "" and GCStandardMessageID > 0 then
						ThisTitle = "" & GCArr(5,c)	

						if SelLang <> 0 then
							'jhAdmin "DO LOOKUP?"
							GCLookupLabel = "GLOBAL_MESSAGE_" & GCStandardMessageID
							LookupGCTitle = lne_NoEdit(GCLookupLabel)
							if LookupGCTitle <> "" and GCLookupLabel <> LookupGCTitle then
								ThisTitle = LookupGCTitle
							end if
						end if
						
					else
						ThisTitle = "" & GCArr(6,c)			
					end if
					
					ThisDesc = trim("" & GCArr(7,c))
					'jh "ThisDesc = " & ThisDesc
					
					DateStr = formatdatetime(SDate,2)
					if isdate(EDate) and EDate <> SDate then DateStr = DateStr & " - " & formatdatetime(EDate,2)
					
					if GCStandardMessageID = 0 and ThisTitle = "" then
						ThisTitle = "" & GCArr(9,c)						
					end if
					
					if GCStandardMessageID = 0 and ThisDesc = "" then
						ThisDesc = "" & GCArr(10,c)						
					end if			
					
					'jh "ThisDesc = " & ThisDesc
					
					if GCStandardMessageID > 0 and SelLang <> "0" then
						'jhAdmin "TRANSLATE"
						'ThisTitle = getlabel(ThisTitle)
						if ThisTitle = "" then ThisTitle = GCArr(9,c)	
						if ThisDesc = "" then ThisDesc = GCArr(10,c)	
					end if
					
					if session("et") <> "1" then
						ThisTitle = replace(ThisTitle, "[DATE RANGE]", DateStr)
						ThisDesc = replace(ThisDesc, "[DATE RANGE]", DateStr)
					end if
					

					ShowThis = true
					if ThisTitle = "" and ThisDesc = "" then
						ShowThis = false
					end if
					
					if ShowThis then
						
						rCount = rCount + 1
					
						gsStr = gsStr & "<div class=""row"">"
							gsStr = gsStr & "<div class=""span12"">"
								gsStr = gsStr & "<div class=""alert alert-success"" style=""background:#f0f0f0;"">"
								  gsStr = gsStr & "<button type=""button"" class=""close announceClose"" data-announce-id=""" & ThisID & """ data-dismiss=""alert"">&times;</button>"
								  gsStr = gsStr & "<h3>" & ThisTitle & "</h3>"
								  gsStr = gsStr & "" & addbr(ThisDesc) & ""
								  gsStr = gsStr & "<div class=spacer10></div>" 
								  gsStr = gsStr & "<a data-announce-id=""" & ThisID & """ data-dismiss=""alert"" class=""btn btn-warning btn-small announceClose"" href=""#"">" & lne("btn_dismiss_alert") & "</a>"
								  gsStr = gsStr & "<div class=spacer10></div>"
								  gsStr = gsStr & " <label for=""DontShowAgain_" & ThisID & """><input class=form-control type=checkbox id=""DontShowAgain_" & ThisID & """> <small>" & lne("btn_dismiss_alert_dont_show_again") & "</small></label>"
								gsStr = gsStr & "</div>"				
							gsStr = gsStr & "</div>"
						gsStr = gsStr & "</div>"			
						gsStr = gsStr & vbcrlf
					end if
				end if
			next
			
			'response.write gsStr
		end if
	end if

	getAnnouncements = gsStr
	
end function

function getDeliveryCost(pOrderTotal, pThreshold, pSTDDelivery)
	
	if pOrderTotal >= pThreshold then
		getDeliveryCost = 0
	else
		getDeliveryCost = pSTDDelivery
	end if
	
end function

function GetExDate(pDate)
	if isdate(pDate) then
		GetExDate = year(pDate) & right("0" & month(pDate),2) & right("0" & day(pDate),2) 
	end if
end function

function GetSysVar(pvar)
	dim sql,rs
	dbConnect()
	GetSysVar = 0
	sql = "SELECT * FROM END_SystemVariables WHERE SystemVariableName = '" & cleansql(pVar) & "'"
	'jh sql
	set rs = db.Execute(Sql)
	
	if not rs.eof then
		GetSysVar = rs("SystemVariableStatus")
	end if
	
	rsClose()
	
end function	

function SetSysVar(pvar, pVal)
	dim sql,rs
	dbConnect()
	sql = "UPDATE END_SystemVariables SET SystemVariableStatus = " & pVal & " WHERE SystemVariableName = '" & cleansql(pVar) & "'"
	'jh sql
	db.Execute(Sql)
	
end function	

function SetSysVarDate(pvar, pDate)
	dim sql,rs
	if isdate(pDate) then
		dbConnect()
		sql = "UPDATE END_SystemVariables SET SystemVariableDate = '" & pDate & "' WHERE SystemVariableName = '" & cleansql(pVar) & "'"
		'jh sql
		db.Execute(Sql)
	else
		jhError "Not a date!"
	end if	
end function	

function SetSysVarValue(pvar, pVal)
	dim sql,rs
	
	dbConnect()
	sql = "UPDATE END_SystemVariables SET SystemVariableTextValue = '" & pVal & "' WHERE SystemVariableName = '" & cleansql(pVar) & "'"
	'jh sql
	db.Execute(Sql)

end function	

function GetSysVarDate(pvar)
	dim sql,rs
	dbConnect()
	GetSysVarDate = ""
	sql = "SELECT SystemVariableDate FROM END_SystemVariables WHERE SystemVariableName = '" & cleansql(pVar) & "'"
	'jh sql
	set rs = db.Execute(Sql)	
	if not rs.eof then
		GetSysVarDate = rs("SystemVariableDate")
	end if	
	rsClose()
end function	

function GetSysVarValue(pvar)
	dim sql,rs
	dbConnect()
	GetSysVarValue = ""
	sql = "SELECT SystemVariableTextValue FROM END_SystemVariables WHERE SystemVariableName = '" & cleansql(pVar) & "'"
	'jh sql
	set rs = db.Execute(Sql)	
	if not rs.eof then
		GetSysVarValue = rs("SystemVariableTextValue")
	end if	
	rsClose()
end function	


function CleanSql(p)
	'jhAdmin "+ " & p
	cleanSql = p
	
	redim CleanArr(100)
	CleanArr(1) = ";"
	'CleanArr(2) = "'"
	CleanArr(3) = "--"
	CleanArr(4) = "/*"
	CleanArr(5) = "*/"
	CleanArr(6) = "xp_"
	CleanArr(7) = "%"
	
	for sCounter = 1 to ubound(CleanArr)
	
		if CleanArr(sCounter) <> "" then
			'jh CleanArr(c)
			cleanSql = replace(cleanSql,CleanArr(sCounter),"")
		end if	
	next

	cleanSql = validstr(cleanSql)

	cleanSql = trim(cleanSql)
	'jh "CLEANED SQL - " & CleanSql

end function

function getlabel(p)
	getlabel = p
end function


function topPageNav()

	exit function

	dim c, x, z, ThisPageID, NavOS, ThisParentID, ThisPageTitle, ShowThis
	
	sql = "SELECT PageID, ParentID, PageTitle, PageURL, (SELECT COUNT(*) FROM B2B_Pages psub WHERE pSub.ParentID = B2B_Pages.PageID) AS ChildCount, PageStatusID "
	sql = sql & "FROM B2B_Pages "
	sql = sql & "WHERE PageStatusID = 1 "

	sql = sql & "ORDER BY ParentID, PageZorder, PageID"
	
	'jh sql
	x = getrs(sql, PageArr, PageC)
	'jh "PageC = " & PageC	
	
	for c = 0 to PageC
		ThisPageID = PageArr(0,c)
		ThisParentID = PageArr(1,c)
		ThisPageTitle = PageArr(2,c)
		
		'jhx "ShowThis = " & ShowThis
		
		ThisPageURL = "" & PageArr(3,c)
		if ThisPageURL = "" then ThisPageURL = "/content/"
		ThisChildren = PageArr(4,c)
		if ThisParentID = 1 then
			'LookupName = "page_title_" & ThisPageID
			'DispPageTitle = lne(LookupName)		
			DispPageTitle = ThisPageTitle
			
			'jhInfo "ThisPageTitle = " & ThisPageTitle	
			
			if ThisChildren = 0 then
				NavOS = NavOS & "<li><a href=""" & ThisPageURL & """>" & DispPageTitle & "</a></li>"
			else
				NavOS = NavOS & "<li class=""dropdown"">"
				NavOS = NavOS & "<a class=""dropdown-toggle"" data-toggle=""dropdown"" href=""#"">" & DispPageTitle & " <b class=""caret""></b></a>"
				NavOS = NavOS & "<ul class=""dropdown-menu"" id=""DD" & ThisPageID & """>"
				for x = 0 to PageC
					if PageArr(1,x) = ThisPageID then
						
						ThisPageTitle = PageArr(2,x)
						
						ShowThis = true
						
						if "" & PageArr(0,x) = "28" then 'SHOP - CHECK DEPTS ACTIVE
							sql = "SELECT COUNT(*) AS ActiveShopDeptCount FROM B2B_ShopDept WHERE B2BShopDeptStatusID = 1 "
							'jhx sql
							set rs = db.execute(sql)
							if not rs.eof then
								ActiveShopDeptCount = rs("ActiveShopDeptCount")
							else	
								ActiveShopDeptCount = 0
							end if
							rsClose()
							'jhx "ActiveShopDeptCount = " & ActiveShopDeptCount
							if ActiveShopDeptCount <= 0 then 
								'jhx "HIDE " & ThisPageTitle
								ShowThis = false
							end if
						end if												
						
						LookupName = "page_title_" & PageArr(0,x)
						DispPageTitle = lne(LookupName)						
						
						'jh "SUB - " & ThisPageTitle
						ThisPageURL = "" & PageArr(3,x)			
						if ThisPageURL = "" then ThisPageURL = "/content/"		
						
						if ShowThis then 
							NavOS = NavOS & "<li><a href=""" & ThisPageURL & """>" & DispPageTitle & "</a></li>"
						end if
					end if
				next
				NavOS = NavOS & "</ul>"
				NavOS = NavOS & "</li>"
			end if
			
		end if
			
		

	next
	
	topPageNav = "<ul class=""nav pull-left"">" & NavOS & "</ul>"
end function

function getSelectedLanguage()
	
	getSelectedLanguage = request.cookies("B2B")("SelectedLanguageID")
	if getSelectedLanguage = "" then getSelectedLanguage = "0"
	
	call setSelectedLanguageID(getSelectedLanguage)
	
end function

function setSelectedLanguageID(pLanguageID)
	
	response.cookies("B2B")("SelectedLanguageID") = pLanguageID
	response.cookies("B2B").expires = dateadd("d", 28, now)	
	
end function

function getProductColorBar(pCode)
	
	if len(pCode) > 2 then pCode = right(pCode,2)
	
	fn = pCode & ".png"
	'jhAdmin "fn = " & fn
	imgURL = "/assets/images/prodbrowser/colorbars/"
	floc = server.mappath(imgURL) & "\"
	
	getProductColorBar = "BK.png"
	
	if imageExists(floc, fn) then
		'jh "FOUND BAR"
		getProductColorBar = imgURL & "" & fn		
	end if
	
end function

function getStatementLink(byval pStatement, byRef rStatementFN)
	
		if not isobject(fs) then
			set fs = server.createobject("Scripting.FileSystemObject")
		end if	
		
		dim StateNum, LookFor
		
        ' To find split payment customers ie. Those with Word (.docx) statement
		LookFor = glStatementPDFLoc & pStatement & "_st.docx"
		
		if fs.FileExists(LookFor) then
			getStatementLink = true		
			rStatementFN = LookFor
		else
            ' If nothing found, customer should be 'standard' paying (ie. .pdf statement)
			LookFor = glStatementPDFLoc & pStatement & "_st.PDF"
		
		    if fs.FileExists(LookFor) then
			    getStatementLink = true		
			    rStatementFN = LookFor
		    else
			    rStatementFN = ""
		    end if
		end if

end function

function getInvoiceFolder(pInvoice)

	dim subfolder
	subfolder = mid(pInvoice, 4, 2) & "0000"
	if isnumeric(subfolder) then subfolder = cdbl(subfolder) else subfolder = ""
	subfolder = cstr(subfolder)
	getInvoiceFolder = subfolder
	
end function

function getInvoiceLink(byval pInvoice, byRef rInvoiceFN)
	
		if not isobject(fs) then
			set fs = server.createobject("Scripting.FileSystemObject")
		end if	
		
		dim altFolder, InvNum, LookFor, LookForSub
		altFolder = getInvoiceFolder(pInvoice)		
		
		LookForSub = glInvoicePDFLoc & altFolder & "\" & pInvoice & ".PDF"
		'jhAdmin LookForSub
		LookFor = glInvoicePDFLoc & pInvoice & ".PDF"
		
		if fs.FileExists(LookForSub) then
			getInvoiceLink = true
			rInvoiceFN = LookForSub
			'jhAdmin "FOUND IN SUB"
		elseif fs.FileExists(LookFor) then
			'jhInfo "FOUND - " & LookFor
			getInvoiceLink = true		
			rInvoiceFN = LookFor
		else
			rInvoiceFN = ""
		end if
	
end function

function exDate(pd)
	if isdate(pd) then
		exDate = year(pd) & right("0" & month(pd),2) & right("0" & day(pd),2) 
	else
		exit function
	end if
end function

function FromExDate(pd)
	pd = trim("" & pd)
	FromExDate = right(pd,2) & "/" & mid(pd, 5, 2) & "/" & left(pd,4)
	'jh "FN FromExDate = " & FromExDate
	if isdate(FromExDate) then FromExDate = formatdatetime(FromExDate,2) else FromExDate = ""
end function

function RNull(p)

	if isnull(p) then 
		rNull = "" 
		exit function
	end if
	Set myRegExp = New RegExp
	myRegExp.IgnoreCase = True
	myRegExp.Global = True
	myRegExp.Pattern = "[^\x20-\x7E]"

	RNull = myRegExp.Replace(p, "")
	
	Set myRegExp = nothing
	
end function

function getAccountDownloadFolder()

	dim FolderName 
	
	FolderName = AcDetails.AcCode & "_" & AcDetails.GUID
	if FolderName = "_" then exit function
	
	CheckLocation = glAccountDownloadsFolder & FolderName & "\"
	
	set fs = server.CreateObject("Scripting.FileSystemObject")	
	if not fs.folderexists(CheckLocation) then
		fs.createfolder CheckLocation
	end if
	
	getAccountDownloadFolder = CheckLocation
	
end function

function getAccountDownloadFolderEPOS()

	dim FolderName 
	
    if isAdminUser() then
        FolderName = "ENDURASTAFF"
    else
        FolderName = AcDetails.AcCode & "_" & AcDetails.GUID
    end if
	
	if FolderName = "_" then exit function
	
	CheckLocation = glAccountDownloadsFolder & FolderName & "\"

	set fs = server.CreateObject("Scripting.FileSystemObject")	
	if not fs.folderexists(CheckLocation) then
		fs.createfolder CheckLocation
	end if

	getAccountDownloadFolderEPOS = CheckLocation
	
end function

function getAccountDownloadURL()

	dim FolderName 
	
	FolderName = AcDetails.AcCode & "_" & AcDetails.GUID
	getAccountDownloadURL = "/downloads/accounts/" & FolderName & "/"
	
end function

function getAccountDownloadURLEPOS()

	dim FolderName 
	
	if isAdminUser() then
        FolderName = "ENDURASTAFF"
    else
        FolderName = AcDetails.AcCode & "_" & AcDetails.GUID
    end if

	getAccountDownloadURLEPOS = "/downloads/accounts/" & FolderName & "/"
	
end function

function writeReportOutput (pLoc, pFn, pOs)
	'on error resume next
	
	'exit function
	
	set fs = server.createobject("Scripting.FileSystemObject")
	set f = fs.createtextfile(pLoc & pFn, true)
	f.write pOs
	f.close
	set f = nothing
	set fs = nothing
	
	on error goto 0
	
	if err.number = 0 then
		writeReportOutput = true
	end if
	
end function

function shortdate(pDate)
	if not isdate(pDate) then exit function
	shortdate = right("0" & day(pDate),2) & " " & monthname(month(pDate),true) ' & " " & right(year(pDate),2)
	'shortdate = formatdatetime(pDate,2)
end function

function DrawBigLanguageNotification()

		dim c, rc, sql

		response.cookies("BigLanguageNotification") = "1"
		response.cookies("BigLanguageNotification").expires = dateadd("d", 365, now)
		
		sql = "SELECT LanguageID, LanguageName, LanguageISO FROM END_Language WHERE B2BActive = 1 ORDER BY LanguageID "
		'jh sql
		x = getrs(sql, RecArr, rc)
		
		os = ""
		os = os & "<div class=well>"
		os = os & "<button type=""button"" class=""close"" data-announce-id="""" data-dismiss=""alert"">&times;</button>"
		os = os & "<h4>Select your language</h4>"
		os = os & "<p>Select your language for future visits to the Endura B2B</p>"
		os = os & "<p>"
		os = os & "NB you can always change your selected language using the drop down at the top of the page</p>"
		os = os & "</p>"
		os = os & "<div class=""row spacer20"">"
		
		for c = 0 to rc
			ThisLanguageID = RecArr(0,c)
			ThisLanguageName = RecArr(1,c)
			ThisLanguageISO = RecArr(2,c)
			'jh RecArr(1,c)
		
			os = os & "<div class=""span2 spacer5"">"
			os = os & "<a href=""/set_options.asp?BigLanguageNotification=1&SetLanguageID=" & ThisLanguageID & """ class=""legible btn btn-mini btn-cinverse""><img src=""/assets/images/icons/country/" & ThisLanguageISO & ".png""> " & ThisLanguageName & "</a>"
			os = os & "&nbsp;"
			os = os & "</div>"
		
		next
				
		os = os & "</div>"
		os = os & "</div>"						
	
		DrawBigLanguageNotification = os

end function

function killArray(byref pArray)
	if isarray(pArray) then 
		'jhAdmin "ERASING ARRAY!"
		erase pArray
		set pArray = nothing
	else
		'jhAdmin "NOT ARRAY!"
	end if
end function

Function InsertJob(pTaskName, PContents, pFullVersion)
	dim querysql, CurrentActiveStatus
	
	'if isjh() then CurrentActiveStatus = 0 else CurrentActiveStatus = 1
	CurrentActiveStatus = 1
	querysql = "INSERT INTO ExchequerQueue.dbo.ExchequerQueue (Name, Contents, RunType, IsFullVersion, CurrentActiveStatus) VALUES ('" & pTaskName & "', '" & PContents & "', 'IMMEDIATE', " + CStr(pFullVersion) + ", 0" & CurrentActiveStatus & ")"
	
	if isjh() and 1 = 0 then
		jhAdmin "NOT SUBMITTING - " & pTaskName
		jhXML pContents
	else
		db.execute(querysql)
	end if
end function

Function CheckDataLoad(pThisUserID, pTaskName)
	dim querysql
	querysql = "SELECT Name FROM ExchequerQueue.dbo.ExchequerQueue WHERE Name = '" & pTaskName  & "' AND Contents LIKE '%" + pThisUserID + "%' AND CurrentActiveStatus = 1"	
	set rs = db.execute(querysql)
	if not (rs.bof AND  rs.eof ) then
		CheckDataLoad = False
	else
		CheckDataLoad = True
	end if
	rsClose()
end function


function ExportBE(byval pName, byval pContents)

	const adTypeBinary = 1
	const adSaveCreateOverwrite = 2
	const adModeReadWrite = 3

	'pContents = HTMLDecode(pContents)
	
	'jh pName
	'jhAdmin pContents
	
	if 1 = 1 then
		DecodedContents = CharDecoder(pContents)
	end if	
	
	Set objStream = server.CreateObject("ADODB.Stream")
	objStream.Open 
	objStream.CharSet = "UTF-8"
	objStream.WriteText(DecodedContents)
	
	objStream.SaveToFile pName , adSaveCreateOverWrite
	objStream.Close
	set objStream = nothing
	
	ExportBE = true

end function


function isDealer()	

	dim CheckCode, CheckGUID
	
	isDealer = false

	if session("B2B_AcCode") <> "" then 
		isDealer = true
	else
	
		'jhStop

		CheckCode = request.cookies("Persistence")("AcCode")
		CheckGUID = request.cookies("Persistence")("GUID")		
		LoginPersistence = request.cookies("Persistence")("Allow")		
		
		if CheckCode <> "" and CheckGUID <> "" and LoginPersistence <> "" then
		
			dbConnect()
			
			'jhAdmin "CheckAdminID_" & CheckCode & "_" & CheckGUID & "_" & LoginPersistence				
			sql = "SELECT TOP 1 * FROM END_EbusClients WHERE EbusClientCode = '" & validstr(CheckCode) & "' AND LoginGUID_PERSISTENT  = '" & validstr(CheckGUID) & "' AND EBUSClientStatusID = 1 "
			'jh sql
			set rs = db.execute(sql)
			if not rs.eof then
				'jh "FOUND!"
				isDealer = true
				session("B2B_AcCode") = CheckCode
				'jhInfo ".AUTO LOGIN IN DEALER - " & CheckCode
			end if
			rsClose()
			
		end if
	
		
	end if
	
end function


function isAdminUser()

	dim CheckCode, CheckGUID, sql

	isAdminUser = false
	
	'jh "session(""B2B_AdminUserID"") = " & session("B2B_AdminUserID")
	
	'exit function
	
	if session("B2B_AdminUserID") <> "" then 
		isAdminUser = true
	else
	
		'jhInfo "TRY AUTO LOGIN"
		
		CheckCode = cleannum(request.cookies("Persistence")("A_ID"))
		CheckGUID = request.cookies("Persistence")("A_GUID")		
		LoginPersistence = request.cookies("Persistence")("Allow")		
		
		'jh "CheckAdminID_" & CheckCode & "_" & CheckGUID & "_" & LoginPersistence				
		
		if CheckCode <> "" and CheckGUID <> "" and LoginPersistence <> "" then
		
			dbConnect()
			
			'jhAdmin "CheckAdminID_" & CheckCode & "_" & CheckGUID & "_" & LoginPersistence				
			sql = "SELECT TOP 1 * FROM END_AdminUsers WHERE AdminID = 0" & validstr(CheckCode) & " AND LoginGUID_PERSISTENT  = '" & validstr(CheckGUID) & "' AND AdminStatusID = 1 "
			'jh sql
			set rs = db.execute(sql)
			if not rs.eof then
				isAdminUser = true
				session("B2B_AdminUserID") = CheckCode
			end if
			rsClose()
			'jhInfo "AUTO LOGIN IN ADMIN - " & CheckCode
		end if	
	
	end if
end function

function GetText(PassedID)
	
	GetText = PassedID
	
	dim c, fs, FName, ThisURL
	
	set fs = server.CreateObject("Scripting.FileSystemObject")
	
	RootDir = server.MapPath ("/") & "\"
	TextPath = "assets\text\"	
	
	'jh "LANG = " & selLang
	
	
	'jhAdmin "glOrigB2BContentLocation = " & glOrigB2BContentLocation
	'jhAdmin "AcDetails.SelectedLanguageID = " & AcDetails.SelectedLanguageID
	
	FName = glOrigB2BContentLocation & AcDetails.SelectedLanguageID & "\" & PassedID & ".txt"
	
	'jhAdmin "FName = " & FName
	
	
	if fs.FileExists(FName) then
		'jhAdmin "FOUND"
		set f = fs.opentextfile(FName,1)
	
		if not f.atendofstream then	
			GetText = f.ReadAll
		end if	
		f.close
		set f = nothing
	
	else
		'jhAdmin "NOT FOUND"
	end if

	ThisURL = request.ServerVariables("PATH_INFO")
	'jhAdmin "ThisURL = " & ThisURL
	dim ThisQS
	ThisQS = request.ServerVariables("QUERY_STRING")
	'jhAdmin "ThisQS = " & ThisQS
	if ThisQS <> "" then
		ThisURL = ThisURL & "?" & ThisQS
		ThisURL = server.URLEncode(ThisURL)
	end if
	
	set fs = nothing
	
end function

function SwapBG()
	if BGSwapColor = glLightcolor then BGSwapColor = glDarkColor else BGSwapColor = glLightColor
	SwapBG = BGSwapColor
end function

Function SetASPNETSession(pThisUserID)
	' The block applies the mechnanism to share a session state between ASP and ASP.NET
	Set objCommand = CreateObject("ADODB.Command")
	
	With objCommand
		.CommandText = "DECLARE @LoginGUID uniqueidentifier; SELECT @LoginGUID = NEWID();  UPDATE END_EbusClients SET LoginGUID = @LoginGUID WHERE EbusClientCode = ?; SELECT ? = CAST(@LoginGUID AS varchar(50));"
		.CommandType = adCmdText
		.ActiveConnection = db
		.Parameters.Append .CreateParameter("@UserID", adVarChar, adParamInput, 50, pThisUserID)
		.Parameters.Append .CreateParameter("@NETSession", adVarChar, adParamOutput, 50)
		.Execute
		NETSession = .Parameters("@NETSession").Value
	End With
	Set objCommand = Nothing
	
	Response.Cookies("NETUserSession").Domain = "endura.co.uk"
	Response.Cookies("NETUserSession") = NETSession				
	
End Function

function basketDelete(byval pSKU, byVal pAcCode)
	sql = "exec [spB2BBasketAdd] '" & validstr(pSKU) & "', 0, '" & pAcCode & "', ''"		

	db.execute(sql)
end function

function basketDelete_PRODUCT(byval pProductID, byVal pAcCode)

	sql = "DELETE b FROM B2B_Basket b INNER JOIN SML_SKUS ss ON ss.SKU = b.SKU "
	sql = sql & "WHERE ss.ProductID = 0" & pProductID & " AND b.AcCode = '" & pAcCode & "' AND b.B2BOrderID = 0 "

	db.execute(sql)
	
end function

function basketAdd(byval pSKU, byval pQty, byVal pAcCode)
	pQty = cleannum(pQty)	
	if pQty <> "" then
		pQty = cdbl(pQty)
		if pQty < 0 then pQty = 0
		
		sql = "exec [spB2BBasketAdd] '" & validstr(pSKU) & "', 0" & pQty & ", '" & pAcCode & "', ''"				

		db.execute(sql)						
	
	end if
end function

function jhHTML(byval pStr)
	jhHTML = replace("" & pStr, "<", "&lt;")	
end function

function sqlDate(pDate)

	if isdate(pDate) then
		sqlDate = year(pDate) & "-" & right("0" & month(pDate),2) & "-" & right("0" & day(pDate),2)
	else
		sqlDate = ""
	end if
	
end function

function ActivityTrail(pType, pInfo)	
	'jh "ADD AUDIT TRAIL"
	
	dim DealerCode, AdminID, ThisD, ThisIP
	
	if isDealer then
		DealerCode = AcDetails.AcCode
	end if
	if isAdminUser then
		AdminID = AdminDetails.AdminID
	end if	
		
	ThisD = formatdatetime(now,1) & " " & formatdatetime(now,3)
	ThisIP = request.ServerVariables("REMOTE_HOST")
	
	'jh ThisIP	
		
	sql = "INSERT INTO B2B_Activity (ActivityType, ActivityDate, AdminID, AcCode, IPAddress, ActivityDetails) VALUES ("

	sql = sql & "'" & cleanSql(pType) & "', "
	sql = sql & "'" & cleanSql(thisD) & "', "
	sql = sql & "0" & cleanSql(AdminID) & ", "
	sql = sql & "'" & cleanSql(DealerCode) & "', "
	sql = sql & "'" & cleanSql(ThisIP) & "',"
	sql = sql & "'" & cleanSql(pInfo) & "'"	
	
	sql = sql & ")"
	
	jhAdmin sql
	'db.Execute(Sql)
	if isjh then jhstop
end function

function cleancheck(byval p)
	p = "|" & replace(p, ",", "|") & "|"
	p = replace(p, " ", "")
	p = replace(p, "||", "|")
	cleancheck = p
end function


function localDate(byval pDate)
	if not isdate(pDate) then exit function
	if glCustom = "US" then
		localDate = right("0" & month(pDate),2) & "/" & right("0" & day(pDate),2) & "/" & year(pDate)
	else
		localDate = formatdatetime(pDate,2)
	end if
end function


Function StripHTML(byval pText)
    dim strResult, RegularExpressionObject
    Set RegularExpressionObject = New RegExp
    With RegularExpressionObject
    .Pattern = "<[^>]+>"
    .IgnoreCase = True
    .Global = True
    End With
    strResult = RegularExpressionObject.Replace(pText ,  "")
	strResult = replace(strResult,  "&nbsp;", " ")
    strResult = trim("" & strResult)
    Set RegularExpressionObject = nothing
    StripHTML = strResult
End Function

function getProdName_TRANSLATED(pProduct)
	
	dim sql, ProductName, ProductName_TRANS, rs
		
	sql = "SELECT sp.ProductID, sp.ProductName, t.ProductName_TRANS FROM SML_Products sp LEFT JOIN (SELECT ProductID, ProductName AS ProductName_TRANS FROM SML_ProductTranslations WHERE LanguageID = 0" & AcDetails.SelectedLanguageID & ") t ON t.ProductID = sp.ProductID "
	
	if cleannum(pProduct) = "" then 
		sql = sql & "WHERE sp.ProductCode = '" & validstr(pProduct) & "' "
	else	
		sql = sql & "WHERE sp.ProductID = 0" & pProduct
	end if
	
	'jhAdmin sql
	'exit function
	
	'dbConnect()
	set rs = db.execute(sql)
	if not rs.eof then
		ProductName = "" & rs("ProductName")
		ProductName_TRANS = "" & rs("ProductName_TRANS")
	end if
	rsClose()
	
	if ProductName_TRANS <> "" then getProdName_TRANSLATED = ProductName_TRANS else getProdName_TRANSLATED = ProductName
	
	'jhAdmin "getProdName_TRANSLATED = " & getProdName_TRANSLATED
	
end function

function DrawCombo(SelectName, TableName, IDField, TextField, WhereStr, ComboVal, OrderField , Descending, LimitToList, NotInListText)

	'jh "UPDATE DRAWCOMBO"
		'jh ChangeSubmit
		dim ls 
		set ls = new strConcatStream

        ' DL 01/12/2015
        if TableName = "END_ForwardOrderWindow" then
            ChangeSubmit = "FOPriceChange(this.form.ForwardOrderWindowID, this.form.SelAc)"
        end if
        ' END
	
		dim RecArr
		'jh TableName
		if ucase(TableName) = "S2_STATUS" and LimitToList then 
			DefField = ", isDefault"
			GetDefault = true
			'jh GetDefault
		end if
		
		if IDField = TextField and instr(1, IDField, "AS ") = 0 then 
			IDFIeld = IDField  & " AS ComboIDField"
			TextField = TextField  & " AS ComboTextField"
		end if
		
		if UseDistinct then
			DistinctStr = "DISTINCT "
		end if
		UseDistinct = false
		sql = "SELECT " & DistinctStr & " " & IDField & "," & TextField & DefField & " FROM " & TableName 
		if WhereStr <> "" then
			sql = sql & " WHERE " & WhereStr
		end if
		if OrderField <> "" then
			sql = sql & " ORDER BY " & OrderField
			if Descending then
				sql = sql & " DESC"
			end if
		end if		
		sql = sql & ";"
		
		'jhAdmin sql
		
		set combors = db.Execute(sql)
		
		'jh sql
		
		if not combors.eof then
			
			RecArr = combors.GetRows()
			cc = ubound(RecArr,2)
		
		else
		
			cc = -1
			DrawCombo = "---"
		
		end if
		
		combors.close
		set combors = nothing
		
		DrawCombo = ""
		
        if ChangeSubmit <> "" then ChangeStr = "onchange=""" & ChangeSubmit & """"
		'if ChangeSubmit <> "" then ChangeStr = "onchange=""document." & ChangeSubmit & ".submit();"""


		if EventStr <> "" then ChangeStr = EventStr
		EventStr = ""
		AllowMulti = false
		ComboMultiStr = ""
		if ComboRows > 0 then
			ComboMultiStr = " MULTIPLE SIZE=" & ComboRows & " "
			ComboRows = 0
			AllowMulti = true
		end if
		'jh "ComboMultiStr = " & ComboMultiStr
		
		pInd = pInd+1
		'DrawCombo = DrawCombo & "<select TABINDEX=" & pInd & " " & ChangeStr & " class=FormSelect style="";"" name=""" & SelectName & """ id=""" & SelectName & """>"
		ls.add "<select xTABINDEX="		
		ls.add pInd
		ls.add " " 
		
		if "" & curTabIndex <> "" then
			ls.add " TABINDEX=" & curTabIndex & " "
		end if
		
		ls.add ComboMultiStr
		ls.add ChangeStr
		ls.add " class=FormSelect style=""font-size:9pt;"
		if comboStyleStr <> "" then
			ls.add comboStyleStr
			comboStyleStr = ""
		end if
		ls.add """ name="""
		ls.add SelectName
		ls.add """ id="""
		ls.add SelectName
		ls.add """>"	
		
		if not LimitToList then
			if NotInListText = "" then
				'DrawCombo = DrawCombo & "<option value="""">Not in List...</option>"
				ls.add "<option value="""">Not in List...</option>"
			else
				'DrawCombo = DrawCombo & "<option value="""">" & NotInListText & "</option>"			
				ls.add "<option value="""">" & NotInListText & "</option>"
			end if
		end if

        
		
		'if LimitToList = 2 then
		'	DrawCombo = DrawCombo & "<option value=""NEW_COMPANY"">[NEW COMPANY]</option>"
		'end if
		
		for RecWalker = 0 to cc
			ls.add "<option value=""" & RecArr(0,RecWalker) & """"
			ls.add RecArr(0,RecWalker)
			ls.add """"
			
			if "" & RecArr(0,RecWalker) = trim(cstr("" & ComboVal)) then
				ls.add " SELECTED"
			elseif AllowMulti and instr(1, ComboVal, "|" & RecArr(0,RecWalker) & "|") > 0 then
				ls.add " SELECTED"	
			elseif ComboVal = "" and GetDefault then	
				'jh "GET DEFAULT"
				if RecArr(2,RecWalker) = 1 then
					ls.add " SELECTED"
				end if
			end if
			'DrawCombo = DrawCombo & ">" & RecArr(1,RecWalker) & "</option>"			
			ls.add ">"
			ls.add RecArr(1,RecWalker)
			ls.add "</option>"		
		next
		
		'DrawCombo = DrawCombo & "</select>."		
		ls.add "</select>."	
    	
		drawcombo = ls.value
		
		ChangeSubmit = ""
		set ls = nothing
end function

function updateRanging_DEALER(pRangingID)
	dim sql
	sql = "UPDATE B2B_Ranging SET DealerLastUpdated = GETDATE() WHERE RangingID = 0" & pRangingID & ""
	'jh sql
	db.execute(sql)	
	'jhInfoNice "DEALER UPDATED!", "The dealer last updated datetime has been set!"
end function

function viewedRanging_DEALER(pRangingID)
	dim sql
	sql = "UPDATE B2B_Ranging SET DealerLastViewed = GETDATE() WHERE RangingID = 0" & pRangingID & ""
	'jh sql
	db.execute(sql)	
	'jhInfoNice "DEALER UPDATED!", "The dealer last viewed datetime has been set!"
end function


Function CheckIsKSIT(ThisSKU, pList)

	CheckIsKSIT = False
 
	arrKSIT = Split(pList,",")

 	for each strKSITSKU in arrKSIT
		if (strKSITSKU = ThisSKU) Then
			CheckIsKSIT = True
			Exit For
		End If
	next

End Function

function ShowOverwrittenSizeName(pSKU, pCheckSize)
	
	strOverwrittenSizeName = pCheckSize
							
							If (pSKU = "E8103BA/3" OR pSKU = "E8103BA/4" OR pSKU = "E8103BA/5" OR pSKU = "E8103BA/6" OR pSKU = "E8103BA/7"  OR pSKU = "E8103BK/3" OR pSKU = "E8103BK/4" OR pSKU = "E8103BK/5" OR pSKU = "E8103BK/6" OR pSKU = "E8103BK/7" OR pSKU = "E8103OT/3" OR pSKU = "E8103OT/4" OR pSKU = "E8103OT/5" OR pSKU = "E8103OT/6" OR pSKU = "E8103OT/7" OR pSKU = "E8103GF/3" OR pSKU = "E8103GF/4" OR pSKU = "E8103GF/5" OR pSKU = "E8103GF/6" OR pSKU = "E8103GF/7" OR pSKU = "E8103MO/3" OR pSKU = "E8103MO/4" OR pSKU = "E8103MO/5" OR pSKU = "E8103MO/6" OR pSKU = "E8103MO/7" OR pSKU = "E8103PA/3" OR pSKU = "E8103PA/4" OR pSKU = "E8103PA/5" OR pSKU = "E8103PA/6" OR pSKU = "E8103PA/7" OR pSKU = "E6170BK/2" OR pSKU = "E6170BK/3" OR pSKU = "E6170BK/4" OR pSKU = "E6170BK/5" OR pSKU = "E6170BK/6" OR pSKU = "E6170GK/2" OR pSKU = "E6170GK/3" OR pSKU = "E6170GK/4" OR pSKU = "E6170GK/5" OR pSKU = "E6170GK/6" OR pSKU = "E6170YS/2" OR pSKU = "E6170YS/3" OR pSKU = "E6170YS/4" OR pSKU = "E6170YS/5" OR pSKU = "E6170YS/6" )Then 
								strOverwrittenSizeName = pCheckSize & " (Standard Fit)"
							ElseIf (pSKU = "E3192GK/3" OR pSKU = "E3192GK/4" OR pSKU = "E3192GK/5" OR pSKU = "E3192GK/6" OR pSKU = "E3192GK/7" OR pSKU = "E3192BK/3" OR pSKU = "E3192BK/4" OR pSKU = "E3192BK/5" OR pSKU = "E3192BK/6" OR pSKU = "E3192BK/7" OR pSKU = "E3192RR/3" OR pSKU = "E3192RR/4" OR pSKU = "E3192RR/5" OR pSKU = "E3192RR/6" OR pSKU = "E3192RR/7" OR pSKU = "E3192WH/3" OR pSKU = "E3192WH/4" OR pSKU = "E3192WH/5" OR pSKU = "E3192WH/6" OR pSKU = "E3192WH/7" OR pSKU = "E3192BE/3" OR pSKU = "E3192BE/4" OR pSKU = "E3192BE/5" OR pSKU = "E3192BE/6" OR pSKU = "E3192BE/7" OR pSKU = "E3192YV/3" OR pSKU = "E3192YV/4" OR pSKU = "E3192YV/5" OR pSKU = "E3192YV/6" OR pSKU = "E3192YV/7" ) Then
								strOverwrittenSizeName = pCheckSize & " (Athletic Fit)"
							ElseIf (pSKU = "E4070BK/W2" OR pSKU = "E4070BK/W3" OR pSKU = "E4070BK/W4" OR pSKU = "E4070BK/W5" OR pSKU = "E4070BK/W6" OR pSKU = "E4070BK/W7" OR pSKU = "E4071BK/W2" OR pSKU = "E4071BK/W3" OR pSKU = "E4071BK/W4" OR pSKU = "E4071BK/W5" OR pSKU = "E4071BK/W6" OR pSKU = "E4071BK/W7" ) Then
								strOverwrittenSizeName = Mid(pCheckSize,1,InStr(pCheckSize," ")) & " (Wide Pad)"

							End If
	ShowOverwrittenSizeName = strOverwrittenSizeName
end function

%>



