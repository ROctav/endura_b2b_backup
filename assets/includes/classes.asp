<%

Class AdminAccountDetails

	private x, sql, c, AdminArr, AdminC
	private rAcCode
	
	Private rAdminID, rUserName, rDepartmentCode
	Private rAllowedLanguages, rGUID, CheckAdminGUID, CheckAdminID
	Private rGroupMemberships, CheckAdminLogin, LoginPersistence
	
	'called at creation of instance
	Private Sub Class_Initialize()
	
		'jhAdmin sql
		
		if not isAdminUser() then exit sub
		
		rAdminID = 0
		rUserName = ""
		rDepartmentCode = ""
		rAllowedLanguages = ""
		rGroupMemberships = ""

		rAdminID = session("B2B_AdminUserID")		
		'jh "rAdminID (session) = " & rAdminID
		
		if rAdminID <> "" then
			
			dbConnect()
			
			sql = "exec [spB2BAdminDetails] 0" & rAdminID & " "		
			'jhAdmin sql		
			x = getrs(sql, AdminArr, AdminC)
			
			'jhAdmin "AdminC = " & AdminC
			
			'jhInfo "INITIALISED - " & acCode
			
			rGroupMemberships = "|"
			
			if AdminC >= 0 then
			
				if CheckAdminLogin then
					'jh "AdminArr(8,0) = " & AdminArr(8,0)
					if CheckAdminGUID <> "" and CheckAdminGUID = "" & AdminArr(8,0) then
						AutoLoginValid = true
						jhAdmin "*** AUTO LOGGED IN (ADMIN)"
						session("B2B_AdminUserID") = rAdminID						
					else
						'jhError "NOT AUTO LOGGED IN"
						rsClose()
						exit sub
					end if
				end if			
			
				rAdminID = AdminArr(0,0)
				rUserName = AdminArr(2,0)
				rDepartmentCode = AdminArr(3,0)
				rAllowedLanguages = AdminArr(4,0)
				
				rGUID = AdminArr(8,0)
				
				for c = 0 to AdminC				
					rGroupMemberships = rGroupMemberships & AdminArr(7,c) & "|"				
				next
				
				if LoginPersistence = "1" then					
					'jh "SET AUTO LOGIN COOKIES!"					
					response.cookies("Persistence")("A_ID") = rAdminID
					response.cookies("Persistence")("A_GUID") = rGUID					
				end if	
			end if
		end if
		
	End Sub
	
	Private Sub Class_Terminate()
		'jhAdmin "DESTROY ADMIN USER CLASS"
		if isarray(AdminArr) then 
			erase AdminArr			
		end if
	End Sub
	
	Public Property Get AdminID
		AdminID = rAdminID
	End Property	
	
	Public Property Get UserName
		UserName = rUserName
	End Property	
	
	
	Public Property Get DepartmentCode
		DepartmentCode = rDepartmentCode
	End Property		
	
	Public Property Get GroupMemberships
		GroupMemberships = rGroupMemberships
	End Property		

	Public Property Get AllowedLanguages
		AllowedLanguages = rAllowedLanguages
	End Property	

	Public Property Get GUID
		GUID = rGUID
	End Property	
	
	Public Property Get isAllowedLanguage(pLanguageID)
		if instr(1, rAllowedLanguages, "|" & pLanguageID & "|") > 0 then
			isAllowedLanguage = true
		else
			isAllowedLanguage = false
		end if
	End Property	
	
	Public Property Get isGroupMember(pGroupID)
		if instr(1, rGroupMemberships, "|" & pGroupID & "|") > 0 then
			isGroupMember = true
		else
			isGroupMember = false
		end if
	End Property		
	
		
	
end class


Class DealerAccountDetails

	private rAcCode
	private rAcStatus

	private rOnHold
	
	private rCompanyName
	
	Private rCreditLimit
	Private rCurrentBalance
	Private rAvailableCredit
	Private rCostBand
	Private rSRCapActive
	Private rVATCode
	Private rSRCapPct
	Private rBBLDealerStatus

	Private rCurrencyID
	
	Private rCurrencySymbol
	Private rCurrencyDecimals
	Private rVatApplicable
	
	Private rDeliverySKU
	Private rDeliveryCOST
	private rVatRate
	
	private rDepartmentCode
	private rCostCentre
	
	private rFreeDeliveryThreshold
	
	private rDeliveryZoneID
	
	private rRiderCard, rRiderCardID
	
	private rContactEmail, rContactEmailArr
	
	private rGUID, CheckAutoLogin, rMandatoryPONumber, rSagePayUser, rContactName, rSagePayCurrency, rDailyRate, rCurrencyName, rExchequerCurrencyCode, rParentAcCode, rOriginalBalance, rDiscountPCT, rSeparateChildSR
	private rDeliveryTBC
	
	private LoginPersistence
	
	private rDealerDiscount
	
	
	'called at creation of instance
	Private Sub Class_Initialize()
	
		dbConnect()
		'jhAdmin "INITIALISE DEALER CLASS"
		
		if not isDealer() then exit sub
		
		rAcCode = session("B2B_AcCode")
				
		sql = "exec spB2BAccountDetails '" & rAcCode & "' "
		'jhAdmin sql
		
		rCreditLimit = 0
		rCurrentBalance = 0
		rAvailableCredit = 0
		rCostBand = "A"
		rCurrencyID = "1"
		rDeliverySKU = 0
		rDeliveryCost = 0
		rAcStatus = "OK"
		rOnHold = false
		'jhStop
		set rs = db.execute(sql)
				
		if not rs.eof then
		
			rCompanyName = rs("CompanyName")

			rAcStatus = "" & rs("AcStatus")
			if rAcStatus = "HOLD" then
				rOnHold = true
			end if
					
			rCreditLimit = "" & rs("CreditLimitNativeCurrency")
			rCurrentBalance = "" & rs("BalanceNativeCurrency")
			rAvailableCredit = "" & rs("CreditAvailableNativeCurrency")
		
			if rCreditLimit = "" then
				rCreditLimit = "" & rs("OriginalCreditLimit")
				rCurrentBalance = "" & rs("OriginalBalance")				
				rAvailableCredit = convertNum(rCreditLimit) - convertNum(rCurrentBalance)
			end if
		
			'jhInfo "FOUND!"
			if rCreditLimit = "" then rCreditLimit = cdbl(rs("CreditLimit")) else rCreditLimit = cdbl(rCreditLimit)
			if rCurrentBalance = "" then rCurrentBalance = cdbl(rs("Balance")) else rCurrentBalance = cdbl(rCurrentBalance)
			if rAvailableCredit = "" then rAvailableCredit = cdbl(rs("AvailableCredit")) else rAvailableCredit = cdbl(rAvailableCredit)
						
			rCostBand = rnull("" & rs("DiscountBand"))
			if rCostBand = "" or rCostBand = "%" then rCostBand = "A"

			rSRCapActive = "" & rs("SRCapActive")
			rVATCode =  "" & rs("cuVATCode")
			rSRCapPct = "" & rs("SRCapPct")
			
			rBBLDealerStatus = "" & rs("BBLDealerStatus")
			rDeliveryZoneID = rs("DeliveryZoneID")
			
			rCurrencyID = "" & rs("Currency")
			
			rDailyRate = rs("CurrencyDailyRate")
			rCurrencyName = rs("CurrencyDesc")
			 
			rDealerDiscount = convertnum(rs("DealerDiscount"))
			
			
			SymbolSuccess = GetCurrencyDetails(rCurrencyID,rCurrencySymbol,rCurrencyDecimals)
			
			rDeliverySKU = "" & rs("DeliverySKU")			
			rDeliveryCost = "" & rs("OverridePrice")
			
			if convertnum(rDeliveryCost) < 0 then
				rDeliveryTBC = true
			else
				rDeliveryTBC = false
			end if
			
			rFreeDeliveryThreshold = cdbl(rs("OrderThreshold"))
			
			session("B2B_CurrencySymbol") = rCurrencySymbol
			session("B2B_CurrencyDecimals") = rCurrencyDecimals
				
			if rAvailableCredit < 0 then rAvailableCredit = 0
			
			cuVatCode = "" & rs("cuVatCode")
			if cuVatCode <> "S" AND cuVatCode <> "8" then 
				cuVatCode = false 
			else 
				rVatApplicable = true
			end if
			
			if rVatApplicable then
				if (cuVatCode = "S") Then
					rVatRate = glVatRate
				elseif (cuVatCode = "8") Then
					rVatRate = glVAT8Rate
				else 
					rVatRate = glVatRate
				end if
			else
				rVatRate = 0
			end if
			
			rDepartmentCode = rs("DepartmentCode")
			rCostCentre = rs("CostCentre")
			
			rContactEmail = trim("" & rs("ContactEmail"))
			
			rContactEmail = replace(rContactEmail, " ", "")
			rContactEmailArr = split(rContactEmail, ",")
			
			rContactName = trim("" & rs("Contact"))
			
			rSagePayUser = rs("SagePayUser")			
			
			rRiderCardID = trim("" & rs("RiderCardID"))
			
			rDiscountPCT = convertNum(rs("DiscountPCT"))
			

			if cleannum(rRiderCardID) <> "" then 
				rRiderCard = true 
				rContactEmail = trim("" & rs("RiderCardEmail"))
				rContactName = trim("" & rs("RiderCardName"))
				rCreditLimit = trim("" & rs("ValueLimit"))
				rCurrentBalance = 0
				rAvailableCredit = rCreditLimit - rCurrentBalance
				rDealerDiscount = convertnum(rs("DiscountPct")) / 100
			else 
				rRiderCard = false
			end if
			
			
			rGUID = "" & rs("LoginGUID_PERSISTENT")
						
			rMandatoryPONumber = "" & rs("MandatoryPONumber")			
			
			rParentAcCode = "" & rs("ParentAcCode")			
						
			rExchequerCurrencyCode = rs("ExchequerCurrencyCode")
			
			if rParentAcCode <> "" then
				sql = "select ISNULL(IsSeparateChildSR, 0) AS IsSeparateChildSR from END_CRMContacts WHERE ContactFirstName = 'IT System Profile' AND ClientID = '" & rParentAcCode & "'"
				jhAdmin sql
				set rs = db.execute(sql)
				if not rs.eof then
					IsSeparateChildSR = rs("IsSeparateChildSR")
				end if
				rsClose()
				jhAdmin "IsSeparateChildSR = " & IsSeparateChildSR
				if IsSeparateChildSR then
					rSeparateChildSR = 1
				else
					rSeparateChildSR = 0
				end if					
			end if
			
			if glCheckDeliveryOverrides  then

				
				sql = "SELECT TOP 1 DeliveryThresholds FROM ExchequerOfferGroup WHERE  IsActive = 1 AND OfferGroupID IN "
				sql = sql & "	(SELECT OfferID FROM ExchequerSOROffer WHERE AcCode = '" & rAcCode & "') "
				sql = sql & "	and ISNULL(DeliveryThresholds,'') <> ''  ORDER BY OfferGroupID"
				

				set rs = db.execute(sql)			
				if not rs.eof then

					ThresholdOverrideFound = true


					DelCThresholds = Split("" & rs("DeliveryThresholds"),"|")

					for each delct in DelCThresholds
						If Len(delct) > 0 Then
						
						DelVThresholds = Split("" & delct,",")

						if (DelVThresholds(0) = CStr(rDeliveryZoneID)) Then
							rFreeDeliveryThreshold   = convertnum(DelVThresholds(1))
						End If

						End If

					 next
				end if
				
				rsClose()
			end if			
			
			if LoginPersistence = "1" and rGUID <> "" then
				'jhAdmin "SET AUTO LOGIN COOKIES! (" & LoginPersistence & ")"
				response.cookies("Persistence")("AcCode") = rAcCode
				response.cookies("Persistence")("GUID") = rGUID				
			end if	
		end if
		rsClose()
				
		'jhInfo "INITIALISED - " & acCode
		
	End Sub
	
	Private Sub Class_Terminate()
		'jhAdmin "DESTROY DEALER CLASS"
	
	End Sub
	
	Public Property Get AcCode
		AcCode = rAcCode
	End Property	

	Public Property Get AcStatus
		AcStatus = rAcStatus
	End Property
	
	Public Property Get OnHold
		OnHold = rOnHold
	End Property
	
	'return the concatenated string
	Public Property Get CreditLimit
		CreditLimit = rCreditLimit
	End Property

	
	Public Property Get CurrentBalance
		CurrentBalance = rCurrentBalance
	End Property	
	
	Public Property Get AvailableCredit
		AvailableCredit = rAvailableCredit
	End Property	

	Public Property Get CostBand
		CostBand = rCostBand
	End Property		

	Public Property Get SRCapActive
		SRCapActive = rSRCapActive
	End Property	

	Public Property Get VATCode
		VATCode = rVATCode
	End Property	
	
	Public Property Get SRCapPct
		SRCapPct = rSRCapPct
	End Property	

	Public Property Get BBLDealerStatus
		BBLDealerStatus = rBBLDealerStatus
	End Property	
	
	Public Property Get CurrencyID
		CurrencyID = rCurrencyID
	End Property			
	
	Public Property Get CurrencySymbol
		CurrencySymbol = rCurrencySymbol
	End Property			

	Public Property Get CurrencyDecimals
		CurrencyDecimals = rCurrencyDecimals
	End Property			

	Public Property Get DeliverySKU
		DeliverySKU = rDeliverySKU
	End Property				
	
	Public Property Get VatApplicable
		VatApplicable = rVatApplicable
	End Property		

	Public Property Get VatRate
		VatRate = rVatRate
	End Property			
	
	Public Property Get STDDeliveryCost
		STDDeliveryCost = rDeliveryCost
	End Property		

	Public Property Get DeliveryThreshold
		
		DeliveryThreshold = rFreeDeliveryThreshold
		
		
	End Property	

	Public Property Get DeliveryTBC
		
		DeliveryTBC = rDeliveryTBC
		
	End Property		
			
	
	Public Property Get DepartmentCode
		DepartmentCode = rDepartmentCode
	End Property		

	Public Property Get CostCentre
		CostCentre = rCostCentre
	End Property		

	Public Property Get DeliveryZoneID
		DeliveryZoneID = rDeliveryZoneID
	End Property		
	
	Public Property Get RiderCard
		RiderCard = rRiderCard
	End Property		
	
	Public Property Get SelectedLanguageID
		SelectedLanguageID = getSelectedLanguage()
	End Property	

	Public Property Get SelectedLanguageISO
		if SelectedLanguageISO = "" then
			sql = "SELECT TOP 1 LanguageISO FROM END_Language WHERE LanguageID = 0" & getSelectedLanguage() 
			set rs = db.execute(sql)
			if not rs.eof then
				SelectedLanguageISO = rs("LanguageISO")
			else
				SelectedLanguageISO = "UK"
			end if
			rsClose()
		end if
	End Property			
	
	Public Property Get ContactName
		ContactName = rContactName
		'jh "ContactName = " & ContactName
	End Property		
	
	Public Property Get ContactEmail
		if isarray(rContactEmailArr) then ContactEmail = rContactEmailArr(0) else ContactEmail = ""
	End Property			
	
	Public Property Get ContactEmails
		ContactEmails = rContactEmailArr
	End Property				

	Public Property Get GUID
		GUID = rGUID
	End Property				
	
	Public Property Get DownloadURL
		DownloadURL = "/downloads/accounts/" & AcCode & "_" & rGUID
	End Property	
	
	Public Property Get MandatoryPONumber
		if rMandatoryPONumber = 1 then MandatoryPONumber = true else MandatoryPONumber = false
	End Property		
	
	Public Property Get SagePayUser
		if rSagePayUser = 1 then SagePayUser = true else SagePayUser = false
	End Property	
	
	Public Property Get DailyRate
		DailyRate = rDailyRate
	End Property		

	Public Property Get CurrencyName
		CurrencyName = rCurrencyName
	End Property	
	
	Public Property Get CompanyName
		CompanyName = rCompanyName
	End Property
		
	Public Property Get SagePayCurrency
		select case rCostBand 			
			case "A"
				SagePayCurrency = "GBP" 
			case "C", "E"
				SagePayCurrency = "EUR" 
			case else			
				SagePayCurrency = ""	
		end select 
		
	End Property		
	
	Public Property Get ExchequerCurrencyCode
		ExchequerCurrencyCode = rExchequerCurrencyCode		
	End Property	

	Public Property Get ParentAcCode
		ParentAcCode = rParentAcCode
	End Property	

	Public Property Get DiscountPCT
		DiscountPCT = rDiscountPCT
	End Property	

	Public Property Get SeparateChildSR
		SeparateChildSR = rSeparateChildSR
	End Property	

	Public Property Get DealerDiscount
		DealerDiscount = rDealerDiscount
	End Property		
	
	'resets class
	'Public Function Clear()
	'	
	'end Function		
end class



Class ClassPriceMatrix

	private x, c
	private sql
	private PriceArr, PriceC, OrdPos, OrdPos_RRP
	

	
	'called at creation of instance
	Private Sub Class_Initialize()
	
		if 1 = 1 then
			dbConnect()
			jhAdmin "INITIALISE PRICES CLASS"
			
			sql = "exec spB2BPriceMatrix "
			
			x = getrs(sql, PriceArr, PriceC)
			
			jh "PriceC = " & PriceC
	
			OrdPos = 3 'COLUMN IN PRICE MATRIX FOR THE TRADE PRICE
			'jhInfo "INITIALISED - " & acCode
		end if
		
	End Sub
	
	private function LookupTradePrice(pSKU)
		for c = 0 to PriceC 
			if PriceArr(0,c) = pSKU then 
				LookupTradePrice = PriceArr(OrdPos,c)
				exit for
			end if
		next
	end function
	
	Private Sub Class_Terminate()
		'jhAdmin "DESTROY DEALER CLASS"
	
	End Sub
	
	'return the concatenated string
	Public Property let CostBand(pCostBand)
		MatrixCostBand = pCostBand
		jh "MatrixCostBand = " & MatrixCostBand
		select case pCostBand		
			case "C"
				OrdPos = 4
			case "D"
				OrdPos = 5
			case "E"
				OrdPos = 6
			case "F"
				OrdPos = 7
			case "G"
				OrdPos = 8
			case "H"
				OrdPos = 9							
			case else
				OrdPos = 3
		end select
		
		jh "OrdPos = " & OrdPos
		
	End Property
	
	Public Property Get getPrice(pSKU)
		'jh "GET PRICE = " & pSKU
		getPrice = LookupTradePrice(pSKU)
	End Property	
	
	'resets class
	'Public Function Clear()
	'	
	'end Function		
end class


class ClassBasketDetails


	private bc, c, x, sql, rBasketTotal, rVATTotal, rDeliveryTotal, rGrandTotal, rDeliverySKU, rBasketCount, rBasketArr, rFreeDelGap, rBODespBalance, rBONewSmallOrdersBalance, rUnitTotal
	private rVattableTotal, rExVatTotal, rDiscountTotal
	
	Private Sub Class_Initialize()
	
		loadDetails()
		
	End Sub

	private sub loadDetails()

	
		if 1 = 1 then
		
			dbConnect()

		blnIsDEConsumer =  false
		
		If AcDetails.VATCode = "7" Then
			blnIsDEConsumer =  true
		End If
			
			sql = "exec spB2BBasketContents '" & acDetails.AcCode & "', '" & acDetails.CostBand & "' ," & acDetails.SelectedLanguageID 

			
			x = getrs(sql, rBasketArr, bc)
			
			rBasketCount = bc + 1	
				
			rBasketTotal = 0
			rDeliveryTotal = 0
			rVATTotal = 0				
			rGrandTotal = 0
			rFreeDelGap = 0
			rDeliverySKU = "" 
			
			rBODespBalance = 0
			rBONewSmallOrdersBalance = 0
			
			DiscountPCT = convertNum(AcDetails.DiscountPCT)


			if 1 = 1 then
	
			            sql = "select ISNULL(SUM(UnitValue * PickedQuantity), 0) AS BODespBalance from fnBackForwardOrders('<Parameters><AcCode>" & ThisUserID & "</AcCode></Parameters>') where TagNo IN (15,16,17,18,20,21,22,19,34) AND OrderStatus <> 'SCANNER' "
				set rs = db.execute(sql)
				
				if not rs.eof then
					rBODespBalance = CDbl(rs("BODespBalance").Value)
				end if 
				rsClose()

		            sql = "select ISNULL(SUM(UnitValue * PickedQuantity), 0) AS BONewSmallOrdersBalance from fnBackForwardOrders('<Parameters><AcCode>" & ThisUserID & "</AcCode></Parameters>') where TagNo = 33 AND OrderStatus <> 'SCANNER' "			
				set rs = db.execute(sql)
				if not rs.eof then
					rBONewSmallOrdersBalance = cdbl(rs("BONewSmallOrdersBalance").Value)
				end if 
				rsClose()
				
				
			end if			
			
			rUnitTotal = 0
			rVattableTotal = 0
			
			for c = 0 to bc
			
			
				ThisQty = cdbl(rBasketArr(7,c))
				
				ThisVatCode = "" & rBasketArr(26,c)
								
				rUnitTotal = rUnitTotal + ThisQty
				
				if AcDetails.RiderCard then
					ThisValue = cdbl(rBasketArr(23,c)) 
				else
					ThisValue = cdbl(rBasketArr(10,c))
					
				end if

					if AcDetails.DealerDiscount > 0 then
			                       	rDiscountTotal = rDiscountTotal + ThisValue * AcDetails.DealerDiscount
						ThisValue = ThisValue * (1 - AcDetails.DealerDiscount)
					end if
				
				rBasketTotal = rBasketTotal + (ThisQty * ThisValue)

				if blnIsDEConsumer OR ThisVatCode = "S" OR ThisVatCode = "8" then
					rVattableTotal = rVattableTotal + (ThisQty * ThisValue)
				end if
				
			next	
			

			if rBODespBalance > 0 then
											
			elseif rBasketTotal > 0 and (rBasketTotal + rBONewSmallOrdersBalance) < acDetails.DeliveryThreshold then
				if AcDetails.RiderCard then
					if blnIsDEConsumer Then
                            rDeliveryTotal = acDetails.STDDeliveryCost * (1 + (glVatDERate / 100))
					else
                            rDeliveryTotal = acDetails.STDDeliveryCost * (1 + (glVatRate / 100))
					end if
				else
                                	rDeliveryTotal = acDetails.STDDeliveryCost
				end if

				rDeliverySKU = acDetails.DeliverySKU
				
			end if

			if AcDetails.RiderCard then
				if blnIsDEConsumer then
					rVATTotal =   rVattableTotal + rDeliveryTotal - ((rVattableTotal + rDeliveryTotal) / (1 + glVatDERate/100))
				else
					rVATTotal =   rVattableTotal + rDeliveryTotal - ((rVattableTotal + rDeliveryTotal) / (1 + glVatRate/100))
				End If
			elseif acDetails.VATApplicable then
				if (acDetails.VATCode = "S") Then 
					rVATTotal = (rVattableTotal + rDeliveryTotal) * (glVatRate/100)				
				elseif (acDetails.VATCode = "8") Then 
					rVATTotal = (rVattableTotal + rDeliveryTotal) * (glVat8Rate/100)				
				else
					rVATTotal = (rVattableTotal + rDeliveryTotal) * (glVatRate/100)				
				end if 
			end if								
			
			if rBasketTotal < acDetails.DeliveryThreshold then 
		                rFreeDelGap = acDetails.DeliveryThreshold - (rBasketTotal + rBONewSmallOrdersBalance)
			end if
			
			if AcDetails.RiderCard then
				rGrandTotal = (rBasketTotal + rDeliveryTotal)
			else
				rGrandTotal = (rBasketTotal + rDeliveryTotal + rVATTotal)
			end if
			
				
		end if	
	
	end sub

	
	Public Property Get BasketC
		BasketC = bc
	End Property		
	
	Public Property Get BasketCount
		BasketCount = rBasketCount
	End Property	

	Public Property Get DeliveryTotal
		DeliveryTotal = rDeliveryTotal
	End Property		
	
	Public Property Get DeliverySKU
		DeliverySKU = rDeliverySKU
	End Property		
	
	Public Property Get DeliveryItems
		DeliveryCost = rDeliveryTotal
	End Property			
	
	Public Property Get BasketTotal
		BasketTotal = rBasketTotal
	End Property				

	Public Property Get OrderTotal
		OrderTotal = rBasketTotal
	End Property		
	
	Public Property Get VATTotal
		VATTotal = rVATTotal
	End Property				
	
	Public Property Get GrandTotal
		GrandTotal = rGrandTotal
	End Property	

	Public Property Get BasketArr
		BasketArr = rBasketArr
	End Property		
	
	Public Property Get FreeDelGap
		FreeDelGap = rFreeDelGap
	End Property			
	
	Public Property Get BODespBalance
		BODespBalance = rBODespBalance
	End Property			

	Public Property Get BONewSmallOrdersBalance
		BONewSmallOrdersBalance = rBONewSmallOrdersBalance
	End Property				

	Public Property Get UnitTotal
		UnitTotal = rUnitTotal
	End Property	

	Public Property Get DiscountTotal
		DiscountTotal = rDiscountTotal
	End Property		
	
end class


class DealerAccountRestrictions

	private acCode
	
	Private rShowCodes, rHideCodes, rShowCategories, rHideCategories, rShowRanges, rHideRanges, rCustomRestriction

	private sub Class_Initialize()
	
		AccountRestrictionSQL = ""

		acCode = "JIMB01"
		sql = "SELECT TOP 1 SMLAccountRestrictionID, AccountCode, ShowCodes, HideCodes, ShowCategories, HideCategories, ShowRanges, HideRanges, CustomRestriction "
		sql = sql & "FROM SML_AccountRestrictions "
		sql = sql & "WHERE AccountCode = '" & acCode & "' "
		sql = sql & "ORDER BY SMLAccountRestrictionID DESC "
		'jhAdmin sql
		
		set rs = db.execute(sql)
		if not rs.eof then
			'jhAdmin "FOUND!"
			rShowCodes = "" & rs("ShowCodes")
			rHideCodes = "" & rs("HideCodes")		
			rShowCategories = "" & rs("ShowCategories")
			rHideCategories = "" & rs("HideCategories")
			rShowRanges = "" & rs("ShowRanges")
			rHideRanges = "" & rs("HideRanges")
			
			rCustomRestriction = rs("CustomRestriction")			
		end if	
		
		rsClose()
		
		'jhInfo "DealerAccountRestrictions INITIALISED - " & acCode
		
		
	End Sub
			
	Public Property Get ShowCodes
		ShowCodes = rShowCodes
	End Property	

	Public Property Get HideCodes
		HideCodes = rHideCodes
	End Property

	Public Property Get ShowCategories
		ShowCategories = rShowCategories
	End Property	
	
	Public Property Get HideCategories
		HideCategories = rHideCategories
	End Property	
	
	Public Property Get ShowRanges
		ShowRanges = rShowRanges
	End Property		
	
	Public Property Get HideRanges
		HideRanges = rHideRanges
	End Property	

	Public Property Get CustomRestriction
		CustomRestriction = rCustomRestriction
	End Property			
	
	public property get restrictionSQL

		if rShowCodes <> "" then
			AccountRestrictionSQL = AccountRestrictionSQL & " AND [SKUField] " & rShowCodes & ""
		end if
		if rHideCodes <> "" then
			AccountRestrictionSQL = AccountRestrictionSQL & " AND [SKUField] " & rHideCodes & ""
		end if
		
		'CatStr, RangeStr
		CatStr = ""
		
		if ShowCategories <> "" then
			catArr = split(rShowCategories, "|")
			for z = 0 to ubound(catArr)
				if catArr(z) <> "" then
					CatStr = CatStr & "[RangeField] LIKE '%|" & catArr(z) & "|%' OR "
				end if
			next
			if CatStr <> "" then
				if right(CatStr, 4) = " OR " then
					CatStr = left(CatStr, len(CatStr)-4)
				end if
				AccountRestrictionSQL = AccountRestrictionSQL & " AND (" & CatStr & ")"	
			end if 				
		end if	
		
		CatStr = ""
		
		if rHideCategories <> "" then
			catArr = split(rHideCategories, "|")
			for z = 0 to ubound(catArr)
				if catArr(z) <> "" then
					CatStr = CatStr & "[CategoryField]  NOT LIKE '%|" & catArr(z) & "|%' OR "
				end if		
			next
			if CatStr <> "" then
				if right(CatStr, 4) = " OR " then
					CatStr = left(CatStr, len(CatStr)-4)
				end if
				AccountRestrictionSQL = AccountRestrictionSQL & " AND (" & CatStr & ")"	
			end if 				
		end if		
		
		'CatStr, RangeStr
		RangeStr = ""
		
		if rShowRanges <> "" then
			rangeArr = split(rShowRanges, "|")
			for z = 0 to ubound(rangeArr)
				if rangeArr(z) <> "" then
					RangeStr = RangeStr & "[RangeField] LIKE '%|" & rangeArr(z) & "|%' OR "
				end if			
			next
			if RangeStr <> "" then
				if right(RangeStr, 4) = " OR " then
					RangeStr = left(RangeStr, len(RangeStr)-4)
				end if
				AccountRestrictionSQL = AccountRestrictionSQL & " AND (" & RangeStr & ")"	
			end if 				
		end if		
		
		RangeStr = ""
		if rHideRanges <> "" then
			rangeArr = split(rHideRanges, "|")
			for z = 0 to ubound(rangeArr)
				if rangeArr(z) <> "" then
					RangeStr = RangeStr & "[RangeField] NOT LIKE '%|" & rangeArr(z) & "|%' OR "
				end if			
			next
			if RangeStr <> "" then
				if right(RangeStr, 4) = " OR " then
					RangeStr = left(RangeStr, len(RangeStr)-4)
				end if
				AccountRestrictionSQL = AccountRestrictionSQL & " AND (" & RangeStr & ")"	
			end if 				
		end if			
		
		if CustomRestriction <> "" then		
			AccountRestrictionSQL = AccountRestrictionSQL & " AND (" & CustomRestriction & ")"			
		end if
		
		restrictionSQL = AccountRestrictionSQL

	end property 	
		
	
	Private Sub Class_Terminate()
		jhAdmin "DESTROY DEALER RESTRICTION CLASS"
	
	End Sub	

end class
%>