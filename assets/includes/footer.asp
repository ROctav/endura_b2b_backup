	<!-- Modal Stock Enquiry Form-->
	<div id="ModalContainer_STOCK" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-body">
			
			<div class=pull-right>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
			</div>
			
			<div id="ModalStockDiv">...</div>	
	
		</div>
		<div class="modal-footer">
			<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">OK</button>			
		</div>
	</div>


    <!-- starts footer -->
	<div class=spacer40>&nbsp;</div>	
    <footer id="footer" class=hidden-print style="margin-top:0;">
		<div class=spacer40>&nbsp;</div>		
        <div class="container">
<%
if AcDetails.AcCode <> "" then
%>		
		
			<div class="row spacer40" style="color:#777E83;" >
			
			
				<div class=span3>					
                    <%=glFooterContactAddress%>
				</div>
				<div class=span6>
					<a class=FooterLink href="/contact/"><%=lne("label_contact_us")%></a>
					<br>
					<a class=FooterLink href="<%=glConsumerWebsite%>"><%=lne("label_consumer_website")%></a>
				</div>	
														
<%	

					if session("B2B_SuppressPricing") = "1" then
						SuppressClass = "badge-important"
						SuppressStr = acDetails.CurrencySymbol & " <i class=""icon-remove icon-white""></i>"
						SuppressTitle = "Displayed pricing is currently switched OFF, click to switch on"
						ToogleOption = "ON"
					else
						SuppressClass = "badge-success"
						SuppressStr = acDetails.CurrencySymbol & " <i class=""icon-ok icon-white""></i>"
						SuppressTitle = "Displayed pricing is currently switched ON, click to switch off"
						ToogleOption = "OFF"
					end if

					
%>					
																
				<div style="float: right">
						<strong><%=lne_NOEDIT("footer_pricing_label")%></strong>
						<a id=ProdSearchDD title="" account" href="/?TogglePricing=<%=ToogleOption%>">	
							<span title="<%=SuppressTitle%>" class="badge <%=SuppressClass%>" style="color:white;"><%=SuppressStr%></span>
						</a>					

						<div class=spacer10></div>						
				</div>					
				
			</div>
<%
end if
%>
            <div class="row credits">
                <div class="span12">
                    <div class="row social">
                        <div class="span12">
                            <a href="https://www.facebook.com/EnduraOfficial?" target="_BLANK" class="facebook">
                                <span class="socialicons ico1"></span>
                                <span class="socialicons_h ico1h"></span>
                            </a>
                            <a href="https://twitter.com/endura" target="_BLANK" class="twitter">
                                <span class="socialicons ico2"></span>
                                <span class="socialicons_h ico2h"></span>
                            </a>                         
                        </div>
                    </div>
                    <div class="row copyright">
                        <div class="span12">
                            � <%=year(now)%> Endura Limited. <%=lne("footer_copyright_rights")%>
                        </div>
                    </div>
                </div>            
            </div>
			

			
        </div>
    </footer>



	
	
<%
if galleryPage then
%>	
	
	<link rel="stylesheet" href="/assets/colorbox/colorbox.css" />		
	<script src="/assets/colorbox/jquery.colorbox.js"></script>	
	
<%
end if
%>		
	
</body>
</html>


<%

	dbConnect()

	strProdLiveFilter = -1 
	strProdDiscFilter = -1
	strProdLimFilter = -1
	strProdClearFilter = -1 
	strProdForthFilter = -1 

	If Session("B2B_ProductSearchCurrent") = "1" Then 
		strProdLiveFilter = 1 
		strProdDiscFilter = 4
		strProdLimFilter = 90
	End If
	
	If Session("B2B_ProductSearchClearance") = "1" Then 
		strProdClearFilter = 5 
	End If
	
	If Session("B2B_ProductSearchFuture") = "1" Then 
		strProdLimFilter = 90
		strProdForthFilter = 3 
	End If
	
	
	sql = "exec [spB2BQuickSearch_AUTOCOMPLETE] 0" & AcDetails.SelectedLanguageID  & ", '" & AcDetails.AcCode & "'"
	sql = sql & ", " & strProdLiveFilter & ", " & strProdDiscFilter & ", " & strProdLimFilter & ", " & strProdClearFilter & ", " & strProdForthFilter 

	x = getrs(sql, ProdArr, ProdCount)

	set ls = new strconcatstream	
%>

	
<script>	

    var availableProds = [
	
<%
	for c = 0 to ProdCount
	
		ls.add "{"
		
		ls.add "label:"
		ls.add """" & ProdArr(1,c) & " - " & ProdArr(2,c) & """"				
		ls.add ", "
		ls.add "prodCode:"
		ls.add """" & ProdArr(1,c) & """"		
		ls.add ", "
		ls.add "prodID:"
		ls.add """" & ProdArr(0,c) & """"			
		ls.add "}"
		
		if c < ProdCount then
			ls.add "," 
			ls.add vbcrlf
		end if
	next
	
	response.write ls.value 
%>	

    ];		
	
<%


	sql = "exec [spB2BQuickSearch_AUTOCOMPLETE_STYLE] 0" & AcDetails.SelectedLanguageID & ", '" & AcDetails.AcCode & "'"
	'jh sql
	x = getrs(sql, StyleArr, StyleCount)
	
	ls.clear

	'This is used on Checkout page for Quick Add
	if AutoCompleteStylesRequired then	
	%>	
		var availableStyles = [	
	<%
		for c = 0 to StyleCount
		
			ThisProductID = StyleArr(0,c)
			ThisVariant = StyleArr(3,c)
			ThisName = StyleArr(5,c)
			ThisColorName = StyleArr(6,c)
			
			labelName  = ThisName & " - " & ThisColorName & " (" & ThisVariant & ")"
			labelName = replace(labelName, "'", "")
			
			ls.add "{"
			
			ls.add "label:"
			ls.add """" & labelName &  """"				
			ls.add ", "
			ls.add "variantCode:"
			ls.add """" & ThisVariant & """"		
			ls.add ", "
			ls.add "prodID:"
			ls.add """" & ThisProductID & """"			
			ls.add "}"
				
			if c < StyleCount then
				'response.write "," & vbcrlf
				ls.add "," 
				ls.add vbcrlf
			end if
			
		next
		
		response.write ls.value 
		
		ls.clear
		
	%>	

		];				
			
		
<%
	end if
%>		
		
		
	$( document ).ready(function() {

	
		$( "#searchVal_HEADER" ).autocomplete({		  
		  minLength: 3, 
		  source: availableProds	

		  ,		  

		  select: function (event, ui) {
					
					var selID = ui.item.prodID;					
					//alert(selID)
					window.location.assign('/products/?ProductID=' + selID)
					
					var pc = ui.item.prodCode;
					$("#searchVal_HEADER").val(pc)
					
					return false;
				}
		  
		});			
		
		$("#searchVal_HEADER").keyup(function (e) {
			//alert(e.keyCode);
			if (e.keyCode == 13) {
				window.location.assign('/?SearchVal=' + $("#searchVal_HEADER").val())
			}
		});					
		
		//alert('auto complete complete!')
	})
	
	<%
	
	%>
	
</script>  				

<script>

	function ba(obj, pid) {
			//	alert(pid);
			
			var thisForm = document.getElementById('productform_' + pid)
			var str = $(thisForm).serialize();
			//alert(str);
			
			$('.ProductAjax').hide();
			
			//alert('hidden!');
			
			//$("#BasketStatusDiv").html('!!!!');
		
			$("#ProductAjaxDiv_" + pid).html('<img src="/assets/images/icons/indicator.gif"> adding, please wait...');
			$('#ProductAjaxDiv_' + pid).show(100, function() {
				
			});
			$.ajax({
				scriptCharset: "ISO-8859-1" ,
				contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
				url: "/basket_server.asp?CallType=ADD",
				data: str,
				cache: false,			
				}).done(
					function( html ) {
						//alert('done...')
						//$("#BasketStatusDiv").html(html);
						
						$("#ProductAjaxDiv_" + pid).html(html);
						
						$('#ProductAjaxDiv_' + pid).show(100, function() {
							
						//alert('complete')	
						
						bCount();
						bSummary();
						
						return false;
						
					 });			

				  
			});	
			
			return false;
		
		
	}

	function bCount() {
			$("#basketCountHolder").html('loading...');
			
			$.ajax({
				scriptCharset: "ISO-8859-1" ,
				contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
				url: "/basket_server.asp?CallType=BASKET_COUNT",				
				cache: false,			
				}).done(
					function( html ) {						
						$(".basketCountHolder").html(html);						
			});	
			
	}
	

	function bSummary() {
			
			$("#BasketTop").html('loading...');
			
			$.ajax({
				scriptCharset: "ISO-8859-1" ,
				contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
				url: "/basket_server.asp?CallType=BASKET_SUMMARY",				
				cache: false,			
				}).done(
					function( html ) {						
						$("#BasketTop").html(html);											

					});	

	}
	
	$( document ).ready(function() {
		//alert('basket top')
		bCount();
		bSummary();
			
		$( ".DatePicker" ).datepicker(
			{ dateFormat: 'dd/mm/yy' }
		);
		
<%
if UseLazyLoad then
%>
		$("img.lazy").lazyload();
		//alert('ll');			
<%
end if
%>		
		
		
	
		$( ".announceClose" ).click( function () {
			
			var AnnouncementID = $(this).data("announce-id");
			var dontShowAgain = $("#DontShowAgain_" + AnnouncementID).prop('checked');
			//alert(dontShowAgain)
			
			//alert(AnnouncementID);			
			$.ajax({
				url: "/misc_server.asp?CallType=ANNOUNCEMENT_READ&AnnouncementID=" + AnnouncementID + "&dontShowAgain=" + dontShowAgain,				
				cache: false
				}).done(
					function( html ) {
					$("#AnnounceDiv").html(html);
					$("#AnnounceDiv").show(100, function() {
						// Animation complete.
					  });					
				});						
		});
		
		
		
	});

	

		//ADDED FOR IE PLACEHOLDER COMPATIBILITY	15 OCT 2013

		$('[placeholder]').focus(function() {
		  var input = $(this);
		  if (input.val() == input.attr('placeholder')) {
			input.val('');
			input.removeClass('placeholder');
		  }
		}).blur(function() {
		  var input = $(this);
		  if (input.val() == '' || input.val() == input.attr('placeholder')) {
			input.addClass('placeholder');
			input.val(input.attr('placeholder'));
		  }
		}).blur();


		$('[placeholder]').parents('form').submit(function() {
		  $(this).find('[placeholder]').each(function() {
			var input = $(this);
			if (input.val() == input.attr('placeholder')) {
			  input.val('');
			}
		  })
		});
		
				
		$('.dropdown-toggle').click(function(e) {
		  e.preventDefault();
		  setTimeout($.proxy(function() {
			if ('ontouchstart' in document.documentElement) {
			  $(this).siblings('.dropdown-backdrop').off().remove();
			}
		  }, this), 0);
		});		
		
		function hintClose(pType) {		  
		 
		  
			$.ajax({
				//contentType: "application/x-www-form-urlencoded;charset=ISO-8859-1",
				//dataType: 'json',
				scriptCharset: "ISO-8859-1" ,
				contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",	

				url: "/misc_server.asp?CallType=HINT_READ&HintType=" + pType,

				cache: false,			
				}).done();	
			 //alert(pType);		
		  
		};		
		
		//PLACEHOLDER END	ProdSearchDD searchVal_HEADER

		$(document).bind('keydown', 'ctrl+s', fn);
		
		function fn() {
			//alert('y');
			
			$("#ProdSearchDD").click();
			
			event.preventDefault();
		}
		
		$('#searchVal_HEADER').on('keydown', null, 'esc', function(){
			$("#ProdSearchDD").click();
		});     
		
		
		$("#OnHoldDismiss").click( function () {
			//alert('.')
			$.ajax({

				scriptCharset: "ISO-8859-1" ,
				contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
				url: "/misc_server.asp?CallType=ON_HOLD_DISMISS",
				cache: false,			
				}).done(
					function( html ) {					
					  $("#OnHoldResultsDiv").html( html );
				})
		});		
		
</script>

	<script>


		function stockModal(pid, pVariant, pSKU) {
					
				$("#ModalStockDiv").html('<img src="/assets/images/icons/indicator.gif"> loading...');
					
				$.ajax({
					scriptCharset: "ISO-8859-1" ,
					contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",	

					url: "/product_server.asp?CallType=STOCK_ENQUIRY&ProductID=" + pid + "&VariantCode=" + pVariant + "&SKU=" + pSKU + "",

					cache: false,			
					}).done(
						function( html ) {
						$("#ModalStockDiv").html(html);
						$("#ModalStockDiv").show(100, function() {
							// Animation complete.
						  });					
					});
				
			}				

			function stockLevelClick_VARIANT(pobj) {
				
				var productid = $(pobj).data('product-id');	
				var thisVariant = $(pobj).data('variant-code');
				var thisSKU = $(pobj).data('sku');
				stockModal(productid, thisVariant, thisSKU);
				$('#ModalContainer_STOCK').modal();				
			}


	</script>
	

<%
if isAdminUser and glCustom = "UK" then
	%>
	
	<script>
	$("#EditModeLink").click( function() {
		//alert('clicked!')
		
		var setMode = '';
		
		if ($(".CMSLink").hasClass('CMSHidden')) {
			$(".CMSLink").removeClass('CMSHidden');
			setMode = 'on';
		} else {
			$(".CMSLink").addClass('CMSHidden');
		}
		
		$.ajax({
			scriptCharset: "ISO-8859-1" ,
			contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
			url: "/misc_server.asp?CallType=SET_EDIT_MODE&SetEditMode=" + setMode + "",
			cache: false			
		});		
		
		
	});
	
	</script>
	
	<%

end if


dbClose()

response.cookies("ProductsLastVisited") = ""
response.cookies("ProductsLastVisited").expires = dateadd("d", -2, now)

set acDetails = nothing

killarray B2BTranslationArr
killarray ProdArr
killarray StyleArr
killarray BasketArr

%>



	

