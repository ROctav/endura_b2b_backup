<%

glSiteOffline = false
glSiteOfflineMessage = "We are performing essential maintenance work now. Our B2B website will be accessible again in 30 minutes. "



glCustom = "UK"

if glSiteOffline and not NoRedirect_HOLDING then
	dbClose()
	response.redirect "/holding/"
end if

glMultiLang = true
glWebsiteName = "Endura B2B"

if instr(1, glWebsiteURL, "b2b2013.") > 0 and not isjh() then
	dbClose()
	response.redirect "http://b2b.endurasport.com"
end if

glBackupRangingSummarys = true

'glHTTPS = true
glHTTPS = false

if glHTTPS then
	glWebsiteURL = "https://" & request.servervariables("SERVER_NAME")
else
	glWebsiteURL = "http://" & request.servervariables("SERVER_NAME")
end if

if isjh() or 1 = 1 then
	CurrentHTTPS = lcase(request.servervariables("HTTPS"))	
	if CurrentHTTPS = "off" and glHTTPS then
		'jhAdmin "REDIRECT TO HTTPS!"
		dbClose()
		response.redirect "https://b2b.endurasport.com"
	end if	
end if

glStockRotationEnabled = true
glCreateRotationJobName = "CreateSalesReturn"
glNewOrderJobName = "ProcessNewOrder"
glFAQEnabled = false

glClearanceListActive = true
glDespatchStats = true
glSystemAnnouncements = true
glFOBOCheckout = true
glPendingPreAllocs = true
glUpdateContactDetails = true

glConsumerWebsite = "http://www.endurasport.com"

glACMDefaultContactDetails = ""
'glPromoActive = true
'glPromoBG = "promo_bg.jpg"
glPromoCodes = ""
glPromoEndDate = "2015-12-31"
if glPromoActive then
	CheckDD = datediff("d", now, glPromoEndDate)
	'jhAdmin "CheckDD = " & CheckDD
	if CheckDD <= 0 then
		glPromoActive = false
	end if
end if


'glDBConnStr = "PROVIDER=SQLOLEDB;Data Source=ENDURA02;Initial Catalog=JH_EnduraWeb;uid=sa;pwd=P3tf0rce!$;"
glDBConnStr = "PROVIDER=SQLOLEDB;Data Source=endurasql;Initial Catalog=JH_EnduraWeb;uid=sa;pwd=$Cycl1ng!;" 

glRangingActive = true
glRangingUseFOPrices = true
glShowRotationFigures = true

glLoginPersistDays = 30

glReviewCutoffMonths = 24

glAdvancedSearch = true
glCustomProductsEnabled = false

glProductImageLoc = "e:\inetpub\extranet\website\assets\SML\media\ProductImages\"
glPriceListLoc = "e:\inetpub\extranet\DownloadArea\PriceList\"

glDropBoxImagesLink = "http://bit.ly/StandardProductImages"
glDropBoxImagesLink1 = "http://bit.ly/StandardProductImages"
glDropBoxImagesLink2 = "http://bit.ly/ActionLifestyle"
glDropBoxImagesLink3 = "https://goo.gl/jHeAaA"
glDropBoxImagesLink4 = "https://goo.gl/7sMN92"

glDropBoxCatalogueLink = "https://www.dropbox.com/sh/2vchbemeatb878n/AACjrKGPHMH_LG3lc9xefJhFa?dl=0"

glForgottenPasswordNoEmailTo = "enquiry@endura.co.uk"

glOrderThreshold = 200

glStockRed = 0
glStockAmber = 25
glStockGreen = 25
glStockUpperDisplayQty = 250

glNextArrivalUpperDisplayQty = 500

glLightColor = "#fff"
glDarkColor = "lightyellow"

glInvoicePDFLoc = "e:\Data\PDFInv\"
glStatementPDFLoc = "e:\Data\CustState\"

glJobProcessingCompanyRef = "LTD"

glXMLDropLocation  = "e:\inetpub\EXTRANET_B2B_2013\XML\orders\"	
glXMLTemplate_OrderHeader = "e:\inetpub\EXTRANET_B2B_2013\XML\template\B2B_OrderHead.txt"	
glXMLTemplate_OrderLine = "e:\inetpub\EXTRANET_B2B_2013\XML\template\B2B_OrderLine.txt"	
'glXMLFinalLocation = "e:\inetpub\EXTRANET_B2B_2013\XML\final\"	
glXMLFinalLocation = "e:\inetpub\extranet\XMLORders\"

glAccountDownloadsFolder = "e:\inetpub\EXTRANET_B2B_2013\website\downloads\accounts\"	

glOrigB2BContentLocation = "e:\inetpub\extranet\website\assets\text\"

glGoogleAnalyticsCode = "XXXX"

glShowOnlineSpecialists = false

glLegacyWebsiteURL = "http://endura2013.endura.co.uk/"

CheckUKServer = request.servervariables("SERVER_NAME")
if instr(1,CheckUKServer,"www.endura.co.uk") > 0 then

	dbClose()
	Response.Status="301 Moved Permanently" 
	Response.AddHeader "Location", "http://www.endurasport.com/"

end if


glB2Burl = "http://extranet.endura.co.uk"
glWebsiteName = "Endura"
glEnduraCompanyName = "Endura Ltd"

glContactEmail = "ukenquiry@endura.co.uk"
glContactFormTo = "ukenquiry@endura.co.uk"
glFooterContactAddress = "<strong>ENDURA LIMITED</strong><div>3 Starlaw Park, Livingston</div><div>West Lothian, EH54 8SF</div><div>United Kingdom</div>" 
glFooterContactOptions = "<div>Tel: +44 (0)1506 497749</div><div>Fax: +44 (0)1506 497750</div><div>Email: <a class=FooterLink href=""mailto:" & glContactEmail & """>" & glContactEmail & "</a></div>"

glFeedbackFormTo = "jhutton1@gmail.com"
glAllowFeedback = true
glCheckDeliveryOverrides = true

glSpamCheckEnabled = true

glVATEnabled = true
CurDate = now()

glVATRate = 20
glVATDERate = 19
glVAT7Rate = 19
glVAT8Rate = 21
glVAT6Rate = 20
glVATBRate = 25
glVATXRate = 24
glVATTRate = 23
glVAT9Rate = 22

'EMAIL SETTINGS

glContactFormTo = "ukenquiry@endura.co.uk"
glEmailAddress = "ukenquiry@endura.co.uk"
glContactTel = "+44 (0)1506 497749"

glEmailDisabled = false
glRecordEmails = true
glMailServer = "195.147.9.50"
glMailPrefix = "NS "

glOrdersTo = "amanda@endura.co.uk|jim@endura.co.uk"
glSagePayNotificationsTo = "jhutton1@gmail.com|"

'SAGEPAY SETTINGS
glSagePayEnabled = true
glSagePayURL = "http://www.sagepay.co.uk/"
glSenderEmail_SAGEPAY = "info@endura.co.uk"
glContactEmail_SAGEPAY = "lorraine@endura.co.uk" '"john@kmvcreative.com" ' 
glContactEmail_SAGEPAY_INTERNATIONAL = "internationalpayments@endura.co.uk" ' "john@kmvcreative.com" ' 

'ICONS

glExcelIcon = "/assets/images/icons/page_white_excel.png"

'Data Lists

glKSITList = "E0006/3,E0006/5,E0042/2,E0042/3,E0042/4,E0042/5,E0042/6,E0042/7,E0052,E0068Y/2,E0068Y/3,E0068Y/4,E0068Y/5,E0068Y/6,E0068Y/7,E0076BK/2,E0076BK/3,E0076BK/4,E0076BK/5,E0076BK/6,E0076BK/7,E0095/2,E0095/3,E0095/4,E0095/5,E0095/6,E0095/7,E1004BK/L-XL,E1004BK/S-M,E1005BK/L-XL,E1005BK/S-M,E1023BK/3,E1023BK/4,E1023BK/5,E1023BK/6,E1023BK/7,E1035BK/L-XL,E1035BK/M-L,E1035BK/S-M,E1037BK/L-XL,E1037BK/M-L,E1037BK/S-M,E3025/3,E3025/4,E3025/5,E3025/6,E3025/7,E3078BK/3,E3078BK/4,E3078BK/5,E3078BK/6,E3078BK/7,E3115BK/2,E3115BK/3,E3115BK/4,E3115BK/5,E3115BK/6,E3115BK/7,E4013BK/3,E4013BK/4,E4013BK/5,E4013BK/6,E4013BK/7,E4014BK/3,E4014BK/4,E4014BK/5,E4014BK/6,E4014BK/7,E4014BK/8,E5022BK/3,E5022BK/4,E5022BK/5,E5022BK/6,E5022BK/7,E6091BK/1,E6091BK/2,E6091BK/3,E6091BK/4,E6091BK/5,E6091BK/6,E6091BK/7,E8064CM/2,E8064CM/3,E8064CM/4,E8064CM/5,E8064CM/6,E8064CM/7,E8064CM/8,E8064CM/9,E8074OM/3,E8074OM/4,E8074OM/5,E8074OM/6,E8074OM/7,E9076GV/3,E9076GV/4,E9076GV/5,E9076GV/6,E9076GV/7,E9076YV/3,E9076YV/4,E9076YV/5,E9076YV/6,E9076YV/7,E9081GV/3,E9081GV/4,E9081GV/5,E9081GV/6,E9081GV/7"
glDirectSRList = "BA,RS,BG,LV,EE,IE,CY,MT,ND"

if NOT Session("B2B_ProductSearchCurrent") = "0" Then Session("B2B_ProductSearchCurrent") = "1"

%>
