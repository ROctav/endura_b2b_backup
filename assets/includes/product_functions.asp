<%

function quickAddTypes()
	
	'exit function
	
	dim c, sql, TypeC, TypeArr, x 
	
	sql = "exec spB2BQuickSearch_TYPES 0" & AcDetails.SelectedLanguageID
	
	
	x = getrs(sql, TypeArr, TypeC)
	
	set ls = new strconcatstream
	
	'jh "TypeC = " & TypeC
	
	for c = 0 to TypeC
		
		if TypeArr(1,c) = 0 then
			ThisID = TypeArr(0,c)
			DefCode = "" & TypeArr(4,c)
			imgURL = ""
			
			ShowThis = true
			
			'jh "DefCode = " & DefCode
			
			if DefCode <> "" then
				imgURL = getImageDefault(DefCode, "")
			else
				ShowThis = false
			end if
			
			imgURL = ""
			
			select case thisid
				case 1
					imgURL = "/assets/images/prodbrowser/nav_symb_jackets.png"
				case 2
					imgURL = "/assets/images/prodbrowser/nav_symb_gilets.png"	
				case 3
					imgURL = "/assets/images/prodbrowser/nav_symb_jerseys.png"	
				case 6
					imgURL = "/assets/images/prodbrowser/nav_symb_baselayers.png"	
				case 4
					imgURL = "/assets/images/prodbrowser/nav_symb_baggy.png"	
				case 5
					imgURL = "/assets/images/prodbrowser/nav_symb_skintight.png"	
				case 51
					imgURL = "/assets/images/prodbrowser/nav_symb_accessories.png"	
				case 52
					imgURL = "/assets/images/prodbrowser/nav_symb_hardware.png"		
			end select			
			
			if DefCode <> "" then
				if imgURL = "" then
					imgURL = getImageDefault(DefCode, "")
				end if
			else
				ShowThis = false
			end if			
			
			'jh imgURL
			
			if ShowThis then
			
				'jh TypeArr(2,c)
				'ls.add "<hr>"
				ls.add "<div class=row>"
				
				if imgURL <> "" then
					ls.add "<div class=span1>"
					ls.add "<img style=""height:60px;xmargin-right:10px;"" class=pull-left src=""" & imgURL & """>"
					ls.add "</div>"
				end if

				ls.add "<div class=span4>"
				
				ls.add "<h4>"
				'ls.add "<i class=icon-chevron-right></i>"
				ls.add "<a class=ProductTypeSearchLink data-product-type=""" & ThisID & """ onclick="" $('#ProductTypeDiv').hide();productTypeSearch(" & ThisID & ")"" href=""#"">"
				ls.add TypeArr(2,c)
				ls.add "</a>"
				ls.add "</h4>"
				ls.add "<ul class=""nav nav-pills small"" style=""padding:2px;"">"
				for x = 0 to TypeC
					if ThisID = TypeArr(1,x) then
					
						SubID = TypeArr(0,x)
						
						if "" & TypeArr(4,x) <> "" then
							ls.add "<li>"
							ls.add "<a class=ProductTypeSearchLink data-product-type=""" & SubID & """ onclick="" $('#ProductTypeDiv').hide();productTypeSearch(" & SubID & ")"" href=""#"">"
							ls.add TypeArr(2,x)							
							ls.add "</a> "
							ls.add "</li>"
						end if
					end if
				next
				ls.add "</ul>"
				
				ls.add "</div>"
				
				ls.add "</div>"
			end if
		end if
	next
	
	quickAddTypes = ls.value
	
	'jh quickAddTypes
	
	set ls = nothing
	
end function

public colorBarArr, colorBarC

function altColorBar(byval pColorCode, byval pVariant, byref rOverRideColorName )
	dim x
	if not isarray(ColorBarArr) and colorBarC <> -1 then
		sql = "SELECT ColorOverrideID, Variantcode, ColorBarCode, ColorOverrideName FROM B2B_ColorOverrides "
		'jh sql
		x = getrs(sql, colorBarArr, colorBarC)
	end if
		
	altColorBar = pColorCode
	OverrideColorName = ""
	
	for x = 0 to colorBarC
		if "" & pVariant = "" & colorBarArr(1,x) then
			altColorBar = colorBarArr(2,x)
			rOverRideColorName = colorBarArr(3,x)
			exit for
		end if
	next
	
	if altColorBar = "" then altColorBar = "BK"

end function

function getAvailableProds(pType, pArr)
	
	dim c, ShowWhenNoProds, ShowProdCounts, ProdTypeName, SomeFound, ThisProdTypeID
	ShowWhenNoProds = false
	ShowProdCounts = false
	
	getAvailableProds = ""
	prc = ubound(pArr,2)
	
	for c = 0 to pRC
		if "" & pType = "" & pArr(0,c) then
			ThisProdTypeID = pArr(0,c)
			ProdTypeName = pArr(1,c)
			ProdTypeName_TRANSLATED = "" & pArr(6,c)
			if SelectedLanguageID <> "0" and ProdTypeName_TRANSLATED <> "" then
				ProdTypeName = ProdTypeName_TRANSLATED
			end if
			
			ThisProdCount = pArr(3,c)
			'jhInfo "FOUND - " & pArr(1,c) & "(" & ThisProdCount & ")"
			if ThisProdCount > 0 or ShowWhenNoProds then
				'jh "OK, SOME PRODS (" & ThisProdCount & ")"
				getAvailableProds = getAvailableProds & "<div style=""margin-bottom:5px;line-height:12pt;""><xnobr><strong>"
				
				if ThisProdCount > 0 then
					getAvailableProds = getAvailableProds & "<a id=""ProdCatHeadLink_" & ThisProdTypeID & """ class=ProdCatLink title=""" & ThisProdCount & " product(s)"" onclick=""setActiveCatLink(this);getResults('PRODTYPE', '" & EndUseID &"', '" & ThisProdTypeID & "', 0);"" style=""color:#666;cursor:pointer"">" & ProdTypeName & "</a>"
				else
					getAvailableProds = getAvailableProds & "<span title=""" & ThisProdCount & " product(s)"" style=""color:#f0f0f0;"">" & ProdTypeName & "</span>"
				end if				
				
				if ShowProdCounts then
					getAvailableProds = getAvailableProds & " (" & ThisProdCount & ")" 	
				end if	
				getAvailableProds = getAvailableProds & "</strong></xnobr></div>"				
				SomeFound = true
			else
				'jhError "NO PRODS"
				'exit function
				
			end if
		end if
	next

	if SomeFound then ' child elements exist
		for c = 0 to pRC
			if "" & pType = "" & pArr(2,c) then
				ThisProdTypeID = pArr(0,c)
				ThisProdCount = pArr(3,c)
				if ThisProdCount > 0 or ShowWhenNoProds then
					ProdTypeName = pArr(1,c)
					ProdTypeName_TRANSLATED = "" & pArr(6,c)
					if SelectedLanguageID <> "0" and ProdTypeName_TRANSLATED <> "" then
						ProdTypeName = ProdTypeName_TRANSLATED
					end if					
					'jh "" & pArr(1,c) & "(" & ThisProdCount & ")"
					getAvailableProds = getAvailableProds & "<div style=""line-height:14pt;""><xnobr>"
					if ThisProdCount > 0 then
						getAvailableProds = getAvailableProds & "<a class=ProdCatLink title=""" & ThisProdCount & " product(s)"" onclick=""setActiveCatLink(this);getResults('PRODTYPE', '" & EndUseID &"', '" & ThisProdTypeID & "', 0);"" style=""color:#666;cursor:pointer"">" & ProdTypeName & "</a>"
					else
						getAvailableProds = getAvailableProds & "<span title=""" & ThisProdCount & " product(s)"" style=""color:#f0f0f0;"">" & ProdTypeName & "</span>"
					end if
					if ShowProdCounts then
						getAvailableProds = getAvailableProds & " (" & ThisProdCount & ")" 	
					end if
					getAvailableProds = getAvailableProds & "<xnobr></div>"
				end if
			end if
		next
	end if
	
	if SomeFound then
		getAvailableProds = "<div style=""border:0px solid green;xwidth:90px;font-size:9pt;"">" & getAvailableProds & "</div>"
	end if
	
end function



function productGrid (pProductID, pVariant)
	
	dim c, ls, sql, SKUC,x

	intRotateSizeThreshold = 9
	
	sql = "SELECT DISTINCT ss.SizeID, sz.SizeName, sz.SizeType, sz.SizeZorder FROM SML_SKUS ss INNER JOIN SML_SizeList sz ON sz.SizeID = ss.SizeID WHERE ss.ProductID = 0" & pProductID & " ORDER BY SizeType, SizeZorder "
	x = getrs(sql, SizeArr, SizeC)	

	blnRotateSizeList = False
	If (SizeC + 1 >= intRotateSizeThreshold) Then 
		blnRotateSizeList = True 
	End If
	
	sql = "exec spB2BQuickSearch_DETAIL 0" & pProductID & ", '" & acDetails.AcCode & "', '" & acDetails.CostBand & "', " & acDetails.SelectedLanguageID & ", '" & pVariant & "'"
	x = getrs(sql, SKUArr, SKUC)

	CurrentStockQtyLabel = lne("label_current_stock_qty")
	ForthcomingItemLabel = lne("label_forthcoming_SKU") 
	
	ColorSizeNotAvailableStr =  lne("label_product_grid_unavailable") 
	ClearanceSKUStr = lne("label_product_grid_sku_on_clearance") 
	
		if SKUC >= 0 then
			NoteCount = "" & SKUArr(35,0) 

			if NoteCount > 0 then 
				HasNotes = true
				sql = "exec [spB2BGetProductNotes] 0" & pProductID & ", 0" & AcDetails.SelectedLanguageID
				set rs = db.execute(sql)
				if not rs.eof then					
					ThisNoteText = "" & rs("NoteDetail")	
					NoteDetail_TRANSLATED = "" & rs("NoteDetail_TRANSLATED")	
					if NoteDetail_TRANSLATED <> "" then
						ThisNoteText = NoteDetail_TRANSLATED
					end if
				end if
				rsClose()
			end if
								
		end if
				
	
		set ls = new strconcatstream
		
		ls.add "<div class=spacer10></div>"
		ls.add "<form id=""productform_" & pProductID & """>"
		
		if ThisNoteText <> "" then
			ls.add "<div class=""alert spacer10 legible""><button type=""button"" class=""close"" data-dismiss=""alert"">&times;</button>" & addbr(ThisNoteText) & "</div>"			
		end if
		
		if ProductPageZoom then
			ZoomGalleryID = "ProductZoomGallery"										
		end if
		
		ls.add "<div id=""" & ZoomGalleryID & """ style="""">"

		LevelsHintDismissed = request.cookies("hints")("stock_levels")
			
			if LevelsHintDismissed <> "1" then
				ls.add "<div class=""alert alert-info"">"
				ls.add "<button type=""button"" class=""close"" onclick=""hintClose('stock_levels');"" data-dismiss=""alert"">&times;</button>"
				ls.add "<i class=""icon icon-info-sign""></i> <small>"
				ls.add lne("stock_level_click_explained")
				ls.add "</small>"
				ls.add "</div>"
			end if
		
		ls.add "<table class=ProductDetailSKUGrid border=1>"
	
		if (NOT blnRotateSizeList) Then
			
			ls.add "<tr>"
			ls.add "<th></th>"
			ls.add "<th></th>"
			
			for x = 0 to SizeC
				SizeName = SizeArr(1,x)
				ls.add "<th style=""width:60px;"">"
				ls.add SizeName	
				ls.add "</th>"
			next	
			
			ls.add "</tr>"
		End If

		LastVariant = ""
		
		MatchedSKUS = -1
		Dim TradePrice, RRPPrice
		for c = 0 to SKUC
			
			ThisProductID = SKUArr(0,c)	
			ThisProductCode = SKUArr(1,c)	
			ThisColor = SKUArr(7,c)	
			ThisVariant = SKUArr(5,c)
			if (IsNull(SKUArr(38,c))) Then 
				RRPPrice = 0
			Else
				RRPPrice = CDbl(SKUArr(38,c))
			End If

			if (IsNull(SKUArr(39,c))) Then 
				TradePrice = 0
			Else
				TradePrice = CDbl(SKUArr(39,c))
			End If
			
			if LastVariant <> ThisVariant then
			
				imgURL = getImageDefaultCDN(ThisProductCode, ThisVariant)				
				
				ZoomData = ""
				LinkURL = ""
				ColorBack = ""
				ColorBackZoom = ""
				
				if ProductPageZoom then 
					
					imgURL_ZOOM = getImageColorZoom(ThisProductCode, ThisVariant, rURL)					
					if imgURL_ZOOM = "" then 
						imgURL_ZOOM = imgURL
					end if
					ZoomData = "data-image=""" & imgURL & """ data-zoom-image=""" & imgURL_ZOOM & """ data-thumb-caption=""" & ThisColor & """"
					
					
					ColorBack = getImageColorBack(ProductCode, ThisVariant, rURL)
					if ColorBack <> "" then
						ColorBackZoom = getImageColorBackZoom(ProductCode, ThisVariant, rURL)					
						ZoomData_BACK = "data-image=""" & ColorBack & """ data-zoom-image=""" & ColorBackZoom & """ data-thumb-caption=""" & ThisColor & " - back view """
					end if
					
				else
					LinkURL = "/products/?ProductID=" & pProductID & "&InitCode=" & ThisVariant
				end if				
				
				ls.add "<tr valign=""top"">"
				
				ls.add "<td class=text-center >"

				ls.add "<a href=""" & LinkURL & """ " & ZoomData & " class=ZoomThumbLink>"
				ls.add "<img class=""img-responsive ProdThumb40"" src=""" & imgURL & """>"
				ls.add "</a>"
				
				if ColorBackZoom <> "" then
					ls.add "<a " & ZoomData_BACK & " class=ZoomThumbLink>"
					ls.add "<img class=""img-responsive ProdThumb40"" src=""" & ColorBack & """>"
					ls.add "</a>"
				end if
				
				ls.add "<div><strong>" & ThisColor & "</strong></div>"									
				ls.add "<div class=ProductDetailVariant>" & ThisVariant & "</div>"
				ls.add "</td>"									

				ls.add "<td class=text-center >" 

				if NOT AcDetails.RiderCard then
					if pricingActive Then

						ls.add "<div class=spacer10><span class=""label label-success"">" & lne("label_each") & " " 
						if (TradePrice = 0)  Then
							ls.add "CALL"
						Else
							ls.add dispcurrency_ALWAYS(TradePrice) 
						End If
						ls.add "</span></div>"
					End If
				End If

				ls.add "<div class=spacer10><span class=""label"">" & lne("label_rrp") & " " 
				if (RRPPrice = 0)  Then
					ls.add "CALL"
				Else
					ls.add dispcurrency_ALWAYS(RRPPrice) 
				End If
				ls.add "</span></div>"
				ls.add "</td>"									
				

				strCWSizeHTML = ""

				for x = 0 to SizeC
					strSizeHTML = ""
					
					blnSizeExists = false
					
					CheckSize = SizeArr(1,x)

					if (blnRotateSizeList) Then
						strCWSizeHTML = strCWSizeHTML & "<tr>"									
						strCWSizeHTML = strCWSizeHTML & "<td valign=top class=""text-center"" style=""border: 0"">"
					Else
						strCWSizeHTML = strCWSizeHTML & "<td class=""text-center"">"
					End If
					
					for y = c to SKUC 
						if SKUArr(5,y) = ThisVariant and SKUArr(11,y) = CheckSize then
				
							blnSizeExists= true
							
							ThisSKU = SKUArr(10,y) 
				
							MAIQty = convertnum(SKUArr(29,y))
							if MAIQty < 0 then MAIQty = 0
							
							CanPurchase = SKUArr(30,y) 
							
							SKUStatusID = "" & SKUArr(33,y) 

							if SKUStatusID = "3" then ForthcomingItem = true else ForthcomingItem = false
							
							if SKUStatusID = "5" then ClearanceItem = true else ClearanceItem = false
														
							if acDetails.RiderCard and MAIQty = 0 then CanPurchase = "0"
							
							StatusExpStr = ""
							if CanPurchase = "0" then 
								AllowPurchase = false
								DisStr = "DISABLED" 
								if acDetails.RiderCard then
									StatusExpStr = "Sorry, you can only purchase items currently in-stock"
								else
									StatusExpStr = ColorSizeNotAvailableStr
								end if
							elseif ClearanceItem and MAIQty = 0 then
								AllowPurchase = false
								DisStr = "DISABLED" 
							else
								AllowPurchase = true
								DisStr = ""
							end if
							
							QtyInBasket = "" & SKUArr(31,y)
							DispQtyInBasket = QtyInBasket 
													
							MAIQtySimple = MAIQty						
							if MAIQty <= glStockRed then 
								MAIClass = "label-important"
							elseif MAIQty <= glStockAmber then 
								MAIClass = "label-warning"
							else
								MAIClass = "label-success"
								if MAIQty > glStockUpperDisplayQty then
									MAIQtySimple = glStockUpperDisplayQty & "+"
								end if
							end if
							
							DeadStr = ""
							
							ThisOutstanding = cdbl(SKUArr(32,y))
							
							BOStr = dispBO(ThisOutstanding)
							
							ThisFO = cdbl(SKUArr(36,y))
							
							FOStr = dispFO(ThisSKU, ThisFO)
																
							MAIStr = dispStock_ENQUIRY(MAIQty, pProductID, ThisVariant, ThisSKU)
						
							MatchedSKUS = MatchedSKUS + 1
							
							
							if not AllowPurchase then
								strSizeHTML = strSizeHTML &  "<div class=spacer20 title=""" & StatusExpStr & """ style=""padding-top:4px;""><i class=icon-exclamation-sign></i></div>"						
							else
								strSizeHTML = strSizeHTML &  "<div><input type=""number"" autocomplete=off name=SKUQty id=SKUQty title=""" & ThisSKU & StatusExpStr & """ value=""" & DispQtyInBasket & """ class=QuickSearchSKUQty>"
								if glClearanceListActive and ClearanceItem and not AcDetails.RiderCard then
									strSizeHTML = strSizeHTML &  "<div class=spacer20 title=""" & ClearanceSKUStr & """ style=""padding-top:4px;""><a class=OrangeLink href=""/products/clearance/#VARIANT_" & ThisVariant & """><i class=icon-fire></i></a></div>"	
								end if
								strSizeHTML = strSizeHTML &  "</div>"	
								strSizeHTML = strSizeHTML &  "<input style=""width:20px;"" type=hidden name=SKU value=""" & ThisSKU & """ class=FormHidden>"
								strSizeHTML = strSizeHTML &  "<input style=""width:20px;"" type=hidden name=WASQty value=""" & DispQtyInBasket & """ class=FormHidden>"
							
							end if
							strSizeHTML = strSizeHTML &  "<div title=""" & CurrentStockQtyLabel & """ class=spacer10>"
							strSizeHTML = strSizeHTML &  MAIStr
							if ForthcomingItem then
								strSizeHTML = strSizeHTML &  "&nbsp;<span title=""" & ForthcomingItemLabel & """ class=""label label-forthcoming"">F</span>"
							end if							
							strSizeHTML = strSizeHTML &  "</div>"	

							strSizeHTML = strSizeHTML &  "<div>"

							
							strSizeHTML = strSizeHTML &  BOStr
							if FOStr <> "" then
								strSizeHTML = strSizeHTML &  " " & FOStr
							end if
							strSizeHTML = strSizeHTML & "</div>"
							

							
							
							exit for
						end if	
					next
					
					if (blnRotateSizeList) Then
						if (blnSizeExists) Then
						
							'Hardcoded deliberately
							strOverwrittenSize = ShowOverwrittenSizeName(ThisSKU, CheckSize)
		
							strSizeHTML = "<div style=""width:100%; text-align: center; font-weight: bold;"">" & strOverwrittenSize & "</div>" & strSizeHTML
							'
						End If
					End If

					strCWSizeHTML = strCWSizeHTML & strSizeHTML  

					if (blnRotateSizeList) Then
						strCWSizeHTML = strCWSizeHTML & "</td></tr>"								
					Else
						strCWSizeHTML = strCWSizeHTML & "</td>"
					End If


				next							
				
				if (blnRotateSizeList) Then 
					ls.add "<td>"									
					ls.add "<table cellpadding=""0"" cellspacing=""0"" border=""0"">"									
					ls.add strCWSizeHTML
					ls.add "</table>"									
					ls.add "</td>"									
				Else
					ls.add strCWSizeHTML
				End If
				
				ls.add "</tr>"
				
				end if
				
				LastVariant = ThisVariant
			
		next
		
		ls.add "</table>"
		ls.add "</div>"
		
		ls.add "<div class=spacer10></div>"
		ls.add "<div class=ProductAjax style=""text-align:left;"" id=""ProductAjaxDiv_" & pProductID & """>"
		ls.add "</div>"		
		
		ls.add "<div id=QuickAddProgressNotification class=spacer10></div>"
		
		btnLabel = lne("btn_add_to_basket")
		
		ls.add "<button onclick=""ba(this, " & pProductID & ");return false;"" class=""btn btn-primary"" data-product-id=""" & pProductID & """>"
		ls.add btnLabel
		ls.add "</button>"			
		
		ls.add "<input style=""width:20px;"" type=hidden name=ProductID value=""" & pProductID & """ class=FormHidden>"
		
		ls.add "</form>"
		
		

						
	productGrid = ls.value
	
	set ls = nothing
	
	killarray SKUArr
	killarray SizeArr
	
end function

	
%>
