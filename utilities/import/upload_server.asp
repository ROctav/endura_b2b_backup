<%
Response.ContentType = "text/html"
Response.AddHeader "Content-Type", "text/html;charset=ISO-8859-1"
'Response.CodePage = 65001
Response.CharSet = "ISO-8859-1"

Response.Expires = 0
Response.Expiresabsolute = Now() - 1442
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"

SelectedLanguageID = getSelectedLanguage()



%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	</head>
	<body>
	
	<!--#include virtual="/assets/includes/global_functions.asp" -->
<%

if not isLoggedIn() then jhStop

CallType = request.querystring("CallType")

jh "CallType = " & CallType

function ImportHoldingSKU(byval pAcCode, byval pSKU, byval pQty)
	sql = "INSERT INTO B2B_ImportHolding (AcCode, SKU, Qty, ImportStatus) VALUES ("
	sql = sql & "'" & pAcCode & "', "
	sql = sql & "'" & validstr(pSKU) & "', "
	sql = sql & "0" & pQty & ", "
	sql = sql & "0)"
	
	jhAdmin sql
	db.execute(sql)
	
end function

function deleteImportHolding()
	sql = "DELETE FROM B2B_ImportHolding WHERE AcCode = '" & AcDetails.AcCode & "' AND ImportStatus = 0 "
	jhAdmin sql
	db.execute(sql)
end function

if CallType = "IMPORT_CSV" then

	ImportMax = 500
	SomeImported = false
	ImportCount = 0

	set acDetails = new DealerAccountDetails

	Set upl = Server.CreateObject("SoftArtisans.FileUp")
	
	jhInfo "IMPORT! " & AcDetails.AcCode
	
	ReturnCode = "xxx"
	
	call deleteImportHolding()
	
	if upl.Form("ImportFile").IsEmpty then
		ReturnCode = "NO_FILE"
	else
		jhInfo "GOT A FILE!"
		
		SaveLoc = getAccountDownloadFolder()
		jh "SaveLoc = " & SaveLoc
		
		if 1 = 1 then
			DocumentFileName = Mid(upl.Form("ImportFile").UserFileName, InstrRev(upl.Form("ImportFile").UserFileName, "\") + 1)
			
			jh DocumentFileName
			
			ThisDocType = lcase(right(DocumentFileName,4))
			if ThisDocType <> ".csv" then
				ReturnCode = "FILE_NOT_VALID"
				'jh ErrorMessage 
			else
				set fs = server.CreateObject("Scripting.FileSystemObject")		
				
				upl.Form("ImportFile").SaveAs(SaveLoc & "\" & DocumentFileName)

				jhInfo "CSV FILE IMPORTED"
				
				FileUploaded = true
				
				set f = fs.opentextfile(SaveLoc & "\" & DocumentFileName)
				
				while not f.atendofstream and not FatalError
				
					'jh "GET LINE"
					ThisLine = f.ReadLine					
					'jh ThisLine
				
					if ThisLine <> "" then
				
						LineArr = split(ThisLine, ",")
						
						if ubound(LineArr) >= 1 then
							ProcessSKU = ucase(trim("" & LineArr(0)))
							ProcessQty = convertNum(trim("" & LineArr(1)))
							'jh ProcessSKU & "_" & ProcessQty							
							if ProcessSKU <> "" and ProcessQty > 0 then
								jhInfo "IMPORT - " & ProcessSKU & " (" & ProcessQty & ")"
								call ImportHoldingSKU(AcDetails.AcCode, ProcessSKU, ProcessQty)
								SomeImported = true
								ImportCount = ImportCount + 1
								
								if ImportCount >= ImportMax then FatalError = true
							end if
						end if
						
					end if
				wend 
				
				f.close
				
				set f = nothing
				
				
				set fs = nothing
			end if
			
		end if
	end if		
		
	
	jh "ENDS!"
	
	if SomeImported then
		ReturnCode = "FILE_OK"
		FormAction = "CHECK_IMPORT"
	else
		ReturnCode = "FILE_ERROR"
		ReturnParams = "&ErrFile=" & DocumentFileName
	end if	
	
	Set upl = nothing
	
elseif CallType = "IMPORT_TEXT" then

	ImportMax = 500
	SomeImported = false
	ImportCount = 0

	set acDetails = new DealerAccountDetails
	DelimChar = "~"
	
	ImportArea = request.form("ImportArea")
	
	ProcessList = replace(ImportArea, chr(10), "|")	
	ProcessList = replace(ProcessList, chr(9), DelimChar)
	ProcessList = replace(ProcessList, ",", DelimChar)
	ProcessList = replace(ProcessList, ";", DelimChar)
	
	jh "ProcessList = " & ProcessList
	
	ProcessArr = split(ProcessList, "|")
	
	ProcessC = ubound(ProcessArr)

	call deleteImportHolding()
	
	for c = 0 to ProcessC
		ProcessLine = ProcessArr(c)
		LineCount = LineCount + 1
		
		if ProcessLine <> "" then
			'jh "ProcessLine = " & ProcessLine
			LineArr = split(ProcessLine, DelimChar)
						
			if ubound(LineArr) >= 1 then
				'jh "VALID LINE"
				ProcessSKU = ucase(trim(LineArr(0)))
				ProcessQty = convertnum(LineArr(1))				
				if ProcessSKU <> "" and ProcessQty > 0 then	
					jhInfo "IMPORT - " & ProcessSKU & " (" & ProcessQty & ")"
					call ImportHoldingSKU(AcDetails.AcCode, ProcessSKU, ProcessQty)
					SomeImported = true
					ImportCount= ImportCount + 1
					if ImportCount >= ImportMax then exit for
				end if
			end if
			
		end if
	next
	
	if SomeImported then
		ReturnCode = "TEXT_OK"
		FormAction = "CHECK_IMPORT"
	else
		ReturnCode = "TEXT_ERROR"
	end if
	
	
elseif CallType = "IMPORT_CONFIRM" then	
		
	set acDetails = new DealerAccountDetails	
		
	ImpList = request("ImpList")	
	ImpList = cleancheck(ImpList)
	ImportOptions = request("ImportOptions")
	
	if ImportOptions = "CLEAR" then
		sql = "DELETE FROM B2B_Basket WHERE B2BOrderID = 0 AND AcCode = '" & AcDetails.AcCode & "'"
		jhAdmin sql
		db.execute(sql)	
	end if	
	
	sql = "exec spB2BImport_CHECK '" & AcDetails.AcCode & "', " & AcDetails.SelectedLanguageID & " "
	jhAdmin sql

	'jhStop
	
	x = getrs(sql, ImpArr, ImpC)
	
	jhAdmin "ImpC = " & ImpC
	
	UnitsImported = 0
	
	for c = 0 to ImpC
		ThisImportID = ImpArr(0,c)
		ThisSKU = ImpArr(2,c)
		ThisQty = ImpArr(3,c)
		
		if instr(1, ImpList, "|" & ThisSKU & "|") > 0 then
			jhAdmin "YES - " & ThisSKU & " (" & ThisQty & ")"
			call basketAdd(ThisSKU, ThisQty, AcDetails.AcCode)
			SomeAdded = true
			UnitsImported = UnitsImported + ThisQty
		end if
	
	next
		
	if SomeAdded then
		ReturnCode = "IMPORT_COMPLETE"
		FormAction = "IMPORT_COMPLETE"
		ReturnParams = "&UnitTotal=" & UnitsImported
	else	
		ReturnCode = "NONE_IMPORTED"
	end if
	'jhStop	

	'call deleteImportHolding()	
		
	
end if

set acDetails = nothing
dbClose()

if ReturnCode <> "" then
	

	redirto = "/utilities/import/default.asp?FormAction=" & FormAction & "&ReturnCode=" & ReturnCode & ReturnParams
	jh redirto
	response.redirect redirto	
	
end if


%>				

</body>
</html>
