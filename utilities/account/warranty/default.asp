<%
SelectedLanguageID = getSelectedLanguage()

if not isLoggedIn() then 
	response.end
end if

%>

<!--#include virtual="/assets/includes/header.asp" -->

<link rel="stylesheet" href="css/index.css" type="text/css" media="screen" />
<style type="text/css">
#info_section div 
{
margin:15px 0;
}
</style>
	<div class="container">      

		<div class=row>

			<div class=span6  style="width: 100%;">
			
				
				<h3><%=lne("report_title_warranty_returns")%></h3>
            
				
				<div style="margin: 20px 0;"><span class="label label-success"><%=lne("warranty_help")%></span></div>


<table cellpadding="4" class="ReportTable legible" border="1" style="width: 100%;">
<tr><th>#</th><th><%=lne("label_product")%></th><th><%=lne("label_product_name")%></th><th><%=lne("label_qty")%></th><th><%=lne("warranty_label_fault")%></th><th><%=lne("warranty_label_POR")%></th>
<th><i class="icon icon-remove"></th></tr>

<tr class=legible valign=middle>
<td class=RepSmall>1</td>
<td class=RepSmall><input id="SKU_Warr1" type="text" autocomplete=off class=BigInput placeholder="<%=lne_NOEDIT("header_search_for")%>" ></td>
<td class=RepSmall ><span id="Descr_Warr1" style="font-weight: bold; white-space: nowrap;"></span>
<span id="Error_Warr1" style="color: red; font-weight: bold; display: none;"></span></td>
<td class=RepSmall align="center"><span id="Qty_Warr1" class="badge badge-inverse" isreplace=""></span></td>
<td class=RepSmall id="ReasonCont_Warr1"></td>
<td class=RepSmall  id="PORCont_Warr1"></td>
<td class=RepSmall><i id="Clear_Warr1" class="icon icon-remove" style="cursor: pointer; cursor: hand; display: none;"></i></td></tr>

<tr class=legible valign=middle>
<td class=RepSmall>2</td>
<td class=RepSmall><input id="SKU_Warr2" type="text" autocomplete=off class=BigInput placeholder="<%=lne_NOEDIT("header_search_for")%>" ></td>
<td class=RepSmall ><span id="Descr_Warr2" style="font-weight: bold; white-space: nowrap;"></span>
<span id="Error_Warr2" style="color: red; font-weight: bold; display: none;"></span></td>
<td class=RepSmall align="center"><span id="Qty_Warr2" class="badge badge-inverse" isreplace=""></span></td>
<td class=RepSmall id="ReasonCont_Warr2"></td>
<td class=RepSmall  id="PORCont_Warr2"></td>
<td class=RepSmall><i id="Clear_Warr2" class="icon icon-remove" style="cursor: pointer; cursor: hand; display: none;"></i></td></tr>

<tr class=legible valign=middle>
<td class=RepSmall>3</td>
<td class=RepSmall><input id="SKU_Warr3" type="text" autocomplete=off class=BigInput placeholder="<%=lne_NOEDIT("header_search_for")%>" ></td>
<td class=RepSmall ><span id="Descr_Warr3" style="font-weight: bold; white-space: nowrap;"></span>
<span id="Error_Warr3" style="color: red; font-weight: bold; display: none;"></span></td>
<td class=RepSmall align="center"><span id="Qty_Warr3" class="badge badge-inverse" isreplace=""></span></td>
<td class=RepSmall id="ReasonCont_Warr3"></td>
<td class=RepSmall  id="PORCont_Warr3"></td>
<td class=RepSmall><i id="Clear_Warr3" class="icon icon-remove" style="cursor: pointer; cursor: hand; display: none;"></i></td></tr>

<tr class=legible valign=middle>
<td class=RepSmall>4</td>
<td class=RepSmall><input id="SKU_Warr4" type="text" autocomplete=off class=BigInput placeholder="<%=lne_NOEDIT("header_search_for")%>" ></td>
<td class=RepSmall ><span id="Descr_Warr4" style="font-weight: bold; white-space: nowrap;"></span>
<span id="Error_Warr4" style="color: red; font-weight: bold; display: none;"></span></td>
<td class=RepSmall align="center"><span id="Qty_Warr4" class="badge badge-inverse" isreplace=""></span></td>
<td class=RepSmall id="ReasonCont_Warr4"></td>
<td class=RepSmall  id="PORCont_Warr4"></td>
<td class=RepSmall><i id="Clear_Warr4" class="icon icon-remove" style="cursor: pointer; cursor: hand; display: none;"></i></td></tr>

<tr class=legible valign=middle>
<td class=RepSmall>5</td>
<td class=RepSmall><input id="SKU_Warr5" type="text" autocomplete=off class=BigInput placeholder="<%=lne_NOEDIT("header_search_for")%>" ></td>
<td class=RepSmall ><span id="Descr_Warr5" style="font-weight: bold; white-space: nowrap;"></span>
<span id="Error_Warr5" style="color: red; font-weight: bold; display: none;"></span></td>
<td class=RepSmall align="center"><span id="Qty_Warr5" class="badge badge-inverse" isreplace=""></span></td>
<td class=RepSmall id="ReasonCont_Warr5"></td>
<td class=RepSmall  id="PORCont_Warr5"></td>
<td class=RepSmall><i id="Clear_Warr5" class="icon icon-remove" style="cursor: pointer; cursor: hand; display: none;"></i></td></tr>
</table>

<div id="info_section">
<div class="alert alert-warning"  id="PReplaceNA" style="display: none;"><%=lne("warranty_replace_na")%></div>

<div><span style="font-weight: bold;"><%=lne("warranty_requested_action")%></span><br><span id="PCreditOpt"><input type="radio" id="BCreditOpt" name="ReturnOpt" value="CREDIT"/>&nbsp;&nbsp;<%=lne("warranty_credit")%></span><br><span id="PReplaceOpt"><input type="radio" id="BReplaceOpt" name="ReturnOpt"  value="REPLACE" checked/>&nbsp;&nbsp;<%=lne("warranty_replacement")%></span></div>

<div class="alert alert-warning" id="PValidError" style="display: none;"><%=lne("warranty_validation")%></div>

<div class="alert alert-success" id="PSubmitConfirm" style="display: none;"><%=lne("warranty_thank_you")%></div>

<div><input id="BSubmit" type=submit class="btn btn-primary" value="<%=lne("warranty_submit_btn")%>"></div>
</div>				
				
			</div>		
		</div>		
	</div>		
			
			

<script type="text/javascript">
var 		intSectionNo = 5;



	function ProcessWarranty(ProcessType, SectionID) {

	//$("#QuickAddDiv").html('<img src="/assets/images/icons/indicator.gif">');
		
	$("#SKU_Warr" + SectionID).attr("disabled", true) ;
	$("#Error_Warr" + SectionID).css("display","none");
	$("#PValidError").css("display","none");
	$("#BReplaceOpt").attr("disabled", false);
	$("#PReplaceNA").css("display","none");




        url = "";         
	strProcessURL = "warranty_server.asp";

	if (ProcessType == "CLEAR")
	{
			$("#PValidError").css("display","none");
			$("#Error_Warr" + SectionID).css("display","none");
			$("#SKU_Warr" + SectionID).attr("disabled", false) ;
			$("#SKU_Warr" + SectionID).val("");
			$("#Descr_Warr" + SectionID).text("");
			$("#Qty_Warr" + SectionID).text("");
			$("#Qty_Warr" + SectionID).prop("isreplace", "undefined");
			$("#ReasonCont_Warr" + SectionID).html("");
			$("#PORCont_Warr" + SectionID).html("");
			$("#Clear_Warr" + SectionID).css("display","none");

	}
 
	if (ProcessType == "PRODUCTCHECK")
	{
		url = strProcessURL + "?ProcessType=" + ProcessType + "&SKU=" + $('#SKU_Warr' + SectionID).val();
	}

	if (ProcessType == "SUBMIT")
	{

		
		blnIsValidIssue = false; 

		//
		try
		{
	
		strSubmitData = "";


		for (i = 1; i <= intSectionNo; i++)
		
		{
			if ($("#Qty_Warr" + i).text() == "1")
			{
				if ($("#Reason_Warr" + i).val() == "") 
				{
					throw "";
				}
				if ($("#POR_Warr" + i).val() == "") 
				{
					throw "";
				}

				strSubmitData += $("#SKU_Warr" + i).val().toUpperCase() + "," + $("#Reason_Warr" + i).val()  + "," + $("#POR_Warr" + i).val() +  "|";

			}
		}		

		if (strSubmitData == "")
		{
			throw "";
		}

		url = strProcessURL + "?ProcessType=" + ProcessType + "&ActionType=" + $('input[name=ReturnOpt]:checked').val() + "&Data=" + strSubmitData

		}
		catch (err)
		{
			$("#PValidError").css("display","inline");
		}

	}


	if (url != "") 
	{
$.ajax({
    type: "GET",	
    url: url,
    dataType: "xml",
 contentType: "text/xml; charset=\"ISO-8859-1\"",
async: false,
	cache: false, 
    success: function(xmldata) {
		//alert((new XMLSerializer()).serializeToString(xmldata));


	if (ProcessType == "SUBMIT") 
	{



		if ($(xmldata).find("Response").attr("Status") == "OK")
		{
			$("#PSubmitConfirm").css("display","inline");
			$("#BSubmit").css("display","none");
		}
	}

	if (ProcessType == "PRODUCTCHECK") 
	{



		if ($(xmldata).find("Response").attr("Status") == "Error")
		{
			strStatusMessage = $(xmldata).find("Response").attr("StatusMessage");
			strErrorMessage = $(xmldata).find("Response").attr("ShowMessage");
			
			$("#Error_Warr" + SectionID).text(strErrorMessage);
			$("#Error_Warr" + SectionID).css("display","inline");
			$("#SKU_Warr" + SectionID).attr("disabled", false) ;
			$("#SKU_Warr" + SectionID).focus() ;
			
		}

		else
		{
			$("#Clear_Warr" + SectionID).css("display","block");

				$("#Descr_Warr" + SectionID).html($(xmldata).find("Response").find("Product").find("Descr").text());
				$("#Qty_Warr" + SectionID).text("1");

			//RLRs
			strDropDownList = "";

		$(xmldata).find("Response").find("Product").find("RLRs").find("RLR").each(function(){

			strDropDownList += "<option value=\"" + $(this).attr("ExchID")  +  "\">" + $(this).attr("Descr") + "</option>";
	   });

		strDropDownList = "<select class=FormSelect id=\"" + "Reason_Warr" + SectionID  + "\">" + strDropDownList + "</select>";

				$("#ReasonCont_Warr" + SectionID).html(strDropDownList);
			//


			//PORs

			strDropDownList = "";

		$(xmldata).find("Response").find("Product").find("PORs").find("POR").each(function(){

			strDropDownList +=  "<option value=\"" + $(this).attr("OurRef")  +  "\">" + $(this).attr("Text") + "</option>";
	   });


		strDropDownList = "<select class=FormSelect id=\"" + "POR_Warr" + SectionID  + "\">" + strDropDownList + "</select>";

				$("#PORCont_Warr" + SectionID).html(strDropDownList);
			//

		
				

		$("#Qty_Warr" + SectionID).prop("isreplace", $(xmldata).find("Response").find("Product").attr("IsReplace"));
		}

	}

    }


        
});

}
		for (i = 1; i <= intSectionNo; i++)
		
		{
			if ($("#Qty_Warr" + i).prop("isreplace") == "No")
			{
	       			$("#PReplaceNA").css("display","block");
				$("#BCreditOpt").prop("checked",true);		
				$("#BReplaceOpt").attr("disabled", true);
				break;					
			}
			
		}

	}	



</script>


<script>

	$('#BSubmit').click(function (e) {

	ProcessWarranty("SUBMIT", null);


	}); 	

		for (i = 1; i <= intSectionNo; i++)
		
		{

$('#SKU_Warr' + i).keyup(function (e) {
    if (e.keyCode === 13) {
	ProcessWarranty("PRODUCTCHECK", this.id.substring(this.id.indexOf("Warr") + 4, this.id.length));
    }
  });

	$('#Clear_Warr' + i).click(function (e) {

	ProcessWarranty("CLEAR", this.id.substring(this.id.indexOf("Warr") + 4, this.id.length));


	}); 	

}
	
</script>


<!--#include virtual="/assets/includes/footer.asp" -->

<%

dbClose()

%>

