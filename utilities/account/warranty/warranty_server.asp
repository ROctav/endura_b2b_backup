<%
Response.ContentType = "text/xml"
Response.AddHeader "Content-Type", "text/xml;charset=ISO-8859-1"

Response.Expires = 0
Response.Expiresabsolute = Now() - 1442
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"

SelectedLanguageID = getSelectedLanguage()

if not isLoggedIn() then 
	response.end
end if

%>


<!--#include virtual="/assets/includes/global_functions.asp" -->
<!--#include virtual="/assets/includes/product_functions.asp" -->
<%

set acDetails = new DealerAccountDetails
AcCode = acDetails.AcCode
ThisCostCentre = acDetails.CostCentre
      
IsException = False
strExceptionCode = ""

ActionType = request("ActionType")
CallType = request("ProcessType")

XMLResponse = "<?xml version=""1.0"" encoding=""ISO-8859-1""?>"

if CallType = "SUBMIT" then

	strSubmitData = request("Data")
	
	XMLMessageData = ""
	
	 ProductsData = Split(strSubmitData,"|")

	XMLMessageData = XMLMessageData & "<ProcessWarranty AcCode=""" & AcCode &  """ ActionType=""" & ActionType &  """>"
 
	for each s in ProductsData
		If (Len(s) > 0) Then

			XMLMessageData = XMLMessageData & "<Product"

			 ProductData = Split(s,",")
			l = ubound(ProductData)

			XMLMessageData = XMLMessageData & " SKU=""" & ProductData(0) & """ ExchReasonID=""" & ProductData(1) & """ PORRef=""" & ProductData(2) & """"  &_
			" ExchReasonDescr = """ & lne("warranty_fault_" & ProductData(1)) & """"  &_
			"></Product>"
			
		End If	
 	next

	XMLMessageData = XMLMessageData & "</ProcessWarranty>"	 

	call InsertJob("CreateWarrantyReturn", XMLMessageData, 0)		
	
end if

if CallType = "PRODUCTCHECK" then



	ThisSKU = request("SKU")

	Set objcmd = Server.CreateObject("ADODB.Command")
	Set objcmd.ActiveConnection = db
	objcmd.CommandText = "ExchequerProcessWarranty"
	objcmd.CommandType = adcmdStoredProc

	objcmd.Parameters.append objcmd.createParameter("@ProcessType", adVarChar, adParamInput, 20)
	objcmd.Parameters("@ProcessType") = CallType 
	objcmd.Parameters.append objcmd.createParameter("@AcCode", adVarChar, adParamInput, 10)
	objcmd.Parameters("@AcCode") = AcCode 

	objcmd.Parameters.append objcmd.createParameter("@LanguageID", adInteger, adParamInput, 3)
	objcmd.Parameters("@LanguageID") = SelectedLanguageID 

	objcmd.Parameters.append objcmd.createParameter("@SKU", adVarChar, adParamInput, 20)
	objcmd.Parameters("@SKU") = ThisSKU 


	objcmd.Parameters.append objcmd.createParameter("@ErrorCode", adInteger, adParamOutput, 3)

	Set objrs = Server.CreateObject("ADODB.Recordset")

	Set objrs = objcmd.execute()

	ErrorCode =  objcmd.Parameters("@ErrorCode")
	
	if ErrorCode = 0 then

			If (NOT objrs.eof) Then 
				SKUGLCode = objrs.Fields("GLCode").Value 
				If Len(Trim(objrs.Fields("TransDescr").Value)) > 0  Then 
					Descr = Trim(objrs.Fields("TransDescr").Value)  
				Else
					Descr = Trim(objrs.Fields("Descr").Value)        

				End If   

			
				if (IsNull(objrs.Fields("FreeQty").Value)  OR ThisCostCentre = "CH")  Then
		
					strIsReplace = "No"
		
				else
					strIsReplace = "Yes"
				End If

				ProductXML = ProductXML & "<Product SKU=""" & Trim(objrs.Fields("SKU").Value) & """ IsReplace=""" & strIsReplace & """ CostCentre=""" & ThisCostCentre  &  """>" 
				ProductXML = ProductXML & "<Descr><![CDATA[" & Descr  & "]]></Descr>"
				objrs.Close
				set objrs = Nothing

				ProductXML = ProductXML  & "<PORs>"

				objcmd.Parameters("@ProcessType") = "GETPORLIST" 
				set objrs = objcmd.execute()

				ProductXML = ProductXML  & "<POR OurRef="""" Text=""" & lne("warranty_select_mandatory") & """/>"
	
				While not objrs.eof
					ProductXML = ProductXML  & "<POR OurRef=""" & objrs.Fields("OurRef").Value &  """ Text=""" & objrs.Fields("OurRef").Value &  """/>"
					objrs.MoveNext
				Wend
				objrs.Close
				set objrs = Nothing

				ProductXML = ProductXML  & "<POR OurRef=""N/A"" Text=""" & lne("warranty_por_na") & """/>"


				ProductXML = ProductXML  & "</PORs>"


				ProductXML = ProductXML  & "<RLRs>"

				objcmd.Parameters("@ProcessType") = "GETRLRLIST" 
				set objrs = objcmd.execute()

				ProductXML = ProductXML  & "<RLR ExchID="""" Descr=""" & lne("warranty_select_mandatory") & """/>"

				While not objrs.eof
                                        
					RLRID = objrs.Fields("ReasonCount").Value

					If _
					(RLRID = 1 AND _
					(SKUGLCode = "51013" OR SKUGLCode = "51015" OR SKUGLCode = "51020" OR SKUGLCode = "51025" _
					OR SKUGLCode = "52013" OR SKUGLCode = "52015" _
					OR SKUGLCode = "53020" OR SKUGLCode = "53025" _
					OR SKUGLCode = "54013" OR SKUGLCode = "54017" OR SKUGLCode = "54021" OR SKUGLCode = "54025" OR SKUGLCode = "54029")) _

					OR _

					(RLRID = 2 AND _
					(SKUGLCode = "51013" OR SKUGLCode = "51015" OR SKUGLCode = "51020" OR SKUGLCode = "51025" _
					OR SKUGLCode = "52013" OR SKUGLCode = "52015" _
					OR SKUGLCode = "53025" _
					OR SKUGLCode = "54029")) _

					OR _

					(RLRID = 3 AND _
					(SKUGLCode = "51013" OR SKUGLCode = "51015" OR SKUGLCode = "51025" _
					OR SKUGLCode = "52013" _
					OR SKUGLCode = "54021" )) _


					OR _

					(RLRID = 4 AND _
					(SKUGLCode = "51013" OR SKUGLCode = "51015" OR SKUGLCode = "51020" OR SKUGLCode = "51025" _
					OR SKUGLCode = "52013" OR SKUGLCode = "52015" _
					OR SKUGLCode = "54013" OR SKUGLCode = "54017" OR SKUGLCode = "54021" OR SKUGLCode = "54029")) _

					OR _

					(RLRID = 5 AND _
					(SKUGLCode = "52013" OR SKUGLCode = "52015" _
					OR SKUGLCode = "53010" OR SKUGLCode = "53020" OR SKUGLCode = "53025" _
					OR SKUGLCode = "54029")) _

					OR _

					(RLRID = 6 AND _
					(SKUGLCode = "51013" _
					OR SKUGLCode = "53010")) _

					OR _

					(RLRID = 7 AND _
					(SKUGLCode = "51013" OR SKUGLCode = "51015" OR SKUGLCode = "51020"  _
					OR SKUGLCode = "52013" OR SKUGLCode = "52015" _
					OR SKUGLCode = "54029")) _

					OR _

					(RLRID = 8 AND _
					(SKUGLCode = "51020" _
					OR SKUGLCode = "52015" _
					OR SKUGLCode = "53020" OR SKUGLCode = "53025" _
					OR SKUGLCode = "54021")) _

					OR _

					(RLRID = 9 AND _
					(SKUGLCode = "53010" OR SKUGLCode = "53015")) _

					OR _

					(RLRID = 10 AND _
					(SKUGLCode = "52013")) _

					OR _

					(RLRID = 11 AND _
					(SKUGLCode = "51013" OR SKUGLCode = "51015" OR SKUGLCode = "51020" OR SKUGLCode = "51025" _
					OR SKUGLCode = "52013" OR SKUGLCode = "52015" _
					OR SKUGLCode = "54013" OR SKUGLCode = "54017" OR SKUGLCode = "54021" OR SKUGLCode = "54025" OR SKUGLCode = "54029")) _

					OR _

					(RLRID = 12 AND _
					(SKUGLCode = "52015")) _

					OR _

					(RLRID = 13 AND _
					(SKUGLCode = "51013" OR SKUGLCode = "51015" OR SKUGLCode = "51020"  _
					OR SKUGLCode = "52013" _
					OR SKUGLCode = "53010"OR SKUGLCode = "53015" OR SKUGLCode = "53020" OR SKUGLCode = "53025" _
					OR SKUGLCode = "54013" OR SKUGLCode = "54017" OR SKUGLCode = "54021" OR SKUGLCode = "54025" OR SKUGLCode = "54029")) _

					OR _

					(RLRID = 14 AND _
					(SKUGLCode = "51013" OR SKUGLCode = "51015" OR SKUGLCode = "51020"  _
					OR SKUGLCode = "52013" _
					OR SKUGLCode = "53010"OR SKUGLCode = "53015" OR SKUGLCode = "53020" OR SKUGLCode = "53025" _
					OR SKUGLCode = "54013" OR SKUGLCode = "54017" OR SKUGLCode = "54021" OR SKUGLCode = "54025" OR SKUGLCode = "54029")) _

					OR _

					(RLRID = 15 AND _
					(SKUGLCode = "51013" OR SKUGLCode = "51015" OR SKUGLCode = "51020" OR SKUGLCode = "51025" _
					OR SKUGLCode = "52013" OR SKUGLCode = "52015" _
					OR SKUGLCode = "53015" _
					OR SKUGLCode = "54013" OR SKUGLCode = "54017" OR SKUGLCode = "54021")) _




					Then
						ProductXML = ProductXML & "<RLR ExchID=""" & RLRID &  """ Descr=""" & lne("warranty_fault_" & RLRID)  &  """/>"
					End if

					'Disabled as not reuqired
					'OR _ (RLRID = 16 AND _ (SKUGLCode = "53010")) _
					'					OR _					(RLRID = 17 AND _ (SKUGLCode = "52015" _					OR SKUGLCode = "54013" OR SKUGLCode = "54017" OR SKUGLCode = "54021")) _

					objrs.MoveNext
				Wend
				objrs.Close
				set objrs = Nothing

				ProductXML = ProductXML  & "</RLRs>"
				
				ProductXML = ProductXML & "</Product>"		

			Else
				objrs.Close
				IsException = true
			End If
	Else

		IsException = true
	end if

	set objcmd = Nothing

end if

if (IsException) Then

	If (ErrorCode = 1) Then
			strExceptionCode = "SKUNOTVALID"
			strShowMessage = lne("warranty_not_valid")
	ElseIf (ErrorCode = 2) Then
			strExceptionCode = "SKUNOTFOUND"
			strShowMessage = lne("warranty_not_found")

	Else
			strExceptionCode = "GENERAL"
			strShowMessage = lne("warranty_system_error")

	End If

	XMLResponse = XMLResponse & "<Response Status=""Error"" StatusMessage=""" & strExceptionCode &  """ ShowMessage=""" & strShowMessage & """></Response>"
Else
	XMLResponse = XMLResponse & "<Response Status=""OK"" StatusMessage="""" ShowMessage="""">" & ProductXML & "</Response>"

End If 


dbClose()


Response.Write(XMLResponse)
Response.Flush
Response.End

		
%>