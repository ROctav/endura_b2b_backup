
<%
PageTitle = "Your account"

Response.ContentType = "text/html"
Response.AddHeader "Content-Type", "text/html;charset=ISO-8859-1"
'Response.CodePage = 65001
Response.CharSet = "ISO-8859-1"

Response.Expires = 0
Response.Expiresabsolute = Now() - 1442
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"

%>

<!--#include virtual="/assets/includes/header.asp" -->

<%

%>

<style>
	.ReportTable TD {
		padding:5px;
	}
</style>

<link rel="stylesheet" href="css/index.css" type="text/css" media="screen" />

	<div class="container">      

		<div class="row">      
			<div class="span3 spacer10">
		

<%
'if isjh() then

	ShowHints = cleannum(request("ShowHints"))
	if ShowHints = "1" then		
		response.cookies("hints").expires = dateadd("d", -100, now)		
		jhAdmin "HINTS RESET!"
	end if
	
	
	call getFileDownloads()

'end if


%>		

					<hr>
					
					<h3 class=spacer20><%=lne("label_downloads")%></h3>
					
                    <% if isFOException() then 
                        %>
						<!--
                        <p>
							<a class=OrangeLink href="/utilities/epos/default.asp?feedtype=simple&band=FON"><i class=icon-file></i><%=lne("EPOSLinkNextFOSimple")%>  - AW20</a>
						</p>	
                        <p>
							<a class=OrangeLink href="/utilities/epos/default.asp?feedtype=extended&band=FON"><i class=icon-file></i><%=lne("EPOSLinkNextFOExtended")%> - AW20</a>
						</p>
						-->
                        <p>
							<a class=OrangeLink href="/utilities/epos/default.asp?feedtype=simple&band=FO"><i class=icon-file></i><%=lne("EPOSLinkFOSimple")%>  - AW20</a>
						</p>	
                        <p>
							<a class=OrangeLink href="/utilities/epos/default.asp?feedtype=extended&band=FO"><i class=icon-file></i><%=lne("EPOSLinkFOExtended")%> - AW20</a>
						</p>
                   <% end if %>
						<p>
							<a class=OrangeLink href="/utilities/epos/default.asp?feedtype=simple"><i class=icon-file></i><%=lne("EPOSLinkSimple")%>  - SS20</a>
						</p>
                        <p>
							<a class=OrangeLink href="/utilities/epos/default.asp?feedtype=extended"><i class=icon-file></i><%=lne("EPOSLinkExtended")%>  - SS20</a>
						</p>	

							

					<%
						DLBand = AcDetails.CostBand
						if request("dlBand") <> "" and isAdminUser() then
							dlBand = request("dlBand") 
						end if	
				
						sql = "SELECT DownloadName, DownLoadURL "
						sql = sql & "FROM B2B_Downloads "
						sql = sql & "WHERE DownloadStatusID = 1 "
						sql = sql & "AND (ISNULL(CostBands, '') = '' OR CostBands LIKE '%|" & dlBand &  "|%') "
						sql = sql & "AND (ISNULL(CostBands_HIDE, '') = '' OR CostBands_HIDE NOT LIKE '%|" & dlBand &  "|%') "
						sql = sql & " ORDER BY DownloadName"
						
						x = getrs(sql, DownloadsArr, DownloadsC)
						
						for c = 0 to DownloadsC
							
							ThisName = DownloadsArr(0,c)
							ThisURL = DownloadsArr(1,c)
							

                                %>
							    <p>
								    <a target="_BLANK" class=OrangeLink href="<%=ThisURL%>"><i class=icon-file></i> <%=ThisName%></a>
							    </p>							
							
							    <% next			%>						
					
                        <p>
							<a class=OrangeLink href="/utilities/epos/default.asp?feedtype=allseasons"><i class=icon-file></i><%=lne("EPOSLinkAllSeasons")%></a>
						</p>	
		
			</div>	
			
<%
if isjh() or 1 = 1 then
	
		sql = "exec spGetBackOrderCount '" & AcDetails.AcCode & "', " & exDate(now)
		'jhAdmin sql
		BOCount = 0
		set rs = db.execute(sql)
		if not rs.eof then
			BOCount = rs("BOCount")
		end if
		rsClose()
		'jhAdmin "BOCount = " & BOCount
		if BOCount > 0 then
			BOStr = "<span title=""You currently have " & BOCount & " item(s) on backorder, click the link to view"" class=""label label-warning"">" & BOCount & "</span>"
		end if
		
		if glPendingPreAllocs then
			
			sql = "SELECT AcCode FROM ExchequerAllocStockArrival WHERE AcCode = '" & AcDetails.AcCode & "' AND IsConfirmed = 0"
			jhAdmin sql
			set rs = db.execute(sql)
			If NOT (rs.bof OR rs.eof) Then
				ShowPendingPA = True
				ShowPendingPAStr = "&nbsp;<span class=""label label-important""><i title=""" & lne("label_you_have_pre_allocs") & """ class=""icon-asterisk icon-white"" style=""color:red !important;""></i></span>"
			End If
			rsClose()		
		end if	
		'jh BOStr

%>			
			
			<div class="span4 spacer10">
	
				<h3 class=xspacer20><%=lne("label_order_management")%></h3>

<%
jhAdmin "glCustom = " & glCustom

if glCustom = "UK" then
%>
				
				<p>
					<a class=OrangeLink href="/utilities/account/backorders/"><%=lne("report_title_back_orders")%></a> <%=BOStr%>
				</p>					

				<p>
					<a class=OrangeLink href="/utilities/account/rotation/"><%=lne("report_title_stock_rotation")%></a>
				</p>					
				<p>
					<a class=OrangeLink href="/utilities/account/warranty/"><%=lne("report_title_warranty_returns")%></a>
				</p>					

				<p>
					<a class=OrangeLink href="/utilities/account/pre-alloc/"><%=lne("label_pending_pre_alloc")%> <%=ShowPendingPAStr%></a> 
				</p>					
					
		
				
<%
end if
%>				
				
				<p>
					<a class=OrangeLink href="/utilities/account/reports/forward-orders/"><%=lne("report_title_forward_orders")%></a>
				</p>

				<p>
					<a class=OrangeLink href="/utilities/account/repeat/"><%=lne("report_title_repeat_orders")%></a>
				</p>

				<p>
					<a class=OrangeLink href="/utilities/import/"><%=lne("home_heading_import")%></a>
				</p>
				
				<hr>
				
					<h3 class=spacer20><%=lne("label_reports")%></h3>
						
					<p>
						<a class=OrangeLink href="/utilities/account/reports/forward-orders/"><%=report_title_forward_orders%></a>
					</p>
						
					<p>
						<a class=OrangeLink href="/utilities/account/reports/top-sellers-not-listed/"><%=lne("report_title_top_sellers")%></a>
					</p>

					<p>
						<a class=OrangeLink href="/utilities/account/reports/statement/"><%=lne("report_title_view_statement")%></a>
					</p>

					<p>
						<a class=OrangeLink href="/utilities/account/reports/sales-by-sku/"><%=lne("report_title_sales_by_sku")%></a>
					</p>
					
					<%
					if glCustom = "UK" then
					%>

                                      	<%
					if (AcDetails.CostCentre = "FR" OR AcDetails.CostCentre = "ES" OR AcDetails.CostCentre = "AT" OR AcDetails.CostCentre = "BE" OR AcDetails.CostCentre = "NL" OR AcDetails.CostCentre = "LU"  OR AcDetails.CostCentre = "DE"  OR AcDetails.CostCentre = "IT"  OR AcDetails.CostCentre = "SE") then
					%>

					<p>
						<a class=OrangeLink href="/utilities/account/reports/statement/cred_alloc.asp"><%=lne("label_credit_alloc")%></a>
					</p>
                                        <%
					end if
					%>
					
						<p>
							<a class=OrangeLink href="/utilities/account/reports/dd-payments/"><%=lne("report_title_dd_payments")%></a>
						</p>					

					<%
					end if
					%>
					
					
				<%
				if glDropBoxImagesLink <> "" then
				%>
					<hr>
					<h3 class=spacer20><%=lne("label_media_centre")%></h3>
					<p>
						<a class=OrangeLink target="_BLANK" href="<%=glDropBoxImagesLink1%>"><%=lne("label_media_centre_link_1")%></a>
						<p><small><%=lne("label_media_centre_explained_1")%></small></p>
					</p>					

					<p>
						<a class=OrangeLink target="_BLANK" href="<%=glDropBoxImagesLink2%>"><%=lne("label_media_centre_link_2")%></a>
						<p><small><%=lne("label_media_centre_explained_2")%></small></p>
					</p>			
		
					<p>
						<a class=OrangeLink target="_BLANK" href="<%=glDropBoxImagesLink4%>"><%=lne("label_media_centre_link_4")%></a>
						<p><small><%=lne("label_media_centre_explained_4")%></small></p>
					</p>			
		

						<p><small><em><%=lne("image_usage_policy")%></em></small></p>


				<%
				end if
				%>				
					
									
				
			</div>				
			
<%
end if
%>
			
			<div class="span5 spacer10">
				
				
				<%
				
				if isjh() or glRangingActive then
				
					sql = "SELECT RangingID, RangingGUID, RangingTitle FROM B2B_Ranging WHERE AcCode = '" & AcDetails.AcCode & "' AND AllowDealerView = 1 AND RangingStatusID IN (0,1) ORDER BY RangingID DESC "
					jhAdmin sql
					x = getrs(sql, RangingArr, RangingC)
					
					if RangingC >= 0 then
				%>
						<h3><%=lne("account_page_ranging_heading")%></h3>
				<%	
						for c = 0 to RangingC
							ThisRangingID = RangingArr(0,c)
							ThisRangingGUID = RangingArr(1,c)
							ThisRangingTitle = RangingArr(2,c)
						%>
							<div class="spacer5">
							<i class="icon icon-chevron-right"></i> <a class="OrangeLink" href="/utilities/ranging/?InitFilter=RANGED&RangingID=<%=ThisRangingID%>&RangingGUID=<%=ThisRangingGUID%>"><%=ThisRangingTitle%></a>
							</div>
						<%
						next
						
						%>
						<hr>
						<%
					end if
					
				end if
				
				SearchVal_INVOICE = request("SearchVal_INVOICE")
				InvoiceOlderThan = request("InvoiceOlderThan")
				if not isdate(FromExDate(InvoiceOlderThan)) then InvoiceOlderThan = ExDate(now)
				
				MaxInvoices = 24
				
				sql = "exec spB2BRecentInvoices '" & AcDetails.AcCode & "', '" & validstr(SearchVal_INVOICE) & "', " & InvoiceOlderThan
				
				SCRLabel = lne("label_scr")
			
				x = getrs(sql, InvoiceArr, InvoiceC)
				
				if InvoiceC > MaxInvoices then InvoiceC = MaxInvoices
				
				set ls = new strconcatstream
				
				if invoiceC = -1 then
					if SearchVal_INVOICE <> "" then
						ls.add "<div class=""alert alert-error"">" & lne("account_invoices_search_not_found") & " - '<strong>" & SearchVal_INVOICE & "</strong>'</div>"
					else
						ls.add "<div class=""alert alert-error"">" & lne("account_invoices_none") & "</div>"
					end if
				else
				
					ls.add "<table width=100% border=1 class=""ReportTable legible"">"
					ls.add "<tr>"
					ls.add "<th></th>"
					ls.add "<th>" & lne("account_invoices_heading_sin") & "</th>"
					ls.add "<th>" & lne("account_invoices_heading_your") & "</th>"
					ls.add "<th>" & lne("account_invoices_heading_date") & "</th>"
					ls.add "<th>" & lne("account_invoices_heading_value") & "</th>"
					ls.add "<th></th>"
					ls.add "<th>" & lne("account_invoices_heading_track") & "</th>"

					ls.add "</tr>"
					
					for c = 0 to InvoiceC
						'jh InvoiceArr(0,c)
						ThisRef = InvoiceArr(0,c)
						ThisDate = InvoiceArr(1,c)
						'jhInfo "ThisDate = " & ThisDate
						DispDate = FromExDate(ThisDate)
						DispDate = localDate(DispDate)
						'jh "DispDate = " & DispDate
						
						YourRef = "" & InvoiceArr(2,c)
						isSCR = false
						ThisValue = convertnum(InvoiceArr(6,c))
						if left(ThisRef,3) = "SCR" then
							isSCR = true
							ThisValue = ThisValue*-1
						end if
						
						InvoiceExists = getInvoiceLink(ThisRef, InvoiceFN)
						'jh "InvoiceExists = " & InvoiceExists
						PDFLink = ""
						
						if invoiceExists then						
							PDFLink = "<a target=""_BLANK"" href=""/utilities/downloader/?Type=INVOICE&i=" & ThisRef & """><img title=""" & lne_NOEDIT("PDF_DOWNLOAD_EXP") & " " & ThisRef & """ style=""border:0px;"" src=""/assets/images/icons/page_white_acrobat.png""></a>"					
						end if

						strTrackLink = ""
						ThisDCID = InvoiceArr(7,c)
						ThisDCCompany = InvoiceArr(8,c)
						ThisDCDate = InvoiceArr(9,c)
		If (Len(ThisDCID) > 0 AND Len(ThisDCCompany) > 0) Then
		if (ThisDCCompany = "PALLETWAYS") Then
						 strTrackLink = "https://track2.palletways.com/?dc_syscon=" + ThisDCID 
                elseif (ThisDCCompany = "DPD") Then
				                 strTrackLink =  "http://www.dpd.co.uk/service/tracking?parcel=" + ThisDCID
                
                elseif (ThisDCCompany = "UPS") Then
                  strTrackLink = "http://wwwapps.ups.com/WebTracking/processRequest?loc=en_GB&tracknum=" + ThisDCID
		End If
		strTrackLink = "<a href=""" + strTrackLink + """ target=""_BLANK""><i class=""icon icon-road""></i></a> "
		End If
						ls.add "<tr>"
						
							ls.add "<td>"
							ls.add "<a href=""#"" class=InvoiceLink data-invoice-no=""" & ThisRef & """><i class=""icon icon-search""></i></a> "
							ls.add "</td>"
						
						ls.add "<td><strong>"
						ls.add ThisRef
						ls.add "</td>"
						ls.add "<td>"
						ls.add YourRef
						ls.add "</td>"										
						ls.add "<td>"
						ls.add DispDate
						ls.add "</td>"					
						ls.add "<td class=text-right>"
						if isSCR then
							ls.add "<span title=""" & SCRLabel & """ class=""label label-success"">" & dispCurrency(ThisValue) & "</span>"
						else
							ls.add dispCurrency(ThisValue)
						end if
						ls.add "</td>"		
						ls.add "<td class=text-center>"
						ls.add "<div style=""width:30px !important;"" >"
						ls.add PDFLink
						ls.add "</div>"					
						ls.add "</td>"
							ls.add "<td align=""center"">"
							ls.add strTrackLink
							ls.add "</td>"
					
						ls.add "</tr>"
						LastDate = ThisDate
					next
					
					ls.add "</table>"
					
					ls.add "<p class=text-right><a class=""OrangeLink legible"" href=""?InvoiceOlderThan=" & LastDate & """>" & lne("label_account_invoices_view_older") & "</a></p>"
					
				end if
				
				
				invoiceOS = ls.value
				
				set ls = nothing
				
				
				AccountClass = ""
				if AcDetails.AvailableCredit >= 100 then AccountClass = "alert-success"
				
				%>
				
				<h3><%=lne("account_page_your_account")%></h3>
				
				<%
					if glUpdateContactDetails then 
				%>					
						<p>
							<a class=OrangeLink href="/utilities/account/contact-details/"><%=lne("label_edit_dealer_contact_details")%></a>						
						</p>	
				<%
					end if				
				%>	
					
					<p>
						<a class=OrangeLink href="/utilities/account/password/"><%=lne("label_change_password")%></a>						
					</p>						
				<div class="alert <%=AccountClass%>">
				
				<div class=spacer10>
					<%=lne("label_current_balance")%>: 
					<strong><%
					=dispCurrency(AcDetails.CurrentBalance)
					%></strong>
				</div>
				<div class=spacer10>
					<%=lne("label_credit_limit")%>: 
					<strong><%
					=dispCurrency(AcDetails.CreditLimit)
					%></strong>
				</div>			
				<div class=spacer10>
					<%=lne("label_available_credit")%>: 
					<strong><%
					=dispCurrency(AcDetails.AvailableCredit)
					%></strong>
				</div>	

				<%
				if glSagePayEnabled then 
				%>
					<div><a class="btn btn-small btn-success" href="/utilities/sagepay/"><%=lne("label_make_payment")%></a></div>
				<%
				end if				
				%>
				
				</div>
								
				<div class="pull-right legible small text-right">					
					<form id="FormInvoiceSearch" method=post action="/utilities/account/">
						<div class="input-append">							
							<input autocomplete="OFF" class="span2 legible" id="SearchVal_INVOICE" style="width:70px;"  name=SearchVal_INVOICE value="<%=SearchVal_INVOICE%>" type="text" placeholder="<%=lne("account_page_search_placeholder")%>">
							<button class="btn" type="submit"><i class="icon icon-search"></i>&nbsp;</button>
						</div>											
					</form>
				</div>				
				
				<h4><%=lne("account_page_recent_invoices")%></h4>
								
				<%
				=invoiceOS
				%>
				
			</div>
		</div>		
	</div>		

<!--#include virtual="/assets/includes/footer.asp" -->

	<!-- Modal Invoice Lookup Form-->
	<div id="ModalContainer_INVOICE" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-body">
			
			<div class=pull-right>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
			</div>
			
			<div id="ModalInvoiceDiv">...</div>	
	
		</div>
		<div class="modal-footer">
			<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">OK</button>						
		</div>
	</div>


<%
function isFOException()

    isFOException = true

end function

function getFileDownloads()

			dim pc, c, x

				ThisCostCentre = AcDetails.CostCentre
				
				set fs = server.createobject("scripting.filesystemobject")
	
				ThisCostBand = AcDetails.CostBand
				
				sql = "SELECT FileName, Params, DirectDownload, FileNotes FROM END_PriceLists WHERE PriceBand = '" & ThisCostBand & "' AND FileStatusID = 1 "
				
				
				if session("ENDURA_EquipeDealer") <> "1" then
					select case ThisCostCentre				
						case "DE", "AT", "CH", "LU"
							HideMTR = true
						case else						
					end select		
				end if
				
				if HideMTR then 
					sql = sql & "AND Params NOT LIKE '%|MTR|%' "
				end if
				
				'jhAdmin sql
				x = getrs(sql, PLArr, pc)
				'jhAdmin "pc = " & pc
								
				fLoc = glPriceListLoc & "Band " & ThisCostBand & "\"
								
				MaxDiff = 0			
				
				'jhAdmin "fLoc = " & floc
				
				if fs.folderexists(floc) then
					'jhAdmin "FOUND PRICE LISTS"
					
%>					
						<div style="padding-left:5px;">
						<h3><%=lne("label_price_lists")%></h3>
<%
					set fx = fs.getfolder(floc)
					
					for each file in fx.files
						ThisExt = lcase("" & right(file.name,4))
						'jhAdmin "ThisExt = " & ThisExt & " (" & file.name & ")"						
						
						fn = lcase("" & file.name)
						IcoFile = "<i class=""icon icon-file""></i> "
										
						ShowThis = false
						FileNotes = ""
						for x = 0 to pc
							CheckFN = PLArr(0, x)
							'jhAdmin "CheckFN = " & CheckFN & " _ " & fn 
							if lcase("" & CheckFN) = lcase("" & file.name) then
								ShowThis = true	
								FileNotes = PLArr(3, x)
								exit for
							end if
						next
						
						if ShowThis then
							dlFileName = file.name
							dlModified = file.datelastmodified
							'jhAdmin ThisCostCentre
							if ThisCostCentre <> "" and 1 = 0 then
								transLoc = floc & ThisCostCentre & "\"									
								'jh transLoc									
								langFile = transLoc & dlFileName
								'jh langFile
								if fs.fileexists(langFile) then
									'jh "FOUND TRANS FILE"
									dlFileName = ThisCostCentre & "\" & dlFileName 
									set v = fs.getfile(langFile)
									dlModified = v.datelastmodified
								end if
							end if
%>							
							<p>								
								<%=icoFile%>
								<a class=OrangeLink target="_BLANK" href="/utilities/downloader/?type=PRICE_LIST&fn=<%=dlFileName%>"><%=FileNotes%></a>								
							</p>								
<%									
							
						end if
					
					next
					
%>
					</div>	
<%					
					
					'jh "FINAL FILE = " & StreamFN
				
				end if
							

end function


%>
<script>

	$(".InvoiceLink").click(function () {
	
		var invoiceNo = $(this).data('invoice-no');
		
			$("#ModalInvoiceDiv").html('<img src="/assets/images/icons/indicator.gif"> loading...');
					
				$.ajax({
					scriptCharset: "ISO-8859-1" ,
					contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",	

					url: "/misc_server.asp?CallType=INVOICE_ENQUIRY&InvoiceNo=" + invoiceNo + "",

					cache: false,			
					}).done(
						function( html ) {
						$("#ModalInvoiceDiv").html(html);
						$("#ModalInvoiceDiv").show(100, function() {
							// Animation complete.
						  });					
					});		
	
		$('#ModalContainer_INVOICE').modal();				
	
	});		
			

	$("#searchVal_INVOICE").keyup(function (e) {
		//alert(e.keyCode);
		if (e.keyCode == 13) {
			$("#FormInvoiceSearch").submit()
		}
	});					
			
</script>
