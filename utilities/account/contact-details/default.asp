<%

PageTitle = "Edit your contact details"

%>

<!--#include virtual="/assets/includes/header.asp" -->
<%

ShowPage = CheckDataLoad(AcDetails.AcCode, "UpdateCustomerAccount")
	
FormAction = request("FormAction")

if FormAction = "UPDATE_CONTACT_DETAILS" then

		Set re = New RegExp
         With re
            .Pattern    = "^[0-9\(\)\+\s]+$"
            .Global     = False
         End With
        
		
	Email = trim("" & request("Email"))
	Contact = trim("" & request("Contact"))
	Phone = trim("" & request("Phone"))
	
	ReturnsEmail = trim("" & request("ReturnsEmail"))
	DespatchEmail = trim("" & request("DespatchEmail"))
	ProformaInvoiceEmail = trim("" & request("ProformaInvoiceEmail"))
	OrderAckEmail = trim("" & request("OrderAckEmail"))

	InvoiceStreet1 = trim("" & request("InvoiceStreet1"))
	InvoiceStreet2 = trim("" & request("InvoiceStreet2"))
	InvoiceTown = trim("" & request("InvoiceTown"))
	InvoiceCounty = trim("" & request("InvoiceCounty"))
	InvoicePostCode = ucase(trim("" & request("InvoicePostCode")))
	
	DelivStreet1 = trim("" & request("DelivStreet1"))
	DelivStreet2 = trim("" & request("DelivStreet2"))
	DelivTown = trim("" & request("DelivTown"))
	DelivCounty = trim("" & request("DelivCounty"))
	DelivPostCode = ucase(trim("" & request("DelivPostCode")))
	
	ErrorsFound = false
	
	if Contact = "" or Phone = "" or Email = "" or InvoiceStreet1 = "" or InvoiceTown = "" or InvoicePostCode = "" or DelivStreet1 = "" or DelivTown = "" or DelivPostCode = "" then
		ErrorsFound = true
		ErrorMessage = "<div>" & lne("contact_details_mandatory_error") & "</div>"
	end if
	
	if not ValidEmailList(Email) then
		ErrorsFound = true	
		ErrorMessage = ErrorMessage & "<div>" & lne("contact_details_email_error") & " - " & Email & "</div>"
	end if
	
	if DespatchEmail <> "" and not ValidEmailList(DespatchEmail) then
		ErrorsFound = true	
		ErrorMessage = ErrorMessage & "<div>" & lne("contact_details_email_error") & " - " & DespatchEmail & "</div>"
	end if
	
	if ReturnsEmail <> "" and not ValidEmailList(ReturnsEmail) then
		ErrorsFound = true	
		ErrorMessage = ErrorMessage & "<div>" & lne("contact_details_email_error") & " - " & ReturnsEmail & "</div>"
	end if	
	
	if ProformaInvoiceEmail <> "" and not ValidEmailList(ProformaInvoiceEmail) then
		ErrorsFound = true	
		ErrorMessage = ErrorMessage & "<div>" & lne("contact_details_email_error") & " - " & ProformaInvoiceEmail & "</div>"
	end if	

	if OrderAckEmail <> "" and not ValidEmailList(OrderAckEmail) then
		ErrorsFound = true	
		ErrorMessage = ErrorMessage & "<div>" & lne("contact_details_email_error") & " - " & OrderAckEmail & "</div>"
	end if		
	
	If NOT re.Test(Phone) Then
		ErrorsFound = true	
		ErrorMessage = ErrorMessage & "<div>" & lne("contact_details_phone_error") & " - " & Phone & "</div>"
    End If
	
	
    Set re = Nothing

	if not ErrorsFound then

		strUpdateXML = "<InvoiceStreet1><![CDATA[" & validstr(CharDecoder(InvoiceStreet1)) & "]]></InvoiceStreet1>"
		strUpdateXML = strUpdateXML & "<InvoiceStreet2><![CDATA[" & validstr(CharDecoder(InvoiceStreet2)) & "]]></InvoiceStreet2>"
		strUpdateXML = strUpdateXML & "<InvoiceTown><![CDATA[" & validstr(CharDecoder(InvoiceTown)) & "]]></InvoiceTown>"
		strUpdateXML = strUpdateXML & "<InvoiceCounty><![CDATA[" & validstr(CharDecoder(InvoiceCounty)) & "]]></InvoiceCounty>"
		strUpdateXML = strUpdateXML & "<InvoicePostCode>" & validstr(CharDecoder(InvoicePostCode)) & "</InvoicePostCode>"
		strUpdateXML = strUpdateXML & "<DelivStreet1><![CDATA[" & validstr(CharDecoder(DelivStreet1)) & "]]></DelivStreet1>"
		strUpdateXML = strUpdateXML & "<DelivStreet2><![CDATA[" & validstr(CharDecoder(DelivStreet2)) & "]]></DelivStreet2>"
		strUpdateXML = strUpdateXML & "<DelivTown><![CDATA[" & validstr(CharDecoder(DelivTown)) & "]]></DelivTown>"
		strUpdateXML = strUpdateXML & "<DelivCounty><![CDATA[" & validstr(CharDecoder(DelivCounty)) & "]]></DelivCounty>"
		strUpdateXML = strUpdateXML & "<DelivPostCode>" & validstr(CharDecoder(DelivPostCode)) & "</DelivPostCode>"
		strUpdateXML = strUpdateXML & "<Contact>" & validstr(CharDecoder(Contact)) & "</Contact>"
		strUpdateXML = strUpdateXML & "<Phone>" & validstr(CharDecoder(Phone)) & "</Phone>"
		strUpdateXML = strUpdateXML & "<Email>" & validstr(CharDecoder(Email)) & "</Email>"
		
		
		sql = "UPDATE END_CRMContacts SET " 
		sql = sql & "DespatchEmail = '" & validstr(DespatchEmail) & "', "		
		sql = sql & "ProformaInvoiceEmail = '" & validstr(ProformaInvoiceEmail) & "', "	
		sql = sql & "OrderAckEmail = '" & validstr(OrderAckEmail) & "', "	
		sql = sql & "ReturnsEmail = '" & validstr(ReturnsEmail) & "' "	
		sql = sql & "WHERE ClientID = '" & AcDetails.AcCode & "' AND UPPER(ContactFirstName) = 'IT SYSTEM PROFILE' "		
		db.execute(sql)
		
		if isAdminUser() then
			EditName = AdminDetails.UserName
		else
			EditName = AcDetails.AcCode
		end if
		
		JobXML = "<UpdateAccount AcCode=""" & AcDetails.AcCode & """ EditedBy=""" & EditName & """ >" & strUpdateXML & " </UpdateAccount>"				
		
		call InsertJob("UpdateCustomerAccount", JobXML, 0)		
		
		UpdateComplete = true
	end if
	
	
	
end if

if FormAction = "" then

	sql = "SELECT Accode, acAddressLine1 AS InvoiceStreet1, acAddressLine2 AS InvoiceStreet2, acAddressLine3 AS InvoiceTown "
	sql = sql & ",acAddressLine4 AS  InvoiceCounty, acPostCode AS InvoicePostCode, acDespAddressLine1 AS DelivStreet1, acDespAddressLine2 AS DelivStreet2 "
	sql = sql & ",acDespAddressLine3 AS DelivTown, acDespAddressLine4 AS DelivCounty, acDeliveryPostCode AS DelivPostCode, "
	sql = sql & "acContact AS Contact, acPhone AS Phone, acEmailAddr AS Email "
	sql = sql & ", cc.ProformaInvoiceEmail, cc.OrderAckEmail, cc.DespatchEmail, cc.ReturnsEmail "
	sql = sql & "FROM Exchequer.ENDU01.CUSTSUPP c WITH (NOLOCK) "
	sql = sql & "INNER JOIN (SELECT * FROM END_CRMContacts WHERE UPPER(ContactFirstName) = 'IT SYSTEM PROFILE') cc ON cc.ClientID COLLATE Latin1_General_CI_AS = c.AcCode  "
	sql = sql & "WHERE AcCode = '" & AcDetails.AcCode & "';"
	set rs = db.execute(sql)

	if not rs.eof then
		'jhInfo "FOUND!"
		
		Email = trim("" & rs("Email"))
		Contact = trim("" & rs("Contact"))
		Phone = trim("" & rs("Phone"))
		
		ReturnsEmail = trim("" & rs("ReturnsEmail"))
		DespatchEmail = trim("" & rs("DespatchEmail"))

		InvoiceStreet1 = trim("" & rs("InvoiceStreet1"))
		InvoiceStreet2 = trim("" & rs("InvoiceStreet2"))
		InvoiceTown = trim("" & rs("InvoiceTown"))
		InvoiceCounty = trim("" & rs("InvoiceCounty"))
		InvoicePostCode = trim("" & rs("InvoicePostCode"))
		
		DelivStreet1 = trim("" & rs("DelivStreet1"))
		DelivStreet2 = trim("" & rs("DelivStreet2"))
		DelivTown = trim("" & rs("DelivTown"))
		DelivCounty = trim("" & rs("DelivCounty"))
		DelivPostCode = trim("" & rs("DelivPostCode"))
		
		ProformaInvoiceEmail = trim("" & rs("ProformaInvoiceEmail"))
		OrderAckEmail = trim("" & rs("OrderAckEmail"))
		
	else
		jhErrorNice "Error loading contact details", "Please contact us"
		
		AllStop()
	end if

end if

%>

	
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
<link rel="stylesheet" href="css/index.css" type="text/css" media="screen" />

	<div class="container">      

		
		
		<div class=row>
			<div class=span12>
				<h3><%=lne("label_edit_dealer_contact_details")%></h3>
				
				
				<%
				if UpdateComplete OR NOT ShowPage then
				%>
					<hr>
					<h5><%=lne("backorders_refresh_in_progress_title")%></h5>
				
					<p><%=lne("contact_details_refresh_explained")%></p>
					
					<!--<p><a class=OrangeLink href="/utilities/account/"><%=lne("contact_details_update_complete_return")%></a></p>-->
				<%
				else
				%>
					<p><span class="label label-important">*</span> <%=lne("contact_details_label_mandatory_items")%></p>	
					<hr>					
				<%
				end if
				%>
			
				
			</div>
		</div>
		
		<%
		if NOT UpdateComplete AND ShowPage then
		%>
		
		<form action="/utilities/account/contact-details/default.asp" method=post class="form-horizontal">
		
		<div class=row>

			<%
			if ErrorMessage <> "" then
				jhErrorNice lne("label_errors_found"), ErrorMessage
			end if
			%>
		
			<div class=span6>
			
				<div class=spacer40></div>
				
					<h4 class=spacer20><%=lne("contact_details_label_main_details")%></h4>
				
					<div class="control-group">
						<label class="control-label" for="Email"><%=lne("contact_details_label_main_email")%> <span class="label label-important">*</span></label>
						<div class="controls">
							<input type="text" id="Email" name="Email" value="<%=Email%>">
							<div><small><%=lne("contact_details_email_explained")%></small></div>
						</div>
					 </div>
					 
					<div class="control-group">
						<label class="control-label" for="Contact"><%=lne("contact_details_label_contact_name")%> <span class="label label-important">*</span></label>
						<div class="controls">
							<input type="text" id="Contact" name="Contact" value="<%=Contact%>">
						</div>
					 </div>

					<div class="control-group">
						<label class="control-label" for="Phone"><%=lne("contact_details_label_phone")%>  <span class="label label-important">*</span></label>
						<div class="controls">
							<input type="text" id="xxxx" name="Phone" value="<%=Phone%>">
						</div>
					</div>			
					
			</div>

			<div class=span6>
				
				
				<div class=spacer40></div>				

					<h4 class=spacer20><%=lne("contact_details_order_emails")%></h4>
					 			
					<div class="control-group">
						<label class="control-label" for="DespatchEmail"><%=lne("contact_details_label_email_despatch")%> </label>
						<div class="controls">
							<input type="text" id="DespatchEmail" name="DespatchEmail" value="<%=DespatchEmail%>">
							<div><small><%=lne("contact_details_invoice_email_explained")%></small></div>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label" for="ReturnsEmail"><%=lne("contact_details_label_email_returns")%> </label>
						<div class="controls">
							<input type="text" id="ReturnsEmail" name="ReturnsEmail" value="<%=ReturnsEmail%>">
							<div><small><%=lne("contact_details_sr_email_explained")%></small></div>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label" for="ProformaInvoiceEmail"><%=lne("contact_details_label_email_accounts")%></label>
						<div class="controls">
							<input type="text" id="ProformaInvoiceEmail" name="ProformaInvoiceEmail" value="<%=ProformaInvoiceEmail%>">
							<div><small><%=lne("contact_details_dd_email_explained")%></small></div>
						</div>
					</div>	
					<div class="control-group">
						<label class="control-label" for="OrderAckEmail"><%=lne("contact_details_label_email_orders")%> </label>
						<div class="controls">
							<input type="text" id="OrderAckEmail" name="OrderAckEmail" value="<%=OrderAckEmail%>">
							<div><small><%=lne("contact_details_order_email_explained")%></small></div>
						</div>
					</div>						
					
					
			</div>								
					
		</div>	
	
		<hr>
	
		<div class=row>

			<div class=span6>
			
				<div class=spacer40></div>
				
					<h4 class=spacer20><%=lne("contact_details_label_invoice_address")%></h4>
					 
	
					<div class="control-group">
						<label class="control-label" for="InvoiceStreet1"><%=lne("contact_details_label_address1")%>  <span class="label label-important">*</span></label>
						<div class="controls">
							<input type="text" id="InvoiceStreet1" name="InvoiceStreet1" value="<%=InvoiceStreet1%>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="InvoiceStreet2"><%=lne("contact_details_label_address2")%> </label>
						<div class="controls">
							<input type="text" id="InvoiceStreet2" name="InvoiceStreet2" value="<%=InvoiceStreet2%>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="InvoiceTown"><%=lne("contact_details_label_town")%>  <span class="label label-important">*</span></label>
						<div class="controls">
							<input type="text" id="InvoiceTown" name="InvoiceTown" value="<%=InvoiceTown%>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="InvoiceCounty"><%=lne("contact_details_label_county")%> </label>
						<div class="controls">
							<input type="text" id="InvoiceCounty" name="InvoiceCounty" value="<%=InvoiceCounty%>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="InvoicePostCode"><%=lne("contact_details_label_postcode")%>  <span class="label label-important">*</span></label>
						<div class="controls">
							<input type="text" id="InvoicePostCode" name="InvoicePostCode" value="<%=InvoicePostCode%>">
						</div>
					</div>					
					
			</div>

			<div class=span6>
				
				
				<div class=spacer40></div>				

					
					<h4 class=spacer20><%=lne("contact_details_label_delivery_address")%></h4>
					 	
					<div class="control-group">
						<label class="control-label" for="DelivStreet1"><%=lne("contact_details_label_address1")%>  <span class="label label-important">*</span></label>
						<div class="controls">
							<input type="text" id="DelivStreet1" name="DelivStreet1" value="<%=DelivStreet1%>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="DelivStreet2"><%=lne("contact_details_label_address2")%> </label>
						<div class="controls">
							<input type="text" id="DelivStreet2" name="DelivStreet2" value="<%=DelivStreet2%>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="DelivTown"><%=lne("contact_details_label_town")%>  <span class="label label-important">*</span></label>
						<div class="controls">
							<input type="text" id="DelivTown" name="DelivTown" value="<%=DelivTown%>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="DelivCounty"><%=lne("contact_details_label_county")%> </label>
						<div class="controls">
							<input type="text" id="DelivCounty" name="DelivCounty" value="<%=DelivCounty%>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="DelivPostCode"><%=lne("contact_details_label_postcode")%>  <span class="label label-important">*</span></label>
						<div class="controls">
							<input type="text" id="DelivPostCode" name="DelivPostCode" value="<%=DelivPostCode%>">
						</div>
					</div>						
					
			</div>					
			
					
		</div>
		
		<div class=row>
			<div class=span12>
				
				<hr>
			
				<input type=submit name=SubmitButton class="btn btn-success" value="<%=lne("label_save_changes")%>">
				
				<input type=hidden name=FormAction class="FormHidden" value="UPDATE_CONTACT_DETAILS">
			</div>
		</div>
		
		</form>		

		<%
		end if
		%>
		
	</div>
	<div class=spacer40></div>			
			

<!--#include virtual="/assets/includes/footer.asp" -->



<%

dbClose()

%>

<%
function CharDecoder(pText)
	
	CharDecoder = pText

    CharDecoder = replace(CharDecoder, "&#261;", "a")
	CharDecoder = replace(CharDecoder, "&#322;", "l")
	CharDecoder = replace(CharDecoder, "&#380;", "z")
	CharDecoder = replace(CharDecoder, "&#378;", "z")
	CharDecoder = replace(CharDecoder, "&#281;", "e")
	CharDecoder = replace(CharDecoder, "&#347;", "s")
	CharDecoder = replace(CharDecoder, "&#263;", "c")
	CharDecoder = replace(CharDecoder, "&#357;", "t")
	CharDecoder = replace(CharDecoder, "&#269;", "c")
	CharDecoder = replace(CharDecoder, "&#283;", "e")
	CharDecoder = replace(CharDecoder, "&#345;", "r")
	CharDecoder = replace(CharDecoder, "&#367;", "u")
	CharDecoder = replace(CharDecoder, "&#324;", "n")
	CharDecoder = replace(CharDecoder, "&#317;", "L")
	CharDecoder = replace(CharDecoder, "&#328;", "n")
	CharDecoder = replace(CharDecoder, "&#314;", "l")
	CharDecoder = replace(CharDecoder, "&#318;", "l")
	CharDecoder = replace(CharDecoder, "&#271;", "d")
	CharDecoder = replace(CharDecoder, "&#341;", "r")
	CharDecoder = replace(CharDecoder, "&#268;", "C")
	CharDecoder = replace(CharDecoder, "&#321;", "L")
	CharDecoder = replace(CharDecoder, "&#379;", "Z")
	CharDecoder = replace(CharDecoder, "&#280;", "E")
	CharDecoder = replace(CharDecoder, "&#356;", "T")

end function

%>

