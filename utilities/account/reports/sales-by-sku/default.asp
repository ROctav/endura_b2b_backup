<%
PageTitle = "Sales by SKU/Colorway"

Response.ContentType = "text/html"
Response.AddHeader "Content-Type", "text/html;charset=ISO-8859-1"
'Response.CodePage = 65001
Response.CharSet = "ISO-8859-1"

Response.Expires = 0
Response.Expiresabsolute = Now() - 1442
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"

%>

<!--#include virtual="/assets/includes/header.asp" -->

<%



%>

<style>
	.ReportTable TD {
		padding:5px;
	}
</style>

<link rel="stylesheet" href="css/index.css" type="text/css" media="screen" />

	<div class="container">      

		<div class="row">      
			<div class="span12 spacer10">
		
					<h3 class=spacer40><%=lne("report_title_sales_by_sku")%></h3>
					
					<p>
						<a class=OrangeLink href="/utilities/account/"><i class="icon-circle-arrow-left"></i> <%=lne("reports_back_to_reports")%></a>
					</p>
			
<%
FormAction = request("FormAction")
SearchText = request("SearchText")
sField = cleannum(request("sField"))
sOrder = request("sOrder")
ColorWay = request("ColorWay")
'jh "ColorWay = " & ColorWay



if sOrder <> "ASC" then sOrder = "DESC"

if FormAction = "" then
		
	if SearchText = "" then SearchText = request.cookies("SKUReport")("SearchText") 
	if RepDateFrom = "" then RepDateFrom = request.cookies("SKUReport")("RepDateFrom")
	if RepDateTo = "" then RepDateTo = request.cookies("SKUReport")("RepDateTo") 
	if ColorWay = "" then ColorWay = request.cookies("SKUReport")("ColorWay")	
	if sField = "" then sField = request.cookies("SKUReport")("sField")	
	if sOrder = "" then sOrder = request.cookies("SKUReport")("sOrder")	
	
else
	'jh "HERE"

	
	RepDateFrom = getdate_JQ("RepDateFrom")
	RepDateTo = getdate_JQ("RepDateTo")
	

	if not isdate(RepDateFrom) then RepDateFrom = dateadd("M",-3,now)
	if not isdate(RepDateTo) then RepDateTo = now

	CountryCheck = request("CountryCheck")
	if CountryCheck = "" then CountryCheck = "1"

	if CountryCheck = "1" then CountryCompare = AcDetails.CostCentre

	PurchaseCutoff = dateadd("m",-6,RepDateFrom)

	'jh "RepDateFrom = " & RepDateFrom
	'jh "RepDateTo = " & RepDateTo
	
	'jhStop
	
end if

if not isdate(RepDateFrom) then RepDateFrom = dateadd("M",-3,now)
if not isdate(RepDateTo) then RepDateTo = now

FROMD = exDate(RepDateFrom)
TOD = exDate(RepDateTo)
PurchaseCutoff = exDate(PurchaseCutoff)	

'jhStop

'if sField = 0 then sField = 2
'jhAdmin "sField = " & sField
'jhAdmin "sOrder = " & sOrder
'if sField = request.cookies("SKUReport")("sField") then
'	if request.cookies("SKUReport")("sOrder") = "DESC" then sOrder = "ASC" else sOrder = "DESC"
'elseif sField = 1 or sField = 2 then
'	sOrder = "DESC"
'else
'	sOrder = "ASC"
'end if
'jhAdmin "sOrder = " & sOrder

if ColorWay = "1" then 
	ColorWay = 1 
	CheckStr = "CHECKED"
else 
	ColorWay = 0
end if

response.cookies("SKUReport")("SearchText") = SearchText
response.cookies("SKUReport")("RepDateFrom") = RepDateFrom
response.cookies("SKUReport")("RepDateTo") = RepDateTo
response.cookies("SKUReport")("ColorWay") = ColorWay
response.cookies("SKUReport")("sField")	= sField
response.cookies("SKUReport")("sOrder")	= sOrder

response.cookies("SKUReport").expires = dateadd("d", 1, now)

'RunReport = true	

if FormAction <> "" then
	RunReport = true
end if

if RunReport then
	dbConnect()
	
	'jh "START REPORT"
	
	if sOrder = "ASC" then SortMultiplier = 1 else SortMultiplier = -1

	'jhAdmin "sField = 0" & sField
	'jhAdmin "SortMultiplier = 0" & SortMultiplier
	
	sql = "exec [spB2BSalesBySKUReport] 0" & AcDetails.SelectedLanguageID & ", " & FROMD & ", " & TOD & ", '" & ThisUserID & "', '" & validstr(SearchText) & "', 0" & ColorWay & ", 0" & sField
	'jhAdmin sql
	'xxx
	'jhStop
	

	x = getrs(sql, RecArr, rc)
	'#jhAdmin "rc = " & rc
			
	if rc < 0 then
		os = "<p>" & lne("label_no_records_in_range") & "</p> "
	
	end if
	
	set ls = new strconcatstream
	set csvOut = new strconcatstream
	
	'jhstop

	if ColorWay = "1" then
		
		'jh "GROUP"
	
	else
	
		'jh "DONT GROUP"
	
	end if
	
	
	
	if rc >= 0 then
	
		ls.add "<table class=""ReportTable legible"" border=1>"
		

		'jhStop
	
		if ColorWay = "1" then
			
			'jh "GROUP"
			ls.add "<tr class=TableHead>"					
			ls.add "<td></td>"
			ls.add "<td>Code</td>"
			ls.add "<td>Name</td>"
			ls.add "<td>Color</td>"
			ls.add "<td align=right>Qty</td>"
			ls.add "<td align=right>Value</td>"		
			ls.add "</tr>"

			csvOut.add "Code,"
			csvOut.add "Name,"
			csvOut.add "Color,"
			csvOut.add "Qty,"
			csvOut.add "Value,"
			csvOut.add vbcrlf			
			
			CodeCol = 2
		
		else
		
			'jh "DONT GROUP"
			
			ls.add "<tr class=TableHead>"					
			ls.add "<td></td>"
			ls.add "<td>SKU</td>"
			ls.add "<td>Name</td>"
			ls.add "<td>Color</td>"
			ls.add "<td>Size</td>"
			ls.add "<td align=right>Qty</td>"
			ls.add "<td align=right>Value</td>"		
			ls.add "</tr>"

			csvOut.add "SKU,"
			csvOut.add "Name,"
			csvOut.add "Color,"
			csvOut.add "Size,"
			csvOut.add "Qty,"
			csvOut.add "Value,"
			csvOut.add vbcrlf					
			
			CodeCol = 3
		
		end if	
	
	
	
		for c = 0 to rc
			
			ThisProductID = RecArr(0,c)
			ThisProductCode = RecArr(1,c)
			ThisVariantCode = RecArr(2,c)
			
			DisplayCode = RecArr(CodeCol,c)
			
			ThisName = "" & RecArr(9,c)
			
			if ThisName = "" then 
				ThisName = RecArr(4,c)
			end if
			
			ThisColor = RecArr(5,c)
			ThisSize = RecArr(6,c)
			
			imgURL = getImageColorThumb(ThisProductCode, ThisVariantCode, imgURL)
			'jh "imgURL = " & imgURL
			
			thisQty = RecArr(7,c)
			thisVal = cdbl(RecArr(8,c))
	
			if thisVal > 0 then
		
				ls.add "<tr>"			
				ls.add "<td class=MainSmall valign=top><a class=OrangeLink href=""/products/?ProductID=" & ThisProductID & """><img class=ProdThumb60 src=""" & imgURL & """></a></td>"
				ls.add "<td class=MainSmall valign=top><a class=OrangeLink href=""/products/?ProductID=" & ThisProductID & """>" & DisplayCode & "</a></td>"
				ls.add "<td class=MainSmall valign=top>" & ThisName & "</td>"
				ls.add "<td class=MainSmall valign=top>" & ThisColor & "</td>"
				
				if ColorWay = "1" then
					
				else
					ls.add "<td class=MainSmall valign=top>" & ThisSize & "</td>"
				end if
				
				ls.add "<td class=MainSmall style=""text-align:right;font-weight:bold;"" valign=top>" & formatnumber(thisQty,0) & "</td>"
				ls.add "<td class=MainSmall style=""text-align:right;font-weight:bold;"" valign=top>" & dispCurrency(thisVal) & "</td>"
				ls.add "</tr>"
				
				csvOut.add DisplayCode & ","
				csvOut.add replace(ThisName, ",", ".") & ","
				csvOut.add ThisColor & ","
				
				if ColorWay = "1" then
					
				else
					csvOut.add ThisSize & ","
				end if				
				csvOut.add formatnumber(thisQty,0,,,false) & ","
				csvOut.add dispCurrency_PLAIN(thisVal) & ","
				csvOut.add vbcrlf			
			end if
			
		next	

		
		if 1 = 0 then
		
			for c = 0 to rc
				ThisName = RecArr(2,c)
				
				if ColorWay = 1 then
					'ThisName = remSize("" & ThisName)
				end if
				'ThisName = strReplace(ThisName)
				
				ThisCode = trim("" & RecArr(1,c))
				thisQty = RecArr(3,c)
				thisVal = RecArr(4,c)
				'jh "thisQty = " & thisQty
				'jh "thisVal = " & thisVal
				ThisQty = cdbl(ThisQty)	
				thisVal = cdbl(thisVal)				
				
				ls.add "<tr>"
				'ls.add "<td class=MainSmall style=""font-weight:bold;"" valign=top>" & c+1 & "</td>"
				ls.add "<td class=MainSmall valign=top>" & ThisCode & "</td>"
				ls.add "<td class=MainSmall valign=top>" & ThisName & "</td>"
				ls.add "<td class=MainSmall style=""text-align:right;font-weight:bold;"" valign=top>" & formatnumber(thisQty,0) & "</td>"
				ls.add "<td class=MainSmall style=""text-align:right;font-weight:bold;"" valign=top>" & dispCurrency(thisVal) & "</td>"
				ls.add "</tr>"
				
				csvOut.add ThisCode & ","
				csvOut.add replace(ThisName, ",", ".") & ","
				csvOut.add formatnumber(thisQty,0,,,false) & ","
				csvOut.add dispCurrency_PLAIN(thisVal) & ","
				csvOut.add vbcrlf			
						
			next
		end if

		
		ls.add "</table>"
		
		'Response.Write ThisList 
	
		os = ls.value
		
		'jh addbr(csvOut.value)
		
		if 1 = 1 then
			DownloadFolder = getAccountDownloadFolder()
			
			fn = "sales_by_sku_report.csv"
			ReportSuccess = writeReportOutput(DownloadFolder, fn, csvOut.value)
			
			'jhAdmin "Account GUID = " & AcDetails.GUID
			
			DownloadURL = "" & AcDetails.DownloadURL & "/" & fn	
			
		end if	
	end if
		
	
end if					
			


			
					%>
		
		
	<form class="well form-inline" name=form1 action="/utilities/account/reports/sales-by-sku/" method=post>

		<p>
			<%=lne("label_compare_sales_from")%>&nbsp;<%=DrawDate_JQ("RepDateFrom",RepDateFrom)%> to <%=DrawDate_JQ("RepDateTo",RepDateTo)%>		
		</p>	
	
		<p>
			<%=lne("label_enter_prod_sku_invoice")%>&nbsp;<input type=text name=SearchText value="<%=SearchText%>"> 
		</p>
		
		<p>
			<input type=submit name=submitbutton class="btn btn-success" value="<%=lne("label_run_report")%>">
			&nbsp;
			<input type=checkbox value=1 name="ColorWay" <%if ColorWay = "1" then response.write "CHECKED"%>>&nbsp;<small><%=lne("label_consolidate_report_by_colorway")%> </small>
		</p>

<%
		if DownloadURL <> "" then
%>
			<p><a target="_BLANK" href="<%=DownloadURL%>"><img src="<%=glExcelIcon%>"> <%=lne("label_download_csv")%></a></p>
<%
		end if					
%>	 
	 
		<input type=hidden class=FormHidden name=FormAction value="RUN_REPORT">
	 
     </form>				
		
		
			<%
			=os
			%>	
		
			</div>	
			
			
		</div>		
	</div>		

<!--#include virtual="/assets/includes/footer.asp" -->

