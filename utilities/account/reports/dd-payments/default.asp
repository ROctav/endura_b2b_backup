<%
PageTitle = "Requested Direct Debit Payments"

Response.ContentType = "text/html"
Response.AddHeader "Content-Type", "text/html;charset=ISO-8859-1"
'Response.CodePage = 65001
Response.CharSet = "ISO-8859-1"

Response.Expires = 0
Response.Expiresabsolute = Now() - 1442
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"

%>

<!--#include virtual="/assets/includes/header.asp" -->

<%



%>

<style>
	.ReportTable TD {
		padding:5px;
	}
</style>

<link rel="stylesheet" href="css/index.css" type="text/css" media="screen" />

	<div class="container">      

		<div class="row">      
			<div class="span12 spacer10">
		
					<h3 class=spacer40><%=lne("report_title_dd_payments")%></h3>
					
					<p>
						<a class=OrangeLink href="/utilities/account/"><i class="icon-circle-arrow-left"></i> <%=lne("reports_back_to_reports")%></a>
					</p>
			
<%
FormAction = request("FormAction")

RunReport = true	

if RunReport then
	dbConnect()
	
	
    sql = "SELECT DATEADD(Day, DATEDIFF(Day, 0, RequestedDate), 0) AS RequestedDate, SUM(tranvalue) AS DDTranVlaue, CAST(RequestedDate AS datetime2(0)) AS RequestedDateTime "
    sql = sql & "FROM ExchequerDueSINRequested "
    sql = sql & "WHERE AcCode = '" & AcDetails.AcCode & "' AND   LEFT(TranOurRef, 3) = 'SIN' "
    sql = sql & "GROUP BY DATEADD(Day, DATEDIFF(Day, 0, RequestedDate), 0), CAST(RequestedDate AS datetime2(0)) "
    sql = sql & "ORDER BY RequestedDate DESC "

	
	x = getrs(sql, RecArr, rc)
	
			
	if rc < 0 then
		os = "<p>Sorry, no records found in selected date range "
	
	end if
	
	'jhstop
	
	set ls = new strconcatstream
	
	'jhstop
	
	if rc >= 0 then
	
		ls.add "<table class=""ReportTable legible"" border=1>"
	
		ls.add "<tr class=TableHead>"					
		ls.add "<td>Date</td>"
		ls.add "<td>Amount</td>"
		ls.add "<td colspan=2>Details</td>"
		ls.add "</tr>"				

		for c = 0 to rc
			
			ThisDate = RecArr(0,c)
            ThisDateTime = RecArr(2,c)
			ThisValue = RecArr(1,c)
						
			CheckDate = year(ThisDate) & "/" & right("0" & month(ThisDate),2)
		
			if LastDate <> CheckDate then
			
				DispDate = monthname(month(ThisDate)) & " " & year(ThisDate)
				LastDate = CheckDate
				ls.add "<tr>"			
				ls.add "<td colspan=4>"				
				ls.add "<h4>" & DispDate & "</h4>"
				ls.add "</td>"			
				ls.add "</tr>"		
				
			end if
		
			ls.add "<tr>"			
							
			ls.add "<td class=MainSmall valign=top>" & ThisDate & "</td>"
			ls.add "<td class=""MainSmall numeric"" valign=top><strong>" & DispCurrency(ThisValue) & "</strong></td>"
			
			ls.add "<td class=""MainSmall text-left"" valign=top><i data-sel-date=""" & sqlDate(ThisDate) & """ data-sel-datetime=""" & sqlDate(ThisDateTime) & " " & formatdatetime(ThisDateTime, 3) & """ data-target-div=""DetaiDiv_" & c & """ class=""icon-search DetailLink""></i></td>"
			ls.add "<td><div class=TargetDiv id=""DetaiDiv_" & c & """></div></td>"
			
			ls.add "</tr>"
			
		
		next	
		
		ls.add "</table>"
		
		'Response.Write ThisList 
	
		os = ls.value
		
		'jh addbr(csvOut.value)
			
	end if
		
	
end if					


			
					%>
		
		
		
			<%
			=os
			%>	
		
			</div>	
			
			
		</div>		
	</div>		

<!--#include virtual="/assets/includes/footer.asp" -->


<script>
	
	$(".DetailLink").click (function ()	{
		
		var thisDate = $( this ).data('sel-date')
		var targetDiv = $( this ).data('target-div')
		var thisDateTime = $( this ).data('sel-datetime')
		
		$("#" + targetDiv).html('<span class="AjaxLoader"><img src="/assets/images/icons/indicator.gif"> loading...</span>');
		
		$.ajax({

			url: "/utilities/account/reports/reports_server.asp?CallType=DD_DETAIL&SelDate=" + thisDateTime, 
			cache: false
			}).done(
				function( html ) {
				$("#" + targetDiv).html(html);
				$("#" + targetDiv).show(100, function() {
					// Animation complete.
				  });					
			});		
	
	});
	
</script>