<%
PageTitle = "Forward Orders"

Response.ContentType = "text/html"
Response.AddHeader "Content-Type", "text/html;charset=ISO-8859-1"
'Response.CodePage = 65001
Response.CharSet = "ISO-8859-1"

Response.Expires = 0
Response.Expiresabsolute = Now() - 1442
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"

%>

<!--#include virtual="/assets/includes/header.asp" -->

<%



%>

<style>
	.ReportTable TD {
		padding:5px;
	}
</style>

<link rel="stylesheet" href="css/index.css" type="text/css" media="screen" />

	<div class="container">      

		<div class="row">      
			<div class="span12 spacer10">
		
					<h3 class=spacer40><%=PageTitle%></h3>
					
					<p>
						<a class=OrangeLink href="/utilities/account/"><i class="icon-circle-arrow-left"></i> <%=lne("reports_back_to_reports")%></a>
					</p>
			
<%
SearchVal = request("SearchVal")
SelOrder =  request("SelOrder")

%>
			
				
					
					<%
					
					'jhstop
					
					OrderList = ""				
						
					sql = "exec spB2BForwardOrder_List '" & AcDetails.AcCode & "' "
					x = getrs(sql, OrderArr, OrderC)			

                    sql = "exec spB2BForwardOrder_Details @pAcCode = '" & AcDetails.AcCode & "' "
					if SearchVal <> "" then			
                        sql = sql & ", @Search = '" & validstr(SearchVal) &"' "
					end if
					
					if SelOrder <> "" then
                        sql = sql & ", @SOROurRef = '" & validstr(SelOrder) & "' "
					end if	
	
					jhAdmin sql
					
					
					dbConnect()
					x = getrs(sql, RecArr, rc)
					
'jh "rc = " & rc
'jhstop					
					
					set ls = new strconcatstream
					

					ls.add "<table border=1 class=""ReportTable legible"">"
										
ls.add "<tr>"							
ls.add "<th>Product</th>"		
ls.add "<th>Name</th>"		
ls.add "<th>Colour</th>"						
ls.add "<th>Size</th>"	
ls.add "<th>SKU</th>"		
	
ls.add "<th>Order Ref</th>"						
ls.add "<th>Your Ref</th>"						
ls.add "<th>Due</th>"						
				
					
			
				
ls.add "<th>Outstanding</th>"						
        ls.add "<th>" & lne("bo_header_2") & "</th>"
        ls.add "<th>" & lne("bo_header_1") & "</th>"
        ls.add "<th>" & lne("bo_header_3") & "</th>"

ls.add "</tr>"											
										
					if rc >= 0 then
						
						for c = 0 to rc
						
							ThisBGColor = "white"

							ProductID = RecArr(11,c)
							
						
							'jh InvoiceArr(0,c)
							SKU = RecArr(0,c)
							
							ProductCode = RecArr(6,c)						
							VariantCode = RecArr(7,c)						
							
							ProductName = RecArr(8,c)						
							ColorName = RecArr(9,c)						
							SizeName = RecArr(10,c)						

							DueDate = RecArr(3,c)			
							if isdate(DueDate) then DueDate = formatdatetime(DueDate,2)

							QTYOS = RecArr(1,c)		
							
							TotalQtyOS = TotalQtyOS + QTYOS

					                ThisEDD = RecArr(12,c)
					                ThisIsMultipleDel = CStr("" & RecArr(13,c))
                                           		ThisPickedQty = RecArr(14,c)
							

					                ThisTagNo = CInt(RecArr(15,c))

							If NOT IsNull(RecArr(16,c)) Then

						                ThisFSR = CBool(RecArr(16,c))

								If (ThisFSR) Then

									ThisBGColor = "lightblue"
								End If	

							End If

							strPickedQty = ""
							strEDDValue = ""
					                strMultipleDelValue = ""


		If (DateDiff("d",Now,CDate(DueDate)) > 45) Then
                                                            strPickedQty = lne("bo_label_na") 
                                                            strEDDValue = lne("bo_label_focheckagain") & " " & DateAdd("d", -45, DueDate)
                                                            strMultipleDelValue = lne("bo_label_na") 

		Else 
		
		strPickedQty = ThisPickedQty
		
		blnIsProcessing = (ThisStatus = "SCANNER"  OR ThisTagNo = 18 OR ThisTagNo = 20 OR ThisTagNo = 22 OR ThisTagNo = 40) 

		If (Len(ThisEDD) > 0) Then 

            dtEDDDate = CDate(ThisEDD) 

			if (ThisPickedQty = 0) Then 
				strEDDValue = 	formatdatetime(dtEDDDate,1)
			else
				if ((ThisPickedQty * 100) / QTYOS) < 50 Then
					strEDDValue = 	formatdatetime(dtEDDDate,1)
				else
					if blnIsProcessing Then
						strEDDValue = lne("bo_label_asap")						
					else
						strEDDValue = 	formatdatetime(dtEDDDate,1)
					end if	
				end if				
			end if			
		Else  
            if (ThisPickedQty = 0) Then 
				strEDDValue = lne("label_delivery_TBC")						
			else
				if ((ThisPickedQty * 100) / QTYOS) < 50 Then
					strEDDValue = lne("label_delivery_TBC")						
				else
					if blnIsProcessing Then
						strEDDValue = lne("bo_label_asap")						
					else
						if (QTYOS - ThisPickedQty = 0)  Then
							strEDDValue = 	formatdatetime(DueDate,1)
						else
							strEDDValue = lne("label_delivery_TBC")						
						end if
					end if	
				end if				
				
				
			end if			


		End If


 	
			If ThisIsMultipleDel = "" Then
				If (QTYOS - ThisPickedQty = 0) Then
					strMultipleDelValue = "No"
				Else 
					strMultipleDelValue =  lne("bo_label_na")  
				End If 
			Else
				If ThisIsMultipleDel = "True" Then
					strMultipleDelValue = "Yes"
				Else
					strMultipleDelValue = "No"
				End If
				
			End If
	End If                         

		
							OurRef = RecArr(2,c)	


							
							YourRef = RecArr(5,c)			
							
							if instr(1, OrderList, OurRef) = 0 then
								OrderList = OrderList & OurRef & "|"
											
								OrderCount = OrderCount + 1																
							end if							
							
							
							ls.add "<tr style=""background-color: " & ThisBGColor &  ";"">"
							
							ls.add "<td><strong>"
							ls.add "<a class=OrangeLink href=""/products/?ProductID=" & ProductID & "&BackTo=FO_REPORT"">"
							ls.add ProductCode
							ls.add "</a>"
							ls.add "</td>"							
							
							ls.add "<td>"
							ls.add ProductName
							ls.add "</td>"		
							
							ls.add "<td>"
							ls.add ColorName
							ls.add "</td>"		
							ls.add "<td>"
							'Hardcoded deliberately
							strOverwrittenSize = ShowOverwrittenSizeName(SKU, SizeName)
							'
							ls.add strOverwrittenSize
							ls.add "</td>"		

							ls.add "<td>"
							ls.add SKU
							ls.add "</td>"							
							
							ls.add "<td>"
							ls.add OurRef
							ls.add "</td>"	
							ls.add "<td>"
							ls.add YourRef
							ls.add "</td>"								
							ls.add "<td>"
							ls.add DueDate
							ls.add "</td>"															
							
	
							ls.add "<td class=text-right><h4>"
							ls.add QTYOS
							ls.add "</h4></td>"									

							ls.add "<td align=center>"
							ls.add strPickedQty
							ls.add "</td>"									





                    ls.add "<td class=RepSmall align=center>" 
                    ls.add strEDDValue				
                    ls.add "</td>"				

                    ls.add "<td class=RepSmall align=center>" 
                    ls.add strMultipleDelValue				
                    ls.add "</td>"				


				
							ls.add "</tr>"
						next
						
						ls.add "</table>"
						
						ReportOS = ls.value
						
						set ls = nothing					
					else
						ReportOS = "<p>No current forward orders...</p>"
					end if
					
					jhAdmin "TotalQtyOS = " & TotalQtyOS
					jhAdmin "OrderList = " & OrderList
					

					
					%>
					
	<form class="well form-inline" name=form1 action="/utilities/account/reports/forward-orders/" method=post>
		<%
		if OrderC >= 0 then
		%>
			<p> 
				<%
				OrderOS = "<select name=SelOrder class=form-control>"
				OrderOS = OrderOS & "<option value="""">---</option>"
					for c = o to OrderC
						ThisOurRef = OrderArr(0, c)
						ThisYourRef = OrderArr(1, c)
						ThisDate = OrderArr(2, c)
						if SelOrder = ThisOurRef then SelStr = "SELECTED" else SelStr = ""
						OrderOS = OrderOS & "<option " & SelStr & " value=""" & ThisOurRef & """>" & ThisYourRef & " (" & ThisOurRef & ", " & ThisDate & ")</option>"
					next	
				
				OrderOS = OrderOS & "</select>"
				%>
				<strong>Filter by order:</strong>
				<%
				=OrderOS
				%>
			</p>
		<%
		end if
		%>
		
		<p>
			<strong>Search for</strong>&nbsp;&nbsp;
			<input type=text class=form-control name="SearchVal" value="<%=SearchVal%>">			
		</p>
		
		<input type=submit name=submitbutton class="btn btn-success" value="Run Report">

		 <!--<p><a href="?viewrange=1">Last Month</a> | <a href="?viewrange=3">Last 3 Months</a> | <a href="?viewrange=6">Last 6 Months</a> </p>                                        -->
     
     </form>						
					
					
					<%
					=ReportOS
					%>
		
			</div>	
			
			
		</div>		
	</div>		

<!--#include virtual="/assets/includes/footer.asp" -->

