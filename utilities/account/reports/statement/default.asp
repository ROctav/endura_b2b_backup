

<%
PageTitle = "Your Statement"

Response.ContentType = "text/html"
Response.AddHeader "Content-Type", "text/html;charset=ISO-8859-1"
'Response.CodePage = 65001
Response.CharSet = "ISO-8859-1"

Response.Expires = 0
Response.Expiresabsolute = Now() - 1442
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"

%>

<!--#include virtual="/assets/includes/header.asp" -->

<%



%>

<style>
	.ReportTable TD {
		padding:5px;
	}
</style>

<link rel="stylesheet" href="css/index.css" type="text/css" media="screen" />

	<div class="container">      

		<div class="row">      
			<div class="span12 spacer10">
		
					<h3 class=spacer40><%=lne("label_your_statement")%></h3>
					
					<h4>
					<%
						=AcDetails.CompanyName
					%>
					</h4>
					
					<p>
						<a class=OrangeLink href="/utilities/account/"><i class="icon-circle-arrow-left"></i> <%=lne("reports_back_to_reports")%></a>
					</p>
			
<%

RepDateFrom = getdate_JQ("RepDateFrom")
RepDateTo = getdate_JQ("RepDateTo")

showref = request("showref")
'jhAdmin showref

if isdate(RepDateFrom) and isdate(RepDateTo) then
	RunRep = "-1"
	RepDateFrom = formatdatetime(RepDateFrom,1)
	RepDateTo = formatdatetime(RepDateTo,1)
end if

ReportID = request("ReportID") 
ShowStats = "-1"
RunRep = "-1"



if ShowStats <> "" and RunRep <> "" then
	
	dbConnect()
	
	'jh "START REPORT"
	
	if showref <> "" then
		
		'jh showref
		
		sql = "SELECT * FROM vwOrderLines WHERE thOurRef = '" & validstr(showref) & "'"
		
		'jh sql
		
		set rs = db.execute(sql)
	
		if not rs.eof then
	
			LineArr = rs.Getrows()
			lc = ubound(LineArr,2)
		else
			lc = -1
			os = "Sorry, no records found in selected  report "
		end if		
		
		lo = "<table cellpadding=4 style=""background-color:white;"" border=0>"
		
		
		for c = 0 to lc
			ThisSKU = LineArr(2,c)
			ThisName = LineArr(3,c)
			ThisName = strReplace(ThisName)
			ThisQ = LineArr(4,c)
			
			lo = lo & "<tr>"
			lo = lo & "<td class=MainSmall>"
			lo = lo & ThisSKU
			lo = lo & "</td>"
			lo = lo & "<td class=MainSmall>"
			lo = lo & ThisName
			lo = lo & "</td>"
			lo = lo & "<td class=MainSmall>"
			lo = lo & ThisQ
			lo = lo & "</td>"						
			lo = lo & "</tr>"
		
		next
		
		lo = lo & "</table>"	
		
		
	end if	
	
	sql = "SELECT thAcCode AS idxacCode, thOurRef, thTransDate, 0, 0, 0, 0, 0, thDueDate, 0, thCurrency, thDailyRate, 0, thYourRef, 0, thUserField2 AS SRNRef "
	sql =  sql & " FROM Exchequer.ENDU01.DOCUMENT WITH (NOLOCK) WHERE thAcCode = '" & AcDetails.AcCode & "' AND thDocType  IN (0,1,2) AND thOutstanding = 'C'  AND thPostedDate <> '' "
	sql =  sql & " ORDER by thTransDate DESC"
	
	set rs = db.execute(sql)
	
	if not rs.eof then
	
		RecArr = rs.Getrows()
		rc = ubound(RecArr,2)
	else
		rc = -1
		os = "<p>Sorry, no records found in selected report</p>"
	end if
	
	'jh "GOT"
	
	rsClose()
	
	
	MaxProds = 10
	
	TotalOutstanding = 0
	TotalOver = 0
	DueCutoff = formatdatetime(now,1)
		
	if rc >= 0 then
	
		
		CurrStr = ""
	    
		os = "<table border=1 class=legible cellpadding=8 cellspacing=0 xstyle=""width:100%;border:1px solid black;"" >"
		
		os = os & "<tr class=TableHead>"
		os = os & "<td>#</td>"
		os = os & "<td>" & lne("INVOICE NUMBER") & "</td>"
		os = os & "<td>" & lne("YOUR REF") & "</td>"
		os = os & "<td>" & lne("Returns_Ref") & "</td>"
		os = os & "<td>" & lne("DATE") & "</td>"
		os = os & "<td align=right>" & lne("DOWNLOAD") & "</td>"
		os = os & "</tr>"
	
	
		CustomerDiscount = rnull(RecArr(14,0))
	
		for c = 0 to rc
			
			ShowThis = false
		
			ThisRef = RecArr(1,c)
			CreditStr = ""
			CreditStyle = ""
			if left(ThisRef,3) = "SCR" then
				CreditStr = "&nbsp;<span title=""CREDIT"" class=""badge badge-success"">CR</span>"
				CreditStyle = "color:green;"
			end if				
			
			ThisDate = RecArr(2,c)
			ThisDueDate = RecArr(8,c)
			
			ThisCurrency = RecArr(10,c)
			ThisRate = trim("" & RecArr(11,c))
			
			ThisYourRef = rnull(RecArr(13,c))

			ThisSRNRef = rnull(RecArr(15,c))
			If LEFT(ThisRef, 3) = "SRC" OR LEFT(ThisSRNRef, 3) <> "SRN" Then ThisSRNRef = ""
			
			if not isnumeric(ThisRate) or ThisRate = "" then ThisRate = 1 else ThisRate = cdbl(ThisRate)
			
			ThisDate = FromExDate(ThisDate)
			ThisDueDate = FromExDate(ThisDueDate)
			
			ThisTotal = trim("" & RecArr(3,c))
			ThisTotalEx = ThisTotal
			
			ThisTotalVAT = trim("" & RecArr(4,c))
			ThisSettled = trim("" & RecArr(5,c))
			ThisSettledVAT = trim("" & RecArr(6,c))
			
			ThisDue = RecArr(8,c)
			ThisDue = FromExDate(ThisDue)
			
			ThisTerms = "" & RecArr(9,c)
			if ThisTerms <> "" then ThisTerms = 0 else ThisTerms = cdbl(ThisTerms)
			
			ThisDiscount = trim("" & RecArr(7,c)) 
			
			LineStr = "Total: " & ThisTotal & "_" & ThisTotalVat & "<br>"  & "Settled: " & ThisSettled & "_" & ThisSettledVAT & "_" & ThisDiscount
						
			LineMultiplier = 1
			if left(ThisRef,3) <> "SIN" then LineMultiplier = -1
			
			if not isnumeric(ThisTotal) then ThisTotal = 0 else ThisTotal = cdbl(ThisTotal)
			if not isnumeric(ThisTotalVAT) then ThisTotalVAT = 0 else ThisTotalVAT = cdbl(ThisTotalVAT)
			
			if not isnumeric(ThisSettled) then ThisSettled = 0 else ThisSettled = cdbl(ThisSettled)
			if not isnumeric(ThisSettledVAT) then ThisSettledVAT = 0 else ThisSettledVAT = cdbl(ThisSettledVAT)
			
			ThisTotal = (ThisTotal + ThisTotalVAT) - ThisDiscount
			
			ThisSettled = ThisSettled + ThisSettledVAT
			
			ThisTotal = ThisTotal*LineMultiplier
						
			ThisOutstanding = (ThisTotal - ThisSettled)
			LineOutstanding = ThisOutstanding
			

			
			ThisSettled = ThisSettled*LineMultiplier
			
			
			
			if ThisOutstanding <> 0 then 
				ShowThis = true
				
				
				AdjustedDiscountTotal = (ThisTotalEx * (1-CustomerDiscount)) + ThisTotalVAT
				if AdjustedDiscountTotal = ThisSettled or ThisTotalEx = 0 then			
					ShowThis = false				
				end if
				
			end if
			
			
			if formatnumber(ThisSettled + ThisTotal,2) = 0 then ShowThis = false
			
			if cdbl(ThisOutstanding) < 1 and cdbl(ThisOutstanding) > -1 then ShowThis = false
			
			' Replaced by EM with real time version
			'if ShowThis then
			if True then


				TotalOutstanding = TotalOutstanding + ThisOutstanding	
			
				ThisDD = datediff("d", DueCutoff, ThisDue)
				
				if ThisDD < 0 then	
				
					TotalOver = TotalOver + LineOutstanding
					
				end if	
				
				InvoiceExists = getInvoiceLink(ThisRef, InvoiceFN)

				PDFLink = ""
				
				if invoiceExists then						
                    			PDFLink = "<a target=""_BLANK"" href=""/../../utilities/downloader/?Type=INVOICE&i=" & ThisRef & """><img title=""" & lne_NOEDIT("PDF_DOWNLOAD_EXP") & " " & ThisRef & """ style=""border:0px;"" src=""" & glWebsiteURL & "/assets/images/icons/page_white_acrobat.png""></a>"					
				end if				
				
				DispSettled = ThisSettled
				DispOutstanding = ThisOutstanding
			
				ShowCount = ShowCount + 1
				os = os & "<tr>"
				os = os & "<td class=MainSmall style=""font-weight:bold;"" valign=top>" & ShowCount & "</td>"
				os = os & "<td class=MainSmall style=""text-align:left;font-weight:bold;"" valign=top><strong>" & ThisRef & CreditStr & "</strong></td>"
				os = os & "<td class=MainSmall valign=top>" & ThisYourRef & "</td>"
				os = os & "<td class=MainSmall valign=top>" & ThisSRNRef & "</td>"
				os = os & "<td class=MainSmall valign=top>" & localDate(ThisDate) & "</td>"
				os = os & "<td align=center class=MainSmall valign=top>" & PDFLink & "</td>"
				os = os & "</tr>"
				
				if ShowRef = ThisRef then
					
					os = os & "<tr><td colspan=2 align=right class=MainSmall>INVOICE&nbsp;LINE&nbsp;DETAILS</td><td colspan=3>" & lo & "</td></tr>"
				
				end if
			
        	        RowsExist = true
			end if
		next

        if RowsExist = true then
            os = "<p><a class=OrangeLink href=""" & glWebsiteURL & "/utilities/downloader/default.asp?Type=STATEMENT&i=" & AcDetails.AcCode & """><img src=""" & glWebsiteURL & "/assets/images/icons/file-download.png""/><font color=""#E37795""><u>Download Statement</u></font></a></p>" & os
        end if

		os = os & "</table>"
		
		ShowEmailLink = true
		
		ThisList = ""

        
	end if
	
	
	
	if request("FormAction") = "SEND_EMAIL" then
				
		EmailTo = request("EmailTo")
		
		if validemail(EmailTo) then
			
            MailBody =  GetLabel("Your current statement from Endura") & " (" & AcDetails.AcCode & ")"
			
			SendSubject = "Endura reports: Your statement " & AcDetails.AcCode

            if not isobject(fs) then
			    set fs = server.createobject("Scripting.FileSystemObject")
		    end if	

            if fs.FileExists(glStatementPDFLoc & AcDetails.AcCode & "_st.DOCX") then
                EmailSent = SendMailWithAttachment(SendSubject, MailBody, EmailTo, "ukcustomerservices@endura.co.uk", true, glStatementPDFLoc & AcDetails.AcCode & "_st.DOCX")
            elseif  fs.FileExists(glStatementPDFLoc & AcDetails.AcCode & "_st.PDF") then
		        EmailSent = SendMailWithAttachment(SendSubject, MailBody, EmailTo, "ukcustomerservices@endura.co.uk", true, glStatementPDFLoc & AcDetails.AcCode & "_st.PDF")
            end if
		elseif EmailTo = "" then
			jhErrorNice getlabel(lne("errors_errors_found"), lne("errors_email_not_entered"))
		else
			jhErrorNice getlabel(lne("errors_errors_found"), lne("errors_email_not_valid"))			
		end if	
		
	end if
	
    
	if EmailSent then
		jhInfoNice lne("statement_email_sent"), lne("statement_email_sent_to") & " " & EmailTo	
	elseif ShowEmailLink then
	
        Response.Write os
	
        if RowsExist = true  then
		    if EmailTo = "" then
			    EmailTo = AcDetails.ContactEmail
		    end if	
	        %>
	        &nbsp;
	        <p>
            <a class=OrangeLink href="<%=glWebsiteURL%>/utilities/downloader/default.asp?Type=STATEMENT&i=<%=AcDetails.AcCode%>"><img src="<%=glWebsiteURL%>/assets/images/icons/file-download.png" /><font color="#E37795"><u>Download Statement</u></font></a>
            </p>
            <p></p>
	        <form method="post" action="" name=form1 style="">                                              
		
		        <%=lne("label_send_statement_to")%>
		        <br>
		        <input class=FormFieldWide style="width:200px;" name="EmailTo" value="<%=EmailTo%>"> 
		        <br><br>
		        <input class="btn btn-success" type=submit name=SubmitButton value="<%=lne("label_send_email")%>"/> 
		        <input type="hidden" name="formtype" value="getbackorders"/>
		        <input type="hidden" name="FormAction" value="SEND_EMAIL"/>
	        </form>		
	        <%		
	    end if
	end if
end if
	
	


%>		
			</div>	
			
			
		</div>		
	</div>		

<!--#include virtual="/assets/includes/footer.asp" -->

