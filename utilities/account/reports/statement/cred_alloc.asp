

<%
PageTitle = "Credit Allocation"

Response.ContentType = "text/html"
Response.AddHeader "Content-Type", "text/html;charset=ISO-8859-1"
'Response.CodePage = 65001
Response.CharSet = "ISO-8859-1"

Response.Expires = 0
Response.Expiresabsolute = Now() - 1442
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"

%>

<!--#include virtual="/assets/includes/header.asp" -->

<%



%>

<style>
	.ReportTable TD {
		padding:5px;
	}
</style>

<link rel="stylesheet" href="css/index.css" type="text/css" media="screen" />

	<div class="container">      

		<div class="row">      
			<div class="span12 spacer10">
		
					<h3 class=spacer40><%=lne("label_credit_alloc")%></h3>
					
					<h4>
					<%
						=AcDetails.CompanyName
					%>
					</h4>
					
					<p>
						<a class=OrangeLink href="/utilities/account/"><i class="icon-circle-arrow-left"></i> <%=lne("reports_back_to_reports")%></a>
					</p>
			
<%




	
	dbConnect()
	

	if (AcDetails.CostCentre = "FR" OR AcDetails.CostCentre = "ES") Then
		strLimitDate = "20170925"
	elseif 	(AcDetails.CostCentre = "AT" OR AcDetails.CostCentre = "BE" OR AcDetails.CostCentre = "NL" OR AcDetails.CostCentre = "LU") Then 
		strLimitDate = "20171016"
	elseif 	(AcDetails.CostCentre = "DE" OR AcDetails.CostCentre = "IT") Then 
		strLimitDate = "20171017"

	end if


	sql = "select d.thOurRef, d.thYourRef, PayRef, OwnCVal, d1.thYourRef, d1.thUserField2, (d1.thNetValue - d1.thTotalLineDiscount + d1.thTotalVAT) AS SettleValue, d1.thTransDate from Exchequer.ENDU01.DOCUMENT d WITH (NOLOCK) "
	sql =  sql & " 	INNER JOIN Exchequer.ENDU01.FinancialMatching fm WITH (NOLOCK) ON fm.DocRef = d.thOurRef AND PayRef LIKE 'SCR%'"
	sql =  sql & " 	INNER JOIN Exchequer.ENDU01.DOCUMENT d1 WITH (NOLOCK) ON d1.thOurRef = fm.PayRef " 
	sql =  sql & " 	INNER JOIN Exchequer.ENDU01.CUSTSUPP cs WITH (NOLOCK) ON cs.acCode = d.thAcCode AND acUserDef9 = 'Yes' "
	sql =  sql & " 	WHERE d.thDocType = 0  and d.thTransDate > '" & strLimitDate & "' AND d.thAcCode = '" & AcDetails.AcCode & "'"
	sql =  sql & " 	ORDER BY PayRef, d.thOurRef"
	
	set rs = db.execute(sql)
	
	if not rs.eof then
	
		RecArr = rs.Getrows()
		rc = ubound(RecArr,2)
	else
		rc = -1
		os = "<p>Sorry, no records found in selected report</p>"
	end if
	
	rsClose()
	
		
	if rc >= 0 then
	
		
		CurrStr = ""
	    
		os = "<table border=1 class=legible cellpadding=8 cellspacing=0 xstyle=""width:100%;border:1px solid black;"" >"
		
		os = os & "<tr class=TableHead>"
		os = os & "<td>" & lne("label_scr") & "</td>"
		os = os & "<td>" & lne("label_your_ref") & "</td>"
		os = os & "<td>" & lne("Returns_Ref") & "</td>"
		os = os & "<td>" & lne("label_value") & "</td>"
		os = os & "<td>" & lne("account_invoices_heading_sin") & "</td>"
		os = os & "<td>" & lne("label_your_ref") & "</td>"
		os = os & "<td>" & lne("header_settled") & "</td>"
		os = os & "</tr>"
	
	
	
		for c = 0 to rc
			
			ThisRef = RecArr(0,c)
			ThisCreditRef = RecArr(2,c)
			
			
			ThisYourRef = rnull(RecArr(1,c))
			ThisCreditYourRef = rnull(RecArr(4,c))

			ThisSRNRef = rnull(RecArr(5,c))
			
			
			ThisSettled = trim("" & RecArr(3,c))
			
			if not isnumeric(ThisSettled) then ThisSettled = 0 else ThisSettled = formatnumber(cdbl(ThisSettled),2)

			ThisTotal = trim("" & RecArr(6,c))
			

			if not isnumeric(ThisTotal) then ThisTotal = 0 else ThisTotal = formatnumber(cdbl(ThisTotal),2)

			
				os = os & "<tr>"
				os = os & "<td class=MainSmall style=""text-align:left;font-weight:bold;"" valign=top><strong>" & ThisCreditRef & "</strong></td>"
				os = os & "<td class=MainSmall valign=top>" & ThisCreditYourRef & "</td>"

				os = os & "<td class=MainSmall valign=top>" & ThisSRNRef & "</td>"
				os = os & "<td class=MainSmall valign=top>" & dispCurrency(ThisTotal) & "</td>"

				os = os & "<td class=MainSmall style=""text-align:left;font-weight:bold;"" valign=top><strong>" & ThisRef & "</strong></td>"
				os = os & "<td class=MainSmall valign=top>" & ThisYourRef & "</td>"
				os = os & "<td class=MainSmall valign=top>" & dispCurrency(ThisSettled) & "</td>"
				os = os & "</tr>"
				


		next

		os = os & "</table>"
		

        
	end if
	
	
	
	
        Response.Write os
	


%>		
			</div>	
			
			
		</div>		
	</div>		

<!--#include virtual="/assets/includes/footer.asp" -->

