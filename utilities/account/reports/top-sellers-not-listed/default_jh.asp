<%
PageTitle = "Top Sellers Not Listed"

Response.ContentType = "text/html"
Response.AddHeader "Content-Type", "text/html;charset=ISO-8859-1"
'Response.CodePage = 65001
Response.CharSet = "ISO-8859-1"

Response.Expires = 0
Response.Expiresabsolute = Now() - 1442
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"

%>

<!--#include virtual="/assets/includes/header.asp" -->

<%



%>

<style>
	.ReportTable TD {
		padding:5px;
	}
</style>

<link rel="stylesheet" href="css/index.css" type="text/css" media="screen" />

	<div class="container">      

		<div class="row">      
			<div class="span12 spacer10">
		
					<h3 class=spacer40><%=lne("tsnl_page_title")%></h3>
					
					<p>
						<a class=OrangeLink href="/utilities/account/"><i class="icon-circle-arrow-left"></i> <%=lne("reports_back_to_reports")%></a>
					</p>
			
<%
RepDateFrom = getdate_JQ("RepDateFrom")
RepDateTo = getdate_JQ("RepDateTo")

if not isdate(RepDateFrom) then RepDateFrom = dateadd("M",-3,now)
if not isdate(RepDateTo) then RepDateTo = now

CountryCheck = request("CountryCheck")
if CountryCheck = "" then CountryCheck = "1"

if CountryCheck = "1" then CountryCompare = AcDetails.CostCentre

PurchaseCutoff = dateadd("m",-6,RepDateFrom)

FROMD = exDate(RepDateFrom)
TOD = exDate(RepDateTo)
PurchaseCutoff = exDate(PurchaseCutoff)


%>
			
	<form class="well form-inline" name=form1 action="/utilities/account/reports/top-sellers-not-listed/" method=post>

		<p>
			<%=lne("label_compare_sales_from")%> <%=DrawDate_JQ("RepDateFrom",RepDateFrom)%> to <%=DrawDate_JQ("RepDateTo",RepDateTo)%>		
		</p>	
	
		<p><strong><%=lne("tsnl_label_compare_with")%></strong>
		<input type=radio value=1 name="CountryCheck" <%if CountryCheck = "1" then response.write "CHECKED"%>> <%=lne("label_your_country")%> <Span class="label label-inverse"><%=AcDetails.CostCentre%></span>
		&nbsp;&nbsp;&nbsp;&nbsp;<input type=radio value=2 name="CountryCheck" <%if CountryCheck = "2" then response.write "CHECKED"%>> <%=lne("label_all_countries")%>
		
		</p>
		<input type=submit name=submitbutton class="btn btn-success" value="<%=lne("label_run_report")%>">

		 <!--<p><a href="?viewrange=1">Last Month</a> | <a href="?viewrange=3">Last 3 Months</a> | <a href="?viewrange=6">Last 6 Months</a> </p>                                        -->
     
     </form>					
					
					<%
					sql = "EXEC [spB2BTop10NotListed] 0" & SelectedLanguageID & ", " & FROMD & ", " & TOD & ", " & PurchaseCutoff & ", '" & AcDetails.AcCode  & "', '', '" & CountryCompare & "' "
					jhAdmin sql
					
					'jhstop
					
					dbConnect()
					x = getrs(sql, RecArr, rc)
					
					set ls = new strconcatstream

					ls.add "<table border=1 class=""ReportTable legible"">"
					
					if rc > 19 then rc = 19
					
					if rc >= 0 then
						
						TopRank = RecArr(2,0)
						TopRank = cdbl(TopRank) 
						
					'	jh TopRank
						
						if TopRank > 0 then 
							RankUnit = TopRank / 100
							'jh RankUnit
						else
							TopRank = 100
							RankUnit = 1
						end if						
						
						for c = 0 to rc
							'jh InvoiceArr(0,c)
							ProductID = RecArr(0,c)
							ProductCode = RecArr(1,c)						
							ProductName = "" & RecArr(3,c)						
							ThisValue = cdbl(RecArr(2,c))
							
							imgURL = getImageDefault(ProductCode, "")
							'jh "imgURL = " & imgURL
							
							ThisSalesRank = (ThisValue/RankUnit)
							
							ThisSalesRank = formatnumber(ThisSalesRank,0)							
							
							ls.add "<tr>"
							ls.add "<td>"
							ls.add "<a href=""/products/?ProductID=" & ProductID & "&BackTo=TOP10REPORT"">"
							ls.add "<img src=""" & imgURL & """ class=ProdThumb60>"
							ls.add "</a>"	
							ls.add "</td>"								
							ls.add "<td class=text-right>"
							ls.add "<span class=""badge badge-success"">" & ThisSalesRank & "%</span>"
							ls.add "</td>"								
						
							ls.add "<td><strong>"
							ls.add "<a class=OrangeLink href=""/products/?ProductID=" & ProductID & "&BackTo=TOP10REPORT"">"
							ls.add ProductCode
							ls.add "</a>"
							ls.add "</td>"
							ls.add "<td>"
							ls.add ProductName
							ls.add "</td>"										
				
							'ls.add "<td class=text-right>"
							'ls.add ThisSalesRank 'dispCurrency(ThisValue)
							'ls.add "</td>"		
							ls.add "</tr>"
						next
						
						ls.add "</table>"
						
						ReportOS = ls.value
						
						set ls = nothing					
					
					end if
					%>
					
					<p class=spacer20>
						<span class="badge badge-success">%</span> <%=lne("tsnl_percentage_explained")%> 
					</p>
					
					<%
					=ReportOS
					%>
		
			</div>	
			
			
		</div>		
	</div>		

<!--#include virtual="/assets/includes/footer.asp" -->

