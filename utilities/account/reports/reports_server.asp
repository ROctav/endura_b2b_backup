<%
Response.ContentType = "text/html"
Response.AddHeader "Content-Type", "text/html;charset=ISO-8859-1"
'Response.CodePage = 65001
Response.CharSet = "ISO-8859-1"

Response.Expires = 0
Response.Expiresabsolute = Now() - 1442
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"

SelectedLanguageID = getSelectedLanguage()
ThisUserID = session("B2B_AcCode")
if not isLoggedIn() then jhStop

%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	</head>
	<body>
	

	<!--#include virtual="/assets/includes/global_functions.asp" -->
<%


CallType = request("CallType")

'jh "CallType = " & CallType

if CallType = "DD_DETAIL" then

	set acDetails = new DealerAccountDetails

	'jhAdmin "CallType = " & CallType
	
	SelDate = request("SelDate")
	
	if isdate(SelDate) then
	
		sql = "select RequestedDate, TranDate, TranOurRef, "
		sql = sql & "DDTranValue = "
		sql = sql & "CASE "
		sql = sql & "WHEN LEFT(TranOurRef, 3) = 'SIN' THEN OrigValue "
		sql = sql & "ELSE "
		sql = sql & "(0 - TranValue) "
		sql = sql & "END "
		sql = sql & "from ExchequerDueSINRequested " 
        sql = sql & "where AcCode = '" & AcDetails.AcCode & "' AND  CAST(RequestedDate AS datetime2(0)) = '" & SelDate & "' " 
        sql = sql & " ORDER BY RequestedDate, LEFT(TranOurRef, 3), RIGHT(TranOurRef, LEN(TranOurRef) - 3), RIGHT(ISNULL(NULLIF(MatchedTranOurRef, ''), 'SIN0'), LEN(ISNULL(NULLIF(MatchedTranOurRef, ''), 'SIN0')) - 3) "

		'jhAdmin sql	
	
		dbConnect()
	
		x = getrs(sql, DetailArr, DetailC)
		
		set ls = new strconcatstream
		
		ls.add "<table class=""ReportTable legible"" border=1>"	
		ls.add "<tr class=TableHead>"					
		ls.add "<td>Invoice Date</td>"
		ls.add "<td>Endura Ref</td>"
		ls.add "<td>Amount</td>"
		ls.add "</tr>"		
		
		for c = 0 to DetailC
		
			ThisDate = DetailArr(1,c)
			ThisRef = DetailArr(2,c)
			ThisAmount = DetailArr(3,c)

			ls.add "<tr>"					
			ls.add "<td>" & ThisDate & "</td>"
			ls.add "<td>" & ThisRef & "</td>"
			ls.add "<td class=numeric><strong>" & dispCurrency(ThisAmount) & "</strong></td>"
			ls.add "</tr>"		
		
		next
		
		ls.add "</table>"		
		
		response.write ls.value
		
		set ls = nothing
	
	end if
		
	dbClose()
	
end if

set acDetails = nothing
dbClose()

%>				

</body>
</html>
