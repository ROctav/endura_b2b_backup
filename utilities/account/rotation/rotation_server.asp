<%
Response.ContentType = "text/html"
Response.AddHeader "Content-Type", "text/html;charset=ISO-8859-1"
'Response.CodePage = 65001
Response.CharSet = "ISO-8859-1"

Response.Expires = 0
Response.Expiresabsolute = Now() - 1442
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"

SelectedLanguageID = getSelectedLanguage()

if not isLoggedIn() then 
	response.end
	jhStop
end if

%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	</head>
	<body>
	

<!--#include virtual="/assets/includes/global_functions.asp" -->
<!--#include virtual="/assets/includes/product_functions.asp" -->
<%

set acDetails = new DealerAccountDetails
ThisUserID = acDetails.AcCode

CallType = request("CallType")

RotationWindowDate = session("RotationWindowDate") 
if not isdate(RotationWindowDate) then RotationWindowDate = now()


Dim intSRBalancePct 
intSRBalancePct = 0

	If CBool(AcDetails.SRCapActive) Then 

	Set objcmd = Server.CreateObject("ADODB.Command")
	Set objcmd.ActiveConnection = db
	objcmd.CommandText = "ExchequerGetSRBalance"
	objcmd.CommandType = adcmdStoredProc
	objcmd.Parameters.append objcmd.createParameter("@AcCode", adVarChar, adParamInput, 10)
	objcmd.Parameters("@AcCode") = ThisUserID 
	objcmd.Parameters.append objcmd.createParameter("@SalesBand", adVarChar, adParamInput, 1)
	objcmd.Parameters("@SalesBand") = "" 
	objcmd.Parameters.append objcmd.createParameter("@ErrorCode", adInteger, adParamOutput, 3)
	Set objrs = Server.CreateObject("ADODB.Recordset")
	Set objrs = objcmd.execute()

	ErrorCode =  objcmd.Parameters("@ErrorCode")

	if ErrorCode = 0 then

			If (NOT objrs.eof) Then 
					If (CDbl(objrs("SalesVal").Value) > 0) Then
				        	intSRBalancePct =  (CDbl(objrs("SRVal").Value) + CDbl(objrs("B2BVal").Value) + CDbl(objrs("PendVal").Value)) * 100 / CDbl(objrs("SalesVal").Value)
					End If
			End If	
	End If

	objrs.Close

	End If



if CallType = "RETURNS_DELETE_ALL" then
	
	
	sql = "DELETE FROM B2B_ReturnItems WHERE AcCode = '" & validstr(acDetails.AcCode) & "' AND ReturnStatusID = 0;"
	jhAdmin sql
	
	db.execute(sql)
	
	jhInfoNice "", lne("rotation_pending_list_cleared")
	
	
elseif CallType = "RETURNS_ADD" then

	ThisSKU = request("SKU")
	ThisQty = cleannum(request("SKUQty"))
	AppendQty = cleannum(request("AppendQty"))
	
	jhAdmin "ADD THIS = " & request("SKU")
	CallContext = request("CallContext")
	
	if CallContext = "DELQTY" then
		sql = "DELETE FROM B2B_ReturnItems WHERE SKU = '" & validstr(ThisSKU) & "' AND AcCode = '" & validstr(acDetails.AcCode) & "' AND ReturnStatusID = 0;"
		db.execute(sql)
		jhInfoNice "Done...", ThisQty & " x " & ThisSKU & " was removed from your stock rotation list"
	elseif CallContext = "SCANNER" and ThisSKU <> "" then

		sql = "exec [spB2B_GetReturns_CHECKALLOWED] '" & validstr(acDetails.AcCode) & "', '" & validstr(ThisSKU) & "', " & 1						
		set rs = db.execute(sql)
		if not rs.eof then
			ThisSKU = trim("" & rs("FullSKU"))
		else
			jhErrorNice "Not found", ThisSKU
			ThisSKU = ""
		end if
		
		if ThisSKU <> "" and ThisQty <> "" then
			sql = "INSERT INTO B2B_ReturnItems (SKU, ReturnQty, AcCode, ReturnStatusID) SELECT SKU, 0 AS ReturnQty, '" & validstr(acDetails.AcCode) & "', 0 AS ReturnStatusID FROM SML_SKUS WHERE SKU = '" & validstr(ThisSKU) & "' "
			sql = sql & "AND NOT EXISTS (SELECT * FROM B2B_ReturnItems WHERE SKU = '" & validstr(ThisSKU) & "' AND AcCode = '" & validstr(acDetails.AcCode) & "' AND ReturnStatusID = 0) "
			db.execute(sql)
			
			sql = "UPDATE B2B_ReturnItems SET ReturnQty = ReturnQty + 1 WHERE SKU = '" & validstr(ThisSKU) & "' AND AcCode = '" & validstr(acDetails.AcCode) & "' AND ReturnStatusID = 0;"
			db.execute(sql)
			
			jhInfoNice "Done...", ThisQty & " x " & ThisSKU & " was added to your stock rotation list"

			If CBool(AcDetails.SRCapActive) AND intSRBalancePct > CInt(AcDetails.SRCapPct) Then 
				jhErrorNice lne("rotation_cap_warn_header"), lne("label_rotation_over_cap") 
			End If

			
		end if
		
	else
	
		if CallContext = "SETQTY" or CallContext = "QUICKQTY" then
			sql = "DELETE FROM B2B_ReturnItems WHERE SKU = '" & validstr(ThisSKU) & "' AND AcCode = '" & validstr(acDetails.AcCode) & "' AND ReturnStatusID = 0;"
			db.execute(sql)
		end if

		
		if ThisSKU <> "" and ThisQty <> "" then
			sql = "INSERT INTO B2B_ReturnItems (SKU, ReturnQty, AcCode) VALUES ('" & validstr(ThisSKU) & "', 0" & ThisQty & ", '" & validstr(acDetails.AcCode) & "');"
			db.execute(sql)
			
			if CallContext = "SETQTY" then
				jhInfoNice "Done...", ThisQty & " x " & ThisSKU & " was added to your stock rotation list"

			elseif CallContext = "QUICKQTY" then
				response.write "<span class=""label label-success""><i class=""icon icon-white icon-check""></i></span>"
			end if
			
			
			If CBool(AcDetails.SRCapActive) AND intSRBalancePct > CInt(AcDetails.SRCapPct) Then 
				jhErrorNice lne("rotation_cap_warn_header"), lne("label_rotation_over_cap") 
			End If


		end if
	end if	
	'jhstop	
	
elseif CallType = "xxxRETURNS_ALL" then		' no longer an ajax function, runs on main page	
	
		
elseif CallType = "RETURNS_LIST" then		
	
	
	sql = "SELECT ExchequerTaskID, Name, Contents, RunType, CurrentActiveStatus, IsFullVersion "
	sql = sql & "FROM ExchequerQueue.dbo.ExchequerQueue "
	sql = sql & "WHERE Name = N'CreateSalesReturn' AND Contents LIKE '%AcCode=""" & AcDetails.AcCode & """%' AND CurrentActiveStatus = 1 "	
	
	set rs = db.execute(sql)
	if not rs.eof then
		ExistingRequest = true
	end if
	rsClose()


	If CBool(AcDetails.SRCapActive) AND intSRBalancePct > CInt(AcDetails.SRCapPct) Then 
		SubmitDisabledStr = "DISABLED"
		ErrorMessage = lne("label_rotation_over_cap") 
	End If

	
	if AcDetails.ParentAcCode <> "" then
		ParentAcCode = AcDetails.ParentAcCode	
		SeparateChildSR = AcDetails.SeparateChildSR
	else
		ParentAcCode = ""	
	end if
	
	
	sql = "exec spB2B_GetReturns_LIST '" & AcDetails.AcCode & "', '" & ParentAcCode & "', '', '" & sqlDate(RotationWindowDate) & "', 0" & SeparateChildSR

	x = getrs(sql, retArr, ReturnsC)
	
	
	if ReturnsC = -1 then
		%>
		<h3><%=lne("rotation_empty_list")%> <a onclick="returnsList()" href="#"><i class=icon-refresh></i></a></h3>
		
		<p>Add items using the 'all qualifying items' tab</p>
		
		<%
	else
		
		%>
		<h3><%=lne("rotation_list_pending")%> <a onclick="returnsList()" href="#"><i class=icon-refresh></i></a></h3>
		<%
		
		set ls = new strconcatstream
		
		ls.add "<table cellpadding=4 class=""ReportTable legible"" border=1>"
		
		ls.add "<tr>"
		ls.add "<th colspan=2>Product</td>"
		ls.add "<th>Max</td>"
		ls.add "<th>Added</td>"
		ls.add "<th><i onclick=""listDelete();"" class=""pointer icon icon-remove""></i></td>"
		ls.add "</tr>"
		
		TotalUnits = 0
		
		for x = 0 to ReturnsC
		
			ThisProductID = RetArr(0,x)
			ThisProductCode = RetArr(1,x)
			ThisVariantCode = RetArr(2,x)
			ThisSKU = RetArr(3,x)
			ThisColor = RetArr(4,x)
			ThisSize = RetArr(5,x)
			
			ThisProductName = RetArr(6,x)
			
			ThisBarCode = RetArr(9,x)
			
			AddedQty = RetArr(17,x)
			
			if ThisSKU <> LastSKU then
			
				TotalUnits = TotalUnits + AddedQty
				
				imgURL = getImageDefault(ThisProductCode, ThisVariantCode)	
				
				SKUName = ThisProductName  & ", " & ThisColor & " - " & ThisSize
			
				ls.add "<tr>"
				ls.add "<td valign=top>"
				if imgURL <> "" then ls.add "<a href=""#"" onclick=""skuClick(' "& ThisSKU & "')""><img class=ProdThumb60 src=""" & imgURL & """></a>"
				ls.add "</td>"			
				ls.add "<td valign=top>"
				ls.add SKUName
				ls.add "<br />" 
				ls.add "<a href=""#"" onclick=""skuClick(' "& ThisSKU & "')""><span class=""badge badge-inverse"">" & ThisSKU & "</span></a>"
				ls.add "&nbsp;&nbsp;&nbsp;<span class=""subtle tiny"">EAN: " & ThisBarCode & "</span>"				
				ls.add "</td>"
				

				MaxAllowed = 0
				
				for y = x to ReturnsC
					if ThisSKU = RetArr(3,y) then
						ThisAllowed =  RetArr(15,y)
						MaxAllowed = MaxAllowed + ThisAllowed						
					else
						exit for
					end if 
				next
				
				ThisPending =  cdbl(RetArr(19,x))
				MaxAllowed = MaxAllowed - ThisPending
				if MaxAllowed < 0 then MaxAllowed = 0
				
				
				if AddedQty > MaxAllowed then
					BadgeClass = "badge-important"
					QtyError = true
					SubmitDisabledStr = "DISABLED"
					SubmitClass = "btn btn-default"
					if ErrorMessage = "" then ErrorMessage = lne("label_rotation_error_too_many_units")
				elseif AddedQty = MaxAllowed then
					BadgeClass = "badge-success"
					SubmitClass = "btn btn-primary"
				else
					BadgeClass = ""
				end if
				
				ls.add "<td valign=top>"
				ls.add "<span class=""badge badge-inverse"">" & MaxAllowed & "</span>"
				ls.add "</td>"		
				ls.add "<td valign=top>"
				ls.add "<span class=""badge " & BadgeClass & """>" & AddedQty & "</span>"
				ls.add "</td>"	
				ls.add "<td valign=top>"
				ls.add "<i onclick=""delQty('" & ThisSKU & "')"" class=""icon icon-remove""></i>"
				ls.add "</td>"					
				
				ls.add "</tr>"				
				
				LastSKU = ThisSKU
				
			end if	
				
		
		next
		
		ls.add "</table>"
		
		if ExistingRequest then
			SubmitDisabledStr = "DISABLED"
			ErrorMessage = lne("label_rotation_error_in_progress")
		end if		
		
		%>
			<%=ls.value%>
		
			<hr>
			
			<form name=formsubmitreturn method=post action="rotation-submit.asp">
				<input type=submit <%=SubmitDisabledStr%> class="<%=SubmitClass%>" value="<%=lne("rotation_submit_btn")%>">
				<input type=hidden class=FormHidden name=FormAction value="SUBMIT_RETURN">
			</form>
<%
if isAdminUser() then
%>
			<form name=formsubmitksit method=post action="rotation-submit.asp">
				<input type=submit <%=SubmitDisabledStr%> class="<%=SubmitClass%>" value="Submit your KSIT request">
				<input type=hidden class=FormHidden name=FormAction value="SUBMIT_KSIT">
			</form>
<%
End If
%>

			
			<%
			if ErrorMessage <> "" then
				jhErrorNice lne("label_rotation_cannot_submit"), ErrorMessage
			end if
			%>
			
			<div class=spacer20></div>
			<%
			jhNice TotalUnits & " Units", "You have added" & "&nbsp;" & TotalUnits & " in total to  your stock rotation list, click the submit button to submit your rotation request"
			%>
			
			
		<%
		
		set ls = nothing
		
		
		killarray RetArr
		
	end if
	dbClose()
	
elseif CallType = "RETURNS_SEARCH" then
	
	SearchVal = trim("" & request("SearchVal"))

	if SearchVal <> "" then
	if AcDetails.ParentAcCode <> "" then
		ParentAcCode = AcDetails.ParentAcCode	
		SeparateChildSR = AcDetails.SeparateChildSR	
	else
		ParentAcCode = ""
	end if		
		
		sql = "exec spB2B_GetReturns_WITHDATE '" & AcDetails.AcCode & "', '" & ParentAcCode & "', '" & validstr(SearchVal) & "', '" & sqlDate(RotationWindowDate) & "', 0" & SeparateChildSR & ", '" & RotationSortOrder & "'"
		   
		dbConnect()
			
		x = getrs(sql, ReturnsArr, ReturnsC)
		
		if ReturnsC >= 0 then
		
			SKUFound = true
			
			ThisProductID = ReturnsArr(0,c)
			ThisCode = ReturnsArr(1,c)
			ThisVariantCode =  ReturnsArr(2,c)
			ThisSKU = ReturnsArr(3,c)
			ThisColor = ReturnsArr(4,c)
			ThisSize = ReturnsArr(5,c)			
			ThisName = ReturnsArr(6,c)
			ThisBarCode =  ReturnsArr(9,c)						
			CurrentlyAdded = convertNum(ReturnsArr(17,c))
			
			MaxAllowed = 0
			SINStr = ""	
			SomeAllowed = false	
				
			'jhstop
			ThisPending = cdbl(ReturnsArr(20,c))
			
			for c = 0 to ReturnsC
				ThisAllowed = cdbl(ReturnsArr(15,c)) '- cdbl(ReturnsArr(20,c))
				ThisDate = ReturnsArr(1,c)
				CutoffDate = ReturnsArr(18,c)
				
				MaxAllowed = MaxAllowed + ThisAllowed
				
				'jhstop
				
				if ThisAllowed > 0 then
					SomeAllowed = true	
					'SINStr = SINStr & "<hr>"
					SINStr = SINStr & "<div>"
					SINStr = SINStr & "<strong>" & ThisAllowed & "</strong> Valid until " & CutoffDate & ""
					SINStr = SINStr & "</div>"
				
				end if
			next
			
			if ThisPending	> 0 then
				SINStr = SINStr & "<div style=""color:red;font-weight:bold;"">"
				SINStr = SINStr & "<strong>" & ThisPending & "</strong> ALREADY PENDING "
				SINStr = SINStr & "</div>"			
				MaxAllowed = MaxAllowed - ThisPending
				if MaxAllowed <= 0 then
					SomeAllowed = false
					MaxAllowed = 0
				end if
			end if
				
			if not SomeAllowed then 
				'jhError "NOT ALLOWED"	
				DisabledStr = "DISABLED"
			end if
				
		end if
		
		'jhstop
		
		rsClose()
		
	end if
	'jh "qc = " & qc

	'jhstop
	if SKUFound then
				
		ProductName = ThisName & " " & ThisCode
		SKUName = ThisName & "<br><x>" & ThisColor & " - " & ThisSize & "</x>"
		
		imgURL = getImageDefault(ThisCode, ThisVariantCode)		
		
		btnLabel = lne("btn_add_to_rotation")
	
		InitReturnQty = 1
		if CurrentlyAdded > 0 then InitReturnQty = CurrentlyAdded
		
		if CurrentlyAdded > MaxAllowed then 
			BadgeClass = "badge-important" 
			QtyError = true
			DisabledStr = "DISABLED"
		elseif CurrentlyAdded = MaxAllowed then 
			BadgeClass = "badge-success" 			
			'DisabledStr = "DISABLED"
		else 
			BadgeClass = ""
		end if
		
		InputMax = MaxAllowed
		
		
%>
		<div class="row QAForm">
			<form id="productform_<%=ThisProductID%>">
				<div class=span1>			
					<a href="/products/?productid=<%=ThisProductID%>"><img title="<%=ProductName%>" src="<%=imgURL%>" class="pull-left ProdThumb60"></a>
				</div>
				<div class=span4>			
					<h5><a href="/products/?productid=<%=ThisProductID%>"><%=SKUName%></a></h5>
					<div class="subtle spacer20">
						<span class="badge badge-inverse"><%=ThisSKU%></span>						
						<span class="subtle tiny">EAN: <%=ThisBarCode%></span>
					</div>
					
					<div class=spacer40>
						Quantity <input <%=DisabledStr%> onchange="" name="SKUQty" id="SKUQty" value="<%=InitReturnQty%>" type="number" max="<%=InputMax%>" ondblclick="setMax(this, '<%=InputMax%>')" autocomplete=off class=BigQty>
						
						<hr>
						<%
						if SomeAllowed then
						%>
						
						You can return up to <span class="badge badge-inverse"><%=MaxAllowed%></span> of this SKU
						<br />
						<span class="badge <%=BadgeClass%>"><%=CurrentlyAdded%></span> currently in your list
						
						<%
						else
							jhErrorNice "Not valid", "This SKU is not currently valid for returns according to our records"
						end if
						
						if QtyError then
						%>
							<hr>
							<strong>You are trying to return too many of this item </strong><br />
							<span class="label label-info"><a class="pointer WhiteLink" onclick="setQty('<%=ThisSKU%>', <%=MaxAllowed%>) "> Set the return qty to <span class="badge badge-inverse"><%=MaxAllowed%></span></a></span>
						<%						
						end if
						%>
						
						<hr>
						

						
						<input style="width:20px;" type=hidden name=SKU value="<%=ThisSKU%>" class=FormHidden>
						<input style="width:20px;" type=hidden name=WASQty value="<%=ReturnQty%>" class=FormHidden>
						
					</div>
					
					<div class=ProductAjax style="text-align:left;" id="ProductAjaxDiv_<%=ThisProductID%>">
					
					</div>					
					

<div class=pull-right>
					<div><strong>Rotation detail</strong></div>
						<%
						=SINStr
						%>
</div>											
					
					<div>
						<button <%=DisabledStr%> onclick="returnsClick(this, <%=ThisProductID%>);return false;" class="btn btn-large btn-primary" data-product-id="<%=ThisProductID%>"><%=btnLabel%></button>
						
						
					</div>
					
				</div>		
			</form>
		
		
		
<%		
	else
		sql = "SELECT * FROM vwB2B_ProductDetails WHERE SKU = '" & validstr(SearchVal) & "' OR stBarCode = '" & validstr(SearchVal) & "' " 
		'jh sql
		dbConnect()
		set rs = db.execute(sql)
		
		if not rs.eof then
			'jhInfo "FOUND!"
			SKU = rs("SKU")
			ProductName = rs("ProductName")
			VariantCode = rs("VariantCode")
			ProductFound = true
		else
			ProductFound = false
		end if
		rsClose()
		
		if ProductFound then
			jhErrorNice "Not valid for return", "The entered SKU/EAN ('" & SearchVal & "'/" & ProductName & ") is not currently valid for returns"
		else
			jhErrorNice "Not a valid SKU/EAN", "The entered SKU/EAN ('" & SearchVal & "') could not be found, please retry - if the problem persists, contact us"
		end if
	end if	
	
end if

dbClose()

set acDetails = nothing
set BasketDetails = nothing

set ls = nothing
set rs = nothing

%>				

</body>
</html>
