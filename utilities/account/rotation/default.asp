<%
PageTitle = "Stock Rotation"

MaxInstructionLength = 50
UseLazyLoad = true

%>

<!--#include virtual="/assets/includes/header.asp" -->

<style>
	.HomePanelLayer {
		color:white;
	}
	.header-panel {		
		color:white;
		
	}
	
	A.whitelink {
		color:white;
	}
	
	.header-title {		
		color:white;
		font-size:36pt;
		font-family:Oxygen, arial;
		text-align:left;
	}	
	
	.header-trans {
		background-image:url(/assets/images/home_panels/semi-trans.png);
		padding:20px 0 20px 0;
		xmargin-top:20px;
		
	}
	
	.header-button {
		text-align:left;
	}
	
	#slide1, #slide2, #slide3, #slide4 {
		border:0px solid red;
		padding-top:50px;
		height:200px;
	}
	
	#feature_slider {
		height:250px !important;
	}
	
	.slide {
		height:220px;
	}
	
	.header-title {
		line-height:30pt;font-size:24pt;
	}
	
	.colSpacer_LEFT {
		border-left:1px solid #f0f0f0;
		padding-left:20px;
	}
	
	.ProductGrid TD {
		font-family:arial;
		font-size:10pt;
		padding:5px;
	}
	
	.legible {
		font-family:arial;
		font-size:8pt;
	}
	
	.basketAddButton {
		border:1px solid red;
	}
	
	#CheckoutTable {
		border:1px solid silver;
	}
	
	#CheckoutTable TD {
		font-family:arial;
		font-size:9pt;
		padding:7px;
	}
	
	.CheckoutImage {
		width:50px;
	}
	
	.CheckoutQty {
		width:40px;
		text-align:right;
	}
	
	.rotation-import {
		height:150px;
		width:90%;
	}
	
</style>

<%

blnIsEuro = NOT (acDetails.CostCentre = "UK" OR CheckIsKSIT(acDetails.CostCentre, glDirectSRList) OR Left(acDetails.AcCode, 4) = "ENDU")

'When fully released remove
blnIsShipDocs = (acDetails.CostCentre = "UK" OR acDetails.CostCentre = "DE" OR acDetails.CostCentre = "PL" OR CheckIsKSIT(acDetails.CostCentre, glDirectSRList) OR Left(acDetails.AcCode, 4) = "ENDU")

if not glStockRotationEnabled then
	dbClose()
	response.redirect "/utilities/account/"
end if

FormAction = request("FormAction")


if FormAction = "ROTATION_IMPORT" then

	ClearPending = request("ClearPending")
	if ClearPending <> "" then ClearPending = true
	
	MaxAllowed = 500
	DelimChar = "~"

	ProcessList = request("ProcessList")
	OrigProcessList = ProcessList
	
	ProcessList = replace(ProcessList, "'", "")	
	ProcessList = replace(ProcessList, """", "")	
	
	ProcessList = replace(ProcessList, chr(10), "|")
	ProcessList = replace(ProcessList, chr(13), "")
	
	ProcessList = replace(ProcessList, chr(9), DelimChar)
	ProcessList = replace(ProcessList, ",", DelimChar)
	ProcessList = replace(ProcessList, ";", DelimChar)
	
	ProcessArr = split(ProcessList, "|")
	
	ProcessC = ubound(ProcessArr)	
	
	ImportErrorList = ""
	ImportUnitTotal = 0
	
	if ClearPending then
		sql = "DELETE FROM B2B_ReturnItems WHERE AcCode = '" & validstr(acDetails.AcCode) & "' AND ReturnStatusID = 0;"
		jhAdmin sql
		db.execute(sql)					
	end if
	
	for c = 0 to ProcessC
		ProcessLine = ProcessArr(c)
		LineCount = LineCount + 1
		
		if ProcessLine <> "" then
			'jh "ProcessLine = " & ProcessLine
			LineArr = split(ProcessLine, DelimChar)
			
			ProcessSKU = ""
			ProcessQty = ""
			ImportMax = false
			
			if ubound(LineArr) >= 0 then
				'jh "VALID LINE"
				ProcessSKU = ucase(trim(LineArr(0)))
				
				if ubound(LineArr) >= 1 then
					ProcessQty = cleannum(LineArr(1))
				end if
				
				if ProcessQty = "" then 
					ProcessQty = 0
					ImportMax = true
				end if
				
				if ProcessSKU <> "" and ProcessQty <> "" then
				
					ProcessQty = cdbl(ProcessQty)
				
					if ProcessSKU <> "" and ProcessQty >= 0 then
					
						'jhInfo "RUN CHECK!"
						sql = "exec [spB2B_GetReturns_CHECKALLOWED] '" & validstr(acDetails.AcCode) & "', '" & validstr(ProcessSKU) & "', " & ProcessQty						
						jhAdmin sql
						
						set rs = db.execute(sql)
						InsertAllowed = false
						MaxExceeded = false
						AttemptedQty = 0
						
						InsertSKU = ProcessSKU
						
						if not rs.eof then
							MaxAllowedQty = cdbl(rs("MaxQty"))
							if ImportMax then ProcessQty = MaxAllowedQty							
							'jh "MaxAllowedQty = " & MaxAllowedQty
							if MaxAllowedQty >= ProcessQty then
								InsertAllowed = true																
							elseif ProcessQty > MaxAllowedQty then
								MaxExceeded = true
								AttemptedQty = ProcessQty
								ProcessQty = MaxAllowedQty
								InsertAllowed = true	
								jhAdmin "Max:" & MaxAllowedQty & " _ Entered:" &  ProcessQty & " (" & ProcessSKU & ")"
							end if
							
							InsertSKU = trim("" & rs("FullSKU"))
						else
							'jhAdmin "EOF - " & ProcessSKU
							
						end if
						
						rsClose()
						
						if InsertAllowed then
							jhAdmin "INSERT (" & InsertSKU & ")"
						
							sql = "DELETE FROM B2B_ReturnItems WHERE SKU = '" & validstr(InsertSKU) & "' AND AcCode = '" & validstr(acDetails.AcCode) & "' AND ReturnStatusID = 0;"
							jhAdmin sql
							db.execute(sql)					
						
							if ProcessQty > 0 then
								sql = "INSERT INTO B2B_ReturnItems (SKU, ReturnQty, AcCode) VALUES ('" & validstr(InsertSKU) & "', 0" & ProcessQty & ", '" & validstr(acDetails.AcCode) & "');"
								jhAdmin sql							
								db.execute(sql)
								
								ImportUnitTotal = ImportUnitTotal + ProcessQty
							end if
							
							ProcesssedCount = ProcessedCount + 1
							
							if ProcesssedCount = MaxAllowed then exit for
							
							
							MaxExceededStr = ""
							if MaxExceeded then
								MaxExceededStr = lne_REPLACE("label_rotation_import_max_exceeded", AttemptedQty & "|" & MaxAllowedQty)
								ImportWarningList = ImportWarningList & "<div class=spacer10><strong>" & ProcessSKU & "</strong> " & MaxExceededStr & "</div>"
							end if							
														
						else
							MaxExceededStr = ""
							ImportErrorList = ImportErrorList & "<div class=spacer10><strong>" & ProcessSKU & "</strong></div>"
						end if
					end if					
					
				end if
			end if
		end if
	next
	
	ImportComplete = true
elseif FormAction = "ROTATION_BOXING" then

	strBoxNo = Request("BoxNo")
	strSRNOurRef = Request("SRNOurRef")
	
	If (strBoxNo <> "" AND strSRNOurRef <> "" AND IsNumeric(strBoxNo) AND Left(strSRNOurRef, 3) = "SRN") Then
	
	For i = 1 To CInt(strBoxNo)
		sql = "INSERT INTO ExchequerSRNShipDocs (srnourref, accode, indivboxno) values ('" & strSRNOurRef &"', '" & AcDetails.AcCode & "', " & i & ")"
		db.execute(sql)
	Next

	JobXML = "<GetReturnShippingDocs SRNOurRef=""" & strSRNOurRef & """ AcCode=""" & AcDetails.AcCode & """ TotalBoxNo=""" & strBoxNo & """ Type=""Stock Rotation""></GetReturnShippingDocs>"
	sql = "INSERT INTO ExchequerQueue.dbo.ExchequerQueue (Name, Contents, RunType, IsFullVersion, CurrentActiveStatus) VALUES ('GetReturnShippingDocs', '" & validstr(JobXML) & "', 'IMMEDIATE', 1, 1)"
	db.execute(sql)
	
	End If
	
	RotationBoxing = true
end if


RotationWindowDate = formatdatetime(now,2)


session("RotationWindowDate") = RotationWindowDate

		KSITSet = False

		KSIT = request.querystring("KSIT")
		
		if KSIT = "YES" then
			KSITSet = True
		end if
'
		RotationSortOrder = request.querystring("RotationSortOrder")
		
		if RotationSortOrder <> "" then
			RotationOrderSet = true
		else
			RotationSortOrder = request.cookies("CookieRotationSortOrder")
		end if
		
		if RotationSortOrder = "" then RotationSortOrder = "DUE"
		
		response.cookies("CookieRotationSortOrder") = RotationSortOrder
		response.cookies("CookieRotationSortOrder").expires = dateadd("d", 100, now)
				
%>

<link rel="stylesheet" href="css/index.css" type="text/css" media="screen" />

	<div class="container">      

		<div class=row>

			<div class=span6>
			
			
                <%  if (AcDetails.AcStatus <> "HOLD" OR isAdminUser() ) Then %>
				<h3>Stock Rotation</h3>
            
				<%
				entryLabel = lne("rotation_enter_a_sku")
				%>
				
				<div class=spacer20></div>	
				<input style="width:70%" type=text class=BigInput id=ReturnsSearch placeholder="<%=entryLabel%>" value="">	
				
				<span class="label label-info legible">
					<input type="checkbox" name="CheckScanner" id="CheckScanner"> <label style="font-size:9pt" for="CheckScanner"><%=lne("label_using_scanner")%></label>
				</span>				
		<% end if %>

				<div class=spacer20></div>
				<div class="xQAForm xlegible">
				
					<div id=AjaxResultDiv class="spacer20" style="margin-right:20px;">
						<!--results here...-->
					</div>					
				
					<div id=AjaxSearchDiv class="legible spacer20">
						<!--search here...-->
					</div>			
			
				</div>				
				
				
				<%
				if ImportComplete then
				%>
					<h3>Import Complete!</h3>
				
					<p>Your list of stock rotation items has been imported, please check the totals before submitting your rotation request</p>
				
				<%
					if ImportWarningList <> "" then
						jhNice "Import warning", "Please note, some warning messages were generated during import " & ImportWarningList
					end if
					if ImportErrorList <> "" then
						jhErrorNice "Errors found!", "Please note, the following SKUs/qtys were not added to your list, please check the qualifiying items list" & ImportErrorList
					end if
				
				end if
				%>
				
			</div>		
			
			<div class=span6>
			
				<% 	If CBool(AcDetails.SRCapActive) Then %>
				<span class="label label-warning"><%=lne("rotation_cap")%></span>
				<div class=spacer20></div>
				<% End If %>

<% If blnIsShipDocs Then %>
				<span class="label label-success"><%=lne("rotation_docs_remind")%></span>
				<div class=spacer20></div>
<% End If %>


				<span class="label label-success"><%=lne("rotation_valid_period")%>&nbsp;<%=RotationWindowDate%></span>
				
				<div class=spacer20></div>
				
				<%
				if RotationBoxing then
					ActiveTab4 = "active" 
				elseif RotationOrderSet then
					ActiveTab2 = "active" 
				elseif RotationImport then 
					ActiveTab3 = "active" 
				elseif Importcomplete then
					ActiveTab1 = "active" 
				else
					ActiveTab2 = "active" 
				end if
				
				%>
				
				<ul class="legible nav nav-tabs" id="myTab">
					<li class="<%=ActiveTab2%>"><a data-toggle="tab" href="#tab1"><%=lne("rotation_qualifying_items")%></a></li>
					<li class="<%=ActiveTab1%>"><a data-toggle="tab" href="#tab0"><%=lne("rotation_your_list")%></a></li>
					<li class="<%=ActiveTab3%>"><a data-toggle="tab" href="#tabimport"><%=lne("rotation_import")%></a></li>
<% if blnIsShipDocs  then %>
					<li class="<%=ActiveTab4%>"><a data-toggle="tab" href="#tabsubmitted"><%=lne("rotation_submitted")%></a></li>
<% End If %>
				</ul>			
			
				<div class="tab-content">
					
				  <div class="tab-pane <%=ActiveTab1%>" id="tab0">
						<div id=AjaxListDiv class=""></div>	
				  </div>
				  <div class="tab-pane <%=ActiveTab2%>" id="tab1">
				<div class="pull-right small legible">
					<%=lne("label_sort_sort_by")%> <select onchange="setRotationSortOrder(this.value)" id="RotationSortOrder" style="width:90px;" class=small>
						<option <%if RotationSortOrder = "DUE" then response.write "SELECTED"%> value="DUE"><%=lne_NOEDIT("label_sort_days_remaining")%></option>
						<option <%if RotationSortOrder = "SKU" then response.write "SELECTED"%> value="SKU"><%=lne_NOEDIT("label_sort_SKU")%></option>						
					</select>
<% if isAdminUser() then %>
&nbsp;KSIT&nbsp;<input type="checkbox" onclick="setKSIT(this)" id="KSIT" class=small value="YES" <% If (KSITSet) Then Response.Write (" checked") %> />
<% End If %>
				</div>				
<%	
	if AcDetails.ParentAcCode <> "" then
		ParentAcCode = AcDetails.ParentAcCode	
		SeparateChildSR = AcDetails.SeparateChildSR			
	else
		ParentAcCode = ""	
	end if

	dbConnect()

	sql = "exec spB2B_GetReturns_WITHDATE '" & AcDetails.AcCode	 & "', '" & ParentAcCode & "', '', '" & sqlDate(RotationWindowDate) & "', 0" & SeparateChildSR & ", '" & RotationSortOrder & "'"
	
	ResultsCount = getrs(sql, AllArr, AllC)

	'Added by EM
	If (KSITSet) Then

		blnIsAnyKSIT = False
		
		for x = 0 to AllC
					
			ThisSKU = AllArr(3,x)
			blnIsAnyKSIT = CheckIsKSIT(ThisSKU, glKSITList)
			If (blnIsAnyKSIT) Then Exit For
		 next

		If (NOT blnIsAnyKSIT) Then AllC = -1 
	End If
	'
	
	set ls = new strconcatstream
	set csvls = new strconcatstream
	
	
	%>
	<h3><%=lne("rotation_qualifying_items")%></h3>
	<%

	if (AcDetails.AcStatus = "HOLD" AND NOT isAdminUser()) then
		ls.add lne("label_rotation_on_hold") 
	elseif AllC = -1 then
		ls.add lne("label_rotation_no_valid_items") 
	else
		ls.add "<table cellpadding=4 class=""ReportTable legible"" border=1>"	
		
		ls.add "<tr>"
		ls.add "<th colspan=2>" & lne("label_product") & "</td>"
		ls.add "<th>" & lne("label_bo_available_qty") & "</td>"
		ls.add "<th>" & lne("label_bo_days_left") & "</td>"
		ls.add "</tr>"		
		
					
		csvls.add "SKU"
		csvls.add ","
		csvls.add "Maximum allowed"
		csvls.add ","
		csvls.add "Barcode/EAN"
		csvls.add ","
		csvls.add "Days left"		
		csvls.add vbcrlf		
		
		LastSKU = "..."
		
		for x = 0 to AllC
				ThisSKU = AllArr(3,x)
				blnIsProcess = True
			'Added by EM
		         	If (KSITSet) Then
					blnISKSITSKU = False
					blnISKSITSKU = CheckIsKSIT(ThisSKU, glKSITList)

					IF (NOT blnISKSITSKU) Then blnIsProcess = False 
				End If
				        
			'		
				If (blnIsProcess) Then

				ThisProductID = AllArr(0,x)
				ThisProductCode = AllArr(1,x)
				ThisVariantCode = AllArr(2,x)
				ThisColor = AllArr(4,x)
				ThisSize = AllArr(5,x)
				
				ThisProductName = AllArr(6,x)
				
				ThisBarCode = AllArr(9,x)
				
				AddedQty = AllArr(17,x)
				
				if ThisSKU <> LastSKU then
				
					'jh "ThisSKU = " & ThisSKU
					DaysLeft = 0
					
					imgURL = ""
					if ThisProductID <> LastProductID then
						
						LastProductID = ThisProductID
					end if				
					
					
					LastSKU = ThisSKU


					MaxAllowed = 0
					
					for y = x to AllC
						if ThisSKU = AllArr(3,y) then
							ThisAllowed =  AllArr(15,y)
							MaxAllowed = MaxAllowed + ThisAllowed		
							ThisCutOff = AllArr(18,y)
							CheckDD = datediff("d", now, ThisCutOff)
							'jhAdmin "CheckDD = " & CheckDD
							if DaysLeft = 0 or CheckDD < DaysLeft then
								DaysLeft = CheckDD
							end if
						else
							exit for
						end if 
					next				
					
					
					ThisPending = cdbl(AllArr(20,x))
					if ThisPending > 0 then
						SomePending = true
					end if
					
					if MaxAllowed - ThisPending > 0 then
					
											
						imgURL = getImageDefaultCDN(ThisProductCode, ThisVariantCode)	
						
						SKUName = ThisProductName  & ", " & ThisColor & " - " & ThisSize
						
						ls.add "<tr>"
						ls.add "<td valign=top>"
						if imgURL <> "" then ls.add "<a href=""#"" onclick=""skuClick(' "& ThisSKU & "')""><img class=""lazy ProdThumb60"" width=""60"" height=""60"" data-original=""" & imgURL & """></a>"
						ls.add "</td>"			
						ls.add "<td valign=top>"
						ls.add SKUName
						ls.add "<br />" 
						ls.add "<a href=""#"" onclick=""skuClick(' "& ThisSKU & "')""><span class=""badge badge-inverse"">" & ThisSKU & "</span></a>" 
						ls.add "&nbsp;&nbsp;&nbsp;<span class=""subtle tiny"">EAN: " & ThisBarCode & "</span>"
						ls.add "</td>"
						ls.add "<td valign=top>"
						ls.add "<span class=""badge badge-inverse"">"
						ls.add MaxAllowed			
						ls.add "</span>"			
						'ls.add ThisPending	
						ls.add "</td>"		
						ls.add "<td class=numeric valign=top>"				
						ls.add "<span class=xlabel>" 
						ls.add DaysLeft
						ls.add "</span>"			
						ls.add "</td>"													
						ls.add "</tr>"
						
						csvls.add ThisSKU
						csvls.add ","
						csvls.add MaxAllowed
						csvls.add ","
						csvls.add ThisBarCode
						csvls.add ","
						csvls.add DaysLeft						
						csvls.add vbcrlf
						
					end if	
					
					
				end if
			End If		
		next
		
		ls.add "</table>"	
		
			downloadFolder = getAccountDownloadFolder()
			
			RotationFN = "rotationitems.csv"
			
			fn = downloadFolder & RotationFN
			
			on error resume next
			
			set fs = server.createobject("scripting.filesystemobject")
			
			set f = fs.createtextfile(fn, true)
			
			f.write csvls.value
		
			f.close
			set f = nothing
			set fs = nothing
			
			if err.number <> 0 then
				FileCreationError = true
				
			end if
			
			on error goto 0
			
		
	
	end if
	
	if FileCreationError then
		jhErrorNice "", "There was an error creating your downloadable csv of qualifying rotation items - please contact us"
	%>
		
	<%
	elseif AllC >= 0 then
	%>
		<p><a target="_BLANK" class=OrangeLink href="/utilities/downloader/?Type=DOWNLOAD_FOLDER&fn=<%=RotationFN%>"><i class="icon icon-download"></i> <%=lne("rotation_download_csv")%></a></p>
	<%
	end if
	
	if SomePending then
		response.write "<div class=spacer10><span class=""label label-important"">" & lne("rotation_some_pending") & "</span></div>"
	end if	
	
	response.write ls.value
	
	killarray AllArr
	
	set ls = nothing
	set csvls = nothing
%>

					
						<input type=hidden id=AjaxAllItems_STATUS value="" class=FormHidden>
						
						<div id=AjaxAllItemsDiv class="">
							<!--list here...-->
						</div>						
				  </div>
				  
				  <div class="tab-pane  <%=ActiveTab3%>" id="tabimport">
						

						<%
						btnLabel = lne("rotation_import_btn")
						%>
						
						<h3><%=lne("rotation_import_title")%></h3>
						
						
						
						<form name=Importform id=Importform method=post actaction="/utilities/account/rotation/">
                <%  if (AcDetails.AcStatus <> "HOLD" OR isAdminUser() ) Then %>
							
							<textarea class="form-control rotation-import" name=ProcessList id="ProcessList"><%=OrigProcessList%></textarea>
						
							<p>
								<input type=checkbox name="ClearPending"> <%=lne("rotation_clear_pending")%> 
							</p>
							<input type=submit class="btn btn-small btn-primary" name=ImportBtn id=ImportBtn value="<%=btnLabel%>">
	
							<div>
								<input type=hidden class="FormHidden" name=FormAction id=FormAction value="ROTATION_IMPORT">
								<input type=hidden class="FormHidden" name=IgnoreErrors id=IgnoreErrors value="">
							</div>	
		<% End If %>

						</form>
						
						<%
						=lne("backorder_import_explained")
						%>
						
				  </div>				  

				  <div class="tab-pane  <%=ActiveTab4%>" id="tabsubmitted">
						
<% if blnIsShipDocs  then %>
						<h3><%=lne("rotation_submitted_title")%></h3>
						
						<form name=BoxingForm id=BoxingForm method=post>
						
<%
	set ls = new strconcatstream

	If (blnIsEuro) Then
		strStatusFilter =  " IsSigned = 0 "
	Else
		strStatusFilter =  " IsSigned = 1 AND IsSent = 1 AND IsArrived = 0 "
	End If

	sql = "SELECT thOurRef, CONVERT(datetime, thtransdate, 112) AS TDate, ISNULL(sd.BoxNum, 0) AS BoxNum, ISNULL(sd.ScanBoxNum, 0) AS ScanBoxNum FROM Exchequer.ENDU01.DOCUMENT d WITH (NOLOCK) "
	sql = sql & "INNER JOIN ExchequerEuroSRNs s ON s.SRNOurRef COLLATE Latin1_General_CI_AS = d.thOurRef and  " & strStatusFilter
	sql = sql & "LEFT OUTER JOIN "
	sql = sql & "(SELECT SRNOurRef, COUNT(*) AS BoxNum, COUNT(CASE WHEN isscanned = 1 THEN 1 ELSE NULL END) AS ScanBoxNum "
	   sql = sql & "FROM Exchequersrnshipdocs "
	sql = sql & "GROUP by SRNOurRef) AS sd ON sd.srnourref COLLATE Latin1_General_CI_AS =  d.thOurRef "
	sql = sql & "WHERE thDocType = 44 AND thRunNo = -125 AND  thLongYourRefTrans LIKE '%ROTATION%' and thAcCode = '" & AcDetails.AcCode & "' "
	sql = sql & "ORDER BY thOurRef"
	 

	ResultsCount = getrs(sql, SubmittedArr, AllC)
	If (AllC >= 0) Then
	
		ls.add "<table cellpadding=4 class=""ReportTable legible"" border=1>"	
		
		ls.add "<tr>"
		ls.add "<th>" & lne("rotation_submitted_head1") & "</th>"
		ls.add "<th>" & lne("rotation_submitted_head2") & "</th>"
		ls.add "<th>" & lne("rotation_submitted_head3") & "</th>"
		ls.add "<th>&nbsp;</th>"
		ls.add "</tr>"	
		
		for x = 0 to AllC
						intBoxNo = SubmittedArr(2,x)
						intScannedBoxNo = SubmittedArr(3,x)
						strSRNOurRef = SubmittedArr(0,x)
						strStatus = lne("rotation_submitted_text3")
						
						If (intScannedBoxNo > 0) Then
						
						If (intScannedBoxNo = intBoxNo) Then
							strStatus = lne("rotation_submitted_text4")
						ElseIf (intScannedBoxNo < intBoxNo) Then
							strStatus = lne("rotation_submitted_text5")
						End If
						
						End If
						
						ls.add "<tr>" 
						ls.add "<td valign=top>" & strSRNOurRef & "</td>"
						ls.add "<td valign=top>" 

						If (intBoxNo > 0) Then
							ls.add lne("rotation_submitted_text2") &  "&nbsp;<span class=""badge badge-inverse"">" & intBoxNo & "</span>"
						Else
							ls.add lne("rotation_submitted_text1") &  "&nbsp;<select name=""SelBoxNo" & x &  """ id=""SelBoxNo" & x & """ style=""width:50px;"" class=small><option value=""1"">1</option><option value=""2"">2</option><option  value=""3"">3</option><option  value=""4"">4</option><option  value=""5"">5</option><option  value=""6"">6</option><option  value=""7"">7</option><option  value=""8"">8</option><option  value=""9"">9</option><option  value=""10"">10</option><option  value=""11"">11</option><option  value=""12"">12</option><option  value=""13"">13</option><option  value=""14"">14</option><option  value=""15"">15</option><option  value=""16"">16</option><option  value=""17"">17</option><option  value=""18"">18</option><option  value=""19"">19</option><option  value=""20"">20</option></select>"
						End If
						ls.add "</td>"

						ls.add "<td valign=top nowrap>" & strStatus & "</td>"
						ls.add "<td valign=top>"
						
						If (intBoxNo = 0) Then
							ls.add "<input type=button class=""btn btn-small btn-primary"" name=""BoxingBtn" & x & """ id=""BoxingBtn" & x & """ value=""OK"" onclick=""SubmitShipDocs(this);""></td>"
							ls.add "<input type=hidden class=""FormHidden"" name=""SelSRNOurRef" & x & """ id=""SelSRNOurRef" & x & """ value=""" & strSRNOurRef & """></td>"

						End If
						ls.add "</tr>"
		
		Next

		ls.add "</table>"	
		
		ls.add "<br><div class=alert>" & lne("rotation_submitted_help") & "</div>"


		response.write ls.value
	
	End If
	
	killarray SubmittedArr
	set ls = nothing

%>						
							
								<input type=hidden class="FormHidden" name=FormAction id=FormAction value="ROTATION_BOXING">
								<input type=hidden class="FormHidden" name="SRNOurRef" id="SRNOurRef" value="">
								<input type=hidden class="FormHidden" name="BoxNo" id="BoxNo" value="">
						</form>
<% End If %>						
				  </div>				  
				  
				  
				</div>			
			
				
			
			</div>			
			
			
		</div>
		
	


	</div>
	<div class=spacer40></div>			
			
<%
DeleteAllStr = lne("label_returns_delete_all")
%>			
			

<!--#include virtual="/assets/includes/footer.asp" -->


<script>
	
	$("#CheckScanner").click( function () {
		$("#ReturnsSearch").focus()
	})	
	
	function processSearch() {
		//alert('processSearch')

		var isScanner = $("#CheckScanner").prop('checked');
		
		//alert(isScanner)
		var searchCode = $("#ReturnsSearch").val();
		
		if (searchCode=='') {
			alert('no code')
		} else if (isScanner) {
			scanItem(searchCode);
		} else {
			returnSearch(searchCode)
		}		
	
	}
	
	$("#ReturnsSearch").change(function () {
		//alert("CHANGED");
		processSearch()
		
	});
	
	function skuClick(pSearchVal) {
		
		$('#ReturnsSearch').val(pSearchVal);
		returnSearch(pSearchVal)
	}
	
	
	function returnSearch(pSearchVal) {
	
		var searchVal = pSearchVal;
	
		$("#AjaxResultDiv").html('');
	
		if (pSearchVal!='') {
		
			$("#AjaxSearchDiv").html('<span class=legible><img src="/assets/images/icons/indicator.gif"> please wait...</span>');
		
			$.ajax({			
				scriptCharset: "ISO-8859-1" ,
				contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
				url: "rotation_server.asp?CallType=RETURNS_SEARCH&SearchVal=" + searchVal,
				cache: false,			
				}).done(
					function( html ) {
					$("#AjaxSearchDiv").html(html);
					$("#AjaxSearchDiv").show(100, function() {
						// Animation complete.
					  });					
					
					var x = document.getElementById('SKUQty');
					
					if (!$(x).attr('DISABLED')) {					
						$(x).focus()
						//alert('focus')  ;						
						$(x).select();
					}
				});	
		}
	}
	
	$('#ReturnsSearch').keypress(function (e) {
	
	  if (e.which == 13) {
		
		//alert('enter key!')
		processSearch()
		
		return false;    //<---- Add this line
	  }
	});			
		
	$(document).ready(function() {
		$("#ReturnsSearch")
			.focus(function () { $(this).select(); } )
			.mouseup(function (e) {e.preventDefault(); });
			
		returnsList()
		$("#ReturnsSearch").focus();
			
	});	
	
	
</script>

<script>

	function returnsClick(obj, pid) {
			
			
			var thisForm = document.getElementById('productform_' + pid)
			var str = $(thisForm).serialize();
			
						
			$.ajax({				
				scriptCharset: "ISO-8859-1" ,
				contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
				url: "rotation_server.asp?CallType=RETURNS_ADD&CallContext=SETQTY",				
				data: str,
				cache: false,			
				}).done(
					function( html ) {
						$("#AjaxResultDiv").html(html);						
						$('#AjaxResultDiv').show(100, function() {
						
						$("#ReturnsSearch").focus();
						$("#ReturnsSearch").val('');
						
						$("#AjaxSearchDiv").html('Enter or scan another product code');
						
						returnsList()
						
						return false;
						
					 });
			});	
			
			//alert(pid);
			
			//checkoutChanged() 
			
			//alert('Button disable')
			
			return false;		
	}
	
	function scanItem(pSKU) {
	
			//alert('scanItem');
	
			$.ajax({				
				scriptCharset: "ISO-8859-1" ,
				contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
				url: "rotation_server.asp?CallType=RETURNS_ADD&CallContext=SCANNER&SKU=" + pSKU + "&SKUQty=1",								
				cache: false,			
				}).done(
					function( html ) {
						$("#AjaxResultDiv").html(html);						
						$('#AjaxResultDiv').show(100, function() {
						
						$("#ReturnsSearch").focus();
						$("#ReturnsSearch").val('');
						
						returnsList()
						
						return false;
						
					 });
			});	
	}	
	
	function returnsList() {
			
			$("#AjaxListDiv").html('<span class=legible><img src="/assets/images/icons/indicator.gif"> please wait...</span>');
			
			$.ajax({				
				scriptCharset: "ISO-8859-1" ,
				contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
				url: "rotation_server.asp?CallType=RETURNS_LIST&CallContext=RETURNS",								
				cache: false,			
				}).done(
					function( html ) {
						$("#AjaxListDiv").html(html);						
						$('#AjaxListDiv').show(100, function() {												
					 });
			});	
			
	}
	
	
	function setQty(pSKU, pQty) {
			$.ajax({				
				scriptCharset: "ISO-8859-1" ,
				contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
				url: "rotation_server.asp?CallType=RETURNS_ADD&CallContext=SETQTY&SKU=" + pSKU + "&SKUQty=" + pQty,								
				cache: false,			
				}).done(
					function( html ) {
						$("#AjaxResultDiv").html(html);						
						$('#AjaxResultDiv').show(100, function() {
						
						$("#ReturnsSearch").focus();
						$("#ReturnsSearch").val(pSKU);
						
						returnSearch(pSKU)
						
						returnsList()
						
						return false;
						
					 });
			});	
	}
	
	function delQty(pSKU) {
			$.ajax({				
				scriptCharset: "ISO-8859-1" ,
				contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
				url: "rotation_server.asp?CallType=RETURNS_ADD&CallContext=DELQTY&SKU=" + pSKU + "",								
				cache: false,			
				}).done(
					function( html ) {
						$("#AjaxResultDiv").html(html);						
						$('#AjaxResultDiv').show(100, function() {
						
						$("#ReturnsSearch").focus();
						$("#ReturnsSearch").val(pSKU);
						
						returnSearch(pSKU)
						
						returnsList()
						
						return false;
						
					 });
			});	
	}
	
	
	function isNumeric(n) {
		return !isNaN(parseFloat(n)) && isFinite(n);
	}
	
	function importClick() {
		alert('import click')
	}
	
	$("#ImportBtn").click (function() {
		$(this).attr('disabled', 'disabled');
		$(this).parents('form').submit()				
	});
	
	/*
	$("#TimeMachineDate").change( function () {
		
		window.location.assign("/utilities/account/rotation/?TimeMachineDate=" + $( this ).val() );
	
	});
	*/
	
	function setMax(pObj, pMax) {
		pObj.value = pMax;
	}
	
	function setRotationSortOrder(pVal) {
	
		//alert(pVal);
		window.location.assign("/utilities/account/rotation/?RotationSortOrder=" + pVal);
	
	}

//Added by EM
	function setKSIT(KSITBox) {
			window.location.assign("/utilities/account/rotation/default.asp" + (KSITBox.checked ? "?KSIT=YES" : ""));
	}
	
	function SubmitShipDocs(pButton) {
		pButton.form.elements["BoxNo"].value = pButton.form.elements["SelBoxNo" + pButton.id.substr(9)].value;
		pButton.form.elements["SRNOurRef"].value = pButton.form.elements["SelSRNOurRef" + pButton.id.substr(9)].value;
		pButton.form.submit();
	}
	//	
	
	
</script>

<%

dbClose()

%>

