<%
PageTitle = "Stock Rotation"

%>

<!--#include virtual="/assets/includes/header.asp" -->

<style>
	.HomePanelLayer {
		color:white;
	}
	.header-panel {		
		color:white;
		
	}
	
	A.whitelink {
		color:white;
	}
	
	.header-title {		
		color:white;
		font-size:36pt;
		font-family:Oxygen, arial;
		text-align:left;
	}	
	
	.header-trans {
		background-image:url(/assets/images/home_panels/semi-trans.png);
		padding:20px 0 20px 0;
		xmargin-top:20px;
		
	}
	
	.header-button {
		text-align:left;
	}
	
	#slide1, #slide2, #slide3, #slide4 {
		border:0px solid red;
		padding-top:50px;
		height:200px;
	}
	
	#feature_slider {
		height:250px !important;
	}
	
	.slide {
		height:220px;
	}
	
	.header-title {
		line-height:30pt;font-size:24pt;
	}
	
	.colSpacer_LEFT {
		border-left:1px solid #f0f0f0;
		padding-left:20px;
	}
	
	.ProductGrid TD {
		font-family:arial;
		font-size:10pt;
		padding:5px;
	}
	
	.legible {
		font-family:arial;
		font-size:8pt;
	}
	
	.basketAddButton {
		border:1px solid red;
	}
	
	#CheckoutTable {
		border:1px solid silver;
	}
	
	#CheckoutTable TD {
		font-family:arial;
		font-size:9pt;
		padding:7px;
	}
	
	.CheckoutImage {
		width:50px;
	}
	
	.CheckoutQty {
		width:40px;
		text-align:right;
	}
	
</style>

<%

FormAction = request("FormAction")



if FormAction = "RETURN_SUBMIT" then


	jhAdmin "UPDATES COMPLETED!"
	
end if

set BasketDetails = new ClassBasketDetails	


'jh "OrderThreshold = " & OrderThreshold

%>

<link rel="stylesheet" href="css/index.css" type="text/css" media="screen" />

	<div class="container">      

		<div class=row>
			<hr>
			
			<div class=span12>
				<h3>Stock Rotation</h3>
				
				<%
								
				jhAdmin session("RotationWindowDate")				
								
				if isdate(session("RotationWindowDate")) then 
					RotationWindowDate = formatdatetime(session("RotationWindowDate"),2)
				else
					RotationWindowDate = formatdatetime(now,2)
				end if
				
				session("RotationWindowDate") = RotationWindowDate
								
				FormAction = request.form("FormAction")
				

				
				'Added by EM
				
				if FormAction = "SUBMIT_KSIT" then

					SomeAdded = False

					strKSITCSV = "SKU,Qty,Paid Unit Price,Account,Band,SIN Ref,Status,Clearance Unit Price,Credit Value"
					
					if AcDetails.ParentAcCode <> "" then
						ParentAcCode = AcDetails.ParentAcCode	
						SeparateChildSR = AcDetails.SeparateChildSR					
					else
						ParentAcCode = ""	
					end if
									
					sql = "exec spB2B_GetReturns_LIST '" & AcDetails.AcCode & "', '" & ParentAcCode & "', '', '" & sqlDate(RotationWindowDate) & "', 0" & SeparateChildSR
					
					x = getrs(sql, ReturnsArr, ReturnsC)
					
					RotationList = "<table class=ReportTable border=1>"
					
					RotationList = RotationList & "<tr>"
					RotationList = RotationList & "<th>Name</th>"
					RotationList = RotationList & "<th>SKU</th>"
					RotationList = RotationList & "<th>Qty</th>"
					RotationList = RotationList & "</tr>"
					
					for c = 0 to ReturnsC
						
						ThisSKU = ReturnsArr(3,c)
						ReturnQty = ReturnsArr(17,c)
						SKUName = ReturnsArr(6,c) & ", " & ReturnsArr(4,c) & " - " & ReturnsArr(5,c) 
						
						if ThisSKU <> LastSKU then

							LastSKU = ThisSKU

							
							If CheckIsKSIT(ThisSKU, glKSITList) Then


	Set objcmd = Server.CreateObject("ADODB.Command")
	Set objcmd.ActiveConnection = db
	objcmd.CommandText = "ExchequerProcessSRMatching"
	objcmd.CommandType = adcmdStoredProc

	objcmd.Parameters.append objcmd.createParameter("@ProcessType", adVarChar, adParamInput, 20)
	objcmd.Parameters("@ProcessType") = "GETINVOICES" 
	objcmd.Parameters.append objcmd.createParameter("@AcCode", adVarChar, adParamInput, 10)
	objcmd.Parameters("@AcCode") = AcDetails.AcCode 
	objcmd.Parameters.append objcmd.createParameter("@ParentAcCode", adVarChar, adParamInput, 10)
	objcmd.Parameters("@ParentAcCode") = ParentAcCode 
	objcmd.Parameters.append objcmd.createParameter("@IsSeparateChildSR", adInteger, adParamInput, 4)
	objcmd.Parameters("@IsSeparateChildSR") = "0" & SeparateChildSR  
	objcmd.Parameters.append objcmd.createParameter("@SKU", adVarChar, adParamInput, 20)
	objcmd.Parameters("@SKU") = ThisSKU 
	objcmd.Parameters.append objcmd.createParameter("@SINTranDate", adVarChar, adParamInput, 10)
	objcmd.Parameters("@SINTranDate") = sqlDate(RotationWindowDate) 
	objcmd.Parameters.append objcmd.createParameter("@AllocQty", adInteger, adParamInput, 4)
	objcmd.Parameters("@AllocQty") = 0
	objcmd.Parameters.append objcmd.createParameter("@SINOurRef", adVarChar, adParamInput, 10)
	objcmd.Parameters("@SINOurRef") = "" 

	objcmd.Parameters.append objcmd.createParameter("@ErrorCode", adInteger, adParamOutput, 3)


	Set objrs = Server.CreateObject("ADODB.Recordset")

	Set objrs = objcmd.execute()

	ErrorCode =  objcmd.Parameters("@ErrorCode")


	if ErrorCode = 0 then

			If (NOT objrs.eof) Then 

				LoopRun = True

							RotationList = RotationList & "<tr>"
						
							
							RotationList = RotationList & "<td>" & SKUName & "</td>" 
							RotationList = RotationList & "<td><strong>" & ThisSKU & "</strong></td>" 
							RotationList = RotationList & "<td>" & ReturnQty & "</td>" 
							
							RotationList = RotationList & "</tr>"
	
				While not objrs.eof
					If LoopRun Then 


					                    intInvQty = objrs.Fields("quantity").Value
			                                    strSINOurRef = objrs.Fields("sinourref").Value
                        			            strFullSKU = objrs.Fields("fullsku").Value
                        			            strMinUnitValue = Round(objrs.Fields("MinUnitValue").Value, 2)

						            if (intInvQty >= ReturnQty) Then 
						                intAllocQty = ReturnQty
						            else
						                intAllocQty = intInvQty
								End If


							ThisKSITStatus = "Accepted"
							
							SomeAdded = true

                               				strKSITCSV = strKSITCSV & vbcrlf & strFullSKU & "," & intAllocQty  & "," & strMinUnitValue  & "," & AcDetails.AcCode &  "," & AcDetails.CostBand & "," & strSINOurRef & "," & ThisKSITStatus  & ",,"



        
	Set objcmd1 = Server.CreateObject("ADODB.Command")
	Set objcmd1.ActiveConnection = db
	objcmd1.CommandText = "ExchequerProcessSRMatching"
	objcmd1.CommandType = adcmdStoredProc

	objcmd1.Parameters.append objcmd1.createParameter("@ProcessType", adVarChar, adParamInput, 20)
	objcmd1.Parameters("@ProcessType") = "SETMATCHING" 
	objcmd1.Parameters.append objcmd1.createParameter("@AcCode", adVarChar, adParamInput, 10)
	objcmd1.Parameters("@AcCode") = "" 
	objcmd1.Parameters.append objcmd1.createParameter("@ParentAcCode", adVarChar, adParamInput, 10)
	objcmd1.Parameters("@ParentAcCode") = "" 
	objcmd1.Parameters.append objcmd1.createParameter("@IsSeparateChildSR", adInteger, adParamInput, 4)
	objcmd1.Parameters("@IsSeparateChildSR") = 0  
	objcmd1.Parameters.append objcmd1.createParameter("@SKU", adVarChar, adParamInput, 20)
	objcmd1.Parameters("@SKU") = strFullSKU 
	objcmd1.Parameters.append objcmd1.createParameter("@SINTranDate", adVarChar, adParamInput, 10)
	objcmd1.Parameters("@SINTranDate") = "19000101" 
	objcmd1.Parameters.append objcmd1.createParameter("@AllocQty", adInteger, adParamInput, 4)
	objcmd1.Parameters("@AllocQty") = intAllocQty
	objcmd1.Parameters.append objcmd1.createParameter("@SINOurRef", adVarChar, adParamInput, 10)
	objcmd1.Parameters("@SINOurRef") = strSINOurRef 

	objcmd1.Parameters.append objcmd1.createParameter("@ErrorCode", adInteger, adParamOutput, 3)


	objcmd1.execute()

	Set objcmd1 =  Nothing

			                                    ReturnQty = ReturnQty - intAllocQty

		
                			                    if (ReturnQty <= 0) Then LoopRun = False

					End If
					objrs.MoveNext

				Wend

			End If
	End If
				objrs.Close
				set objrs = Nothing

				Set objcmd = Nothing


							
							end if
						End If
					next
					
					RotationList = RotationList & "</table>"
					
					
					CurrentActiveStatus = 1
					
					if SomeAdded then

						strKSITFileName =  "E:\DATA\SKU\KSIT_" & AcDetails.AcCode & ".csv"
						 set KSITfs = Server.CreateObject("Scripting.FileSystemObject")
						 set KSITf = KSITfs.OpenTextFile(strKSITFileName,2,true)
						 KSITf.Write(strKSITCSV)
						 KSITf.Close
						 set KSITfs = Nothing
						 set KSITf = Nothing

							SendSubject = "KSIT Proposal Endura Account " & AcDetails.AcCode 

				                EmailSent = SendMailWithAttachment(SendSubject, "", "david.montgomery@endura.co.uk", "returns@endura.co.uk", true, strKSITFileName)
						
						sql = ""

						sql = "DELETE FROM B2B_ReturnItems WHERE AcCode = '" & AcDetails.AcCode  & "' " 
						db.execute(sql)
						
						
						jhInfoNice "", lne("Your KSIT request has been submitted")
								
						ReturnSubmitted = true
						%>
						
						<a class=OrangeLink href="/"><%=lne("label_home_page_link")%></a>
						
						<hr>
						
						<h5>You have submitted the following for KSIT <a href="#" onclick="window.print();"><i class="icon icon-print"></i></a> </h5>
						<%
						=RotationList
						%>
					
					<%
					else
						
						
						
					end if
				end if

				'
				


				if FormAction = "SUBMIT_RETURN" then
				
					'jhInfo "Draw output here..."
					
					if AcDetails.ParentAcCode <> "" then
						ParentAcCode = AcDetails.ParentAcCode	
						SeparateChildSR = AcDetails.SeparateChildSR					
					else
						ParentAcCode = ""	
					end if
									
					sql = "exec spB2B_GetReturns_LIST '" & AcDetails.AcCode & "', '" & ParentAcCode & "', '', '" & sqlDate(RotationWindowDate) & "', 0" & SeparateChildSR
								
					x = getrs(sql, ReturnsArr, ReturnsC)
					
					if isAdminUser() then ThisSource = "Admin" else ThisSource = "Customer"
					
					JobXML = "<Transaction TransDate=""" & ExDate(now) & """ AcCode=""" & AcDetails.AcCode & """ YourRef="""" Source=""" & ThisSource & """>"	
					
					RotationList = "<table class=ReportTable border=1>"
					
					RotationList = RotationList & "<tr>"
					RotationList = RotationList & "<th>Name</th>"
					RotationList = RotationList & "<th>SKU</th>"
					RotationList = RotationList & "<th>Qty</th>"
					RotationList = RotationList & "</tr>"
					
					for c = 0 to ReturnsC
						
						ThisSKU = ReturnsArr(3,c)
						
						if ThisSKU <> LastSKU then
							
							RotationList = RotationList & "<tr>"
						
							ReturnQty = ReturnsArr(17,c)
							SKUName = ReturnsArr(6,c) & ", " & ReturnsArr(4,c) & " - " & ReturnsArr(5,c) 
							
							jhAdmin ReturnQty & " x " & SKUName
							
							LastSKU = ThisSKU
							
							LineXML = "<TLine StockCode=""" & ThisSKU & """ Qty=""" & ReturnQty & """ Status=""Accepted"" />"
							JobXML = JobXML & LineXML 
							
							RotationList = RotationList & "<td>" & SKUName & "</td>" 
							RotationList = RotationList & "<td><strong>" & ThisSKU & "</strong></td>" 
							RotationList = RotationList & "<td>" & ReturnQty & "</td>" 
							
							RotationList = RotationList & "</tr>"
							
							SomeAdded = true
							
						end if
					next
					
					RotationList = RotationList & "</table>"
					
					
					JobXML = JobXML & "</Transaction>"
					
					if isjh() then jhXML(JobXML)
					
					CurrentActiveStatus = 1
					
					if SomeAdded then
						
						sql = "INSERT INTO ExchequerQueue.dbo.ExchequerQueue (Name, Contents, RunType, IsFullVersion, CurrentActiveStatus) VALUES ('" & glCreateRotationJobName & "', '" & validstr(JobXML) & "', 'IMMEDIATE', 0, " & CurrentActiveStatus & ")"
						db.execute(sql)
						
						sql = ""

						sql = "DELETE FROM B2B_ReturnItems WHERE AcCode = '" & AcDetails.AcCode  & "' " 
						jhAdmin sql
						db.execute(sql)
						
						
						AcknowledgementStr = lne("rotation_thank_you")
						
						jhInfoNice "", AcknowledgementStr
								
						ReturnSubmitted = true
								
						%>
						
						<a class=OrangeLink href="/"><%=lne("label_home_page_link")%></a>
						
						<hr>
						
						<h5>You have submitted the following for rotation <a href="#" onclick="window.print();"><i class="icon icon-print"></i></a> </h5>
						<%
						=RotationList
						%>
					
					<%
					else
						
						
						
					end if
				end if

				
				if not ReturnSubmitted then
					jhErrorNice "No items found", "No rotation request items were submitted, you may have landed on this page in error <a href=""/"">- click here to return to the home page</a>"
					
				end if
%>
				
			</div>
			
			
		</div>
		
	


	</div>
	<div class=spacer40></div>			
			

<!--#include virtual="/assets/includes/footer.asp" -->


<%

dbClose()

%>

