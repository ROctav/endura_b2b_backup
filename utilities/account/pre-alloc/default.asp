<%
PageTitle = "Pending Stock Pre-Allocations"

Response.ContentType = "text/html"
Response.AddHeader "Content-Type", "text/html;charset=ISO-8859-1"
'Response.CodePage = 65001
Response.CharSet = "ISO-8859-1"

Response.Expires = 0
Response.Expiresabsolute = Now() - 1442
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"

%>

<!--#include virtual="/assets/includes/header.asp" -->

<%


%>

<style>
	.ReportTable TD {
		padding:5px;
	}
</style>

<link rel="stylesheet" href="css/index.css" type="text/css" media="screen" />

	<div class="container">      

		<div class="row">      
			<div class="span12 spacer10">
		
					<h3 class=spacer20><%=lne("label_pending_pre_alloc")%></h3>
					
					
					
					
<%

function getSKUTranslation(byval pSKU, byval pLanguageID, byref rName, byref rColor, byRef rSize)

	sql = "exec spB2BTranslateSKU '" & validstr(pSKU) & "', 0" & cleannum(pLanguageID)
	set rs = db.execute(sql)
	if not rs.eof then
		rName = "" & rs("TransProductName")	
		rColor = "" & rs("TransColorName")	
		rSize = "" & rs("SizeName")		
		if rName = "" then rName = "" & rs("ProductName")	
		if rColor = "" then rColor = "" & rs("ColorName")			
		getSKUTranslation = true
	else
		getSKUTranslation = false
	end if
	rsClose()

end function

	
	set ls = new strconcatstream

	ThisUserID = AcDetails.AcCode
	
	dbConnect()
	
	FormAction = request.form("FormAction")
		
	if FormAction = "SUBMIT_REQUEST" then

		'jhInfo "SUBMIT AMENDMENT"
		
		RecCount = cdbl(request("RecCount"))
		
		AutoConfirmCheck = request("AutoConfirmCheck")
		if AutoConfirmCheck <> "" then 
			AutoConfirmCheck = "1" 
			AutoConfirmXML = "YES" 
		else 
			AutoConfirmCheck = "0"
			AutoConfirmXML = "NO" 
		end if
			
		'jhInfo "XML"
		
		Set xmlDoc = server.CreateObject("Microsoft.XMLDOM")
		xmlDoc.LoadXml "<BOStockAllocate/>"
						
		xmlDoc.DocumentElement.SetAttribute "AcCode", AcDetails.AcCode
		xmlDoc.DocumentElement.SetAttribute "IsAutoConfirm", AutoConfirmXML

		AllZero = true
		SomeChanged = false
		ProcessCount = 0
		
		ErrorsFound = false
		
		for c = 1 to RecCount 
		
			ThisSKU = request("SKU_" & c)
			ThisOrigQty = convertNum(request("OrigQty_" & c))
			ThisLineNo = request("LineNo_" & c)
			ThisOurRef = request("OurRef_" & c)
			ThisNewQty = convertNum(request("NewQty_" & c))
			
			'jhAdmin "ThisNewQty = " & ThisNewQty
			
			if not isNumber(request("NewQty_" & c)) then
				ErrorsFound = true
				exit for
			end if			
			
			if ThisNewQty < 0 then 
				ThisNewQty = 0
			elseif ThisNewQty > 0 then
				AllZero = false
			end if
				
			if ThisOrigQty <> ThisNewQty then
				SomeChanged = true
			end if

			set elmNew = xmlDoc.CreateElement("Line")
			elmNew.SetAttribute "SKU", ThisSKU
			elmNew.SetAttribute "SOROurRef", ThisOurRef
			elmNew.SetAttribute "AbsLineNo", ThisLineNo
			elmNew.SetAttribute "Qty", ThisNewQty 
			xmlDoc.DocumentElement.AppendChild elmNew		
			
			ProcessCount = ProcessCount + 1
			
		next		
		
		if AllZero then
			CommandType = "CANCEL"
		elseif SomeChanged then
			CommandType = "AMEND"
		else
			CommandType = "CONFIRM"
		end if
		
		xmlDoc.DocumentElement.SetAttribute "CommandType", CommandType
		xmlDoc.DocumentElement.SetAttribute "NewB2B", "1"

		XMLContents	 = xmlDoc.XML
		
		'jh "END XML!"	
		
		if isjh() then
		
			jhError "JH DO NOT PROCESS!"
		
			jhXML XMLContents 
		
		elseif not ErrorsFound then
			
			sql = "UPDATE END_CRMContacts SET IsBOPAAutoConfirm = 0" & AutoConfirmCheck & "  WHERE ClientID = '" & AcDetails.AcCode & "' AND ContactFirstName = 'IT System Profile'"
			jhAdmin sql
			db.execute(sql)
				
			sql = "UPDATE ExchequerAllocStockArrival SET IsConfirmed = 1  WHERE IsConfirmed = 0 AND AcCode = '" & AcDetails.AcCode & "'"
			jhAdmin sql
			db.execute(sql)
			
			jhXML xmlDoc.XML
			'jhXML XMLContents
			
			call InsertJob("BOStockAllocate", XMLContents, "0")
			

			RequestSubmitted = true	
		else
			
			jhErrorNice lne("label_errors_found"), "label_pre_alloc_error_check_qtys"
			
		end if
		
		'jhstop	
				
		set xmlDoc = nothing	
				
	end if	
		
	
	'jhAdmin "ShowPage = " & ShowPage
	
	if AmendmentSubmitted then
		
		MessageTitle = lne("pre_alloc_submitted_title")
		MessageExplained = lne("pre_alloc_submitted_explained")
		
		jhInfoNice MessageTitle, MessageExplained & "<div class=spacer20></div><a href=""/"" class=""OrangeLink"">" & lne("label_home_page_link") & "</a>"
						
	else
	



sql = "SELECT sa.AcCode AS idxAcCode, sa.SKU AS stStockCode, CONVERT(datetime, bo.tlTransDate, 112) AS thTransDate, bo.tlDescr AS stDesc1, t.thYourRef AS thYourRef, (bo.tlQty_1 - bo.tlQtyDel_1 - bo.tlQtyWOff_1) AS OrderQty, sa.SOROurRef AS thOurRef, sa.ABSLineNo "
sql = sql & "FROM ExchequerAllocStockArrival sa "
sql = sql & "INNER JOIN transactionlines bo ON bo.tlOurRef = sa.SOROurRef and bo.tlStockCode = sa.SKU and bo.tlLineNo = sa.ABSLineNo  "
sql = sql & "INNER JOIN transactions t ON t.thFolio = bo.tlFolio and t.thOurRef LIKE 'SOR%' and t.thRunNo = -40 "
sql = sql & "WHERE sa.AcCode = '" & AcDetails.AcCode & "' AND sa.isConfirmed = 0 "
		
		
		x = getrs(sql, RecArr, rc)
		
		if rc = -1 then	
			os = "<p>" & lne("pre_alloc_no_pre_alloc") & "</p>"
		end if
		
		ShowFO = true
		
		if rc >= 0 then
			ls.add "<table border=1 cellpadding=8 cellspacing=0 class=""ReportTable"" >"
			
			ls.add "<tr class=TableHead>"
						
			ls.add "<th>" & lne("label_sku") & "</th>"
			ls.add "<th>" & lne("label_product_name") & "</th>"
			ls.add "<th>" & lne("label_color") & "</th>"
			ls.add "<th>" & lne("label_size") & "</th>"
			ls.add "<th>" & lne("label_our_ref") & "</th>"
			ls.add "<th>" & lne("label_your_ref") & "</th>"
			ls.add "<th>" & lne("label_ordered_qty") & "</th>"
			ls.add "<th>" & lne("label_new_qty") & " <a id=""CancelAllLink"" title=""" & lne("label_cancel_all") & """ class=""pointer""><i class=""icon icon-trash""></i></a></th>"		
			ls.add "</tr>"
		
			ShowCount = 0
		
			for c = 0 to rc
			
				
				ThisSKU = RecArr(1,c)
				ThisDesc = RecArr(3,c)
				ThisOurRef = RecArr(6,c)
				ThisYourRef = RecArr(4,c)
				
				SKUTrans = getSKUTranslation(ThisSKU, AcDetails.SelectedLanguageID, ThisProductName, ThisColor, ThisSize)
				
				ThisQty = RecArr(5,c)
				ThisLineNo = RecArr(7,c)
			
					if trim("" & ThisYourRef) = "" then ThisYourRef = "-"

					ShowThis = true
					if ShowThis then
						ShowCount = ShowCount + 1
					
						PreAllocFound = true
			
						ls.add "<tr class=legible style=""background-color:"
						ls.add swapBG()
						ls.add """ id=""TR" & ShowCount &  "_" & ThisStatus  & """>"
			
						ls.add "<td class=RepSmall style=""text-align:left;font-weight:bold;"" valign=top id=""SKU" & ShowCount &  """>"
						ls.add ThisSKU
						ls.add "</td>"
						ls.add "<td class=RepSmall valign=top>"
						ls.add ThisProductName
						ls.add "</td>"
						ls.add "<td class=RepSmall valign=top>"
						ls.add ThisColor
						ls.add "</td>"
						ls.add "<td class=RepSmall valign=top>"
						ls.add ThisSize
						ls.add "</td>"						
			
						ls.add "<td class=RepSmall valign=top id=""SOR" & ShowCount &  """>"
						ls.add ThisOurRef
						ls.add "</td>"
						ls.add "<td class=RepSmall valign=top>"
						ls.add ThisYourRef
						ls.add "</td>"

						ls.add "<td class=""RepSmall numeric"" valign=top><strong>"
						ls.add ThisQty
						ls.add "</td>"				

												
						ls.add "<td class=RepSmall align=""center"" style=""font-weight:bold;font-size:12pt;"" valign=top name=""Qty" & ShowCount &  """ id=""Qty" & ShowCount &  """>"

						ls.add "<input data-orig-qty=""" & ThisQty & """ " &  CheckDisStr & " type=""text"" value="""
						ls.add ThisQty
						ls.add """ class=""PreAllocNewQty"" style=""width: 30px;"" name=""NewQty_" & ShowCount & """>"
						
						ls.add "<input class=FormHidden type=""hidden"" name=""OrigQty_" & ShowCount & """ value=""" & ThisQty & """ id=""OrigQty_" & ShowCount & """>"
						ls.add "<input class=FormHidden type=""hidden"" name=""SKU_" & ShowCount & """ value=""" & ThisSKU & """ id=""SKU_" & ShowCount & """>"
						ls.add "<input class=FormHidden type=""hidden"" name=""OurRef_" & ShowCount & """ value=""" & ThisOurRef & """ id=""OurRef_" & ShowCount & """>"
						ls.add "<input class=FormHidden type=""hidden"" name=""LineNo_" & ShowCount & """ value=""" & ThisLineNo & """ id=""LineNo_" & ShowCount & """>"
						
						ls.add "</td>"			
						

						ls.add "</tr>"
					
					end if
			next
			
			ls.add "</table>"
		
			os = ls.value
						
		end if
		
	

	
	
%>	
	
			
								
<%
			if RequestSubmitted then
			
				jhInfoNice lne("label_thank_you"), lne("egor_BOEmailConfirmText")
			%>
				
			<%
			
			elseif PreAllocFound then
			
				if FormAction = "" then
					sql = "SELECT IsBOPAAutoConfirm FROM END_CRMContacts WHERE ClientID = '" & AcDetails.AcCode & "' AND ContactFirstName = 'IT System Profile' "
					set rs = db.execute(sql)
					if not rs.eof then
						IsBOPAAutoConfirm = rs("IsBOPAAutoConfirm")
						jhAdmin "IsBOPAAutoConfirm = " & IsBOPAAutoConfirm
					end if
					rsClose()
				end if
			
				DispArrivaldate = ""
				sql = "SELECT TOP 1 * FROM ExchequerBatchFileStockArrival WHERE IsAllocated = 0 ORDER by ArrivalDate DESC "
				set rs = db.execute(sql)
				if not rs.eof then
					Arrivaldate = rs("Arrivaldate")
				end if
				if isDate(Arrivaldate) then DispArrivaldate = formatdatetime(Arrivaldate, 2) else DispArrivaldate = "[unknown]"
				rsClose()
%>
				<div class="spacer20">	
				<%
				=lne("egor_BOEmailListCaption")
				%>	
				</div>
				
				<form name="Form1" method=post action="/utilities/account/pre-alloc/">
					<div class="spacer20">	
						
						<span class="label label-success"><%=lne("pre_alloc_anticipated_despatch")%>: <%=DispArrivaldate%></span>
					</div>

					<div class=alert>
						<%
						=lne("egor_BOEmailListCaption2")
						%>		
					</div>

								
					<div class="spacer20">				
						<%
						=os
						%>
					</div>	
			
			
			<%
			ConfirmStr = lne("label_are_you_sure")
			%>
				
					<div class="text-left spacer20">											
						<button onclick="if (!confirm('<%=ConfirmStr%>')) { return false; } else { xxthis.form.submit(); }" id=btnConfirmChanges class="btn btn-success"><%=lne("btn_pre_alloc_confirm_changes")%></button>
					</div>				
				
					<div class="text-left spacer20">
						<input <%if IsBOPAAutoConfirm then response.write "CHECKED"%> type=checkbox name="AutoConfirmCheck" id="AutoConfirmCheck">&nbsp;<label for="AutoConfirmCheck"><%=lne("pre_alloc_auto_confirm")%></label>
					</div>
					
					<input type=hidden name="RecCount" class="FormHidden" value="<%=ShowCount%>">
					<input type=hidden name="FormAction" class="FormHidden" value="SUBMIT_REQUEST">
									
				</form>
		
			
				
				
		
<%
			else
%>
				<div class="spacer20">				
					<%
					=os
					%>
				</div>	
<%
			end if
	
%>
			
<%	
	
end if 
	%>			
				
			</div>
		</div>		
	</div>		



<script>
	$(document).ready(function() {
		
		//select and deselect
		$("#btnCheckAll").click(function () {
			$('.RemoveCheck').prop('checked', true);
			//alert('clicked')
			$('.QuickSearchSKUQty').val('0');
			$('.QuickSearchSKUQty').prop('disabled', true);
			
		});

		$("#CancelAllLink").click( function() {
		
			//$('.PreAllocNewQty').val(0);
		
			$('.PreAllocNewQty').each( function () {
				
				if ($( this ).val() == '0') {
					$( this ).val($( this ).data('orig-qty'));
				} else if ($( this ).val() == $( this ).data('orig-qty')) {
					$( this ).val(0);
				}
			
			});
		
		});
			
			
	});	
</script>

<script type="text/javascript">
	//var strAmendXML = "";


</script>

<!--#include virtual="/assets/includes/footer.asp" -->