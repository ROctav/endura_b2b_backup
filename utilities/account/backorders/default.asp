<%
PageTitle = "Backorders"

Response.ContentType = "text/html"
Response.AddHeader "Content-Type", "text/html;charset=ISO-8859-1"
'Response.CodePage = 65001
Response.CharSet = "ISO-8859-1"

Response.Expires = 0
Response.Expiresabsolute = Now() - 1442
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"

%>

<!--#include virtual="/assets/includes/header.asp" -->

<%


%>

<style>
	.ReportTable TD {
		padding:5px;
	}
</style>

<link rel="stylesheet" href="css/index.css" type="text/css" media="screen" />

	<div class="container">      

		<div class="row">      
			<div class="span12 spacer10">
		
					<h3 class=spacer20><%=lne("label_backorder_amendment")%></h3>
						
					
					
<%
	MaxRecs = 5000
	set ls = new strconcatstream

	ThisUserID = AcDetails.AcCode
	
	dbConnect()
	
	FormAction = request.form("FormAction")
	
    ShowPage = true
	  
	if (ShowPage)   Then
	    ShowPage = CheckDataLoad(ThisUserID, "AmendBackOrders")
	end if		
	

	if FormAction = "BACKORDER_AMEND" and ShowPage then

		ErrorsFound = false
	
		'jhInfo "SUBMIT AMENDMENT"
		
		RecCount = cdbl(request("RecCount"))
		
		LineXML = ""
		ProcessCount = 0
		
		for c = 0 to RecCount 
		
			ThisSKU = request("SKU_" & c)
			
			OrigQty = cleannum(request("OrigQty_" & c))
			SOROurRef = request("sfieldhSOROurRef" & c)
            ABSLineNo = request("sfieldhABSLineNo" & c)
			NewQty = trim(request("sfield" & c))
			
			jhAdmin "ThisSKU = " & ThisSKU
			jhAdmin "OrigQty = " & OrigQty
			jhAdmin "NewQty = " & NewQty
			
			if ThisSKU <> "" and OrigQty <> "" then
				ProcessThis = false
				if NewQty = "" then
					NewQty = 0
					ProcessThis = true
				elseif cleannum(NewQty) <> "" and "" & NewQty <> "" & OrigQty then
					SetQty = NewQty
					ProcessThis = true				
				end if
				
				'jh "ProcessThis = " & ProcessThis
			
				if ProcessThis then
					
					'jhInfo "SET " & ThisSKU & " FROM " & OrigQty & " TO " & NewQty
					ProcessCount = ProcessCount + 1
				
                    LineXML = LineXML & "<SKU Qty=""" & NewQty & """ SOROurRef=""" & SOROurRef & """ ABSLineNo=""" & ABSLineNo & """ />"

				end if
				

			end if
			
			
		next

		if ErrorsFound then
			jhError "ERROR!"
		elseif ProcessCount > 0 then
			
			JobXML = "<Transaction TransDate=""" & ExDate(now) & """ AcCode=""" & AcDetails.AcCode & """ NewB2B=""1"">"	

			JobXML = JobXML & LineXML
			
			JobXML = JobXML & "</Transaction>"
			
			if isjh() then CurrentActiveStatus = 0 else CurrentActiveStatus = 1
			
			sql = "INSERT INTO ExchequerQueue.dbo.ExchequerQueue (Name, Contents, RunType, IsFullVersion, CurrentActiveStatus) VALUES ('AmendBackOrders', '" & validstr(JobXML) & "', 'IMMEDIATE', 0, " & CurrentActiveStatus & ")"
			
			jhAdmin jhHTML(sql)	

			if isjh() then 
				jhstop
			else
				db.execute(sql)
			end if
			
			sql = ""
			
			AmendmentSubmitted = true
			
		end if	
		
		'jh "ENDS"

	end if	
		
	
	'jhAdmin "ShowPage = " & ShowPage
	
	if AmendmentSubmitted then
		
		MessageTitle = lne("backorders_submitted_title")
		MessageExplained = lne("backorders_submitted_explained")
		
		jhInfoNice MessageTitle, MessageExplained & "<div class=spacer20></div><a href=""/"" class=""OrangeLink"">" & lne("label_home_page_link") & "</a>"
				
	elseif not ShowPage then
			
		InProgressTitle = lne("backorders_refresh_in_progress_title")
		InProgressExplained = lne("backorders_refresh_in_progress_explained")
		InProgressReload = lne("backorders_refresh_in_progress_reload")
			
		Response.Write "<script type=""text/javascript"">var t = setTimeout(""self.location.replace('/utilities/account/backorders/')"",30000)</script>"
		jhNice InProgressTitle, InProgressExplained & "<div class=spacer20></div><a href=""/utilities/account/backorders/"" id=btnRefresh class=""btn btn-primary btn-mini"">" & InProgressReload & "</a>"
		
	else

        'DEFAULT
        BackOrderSortOrder = "DUE"

        BackOrderSortOrder = request.querystring("BackOrderSortOrder")

        sql  = "exec spB2BBackOrders @AcCode = '" & AcDetails.AcCode & "', @SortOrder = '" & BackOrderSortOrder & "'" 	

        x = getrs(sql, RecArr, rc)
			
        if rc = -1 then	
        os = "<p>" & lne("backorders_no_backorders") & "</p>"
        end if
		
		ShowFO = true
		
        if rc >= 0 then
        ls.add "<table border=1 cellpadding=8 cellspacing=0 class=""ReportTable"" >"
			
        ls.add "<tr class=TableHead>"
			
        ls.add "<th>#</th>"
        ls.add "<th>" & lne("label_sku") & "</th>"
        ls.add "<th>" & lne("label_checkout_backorders_description") & "</th>"
        ls.add "<th>" & lne("label_our_ref") & "</th>"
        ls.add "<th>" & lne("label_your_ref") & "</th>"
        ls.add "<th>" & lne("bo_label_orderdate") & "</th>"
        ls.add "<th>" & lne("bo_label_os") & "</th>"
        ls.add "<th>" & lne("bo_header_2") & "</th>"
        ls.add "<th nowrap>" & lne("bo_label_amend") & "</th>"
        'ls.add "<th>" & GetLabel("Stock") & "</th>"
        ls.add "<th>" & lne("bo_label_cancel") & "</th>"
        ls.add "<th>" & lne("bo_label_amendable") & "</th>"
        ls.add "<th>" & lne("bo_header_1") & "</th>"
        ls.add "<th>" & lne("bo_header_3") & "</th>"
        ls.add "</tr>"
		
        ShowCount = 0
		
        for c = 0 to rc

	ThisBGColor = "white"
			
            oq = RecArr(8,c)

            dq = RecArr(9,c)
            ThisWO = RecArr(11,c)	
				
            QtyOut =  RecArr(10,c)	
				
			
                if ShowCount > MaxRecs then exit for
			
                ThisCode = RecArr(1,c)
                thiscolor = RecArr(1,c)
					
                ThisQty = RecArr(8,c)				
                thisDel = RecArr(9,c)		
                ThisOS = RecArr(10,c)		
					
                thisDesc = RecArr(4,c)
                ThisStatus = RecArr(17,c)
									
                ThisIsPA = RecArr(21,c)
                ThisEDD = RecArr(25,c)
                ThisIsMultipleDel = CStr("" & RecArr(26,c))

			
                ThisPickedQty = RecArr(27,c)
                ThisTagNo = CInt(RecArr(28,c))


							If NOT IsNull(RecArr(29,c)) Then

						                ThisFSR = CBool(RecArr(29,c))

								If (ThisFSR) Then

									ThisBGColor = "lightblue"
								End If	

							End If

		strEDDValue = ""

		If (Len(ThisEDD) > 0) Then 

                	dtEDDDate = CDate(ThisEDD) 

			if (ThisPickedQty = 0) Then 
				strEDDValue = 	formatdatetime(dtEDDDate,1)
			else
				if ((ThisPickedQty * 100) / ThisQty) < 50 Then
					strEDDValue = 	formatdatetime(dtEDDDate,1) 
				else
					if (ThisStatus = "SCANNER" OR ThisTagNo = 15 OR ThisTagNo = 16 OR ThisTagNo = 17 OR ThisTagNo = 18 OR ThisTagNo = 20 OR  ThisTagNo = 21 OR ThisTagNo = 22 OR ThisTagNo = 40) Then
						strEDDValue = lne("bo_label_asap")						
					else
						strEDDValue = 	lne("label_delivery_TBC") 
					end if	
				end if				
			end if			

		Else  
                    	if (ThisPickedQty = 0) Then 
				strEDDValue = lne("label_delivery_TBC") 
			else
				if ((ThisPickedQty * 100) / ThisQty) < 50 Then
					strEDDValue = lne("label_delivery_TBC") 						
				else
					if (ThisStatus = "SCANNER" OR ThisTagNo = 15 OR ThisTagNo = 16 OR ThisTagNo = 17 OR ThisTagNo = 18 OR ThisTagNo = 20 OR  ThisTagNo = 21 OR ThisTagNo = 22 OR ThisTagNo = 40) Then
						strEDDValue = lne("bo_label_asap")						
					else
						strEDDValue = 	lne("label_delivery_TBC")  
					end if	
				end if				
			end if			


		End If



                strMultipleDelValue = ""

			If ThisIsMultipleDel = "" Then

				If (ThisQty - ThisPickedQty = 0) Then
					strMultipleDelValue = "No"
				Else 
					strMultipleDelValue =  lne("bo_label_na")
				End If 
			Else
				If (ThisQty - ThisPickedQty = 0) Then
					strMultipleDelValue = "No"
				Else 

					If (ThisIsMultipleDel = "True" OR ThisPickedQty > 0) Then
						strMultipleDelValue = "Yes"
					Else
						strMultipleDelValue = "No"
					End If
				End If	
			End If




                ThisAbsLineNo = RecArr(22,c)

                ThisAmendStatus = RecArr(18,c)

                If (ThisAmendStatus = "0")  Then
                    ThisAmend = False
                Else
						
                    ThisAmend = True
                End If				

                ThisUnID = RecArr(19,c)
                ThisLineStatus = RecArr(20,c)
				
                OurRef = RecArr(12,c)
                YourRef = cstr("" & RecArr(6,c))
					
                DuffCode1 = chr(0)
					
                ThisDue = RecArr(3,c)				
                ThisDue = FromExDate(ThisDue)
					
                If (DateValue(ThisDue) > DateValue(Now)) Then
                    ThisStatus = "FORWARD"
                End If

                DispDate = formatdatetime(ThisDue,1)
					
                ThisDate = FromExDate(RecArr(3,c))
                DispDate = formatdatetime(ThisDate,1)

                if instr(1, YourRef, DuffCode1) > 0 then
                    YourRef = "-"
                end if
					
                if trim("" & YourRef) = "" then YourRef = "-"

                ShowThis = true
                if ShowThis then
                    ShowCount = ShowCount + 1
					
                    BOFound = true
			
                    ls.add "<tr class=legible style=""background-color: " & ThisBGColor &  ";"""
                    ls.add " id=""TR" & ShowCount &  "_" & ThisStatus  & """>"
			
                    ls.add "<td class=RepSmall valign=top>"
                    ls.add ShowCount
                    ls.add "</td>"
			
                    ls.add "<td class=RepSmall style=""text-align:left;font-weight:bold;"" valign=top id=""SKU" & ShowCount &  """>"
                    ls.add ThisCode
                    ls.add "</td>"
                    ls.add "<td class=RepSmall valign=top>"
                    ls.add thisDesc
                    ls.add "</td>"
			
                    ls.add "<td class=RepSmall valign=top id=""SOR" & ShowCount &  """>"
                    ls.add OurRef
                    ls.add "</td>"
                    ls.add "<td class=RepSmall valign=top>"
                    ls.add YourRef
                    ls.add "</td>"
				
                    ls.add "<td class=RepSmall valign=top>"
                    ls.add DispDate
                    ls.add "</td>"

                    ls.add "<td align=center class=RepSmall valign=top>"
                    ls.add ThisQty
                    ls.add "</td>"				

                    ls.add "<td align=center class=RepSmall valign=top>"
                    ls.add ThisPickedQty
                    ls.add "</td>"				
												
                    ls.add "<td class=RepSmall align=""center"" style=""font-weight:bold;font-size:12pt;"" valign=top name=""Qty" & ShowCount &  """ id=""Qty" & ShowCount &  """>"
                    If ThisAmend Then
                        CheckDisStr = ""
                            ls.add "<input " &  CheckDisStr & " type=""text"" value="""
                            ls.add ThisOS
                            ls.add """ class=""QuickSearchSKUQty"" style=""width: 30px;"" name=""sfield" & CStr(ShowCount) & """"

                            ls.add ">"
                            ls.add "<input Class=FormHidden " &  CheckDisStr & " type=""hidden"" name=""sfieldhSOROurRef" & CStr(ShowCount) & """ value=""" & CStr(OurRef) & """ >"
                            ls.add "<input Class=FormHidden " &  CheckDisStr & " type=""hidden"" name=""sfieldhABSLineNo" & CStr(ShowCount) & """ value=""" & CStr(ThisAbsLineNo) & """ >"
                            ls.add "<input class=FormHidden type=""hidden"" name=""OrigQty_" & CStr(ShowCount) & """ value=""" & ThisOS & """ id=""OrigQty_" & CStr(ShowCount) & """>"
                            ls.add "<input class=FormHidden type=""hidden"" name=""SKU_" & CStr(ShowCount) & """ value=""" & ThisCode & """ id=""SKU_" & CStr(ShowCount) & """>"
                    Else
                        CheckDisStr = "DISABLED"  								

                        ls.add "<span class=RepSmall>" 
                        ls.add "</span>"
                    End If

                    ls.add "</td>"			

                    ls.add "<td class=RepSmall align=center>"
                    If ThisAmend Then
                        
                            ls.add "<input class=RemoveCheck type=""checkbox"" fieldid=""" & CStr(ShowCount)  & """"
                            ls.add " onclick=""CancelLine(this, " & CStr(ShowCount)  & ");"">"
                        
                    End If
                    ls.add "</td>"				


                    ls.add "<td class=RepSmall valign=top align=center>"
                    If (ThisAmend) Then
                        If ThisTagNo = 36 Then
                            ls.add "Yes (Wait for All Stock)"
                        ElseIf (ThisLineStatus = "PICKED") Then
                            ls.add "Yes (Product Allocated)"
			Else
                            ls.add "Yes"
                        End If
                    Else
                        If (ThisStatus = "SCANNER") Then
                            ls.add "No - Order Being Despatched"
                        ElseIf (ThisStatus = "FORWARD") Then
                            ls.add "No - Forward Order"
                        ElseIf (Len(ThisIsPA) > 0 ) Then
                            ls.add "No - Product Pre-Allocated"
                        Else
                            ls.add "No - Admin Process"
                        End If
                    End If
                    ls.add "</td>"				


                    ls.add "<td class=RepSmall align=center>"

                    ls.add strEDDValue				
		

                    ls.add "</td>"				
 	
                    ls.add "<td class=RepSmall align=center>"
                    ls.add strMultipleDelValue				

                    ls.add "</td>"				

    

                    ls.add "</tr>"
					
                end if
        next
			
        ls.add "</table>"
		
        os = ls.value
						
    end if
        
	
%>	

            <div class="pull-left small legible">
				<%=lne("label_sort_sort_by")%> <select onchange="setBackOrderSortOrder(this.value)" id="BackOrderSortOrder" style="width:90px;" class=small>
					<option <%if BackOrderSortOrder = "DUE" then response.write "SELECTED"%> value="DUE">Order Date</option>
					<option <%if BackOrderSortOrder = "SKU" then response.write "SELECTED"%> value="SKU">SKU</option>
                    <option <%if BackOrderSortOrder = "REF" then response.write "SELECTED"%> value="REF">Your Ref</option>						
				</select>
			</div>
			
			<div class="text-right spacer20">
                
				<button id=btnCheckAll class="btn btn-danger btn-mini"><%=lne("backorders_btn_select_all")%></button>
                
			</div>

                	
	
			<form name="Form1" method=post action="/utilities/account/backorders/">
									
					<div class="spacer20">				
						<%
						=os
						%>
					</div>	
				
<%
					if not glHideBackOrderCancel and BOFound then
%>
						&nbsp;<br>
						
						<input class=FormHidden type=hidden name=XMLData value="">
						<input class=FormHidden type=hidden name=RecCount value="<%=ShowCount%>">
						<input class=FormHidden type=hidden name=FormAction  value="BACKORDER_AMEND">
<%
					end if
					
					ConfirmStr = lne("backorders_confirm_quantities")
%>				
				
					<div class="text-right spacer20">
						<button onclick="if (!confirm('<%=ConfirmStr%>')) { return false; } else { this.form.submit(); }" id=btnSend class="btn btn-primary"><%=lne("backorders_btn_send_request")%></button>
					</div>				
				
			</form>

			<div class="alert">
				<i class="icon icon-info-sign"></i> 
				<%
				=lne("label_small_backorder_notification")
				%>                                        
			</div>

	<%
	end if 
	%>			


				
			</div>
		</div>		
	</div>		



<script>
	$(document).ready(function() {
		
		//select and deselect
		$("#btnCheckAll").click(function () {
			$('.RemoveCheck').prop('checked', true);
			//alert('clicked')
			$('.QuickSearchSKUQty').val('0');
			$('.QuickSearchSKUQty').prop('disabled', true);
			
		});

			
	});	
</script>

<script type="text/javascript">
	//var strAmendXML = "";

    function setBackOrderSortOrder(pVal) {

        //alert(pVal);
        window.location.assign("/utilities/account/backorders/default.asp?BackOrderSortOrder=" + pVal);

    }

    function CancelLine(cbobj, FieldID)
	{
		var obj = document.getElementsByName("sfield" + FieldID);
		
		if (cbobj.checked)
		{ 
			obj[0].value = 0;
			obj[0].disabled = true;
		}
		else
		{
			//alert(document.getElementById("OrigQty_" + FieldID).value);
			obj[0].disabled = false;
			obj[0].value = document.getElementById("OrigQty_" + FieldID).value;
		}
	}

	function CompileAmendList()
	{
	
		//alert("CompileAmendList()")
	
		var strAmendXML = "";

		objColl = document.forms["Form1"].elements;

		for (i = 0; i < objColl.length; i++)
		{
			if (objColl[i].type == "text" && objColl[i].name.indexOf("sfield") == 0)
			{
					strAmendXML += "<SKU Qty=\"" + objColl[i].value  + "\""; 
			}
			else if (objColl[i].type == "hidden" && objColl[i].name.indexOf("sfield") == 0)
			{
					strAmendXML += " UnID=\"" + objColl[i].value  +  "\"/>"; 
			}

		}

		//alert(strAmendXML)
		
		strAmendXML = '<Transaction AcCode="<% = AcDetails.AcCode %>">' + strAmendXML + '</Transaction>';
		document.forms["Form1"].XMLData.value = strAmendXML;

		return true;
	}

</script>

<!--#include virtual="/assets/includes/footer.asp" -->