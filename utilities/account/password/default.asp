<%
PageTitle = "Change your password"

%>

<!--#include virtual="/assets/includes/header.asp" -->


<%

FormAction = request("FormAction")


if FormAction = "UPDATE_PASSWORD" then
	
	'jhInfo "FormAction = " & FormAction

	CurrentPassword = trim("" & request("CurrentPassword"))
	NewPassword = trim("" & request("NewPassword"))
	NewPassword_CONFIRM = trim("" & request("NewPassword_CONFIRM"))
	
	if 1 = 1 then
		
		jhAdmin "CurrentPassword = " & CurrentPassword
		jhAdmin "NewPassword = " & NewPassword
		jhAdmin "NewPassword_CONFIRM = " & NewPassword_CONFIRM
		
	end if
	
	ErrorsFound = false
	
	if NewPassword <> "" then
		dbConnect()

		if NewPassword = "" then
			ErrorMessage = "You have not entered a new password."
		else
			
			sql = "SELECT * FROM END_EbusClients WHERE EbusClientCode = '" & AcDetails.AcCode & "'"
			jhAdmin sql
			
			set rs = db.execute(sql)
			
			if not rs.eof then
				ThisPW = trim("" & rs("ClientPassword"))
				'jh ThisPW
			end if
			rsClose()
			
			if CurrentPassword <> ThisPW then
				ErrorMessage = "You have not entered your current password correctly - please re-enter"	
			end if
			
			if NewPassword_CONFIRM <> NewPassword then
				ErrorMessage = "Your new and confirmed passwords do not match - please re-enter"
			end if
		end if
		
		if ErrorMessage = "" then
			sql = "UPDATE END_EbusClients SET ClientPassword = '" & NewPassword & "' WHERE EbusClientCode = '" & AcDetails.AcCode & "'"
			jhAdmin sql
			db.execute(sql)
			PasswordChanged = true
		end if
		
		dbClose()
	end if	
	
end if

'jh "OrderThreshold = " & OrderThreshold

%>

	
<link rel="stylesheet" href="css/index.css" type="text/css" media="screen" />

	<div class="container">      

		<div class=row>
			<div class=span12>
				<h3><%=lne("label_change_password")%></h3>
				
				<%
				if PasswordChanged then
				%>
					<hr>
					<h5>
						<%
						=lne("label_thank_you")
						%>
					</h5>
				
					<p>Your  password has been changed</p>
					
					<p><a class=OrangeLink href="/utilities/account/"><%=lne("contact_details_update_complete_return")%></a></p>
				<%
				elseif ErrorMessage <> "" then
				%>
					<%
					jhErrorNice "Errors found", ErrorMessage
					%>
				<%
				end if
				%>
			
				
				<div class=spacer40></div>
				<div class=spacer40></div>
			</div>
		</div>
		
		<%
		if not PasswordChanged then
		%>
		
		<form action="/utilities/account/password/" method=post class="form-horizontal">
		

		<div class=row>
			<div class=span12>
				
					<div class="control-group">
						<label class="control-label" for="CurrentPassword"><%=lne("label_change_password_current")%></label>
						<div class="controls">
							<input type="password" id="CurrentPassword" name="CurrentPassword" value="<%=CurrentPassword%>">
						</div>
					</div>
					<hr>
					<div class="control-group">
						<label class="control-label" for="NewPassword"><%=lne("label_change_password_new")%></label>
						<div class="controls">
							<input type="password" id="NewPassword" name="NewPassword" value="<%=NewPassword%>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="NewPassword_CONFIRM"><%=lne("label_change_password_new_confirm")%></label>
						<div class="controls">
							<input type="password" id="NewPassword_CONFIRM" name="NewPassword_CONFIRM" value="<%=NewPassword_CONFIRM%>">
						</div>
					</div>					
				
			</div>
		</div>		
		
		<div class=row>
			<div class=span12>
				
				<hr>
			
				<input type=submit name=SubmitButton class="btn btn-success" value="<%=lne("label_save_changes")%>">
				
				<input type=hidden name=FormAction class="FormHidden" value="UPDATE_PASSWORD">
			</div>
		</div>
		
		</form>		

		<%
		end if
		%>
		
	</div>
	<div class=spacer40></div>			
			

<!--#include virtual="/assets/includes/footer.asp" -->



<%

dbClose()

%>

