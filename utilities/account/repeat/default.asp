<%
PageTitle = "Repeat orders"

%>

<!--#include virtual="/assets/includes/header.asp" -->

<%

FormAction = request("FormAction")
RepeatOrderRef = request("RepeatOrderRef")
RecCount = cdbl("0" & request("RecCount"))

if FormAction = "REPEAT_ORDER" then

	'jhInfo "REPEAT - " & RepeatOrderRef
	
	'jh "RecCount = " & RecCount
	
	SomeAdded = false
	
	for c = 1 to RecCount
		ThisSKU = request("SKU_" & c)
		ThisQty = cleannum(request("Qty_" & c))
		
		if ThisSKU <> "" and ThisQty <> "" then
			'jh "ADD = " & ThisSKU & " (" & ThisQty & ")"
			SomeAdded = true
			call basketAdd(ThisSKU, ThisQty, AcDetails.AcCode)
			
		end if
		
	next
	
	
	if SomeAdded then 
		OrderRepeated = true
	else
		OrderRepeatError = true
	end if
	
end if

CheckDate = ExDate(now)

sql = "SELECT DISTINCT TOP 10  OrderRef, DueDate, thYourRef "
sql = sql & "FROM END_SalesOrderLineValues esv INNER JOIN Transactions t ON t.thOurRef = esv.OrderRef "
sql = sql & "WHERE AccountCode = '" & AcDetails.AcCode & "' AND DueDate <= " & CheckDate & " "
sql = sql & "ORDER BY DueDate DESC "

'jhAdmin sql

x = getrs(sql, RecArr, rc)

'jhAdmin "rc = " & rc

set ls = new strconcatstream

ls.add "<table border=1 class=""ReportTable legible"">"

for c = 0 to rc
	ThisRef = RecArr(0,c)
	ThisDate = RecArr(1,c)
	DispDate = FromExDate(ThisDate)
	ThisYourRef = RecArr(2,c)

	ls.add "<tr id=""RepeatLine_" & c & """ class=""RepeatLine"">"
	ls.add "<td><i data-row-id=""" & c & """ onclick=""getRepeatDetails('" & ThisRef & "', '" & c & "');"" class=""pointer icon-search""></i>"
	ls.add "<td><strong>"
	ls.add ThisRef
	ls.add "</strong></td>"									
	ls.add "<td>"
	ls.add ThisYourRef
	ls.add "</td>"					
	ls.add "<td>"
	ls.add DispDate
	ls.add "</td>"													
	ls.add "</tr>"

next					
					
ls.add "</table>"

RepeatOS = ls.value

%>

<style>
	.ReportTable TD {
		padding:5px;
	}
	
	.RepeatLineSelected{
		background-color:<%=glDarkColor%>;
	}
	
</style>

<link rel="stylesheet" href="css/index.css" type="text/css" media="screen" />

	<div class="container">      

		<div class="row">      
			
		
			<div class="span4 spacer10">
		
					<h3 class=spacer20><%=lne("report_title_repeat_orders")%></h3>
					<p><%=lne("label_repeat_orders_explained")%></p>						
					<%
					=RepeatOS
					%>
					
			</div>	
			
			<div class="span8 spacer10">
				
					<div id="RepeatAjaxDiv">
						<div class=spacer20></div>
						<%
						if OrderRepeated then						
							jhInfoNice "Selected items added to basket!", "Your chosen items have been added to your basket - <a href=""/checkout/"">click here to go to the checkout</a>"
						elseif OrderRepeatError then
							jhErrorNice "No items selected", "You did not select any valid quantitys - please re-enter"
						end if
						%>
						
					</div>
							
			</div>
		</div>		
	</div>		

<script>
	$(document).ready(function() {
		
		//select and deselect
		$("#btnCheckAll").click(function () {
			$('.RemoveCheck').prop('checked', true);
			//alert('clicked')
			$('.QuickSearchSKUQty').val('0');
			$('.QuickSearchSKUQty').prop('disabled', true);
			
		});

			
	});	
</script>	
	
<script>

	function getRepeatDetails(pRef, pRow) {
		//alert('in')
		//$('#ajax_prod_results').fadeout('fast')
		
		$("#RepeatAjaxDiv").html('<span class="AjaxLoader"><img src="/assets/images/icons/indicator.gif"> loading...</span>');
		
		var RepeatOrderRef = pRef
		
		//alert(pRef)
		
		$.ajax({
			//contentType: "application/x-www-form-urlencoded;charset=ISO-8859-1",
			//dataType: 'json',
			scriptCharset: "ISO-8859-1" ,
			contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
			url: "/misc_server.asp?CallType=REPEAT_ORDER&RepeatOrderRef=" + RepeatOrderRef,
			cache: false,			
			}).done(
				function( html ) {
				$("#RepeatAjaxDiv").html(html);
				$('#RepeatAjaxDiv').show(100, function() {
					// Animation complete.
					$('.RepeatLine').removeClass('RepeatLineSelected');
					$('#RepeatLine_' + pRow).addClass('RepeatLineSelected');
					
					
				  });					
			});
		
	}

</script>	
	
<!--#include virtual="/assets/includes/footer.asp" -->

