﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sagepay.master" AutoEventWireup="true"
    CodeFile="Result.aspx.cs" Inherits="Form_Result" %>

<%@ Import Namespace="SagePay.IntegrationKit" %>
<%@ Import Namespace="SagePay.IntegrationKit.Messages" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    
<%
jhInfo("BEGIN SAGEPAY RESULT PROCESSING!");


//jh (PaymentStatusResult.Status.ToString());



string strSagepayStatus = PaymentStatusResult.Status.ToString();
string strVendorTxCode = PaymentStatusResult.VendorTxCode.ToString();

string strStatusDetail = PaymentStatusResult.StatusDetail.ToString();

string strAmount = PaymentStatusResult.Amount.ToString();

string strFraudResponse = "";

if (PaymentStatusResult.FraudResponse != FraudResponse.NONE) { 
	strFraudResponse = PaymentStatusResult.FraudResponse.ToString();
}

string strVpsTxId = PaymentStatusResult.VpsTxId.ToString();
string strTxAuthNo = PaymentStatusResult.TxAuthNo.ToString();

string strBankAuthCode = "";
if(!IsNullOrWhiteSpace(PaymentStatusResult.BankAuthCode)) strBankAuthCode = PaymentStatusResult.BankAuthCode.ToString(); 

string strDeclineCode = "";
if(!IsNullOrWhiteSpace(PaymentStatusResult.DeclineCode)) strDeclineCode = PaymentStatusResult.DeclineCode.ToString(); 

string strAvsCv2 = "";
string strAddressResult = "";
string strPostCodeResult = "";
string strCv2Result = "";

if(!IsNullOrWhiteSpace(PaymentStatusResult.AvsCv2)) {
	
	strAvsCv2 = PaymentStatusResult.AvsCv2.ToString(); 
	strAddressResult = PaymentStatusResult.AddressResult.ToString(); 
	strPostCodeResult = PaymentStatusResult.PostCodeResult.ToString(); 
	strCv2Result = PaymentStatusResult.Cv2Result.ToString(); 
	
}

string strThreeDSecureStatus = "";

if (PaymentStatusResult.ThreeDSecureStatus != ThreeDSecureStatus.NONE) { 
	strThreeDSecureStatus = PaymentStatusResult.ThreeDSecureStatus.ToString();
}            
                    
string strCavv	= "";
if (!IsNullOrWhiteSpace(PaymentStatusResult.Cavv)) {
	strCavv = PaymentStatusResult.Cavv.ToString();
}
	
string strCardType	= "";
if (PaymentStatusResult.CardType != CardType.NONE) {
	strCardType = PaymentStatusResult.CardType.ToString();
}

string strLast4Digits	= "";
if (!IsNullOrWhiteSpace(PaymentStatusResult.Last4Digits)) {
	strLast4Digits = PaymentStatusResult.Last4Digits.ToString();
}

string strExpiryDate	= "";
if (!IsNullOrWhiteSpace(PaymentStatusResult.ExpiryDate)) {
	strExpiryDate = PaymentStatusResult.ExpiryDate.ToString();
}

string strppCardType	= "";
string strppAddressStatus = "";
string strppPayerStatus = "";

if (PaymentStatusResult.CardType == CardType.PAYPAL) {
	strppCardType = PaymentStatusResult.CardType.ToString();
	strppAddressStatus = PaymentStatusResult.AddressStatus.ToString();
	strppPayerStatus = PaymentStatusResult.PayerStatus.ToString();
}
	
bool okToInsert = true;

if (okToInsert) {	


	if (1==0) {
		
		
		jh ("strSagepayStatus = " + strSagepayStatus);
		jh ("strVendorTxCode = " + strVendorTxCode);
		jh ("strStatusDetail = " + strStatusDetail);
		jh ("strFraudResponse = " + strFraudResponse);

		jh ("strVpsTxId = " + strVpsTxId);
		jh ("strTxAuthNo = " + strTxAuthNo);
		jh ("strBankAuthCode = " + strBankAuthCode);

		jh ("strAvsCv2 = " + strAvsCv2);
		jh ("strAddressResult = " + strAddressResult);
		jh ("strPostCodeResult = " + strPostCodeResult);
		jh ("strCv2Result = " + strCv2Result);

		jh ("strThreeDSecureStatus = " + strThreeDSecureStatus);

	}	

	string sql;
	
	string connStr = System.Configuration.ConfigurationManager.ConnectionStrings["JH_EnduraWebConnectionString"].ToString();	
	SqlConnection db = new SqlConnection(connStr);
    db.Open();			
	
	sql = "DELETE FROM END_SagePayOrders WHERE VendorTxCode = '" + strVendorTxCode + "' AND VPSTxID = '" + strVpsTxId + "'";
	//jhInfo(sql);
	
	//db.execute(sql)
	
	SqlCommand insertCommand = new SqlCommand(sql, db);	
	insertCommand.ExecuteNonQuery();	
	

	sql = "INSERT INTO END_SagePayOrders (Status, StatusDetail, VendorTxCode, VPSTxID, TxAuthNo, Amount, AVCSCV2, AddressResult, PostCodeResult, CV2Result, CAVV, CardType, Last4Digits, strAddressStatus, PayerStatus ) VALUES (";
	
	sql = sql + "'" + validstr(strSagepayStatus) + "', ";
	sql = sql + "'" + validstr(strStatusDetail) + "', ";
	sql = sql + "'" + validstr(strVendorTxCode) + "', ";
	sql = sql + "'" + validstr(strVpsTxId) + "', ";
	sql = sql + "'" + validstr(strTxAuthNo) + "', ";
	sql = sql + "'" + validstr(strAmount) + "', ";
	sql = sql + "'" + validstr(strAvsCv2) + "', ";
	sql = sql + "'" + validstr(strAddressResult) + "', ";
	sql = sql + "'" + validstr(strPostCodeResult) + "', ";
	sql = sql + "'" + validstr(strCv2Result) + "', ";
	sql = sql + "'" + validstr(strCavv) + "', ";
	sql = sql + "'" + validstr(strCardType) + "', ";
	sql = sql + "'" + validstr(strLast4Digits) + "', ";
	sql = sql + "'" + validstr(strppAddressStatus) + "', ";
	sql = sql + "'" + validstr(strppPayerStatus) + "' ";
	
	sql = sql + ") ";
	
	//jhInfo(sql);
				
	insertCommand = new SqlCommand(sql, db);	
	insertCommand.ExecuteNonQuery();
	
	db.Close();		
	
	jhInfo("INSERTED!");
	
	string redirTo = "/utilities/sagepay_v300/payment-result/?VendorTxCode=" + strVendorTxCode + "&TxAuthNo=" + strTxAuthNo;
	
	jh(redirTo);
	
	Response.Redirect(redirTo);

}

if (1==0) {

	jh (PaymentStatusResult.StatusDetail.ToString());
	jh (PaymentStatusResult.FraudResponse.ToString());
	jh (PaymentStatusResult.Amount.ToString());
	jh (PaymentStatusResult.VpsTxId.ToString());
	jh (PaymentStatusResult.TxAuthNo.ToString());
	jh (PaymentStatusResult.BankAuthCode.ToString());
	jh (PaymentStatusResult.DeclineCode.ToString());

	jh (PaymentStatusResult.AvsCv2.ToString());
	jh (PaymentStatusResult.AddressResult.ToString());
	jh (PaymentStatusResult.PostCodeResult.ToString());
	jh (PaymentStatusResult.Cv2Result.ToString());

	jh (PaymentStatusResult.ThreeDSecureStatus.ToString());

	jh (PaymentStatusResult.Cavv.ToString());

	jh (PaymentStatusResult.CardType.ToString());

	jh (PaymentStatusResult.Last4Digits.ToString());

	jh (PaymentStatusResult.CardType.ToString());

	jh (PaymentStatusResult.AddressStatus.ToString());
	jh (PaymentStatusResult.PayerStatus.ToString());

}

%>		
		
</asp:Content>
