
<%
PageTitle = "SagePay Payment Success"
%>

<!--#include virtual="/assets/includes/header.asp" -->

<%
dim PaymentReceivedAmount

'ThisCurrency = GetCurrency(session("ENDURA_Currency"))


SagePayCurrency = AcDetails.SagePayCurrency
if SagePayCurrency = "" then SagePayCurrency = "GBP"

'OrderSubmitted = ProcessOrderReturn(SagePayOrderID)

'PaymentReceivedAmount = 999.99

VendorTxCode = request("VendorTxCode")
TxAuthNo = request("TxAuthNo")

'jhInfo "VendorTxCode = " & VendorTxCode
'jhInfo "TxAuthNo = " & TxAuthNo

sql = "UPDATE END_SagePayOrders SET ClientCode = '" & AcDetails.AcCode & "' "
sql = sql & "WHERE VendorTxCode = '" & validstr(VendorTxCode) & "' "
sql = sql & "AND TxAuthNo = '" & validstr(TxAuthNo) & "' "
sql = sql & "AND ISNULL(ClientCode, '') = ''  "

'jhAdmin sql

db.execute(sql)

'TOP (200) SagePayOrderID, Status, StatusDetail, VendorTxCode, VPSTxID, TxAuthNo, Amount, AVCSCV2, AddressResult, PostCodeResult, CV2Result, CAVV, CardType, 
 '                     Last4Digits, strAddressStatus, PayerStatus, ClientCode, B2BOrderID, DateEntered

sql = "SELECT * FROM END_SagePayOrders "
sql = sql & "WHERE VendorTxCode = '" & validstr(VendorTxCode) & "' "
sql = sql & "AND TxAuthNo = '" & validstr(TxAuthNo) & "' "
sql = sql & "AND ClientCode = '" & AcDetails.AcCode & "' "

'jhAdmin sql

set rs = db.execute(sql)

if not rs.eof then
	PaymentStatus = "" & rs("Status")
	PaymentReceivedAmount = "" & rs("Amount")
	StatusDetail = "" & rs("StatusDetail")
	
	jhAdmin "PaymentStatus = " & PaymentStatus
	jhAdmin "PaymentReceivedAmount = " & PaymentReceivedAmount
	jhAdmin "StatusDetail = " & StatusDetail
	
else
	jhError "FATAL ERROR"
	FatalError = true
end if

rsClose()


'PaymentStatus = ""

if PaymentStatus = "OK" or PaymentStatus = "AUTHENTICATED" or PaymentStatus = "REGISTERED" then

%>
						   
	<div class=container>
		<div class=row>
			<div class=span12>
			
				<h3><%=lne("label_thank_you")%></h3>             

				<hr>
				
				<%
				PaymentStr = "<span class=""label label-success"" style=""font-size:16pt;line-height:140%"" >" & SagePayCurrency & "&nbsp;" & dispCurrency_PLAIN(PaymentReceivedAmount) & "</span>"
				%>
				
				<h4><%=lne_REPLACE("sagepay_payment_received", PaymentStr)%></h4>
				
				<p><%=lne("sagepay_payment_notification")%></p>
				
				
				<%
				

				set BasketDetails = new ClassBasketDetails
				
				BasketC = BasketDetails.BasketC
				BasketArr = BasketDetails.BasketArr
				
				'jh "BasketC = " & BasketC
				
				if BasketC >= 0 then
					bContents = ""
					
					for c = 0 to BasketC
						ThisSKU = BasketArr(6,c)
						ThisQty = BasketArr(7,c)
						ThisName = BasketArr(20,c)
						
						bContents = bContents & "<div><strong>" & ThisSKU & "</strong> " & ThisName & " x " & ThisQty & "</div>"
						'jh "ThisSKU = " & ThisSKU
					next 
					
					%>
					<div class="alert alert-success">
						<h3><%=lne("sagepay_complete_order_prompt")%></h3>
						<p>
							<%=lne("sagepay_complete_order_prompt_explained")%>
						</p>		
						<%=bContents%>	
						
					</div>
					
					<hr>
					<p><a class=OrangeLink href="/checkout/"><%=lne("sagepay_checkout_return")%></a></p>  				
					
					<%
					
					
				end if
				%>
				
				
							 
				
			</div>		
			
		</div>
			
	</div>

<%
else

	if PaymentStatus = "NOTAUTHED" then
		FailReason = "You payment was declined by the bank. This could be due to insufficient funds, or incorrect card details."
	elseif PaymentStatus = "ABORT" then
		FailReason = "You chose to Cancel your order on the payment pages."
	elseif PaymentStatus = "REJECTED" then
		FailReason = "Your order did not meet our minimum fraud screening requirements."
	elseif PaymentStatus = "INVALID" or PaymentStatus = "MALFORMED" then
		FailReason = "We could not process your order because we have been unable to register your transaction with our Payment Gateway."
	elseif PaymentStatus = "ERROR" then
		FailReason = "We could not process your order because our Payment Gateway service was experiencing difficulties."
	else
		FailReason = "The transaction process failed. Please contact us with the date and time of your order and we will investigate."
	end if
	
	FailureDetails = StatusDetail
	if StatusDetail = "" then StatusDetail = "Unknown"
		
%>
	<div class=container>
		<div class=row>
			<div class=span12>
			
				<h3><%=lne("sagepay_payment_failed")%></h3>              

				<hr>
						
				<%
				PaymentStr = "<span class=""label label-important"" style=""font-size:16pt;line-height:140%"" >" & SagePayCurrency & "&nbsp;" & dispCurrency_PLAIN(PaymentReceivedAmount) & "</span>"
				%>
				
				<h4 class=spacer20><%=lne_REPLACE("sagepay_payment_not_successful", PaymentStr)%></h4>			
				
				<div class=spacer20 style="color:red;"><%=FailReason%></div>
				<div class=spacer20 style="color:silver;"><%=FailureDetails%></div>
				
				
				
				<p><a class=OrangeLink href="/utilities/sagepay/?RetryAmount=<%=PaymentReceivedAmount%>">You can try again here</a></p>
				
				<p><a class=OrangeLink href="/utilities/contact/">If you continue to have problems, please contact us for assistance</a></p>
				
			
				
			</div>		
			
		</div>
			
	</div>

<%
end if
%>


<!--#include virtual="/assets/includes/footer.asp" -->


<script language=javascript>
	
	function swapAddr() {
		
		//var pid document.getElementById('DeliverTo').selected;
		
		var comboValue
		var selIndex = document.CheckOutForm.DeliverTo.selectedIndex;
		comboValue = document.CheckOutForm.DeliverTo.options[selIndex].value;
		
		var x = document.getElementById('Del_'+comboValue).innerHTML;
		//alert(x)
		
		document.getElementById('DelAddrDiv').innerHTML = x;

		//alert(comboValue);
	}

</script>


