﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SagePay.IntegrationKit.Messages;
using System.Text;
using System.Data.SqlClient;

public partial class sagepay_handler : System.Web.UI.Page
{
    public string Crypt { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        SagePayFormIntegration sagePayFormIntegration = new SagePayFormIntegration();

        IFormPayment request = sagePayFormIntegration.FormPaymentRequest();
        SetSagePayAPIData(request);

        var errors = sagePayFormIntegration.Validation(request);

        if (errors == null || errors.Count == 0)
        {
            sagePayFormIntegration.ProcessRequest(request);
            //lblDisplayQueryString.Text = HttpUtility.HtmlEncode(sagePayFormIntegration.RequestQueryString);
            Crypt = request.Crypt;
            phSubmitPayment.Visible = true;
        }
        else
        {
            phSubmitPayment.Visible = false;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < errors.Count; i++)
            {
                sb.AppendFormat("{0} is invalid</br>", errors.GetKey(i));
            }
            //lblErrorMessages.Text = sb.ToString();
            //phError.Visible = true;

        }

       
    }

    public void jh(string pstring)
    {
        Response.Write("<div style=\"font-family:arial;font-size:9pt;margin-bottom:3px;padding:5px;border-radius:3px;color:black;background:gold;\">" + pstring + "</div>");
    }

    public void jhInfo(string pstring)
    {
        Response.Write("<div style=\"font-family:arial;font-size:9pt;margin-bottom:3px;padding:5px;border-radius:3px;color:white;background:limegreen;\">" + pstring + "</div>");
    }

    private void SetSagePayAPIData(IFormPayment request)
    {

        //jhInfo("RUN SAGEPAY API!");

        string nl = System.Environment.NewLine;

        string paymentAmount = Request.QueryString["paymentAmount"];
        decimal intPaymentAmount = Convert.ToDecimal(paymentAmount);

        var acCode = Request.QueryString["acCode"];
        var EmailConfirmationTo = Request.QueryString["EmailConfirmationTo"];
		var CurrencyCode = Request.QueryString["CurrencyCode"];

        //jh("acCode = " + acCode);
        //jh("paymentAmount = " + paymentAmount);
		
        //jh("EmailConfirmationTo = " + EmailConfirmationTo);
		//jh("CurrencyCode = " + CurrencyCode);

        //jh("GET DB DETAILS FROM BASKET!");
		
		string connStr = System.Configuration.ConfigurationManager.ConnectionStrings["JH_EnduraWebConnectionString"].ToString();
		//jh (connStr);
		
        SqlConnection db = new SqlConnection(connStr);
        db.Open();
        //jh("db opened!");

        string sql;

        sql = "SELECT ";
        sql = sql + "c.cuCompany, c.cuAddr1, c.cuAddr2, c.cuAddr3, c.cuAddr4, c.cuAddr5, c.cuIdxPostCode, " + nl;
        sql = sql + "c.cuDelAddr1, c.cuDelAddr2, c.cuDelAddr3, c.cuDelAddr4, c.cuDelAddr5, " + nl;
        sql = sql + "c.cuContact, c.cuPhone, c.cuIdxEmailAddr " + nl;
        sql = sql + "FROM Customers c WHERE idxAcCode = '" + acCode + "'" + nl;

        //jh(sql);

        string companyName = "";
        string CustomerContactName = "";
        string CustomerFirstName = "";
        string CustomerSurname = "";

        string strBillingAddress1 = "";
        string strBillingAddress2 = "";
        string strBillingCity = "";

        string SagePayCountry = "GB";

        string strBillingPostCode = "";
        string strBillingCountry = "";
        //string strBillingState = "";

        string strBillingPhone = "";
       
        using (SqlCommand cmd = new SqlCommand(sql, db))

        {

            using (SqlDataReader dr = cmd.ExecuteReader())
            {

                //jh("HAS ROWS? " + dr.HasRows);

                if (dr.HasRows) {
                    dr.Read();

                    //jhInfo ("company = " + dr["cuCompany"].ToString());

                    companyName = dr["cuCompany"].ToString().Trim();
                    CustomerContactName = dr["cuContact"].ToString().Trim();

                    strBillingAddress1 = dr["cuAddr1"].ToString().Trim();
                    strBillingAddress2 = dr["cuAddr2"].ToString().Trim();
                    strBillingCity = dr["cuAddr3"].ToString().Trim();

		            if (strBillingCity == "" && strBillingAddress2 != "") {
			            strBillingCity = strBillingAddress2;
			            strBillingAddress2 = "";
		            }
		
		            if (strBillingCity == "") strBillingCity = ".";
                    strBillingPostCode = dr["cuIdxPostCode"].ToString().Trim();
                   
		            strBillingCountry = SagePayCountry;
		            //strBillingState = "";

                    strBillingPhone = dr["cuPhone"].ToString().Trim();

                }
               
                dr.Close();
            }


        }

        CustomerContactName = "John Hutton";
        //jhInfo("CustomerContactName = " + CustomerContactName);
        
        Int32 SpacePos = CustomerContactName.IndexOf(" ");

        //jh("SpacePos = " + SpacePos.ToString());


		if (SpacePos > 0) {
			//strBillingFirstnames = left(CustomerContactName, spacepos);

            //jh (CustomerContactName.Substring(0,SpacePos));
            //jh(CustomerContactName.Substring(SpacePos, CustomerContactName.Length - SpacePos));
            //jh("LEN = " + CustomerContactName.Length.ToString());

            CustomerFirstName = CustomerContactName.Substring(0, SpacePos).Trim();
            CustomerSurname = CustomerContactName.Substring(SpacePos, CustomerContactName.Length - SpacePos).Trim();

			//strBillingSurname = right(CustomerContactName, len(CustomerContactName)-SpacePos)
        } else {
            CustomerFirstName = CustomerContactName.Trim();
            CustomerSurname = CustomerContactName.Trim();
		}



        db.Close();
        //jh("db closed!");

        //jhInfo ("END API");

		var isCollectRecipientDetails = SagePaySettings.IsCollectRecipientDetails;
       
        request.VpsProtocol = SagePaySettings.ProtocolVersion;
        request.TransactionType = SagePaySettings.DefaultTransactionType;
        request.Vendor = SagePaySettings.VendorName;

        //Assign Vendor tx Code.
        request.VendorTxCode = SagePayFormIntegration.GetNewVendorTxCode();
      
        //request.Amount = cart.Basket.GetTotalGrossPrice();
        request.Amount = intPaymentAmount;

        request.Currency = "EUR"; // SagePaySettings.Currency;
        
        request.Description = "B2B Payment - " + SagePaySettings.VendorName;
        request.SuccessUrl = SagePaySettings.SiteFqdn + "utilities/sagepay_v300/payment-result/Result.aspx";
        request.FailureUrl = SagePaySettings.SiteFqdn + "utilities/sagepay_v300/payment-result/Result.aspx";

        request.CustomerName = companyName;

        //jh("request.CustomerName = " + request.CustomerName);

        request.BillingSurname = CustomerSurname;
        request.BillingFirstnames = CustomerFirstName;
        request.BillingAddress1 = strBillingAddress1;
        request.BillingAddress2 = strBillingAddress2;
        request.BillingCity = strBillingCity;
        request.BillingCountry = strBillingCountry;

        request.BillingPostCode = strBillingPostCode;
        request.BillingPhone = strBillingPhone;

        request.DeliverySurname = CustomerSurname;
        request.DeliveryFirstnames = CustomerFirstName;
        request.DeliveryAddress1 = strBillingAddress1;
        request.BillingAddress2 = strBillingAddress2;
        request.DeliveryCity = strBillingCity;
        request.DeliveryCountry = strBillingCountry;
        request.DeliveryPostCode = strBillingPostCode;
        

        //Optional

        //request.CustomerName = cart.Billing.FirstNames + " " + cart.Billing.Surname;

        request.CustomerEmail = EmailConfirmationTo;
        request.VendorEmail = SagePaySettings.VendorEmail;
        request.SendEmail = SagePaySettings.SendEmail;

        request.EmailMessage = SagePaySettings.EmailMessage;
       
        //request.BillingPostCode = strBillingPostCode;
		//request.BillingState = cart.Billing.Region;
        //request.BillingPhone = cart.Billing.Phone;
       // request.DeliveryAddress2 = cart.Shipping.Address2;
        //request.DeliveryPostCode = strBillingPostCode;
		//request.DeliveryState = cart.Shipping.Region;
        //request.DeliveryPhone = cart.Shipping.Phone;
        
        request.AllowGiftAid = SagePaySettings.AllowGiftAid;
        request.ApplyAvsCv2 = SagePaySettings.ApplyAvsCv2;
        request.Apply3dSecure = SagePaySettings.Apply3dSecure;

        request.BillingAgreement = "";
        request.ReferrerId = SagePaySettings.ReferrerID;
      
	    string LineCount = "1";
	    string ThisQty = "1";	    
	    string ThisVat = "0";
	
	    var strBasket = LineCount + ":ENDURA B2B PAYMENT:" + ThisQty + ":" + intPaymentAmount + ":" + ThisVat + ":" + intPaymentAmount + ":" + intPaymentAmount;
	    //sngTotal = intPaymentAmount

        request.Basket = strBasket;

        request.SurchargeXml = SagePaySettings.SurchargeXML; 

        //set vendor data
        request.VendorData = ""; //Use this to pass any data you wish to be displayed against the transaction in My Sage Pay.
    }

}