﻿<%@ Page Title="" Language="C#" MasterPageFile="~/FormKitSite.master" AutoEventWireup="true"
    CodeFile="Details.aspx.cs" Inherits="Form_Details" %>
<%@ Register src="../Controls/Details.ascx" tagname="Details" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="content">
        <form id="form1" runat="server">
        <uc1:Details ID="ucDetails" runat="server" />
        <div class="greyHzShadeBar">
            &nbsp;</div>
        <div class="formFooter">
            <asp:ImageButton ID="cmdBack" ImageUrl="../images/back.gif" Style="float: left"
                AlternateText="Go back to the place order page" OnClick="OnClick_Back" runat="server" />
            <asp:ImageButton ID="cmdProceed" ImageUrl="../images/proceed.gif" Style="float: right"
                AlternateText="Proceed to the next page" OnClick="OnClick_Proceed" runat="server" />
        </div>
        </form>
    </div>
</asp:Content>
