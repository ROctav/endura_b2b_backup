﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Form_Details : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    protected void OnClick_Proceed(object sender, ImageClickEventArgs e)
    {
        //Redirect
        ucDetails.SaveDetails();
        Response.Redirect("~/Form/Confirm.aspx");
    }
    protected void OnClick_Back(object sender, ImageClickEventArgs e)
    {
        if (SagePaySettings.CollectExtraInformation)
        {
            Response.Redirect("~/Form/ExtraInformation.aspx");
        }
        else
        {
            Response.Redirect("~/Form/Basket.aspx");
        }
    }
}