
<!--#include virtual="/assets/includes/global_functions.asp" -->
<!--#include virtual="/utilities/sagepay/sagepay_config.asp" -->


<%



Response.ContentType = "text/html"
Response.AddHeader "Content-Type", "text/html;charset=ISO-8859-1"
'Response.CodePage = 65001
Response.CharSet = "ISO-8859-1"

Response.Expires = 0
Response.Expiresabsolute = Now() - 1442
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"

SelectedLanguageID = getSelectedLanguage()

set AcDetails = new DealerAccountDetails
set BasketDetails = new ClassBasketDetails

if not isLoggedIn() then jhStop

'jh "SAGE PAY SERVER!"

PaymentAmount = cleannum(request("PaymentAmount"))

'jh "PaymentAmount = " & PaymentAmount

if PaymentAmount <> "" then
	PaymentAmount = cdbl(PaymentAmount)
	if PaymentAmount > 0 then
		
		'jhInfo "BUILD! (" & PaymentAmount & ")"
		SagePayCurrency = AcDetails.SagePayCurrency
		
		if AcDetails.SagePayCurrency = "" then 
			SagePayCurrency = "GBP"
			'jhAdmin "SagePayCurrency = " & SagePayCurrency			
		end if
		
		ThisSagePayEncrypt = GetPPTxt(PaymentAmount, SagePayCurrency, rNoEnc)
		'jh "ThisSagePayEncrypt = " & ThisSagePayEncrypt
		'jh "rNoEnc = " & rNoEnc		
	
%>
		<form name="FormSagePay" id="FormSagePay" method="POST" action="<%=strPurchaseURL%>">
			
			<input class=FormHidden type="hidden" name="VPSProtocol" value="<%=strProtocol%>">
			<input class=FormHidden type="hidden" name="TxType" value="<%=strTransactionType%>">
			<input class=FormHidden type="hidden" name="Vendor" value="<%=strVendorName%>">
			<input class=FormHidden type="hidden" name="Crypt" value="<%=ThisSagePayEncrypt%>">
			
			<input class=FormHidden type="hidden" name="OrigAmount" value="<%=PaymentAmount%>">
			
			<div class="alert alert-success">
				<h4>Connecting to SagePay...</h4>
				<p>If nothing happens, click the button below</p>
				<button class="btn btn-success">PAY NOW</button>
			</div>
		</form>
		
		<textarea style="display:none;height:300px;width:600px;font-family:arial;font-size:8pt;"><%=rNoEnc%></textarea>		
<%	
		PaymentValid = true
	end if	
end if

if not PaymentValid then 
	jhErrorNice "Not a valid payment amount", "The payment amount entered is not valid, please try again.  If the problem persists, please contact us."
end if

set AcDetails = nothing
set BasketDetails = nothing


dbClose()


function GetPPTxt(byVal pOrderTotal, byVal pSagePayCurrency, byref ThisSagePayNoEncrypt)

	dim sql
	'jhAdmin "BEGIN GetPPTxt()"
	
	'jh CurrencyCountry
	if SagePayCountry = "" then SagePayCountry = "GB"
	
	'jh "AcDetails.RiderCard =  " & AcDetails.RiderCard
	
	If AcDetails.RiderCard then

		sql = "select Name as cuCompany, AddressStreet as cuAddr1,  AddressTown as cuAddr2,  AddressCounty as cuAddr3,  (SELECT CountryName FROm End_countrycodes WHERE countrycode = rc.country) as cuAddr4, '' as cuAddr5, AddressPostCode as cuIdxPostCode, ShippingAddressStreet as cuDelAddr1, ShippingAddressTown as cuDelAddr2,   ShippingAddressCounty as cuDelAddr3,  (SELECT CountryName FROm End_countrycodes WHERE countrycode = rc.country) as cuDelAddr4,  ShippingAddressPostCode as cuDelAddr5, (SELECT adminname FROM end_adminusers WHERE AdminID = rc.ContactID) AS cuContact, phone as cuPhone, email as  cuIdxEmailAddr " 
		sql = sql & "  from ExchequerRiderCard rc WHERE AcCode = '" & AcDetails.AcCode & "'" & vbcrlf

	Else

		sql = "SELECT "
		sql = sql & "cuCompany, cuAddr1, cuAddr2, cuAddr3, cuAddr4, cuAddr5, cuIdxPostCode, " & vbcrlf
		sql = sql & "cuDelAddr1,cuDelAddr2,cuDelAddr3,cuDelAddr4, cuDelAddr5, "& vbcrlf
		sql = sql & "cuContact, cuPhone, cuIdxEmailAddr "& vbcrlf
		sql = sql & "FROM Customers WHERE idxAcCode = '" & AcDetails.AcCode & "'" & vbcrlf

	End if	

	SagePayCurrencyCode = pSagePayCurrency
	if SagePayCurrencyCode = "" then SagePayCurrencyCode = "GBP"
	
	'jhAdmin addbr(sql)
	
	'jhStop
	
	dbConnect()
	
	set rs = db.execute(sql)
	
	if not rs.eof then
	
		strCustomerEMail = AcDetails.ContactEmail
		CustomerContactName = AcDetails.ContactName
				
		'if isjh() then strCustomerEMail = "john@kmvcreative.com"
		
		'jh CustomerContactName
		spacepos = instr(1, CustomerContactName, " ")
		'jh len(CustomerContactName)
		'jh spacepos
		if SpacePos > 0 then
			strBillingFirstnames = left(CustomerContactName, spacepos)
			strBillingSurname = right(CustomerContactName, len(CustomerContactName)-SpacePos)
		else
			strBillingFirstnames = CustomerContactName
			strBillingSurname = CustomerContactName		
		end if

		'jh strBillingFirstnames
		'jh strBillingSurname		
		
		strCustomerName		  = trim("" & rs("cuCompany"))			
		strBillingAddress1    = trim("" & rs("cuAddr1"))
		strBillingAddress2    = trim("" & rs("cuAddr2"))
		strBillingCity        = trim("" & rs("cuAddr3"))
		if strBillingCity = "" and strBillingAddress2 <> "" then
			strBillingCity = strBillingAddress2
			strBillingAddress2 = ""
		end if
		
		if strBillingCity = "" then strBillingCity = "."
		strBillingPostCode    = trim("" & rs("cuIdxPostCode"))
		strBillingCountry     = SagePayCountry
		strBillingState       = ""
		
		strBillingPhone       = trim("" & rs("cuPhone"))
		
		bIsDeliverySame       = true
		strDeliveryFirstnames = strBillingFirstnames
		strDeliverySurname    = strBillingSurname
		strDeliveryAddress1   = strBillingAddress1
		strDeliveryAddress2   = strBillingAddress2
		strDeliveryCity       = strBillingCity
		strDeliveryPostCode   = strBillingPostCode
		strDeliveryCountry    = strBillingCountry
		strDeliveryState      = strBillingState
		strDeliveryPhone      = strBillingPhone
		
	end if
	
	rsClose()

	Randomize
	strVendorTxCode=cleaninput(strVendorName &	"-" & right(DatePart("yyyy",Now()),2) &_
					right("00" & DatePart("m",Now()),2) & right("00" & DatePart("d",Now()),2) &_
					right("00" & DatePart("h",Now()),2) & right("00" & DatePart("n",Now()),2) &_
					right("00" & DatePart("s",Now()),2) & "-" & cstr(round(rnd*100000)),"VendorTxCode")
	
	sngTotal=0.0
	strThisEntry=strCart
	strBasket=""
	iBasketItems=0
	
	'dim c, RecArr
	dim LineCount
	
	'jhAdmin "pOrderTotal = " & pOrderTotal
	
	LineCount = 1
	ThisQty = 1
	ThisCost = pOrderTotal
	ThisVat = 0
	
	strBasket = LineCount & ":ENDURA B2B PAYMENT:" & ThisQty & ":" & ThisCost & ":" & ThisVAT & ":" & (ThisCost+ThisVAT) & ":" & (ThisCost+ThisVAT)*ThisQty
	sngTotal = (ThisCost+ThisVAT)*ThisQty
									
	if right(strBasket, 1) = ":" then strBasket = left(strBasket, len(strBasket)-1)							
																	
	'** Now to build the Form crypt field.  For more details see the Form Protocol 2.23 **
	strPost="VendorTxCode=" & strVendorTxCode '** As generated above **
	strPost=strPost & "&Amount=" & FormatNumber(sngTotal,2,-1,0,0) '** Formatted to 2 decimal places with leading digit **
	strPost=strPost & "&Currency=" & SagePayCurrencyCode
	strPost=strPost & "&Description=Endura B2B Payment Gateway"
	strPost=strPost & "&SuccessURL=" & strYourSiteFQDN & "/utilities/sagepay/payment-success/"
	strPost=strPost & "&FailureURL=" & strYourSiteFQDN & "/utilities/sagepay/payment-failure/"

	'** This is an Optional setting. Here we are just using the Billing names given.
	'strPost=strPost & "&OrderPosted=xxx"  

	'** Email settings:
	'** Flag 'SendEMail' is an Optional setting. 
	'** 0 = Do not send either customer or vendor e-mails, 
	'** 1 = Send customer and vendor e-mails if address(es) are provided(DEFAULT). 
	'** 2 = Send Vendor Email but not Customer Email. If you do not supply this field, 1 is assumed and e-mails are sent if addresses are provided.

	if iSendEMail = 0 then
	    strPost=strPost & "&SendEMail=0"
	else
	    if iSendEMail = 1 then
	        strPost=strPost & "&SendEMail=1" 
	    else
	        strPost=strPost & "&SendEMail=2" 
	    end if
	    
	    if len(strCustomerEMail) > 0 then
	        strPost=strPost & "&CustomerEMail=" & strCustomerEMail '** This is an Optional setting
	    end if
	    
	    if (strVendorEMail <> "[your e-mail address]") and (strVendorEMail <> "") then
		    strPost=strPost & "&VendorEMail=" & strVendorEMail '** This is an Optional setting
	    end if

	    '** You can specify any custom message to send to your customers in their confirmation e-mail here **
	    '** The field can contain HTML if you wish, and be different for each order.  This field is optional **
	    strPost=strPost & "&eMailMessage=Thank you for your payment."
	end if

	
	
	'** Billing Details:
	
	if cstrBillingPostCode = "" then strBillingPostCode = "0"
	if xstrBillingCountry = "" then strBillingCountry = "GB"
	if cstrDeliveryPostCode = "" then strDeliveryPostCode = "0"
	
	'jhAdmin "strBillingPostCode = " & strBillingPostCode
	'jhAdmin "strBillingCountry = " & strBillingCountry
	'jhAdmin "strDeliveryPostCode = " & strDeliveryPostCode
	
	strPost=strPost & "&CustomerName=" & strCustomerName
	strPost=strPost & "&BillingFirstnames=" & strBillingFirstnames
	strPost=strPost & "&BillingSurname=" & strBillingSurname
	strPost=strPost & "&BillingAddress1=" & strBillingAddress1
	
	if len(strBillingAddress2) > 0 then strPost=strPost & "&BillingAddress2=" & strBillingAddress2
	
	strPost=strPost & "&BillingCity=" & strBillingCity
	
	if len(strBillingPostCode) > 0 then strPost=strPost & "&BillingPostCode=" & strBillingPostCode
	if len(strBillingCountry) > 0 then strPost=strPost & "&BillingCountry=" & strBillingCountry
	if len(strBillingState) > 0 then strPost=strPost & "&BillingState=" & strBillingState
	if len(xxx_strBillingPhone) > 0 then strPost=strPost & "&BillingPhone=" & strBillingPhone

	'** Delivery Details:
	strPost=strPost & "&DeliveryFirstnames=" & strDeliveryFirstnames
	strPost=strPost & "&DeliverySurname=" & strDeliverySurname
	strPost=strPost & "&DeliveryAddress1=" & strDeliveryAddress1
	if len(strDeliveryAddress2) > 0 then strPost=strPost & "&DeliveryAddress2=" & strDeliveryAddress2
	strPost=strPost & "&DeliveryCity=" & strDeliveryCity
	strPost=strPost & "&DeliveryPostCode=" & strDeliveryPostCode
	strPost=strPost & "&DeliveryCountry=" & strDeliveryCountry
	if len(strDeliveryState) > 0 then strPost=strPost & "&DeliveryState=" & strDeliveryState
	if len(xxX_strDeliveryPhone) > 0 then strPost=strPost & "&DeliveryPhone=" & strDeliveryPhone

	strPost=strPost & "&Basket=" & strBasket '** As created above. Basket is optional. **

	'** For charities registered for Gift Aid, set to 1 to display the Gift Aid check box on the payment pages. This is an Optional setting. **
	strPost=strPost & "&AllowGiftAid=0"
		
	'** Allow fine control over AVS/CV2 checks and rules by changing this value. 0 is Default **
	'** It can be changed dynamically, per transaction, if you wish.  This is an Optional setting. See the Form Protocol document **
	if strTransactionType<>"AUTHENTICATE" then strPost=strPost & "&ApplyAVSCV2=0"
		
	'** Allow fine control over 3D-Secure checks and rules by changing this value. 0 is Default **
	'** It can be changed dynamically, per transaction, if you wish.  This is an Optional setting. See the Form Protocol document **
	strPost=strPost & "&Apply3DSecure=0"

	' ** Encrypt the plaintext string for inclusion in the hidden field **
	strCrypt = base64Encode(SimpleXor(strPost,strEncryptionPassword))

	GetPPTxt = strCrypt

	'jh "++++"
	'jh replace(strPost, "&", "<br>&")
	'jh "++++"
	
	'jhAdmin "++++"
	'jhAdmin "<div style=""width:800px;"">" & strCrypt & "</div>"
	'jhAdmin "++++"	

	ThisSagePayNoEncrypt = strPost

end function


%>
