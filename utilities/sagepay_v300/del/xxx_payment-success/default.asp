
<%
PageTitle = "SagePay Payment Success"
%>

<!--#include virtual="/assets/includes/header.asp" -->

<!--#include virtual="/utilities/sagepay/sagepay_config.asp" -->

<%
dim PaymentReceivedAmount

'ThisCurrency = GetCurrency(session("ENDURA_Currency"))

function ProcessOrderReturn(byref SagePayOrderID)

	'jhAdmin "CHECK INCOMING!"
	
	
	OrderPosted = request("OrderPosted")
	
	'jhAdmin "OrderPosted = " & OrderPosted
	
	strCrypt=Request.QueryString("Crypt")
	SagePayOrderComplete = request("SagePayOrderComplete")
	
	'strCrypt = "TEST"
	
	if strCrypt <> "" then
		'jhAdmin "<h2>ORDER RETURN - ORDER SUCCESSFUL</h2>"
		'jh "PROCESS AND STORE ORDER"
		
		strDecoded=simpleXor(Base64Decode(strCrypt),strEncryptionPassword)
			
		strStatus=getToken(strDecoded,"Status")
		
		'jh strStatus
		
		'jhAdmin "STRCART = " & Session("strCart")
		
		
		
		strStatusDetail=getToken(strDecoded,"StatusDetail")
		strVendorTxCode=getToken(strDecoded,"VendorTxCode")
		strVPSTxID=getToken(strDecoded,"VPSTxId")
		strTxAuthNo=getToken(strDecoded,"TxAuthNo")
		strAmount=getToken(strDecoded,"Amount")
		strAVSCV2=getToken(strDecoded,"AVSCV2")
		strAddressResult = getToken(strDecoded,"AddressResult")
		strPostCodeResult = getToken(strDecoded,"PostCodeResult")
		strCV2Result = getToken(strDecoded,"CV2Result")
		strGiftAid = getToken(strDecoded,"GiftAid")
		str3DSecureStatus = getToken(strDecoded,"3DSecureStatus")
		strCAVV = getToken(strDecoded,"CAVV")
		strCardType = getToken(strDecoded,"CardType")
		strLast4Digits = getToken(strDecoded,"Last4Digits")
		strAddressStatus = getToken(strDecoded,"AddressStatus") '** PayPal transactions only
		strPayerStatus = getToken(strDecoded,"PayerStatus")     '** PayPal transactions only
	
		if 1 = 0 then
		
			jhAdmin "strStatusDetail = " & strStatusDetail
			jhAdmin "strVendorTxCode = " & strVendorTxCode
			jhAdmin "strVPSTxID = " & strVPSTxID
			jhAdmin "strTxAuthNo = " & strTxAuthNo
			jhAdmin "strAmount = " & strAmount
			jhAdmin "strAVSCV2 = " & strAVSCV2
			jhAdmin"strAddressResult = " & strAddressResult
			jhAdmin "strPostCodeResult = " & strPostCodeResult
			jhAdmin "strCV2Result = " & strCV2Result
			jhAdmin "strCAVV = " & strCAVV
			jhAdmin "strCardType = " & strCardType
			jhAdmin "strLast4Digits = " & strLast4Digits
			jhAdmin "strAddressStatus = " & strAddressStatus	
			jhAdmin "strPayerStatus = " & strPayerStatus	
			
		end if
		PaymentReceivedAmount = strAmount
		
		'jhAdmin PaymentReceivedAmount

		'** Empty the cart, we're done with it now because the order is successful **
		Session("strCart")=""
		
		sql = "DELETE FROM END_SagePayOrders WHERE VendorTxCode = '" & strVendorTxCode & "' AND VPSTxID = '" & strVPSTxID & "'"
		jhAdmin sql
		
		'db.execute(sql)
		
		sql = "INSERT INTO END_SagePayOrders (Status, StatusDetail, VendorTxCode, VPSTxID, TxAuthNo, Amount, AVCSCV2, AddressResult, PostCodeResult, CV2Result, CAVV, CardType, Last4Digits, strAddressStatus, PayerStatus, ClientCode ) VALUES ("
		
		sql = sql & "'" & validstr(strStatus) & "', "
		sql = sql & "'" & validstr(strStatusDetail) & "', "
		sql = sql & "'" & validstr(strVendorTxCode) & "', "
		sql = sql & "'" & validstr(strVPSTxID) & "', "
		sql = sql & "'" & validstr(strTxAuthNo) & "', "
		sql = sql & "'" & validstr(strAmount) & "', "
		sql = sql & "'" & validstr(strAVSCV2) & "', "
		sql = sql & "'" & validstr(strAddressResult) & "', "
		sql = sql & "'" & validstr(strPostCodeResult) & "', "
		sql = sql & "'" & validstr(strCV2Result) & "', "
		sql = sql & "'" & validstr(strCAVV) & "', "
		sql = sql & "'" & validstr(strCardType) & "', "
		sql = sql & "'" & validstr(strLast4Digits) & "', "
		sql = sql & "'" & validstr(strAddressStatus) & "', "
		sql = sql & "'" & validstr(strPayerStatus) & "', "
		sql = sql & "'" & validstr(AcDetails.AcCode) & "') "
		
		jhAdmin sql
		
		'db.execute(sql)
		
		dbClose()
		
		'Response.Redirect "checkout_jh.asp?SagePayOrderComplete=" & strVendorTxCode
	
		'jhAdmin "strStatus = " & strStatus
		
		strStatus = "OK" 
		
		if strStatus = "OK" then
			session("ENDURA_spPaymentMade") = strAmount
			
			jhAdmin "SEND EMAIL "
		
			recList = ""
			
			if glSagePayNotificationsTo <> "" then
				'jhAdmin glOrdersTo
				glArr = split(glSagePayNotificationsTo, "|")
				for c = 0 to ubound(glArr)
					if glArr(c) <> "" then
						'jhAdmin glArr(c)
						ContactEmail = glArr(c)						
						recList = recList & ContactEmail & ";"
						
					end if
				next	
			end if

			if AcDetails.CostBand <> "A" then
				
				recList = recList & glContactEmail_SAGEPAY_INTERNATIONAL & ";"
				
			end if
						
			'recList = "jhutton1@gmail.com"			
						
			jhAdmin "recList = " & recList	
						
			MailBody = "A payment has been received via the SagePay payment gateway for " & AcDetails.AcCode & vbcrlf	
			MailBody = MailBody & "Payment value: " & SagePayCurrency & strAmount & vbcrlf
			MailBody = MailBody & "Please login to MySagePay to view the full payment details "
								
			MailSubject = "B2B Payment Received (NEW B2B SYSTEM) - " & AcDetails.AcCode
	
			'jhInfoNice MailSubject,  addbr(MailBody)
	
			SendSuccess = SendMail(MailSubject, MailBody, RecList, glSenderEmail_SAGEPAY, false)
	
		else
			jhInfoNice "Errors Found!",  "Sorry, there was a problem processing your payment, please contact us."
		end if	
	
	end if

end function

if AcDetails.SagePayUser then
	CCRequired = true
else
	'Response.Redirect "checkout_jh.asp"
end if

SagePayCurrency = AcDetails.SagePayCurrency
if SagePayCurrency = "" then SagePayCurrency = "GBP"

OrderSubmitted = ProcessOrderReturn(SagePayOrderID)

'PaymentReceivedAmount = 999.99

%>
                       
<div class=container>
	<div class=row>
		<div class=span12>
		
			<h3><%=lne("label_thank_you")%></h3>             

			<hr>
			
			<%
			PaymentStr = "<span class=""label label-success"" style=""font-size:16pt;line-height:140%"" >" & SagePayCurrency & "&nbsp;" & dispCurrency_PLAIN(PaymentReceivedAmount) & "</span>"
			%>
			
			<h4><%=lne_REPLACE("sagepay_payment_received", PaymentStr)%></h4>
         	
			<p><%=lne("sagepay_payment_notification")%></p>
			
			
			<%
			

			set BasketDetails = new ClassBasketDetails
			
			BasketC = BasketDetails.BasketC
			BasketArr = BasketDetails.BasketArr
			
			'jh "BasketC = " & BasketC
			
			if BasketC >= 0 then
				bContents = ""
				
				for c = 0 to BasketC
					ThisSKU = BasketArr(6,c)
					ThisQty = BasketArr(7,c)
					ThisName = BasketArr(20,c)
					
					bContents = bContents & "<div><strong>" & ThisSKU & "</strong> " & ThisName & " x " & ThisQty & "</div>"
					'jh "ThisSKU = " & ThisSKU
				next 
				
				%>
				<div class="alert alert-success">
					<h3><%=lne("sagepay_complete_order_prompt")%></h3>
					<p>
						<%=lne("sagepay_complete_order_prompt_explained")%>
					</p>		
					<%=bContents%>	
					
				</div>
				
				<hr>
				<p><a class=OrangeLink href="/checkout/"><%=lne("sagepay_checkout_return")%></a></p>  				
				
				<%
				
				
			end if
			%>
			
			
         	             
         	
		</div>		
		
	</div>
		
</div>


<!--#include virtual="/assets/includes/footer.asp" -->


<script language=javascript>
	
	function swapAddr() {
		
		//var pid document.getElementById('DeliverTo').selected;
		
		var comboValue
		var selIndex = document.CheckOutForm.DeliverTo.selectedIndex;
		comboValue = document.CheckOutForm.DeliverTo.options[selIndex].value;
		
		var x = document.getElementById('Del_'+comboValue).innerHTML;
		//alert(x)
		
		document.getElementById('DelAddrDiv').innerHTML = x;

		//alert(comboValue);
	}

</script>


