﻿<%@ Page Title="" Language="C#" MasterPageFile="~/FormKitSite.master" AutoEventWireup="true"
    CodeFile="Confirm.aspx.cs" Inherits="Form_Confirm" %>

<%@ Import Namespace="SagePay.IntegrationKit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="content">
        <div id="contentHeader">
            Order Confirmation This
        </div>
        <p>
            Page page summarises the order details and customer information gathered on the
            previous screens. It is always a good idea to show your customers a page like this
            to allow them to go back and edit either basket or contact details.<br>
            <br>
            At this stage we also create the Form crypt field and a form to POST this information
            to the Sage Pay Gateway when the Proceed button is clicked.</p>
        <p>
            The URL to post to is: <b>
                <%= SagePaySettings.FormPaymentUrl %></b>
        </p>
        <p>
            <% if (!SagePaySettings.Environment.Equals("LIVE"))
               {%>
            Because you are in
            <%= SagePaySettings.Environment %>
            mode, the unencrypted contents of the crypt field are also displayed below, allowing
            you to check the contents. When you are in Live mode, you will only see the order
            confirmation boxes.
            <% }
               else
               { %>
            Since you are in LIVE mode, clicking Proceed will register your transaction with
            Form and automatically redirect you to the payment page, or handle any registration
            errors.
            <% } %>
        </p>
        <div class="greyHzShadeBar">
            &nbsp;</div>
        <table class="formTable">
            <tr>
                <td colspan="5">
                    <div class="subheader">
                        Your Basket Contents</div>
                </td>
            </tr>
            <tr class="greybar">
                <td width="17%" align="center">
                    Image
                </td>
                <td width="45%" align="left">
                    Title
                </td>
                <td width="15%" align="right">
                    Price
                </td>
                <td width="8%" align="right">
                    Quantity
                </td>
                <td width="15%" align="right">
                    Total
                </td>
            </tr>
            <% 
                ShoppingCart cart = ShoppingCart.Current();
                Basket basket = cart.Basket;

                foreach (var item in basket.GetItems())
                {
                    Product product = item.product;
        
            %>
            <tr>
                <td align="center">
                    <img src="../images/<%= product.GetImageName() %>" alt="DVD box">
                </td>
                <td align="left">
                    <%= product.Name %>
                </td>
                <td align="right">
                    <%= string.Format("{0:0.00}", product.GrossPrice)%> <%= SagePaySettings.Currency %>
                </td>
                <td align="right">
                    <%= item.qty %>
                </td>
                <td align="right">
                    <%= string.Format("{0:0.00}", item.GetTotalGrossPrice())%> <%= SagePaySettings.Currency %>
                </td>
            </tr>
            <% } %>
            <tr>
                <td colspan="4" align="right">
                    Delivery:
                </td>
                <td align="right">
                    <%= string.Format("{0:0.00}", basket.GetDeliveryItem().product.GrossPrice)%> <%= SagePaySettings.Currency %>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="right">
                    <strong>Total:</strong>
                </td>
                <td align="right">
                    <strong>
                        <%= string.Format("{0:0.00}", basket.GetTotalGrossPrice())%> <%= SagePaySettings.Currency %></strong>
                </td>
            </tr>
        </table>
        <table class="formTable">
            <tr>
                <td colspan="2">
                    <div class="subheader">
                        Your Billing Details</div>
                </td>
            </tr>
            <tr>
                <td class="fieldLabel">
                    Name:
                </td>
                <td class="fieldData">
                    <%= cart.Billing.FirstNames %>&nbsp;<%= cart.Billing.Surname %>
                </td>
            </tr>
            <tr>
                <td class="fieldLabel">
                    Address Details:
                </td>
                <td class="fieldData">
                    <%= cart.Billing.Address1 %><br>
                    <%= !string.IsNullOrEmpty(cart.Billing.Address2) ?  cart.Billing.Address2 + "<br>" : string.Empty %>
                    <%= cart.Billing.City %><br>
                    <%= !string.IsNullOrEmpty(cart.Billing.Region) ?  cart.Billing.Region + "<br>" : string.Empty %>
                    <%= cart.Billing.PostCode %><br>
                    <script type="text/javascript" language="javascript">
                        document.write(getCountryName("<%= cart.Billing.Country %>"));
                    </script>
                </td>
            </tr>
            <tr>
                <td class="fieldLabel">
                    Phone Number:
                </td>
                <td class="fieldData">
                    <%= cart.Billing.Phone %>&nbsp;
                </td>
            </tr>
            <tr>
                <td class="fieldLabel">
                    e-Mail Address:
                </td>
                <td class="fieldData">
                    <%= cart.Billing.Email %>&nbsp;
                </td>
            </tr>
        </table>
        <table class="formTable">
            <tr>
                <td colspan="2">
                    <div class="subheader">
                        Your Delivery Details</div>
                </td>
            </tr>
            <tr>
                <td class="fieldLabel">
                    Name:
                </td>
                <td class="fieldData">
                    <%= cart.Shipping.FirstNames %>&nbsp;<%= cart.Shipping.Surname%>
                </td>
            </tr>
            <tr>
                <td class="fieldLabel">
                    Address Details:
                </td>
                <td class="fieldData">
                    <%= cart.Shipping.Address1 %><br>
                    <%= !string.IsNullOrEmpty(cart.Shipping.Address2) ? cart.Shipping.Address2 + "<br>" : string.Empty%>
                    <%= cart.Shipping.City %><br>
                    <%= !string.IsNullOrEmpty(cart.Shipping.Region) ? cart.Shipping.Region + "<br>" : string.Empty %>
                    <%= cart.Shipping.PostCode %><br>
                    <script type="text/javascript" language="javascript">
                        document.write(getCountryName("<%= cart.Shipping.Country %>"));
                    </script>
                </td>
            </tr>
            <tr>
                <td class="fieldLabel">
                    Phone Number:
                </td>
                <td class="fieldData">
                    <%= cart.Shipping.Phone %>&nbsp;
                </td>
            </tr>
        </table>
        <% if (!SagePaySettings.Environment.Equals("LIVE"))
           {%>
        <table class="formTable">
            <tr>
                <td>
                    <div class="subheader">
                        Your Form Crypt Post Contents</div>
                </td>
            </tr>
            <tr>
                <td>
                    <p>
                        The text below shows the unencrypted contents of the Form Crypt field. This application
                        will not display this in LIVE mode. If you wish to view the encrypted and encoded
                        contents view the source of this page and scroll to the bottom. You'll find the
                        submission FORM there.</p>
            </tr>
            <tr>
                <td class="code">
                    <div class="protocolMessage">
                        <asp:Label ID="lblDisplayQueryString" runat="server"></asp:Label>
                    </div>
                </td>
            </tr>
        </table>
        <% } %>
        <div class="greyHzShadeBar">
            &nbsp;</div>
        <div class="formFooter">
            <table border="0" width="100%">
                <tr>
                    <td width="50%" align="left">
                        <asp:PlaceHolder ID="phError" runat="server" Visible="false">
                            <p class="warning">
                                <asp:Label ID="lblErrorMessages" runat="server"></asp:Label></p>
                        </asp:PlaceHolder>
                        <a href="details.aspx" title="Go back to the customer details page">
                            <img src="../images/back.gif" alt="Go back to the previous page" border="0" /></a>
                    </td>
                    <td width="50%" align="right">
                        <!-- ************************************************************************************* -->
                        <!-- This form is all that is required to submit the payment information to the system -->
                        <asp:PlaceHolder ID="phSubmitPayment" runat="server">
                            <form action="<%= SagePaySettings.FormPaymentUrl %>" method="post" id="SagePayForm"
                            name="SagePayForm">
                            <input type="hidden" name="VPSProtocol" value="<%= SagePaySettings.ProtocolVersion.VersionString() %>" />
                            <input type="hidden" name="TxType" value="<%= SagePaySettings.DefaultTransactionType %>" />
                            <input type="hidden" name="Vendor" value="<%= SagePaySettings.VendorName %>" />
                            <input type="hidden" name="Crypt" value="<%= Crypt %>" />
                            <input type="image" name="cmdProceed" src="../images/proceed.gif" alt="Proceed to Form registration" />
                            </form>
                        </asp:PlaceHolder>
                        <!-- ************************************************************************************* -->
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
