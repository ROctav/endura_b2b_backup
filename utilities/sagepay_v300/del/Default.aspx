﻿<%@ Page Title="" Language="C#" MasterPageFile="~/FormKitSite.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="Form_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="content">
        <div id="contentHeader">
            Welcome to the Sage Pay FORM .Net Kit
        </div>
        <div class="greyHzShadeBar">
            &nbsp;</div>
        <table class="formTable">
            <tr>
                <td colspan="2">
                    <div class="subheader">
                        Your current kit set-up</div>
                </td>
            </tr>
            <tr>
                <td class="fieldLabel">
                    Vendor Name:
                </td>
                <td class="fieldData">
                    <%= SagePaySettings.VendorName %>
                </td>
            </tr>
            <tr>
                <td class="fieldLabel">
                    Default Currency:
                </td>
                <td class="fieldData">
                    <%= SagePaySettings.Currency %>
                </td>
            </tr>
            <tr>
                <td class="fieldLabel">
                    URL of this kit:
                </td>
                <td class="fieldData">
                    <a href="<%= SagePaySettings.FullUrl %>">
                        <%= SagePaySettings.SiteFqdn %></a>
                    <% if (string.IsNullOrEmpty(SagePaySettings.SiteFqdn))
                       { %>
                    <br />
                    (<span class="warning">warning</span>: this is a guessed value as no "siteFqdn"
                    property is explicitly set in your configuration)
                    <% } %>
                </td>
            </tr>
            <tr>
                <td class="fieldLabel">
                    Gateway:
                </td>
                <td class="fieldData">
                    <a href="<%= SagePaySettings.FormPaymentUrl %>">
                        <%= SagePaySettings.FormPaymentUrl %></a>
                    <br />
                    (The Sage Pay Server URLs come ready configured for each environment but you can
                    also override them if you wish)
                </td>
            </tr>
        </table>
        <%--    <c:if test="${not data.isEncryptionPasswordOk}">
    <p class="warning"><b>Could not perform a test encryption. Verify your encryption password is set correctly.</b></p>
    </c:if>--%>
        <p>
            <% if (SagePaySettings.Environment.Equals("LIVE"))
               { %>
            <span class="warning">Your kit is pointing at the Live Sage Pay environment (you can
                change this by changing the value of the <b>sagepay.api.env</b> property). You should
                only do this once your have completed testing on both the Simulator AND Test servers,
                have sent your GoLive request to the technical support team and had confirmation
                that your account has been set up.
                <br>
                <br>
                <strong>Transactions sent to the Live service WILL charge your customers' cards.</strong></span>
            <% }
               else if (SagePaySettings.Environment.Equals("TEST"))
               { %>
            Your kit is pointing at the Sage Pay TEST environment (you can change this by changing
            the value of the <b>sagepay.api.env</b> property). This is an exact replica of the
            Live systems except that no banks are attached, so no authorisation requests are
            sent, nothing is settled and you can use our test card numbers when making payments.
            You should only use the test environment after you have completed testing using
            the Simulator AND the Sage Pay support team have mailed you to let you know your
            account has been created.<br>
            <br>
            <span><strong>If you are already set up on Live and are testing additional functionality,
                DO NOT leave your kit set to Test or you will not receive any money for your transactions!</strong></span>
            <% }
               else if (SagePaySettings.Environment.Equals("SIMULATOR"))
               { %>
            Your kit is currently pointing at the <b>Simulator</b> (you can change this by changing
            the value of the <b>sagepay.api.env</b> property). The simulator is a system provided
            by Sage Pay to enable you to build and configure your site correctly, to debug the
            messages you send to Server and practise handling responses from it. No customers
            are charged, no money is moved around. The Simulator is for development and testing
            ONLY.
            <% }
               else
               { %>
            <span class="warning">ERROR: unknown environment value</span>
            <% } %>
        </p>
        <div class="greyHzShadeBar">
            &nbsp;</div>
        <div class="formFooter">
            <a href="basket.aspx" title="Proceed to the next page" style="float: right;">
                <img src="../images/proceed.gif" alt="Proceed to the next page" name="cmdProceed"
                    border="0" />
            </a>
        </div>
    </div>
</asp:Content>
