﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SagePay.IntegrationKit.Messages;

public partial class Form_Result : System.Web.UI.Page
{
    public IFormPaymentResult PaymentStatusResult  { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["crypt"] != null && !string.IsNullOrEmpty(Request.QueryString["crypt"]))
        {
            SagePayFormIntegration sagePayFormIntegration = new SagePayFormIntegration();
            PaymentStatusResult = sagePayFormIntegration.ProcessResult(Request.QueryString["crypt"]);

        }
    }
}