﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SagePay.IntegrationKit.Messages;
using System.Text;

public partial class Form_Confirm : System.Web.UI.Page
{
    public string Crypt { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        SagePayFormIntegration sagePayFormIntegration = new SagePayFormIntegration();

        IFormPayment request = sagePayFormIntegration.FormPaymentRequest();
        SetSagePayAPIData(request);

        var errors = sagePayFormIntegration.Validation(request);

        if (errors == null || errors.Count == 0)
        {
            sagePayFormIntegration.ProcessRequest(request);
            lblDisplayQueryString.Text = HttpUtility.HtmlEncode(sagePayFormIntegration.RequestQueryString);
            Crypt = request.Crypt;
            phSubmitPayment.Visible = true;
        }
        else
        {
            phSubmitPayment.Visible = false;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < errors.Count; i++)
            {
                sb.AppendFormat("{0} is invalid</br>", errors.GetKey(i));
            }
            lblErrorMessages.Text = sb.ToString();
            phError.Visible = true;

        }
    }

    private void SetSagePayAPIData(IFormPayment request)
    {
		var isCollectRecipientDetails = SagePaySettings.IsCollectRecipientDetails;
        ShoppingCart cart = ShoppingCart.Current();

        request.VpsProtocol = SagePaySettings.ProtocolVersion;
        request.TransactionType = SagePaySettings.DefaultTransactionType;
        request.Vendor = SagePaySettings.VendorName;

        //Assign Vendor tx Code.
        request.VendorTxCode = SagePayFormIntegration.GetNewVendorTxCode();
      
        request.Amount = cart.Basket.GetTotalGrossPrice();
        request.Currency = SagePaySettings.Currency;
        request.Description = "The best DVDs from " + SagePaySettings.VendorName;
        request.SuccessUrl = SagePaySettings.SiteFqdn + "Form/Result.aspx";
        request.FailureUrl = SagePaySettings.SiteFqdn + "Form/Result.aspx";

        request.BillingSurname = cart.Billing.Surname;
        request.BillingFirstnames = cart.Billing.FirstNames;
        request.BillingAddress1 = cart.Billing.Address1;
        request.BillingCity = cart.Billing.City;
        request.BillingCountry = cart.Billing.Country;

        request.DeliverySurname = cart.Shipping.Surname;
        request.DeliveryFirstnames = cart.Shipping.FirstNames;
        request.DeliveryAddress1 = cart.Shipping.Address1;
        request.DeliveryCity = cart.Shipping.City;
        request.DeliveryCountry = cart.Shipping.Country;

        //Optional
        request.CustomerName = cart.Billing.FirstNames + " " + cart.Billing.Surname;
        request.CustomerEmail = cart.Billing.Email;
        request.VendorEmail = SagePaySettings.VendorEmail;
        request.SendEmail = SagePaySettings.SendEmail;

        request.EmailMessage = SagePaySettings.EmailMessage;
        request.BillingAddress2 = cart.Billing.Address2;
		request.BillingPostCode = cart.Billing.PostCode;
		request.BillingState = cart.Billing.Region;
        request.BillingPhone = cart.Billing.Phone;
        request.DeliveryAddress2 = cart.Shipping.Address2;
		request.DeliveryPostCode = cart.Shipping.PostCode;
		request.DeliveryState = cart.Shipping.Region;
        request.DeliveryPhone = cart.Shipping.Phone;

        request.AllowGiftAid = SagePaySettings.AllowGiftAid;
        request.ApplyAvsCv2 = SagePaySettings.ApplyAvsCv2;
        request.Apply3dSecure = SagePaySettings.Apply3dSecure;

        request.BillingAgreement = "";
        request.ReferrerId = SagePaySettings.ReferrerID;

        if (SagePaySettings.IsBasketXMLDisabled)
        {
            request.Basket = cart.Basket.ToColonSeparatedBasketstring();
        }
        else
        {
            request.BasketXml = cart.Basket.ToXml();
        }

        if (Customer.Current() != null)
        {
            request.CustomerXml = Customer.Current().ToXml(); 
        }
        request.SurchargeXml = SagePaySettings.SurchargeXML; 

		if (isCollectRecipientDetails)
        {
            var financialInstitution = cart.FinancialInstutition;

            if (financialInstitution != null)
            {
                request.FiRecipientAccountNumber = financialInstitution.RecipientAccountNumber;
                request.FiRecipientDateOfBirth = financialInstitution.RecipientDateOfBirth == null ? null : financialInstitution.RecipientDateOfBirth.Value.ToString("yyyyMMdd");
                request.FiRecipientPostCode = financialInstitution.RecipientPostCode;
                request.FiRecipientSurname = financialInstitution.RecipientSurname;
            }
        }

        //set vendor data
        request.VendorData = ""; //Use this to pass any data you wish to be displayed against the transaction in My Sage Pay.
    }

}