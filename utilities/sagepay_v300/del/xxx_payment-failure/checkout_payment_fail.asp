
<%
server.ScriptTimeout = 600
RetrieveBasket = true
PageTitle = "Checkout"

'ignoreBasket = true
%>
<!--#include virtual="/assets/includes/header.asp" -->

<!-- #include virtual="/SagePayFormKit/includes.asp" -->

<%
public PayAmount

ThisCurrency = GetCurrency(session("ENDURA_Currency"))

function ProcessOrderReturn(byref SagePayOrderID)

	'jhAdmin "CHECK INCOMING!"
	
	OrderPosted = request("OrderPosted")
	
	'jhAdmin "OrderPosted = " & OrderPosted
	
	strCrypt=Request.QueryString("Crypt")
	SagePayOrderComplete = request("SagePayOrderComplete")
	
	if strCrypt <> "" then
	
		strDecoded=simpleXor(Base64Decode(strCrypt),strEncryptionPassword)
			
		strStatus=getToken(strDecoded,"Status")
		
		'jh "STATUS: " & strStatus
		
		'jhAdmin "STRCART = " & Session("strCart")
		
		strStatusDetail=getToken(strDecoded,"StatusDetail")
		strVendorTxCode=getToken(strDecoded,"VendorTxCode")
		strVPSTxID=getToken(strDecoded,"VPSTxId")
		strTxAuthNo=getToken(strDecoded,"TxAuthNo")
		strAmount=getToken(strDecoded,"Amount")
		strAVSCV2=getToken(strDecoded,"AVSCV2")
		strAddressResult = getToken(strDecoded,"AddressResult")
		strPostCodeResult = getToken(strDecoded,"PostCodeResult")
		strCV2Result = getToken(strDecoded,"CV2Result")
		strGiftAid = getToken(strDecoded,"GiftAid")
		str3DSecureStatus = getToken(strDecoded,"3DSecureStatus")
		strCAVV = getToken(strDecoded,"CAVV")
		strCardType = getToken(strDecoded,"CardType")
		strLast4Digits = getToken(strDecoded,"Last4Digits")
		strAddressStatus = getToken(strDecoded,"AddressStatus") '** PayPal transactions only
		strPayerStatus = getToken(strDecoded,"PayerStatus")     '** PayPal transactions only
	
		if 1 = 1 then
			jhAdmin "strStatusDetail = " & strStatusDetail
			jhAdmin "strVendorTxCode = " & strVendorTxCode
			jhAdmin "strVPSTxID = " & strVPSTxID
			jhAdmin "strTxAuthNo = " & strTxAuthNo
			jhAdmin "strAmount = " & strAmount
			jhAdmin "strAVSCV2 = " & strAVSCV2
			jhAdmin "strAddressResult = " & strAddressResult
			jhAdmin "strPostCodeResult = " & strPostCodeResult
			jhAdmin "strCV2Result = " & strCV2Result
			jhAdmin "strCAVV = " & strCAVV
			jhAdmin "strCardType = " & strCardType
			jhAdmin "strLast4Digits = " & strLast4Digits
			jhAdmin "strAddressStatus = " & strAddressStatus	
			jhAdmin "strPayerStatus = " & strPayerStatus	
		end if
		
		PayAmount = strAmount
		strAmount = replace(strAmount, ",","")
		
		'** Empty the cart, we're done with it now because the order is successful **
		Session("strCart")=""
		
		'jhAdmin "INSERT?"
		
		sql = "DELETE FROM END_SagePayOrders WHERE VendorTxCode = '" & strVendorTxCode & "' AND VPSTxID = '" & strVPSTxID & "'"
		'jh sql
		db.execute(sql)
		
		sql = "INSERT INTO END_SagePayOrders (Status, StatusDetail, VendorTxCode, VPSTxID, TxAuthNo, Amount, AVCSCV2, AddressResult, PostCodeResult, CV2Result, CAVV, CardType, Last4Digits, strAddressStatus, PayerStatus, ClientCode ) VALUES ("
		
		sql = sql & "'" & validstr(strStatus) & "', "
		sql = sql & "'" & validstr(strStatusDetail) & "', "
		sql = sql & "'" & validstr(strVendorTxCode) & "', "
		sql = sql & "'" & validstr(strVPSTxID) & "', "
		sql = sql & "'" & validstr(strTxAuthNo) & "', "
		sql = sql & "'" & validstr(strAmount) & "', "
		sql = sql & "'" & validstr(strAVSCV2) & "', "
		sql = sql & "'" & validstr(strAddressResult) & "', "
		sql = sql & "'" & validstr(strPostCodeResult) & "', "
		sql = sql & "'" & validstr(strCV2Result) & "', "
		sql = sql & "'" & validstr(strCAVV) & "', "
		sql = sql & "'" & validstr(strCardType) & "', "
		sql = sql & "'" & validstr(strLast4Digits) & "', "
		sql = sql & "'" & validstr(strAddressStatus) & "', "
		sql = sql & "'" & validstr(strPayerStatus) & "', "
		sql = sql & "'" & validstr(ThisUserID) & "') "
		
		jhAdmin sql
		
		db.execute(sql)
		
		dbClose()
		
		'Response.Redirect "checkout_jh.asp?SagePayOrderComplete=" & strVendorTxCode
		
	end if

end function

if session("ENDURA_SagePayUser") then
	CCRequired = true
else
	jhAdmin "REDIRECT"
	'Response.Redirect "checkout_jh.asp"
end if


'jh "CHECK"



%>
                       
<table border=0 style="border:0px solid silver;" width=100%>
	<tr>
	
		<td valign=top class=Main>      
		
			<h3>PAYMENT FAILED!</h3>             

			<%=GetText("CHECKOUT_SagePayFailed")%>	
			
			<%
			'jh strStatusDetail
			OrderSubmitted = ProcessOrderReturn(SagePayOrderID)
			%>
         	
         	<p><a href="checkout_payment.asp?RetryAmount=<%=PayAmount%>">Return and try again</a></p>               
         	
		</td>		
		
		</tr>
		
</table>


<!--#include virtual="/assets/includes/footer.asp" -->


<script language=javascript>
	
	function swapAddr() {
		
		//var pid document.getElementById('DeliverTo').selected;
		
		var comboValue
		var selIndex = document.CheckOutForm.DeliverTo.selectedIndex;
		comboValue = document.CheckOutForm.DeliverTo.options[selIndex].value;
		
		var x = document.getElementById('Del_'+comboValue).innerHTML;
		//alert(x)
		
		document.getElementById('DelAddrDiv').innerHTML = x;

		//alert(comboValue);
	}

</script>


