<%
PageTitle = "Frequently Asked Questions (FAQ)"

%>

<!--#include virtual="/assets/includes/header.asp" -->
<style>
	.QuestionAnswer A {
		color:orange;
		font-weight:bold;
	}
</style>
<%

if not glFAQEnabled and not isAdminUser() then response.redirect "/"

sql = "SELECT ef.FAQID, ISNULL(FAQCategory_TRANSLATED, FAQCategory) AS FAQCategory, ISNULL(FAQTitle_TRANSLATED, FAQTitle) AS FAQTitle, ISNULL(FAQAnswer_TRANSLATED, FAQAnswer) AS FAQAnswer, ef.FAQDisplayIn, trans.FAQCategory_TRANSLATED, FAQTitle_TRANSLATED, FAQAnswer_TRANSLATED "
sql = sql & "FROM END_FAQ ef "
sql = sql & "LEFT JOIN (SELECT FAQID, FAQCategory_TRANSLATED, FAQTitle_TRANSLATED, FAQAnswer_TRANSLATED FROM END_FAQTranslations WHERE LanguageID = 0" & AcDetails.SelectedLanguageID & ") trans ON trans.FAQID = ef.FAQID " 
sql = sql & "WHERE ef.FAQDisplayIn LIKE '%B2B%' "
sql = sql & "ORDER BY ef.FAQCategory, ef.FAQTitle "

jhAdmin sql
'jhstop

x = getrs(sql, FAQArr, FAQC)

LastCategory = "zzz"

for c = 0 to FAQC
	ThisQuestionID = FAQArr(0,c)
	ThisCategory = FAQArr(1,c)
	if ThisCategory <> LastCategory then
		LastCategory = ThisCategory
		'FAQAnchors = FAQAnchors & "<li><a href=""#CATEGORY_" & ThisQuestionID & """><h4>" & ThisCategory & "</h4></a></li>"
		FAQDetail = FAQDetail & "<h4 style=""color:orange;padding:20px 0;"" id=""CATEGORY_" & ThisQuestionID & """>" & ThisCategory & "</h4>"
		
	end if
	
	ThisQuestionTitle = FAQArr(2,c)
	ThisQuestionAnswer = FAQArr(3,c)
	'jhAdmin FAQArr(2,c)
	
	'FAQAnchors = FAQAnchors & "<li><a class=OrangeLink href=""#QUESTION_" & ThisQuestionID & """>" & ThisQuestionTitle & "</a></li>"
	
	FAQDetail = FAQDetail & "<h4 class=""pointer QuestionLink"" data-id=""" & ThisQuestionID & """ id=""QUESTION_" & ThisQuestionID & """>" & ThisQuestionTitle & " <i class=""icon icon-chevron-right""></i></h4>"
	FAQDetail = FAQDetail & "<div class=QuestionAnswer id=""ANSWER_" & ThisQuestionID & """ style=""display:none;"">" & ThisQuestionAnswer & "</div>"
	
	FAQDetail = FAQDetail & "<hr>"
	
next
%>


<link rel="stylesheet" href="css/index.css" type="text/css" media="screen" />

	<div class="container">      

	
		<div class="row">      
			<div class="span12 spacer10">
				<h3 class="PageTitle spacer40"><%=lne_B2C("support_faq_page_title")%></h3>
			</div>
		</div>		
	
		<div class="row">      
			<div class="span12 spacer10">


				<h5><%=lne_B2C("support_faq_intro")%></h5>
				
				<p><a class=OrangeLink href="/contact/"><%=lne_B2C("support_faq_contact")%></a>			
<!--			
				<hr>
					<ul class="nav nav-pills nav-stacked">
						<%=FAQAnchors%>
					</ul>
-->			
				<hr>
				
				<%=FAQDetail%>
				
			</div>
		</div>		
	</div>		

<!--#include virtual="/assets/includes/footer.asp" -->

<script>
	$(".QuestionLink").click( function () {
		var qid = $( this ).data("id")
		$("#ANSWER_" + qid).toggle("fast");
		
	});
</script>