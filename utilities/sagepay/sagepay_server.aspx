﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sagepay.master" AutoEventWireup="true"
    CodeFile="sagepay_server.aspx.cs" Inherits="sagepay_handler" %>
  
<%@ Import Namespace="SagePay.IntegrationKit" %>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="Server">
		
             
        <div class="formFooter">
 
                        <!-- ************************************************************************************* -->
                        <!-- This form is all that is required to submit the payment information to the system -->
                        <asp:PlaceHolder ID="phSubmitPayment" runat="server">
                            <form action="<%= SagePaySettings.FormPaymentUrl %>" method="post" id="FormSagePay"
                            name="FormSagePay">
                            <input type="hidden" name="VPSProtocol" value="<%= SagePaySettings.ProtocolVersion.VersionString() %>" />
                            <input type="hidden" name="TxType" value="<%= SagePaySettings.DefaultTransactionType %>" />
                            <input type="hidden" name="Vendor" value="<%= SagePaySettings.VendorName %>" />
                            <input type="hidden" name="Crypt" value="<%= Crypt %>" />
                            
			                <div class="alert alert-success">
				                <h4>Connecting to SagePay...</h4>
				                <p>If nothing happens, click the button below</p>
				                <button class="btn btn-success">PAY NOW</button>
			                </div>

                            </form>
                        </asp:PlaceHolder>
                        <!-- ************************************************************************************* -->
         
        </div>
    </div>

</asp:Content>   