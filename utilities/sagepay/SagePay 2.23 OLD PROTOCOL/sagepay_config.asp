
<%
'**************************************************************************************************
'Form ASP Kit Includes File
'**************************************************************************************************

'**************************************************************************************************
' Change history
' ==============
'
' 20/01/2009 - Simon Wolfe - Updated for  protocol 2.23
' 30/06/2007 - Mat Peck - New kit version
'**************************************************************************************************
'
' Description
' ===========
'
' Page with no visible content, but defines the constants and functions used in other pages in the
' kit.  It also opens connections to the database and defines record sets for later use.  It is
' included at the top of every other page in the kit and is paried with the closedown scipt.
'**************************************************************************************************

response.buffer=true
Response.Expires=-1
Response.AddHeader "PRAGMA","NO-CACHE"
Response.CacheControl="no-cache"


'**************************************************************************************************
' Values for you to update
'**************************************************************************************************

'** IMPORTANT.  Set the strYourSiteFQDN value to the Fully Qualified Domain Name of your server. **
'** This should start http:// or https:// and should be the name by which our servers can call back to yours **
'** i.e. it MUST be resolvable externally, and have access granted to the Sage Pay servers **
'** examples would be https://www.mysite.com or http://212.111.32.22/ **
'** NOTE: You should leave the final / in place. **

strYourSiteFQDN = glWebsiteURL

'SIM
'strConnectTo="SIMULATOR" 
'strVendorName="endurab2bsim" '** Set this value to the Vendor Name assigned to you by Sage Pay or chosen when you applied **
'strEncryptionPassword="dyoKHK2hVNZvkI8x" ' ** Set this value to the XOR Encryption password assigned to you by Sage Pay **

'TEST
if isjh() and 1 = 0 then
	strConnectTo="SIMULATOR" 
	strVendorName="endurab2bsim" '** Set this value to the Vendor Name assigned to you by Sage Pay or chosen when you applied **
	strEncryptionPassword="dyoKHK2hVNZvkI8x" ' ** Set this value to the XOR Encryption password assigned to you by Sage Pay **
elseif 1 = 0 then
	jhErrorNice "SAGEPAY TEST", "SagePay in TESTING mode - isjh() "
	strConnectTo="TEST" 
	strVendorName="endurauk" '** Set this value to the Vendor Name assigned to you by Sage Pay or chosen when you applied **
	strEncryptionPassword="57RyqVmCUDMM9Uju" ' ** Set this value to the XOR Encryption password assigned to you by Sage Pay **
else
	'LIVE
	strConnectTo="LIVE" 
	strVendorName="endurauk" '** Set this value to the Vendor Name assigned to you by Sage Pay or chosen when you applied **
	strEncryptionPassword="QJut569vLhbL8Tqf" ' ** Set this value to the XOR Encryption password assigned to you by Sage Pay **
end if

strCurrency="GBP" '** Set this to indicate the currency in which you wish to trade. You will need a merchant number in this currency **
strTransactionType="DEFERRED" '** This can be PAYMENT, DEFERRED or AUTHENTICATE if your Sage Pay account supports those payment types **
strPartnerID="" '** Optional setting. If you are a Sage Pay Partner and wish to flag the transactions with your unique partner id set it here.
'strVendorEMail="john@kmvcreative.com" ' ** Set this to the mail address which will receive order confirmations and failures **

strVendorEMail = glContactEmail_SAGEPAY ' ** Set this to the mail address which will receive order confirmations and failures ** 'CHANGED TO LORRAINE, 08 MAY 13

'** Optional setting. 
'** 0 = Do not send either customer or vendor e-mails, 
'** 1 = Send customer and vendor e-mails if address(es) are provided(DEFAULT). 
'** 2 = Send Vendor Email but not Customer Email. If you do not supply this field, 1 is assumed and e-mails are sent if addresses are provided.
iSendEMail=1

'**************************************************************************************************
' Global Definitions for this site
'**************************************************************************************************
strProtocol="2.23"

if strConnectTo="LIVE" then
	strPurchaseURL="https://live.sagepay.com/gateway/service/vspform-register.vsp"
elseif strConnectTo="TEST" then
	strPurchaseURL="https://test.sagepay.com/gateway/service/vspform-register.vsp"	
else
	strPurchaseURL="https://test.sagepay.com/simulator/vspformgateway.asp"
end if

'jh "strPurchaseURL = " & strPurchaseURL

'**************************************************************************************************
' Useful functions for all pages in this kit
'**************************************************************************************************


'** Filters unwanted characters out of an input string.  Useful for tidying up FORM field inputs
public function cleanInput(strRawText,strType)

	if strType="Number" then
		strClean="0123456789."
		bolHighOrder=false
	elseif strType="VendorTxCode" then
		strClean="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_."
		bolHighOrder=false
	else
  		strClean=" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,'/{}@():?-_&�$=%~<>*+""" & vbCRLF
		bolHighOrder=true
	end if

	strCleanedText=""
	iCharPos = 1

	do while iCharPos<=len(strRawText)
    	'** Only include valid characters **
		chrThisChar=mid(strRawText,iCharPos,1)

		if instr(StrClean,chrThisChar)<>0 then 
			strCleanedText=strCleanedText & chrThisChar
		elseif bolHighOrder then
			'** Fix to allow accented characters and most high order bit chars which are harmless **
			if asc(chrThisChar)>=191 then strCleanedText=strCleanedText & chrThisChar
		end if

      	iCharPos=iCharPos+1
	loop       
  
	cleanInput = trim(strCleanedText)

end function


'** Counts the number of : in a string.  Used to validate the basket fields
public function countColons(strSource)

	if strSource="" then
		iNumCol=0
	else
		iCharPos=1
		iNumCol=0
		
		do while iCharPos<>0
			iCharPos=instr(iCharPos+1,strSource,":")
			if iCharPos<>0 then iNumCol=iNumCol+1
		loop 
	end if
	
	countColons = iNumCol

end function


'** Checks to ensure a Basket Field is correctly formatted **
public function validateBasket(strThisBasket)

	bolValid=false
	if len(strThisBasket)>0 and (instr(strThisBasket,":")<>0) then
		iRows=left(strThisBasket,instr(strThisBasket,":")-1)
		if isnumeric(iRows) then
			iRows=Cint(iRows)
			if countColons(strThisBasket)=((iRows*5)+iRows) then bolValid=true
		end if
	end if	
	validateBasket = bolValid

end function


'** ASP has no inbuild URLDecode function, so here's on in case we need it **
public function URLDecode(strString)
	For lngPos = 1 To Len(strString)
    	If Mid(strString, lngPos, 1) = "%" Then
            strUrlDecode = strUrlDecode & Chr("&H" & Mid(strString, lngPos + 1, 2))
            lngPos = lngPos + 2
        elseif Mid(strString, lngPos, 1) = "+" Then
            strUrlDecode = strUrlDecode & " "
        Else
            strUrlDecode = strUrlDecode & Mid(strString, lngPos, 1)
        End If
    Next
    UrlDecode = strUrlDecode
End function


'** There is a URLEncode function, but wrap it up so keep the code clean **
public function URLEncode(strString)
	strEncoded=Server.URLEncode(strString)
	URLEncode=strEncoded
end function


'** Base 64 encoding routine.  Used to ensure the encrypted "crypt" field **
'** can be safely transmitted over HTTP **
public function base64Encode(strPlain)
	if len(strPlain) = 0 then
		base64Encode = ""
		exit function
	end if

	'** Work out rounded down multiple of 3 bytes length for the unencoded text **
	iBy3 = (len(strPlain) \ 3) * 3
	strReturn=""

	'** For each 3x8 byte chars, covert them to 4x6 byte representations in the Base64 map **
	iIndex = 1
	do while iIndex <= iBy3
		iFirst  = asc(mid(strPlain, iIndex+0, 1))
		iSecond = asc(mid(strPlain, iIndex+1, 1))
		iiThird  = asc(mid(strPlain, iIndex+2, 1))
		strReturn = strReturn & arrBase64EncMap((iFirst \ 4) AND 63 )
		strReturn = strReturn & arrBase64EncMap(((iFirst * 16) AND 48) + ((iSecond \ 16) AND 15 ) )
		strReturn = strReturn & arrBase64EncMap(((iSecond * 4) AND 60) + ((iiThird \ 64) AND 3 ) )
		strReturn = strReturn & arrBase64EncMap(iiThird AND 63)
		iIndex = iIndex + 3
	loop

	'** Handle any trailing characters not in groups of 3 **
	'** Extend to multiple of 3 characters using = signs as per RFC **
	if iBy3 < len(strPlain) then
		iFirst  = asc(mid(strPlain, iIndex+0, 1))
		strReturn = strReturn & arrBase64EncMap((iFirst \ 4) AND 63 )
		if (len(strPlain) MOD 3 ) = 2 then
			iSecond = asc(mid(strPlain, iIndex+1, 1))
			strReturn = strReturn & arrBase64EncMap(((iFirst * 16) AND 48) + ((iSecond \ 16) AND 15 ) )
			strReturn = strReturn & arrBase64EncMap((iSecond * 4) AND 60)
		else
			strReturn = strReturn & arrBase64EncMap((iFirst * 16) AND 48)
			strReturn = strReturn & "="
		end if
		strReturn = strReturn & "="
	end if

	'** Return the encoded result string **
	base64Encode = strReturn
end function


' ** Base 64 decoding function **
public function base64Decode(strEncoded)
	if len(strEncoded) = 0 then
		base64Decode = ""
		exit function
	end if

	'** Base 64 encoded strings are right padded to 3 character multiples using = signs **
	'** Work out the actual length of data without the padding here **
	iRealLength = len(strEncoded)
	do while mid(strEncoded, iRealLength, 1) = "="
		iRealLength = iRealLength - 1
	loop
	
	'** Non standard extension to Base 64 decode to allow for + sign to space character substitution by **
	'** some web servers. Base 64 expects a +, not a space, so convert vack to + if space is found **
	do while instr(strEncoded," ")<>0
		strEncoded=left(strEncoded,instr(strEncoded," ")-1) & "+" & mid(strEncoded,instr(strEncoded," ")+1)
	loop

	strReturn=""
	'** Convert the base 64 4x6 byte values into 3x8 byte real values by reading 4 chars at a time **
	iBy4 = (iRealLength\4)*4
	iIndex = 1
	do while iIndex <= iBy4
		iFirst  = arrBase64DecMap(asc(mid(strEncoded, iIndex+0, 1)))
		iSecond = arrBase64DecMap(asc(mid(strEncoded, iIndex+1, 1)))
		iThird  = arrBase64DecMap(asc(mid(strEncoded, iIndex+2, 1)))
		iFourth = arrBase64DecMap(asc(mid(strEncoded, iIndex+3, 1)))
		strReturn = strReturn & chr( ((iFirst * 4) AND 255) +   ((iSecond \ 16) AND 3))
		strReturn = strReturn & chr( ((iSecond * 16) AND 255) + ((iThird \ 4) AND 15) )
		strReturn = strReturn & chr( ((iThird * 64) AND 255) +  (iFourth AND 63) )
		iIndex = iIndex + 4
	loop
 
	'** For non multiples of 4 characters, handle the = padding **
	if iIndex < iRealLength then
		iFirst  = arrBase64DecMap(asc(mid(strEncoded, iIndex+0, 1)))
		iSecond = arrBase64DecMap(asc(mid(strEncoded, iIndex+1, 1)))
		strReturn = strReturn & chr( ((iFirst * 4) AND 255) +   ((iSecond \ 16) AND 3))
		if iRealLength MOD 4 = 3 then
			iThird = arrBase64DecMap(asc(mid(strEncoded,iIndex+2,1)))
			strReturn = strReturn & chr( ((iSecond * 16) AND 255) + ((iThird \ 4) AND 15) )
		end if
	end if

	base64Decode = strReturn
end function


' ** The SimpleXor encryption algorithm. **
' ** NOTE: This is a placeholder really.  Future releases of Form will use AES **
' ** This simple function and the Base64 will deter script kiddies and prevent the "View Source" type tampering **
public function simpleXor(strIn,strKey)
	if len(strIn)=0 or len(strKey)=0 then
		SimpleXor=""
		exit function
	end if
	
	iInIndex=1
	iKeyIndex=1
	strReturn=""
	
	'** Step through the plain text source XORing the character at each point with the next character in the key **
	'** Loop through the key characters as necessary **
	do while iInIndex<=len(strIn)
		strReturn = strReturn & chr( asc(mid(strIn,iInIndex,1)) XOR asc(mid(strKey,iKeyIndex,1)) )
		iInIndex = iInIndex + 1
		if iKeyIndex = len(strKey) then iKeyIndex = 0
        iKeyIndex = iKeyIndex + 1
    loop
 
    simpleXor = strReturn
end function


'** The getToken function. **
'** NOTE: A function of convenience that extracts the value from the "name=value&name2=value2..." reply string **
'** Does not use the split function because returned values can contain & or =, so it looks for known names and **
'** removes those, leaving only the field selected ** 
public function getToken(strList,strRequired)
	dim arrTokens
  	arrTokens = array("Status","StatusDetail","VendorTxCode","VPSTxId", _
  				"TxAuthNo","Amount","AVSCV2","AddressResult","PostCodeResult", _
				"CV2Result","GiftAid","3DSecureStatus","CAVV","AddressStatus", _
				"CardType","Last4Digits","PayerStatus","CardType")

	'** If the toekn we're after isn't in the list, return nothing **
	if instr(strList,strRequired+"=")=0 then
	    getToken=""
    	exit function
	else
		'** The token is present, so ignore everything before it in the list **    
		strTokenValue=mid(strList,instr(strList,strRequired)+len(strRequired)+1)
    
		'** Strip off all remaining tokens if they are present **
		iIndex = LBound(arrTokens)
		do while iIndex <= UBound(arrTokens)
			if arrTokens(iIndex)<>strRequired then
				if instr(strTokenValue,"&"+arrTokens(iIndex))<>0 then 
					strTokenValue=left(strTokenValue,instr(strTokenValue,"&"+arrTokens(iIndex))-1)
				end if
			end if
			iIndex = iIndex + 1
		loop  
		
		getToken=strTokenValue
	end if
	
end function


'************ Global definitions ***************

'** Set up Base64 Encoding and Decoding Maps for when we need them **
const BASE_64_MAP_INIT ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
dim arrBase64EncMap(63)
dim arrBase64DecMap(127)
strNewLine = "<P>" & vbCRLF
for iLoop = 0 to len(BASE_64_MAP_INIT) - 1
	arrBase64EncMap(iLoop) = mid(BASE_64_MAP_INIT, iLoop + 1, 1)
next
for iLoop = 0 to len(BASE_64_MAP_INIT) - 1
	arrBase64DecMap(ASC(arrBase64EncMap(iLoop))) = iLoop
next


%>