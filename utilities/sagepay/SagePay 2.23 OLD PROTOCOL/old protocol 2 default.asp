<%
PageTitle = "Make a payment"
%>

<!--#include virtual="/assets/includes/header.asp" -->

<%
if not glSagePayEnabled then
	dbClose()
	response.redirect "/"
end if
%>

<style>
	.HomePanelLayer {
		color:white;
	}
	.header-panel {		
		color:white;
		
	}
	
	A.whitelink {
		color:white;
	}
	
	.header-title {		
		color:white;
		font-size:36pt;
		font-family:Oxygen, arial;
		text-align:left;
	}	
	
	.header-trans {
		background-image:url(/assets/images/home_panels/semi-trans.png);
		padding:20px 0 20px 0;
		xmargin-top:20px;
		
	}
	
	.header-button {
		text-align:left;
	}
	
	#slide1, #slide2, #slide3, #slide4 {
		border:0px solid red;
		padding-top:50px;
		height:200px;
	}
	
	#feature_slider {
		height:250px !important;
	}
	
	.slide {
		height:220px;
	}
	
	.header-title {
		line-height:30pt;font-size:24pt;
	}
	
	.colSpacer_LEFT {
		border-left:1px solid #f0f0f0;
		padding-left:20px;
	}
	
	.ProductGrid TD {
		font-family:arial;
		font-size:10pt;
		padding:5px;
	}
	
	.legible {
		font-family:arial;
		font-size:8pt;
	}
	
	.basketAddButton {
		border:1px solid red;
	}
	
	#CheckoutTable {
		border:1px solid silver;
	}
	
	#CheckoutTable TD {
		font-family:arial;
		font-size:9pt;
		padding:7px;
	}
	
	.CheckoutImage {
		width:50px;
	}
	
	.CheckoutQty {
		width:40px;
		text-align:right;
	}
	
</style>

<%

FormAction = request("FormAction")
ImportArea = request("ImportArea")
ImportOptions = request("ImportOptions")

ImportMax = 500
DelimChar = "~"


if FormAction = "IxMPORT_CHECK" or FormAction = "IMPORxT_SUBMIT" then
	
end if


RetryAmount = cleannum(request("RetryAmount"))
'jh "RetryAmount = " & RetryAmount

%>



<link rel="stylesheet" href="css/index.css" type="text/css" media="screen" />

<style>
	#ImportArea {
		width:200px;
		height:250px;
	}
</style>

	<div class="container">      

		  <div class="row">      
			<div class="span12 spacer10">
			
				
	
			
				<h2><%=lne("page_title_sagepay")%><br><small><%=lne("sage_pay_explained")%></small></h2>
			
				<hr>
			</div>
		</div>	
		
<%

if not AcDetails.SagePayUser then
%>

		  <div class="row">      
			<div class="span12 spacer10">
							
				<p>Sorry, your account is not configured to make payments via SagePay, please contact us</p>
			
				<hr>
			</div>
		</div>	
<%
else


	'jh AcDetails.SagePayUser
	'jh AcDetails.CostBand

	'jhInfo "SagePayCurrency = " & SagePayCurrency

	ThisCurrentBalance = AcDetails.CurrentBalance
	
	set BasketDetails = new ClassBasketDetails

	ThisBasketTotal = BasketDetails.GrandTotal

	BalanceAndBasketTotal = ThisBasketTotal + ThisCurrentBalance

	if ThisBasketTotal = 0 then BasketDisStr = "DISABLED"
	if ThisCurrentBalance = 0 then BalanceDisStr = "DISABLED"
	if BalanceAndBasketTotal = 0 and ThisCurrentBalance = 0 then CombinedDisStr = "DISABLED"

		SagePayCurrency = AcDetails.SagePayCurrency

		'jhInfo "SagePayCurrency = " & SagePayCurrency
		
		if SagePayCurrency = "" then
			SagePayCurrency = "GBP"
			SagePayNonNative = true ' not the same currency as asge pay - therefore cant pay statement amount etc automatically as currencies differ...
			BalanceDisStr = "DISABLED"
			BasketDisStr = "DISABLED"
			CombinedDisStr = "DISABLED"
			
			DailyRate = AcDetails.DailyRate
			
			if ThisCurrentBalance > 0 then
				BalanceConverted = (ThisCurrentBalance / DailyRate) 
				BalanceExchangeStr = "<span class=""label"">Approximately " & dispCurrency_PLAIN(BalanceConverted) & " GBP</span> "			
			end if
			
			if ThisBasketTotal > 0 then
				BasketConverted = (ThisBasketTotal / DailyRate) 
				BasketExchangeStr = "<span class=""label"">Approximately " & dispCurrency_PLAIN(BasketConverted) & " GBP</span> "			
			end if		
			
			
			if BalanceAndBasketTotal > 0 then
				BalanceAndBasketConverted = (BalanceAndBasketTotal / DailyRate) 
				BalanceAndBasketExchangeStr = "<span class=""label"">Approximately " & dispCurrency_PLAIN(BalanceAndBasketConverted) & " GBP</span> "			
			end if			
			
		end if
		
		'jh "SagePayNonNative = " & SagePayNonNative 

	%>		
			

			 
			 <div class=row>
				
				<div class="span6">	
				

					
					<%
					if SagePayNonNative then
						jhErrorNice "Payment must be made in sterling (GBP)", "Sorry, we are unable to accept online payment in your normal currency (" & AcDetails.CurrencyName & ") - all payments must be made in GBP.<br>Approximate values are displayed below. Please contact us to get an exact figure if required."
					else
	%>
						<h4 class=spacer40><%=lne("sagepay_heading_1")%></h4>
						
						<div id=xSagePayAjax>
						
						</div>						
						
	<%				
					end if
					%>
					
					<h5><%=lne("sagepay_pay_balance")%><small>&nbsp;&nbsp;<a class=OrangeLink href="/utilities/account/reports/statement/"><%=lne("label_view_statement")%></a></small></h5>
					<div class=spacer20>
						<button <%=BalanceDisStr%> data-pay-type="BALANCE" data-pay-amount="<%=ThisCurrentBalance%>" class="spOption btn btn-primary"><strong><%=DispCurrency(ThisCurrentBalance)%></strong></button> <%=BalanceExchangeStr%>
						
					</div>
					
					<hr>
					<h5><%=lne("sagepay_pay_basket")%><small>&nbsp;&nbsp;<a class="OrangeLink" href="/checkout/"><%=lne("label_view_basket")%></a></small></h5>
					<div class=spacer20>
						<button <%=BasketDisStr%> data-pay-type="BASKET" data-pay-amount="<%=ThisBasketTotal%>" class="spOption btn btn-primary"><strong><%=DispCurrency(ThisBasketTotal)%></strong></button> <%=BasketExchangeStr%>
						
					</div>
					
											
					<hr>
					
					<h5><%=lne("sagepay_pay_balance_and_basket")%></h5>
					<p>
						<button <%=CombinedDisStr%> data-pay-type="BALANCE_AND_BASKET" data-pay-amount="<%=BalanceAndBasketTotal%>" class="spOption btn btn-primary"><strong><%=DispCurrency(BalanceAndBasketTotal)%></strong></button> <%=BalanceAndBasketExchangeStr%>
					</p>					
					
					<hr>
					
					<p>
						<h5><%=lne("sagepay_pay_other_amount")%></h5>
						<div class=spacer20><small><%=lne("sagepay_pay_other_amount_explained")%> </small></div>
						<div class="input-prepend">
						  <span class="add-on"><span class="label label-info"><%=SagePayCurrency%></span></span>
						  <input class="span2" id="SagePayOtherAmount" type="text" placeholder="<%=lne_NOEDIT("label_enter_amount")%>" value="<%=RetryAmount%>">
						</div>	
						<div>
							<button data-pay-type="OTHER" class="spOption btn btn-primary"><%=lne_NOEDIT("label_pay_this_amount")%></button>
						</div>
										
					</p>								
								
					<div class=spacer20></div>		
					
			
					
				</div>			
				
				<div class="span5 offset1">	

					<%=getContent("sagepay_explained")%>
					
					<div class=spacer40></div>
					
					<p>
						<a href="/contact/" class="btn btn-warning">Having problems? Contact us</a>
					</p>
					
					<div class=spacer40></div>
					
					<div class=pull-right>
						<a target="_BLANK" href="<%=glSagePayURL%>"><img style="height:70px;" title="Secured by SagePay" src="/assets/images/shop/SecuredBy_Solo.png"></a>
					</div>					
					
				</div>			
				
			 </div>


			 
		
	
<%
end if
%>	

	</div>
	
	<div class=spacer40></div>			
			
			
<div id="SagePayModal" class="modal hide fade">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
    <h3>Connecting to SagePay</h3>
  </div>
  <div class="modal-body">
    <div id="SagePayAjax">
		...
	</div>
	<div class=pull-right>
		<a target="_BLANK" href="<%=glSagePayURL%>"><img style="height:70px;" title="Secured by SagePay" src="/assets/images/shop/SecuredBy_Solo.png"></a>
	</div>		
  </div>
</div>			
		

<!--#include virtual="/assets/includes/footer.asp" -->


<script>
	$(".spOption").click(function () {
		//alert("CLICKED");
		
		var payType = $(this).data("pay-type")
		var PaymentAmount = ""
		
		if (payType != 'OTHER') {
			PaymentAmount = $(this).data("pay-amount")
		} else {
			PaymentAmount = $("#SagePayOtherAmount").val()
		}
		
		
		
		$.ajax({

			url: "sagepay_server.asp?PaymentAmount=" + PaymentAmount, 
			cache: false
			}).done(
				function( html ) {
					
				$("#SagePayAjax").html(html);
				$('#SagePayAjax').show(100, function() {
					
					// Animation complete.
					document.forms.FormSagePay.submit();
				});					
			});
			
		$('#SagePayModal').modal('show')
		
	});


</script>

<%

dbClose()

%>