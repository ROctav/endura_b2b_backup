﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SagePay.IntegrationKit.Messages;




public partial class Form_Result : System.Web.UI.Page
{
    public IFormPaymentResult PaymentStatusResult  { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["crypt"] != null && !string.IsNullOrEmpty(Request.QueryString["crypt"]))
        {
            SagePayFormIntegration sagePayFormIntegration = new SagePayFormIntegration();
            PaymentStatusResult = sagePayFormIntegration.ProcessResult(Request.QueryString["crypt"]);

        }
    }
	
	public void jh(string pstring)
	{
		Response.Write("<div style=\"font-family:arial;font-size:9pt;margin-bottom:3px;padding:5px;border-radius:3px;color:black;background:gold;\">" + pstring + "</div>");
	}

	public void jhInfo(string pstring)
	{
		Response.Write("<div style=\"font-family:arial;font-size:9pt;margin-bottom:3px;padding:5px;border-radius:3px;color:white;background:limegreen;\">" + pstring + "</div>");
	}	
	
	public string validstr(string pstring)
	{
		string retVal = pstring.Replace("'", "''");
		return retVal;
	}		
	
	public bool IsNullOrWhiteSpace(string value)
		{
			if (value != null) {
				for (int i = 0; i < value.Length; i++) {
					if (!char.IsWhiteSpace(value[i])) {
						return false;
					}
				}
			}
			return true;
		}	
	

}



	