<%
PageTitle = "Ranging Proposal Summary Report"
%>

<!--#include virtual="/assets/includes/header.asp" -->
<!--#include virtual="/assets/includes/ranging_functions.asp" -->

<style>
	.OfferValid {
		font-weight:bold;
		color:green;
		padding:5px;
		background:lightgreen;
	}
</style>

<%
function isAccessory(pTypes)

	dim c, CheckTypeID
	isAccessory = false
	
	for c = 0 to sptc
	
		CheckTypeID = sptarr(0,c)
		if instr(1, pTypes, "|" & CheckTypeID & "|") > 0 and sptarr(4,c) = "ALL_ACCESSORIES" then
			isAccessory = true
			exit function
		end if
	
	next
	
	'jh "pTypes = " & pTypes 
	if instr(1, pTypes, "|51|") > 0 then
		isAccessory = true
	end if

end function

%>
<style>

.ReportTotal {
	font-size:large;
	font-weight:bold;
}

.BudgetTotal {
	xfont-size:large;
	font-weight:bold;
}

.BudgetError {
	color:red;
}

.modal-body {
    max-height: 80vh; //Sets the height to 80/100ths of the viewport.
}

.SizeDiv {
	background:#f0f0f0;
	display:inline-block;
	width:70px;
	padding:4px;
	border-radius:3px;
	margin-right:5px;
	text-align:center;
}

	.leftFilter {
		font-family:arial !important;
		font-size:9pt !important;
	}
	.leftNavHolder {
		border-radius:3px;
		background:#f0f0f0;
		padding:10px;
		margin-bottom:5px;
	}
	
	.accordion-inner {
		padding:5px 15px;
	}
	
	.VariantTile {
		display:inline-block;
		width:20px;
		height:5px;
		border:1px solid #333;
		margin-right:3px;
	}
	.VariantTileImage {
		width:20px;
		height:5px;
	}
	
	.pointer {
		cursor:pointer;
	}
	
	.TopNavSep {
		border-bottom:1px solid #f0f0f0;padding:1px 0;
	}
	
	.NavHeading {
		color:orange;
		font-weight:bold;
		padding:10px 0 3px 0;
		font-size:110%;
	}
	
	.modal.large {
		width: 80%; /* respsonsive width */
		margin-left:-40%; /* width/2) */ 
	}	
	
	.colorwayQty {
		padding:2px !important;
		width:20px !important;
		border:1px solid silver;
	}
	
</style>

<%

RangingID = cleannum(request("RangingID"))

if RangingID = "" then RangingID = session("RangingID")

if RangingID <> "" then
	sql = "SELECT * "
	sql = sql & "FROM B2B_Ranging WHERE RangingID = 0" & RangingID & " "
	'jh sql
	if not isAdminUser then
		sql = sql & "AND AcCode = '" & AcDetails.AcCode & "' "
		sql = sql & "AND AllowDealerView = 1 "
	end if	
	set rs = db.execute(sql)
	
	if not rs.eof then
	
		RangingTitle = rs("RangingTitle")
		RangingDescription = rs("RangingDescription")
		session("RangingID") = RangingID
		
		RangingBudget = convertnum(rs("RangingBudget"))
		
		Budget_ROAD = convertnum(rs("Budget_ROAD"))
		Budget_MOUNTAIN = convertnum(rs("Budget_MOUNTAIN"))
		Budget_A2B = convertnum(rs("Budget_A2B"))
		Budget_MEN = convertnum(rs("Budget_MEN"))
		Budget_WOMEN = convertnum(rs("Budget_WOMEN"))
		Budget_KIDS = convertnum(rs("Budget_KIDS"))
		
		Budget_ACC = convertnum(rs("Budget_ACC"))
		
		BenchmarkAcCode = rs("AcCode")
		BenchmarkStartDate = sqlDate(rs("BenchmarkStartDate"))
		BenchmarkEndDate = sqlDate(rs("BenchmarkEndDate"))		
		
		AllowDealerView = convertnum(rs("AllowDealerView"))
		AllowDealerChanges = convertnum(rs("AllowDealerChanges"))
		AllowDealerPrices = convertnum(rs("AllowDealerPrices"))
		AllowDealerBudget = convertnum(rs("AllowDealerBudget"))		
		
		RangingStatusID = rs("RangingStatusID")
		if isAdminUser() and RangingStatusID = 0 then AllowAdminChanges = 1 else AllowAdminChanges = 0
		if RangingStatusID > 0 then AllowDealerChanges = 0
	
		NewSeasonCodes = ucase(trim("" & rs("NewSeasonCodes")))
		
		'jh "NewSeasonCodes = " & NewSeasonCodes
	end if
	
	rsClose()
end if

RangingID = session("RangingID")

if RangingID = "" then
	jhError "NO RANGING PROPOSAL SELECTED - RETURN TO MAIN ADMIN PAGE"
	FormAction = ""
	dbClose()
	response.redirect "/admin/ranging/"
end if

dbConnect()

sql = "SELECT spt.ProductTypeID, spt.ProductTypeName, spt.ProductTypeName_FULL, nc.NavClassificationName, NavClassificationTopLevel, spt.NavClassificationID "
sql = sql & "FROM SML_ProductTypes spt "
sql = sql & "INNER JOIN SML_NavClassifications nc ON nc.NavClassificationID = spt.NavClassificationID "
sql = sql & "WHERE ISNULL(NavClassificationTopLevel, '') <> '' "
sql = sql & "ORDER BY NavClassificationZOrder "

x = getrs(sql, sptArr, sptc)

'jh "sptc = " & sptc

FormAction = request("FormAction")

'jhInfo "FormAction = " & FormAction

if FormAction = "SAVE_RANGING" then
	VariantCount = convertnum(request("VariantCount"))
	'jh "VariantCount = " & VariantCount
		
	for c = 1 to VariantCount
	
		ThisProductID = cleannum(request("ProductID_" & c))
		ThisProductColorwayID = cleannum(request("CWAyID_" & c))
		
		'jhInfo "ThisProductID = " & ThisProductID
		'jhInfo "ThisProductColorwayID = " & ThisProductColorwayID		
		
		'sql = "DELETE FROM B2B_RangingProducts WHERE RangingID = 0" & RangingID & " AND ProductID = 0" & ThisProductID
		'jh sql
		'db.execute(sql)
		sql = "DELETE FROM B2B_RangingColorways WHERE RangingID = 0" & RangingID & " AND ProductColorwayID = 0" & ThisProductColorwayID 
		'jh sql
		db.execute(sql)		
		sql = "DELETE FROM B2B_RangingSKUS WHERE RangingID = 0" & RangingID & " AND ProductColorwayID = 0" & ThisProductColorwayID
	'	jh sql
		db.execute(sql)		
				
		ThisCWayQty = cleannum(request("CWAyQty_" & c))
		'jh "ThisCWayQty = " & ThisCWayQty
		SKUTotal = 0
		
		SKUSAdded = false
		for z = 1 to 20
			ThisSKU = request("SKU_" & c & "_" & z)
			ThisQty = convertNum(request("Qty_" & c & "_" & z))
			if ThisQty > 0 and ThisSKU <> "" then
				SKUSAdded = true
			end if
		next		
		
		if ThisCWayQty <> "" and ThisCWayQty <> "0" and not SKUSAdded then
			'jhInfo "DO CWAY QTYS"
			
			sql = "INSERT INTO B2B_RangingColorways (RangingID, ProductID, ProductColorwayID, ColorwayQty) VALUES ("
			sql = sql & "0" & RangingID & ", "
			sql = sql & "0" & ThisProductID & ", "
			sql = sql & "0" & ThisProductColorwayID & ", "
			sql = sql & "0" & ThisCWayQty & ") "
	
			'jh sql
			db.execute(sql)				
			
		else
			'jh "DO SKU QTYS"
			SomeAdded = false
			for z = 1 to 20
				ThisSKU = request("SKU_" & c & "_" & z)
				ThisQty = convertNum(request("Qty_" & c & "_" & z))
				
				if ThisQty > 0 and ThisSKU <> "" then
					SomeAdded = true
					SKUTotal = SKUTotal + ThisQty
					sql = "INSERT INTO B2B_RangingSKUS (RangingID, ProductID, ProductColorwayID, SKU, SKUQty) VALUES ("
					sql = sql & "0" & RangingID & ", "
					sql = sql & "0" & ThisProductID & ", "
					sql = sql & "0" & ThisProductColorwayID & ", "
					sql = sql & "'" & ThisSKU & "', "
					sql = sql & "0" & ThisQty & " "
					sql = sql & ")"
					
				'	jh sql
					db.execute(sql)
				end if
			
			next			
			
			if SomeAdded then

				sql = "INSERT INTO B2B_RangingColorways (RangingID, ProductID, ProductColorwayID, ColorwayQty) VALUES ("
				sql = sql & "0" & RangingID & ", "
				sql = sql & "0" & ThisProductID & ", "
				sql = sql & "0" & ThisProductColorwayID & ", "
				sql = sql & "0" & SKUTotal & ") "
		
				'jh sql
				db.execute(sql)			
			
			end if
		end if
	
	next
	
	sql = "INSERT INTO B2B_RangingProducts (RangingID, ProductID, UnitQty) "
	sql = sql & "SELECT RangingID, ProductID, SUM(ColorwayQty) AS SKUSum "
	sql = sql & "FROM B2B_RangingColorways "
	sql = sql & "WHERE RangingID = 0" & RangingID & " AND NOT EXISTS (SELECT * FROM B2B_RangingProducts WHERE RangingID = 0" & RangingID & " AND ProductID = B2B_RangingColorways.ProductID) "
	sql = sql & "GROUP BY RangingID, ProductID "
	'jh sql
	db.execute(sql)		
	
	if isjh() or glBackupRangingSummarys then
		call processRangingBackup(RangingID) 
	end if
	
	
end if

VariantCount = 0
%>

    <div id="">
	
		
	
		<div id="FilterOutput">
		
		</div>
		

		
    </div>

</div>

<%



%>

<div class="container spacer40">	
	
	
<div class=row>
	<div class=span12>
		

	
	
	
<%


'jhstop
%>			
	</div>
</div>

<div class=row>
		<div class=span12>		
		
			<hr>
				<div class=spacer20>
					<span class="label label-inverse"><%=AcDetails.AcCode%></span> <span class=legible><%=AcDetails.CompanyName%></span>
					<h3><%=RangingTitle%><br><small><%=lne("ranging_summary_report")%></small></h3>
					<%
					if isAdminUser() then
					%>
						<a class=OrangeLink href="/admin/ranging/">Ranging Admin</a> | 
						<a class=OrangeLink href="/admin/ranging/ranging-detail/?RangingID=<%=RangingID%>"><i class="icon icon-cog"></i></a> | 
					<%
					end if
					%>					
					<a class=OrangeLink href="/utilities/ranging/"><%=lne("ranging_back_to_proposal")%></a>
				</div>				
				
				<div id="AjaxFeedback">
				
				</div>
			<hr>
		
		
		</div>
	</div>
	
	<div class="row">
		
		
		<div class=span12>

<%

	CheckCatOrder = trim("" & request("CheckCatOrder"))
	'jh "CheckCatOrder = " & CheckCatOrder
	CheckHideZeros = trim("" & request("CheckHideZeros"))
	
	SummarySearchVal = trim("" & request("SummarySearchVal"))
	FilterEndUse = convertnum(request("FilterEndUse"))
	'jh "FilterEndUse = " & FilterEndUse
	FilterDemoGraphic = convertnum(request("FilterDemoGraphic"))
	'jh "FilterDemoGraphic = " & FilterDemoGraphic	
	
	FilterNavClassification = convertnum(request("FilterNavClassification"))
	
	'jh request("FilterProductType")
	FilterProductType = convertnum(request("FilterProductType"))
	
	FilterColor = convertnum(request("FilterColor"))
	
	FormAction = request("FormAction")
	'jh "FormAction = " & FormAction

sql = "SELECT SimpleColorID, SimpleColorName FROM SML_ColorwayList_SIMPLE ORDER BY SimpleColorName "
x = getrs(sql, SimpleColorArr, SimpleColorC)
'jh "SimpleColorC = " & SimpleColorC

sql = "SELECT ProductTypeID, ISNULL(ProductTypeName_FULL, ProductTypeName), ParentID AS PRoductTypeName FROM SML_ProductTypes ORDER BY PRoductTypeZORder "
x = getrs(sql, ProductTypeArr, ProductTypeC)
'jh "ProductTypeC = " & ProductTypeC

	'jh "AcDetails.CostBand = " & AcDetails.CostBand
	SelCostBand = AcDetails.CostBand

	sql = "SELECT sp.ProductID, sp.ProductCode, sp.ProductName, MaxPrice, VariantCode, ColorName, DefaultProductImage, spc.ColorCode, SimpleColorID, ProductName_TRANSLATED "
	sql = sql & ", rp.RangingID, spc.ProductColorwayID, sp.Categories, sp.Demographics, ISNULL(spt.ProductTypeName_FULL, spt.ProductTypeName) AS SPTName, Band" & SelCostBand & ", SKUQty, sz.SizeName, Categories, Demographics, ss.SKU, rc.ColorwayQty, sp.ProductTypes, spt.NavClassificationID, nav.NavClassificationName, sp.ClassificationTags, ProductSeason, MaxPrice_FO, pbl." & SelCostBand & "_FO "
	sql = sql & "FROM SML_Products sp "
	sql = sql & "INNER JOIN B2B_RangingProducts rp ON rp.ProductID = sp.ProductID "
	sql = sql & "INNER JOIN SML_SKUS ss ON ss.ProductID = sp.ProductID "
	sql = sql & "INNER JOIN SML_SizeList sz ON sz.SizeID = ss.SizeID "
	sql = sql & "INNER JOIN SML_SKUStatus sta on sta.SKUStatusID = ss.SKUStatusID "
	sql = sql & "INNER JOIN SML_ProductTypes spt ON spt.ProductTypeID = sp.ProductTypeID "
	sql = sql & "INNER JOIN SML_ProductColorways spc ON spc.ProductColorwayID = ss.ProductColorwayID "
	sql = sql & "LEFT JOIN ("
	sql = sql & "SELECT * FROM B2B_RangingColorways WHERE RangingID = 0" & RangingID & " "
	sql = sql & ") rc ON rc.ProductColorwayID = spc.ProductColorwayID "
	sql = sql & "LEFT JOIN (SELECT ProductID, ProductName AS ProductName_TRANSLATED FROM SML_ProductTranslations WHERE LAnguageID = 0" & AcDetails.SelectedLanguageID & ") tra ON tra.ProductID = sp.ProductID "
	sql = sql & "LEFT JOIN (SELECT ProductID, MAX(Band" & SelCostBand & ") AS MaxPrice, MAX(" & SelCostBand & "_FO) AS MaxPrice_FO FROM END_PriceBandLookup INNER JOIN SML_SKUS ss ON ss.SKU = END_PriceBandLookup.StockCode GROUP BY ProductID) mp ON mp.ProductID = sp.ProductID "	
	'sql = sql & "LEFT JOIN (SELECT ProductID, MAX(" & SelCostBand & "_FO) AS MaxPrice_FO FROM END_PriceBandLookup rl INNER JOIN SML_SKUS ss ON ss.SKU = rl.StockCode GROUP BY ProductID) fop ON fop.ProductID = sp.ProductID "		
	sql = sql & "INNER JOIN END_PriceBandLookup pbl ON pbl.StockCode = ss.SKU "
	sql = sql & "LEFT JOIN ("
	sql = sql & "SELECT * FROM B2B_RangingSKUS  WHERE RangingID = 0" & RangingID & " "
	sql = sql & ") rs ON rs.SKU = ss.SKU "
	sql = sql & "LEFT JOIN SML_NavClassifications nav ON nav.NavClassificationID = spt.NavClassificationID "
	
	'sql = sql & "LEFT JOIN SML_ColorwayList_SIMPLE_MATRIX mx ON mx.ColorCode = spc.ColorCode "
		
	
	sql = sql & "WHERE 1 = 1 "
	
	sql = sql & "AND rp.RangingID = 0" & RangingID & " "
	sql = sql & "AND sta.B2BRangingVisible = 1 "
	sql = sql & "AND (pbl." & SelCostBand & "_FO > 0 or pbl." & SelCostBand & "_FON > 0 or MaxPrice > 0) "
	sql = sql & "AND SKUQty > 0  "
	
	csvSql = sql
	
	if CheckHideZeros = "1" then
		sql = sql & "AND rc.ColorwayQty > 0 "	
	end if			
	
	if FilterEndUse <> 0 then
		sql = sql & "AND sp.Categories LIKE '%|" & FilterEndUse & "|%' "
	end if	
	if FilterDemoGraphic <> 0 then
		sql = sql & "AND sp.Demographics LIKE '%|" & FilterDemoGraphic & "|%' "
	end if	
	if FilterColor <> 0 then
		sql = sql & "AND spc.ColorCode IN (SELECT ColorCode FROM SML_ColorwayList_SIMPLE_MATRIX WHERE SimpleColorID = 0" & FilterColor & ")"
	end if
	if FilterProductType <> 0 then
		'sql = sql & "AND sp.ProductTypes LIKE '%|" & FilterProductType & "|%' "
	end if
	if FilterNavClassification <> 0 then
		sql = sql & "AND spt.NavClassificationID = 0" & FilterNavClassification
	end if	
	
	if SummarySearchVal <> "" then
		sql = sql & "AND (sp.ProductCode LIKE '%" & validstr(SummarySearchVal) & "%' OR spt.ProductTypeName LIKE '%" & validstr(SummarySearchVal) & "%' OR sp.ProductName LIKE '%" & validstr(SummarySearchVal) & "%') "
	end if
	
	if CheckCatOrder = "1" then	
		sql = sql & "ORDER BY sp.CatalogueZOrder, nav.NavClassificationZOrder, spt.ProductTypeZOrder, MaxPrice DESC, sp.ProductID, spc.DefaultProductImage DESC, spc.VariantCode, sz.SizeZOrder "
		csvSql = csvSql & "ORDER BY sp.CatalogueZOrder, nav.NavClassificationZOrder, MaxPrice DESC, sp.ProductID, spc.DefaultProductImage DESC, spc.VariantCode, sz.SizeZOrder "
	else
		sql = sql & "ORDER BY nav.NavClassificationZOrder, spt.ProductTypeZOrder, MaxPrice DESC, sp.ProductID, spc.DefaultProductImage DESC, spc.VariantCode, sz.SizeZOrder "
		csvSql = csvSql & "ORDER BY nav.NavClassificationZOrder, MaxPrice DESC, sp.ProductID, spc.DefaultProductImage DESC, spc.VariantCode, sz.SizeZOrder "
	end if	
	
	jhAdmin sql
	
	'jhstop 
	
	x = getrs(sql, RepArr, RepC)
	
	'jh "RepC = " & RepC
	
	
sql = "SELECT n.RangingID, n.AdminNotes, n.DealerNotes, n.AdminUpdatedDate, n.DealerUpdatedDate, n.ProductID, sp.ProductCode , sp.ProductName "
sql = sql & "FROM B2B_Ranging r "
sql = sql & "INNER JOIN B2B_RangingNotes n ON r.RangingID = n.RangingID "
sql = sql & "INNER JOIN SML_Products sp ON sp.ProductID = n.ProductID "
sql = sql & "INNER JOIN B2B_RangingProducts rp ON rp.RangingID = r.RangingID AND rp.ProductID = sp.ProductID "

sql = sql & "WHERE r.RangingID = 0" & RangingID & " "

'jhInfo sql
x = getrs(sql, notearr, notec)	
	
	
	if isjh() and 1 = 0 then
		jhAdmin csvSql
	
		set csvls = new strConcatStream
		
		csvls.add "CURRENCY~GBP" & vbcrlf
		
		csvls.add "CLASSIFICATION~ROAD~MOUNTAIN~A2B~OTHER~MEN~WOMEN~KIDS" & vbcrlf
		
		x = getrs(csvSql, csvArr, csvC)
		
		for c = 0 to csvc
			
			ThisProductType = csvArr(14,c)
			ThisID = csvArr(0,c)
			ThisCode = csvArr(1,c)
			ThisVariant = csvArr(4,c)
			ThisName = trim("" & csvArr(9,c))
			if ThisName = "" then ThisName = "" & csvArr(2,c)
			ThisColor = csvArr(5,c)
			ThisSKU = csvArr(20,c)
			ThisSize = csvArr(17,c)
			ThisPrice = convertnum(csvArr(15,c))
			
			ThisImg = getImageDefault(ThisCode, ThisVariant)
			
			if ThisProductType <> LastProductType then
								
				csvls.add "TYPE~" & ThisProductType & vbcrlf
				
				LastProductType = ThisProductType
			end if
			
			if ThisVariant <> LastVariant then
								
				
				
				LastVariant = ThisVariant
				SizeList = ""
				QtyList = ""
				VariantTotalQty = 0
				VariantTotalValue = 0
				
				for z = c to csvC
				
					if ThisVariant <> csvArr(4,z) then exit for
					
					SizeList = SizeList & csvArr(17,z) & "|"
					QtyList = QtyList & convertNum(csvArr(16,z)) & "|"
					VariantTotalQty = VariantTotalQty + convertNum(csvArr(16,z))
					
					
				next
				
				VariantTotalValue = VariantTotalQty * ThisPrice
				
				if right(SizeList, 1) = "|" then SizeList = left(SizeList, len(SizeList)-1)
				if right(QtyList, 1) = "|" then QtyList = left(QtyList, len(QtyList)-1)
				
				csvls.add "VARIANT~" & ThisCode & "~" & ThisVariant & "~" & ThisName & "~" & ThisColor & "~" & ThisImg & "~" & ThisID & "~" & ThisPrice & "~" & SizeList & "~" & QtyList & "~" & VariantTotalQty & "~" & VariantTotalValue & vbcrlf
				
			end if			
			
			
		
		next
		
		csvOS = csvls.value
		csvOS = replace(csvOS, ",", "+++")
		csvOS = replace(csvOS, "~", ",")
		
		%>
		<textarea class=form-control style="font-size:8pt;font-family:arial;width:100%;height:100px;"><%=csvOS%></textarea>
		<%
		
	end if
%>			
			<form name=cwayForm id=cwayForm action="/utilities/ranging/ranging-report/" method=post>
					

			<div class=well>
				<div class=row>
					<div class="span6 legible">
						<div class=spacer5><input type=checkbox name=CheckCatOrder <%if "" & CheckCatOrder = "1" then response.write "CHECKED" %> value="1"> <%=lne("ranging_sort_by_catalogue_long") %></div>
						<input class=form-control type=text name=SummarySearchVal id=SummarySearchVal value="<%=SummarySearchVal%>">
						
					</div>		

					<div class="span5 legible">
						<%
						if 1 = 1 then
						%>
							<div class=spacer5><input type=checkbox name=CheckHideZeros <%if "" & CheckHideZeros = "1" then response.write "CHECKED" %> value="1"> <%=lne("ranging_hide_zero_qtys")%></div>
						<%
						end if
						%>
						
					</div>						
					
				</div>			
				<div class=row>
					<div class=span6>
						<%
						'=DrawCombo_HIERARCHY("FilterProductType", "SML_ProductTypes", "ProductTypeID", "ProductTypeName", "", FilterProductType, "ParentID, ProductTypeZOrder" , false, false, "--Product type filter---", "ParentID", 0)
						%>
						
<%
for c = 0 to sptc

	ThisClassificationID = sptArr(5,c)
	ThisClassificationName = sptArr(3,c)
	
	if ThisClassificationID <> LastClassificationID then
		
		if "" & ThisClassificationID = "" & FilterNavClassification then
			SelStr = "SELECTED" 
		else
			SelStr = "" 
		end if
		
		TypeOS = TypeOS & "<option " & SelStr & " value=""" & ThisClassificationID & """>" & ThisClassificationName & "</option>"
		LastClassificationID = ThisClassificationID
		
	end if	
next
%>
						<select class="FilterOption legible" name="FilterNavClassification">
							<option value="">--<%=lne("ranging_filter_product_type")%>---</option>
							<%=TypeOS%>
						</select>
						
					</div>			
					<div class=span4>
						<select class="FilterOption legible" name=FilterEndUse id=FilterEndUse>
							<option value="">---<%=lne("ranging_filter_end_use")%>---</option>
							<option <%if FilterEndUse = "1" then response.write "SELECTED"%> value="1">ROAD</option>
							<option <%if FilterEndUse = "2" then response.write "SELECTED"%> value="2">MOUNTAIN</option>
							<option <%if FilterEndUse = "18" then response.write "SELECTED"%> value="18">A2B</option>
							<option <%if FilterEndUse = "14" then response.write "SELECTED"%> value="14">OTHER</option>
						</select>	
					</div>					
				</div>			
				<div class=row>

					<div class=span6>
						<select class="FilterOption legible" name=FilterDemographic id=FilterDemographic>
							<option value="">---<%=lne("ranging_filter_demographic")%>c---</option>
							<option <%if FilterDemoGraphic = "3" then response.write "SELECTED"%> value="3">MEN</option>
							<option <%if FilterDemoGraphic = "1" then response.write "SELECTED"%> value="1">WOMEN</option>
							<option <%if FilterDemoGraphic = "2" then response.write "SELECTED"%> value="2">KIDS</option>
						</select>	
					</div>	
					<div class=span3>
						<select class="FilterOption legible" name=FilterColor id=FilterColor>
							<option value="">---<%=lne("ranging_filter_color")%>---</option>
							<%
							for c = 0 to SimpleColorC
								ThisSimpleColorID = SimpleColorArr(0,c)
								ThisSimpleColorName = SimpleColorArr(1,c)
							%>
								<option <%if "" & ThisSimpleColorID = "" & FilterColor then response.write "SELECTED"%> value="<%=ThisSimpleColorID%>"><%=ThisSimpleColorName%></option>
							<%
							next
							%>
							
						</select>	
					</div>	
					<div class=span1>
						
						<a class="pointer FilterButton FilterGo btn btn-success">GO</a>	
					</div>						
				</div>
			</div>			
		
		<%
		if 1 = 1 then		
		%>
			<div class="hidden-print legible alert alert-success">
				<h3>Export this proposal to Excel</h3>
				<p><i class="icon icon-share-alt"></i> Click <a target="_BLANK" class=OrangeLink href="/utilities/ranging/ranging-report/export/?RangingID=<%=RangingID%>">here</a> to export the details of this proposal as a CSV file</p>
			</div>
		<%				
		end if
		
		if RepC >= 0 then
			'if AllowDealerChanges = 1 or isAdminUser() then
		%>
		
			<div class="legible spacer20"><i id="AllSKUS" class="icon icon-chevron-down"></i> <a class="OrangeLink pointer" id="AllSKUSLink" ><%=lne("ranging_show_hide_qtys")%></a></div>
		
		<%
			'end if
		%>
		
			<table class="ReportTable legible" border=1>
						

				<%
				
				MaxEndUse = 4
				MaxDemogs = 4
				redim TotalArrEndUse(3, MaxEndUse)
				redim TotalArrDemogs(3, MaxDemogs)
				
				TotalUnits = 0
				TotalAccessoryValue = 0
				

				if isdate(BenchmarkStartDate) and isdate(BenchmarkEndDate)  then
				
					BenchmarkStartDate_EX = exDate(BenchmarkStartDate)
					BenchmarkEndDate_EX = exDate(BenchmarkEndDate)
					
					sql = ""
					sql = sql & "SELECT ss.ProductID, spc.VariantCode, SUM(QtyDelivered) AS BenchMarkSum "
					sql = sql & "FROM END_SalesOrderLineValues esv INNER JOIN SML_SKUS ss ON ss.SKU = esv.stockcode "
					sql = sql & "INNER JOIN SML_ProductColorways spc ON spc.ProductColorwayID = ss.ProductColorwayID "
					sql = sql & "WHERE AccountCode = '" & BenchmarkAcCode & "' "
					sql = sql & "AND DueDate > " & BenchmarkStartDate_EX & " AND DueDate < " & BenchmarkEndDate_EX & " "
					sql = sql & "GROUP BY ss.ProductID, spc.VariantCode "
					'jhInfo sql
					
					x = getrs(sql, BenchArr, BenchC)
						
					jhAdmin "BenchC = " & BenchC
				else
					BenchC = -1
				end if				
				
				for c = 0 to RepC
					ThisProductID = RepArr(0,c)
					ThisProductCode = RepArr(1,c)
					ThisProductName = RepArr(2,c)
					
					ThisColorName = RepArr(5,c)
					
					ThisProductType = RepArr(14,c)
					ThisProductClassification = "" & RepArr(24,c)
					
					ThisProductSeason = ucase("" & RepArr(26,c))
					
					LRPlusStr = ""
					if request("jh") = "1" or isjh() then
						ThisTags = ucase("" & RepArr(25,c))
						'jh ucase("" & RepArr(25,c))
						if instr(1, ThisTags, "LRPL") > 0 then
							LRPlusStr = "<div><span class=""label label-warning"">" & ucase("" & RepArr(25,c)) & "</span></div>"
						end if
					end if
					
					if ThisProductClassification = "" then ThisProductClassification = "-"
					
					ThisVariantID = RepArr(11,c)
					ThisVariantCode = RepArr(4,c)
					
					ThisCategories = "" & RepArr(18,c)
					ThisDemographics = "" & RepArr(19,c)
					
					'jhAdmin "ThisCategories = " & ThisCategories & " (" & ThisProductName & ")"
					
					EndUsePos = 4
					if instr(1, ThisCategories, "|1|") then
						EndUsePos = 1
					elseif instr(1, ThisCategories, "|2|") then
						EndUsePos = 2
					elseif instr(1, ThisCategories, "|18|") then
						EndUsePos = 3
					end if					
					
					DemogPos = 3
					if instr(1, ThisDemographics, "|1|") then
						DemogPos = 1
					elseif instr(1, ThisDemographics, "|2|") then
						DemogPos = 2
					else					
						DemogPos = 3					
					end if							
					
					ShowThis = true
					if FilterEndUse <> 0 and FilterEndUse <> EndUsePos then ShowThis = false
					if FilterDemographic <> 0 and FilterDemographic <> DemogPos then ShowThis = false
					
			
					
					
					if ShowThis then
						SomeShown = true
						ShowHeadings = false
						
						if CheckCatOrder = "1" then
							'ShowHeadings = true
						elseif ThisProductClassification <> LastProductClassification then
							ShowHeadings = true
						end if
						
						if ShowHeadings and CheckCatOrder <> "1" then
							
							%>
							<tr>
								
								<td colspan=12><h4><%=ucase(ThisProductClassification)%></h4></td>
								
							</tr>	
						<%
						end if
						
						if ShowHeadings or (c = 0 and CheckCatOrder = "1" ) then
							LastProductClassification = ThisProductClassification
						%>	
							<tr>
								
								<th>Product</th>
								<th>Color</td>
								<th>Image</td>						
								<!--<th>Sizes</td>-->
								<th>Total Units</td>
								<th style="background:">Each</td>
								
								<th style="background:lightyellow">ROAD</th>
								<th style="background:lightyellow">MOUNTAIN</th>
								<th style="background:lightyellow">A2B</th>
								<th style="background:lightyellow">OTHER</th>
								
								<th style="background:lightblue">MEN</th>
								<th style="background:pink">WOMEN</th>
								<th style="background:LemonChiffon">KIDS</th>
								<th title="Accessories" style="background:lightgreen">ACC</th>
							</tr>						
							<%
						end if
						
						if ThisProductType <> LastProductType and CheckCatOrder <> "1" then
							LastProductType = ThisProductType
							%>
							<tr>
								
								<td colspan=12><strong><%=ucase(ThisProductType)%></strong></td>
								
							</tr>							
							<%
						end if						
						
						
						npStr = ""
						
						if ThisProductID <> LastProductID then
							DispName = ThisProductName
							LastProductID = ThisProductID
							ProdChanged = true
							ProductTypes = RepArr(22,c)
							NavClassificationID = RepArr(23,c)
							
							
							'jh "ThisProductName = " & ThisProductName & " (" & ProductTypes & ", " & NavClassificationID & ")"
							
							ThisAccessory = isAccessory(ProductTypes)
							
							'jhInfo "ThisAccessory = " & ThisAccessory
							
							
							'jh NewSeasonCodes & " ; " & ThisProductSeason
							if NewSeasonCodes <> "" and ThisProductSeason <> "" then
								if instr(1, NewSeasonCodes, ThisProductSeason) > 0 then
									npStr = ThisProductSeason
								end if
							end if							
							
						else
							DispName = ""
							ProdChanged = false
						end if
						
						if ThisVariantID <> LastVariantID then
						
							VariantCount = VariantCount + 1
						
							LastVariantID = ThisVariantID
							ThisVariantImage = getImageDefault(ThisProductCode, ThisVariantCode)
							
							SKUCount = 0
							SKUValue = 0
						
							for y = 1 to MaxEndUse
								TotalArrEndUse(0, y) = 0
							next
							for y = 1 to MaxDemogs
								TotalArrDemogs(0, y) = 0
							next		

							SizeStr = ""
							SKUPos = 0
							SomeEntered = false
							for z = c to RepC
								if ThisVariantID = RepArr(11,z) then
									
									ThisSKU = repArr(20,z)
									ThisUnitPrice = convertNum(RepArr(15,z))
									ThisUnitPrice_FO = convertNum(RepArr(27,z))
									
									ThisUnitPrice_FO = convertNum(RepArr(28,z))
									
									'jh ThisSKU & "_" & ThisUnitPrice & "_" & ThisUnitPrice_FO
									if ThisUnitPrice_FO <> 0 and glRangingUseFOPrices then ThisUnitPrice = ThisUnitPrice_FO
									
									ThisQty = convertNum(RepArr(16,z))
									if ThisQty = 0 then DispQty = "" else DispQty = ThisQty
									'jh ThisQty
									if ThisQty > 0 then
										SomeEntered = true
									end if
									
									ThisSizeName = RepArr(17,z)

									SKUCount = SKUCount + ThisQty
									SKUValue = SKUValue + (ThisQty * ThisUnitPrice)
									
									TotalArrEndUse(0, EndUsePos) = SKUValue
									TotalArrEndUse(1, EndUsePos) = ThisQty
									
								
									TotalArrDemogs(0, DemogPos) = SKUValue
									TotalArrDemogs(1, DemogPos) = ThisQty			
								
									SKUPos = SKUPos + 1
									
									SizeStr = SizeStr & "<div class=""SizeDiv""><strong>" & ThisSizeName & "</strong>"
									if AllowDealerChanges = 1 or AllowAdminChanges = 1 then
										SizeStr = SizeStr & "<input name=""Qty_" & VariantCount & "_" & SKUPos & """ value=""" & DispQty & """ class=""form-control legible"" type=text style=""width:24px;"">"
										SizeStr = SizeStr & "<input name=""SKU_" & VariantCount & "_" & SKUPos & """ value=""" & ThisSKU & """ class=""FormHidden"" type=hidden>"
									else
										if DispQty = "" then DispQty = "0"
										SizeStr = SizeStr & "<div><span class=""label label-success"">" & DispQty & "</span></div>"
									end if
									SizeStr = SizeStr & "</div>" 
									
									
								else
									exit for
								end if	
							next	
							
							if SomeEntered then 
						
								TotalArrEndUse(2, EndUsePos) = TotalArrEndUse(2, EndUsePos) + SKUValue
								TotalArrEndUse(3, EndUsePos) = TotalArrEndUse(3, EndUsePos) + ThisQty									
						
								TotalArrDemogs(2, DemogPos) = TotalArrDemogs(2, DemogPos) + SKUValue
								TotalArrDemogs(3, DemogPos) = TotalArrDemogs(3, DemogPos) + ThisQty									
							
							else
							
								SKUCount = convertNum(RepArr(21,c)) 'CWAY QTY
								'jh SKUCount
								ThisMaxValue = ConvertNum(RepArr(3,c))
								SKUValue = SKUCount * ThisMaxValue
								'jh SKUValue
							
								TotalArrEndUse(0, EndUsePos) = SKUValue
								TotalArrEndUse(1, EndUsePos) = SKUCount
								
								TotalArrEndUse(2, EndUsePos) = TotalArrEndUse(2, EndUsePos) + SKUValue
								TotalArrEndUse(3, EndUsePos) = TotalArrEndUse(3, EndUsePos) + SKUCount									
								
								TotalArrDemogs(0, DemogPos) = SKUValue
								TotalArrDemogs(1, DemogPos) = SKUCount			

								TotalArrDemogs(2, DemogPos) = TotalArrDemogs(2, DemogPos) + SKUValue
								TotalArrDemogs(3, DemogPos) = TotalArrDemogs(3, DemogPos) + SKUCount											
							
							end if
							
							AccessoryValue = 0
							if ThisAccessory then
								AccessoryValue = SKUValue
								TotalAccessoryValue = TotalAccessoryValue + SKUValue
							end if							
							
							ThisBenchmarkSum = ""
							ThisBenchmarkStr = ""
							if BenchC >= 0 then
								for y = 0 to BenchC
									if BenchArr(1,y) = ThisVariantCode then
										ThisBenchmarkSum = convertNum(BenchArr(2,y))
										exit for
									end if
								next
							end if							
							
							if ThisBenchmarkSum <> "" then
								ThisBenchmarkStr = "&nbsp;<span title=""Qty ordered in prior window"" class=""label label-info"" style=""font-size:120%;"">" & ThisBenchmarkSum & "</span>"
							end if
							
					
							
							TotalUnits = TotalUnits + SKUCount
							
							if SKUCount = 0 then NoSKUStr = "<i title=""NO QTYS ENTERED"" style="""" class=""icon icon-warning-sign""></i>&nbsp;" else NoSKUStr = ""
							

							
					%>
							<tr>
								
								<%
								ClickStr = ""
								ClassStr = ""
								if ProdChanged then
									ClickStr = "$('.SKUPRod_" & ThisProductID & "').toggle('fast');"
									ClassStr = "pointer"
								end if
								%>
								<td class="<%=ClassStr%>" onclick="<%=ClickStr%>">
								
								<%=DispName%>
								<%
								=LRPlusStr
								%>
								
								<%
								if npStr <> "" then
								%>
									<div>
										<span class="label label-warning"><i class="icon icon-white icon-asterisk"></i> <%=npStr%></span>
									</div>
								<%
								end if	
								%>
								
								</td>
								<td><%=NoSKUStr%><%=ThisColorName%></td>
								<td><img style="width:50px;" class=pointer onclick="$('#SKURow_<%=VariantCount%>').toggle('fast');" src="<%=ThisVariantImage%>"><br><small><%=ThisVariantCode%></small></td>
								
								<!--<td>Sizes</td>-->
								<td class=numeric>
									<%
									if not SomeEntered and (AllowDealerChanges or AllowAdminChanges = 1) then 
									%>
										<input class="form-control legible" style="width:40px;" type=text name="CWAyQty_<%=VariantCount%>" value="<%=SKUCount%>">	
									<%
									else
									%>
										<strong class=pointer onclick="$('#SKURow_<%=VariantCount%>').toggle('fast');"><%=SKUCount%></strong>
									<%
									end if
									%>
									<input class=FormHidden type=hidden name=CWAyID_<%=VariantCount%> value="<%=ThisVariantID%>">
									<input class=FormHidden type=hidden name=ProductID_<%=VariantCount%> value="<%=ThisProductID%>">
									
								</td>
								<td class=numeric style="background:"><%=dispCurrencyDash(ThisUnitPrice)%></td>
								
								<td class=numeric style="background:LemonChiffon"><strong><%=dispCurrencyDash(TotalArrEndUse(0, 1))%></strong></td>
								<td class=numeric style="background:LemonChiffon"><strong><%=dispCurrencyDash(TotalArrEndUse(0, 2))%></strong></td>
								<td class=numeric style="background:LemonChiffon"><strong><%=dispCurrencyDash(TotalArrEndUse(0, 3))%></strong></td>
								<td class=numeric style="background:LemonChiffon"><strong><%=dispCurrencyDash(TotalArrEndUse(0, 4))%></strong></td>
								
								<td class=numeric style="background:lightblue"><strong><%=dispCurrencyDash(TotalArrDemogs(0, 3))%></strong></td>
								<td class=numeric style="background:pink"><strong><%=dispCurrencyDash(TotalArrDemogs(0, 1))%></strong></td>
								<td class=numeric style="background:lightyellow"><strong><%=dispCurrencyDash(TotalArrDemogs(0, 2))%></strong></td>
								<td class=numeric style="background:lightgreen"><strong><%=dispCurrencyDash(AccessoryValue)%></strong></td>
								
								
							</tr>	
							
							<%
						
							%>
							
							<tr id="SKURow_<%=VariantCount%>" class="SKURow SKUPRod_<%=ThisProductID%>" style="display:none;">
								
								<td class=text-right colspan=2>
									Qtys
									<%
									=ThisBenchmarkStr
									%>								
								
								</td>
								<td colspan=12>
									<%
									=SizeStr
									%>
									

								</td>
								
								
							</tr>							
							
					<%
							
						end if	
					end if
				next
				
				EndUseGrandTotal = TotalArrEndUse(2,1) + TotalArrEndUse(2,2) + TotalArrEndUse(2,3) + TotalArrEndUse(2,4)
				DemographicGrandTotal = TotalArrDemogs(2,3) + TotalArrDemogs(2,1) + TotalArrDemogs(2,2)
				
					
						if AllowDealerPrices = 1 or isAdminUser() then
				%>
				
							<tr id="" style="background:green;color:white">
								
								<td class=text-right colspan=3 >TOTALS</td>
								<td class="numeric ReportTotal">
									<%
									=TotalUnits
									%>
								</td>					
								<td></td>	
								<td class="numeric ReportTotal">
									<%
									=dispCurrencyRounded(TotalArrEndUse(2,1))
									%>
								</td>
								<td class="numeric ReportTotal">
									<%
									=dispCurrencyRounded(TotalArrEndUse(2,2))
									%>
								</td>
								<td class="numeric ReportTotal">
									<%
									=dispCurrencyRounded(TotalArrEndUse(2,3))
									%>
								</td>
								<td class="numeric ReportTotal">
									<%
									=dispCurrencyRounded(TotalArrEndUse(2,4))
									%>
								</td>			
								<td class="numeric ReportTotal">
									<%
									=dispCurrencyRounded(TotalArrDemogs(2,3))
									%>
								</td>
								<td class="numeric ReportTotal">
									<%
									=dispCurrencyRounded(TotalArrDemogs(2,1))
									%>
								</td>	
								<td class="numeric ReportTotal">
									<%
									=dispCurrencyRounded(TotalArrDemogs(2,2))
									%>
								</td>	
								<td class="numeric ReportTotal">
									<%
									=dispCurrencyRounded(TotalAccessoryValue)
									%>
								</td>									
								
							</tr>	
							<tr id="">
								
								<td class=text-right colspan=3 >Percentage</td>
								<td></td>					
								<td></td>	
								<td class="numeric">
									<%=formatPercentage(TotalArrEndUse(2,1), EndUseGrandTotal)%>
								</td>
								<td class="numeric">
									<%=formatPercentage(TotalArrEndUse(2,2), EndUseGrandTotal)%>
								</td>
								<td class="numeric">
									<%=formatPercentage(TotalArrEndUse(2,3), EndUseGrandTotal)%>
								</td>
								<td class="numeric">
									<%=formatPercentage(TotalArrEndUse(2,4), EndUseGrandTotal)%>
								</td>			
								<td class="numeric">
									<%=formatPercentage(TotalArrDemogs(2,3), EndUseGrandTotal)%>
								</td>
								<td class="numeric">
									<%=formatPercentage(TotalArrDemogs(2,1), EndUseGrandTotal)%>
								</td>	
								<td class="numeric">
									<%=formatPercentage(TotalArrDemogs(2,2), EndUseGrandTotal)%>
								</td>	
								<td class="numeric">
									<%=formatPercentage(TotalAccessoryValue, EndUseGrandTotal)%>
								</td>									
								
							</tr>			
							
							<%
							end if
							
							if isjh() or 1 = 1 then
								if TotalArrEndUse(2,1) > Budget_ROAD and Budget_ROAD > 0 then
									Budget_ROAD_ERROR = "BudgetError"
								end if
								if TotalArrEndUse(2,2) > Budget_MOUNTAIN and Budget_MOUNTAIN > 0 then
									Budget_MOUNTAIN_ERROR = "BudgetError"
								end if
								if TotalArrEndUse(2,3) > Budget_A2B and Budget_A2B > 0 then
									Budget_A2B_ERROR = "BudgetError"
								end if	

								if TotalArrDemogs(2,3) > Budget_MEN and Budget_MEN > 0 then
									Budget_MEN_ERROR = "BudgetError"
								end if	
								
								if TotalArrDemogs(2,1) > Budget_WOMEN and Budget_WOMEN > 0 then
									Budget_WOMEN_ERROR = "BudgetError"
								end if	

								if TotalArrDemogs(2,2) > Budget_KIDS and Budget_KIDS > 0 then
									Budget_KIDS_ERROR = "BudgetError"
								end if									
								
								if TotalAccessoryValue > Budget_ACC and Budget_ACC > 0 then
									Budget_ACC_ERROR = "BudgetError"
								end if								
							
								if AllowDealerBudget = 1 or isAdminUser() then
							%>
									<tr>
										
										<td class=text-right colspan=3 >vs BUDGET</td>
										<td class="numeric BudgetTotal">
											.
										</td>					
										<td></td>	
										<td class="numeric BudgetTotal <%=Budget_ROAD_ERROR%>">
											<%
											=dispCurrencyRounded(Budget_ROAD)
											%>
										</td>
										<td class="numeric BudgetTotal <%=Budget_MOUNTAIN_ERROR%>">
											<%
											=dispCurrencyRounded(Budget_MOUNTAIN)
											%>
										</td>
										<td class="numeric BudgetTotal <%=Budget_A2B_ERROR%>">
											<%
											=dispCurrencyRounded(Budget_A2B)
											%>
										</td>
										<td class="numeric BudgetTotal">
											<%
											'=dispCurrency(TotalArrEndUse(2,4))
											%>
										</td>			
										<td class="numeric BudgetTotal <%=Budget_MEN_ERROR%>">
											<%
											=dispCurrencyRounded(Budget_MEN)
											%>
										</td>
										<td class="numeric BudgetTotal <%=Budget_WOMEN_ERROR%>">
											<%
											=dispCurrencyRounded(Budget_WOMEN)
											%>
										</td>	
										<td class="numeric BudgetTotal <%=Budget_KIDS_ERROR%>">
											<%
											=dispCurrencyRounded(Budget_KIDS)
											%>
										</td>				
										<td class="numeric BudgetTotal <%=Budget_ACC_ERROR%>">
											<%
											=dispCurrencyRounded(Budget_ACC)
											%>
										</td>											
									</tr>										
							<%

								end if
							
							end if
							%>
			
					</table>
					
					<%
					ThisOffersMatched = getMatchedOffers(RangingID, true)
					
					'sql = "UPDATE B2B_Ranging SET OffersMatched = '" & ThisOffersMatched & "' WHERE RangingID = 0" & RangingID
					'jhAdmin sql
					'db.execute(sql)					
					
					sql = "SELECT ro.RangingOfferID, rod.RangingOfferDetailID, ro.RangingOfferName, OfferDescription, SmallPrint "
					sql = sql & "FROM B2B_RangingOffers ro "
					sql = sql & "INNER JOIN B2B_RangingOfferDetails rod ON ro.RangingOfferID = rod.RangingOfferID "
					sql = sql & "WHERE ro.RangingOfferStatusID = 1 "
					sql = sql & "ORDER BY ro.RangingOfferID, rod.RangingOfferDetailsZOrder, rod.RangingOfferDetailID "
					
					jhinfo sql
					x = getrs(sql, OfferArr, OfferC)	

					'jhInfo "OfferC = " & OfferC
					
					set ols = new strconcatstream
					ols.add "<div class=spacer10>&nbsp;</div>"
					ols.add "<span class=""label label-success"">OFFERS</span>"
					ols.add "<div class=well>"					
					
					for c = 0 to OfferC
						ThisOfferID = OfferArr(0,c)
						ThisOfferDetailID = OfferArr(1,c)
						ThisOfferName = OfferArr(2,c)
						ThisOfferDetail = OfferArr(3,c)
						ThisOfferSmallPrint = "" & OfferArr(4,c)
						
						if LastOfferID <> ThisOfferID then
							LastOfferID = ThisOfferID
							ols.add "<h3>" & ThisOfferName & "</h3>"
							if ThisOfferSmallPrint <> "" then
								ols.add "<div class=""legible spacer5 "" style=""color:red;"">" & ThisOfferSmallPrint & "</div>"
							end if						
						end if
						
						LookFor = "|" & ThisOfferDetailID & "|"
						
						ClassStr = ""
						CheckStr = "" 
						
						if instr(1, ThisOffersMatched, LookFor) > 0 then						
							ClassStr = "OfferValid"			
							CheckStr = "&nbsp;&nbsp;<i class=""icon icon-ok""></i>"		
							OfferMatched = true							
						end if
							
						ols.add "<div class=""legible spacer5 " & ClassStr & """>" & ThisOfferDetail & CheckStr & "</div>"					
					next					
					
					ols.add "</div>"

					response.write ols.value
						
					set ols = nothing
					
					jhAdmin "EndUseGrandTotal = " & EndUseGrandTotal
					jhAdmin "DemographicGrandTotal = " & DemographicGrandTotal
					TotalCheck = abs(EndUseGrandTotal - DemographicGrandTotal)
					jhAdmin "TotalCheck = " & TotalCheck
					if TotalCheck > 20 and isAdminUser() then
						jhErrorNice "Total mismatch", "Totals by End-use and Demographic do not match - please check figures carefully" 
					end if
					
					if EndUseGrandTotal > 0 and (AllowDealerBudget = 1 or isAdminUser()) then
					%>
						<div class=spacer20></div>
						
						<div class=text-right>
							<%
							
							if EndUseGrandTotal > 0 and RangingBudget > 0 then
								ReportPCTotal = (EndUseGrandTotal/RangingBudget) * 100
								DispReportPCTotal = " (<strong>" & formatnumber(ReportPCTotal,0) & "%</strong>)"
								DispBudgetTotalStr = "vs budget of " & dispCurrency(RangingBudget) & " " & DispReportPCTotal & ""
							else
								
							end if
							
							jhInfoNice "<h1>" & dispCurrency(EndUseGrandTotal) & "</h1>", lne("ranging_grand_total") & DispBudgetTotalStr
							%>
							<div class=legible><%=lne("ranging_terms_prices_1")%></div>
							<%
							if glRangingUseFOPrices then
							%>
							<div class=legible><%=lne("ranging_terms_prices_2")%></div>	
							<%
							end if
							%>
						</div>
					<%
					end if
					%>
					<div class=spacer20></div>
					
					<%
					if AllowDealerChanges = 1 or AllowAdminChanges = 1 then
					%>					
						<input class=FormHidden type=hidden name=FormAction id=FormAction value="CWAY_SIM">
						<input class=FormHidden type=hidden name=VariantCount id=VariantCount value="<%=VariantCount%>">
						<input class=FormHidden type=hidden name=RangingID id=RangingID value="<%=RangingID%>">

						<a class="pointer UpdateButton btn btn-success"><%=lne("ranging_update_btn")%></a>	
					<%
					end if
					%>	
				</form>							
			
			<%
			end if
			
			%>
			
			
			
		</div>
		
	</div>
	
</div>

<!--#include virtual="/assets/includes/footer.asp" -->

<script>
	$(".FilterGo").click( function () {
	
		$("#cwayForm").submit();
	});
	
	$(".UpdateButton").click( function () {
	
		$("#FormAction").val("SAVE_RANGING");
		$("#cwayForm").submit();
		
	});	
	
	$("#AllSKUSLink").click( function () {
	
		$("#AllSKUS").toggleClass("icon-chevron-down icon-chevron-up")
		$(".SKURow").toggle()
		
	});
	
	
	$("#SummarySearchVal").keyup(function (e) {
		//alert(e.keyCode);
		if (e.keyCode == 13) {
			$("#cwayForm").submit();
		}
	});			
	
	
</script>

<%
public HierarchyComboCurrentLevel, HComboLS

function padHCombo(pLevel)
	dim c
	padHCombo = ""
	for c = 2 to pLevel
		padHCombo = padHCombo & "----"
	next
end function

function popHCombo(byVal pParent, byVal pArr, byVal pRC, byVal pCurrentVal)
	
	dim c
	'jh "looking for = " & pParent
	
	HierarchyComboCurrentLevel = HierarchyComboCurrentLevel + 1
	pParent = "" & pParent
	for c = 0 to pRC
		
		ThisID = "" & pArr(0,c)
		ThisParent = "" & pArr(2,c)
		
		if ThisParent = pParent then
			'jh padHCombo(HierarchyComboCurrentLevel) & "Found = " & ThisID & " (" & pArr(1,c) & ")"
			HComboLS.add "<option value=""" & pArr(0,c) & """"
			HComboLS.add pArr(0,c)
			HComboLS.add """"
			
			if "" & pArr(0,c) = trim(cstr("" & pCurrentVal)) then
				HComboLS.add " SELECTED"
			end if
			
			'DrawCombo = DrawCombo & ">" & pArr(1,c) & "</option>"			
			HComboLS.add ">"
			padStr = padHCombo(HierarchyComboCurrentLevel)
			HComboLS.add padStr
			HComboLS.add pArr(1,c)
			
			HComboLS.add "</option>"		
			
			call popHCombo(ThisID, pArr, pRC, pCurrentVal)
			
		end if
		
	next
	
	HierarchyComboCurrentLevel = HierarchyComboCurrentLevel - 1
	
end function

function DrawCombo_HIERARCHY(SelectName, TableName, IDField, TextField, WhereStr, ComboVal, OrderField , Descending, LimitToList, NotInListText, ParentField, StartParent)


	'jh "UPDATE DRAWCOMBO"
		'jh ChangeSubmit
		dim x, sql 
		set HComboLS = new strConcatStream
		dim RecArr
		'jh TableName
		
		if IDField = TextField and instr(1, IDField, "AS ") = 0 then 
			IDFIeld = IDField  & " AS ComboIDField"
			TextField = TextField  & " AS ComboTextField"
		end if
		
		if UseDistinct then
			DistinctStr = "DISTINCT "
		end if
		
		UseDistinct = false
		sql = "SELECT " & DistinctStr & " " & IDField & "," & TextField & ", " & ParentField & " " & DefField & " FROM " & TableName 
		if WhereStr <> "" then
			sql = sql & " WHERE " & WhereStr
		end if
		if OrderField <> "" then
			sql = sql & " ORDER BY " & OrderField
			if Descending then
				sql = sql & " DESC"
			end if
		end if		
		sql = sql & ";"
		
		'jhAdmin sql
		
		x = getrs(sql, RecArr, rc)
		'jh "rc = " & rc
		
		DrawCombo_HIERARCHY = ""
		
		if ChangeSubmit <> "" then ChangeStr = "onchange=""document." & ChangeSubmit & ".submit();"""
		if EventStr <> "" then ChangeStr = EventStr
		EventStr = ""

		pInd = pInd+1
		'DrawCombo = DrawCombo & "<select TABINDEX=" & pInd & " " & ChangeStr & " class=FormSelect style="";"" name=""" & SelectName & """ id=""" & SelectName & """>"
		HComboLS.add "<select xTABINDEX="
		HComboLS.add pInd
		HComboLS.add " " 
		HComboLS.add ChangeStr
		HComboLS.add " class=FormSelect style=""font-size:9pt;;"" name="""
		HComboLS.add SelectName
		HComboLS.add """ id="""
		HComboLS.add SelectName
		HComboLS.add """>"	
		
		if not LimitToList then
			if NotInListText = "" then
				'DrawCombo = DrawCombo & "<option value="""">Not in List...</option>"
				HComboLS.add "<option value="""">Not in List...</option>"
			else
				'DrawCombo = DrawCombo & "<option value="""">" & NotInListText & "</option>"			
				HComboLS.add "<option value="""">" & NotInListText & "</option>"
			end if
		end if		
		
		call popHCombo(StartParent, RecArr, rc, ComboVal)
		
		'DrawCombo = DrawCombo & "</select>."		
		HComboLS.add "</select>."		
		DrawCombo_HIERARCHY = HComboLS.value
		
		ChangeSubmit = ""
		set HComboLS = nothing		
		
		exit function
		
		'========================================
		'========================================
		'========================================
		'========================================
	
end function


%>