<%
PageTitle = "Ranging Report #1"
%>

<!--#include virtual="/assets/includes/global_functions.asp" -->

<%

set acDetails = new DealerAccountDetails
set AdminDetails = new AdminAccountDetails

dbConnect()

RangingID = cleannum(request("RangingID"))

if RangingID = "" then RangingID = session("RangingID")
AllowExport = false

if RangingID <> "" and isAdminUser() then
	sql = "SELECT r.*, ecd.CompanyName, u.AdminName "
	sql = sql & "FROM B2B_Ranging r "
	sql = sql & "INNER JOIN exchequercustdetails ecd ON ecd.AcCode = r.AcCode "
	sql = sql & "INNER JOIN END_AdminUsers u ON u.AdminID = r.RangedBy "	
	sql = sql & "WHERE r.RangingID = 0" & RangingID & " "
	'jh sql
	set rs = db.execute(sql)
	
	if not rs.eof then
	
		AcCode = rs("AcCode")
		RangingTitle = rs("RangingTitle")
		CompanyName = rs("CompanyName")
		RangedByName = rs("AdminName")
		ProposedDeliveryDate = rs("ProposedDeliveryDate")
		if isdate(ProposedDeliveryDate) then ProposedDeliveryDate = sqldate(ProposedDeliveryDate)
		
		RangingStatusID = rs("RangingStatusID")	
		if RangingStatusID >= 0 then 
			AllowExport = 1
		else
			ErrorCode = "Status: Draft"
		end if
	else
		ErrorCode = "Not found"
	end if
	
	rsClose()
else 
	ErrorCode = "Not allowed"
end if

if AllowExport then
	set csvls = new strconcatstream

	'jhInfo "HERE!"
	
	DelimChar = "~"	
	
	csvls.add "Proposal for: "  
	csvls.add DelimChar
	csvls.add AcCode
	csvls.add vbcrlf
	csvls.add DelimChar
	csvls.add CompanyName	
	csvls.add vbcrlf	
	
	csvls.add "Proposal name: "  
	csvls.add DelimChar
	csvls.add RangingTitle
	csvls.add vbcrlf
	
	csvls.add "Created by: "  
	csvls.add DelimChar
	csvls.add RangedByName
	csvls.add vbcrlf
	
	csvls.add "To be delivered: "  
	csvls.add DelimChar
	csvls.add ProposedDeliveryDate
	csvls.add vbcrlf	
	csvls.add vbcrlf	
	csvls.add vbcrlf	
	
	sql = "SELECT r.SKU, r.SKUQty, pbl." & AcDetails.CostBand & "_FO, rrp." & AcDetails.CostBand & "_RRP, sp.ProductName, sp.ProductCode, spc.VariantCode, spc.ColorName, sz.SizeName, ISNULL(ProductTypeName_FULL, spt.ProductTypeName) AS ProdType, stBarcode "
	sql = sql & "FROM B2B_RangingSKUS r "
	sql = sql & "INNER JOIN SML_SKUS ss ON ss.SKU = r.SKU " 
	sql = sql & "INNER JOIN SML_ProductColorways spc ON spc.ProductColorwayID = ss.ProductColorwayID "
	sql = sql & "INNER JOIN SML_Products sp ON sp.ProductID = ss.ProductID "
	sql = sql & "INNER JOIN SML_SizeList sz ON sz.SizeID = ss.SizeID "
	sql = sql & "INNER JOIN SML_ProductTypes spt ON spt.ProductTypeID = sp.ProductTypeID "
	sql = sql & "INNER JOIN END_PriceBandLookup pbl ON pbl.StockCode = ss.SKU "
	sql = sql & "LEFT JOIN END_RRPLookup rrp ON rrp.StockCode = ss.SKU "
	sql = sql & "INNER JOIN ExStock es ON es.stIdxStockCode = ss.SKU "
	sql = sql & "WHERE r.RangingID = 0" & RangingID & " "
	sql = sql & "ORDER BY spt.ProductTypeZORder, sp.ProductCode, spc.DefaultProductImage, spc.VariantCode"
	
	'jh sql
	'jhstop
	x = getrs(sql, cArr, cc)
	
	'jh "cc = " & cc

	'SKU, Style code, Colourway code, Description, Colour, Size, Barcode, Cost,  RRP and quantity ordered
	
	
	csvls.add "SKU" & DelimChar & "Qty" & DelimChar & "Each" & DelimChar & "RRP" & DelimChar & "Product Name" & DelimChar & "Code" & DelimChar & "Variant" & DelimChar & "Color" & DelimChar & "Size" & DelimChar & "Product Type" & DelimChar & "Barcode" & vbcrlf
	
	for c = 0 to cc
		
		for y = 0 to ubound(cArr,1)
			csvls.add trim("" & cArr(y,c))
			csvls.add DelimChar
		next
		
		csvls.add vbcrlf
	next
	
	csvos = csvls.value
	
	'jh addbr(csvos)
	csvos = replace(csvos, ",", ".")
	csvos = replace(csvos, DelimChar, ",")
	
	fName = "PROPOSAL_EXPORT_" & RangingID & ".csv"
	
	ThisDownloadFolder = getAccountDownloadFolder()
	
	'jh "ThisDownloadFolder = " & ThisDownloadFolder
	
	DownloadURL = AcDetails.DownloadURL
	
	jh "DownloadURL = " & DownloadURL
	
	'jh addbr(csvos)
	
	'jhstop
	
	
	
	'jh "fName = " & fName
	
	call writeReportOutput (ThisDownloadFolder, fName, csvos)
	ReportRedirect = DownloadURL & "/" & fName
	
end if

dbClose()

if ReportRedirect <> "" then
	response.redirect ReportRedirect
else
	jhError "SORRY THERE WAS A PROBLEM RETRIEVING THE EXPORT FILE (" & ErrorCode & ")" 
end if

%>
