<%
PageTitle = "Good/Better/Best Report"
%>

<!--#include virtual="/assets/includes/header.asp" -->



<style>

.ReportTotal {
	font-size:large;
	font-weight:bold;
}

.modal-body {
    max-height: 80vh; //Sets the height to 80/100ths of the viewport.
}

.SizeDiv {
	background:#f0f0f0;
	display:inline-block;
	width:70px;
	padding:4px;
	border-radius:3px;
	margin-right:5px;
	text-align:center;
}

	.leftFilter {
		font-family:arial !important;
		font-size:9pt !important;
	}
	.leftNavHolder {
		border-radius:3px;
		background:#f0f0f0;
		padding:10px;
		margin-bottom:5px;
	}
	
	.accordion-inner {
		padding:5px 15px;
	}
	
	.VariantTile {
		display:inline-block;
		width:20px;
		height:5px;
		border:1px solid #333;
		margin-right:3px;
	}
	.VariantTileImage {
		width:20px;
		height:5px;
	}
	
	.pointer {
		cursor:pointer;
	}
	
	.TopNavSep {
		border-bottom:1px solid #f0f0f0;padding:1px 0;
	}
	
	.NavHeading {
		color:orange;
		font-weight:bold;
		padding:10px 0 3px 0;
		font-size:110%;
	}
	
	.modal.large {
		width: 80%; /* respsonsive width */
		margin-left:-40%; /* width/2) */ 
	}	
	
	.colorwayQty {
		padding:2px !important;
		width:20px !important;
		border:1px solid silver;
	}
	
</style>

<%

RangingID = cleannum(request("RangingID"))

if RangingID = "" then RangingID = session("RangingID")

if RangingID <> "" then
	sql = "SELECT RangingID, AcCode, RangingTitle, RangingDescription, AllowDealerView, AllowDealerChanges, AllowDealerPrices, AllowDealerBudget "
	sql = sql & "FROM B2B_Ranging WHERE RangingID = 0" & RangingID & " "
	if not isAdminUser then
		sql = sql & "AND AcCode = '" & AcDetails.AcCode & "' "
		sql = sql & "AND AllowDealerView = 1 "
	end if
	set rs = db.execute(sql)
	
	if not rs.eof then
		RangingTitle = rs("RangingTitle")
		RangingDescription = rs("RangingDescription")

		AllowDealerView = convertnum(rs("AllowDealerView"))
		AllowDealerChanges = convertnum(rs("AllowDealerChanges"))
		AllowDealerPrices = convertnum(rs("AllowDealerPrices"))
		AllowDealerBudget = convertnum(rs("AllowDealerBudget"))		
		
		session("RangingID") = RangingID
	end if
	
	rsClose()
end if

RangingID = session("RangingID")

if RangingID = "" then
	jhError "NO RANGING PROPOSAL SELECTED - RETURN TO MAIN ADMIN PAGE"
	FormAction = ""
	dbClose()
	response.redirect "/admin/ranging/"
end if

dbConnect()


VariantCount = 0
%>

    <div id="">
	
		
	
		<div id="FilterOutput">
		
		</div>
		

		
    </div>

</div>

<%



%>

<div class="container spacer40">	
	
	
<div class=row>
	<div class=span12>
		

	
	
	
<%


'jhstop
%>			
	</div>
</div>

<div class=row>
		<div class=span12>		
		
			<hr>
				<div class=spacer20>
					<span class="label label-inverse"><%=AcDetails.AcCode%></span> <span class=legible><%=AcDetails.CompanyName%></span>
					<h3><%=RangingTitle%><br><small><%=PageTitle%></small></h3>
					<a class=OrangeLink href="/utilities/ranging/">Back to ranging proposal</a>
				</div>				
				
				<div id="AjaxFeedback">
				
				</div>
			<hr>
		
		
		</div>
	</div>
	
	<div class="row">
		
		
		<div class=span12>

<%

	FilterEndUse = convertnum(request("FilterEndUse"))
	'jh "FilterEndUse = " & FilterEndUse
	FilterDemoGraphic = convertnum(request("FilterDemoGraphic"))
	'jh "FilterDemoGraphic = " & FilterDemoGraphic	
	
	FilterProductType = convertnum(request("FilterProductType"))
	FilterColor = convertnum(request("FilterColor"))
	
	FormAction = request("FormAction")
	'jh "FormAction = " & FormAction

sql = "SELECT SimpleColorID, SimpleColorName FROM SML_ColorwayList_SIMPLE ORDER BY SimpleColorName "
x = getrs(sql, SimpleColorArr, SimpleColorC)
'jh "SimpleColorC = " & SimpleColorC

sql = "SELECT ProductTypeID, ISNULL(ProductTypeName_FULL, ProductTypeName), ParentID AS PRoductTypeName FROM SML_ProductTypes ORDER BY PRoductTypeZORder "
x = getrs(sql, ProductTypeArr, ProductTypeC)
'jh "ProductTypeC = " & ProductTypeC

	'jh "AcDetails.CostBand = " & AcDetails.CostBand
	SelCostBand = AcDetails.CostBand
	
sql = "SELECT spc.ProductID, spc.VariantCode, ColorSum "
sql = sql & "FROM SML_ProductColorways spc "
sql = sql & "LEFT JOIN (SELECT ProductColorwayID, SUM(ColorwayQty) AS ColorSum FROM b2b_rangingcolorways WHERE RangingID = 0" & RangingID & " AND ColorwayQty > 0 GROUP BY ProductColorwayID) cq ON cq.ProductColorwayID = spc.ProductColorwayID "
sql = sql & "WHERE dbo.spB2BRangingActive(spc.ProductID) > 0 "
sql = sql & "ORDER BY spc.ProductID, spc.DefaultProductImage "
	
x = getrs(sql, VariantArr, VariantC)

'jh "VariantC = " & VariantC
'jh "RangingID = " & RangingID

sql = "SELECT sp.ProductID, sp.ProductCode, sp.ProductName, sp.ProductTypeID, ISNULL(spt.ProductTypeName_FULL, spt.ProductTypeName) AS SPTName, "
sql = sql & "sp.Demographics, sp.Categories, sp.ClassificationTags, rp.RangingID, rp.UnitQty, MaxPrice, VariantCode "
sql = sql & "FROM SML_Products sp  "
sql = sql & "INNER JOIN SML_ProductTypes spt ON spt.ProductTypeID = sp.ProductTypeID "
sql = sql & "LEFT JOIN (SELECT * FROM B2B_RangingProducts WHERE RangingID = 0" & RangingID & ") rp ON rp.ProductID = sp.ProductID "
sql = sql & "LEFT JOIN (SELECT ProductID, MAX(BandA) AS MaxPrice FROM END_PriceBandLookup INNER JOIN SML_SKUS ss ON ss.SKU = END_PriceBandLookup.StockCode GROUP BY ProductID) mp ON mp.ProductID = sp.ProductID "

sql = sql & "LEFT JOIN (SELECT sp.ProductID, sp.ProductCode, spc.VariantCode "
sql = sql & "FROM SML_Products sp INNER JOIN SML_ProductColorWays spc ON spc.ProductID = sp.ProductID "
sql = sql & "WHERE DefaultProductImage = 1) defvar ON defvar.ProductID = sp.ProductID "

sql = sql & "WHERE dbo.spB2BRangingActive(sp.ProductID) > 0 "
sql = sql & "ORDER BY ProductTypeZOrder, MaxPrice DESC, ProductCode	 "
	
	'jh sql
	'jhstop
	x = getrs(sql, gbbarr, gbbc)
	
	'jh gbbc
	
	for c = 0 to gbbc
		ThisProductID = gbbarr(0,c)
		ThisVariantCode = trim("" & gbbarr(11,c))
		if ThisVariantCode = "" then
			for z = 0 to VariantC
				if "" & ThisProductID = "" & VariantArr(0,z) then
					'jh "swap - " & VariantArr(1,z)
					gbbarr(11,c) = VariantArr(1,z)
					exit for
				end if
			next
		end if
	next
	
	'jh now
	
	MaxCheaper = 2
	MaxPricier = 2
	
	set ls = new strconcatstream
	
	ls.add "<table class=""ReportTable legible"" width=100%  border=1>"
	
		for c = 0 to gbbc
		
			ThisProductID = gbbarr(0,c)
			ThisProdCode = gbbarr(1,c)
			ThisProdName = gbbarr(2,c)
			ThisRangingID = convertnum(gbbarr(8,c))
			
			ThisProductTypeID = convertnum(gbbarr(3,c))
			ThisProductTypeName = gbbarr(4,c)
			
	redim CheapArr(4, MaxCheaper)
	redim PricyArr(4, MaxPricier)
			
			ThisDemographics = gbbarr(5,c)
			ThisEndUse = gbbarr(6,c)
			ThisOptions = gbbarr(7,c)
			
			if instr(1, ThisOptions, "|WP|") > 0 then ThisWP = true else ThisWP = false
			
			ThisMaxPrice = gbbarr(10,c)
			ThisVariantCode = gbbarr(11,c)
			
			
			if ThisRangingID > 0 or 1 = 0 then
			
				if ThisProductTypeName <> LastProductTypeName then
					'jhInfo "ThisProductTypeName = " & ThisProductTypeName
					ls.add "<tr>"
					ls.add "<td colspan=10><h3>" & ThisProductTypeName & "</h3></td>"
					ls.add "</tr>"
					LastProductTypeName = ThisProductTypeName
				end if			
			
				'jhInfo "ThisProdCode = " & ThisProdCode
				'jh "ThisProdName = " & ThisProdName
				
				CheaperFound = 0
				
				if c < gbbc then
					for z = c + 1 to gbbc step 1
						
						CheckID = gbbarr(0,z)
						CheckName = gbbarr(2,z)
						CheckType = convertnum(gbbarr(3,z))
						CheckDemogs = gbbarr(5,z)
						CheckEndUse = gbbarr(6,z)
						CheckOptions = gbbarr(7,z)
						
						CheckCode = gbbarr(1,z)
						CheckVariant = gbbarr(11,z)
						CheckRanged = convertnum(gbbarr(8,z))
						
						if instr(1, CheckOptions, "|WP|") > 0 then CheckWP = true else CheckWP = false
							
						if ThisProductTypeID = CheckType and ThisDemographics = CheckDemogs and ThisEndUse = CheckEndUse and ThisWP = CheckWP then

							'jhError "CHEAPER! gbbarr(2,z) = " & gbbarr(2,z)
							CheaperFound = CheaperFound + 1 
							
							CheapArr(0, CheaperFound) = CheckID 'id
							CheapArr(1, CheaperFound) = CheckCode 'code
							CheapArr(2, CheaperFound) = CheckName 'name
							CheapArr(3, CheaperFound) = getImageDefault(CheckCode, CheckVariant) 'img
							CheapArr(4, CheaperFound) = CheckRanged 'ranged
							
							if CheaperFound >= MaxCheaper then exit for
							
						end if
					next
				end if
				
				PricierFound = 0
				
				if c > 0 then
					for z = c-1 to 0 step -1 
					
						CheckID = gbbarr(0,z)
						CheckName = gbbarr(2,z)
						CheckType = convertnum(gbbarr(3,z))
						CheckDemogs = gbbarr(5,z)
						CheckEndUse = gbbarr(6,z)
						CheckOptions = gbbarr(7,z)
						
						CheckCode = gbbarr(1,z)
						CheckVariant = gbbarr(11,z)
						CheckRanged = convertnum(gbbarr(8,z))
					
						if instr(1, CheckOptions, "|WP|") > 0 then CheckWP = true else CheckWP = false
					
						if ThisProductTypeID = CheckType and ThisDemographics = CheckDemogs and ThisEndUse = CheckEndUse and ThisWP = CheckWP then

							'jhError "PRICIER! gbbarr(2,z) = " & gbbarr(2,z)
							PricierFound = PricierFound + 1 
							
							PricyArr(0, PricierFound) = CheckID 'id
							PricyArr(1, PricierFound) = CheckCode 'code
							PricyArr(2, PricierFound) = CheckName 'name
							PricyArr(3, PricierFound) = getImageDefault(CheckCode, CheckVariant) 'img
							PricyArr(4, PricierFound) = CheckRanged 'ranged
							
							if PricierFound >= MaxPricier then exit for
							
						end if
					next				
				end if
				
				ls.add "<tr>"
				
				for z = MaxCheaper to 1 step -1
					ls.add "<td class=text-center width=100></td>"
				next
				ls.add "<td class=text-center>"
				ls.add "<h4>" & ThisProdName & "</h4>"
				ls.add "</td>"
				for z = 1 to MaxPricier
					ls.add "<td class=text-center width=100></td>"				
				next				
				
				ls.add "</tr>"
				
				ls.add "<tr>"
				
				
				ProcArr = CheapArr
				
				for z = MaxCheaper to 1 step -1
					ls.add "<td valign=top class=text-center width=100>"
					
					if ProcArr(0, z) <> "" then
					
						CheckOption = getCheckOption(ProcArr(4, z), ProcArr(0, z))		
						
						ls.add "<div class=pull-left >"
						ls.add CheckOption
						ls.add "</div>"					
						if ProcArr(3, z) <> "" then ls.add "<img onclick=""showRangingModal(" & ProcArr(0, z) & ");"" style=""height:50px;"" src=""" & ProcArr(3, z) & """>"
						ls.add "<div>"
						ls.add ProcArr(2, z)	
					end if
					ls.add "</div>"					
					
					ls.add "</td>"					
				next
				
				CheckOption = getCheckOption(ThisRangingID, ThisProductID)			
				
				'jh "get variant image!"
				
				for z = 0 to variantc
					if variantarr(0,z) = ThisProductID then
						'jh "ThisProductID = " & variantarr(1,z)
						ThisVariantCode = variantarr(1,z)
						if variantarr(2,z) > 0 then exit for
					end if
				next
				
				ThisVariantImage = getImageDefault(ThisProdCode, ThisVariantCode)
				
				ls.add "<td valign=top class=text-center>"
				ls.add "<div class=pull-left style=""padding-left:40px;"">"
				ls.add CheckOption
				ls.add "</div>"					
				ls.add "<img onclick=""showRangingModal(" & ThisProductID & ");"" style=""height:100px;"" src=""" & ThisVariantImage & """>"
				ls.add "</td>"
				
				ProcArr = PricyArr
				
				for z = 1 to MaxPricier
					ls.add "<td valign=top class=text-center width=100>"
					
					if ProcArr(0, z) <> "" then
					
						CheckOption = getCheckOption(ProcArr(4, z), ProcArr(0, z))			
					
						ls.add "<div class=pull-left>"
						ls.add CheckOption
						ls.add "</div>"					
						if ProcArr(3, z) <> "" then ls.add "<img onclick=""showRangingModal(" & ProcArr(0, z) & ");"" style=""height:50px;"" src=""" & ProcArr(3, z) & """>"
						ls.add "<div>"
						ls.add ProcArr(2, z)	
					end if
					ls.add "</div>"					
					
					ls.add "</td>"				
				next				
				
				ls.add "</tr>"				
				
			end if
		next
		
		ls.add "</table>"
		
	'jh now
	
		response.write ls.value
	
	
	%>
	
			
		</div>
		
	</div>
	
</div>

<!--#include virtual="/assets/includes/footer.asp" -->

<script>
	$(".FilterGo").click( function () {
	
		$("#cwayForm").submit();
	});
	
	$(".UpdateButton").click( function () {
	
		$("#FormAction").val("SAVE_RANGING");
		$("#cwayForm").submit();
		
	});	
	
	function addRemoveRangingModal (pid, pCurrent, pContext) {
	
		//alert('add/remove = ' + pContext)
		addRemove(pid, pContext);
		showRangingModal (pCurrent);
		
	}			
	
<%
if AllowDealerChanges = 1 or isAdminUser() then
%>			
	
	function addRemove(pid, pContext)	{
		$.ajax({

			scriptCharset: "ISO-8859-1" ,
			contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
			url: "/utilities/ranging/ranging_server.asp?CallType=PROD_ADD&ProductID=" + pid + "&context=" + pContext + "&RangingID=<%=RangingID%>",				
			cache: false, 
			
			}).done( function( html ) {
					//alert('done...')						
					$("#AjaxFeedback").html(html);						
					//$("#AjaxModalDiv").show();
					//alert('toggle!')
					$(".gbbMarker_" + pid).toggleClass( "label-success label-important" );
					$(".gbbIcon_" + pid).toggleClass( "icon-ok icon-remove" );
					
		});		
	}	
<%
else
%>
		function addRemove(pid, pContext)	{
			//alert('no changes allowed')
		}
<%
end if
%>


	function addRemoveRanging(pid, pObj) {
	
		/*
		
		var pContext = $(pObj).data('context');
		var newContext = "" 
		if (pContext=='ADD') {
			newContext = 'REMOVE'
		} else {
			newContext = 'ADD'
		}
		
		//alert(pid + '_' + pContext)
		
			$.ajax({

				scriptCharset: "ISO-8859-1" ,
				contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
				url: "/utilities/ranging/ranging_server.asp?CallType=PROD_ADD&ProductID=" + pid + "&context=" + pContext + "&RangingID=<%=RangingID%>&ShowOutput=TERSE",				
				cache: false, 
				
				}).done( function( html ) {
						$(pObj).data('context', newContext);
						//alert('done...')						
						$(pObj).html(html);						
						//$("#AjaxModalDiv").show();
						drawProds("")
			});	
		
		*/
		
	} 
	
		function showRangingModal (pid) {
		
			//alert(pid);
		
			$.ajax({

				scriptCharset: "ISO-8859-1" ,
				contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
				url: "/utilities/ranging/ranging_server.asp?CallType=RANGING_GRID&ProductID=" + pid + "&RangingID=<%=RangingID%>",				
				cache: false, 
				
				}).done( function( html ) {
						//alert('done...')						
						$("#AjaxModalDiv").html(html);	
						
						//$("#AjaxModalDiv").show();	
						
						$("#rangingModal").modal();
						
						var firstSKU = document.getElementById("SKUQty_1");
						
						//alert(firstSKU.name)	
												
						setTimeout(function (){
							$(firstSKU).focus()
						}, 1000);						
						
						//alert('done');
						
						rangingCalc();	
				
			});				
		
			
			

			
			
			//$('[autofocus]').focus()
			
		}	
		
function rangingCalc() {

	 
			var someSKUS = false;
			var totalNum = 0;
			var thisQty = 0			
			var thisPrice = 0
			var totalVal = 0;
			
			var list = document.getElementsByClassName("SKUQty");
			for (var i = 0; i < list.length; i++) {
				// list[i] is a node with the desired class name
				thisQty = $(list[i]).val();
				thisPrice = $(list[i]).data('sku-price');
				
				if ($.isNumeric( thisQty )) {
					
					thisQty = parseInt(thisQty)
					thisPrice = parseFloat(thisPrice,2)
					
					totalNum = totalNum + thisQty
										
					totalVal = totalVal + (thisQty * thisPrice)
					
					someSKUS = true;
				}
			}			 
			 
			
			
			if (!someSKUS && totalNum == 0) {
				list = document.getElementsByClassName("ColorwayQty");
				for (var i = 0; i < list.length; i++) {
				
					
					thisPrice = $(list[i]).data('colorway-price');
					
					// list[i] is a node with the desired class name
					thisQty = $(list[i]).val();
					if ($.isNumeric( thisQty )) {
						thisQty = parseInt(thisQty)
						thisPrice = parseFloat(thisPrice,2)						
						totalNum = totalNum + thisQty											
						totalVal = totalVal + (thisQty * thisPrice)						
						someSKUS = true;
						someSKUS = true;
					}
				}	
			}
			 			
			 //alert(totalNum);
			 
			 //var totalVal = (totalNum*19.99)
			 
			 $("#NotionalUnitCount").html(totalNum);
			 $("#NotionalUnitValue").html(totalVal.toFixed(2));

}	


		function saveModal() {

			//alert('save modal');
			//var rangingForm = document.getElementById("rangingForm")
			//$(rangingForm).submit();
			
			var ProductID = ''
			var qty = '';
			var sku = '';
			var variant = ''
			var qtyList = '|';
			var skuList = '|';
			var variantList = '|';
			
			$(".ModalQty").each(function() {
				//alert("mq");
				ProductID =  $( this ).data('product-id');
				qty = $( this ).val();
				sku = $( this ).data('sku');
				variant = $( this ).data('variant-id');
				
				if (qty!='') {
					//alert(qty + '_' + sku);
					qtyList = qtyList + qty + '|';
					skuList = skuList + sku + '|';
					variantList = variantList + variant + '|';
				}
			});
			
			if (qtyList!='') {
			
				$("#SKUGridContainer").html('<div class=legible><img src="/assets/images/icons/indicator.gif"> saving...</div>');
				
				$.ajax({

						scriptCharset: "ISO-8859-1" ,
						contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
						url: "/utilities/ranging/ranging_server.asp?CallType=SKU_ADD&ProductID=" + ProductID + "&RangingID=<%=RangingID%>&SKUList=" + skuList + "&QtyList=" + qtyList + "&VariantList=" + variantList,				
						cache: false, 
						
						}).done( function( html ) {
								//alert('done...')						
								$("#AjaxFeedback_MODAL").html(html);						
								//$("#AjaxModalDiv").show();
								//drawProds("")
								showRangingModal (ProductID);
								
								
								
					});				
			
				//alert(qtyList);
			}		
		
		}
		
		$(document).on("click", '#btnSaveRanging', function() {		
			
			saveModal()
			
		});		
	
</script>

<%
function getCheckOption(pRangingID, pProductID)
	if pRangingID > 0 then
		'getCheckOption = "<a onclick=""addRemoveRanging(" & pProductID & ", this)"" data-context='REMOVE' class=""label label-success""><i xdata-product-id=""" & pProductID & """ class=""xaddRange xpointer icon icon-ok icon-white""></i></a>"			
		getCheckOption = "<span onclick=""showRangingModal(" & pProductID & ");"" data-context='REMOVE' class=""gbbMarker_" & pProductID & " pointer label label-success""><i xdata-product-id=""" & pProductID & """ class=""gbbIcon_" & pProductID & " icon icon-ok icon-white""></i></span>"			
	else		
		'getCheckOption = "<a onclick=""addRemoveRanging(" & pProductID & ", this)"" data-context='ADD' class=""label label-important""><i xdata-product-id=""" & pProductID & """ class=""xaddRange xpointer icon icon-remove icon-white""></i></a>"
		getCheckOption = "<span onclick=""showRangingModal(" & pProductID & ");"" data-context='ADD' class=""gbbMarker_" & pProductID & " pointer label label-important""><i xdata-product-id=""" & pProductID & """ class=""gbbIcon_" & pProductID & " icon icon-remove icon-white""></i></span>"
	end if
end function
%>

	<!-- Modal -->
	<div id="rangingModal" class="modal hide fade large" style="" tabindex="-1" role="dialog" aria-labelledby="rangingModalLabel" aria-hidden="true">
	
		<div class="modal-body">
		
			<div id="AjaxFeedback_MODAL">
				
			</div>

			<div id="AjaxModalDiv">
			
			</div>
			
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			<%
			if AllowDealerChanges = 1 or isAdminUser() then
			%>
				<button id=btnSaveRanging class="btn btn-primary">Save & Close</button>
			<%
			end if
			%>
		</div>
	</div>