<% ProductID = cleannum(request("ProductID")) %>

<input class=FormHidden name="ModalProductID" id="ModalProductID" type=hidden value="<%=ProductID%>">

<%
	dbConnect()

	'jhInfo "RANGING GRID!"

	sql = "SELECT sp.ProductID, sp.ProductCode, sp.ProductName, spc.VariantCode, spc.ColorName, spc.ProductColorwayID, ColorwayQty, sp.Demographics, sp.Categories, sp.ProductTypeID, sp.ClassificationTags "
	sql = sql & "FROM SML_Products sp "
	sql = sql & "INNER JOIN SML_ProductColorways spc ON spc.ProductID = sp.ProductID "
	sql = sql & "LEFT JOIN (SELECT ProductColorwayID, ColorwayQty FROM B2B_RangingColorways WHERE ProductID = 0" & ProductID & " AND RangingID = 0" & RangingID & ") rcw ON rcw.ProductColorwayID = spc.ProductColorwayID "
	sql = sql & "WHERE sp.ProductID = 0" & ProductID & " "
    sql = sql & " AND EXISTS ( SELECT ss.SKUStatusID FROM SML_SKUS ss JOIN SML_SKUStatus st ON st.SKUStatusID = ss.SKUStatusID WHERE ss.SKU like spc.variantcode + '%' AND st.b2brangingvisible = 1 )	"
	sql = sql & "ORDER BY spc.DefaultProductImage DESC, spc.VariantCode "

	'jhAdmin sql
	x = getrs(sql, variantArr, variantC)
	'jh variantC

	sql = "SELECT ss.ProductID, ss.ProductColorwayID, ss.SKU, ss.SizeID, sz.SizeName, sz.SizeType, SKUPrice, SKUQty "
	sql = sql & "FROM SML_SKUS ss "
	sql = sql & "INNER JOIN SML_SKUStatus sta ON sta.SKUStatusID = ss.SKUStatusID "
	sql = sql & "INNER JOIN SML_SizeList sz ON sz.SizeID = ss.SizeID "
	sql = sql & "LEFT JOIN ("
	sql = sql & "SELECT StockCode, SKUPrice = "
    sql = sql & "CASE  "
	sql = sql & "WHEN " & AcDetails.CostBand & "_fon > 0 THEN b_fon "
	sql = sql & "WHEN " & AcDetails.CostBand & "_fo > 0 THEN b_fo  "  
	sql = sql & "ELSE Band" & AcDetails.CostBand 
	sql = sql & " END "
	sql = sql & "FROM END_PriceBandLookup ) pbl ON pbl.StockCode = ss.SKU "
	
	
	sql = sql & "LEFT JOIN (SELECT SKU, SKUQty FROM B2B_RangingSKUS WHERE ProductID = 0" & ProductID & " AND RangingID = 0" & RangingID & ") rs ON rs.SKU = ss.SKU "
	sql = sql & "WHERE  "
	sql = sql & "ss.ProductID = 0" & ProductID & " "
	sql = sql & "AND B2BRangingVisible = 1  "
	sql = sql & "ORDER BY ProductColorwayID, SizeID "

	jhAdmin sql
	x = getrs(sql, SKUArr, SKUC)
	'jh SKUC

	if SKUC >= 0 then
		thenlastSizeType = ""
		sizeTypeSQL = ""
		for count = 0 to SKUC
			SizeType = SKUArr(5,count)
			
			if lastSizeType <> SizeType then
				if sizeTypeSQL <> "" then
					sizeTypeSQL = sizeTypeSQL & " or "
				end if
				sizeTypeSQL = sizeTypeSQL & "SizeType = 0" & SizeType & " "
				lastSizeType = SizeType
			end if
		next
		
		sql = "SELECT SizeID, SizeName FROM SML_SizeList WHERE " & sizeTypeSQL & " ORDER BY SizeZOrder, SizeID "
		jhAdmin sql
		x = getrs(sql, SizeArr, SizeC)
		'jh SizeC
		
	end if	
	
	'jh AcDetails.CostBand
		
	sql = "SELECT ss.ProductID, "
	sql = sql & "MIN(CASE WHEN " & AcDetails.CostBand & "_fon > 0 THEN " & AcDetails.CostBand & "_fon  WHEN " & AcDetails.CostBand & "_fo > 0 THEN " & AcDetails.CostBand & "_fo ELSE band" & AcDetails.CostBand & " END) AS MinPrice, "
	sql = sql & "MAX(CASE WHEN " & AcDetails.CostBand & "_fon > 0 THEN " & AcDetails.CostBand & "_fon  WHEN " & AcDetails.CostBand & "_fo > 0 THEN " & AcDetails.CostBand & "_fo ELSE band" & AcDetails.CostBand & " END) AS MaxPrice "
	sql = sql & "FROM END_PriceBandLookup rp "
	sql = sql & "INNER JOIN SML_SKUS ss ON ss.SKU = rp.StockCode "
	sql = sql & "INNER JOIN SML_SKUStatus sta ON ss.SKUStatusID = sta.SKUStatusID "
	sql = sql & "WHERE ss.ProductID = 0" & ProductID & " AND "
	sql = sql & "("
	sql = sql & "ISNULL(" & AcDetails.CostBand & "_FON, 0) > 0 "
	sql = sql & "OR ISNULL(" & AcDetails.CostBand & "_FO, 0) > 0 "
	sql = sql & "OR ISNULL(Band" & AcDetails.CostBand & ", 0) > 0 "
	sql = sql & ")"
	sql = sql & "AND B2BRangingVisible = 1 "
	sql = sql & "GROUP BY ss.ProductID "
	jhAdmin sql	
	
	set rs = db.execute(sql)
	if not rs.eof then
		MinPrice = convertnum(rs("MinPrice"))
		MaxPrice = convertnum(rs("MaxPrice"))
		
		if MinPrice <> MaxPrice then
			PriceStr = dispCurrency(MinPrice) & " - " & dispCurrency(MaxPrice)
		else
			PriceStr = dispCurrency(MaxPrice) 
		end if
	else
		PriceStr = "?"
	end if
	rsClose()
		
	sql = "SELECT r.RangingID, r.AcCode, r.BenchmarkStartDate, r.BenchmarkEndDate  "
	sql = sql & "FROM B2B_Ranging r "	
	sql = sql & "WHERE r.RangingID = 0" & RangingID & ""
	'jh sql
	set rs = db.execute(sql)
	if not rs.eof then
		BenchmarkAcCode = rs("AcCode")
		BenchmarkStartDate = sqlDate(rs("BenchmarkStartDate"))
		BenchmarkEndDate = sqlDate(rs("BenchmarkEndDate"))
	end if
	rsClose()	
	
	if isdate(BenchmarkStartDate) and isdate(BenchmarkEndDate)  then
		BenchmarkStartDate_EX = exDate(BenchmarkStartDate)
		BenchmarkEndDate_EX = exDate(BenchmarkEndDate)
		
		sql = ""
		sql = sql & "SELECT ss.ProductID, spc.VariantCode, SUM(QtyDelivered) AS BenchMarkSum "
		sql = sql & "FROM END_SalesOrderLineValues esv INNER JOIN SML_SKUS ss ON ss.SKU = esv.stockcode "
		sql = sql & "INNER JOIN SML_ProductColorways spc ON spc.ProductColorwayID = ss.ProductColorwayID "
		sql = sql & "WHERE ss.ProductID = 0" & ProductID & " AND AccountCode = '" & BenchmarkAcCode & "' "
		sql = sql & "AND DueDate > " & BenchmarkStartDate_EX & " AND DueDate < " & BenchmarkEndDate_EX & " "
		sql = sql & "GROUP BY ss.ProductID, spc.VariantCode "
		'jhInfo sql
		
		x = getrs(sql, BenchArr, BenchC)	
		
		'jhAdmin "BenchC = " & BenchC
	else
		BenchC = -1
	end if	

	if isjh() or glShowRotationFigures then
	
		RotationWindowStartDate = dateadd("m", -6, now)	
		RotationWindowStartDate_EX = ExDate(RotationWindowStartDate)
		
		sql = "SELECT ss.ProductID, spc.VariantCode, SUM(esf.Qty) AS RotationSum " 
		sql = sql & "FROM END_SalesFigures esf "
		sql = sql & "INNER JOIN transactions t ON t.thOurRef = esf.tlOurRef "
		sql = sql & "INNER JOIN SML_SKUS ss ON ss.SKU = esf.idxStockCode "
		sql = sql & "INNER JOIN SML_ProductColorways spc ON spc.ProductColorwayID = ss.ProductColorwayID "
		sql = sql & "WHERE ss.ProductID = 0" & ProductID & " AND esf.idxAcCode = '" & BenchmarkAcCode & "' AND (t.thYourRef = 'STOCK ROTATION' OR t.thYourRefLong = 'STOCK ROTATION/B2B') "
		sql = sql & "AND Transdate >= " & RotationWindowStartDate_EX & " "
		sql = sql & "GROUP BY ss.ProductID, spc.VariantCode "
		
		'jhAdmin sql
		x = getrs(sql, RotationArr, RotationC)
		jhAdmin "RotationC = " & RotationC
		
	else	
		RotationC = -1	
	end if			

	if variantC >= 0 then
		ThisProductID = variantArr(0,c)
		ThisProductCode = variantArr(1,c)
		ThisProductName = variantArr(2,c)
		
		ThisDemographics = variantArr(7,c)
		ThisCategories = variantArr(8,c)
		ThisProductTypeID = variantArr(9,c)
		ThisClassificationTags = "" & variantArr(10,c)
		
		ColorwayCount = 0
		SKUCount = 0
		
		
%>
	

			<!--#include virtual="/utilities/ranging/_server_ranging_goodbetterbest.asp" -->
		
	
			<div class=row>
				<div class=span12>
					<h5><%=ThisProductName%></h5>
					<span class="label label-inverse"><%=PriceStr%></span>&nbsp;&nbsp;<a class="label label-info" href="/products/?ProductID=<%=ThisProductID%>"><i class=""></i> <%=lne("ranging_prod_detail_page")%></a>
				</div>
			</div>
			<div class=row>
				<div class=span12>
					
					<div id="SKUGridContainer">
					
						<table border=1 class="ReportTable legible">
							<tr>
								<th></th>
								<th>Code</th>	
								<th>Colour</th>	
								<th>Qtys</th>		

								<% for c = 0 to SizeC
									ThisSizeName = SizeArr(1,c)
									%>
									<th><%=ThisSizeName%></th>
								<% next %>			
								
								<th style="background:royalblue;color:white;">Last</th>		
								<th title="# Rotated within the last 6 months" style="background:crimson;color:white;">Rotated</th>									
							</tr>
							<%
							
							for c = 0 to variantC
								
								ThisProductColorwayID = variantArr(5,c)
								ThisVariantCode = variantArr(3,c)
								ThisColorwayQty = convertnum(variantArr(6,c))
								if ThisColorwayQty = 0 then ThisColorwayQty = ""
								
								if LastVariantCode <> ThisVariantCode then
									
									ThisBenchmarkSum = ""
									if BenchC >= 0 then
										for y = 0 to BenchC
											if BenchArr(1,y) = ThisVariantCode then
												ThisBenchmarkSum = convertNum(BenchArr(2,y))
												exit for
											end if
										next
									end if
									
									ThisRotationSum = ""
									if RotationC >= 0 then
										for y = 0 to RotationC
											if RotationArr(1,y) = ThisVariantCode then
												ThisRotationSum = convertNum(RotationArr(2,y))
												exit for
											end if
										next
									end if									
								
									ColorwayCount = ColorwayCount + 1
								
									imgURL = getImageDefault(ThisProductCode, ThisVariantCode)
									ThisColorName = variantArr(4,c)
					%>
									<tr>
										<td><img style="width:30px;" src="<%=imgURL%>"></td>
										<td><strong><%=ThisVariantCode%></strong></td>
										<td><%=ThisColorName%></td>
									
										<td style="background:lightyellow;text-align:right;">
											<h3><%=ThisColorwayQty%></h3>
										</td>	
					<%
							for x = 0 to SizeC
								ThisSizeID = SizeArr(0,x)
								ThisSizeName = SizeArr(1,x)
								ThisSKU = ""
								ThisSKUPrice = 0
								ShowSKU = false
								for z = 0 to SKUC
									if "" & ThisProductColorwayID = "" & SKUArr(1,z) and "" & ThisSizeID = "" & SKUArr(3,z) then 
										ThisSKU = SKUArr(2,z)
										ThisSKUPrice = SKUArr(6,z)
										ThisSKUQty = convertnum(SKUArr(7,z))
										if ThisSKUQty = 0 then ThisSKUQty = ""
										
										ShowSKU = true
										SKUCount = SKUCount + 1
										exit for
										
									end if
								next
						
					%>
								<td width=20>
									<%
									if ShowSKU then
										if SKUCount = 1 then afStr = "autofocus" else afStr = ""
										'jh ThisSKUPrice
										if AllowDealerChanges or AllowAdminChanges then
									%>
										<input title="<%=ThisSKU%>" <%=afStr%> onkeyup="checkEnterPopup();" data-sku="<%=ThisSKU%>" AUTOCOMPLETE=off data-sku-price="<%=ThisSKUPrice%>" data-product-id="<%=ProductID%>" data-variant-id="<%=ThisProductColorwayID%>" style="width:20px;" type=text class="form-control numeric SKUQty RangingQty  legible ModalQty" name="SKUQty_<%=SKUCount%>" id="SKUQty_<%=SKUCount%>" xplaceholder="<%=ThisSizeName%>" value="<%=ThisSKUQty%>">
										<input class=FormHidden type=hidden name="SKU_<%=SKUCount%>" value="<%=ThisSKU%>">
										<input class=FormHidden type=hidden name="SKUProductColorwayID_<%=SKUCount%>" value="<%=ThisProductColorwayID%>">
										
									<%
										else
										%>
											<span style="" class="label label-inverse"><%=ThisSKUQty%></span>
										<%
										end if
									end if
									%>
									
								</td>
					<%
					next
					%>	
										<td style="text-align:right;background:royalblue;color:white;">
											<%=ThisBenchmarkSum%>
										</td>	
										<td style="text-align:right;background:Crimson;color:white;">
											<%=ThisRotationSum%>
										</td>										
									</tr>
					<%				
									LastVariantCode = ThisVariantCode
								end if
							next
						%>
						
					</table>	
					
					<div class=spacer10>
					</div>
					
					<input class=FormHidden type=hidden name=RangingID value="<%=RangingID%>">
					<input class=FormHidden type=hidden name=ProductID value="<%=ProductID%>">
					<input class=FormHidden type=hidden name=ColorwayCount value="<%=ColorwayCount%>">
					<input class=FormHidden type=hidden name=SKUCount value="<%=SKUCount%>">
					<input class=FormHidden type=hidden name=FormAction value="QTY_SAVE">
					
				
				</div>

				
			</div>
<% end if %>