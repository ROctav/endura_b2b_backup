<div class=row-fluid>
    <div class=span12 style="padding:10px 0px;">
        <strong><small>GOOD / BETTER / BEST <span class="label label-success">(&pound; &gt; &pound;&pound;&pound;)</span></small></strong>
    
            <%
            'jh "ThisDemographics = " & ThisDemographics
            'jh "ThisCategories = " & ThisCategories
            'jh "ThisProductTypeID = " & ThisProductTypeID
            'jh "ThisClassificationTags = " & ThisClassificationTags
        
            gbbWP = ""
            if instr(1, ThisClassificationTags, "|WP|") > 0 then
                
                'jh "ThisClassificationTags = " & ThisClassificationTags
                gbbWP = 1
            else
                gbbWP = 0
            end if
    
            sql = "exec [spB2BRangingGoodBetterBest] 0" & RangingID & ", 0" & ThisProductTypeID & ", '" & ThisCategories & "', '" & ThisDemographics & "', " & gbbWP & ""
            'jh sql
    
            x = getrs(sql, gbbArr, gbbc)
            
            gbbLastID = ""
            gbbFound = false
            gbbProductCount = 0
            
            'jh "ProductID = " & ProductID
            if gbbc >= 0 then
            
                redim gbbReportArr(5, gbbc+1)
            
                for c = 0 to gbbc
                    gbbThisProductID = gbbArr(0,c)
                    gbbThisProductCode = gbbArr(1,c)
                    gbbThisVariantCode = gbbArr(2,c)
                    gbbThisProductName = gbbArr(3,c)
                    gbbThisRanged = convertnum(gbbArr(4,c))
                    'jh "gbbThisRanged = " & gbbThisRanged
                    
                    
                    if gbbLastID <> gbbThisProductID then
                    
                        gbbProductCount = gbbProductCount + 1
                        gbbReportArr(0, gbbProductCount) = gbbThisProductID
                        gbbReportArr(1, gbbProductCount) = gbbThisProductCode
                        gbbReportArr(2, gbbProductCount) = gbbThisVariantCode
                        gbbReportArr(3, gbbProductCount) = gbbThisProductName
                        gbbReportArr(4, gbbProductCount) = gbbThisRanged
                    
                        'jh "gbbThisProductCode = " & gbbThisProductCode
                        if "" & gbbThisProductID = "" & ProductID then
                            'jhInfo "gbbThisProductCode = " & gbbThisProductCode
                            gbbFound = true
                            gbbThisProductPosition = gbbProductCount
                        end if
                        
                        gbbLastID = gbbThisProductID
                    end if
                    
                    
                next
                
                
                if gbbFound then
                    
                    gbbos = "<div class=row>"
                
                    'jhError "gbbProductCount = " & gbbProductCount
                    gbbStart = gbbThisProductPosition - 2
                    gbbEnd = gbbThisProductPosition + 3
                    if gbbStart < 1 then gbbStart = 1
                    if gbbEnd > gbbProductCount then gbbEnd = gbbProductCount
                    
                    for c = gbbStart to gbbEnd
                    
                        gbbProductID =  gbbReportArr(0, c) 
                        gbbProductCode =  gbbReportArr(1, c) 
                        gbbVariantCode =  gbbReportArr(2, c) 
                        gbbProductName =  gbbReportArr(3, c) 
                        gbbThisRanged =  gbbReportArr(4, c) 
                        
                        gbbImgURL = getImageDefault(gbbProductCode, gbbVariantCode)
                        
                        gbbos = gbbos & "<div class=""span2 text-center"" style=""height:100px"">"
                        
                        if gbbThisRanged > 0 then
                            CheckOption = "<a onclick=""addRemoveRangingModal(" & gbbProductID & ", " & ProductID & ", 'REMOVE')"" class=""label label-success""><i xdata-product-id=""" & ThisProductID & """ class=""xaddRange xpointer icon icon-ok icon-white""></i></a>"			
                        else
                            'CheckOption = "<span class=""label label-important""><i data-product-id=""" & ThisProductID & """ data-context=""ADD"" class=""addRange pointer icon icon-remove icon-white""></i></span>"				
                            CheckOption = "<a onclick=""addRemoveRangingModal(" & gbbProductID & ", " & ProductID & ", 'ADD')"" class=""label label-important""><i xdata-product-id=""" & ThisProductID & """ class=""xaddRange xpointer icon icon-plus icon-white""></i></a>"
                        end if
    
                        if c = gbbThisProductPosition then
                        
                            gbbos = gbbos & "<div style=""border:2px solid black;background:white;padding:5px;"">"
                            gbbos = gbbos & "<div class=pull-left>"
                            gbbos = gbbos & CheckOption
                            gbbos = gbbos & "</div>"
                            gbbos = gbbos & "<div><img style=""width:60px;"" class=ProdThumb_60 src=""" & gbbImgURL & """></div>"
                            gbbos = gbbos & "<div style=""color:green;font-weight:bold;"">" & gbbProductName & "</div>"
                            gbbos = gbbos & "</div>"
    
                        else
                            
                            gbbos = gbbos & "<div style=""border:2px solid #f0f0f0;;padding:5px;"">"								
                            gbbos = gbbos & "<div class=pull-left>"
                            gbbos = gbbos & CheckOption
                            gbbos = gbbos & "</div>"									
                            gbbos = gbbos & "<xa class=pointer onclick=""saveModal();showRangingModal('" & gbbProductID & "');"">"
                            gbbos = gbbos & "<div><img style=""width:60px;"" class=ProdThumb_60 src=""" & gbbImgURL & """></div>"
                            gbbos = gbbos & "<div xstyle=""color:green;font-weight:bold;"">" & gbbProductName & "</div>"
                            gbbos = gbbos & "</xa>"
                            gbbos = gbbos & "</div>"
                            
                            
                        end if
                        
                        gbbos = gbbos & "</div>"
                        

                    next
                    
                    gbbos = gbbos & "</div>"
                    
                    
                end if
            end if
            
            if gbbProductCount <= 1 then 
                response.write "<div class=""legible subtle"">No good/better/best products found</div>"
            else
            %>
                <div class="legible well" style="padding-left:20px">	
                <%
                =gbbos
                %>
                </div>
            <%
            end if
            %>
                                
    </div>
</div>	