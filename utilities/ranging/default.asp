<%
PageTitle = "Ranging Proposal"
%>

<!--#include virtual="/assets/includes/header.asp" -->



<style>

.modal-body {
    max-height: 80vh; //Sets the height to 80/100ths of the viewport.
}

	.leftFilter {
		font-family:arial !important;
		font-size:9pt !important;
	}
	.leftNavHolder {
		border-radius:3px;
		background:#f0f0f0;
		padding:10px;
		margin-bottom:5px;
	}
	
	.accordion-inner {
		padding:5px 15px;
	}
	
	.VariantTile {
		display:inline-block;
		width:20px;
		height:5px;
		border:1px solid #333;
		margin-right:3px;
	}
	.VariantTileImage {
		width:20px;
		height:5px;
	}
	
	.pointer {
		cursor:pointer;
	}
	
	.TopNavSep {
		border-bottom:1px solid #f0f0f0;padding:1px 0;
	}
	
	.NavHeading {
		color:orange;
		font-weight:bold;
		padding:10px 0 3px 0;
		font-size:110%;
	}
	
	.modal.large {
		width: 80%; /* respsonsive width */
		margin-left:-40%; /* width/2) */ 
	}	
	
	.colorwayQty {
		padding:2px !important;
		width:20px !important;
		border:1px solid silver;
	}
	
</style>

<%

function isCheckedFilter(byval pName)
	
	dim currentFilter
	
	currentFilter = session("ProdFilter")
	'jh "currentFilter = " & currentFilter
	
	if instr(1, currentFilter, "|" & pName & "|") > 0 then
		isCheckedFilter = "CHECKED"
	end if
	
end function

'session("ProdFilter") = ""

'jh "session(ProdFilter) = " & session("ProdFilter")

if not glRangingActive and not isjh() then 
	jhErrorNice "Ranging Proposal tool is offline at present while updates are being made"
	dbClose()
	response.end
end if

RangingID = cleannum(request("RangingID"))

if RangingID = "" then RangingID = session("RangingID")

if RangingID <> "" then
	sql = "SELECT RangingID, AcCode, RangingTitle, RangingDescription, AllowDealerView, AllowDealerChanges, AllowDealerPrices, AllowDealerBudget, RangingStatusID, NewSeasonCodes "
	sql = sql & "FROM B2B_Ranging WHERE RangingID = 0" & RangingID & " "
	if not isAdminUser then
		sql = sql & "AND AcCode = '" & AcDetails.AcCode & "' "
		sql = sql & "AND AllowDealerView = 1 "
	end if
	'jh sql
	set rs = db.execute(sql)
	
	if not rs.eof then
		RangingTitle = rs("RangingTitle")
		RangingAcCode = rs("AcCode")
		RangingDescription = rs("RangingDescription")
		RangingStatusID = rs("RangingStatusID")
		
		AllowDealerView = convertnum(rs("AllowDealerView"))
		AllowDealerChanges = convertnum(rs("AllowDealerChanges"))
		AllowDealerPrices = convertnum(rs("AllowDealerPrices"))
		AllowDealerBudget = convertnum(rs("AllowDealerBudget"))
		
		NewSeasonCodes = trim("" & rs("NewSeasonCodes"))
		
		if isAdminUser() and RangingStatusID = 0 then AllowAdminChanges = 1 else AllowAdminChanges = 0
		if RangingStatusID > 0 then AllowDealerChanges = 0
		
		if 1 = 0 then
			jhInfo "AllowDealerView = " & AllowDealerView
			jhInfo "AllowDealerChanges = " & AllowDealerChanges
			jhInfo "AllowDealerPrices = " & AllowDealerPrices
			jhInfo "AllowDealerBudget = " & AllowDealerBudget
			jhInfo "AllowAdminChanges = " & AllowAdminChanges
		end if
		session("RangingID") = RangingID
	else
		dbClose() 
		response.redirect "/"
	end if
	
	rsClose()
end if

RangingID = session("RangingID")

'jh "isDealer() = " & isDealer()
'jh "isAdminUser() = " & isAdminUser()

if isDealer() and not isAdminUser() then
	call viewedRanging_DEALER(RangingID)
end if

jhadmin "NOW GET INVALID SKUS."


function recalcRanging(byval pRangingID)

	dim SomeInvalid, c, ThisSKU, DelListSKUS, ThisColorwayID, DelListColorways, DelListProducts, ThisProductID

	jhadmin "INVALID SKUS?"
	sql = "SELECT rs.SKU, ss.SKUStatusID, sta.SKUStatusName, B2BRangingVisible, rs.ProductID, sp.ProductName "
	sql = sql & "FROM B2B_RangingSKUS rs "
	sql = sql & "INNER JOIN SML_Products sp ON rs.PRoductID = sp.ProductID "
	sql = sql & "LEFT JOIN SML_SKUS ss ON ss.SKU = rs.SKU "
	sql = sql & "LEFT JOIN SML_SKUStatus sta ON sta.SKUStatusID = ss.SKUStatusID "
	sql = sql & "WHERE rs.RangingID = 0" & pRangingID & " "
	sql = sql & "AND B2BRangingVisible = 0 "
	sql = sql & "ORDER BY sp.ProductID "
	jhadmin sql
	
	x = getrs(sql, InvalidArr, InvalidC)
	jhAdmin "InvalidC = " & InvalidC
	if InvalidC >= 0 then
		SomeInvalid = true
		for c = 0 to InvalidC
			ThisSKU = InvalidArr(0,c)
			sql = "DELETE FROM B2B_RangingSKUS WHERE RangingID = 0" & pRangingID & " AND SKU = '" & ThisSKU & "'" 	
			jhAdmin sql
			db.execute(sql)
			DelListSKUS = DelListSKUS & ThisSKU & ", "
		next
	end if
	
	jhadmin "INVALID COLORWAYS?"
	sql = "SELECT rcp.ProductID, rcp.ProductColorwayID, ValidCount "
	sql = sql & "FROM B2B_RangingColorways rcp "
	sql = sql & "LEFT JOIN ( "
	sql = sql & "SELECT ss.ProductColorwayID, COUNT(SKU) AS ValidCount "
	sql = sql & "FROM SML_SKUS ss "
	sql = sql & "INNER JOIN SML_SKUStatus sta ON sta.SKUStatusID = ss.SKUStatusID "
	sql = sql & "WHERE B2BRangingVisible = 1 "
	sql = sql & "GROUP BY ss.ProductColorwayID "
	sql = sql & ") vs ON vs.ProductColorwayID = rcp.ProductColorwayID "
	sql = sql & "WHERE RangingID = 0" & pRangingID & "  AND ISNULL(ValidCount, 0) = 0 "
	sql = sql & "ORDER BY rcp.ProductID, rcp.ProductColorwayID	 "
	jhadmin sql
	x = getrs(sql, InvalidArr, InvalidC)
	jhAdmin "InvalidC = " & InvalidC
	if InvalidC >= 0 then
		SomeInvalid = true
		for c = 0 to InvalidC
			ThisProductID = InvalidArr(0,c)
			ThisColorwayID = InvalidArr(1,c)
			sql = "DELETE FROM B2B_RangingColorways WHERE RangingID = 0" & pRangingID & " AND ProductID = 0" & ThisProductID  & " AND ProductColorwayID = 0" & ThisColorwayID 
			jhAdmin sql
			db.execute(sql)
			DelListColorways = DelListColorways & ThisColorwayID & ", "
		next
	end if	
	
	jhadmin "INVALID PRODUCTS?"
	sql = "SELECT rp.ProductID, ValidCount "
	sql = sql & "FROM B2B_RangingProducts rp "
	sql = sql & "LEFT JOIN ( "
	sql = sql & "SELECT ss.ProductID, COUNT(SKU) AS ValidCount "
	sql = sql & "FROM SML_SKUS ss "
	sql = sql & "INNER JOIN SML_SKUStatus sta ON sta.SKUStatusID = ss.SKUStatusID "
	sql = sql & "INNER JOIN END_PriceBandLookup rp ON rp.StockCode = ss.SKU "
	sql = sql & "WHERE B2BRangingVisible = 1 AND "
	sql = sql & "("
	sql = sql & "rp." & AcDetails.CostBand & "_FO > 0 "
	sql = sql & "OR rp." & AcDetails.CostBand & "_FON > 0 "
	sql = sql & "OR rp.Band" & AcDetails.CostBand & " > 0 "
	sql = sql & ")"
	sql = sql & "GROUP BY ss.ProductID "
	sql = sql & ") vs ON vs.ProductID = rp.ProductID "
	sql = sql & "WHERE RangingID = 0" & pRangingID & " AND ISNULL(ValidCount, 0) = 0 "
	sql = sql & "ORDER BY rp.ProductID "
	jhadmin sql
	x = getrs(sql, InvalidArr, InvalidC)
	jhAdmin "InvalidC = " & InvalidC
	if InvalidC >= 0 then
		SomeInvalid = true
		for c = 0 to InvalidC
			ThisProductID = InvalidArr(0,c)
			sql = "DELETE FROM B2B_RangingProducts WHERE RangingID = 0" & pRangingID & " AND ProductID = 0" & ThisProductID & "" 	
			jhAdmin sql
			db.execute(sql)
			DelListProducts = DelListProducts & ThisProductID & ", "
		next
	end if		
	
	sql = "UPDATE B2B_RangingColorways "
	sql = sql & "SET B2B_RangingColorways.ColorwayQty = SKUTotal "
	sql = sql & "FROM B2B_RangingColorways "
	sql = sql & "INNER JOIN  "
	sql = sql & "( "
	sql = sql & "SELECT ProductColorwayID, SUM(SKUQty) AS SKUTotal "
	sql = sql & "FROM B2B_RangingSKUS  "
	sql = sql & "WHERE RangingID = 0" & pRangingID & "  "
	sql = sql & "GROUP BY ProductColorwayID "
	sql = sql & ") s ON s.ProductColorwayID = B2B_RangingColorways.ProductColorwayID "
	sql = sql & "WHERE RangingID = 0" & pRangingID & " "
	jhAdmin sql
	db.execute(sql)
	
	sql = "UPDATE B2B_RangingProducts "
	sql = sql & "SET B2B_RangingProducts.UnitQty = ColorwayTotal "
	sql = sql & "FROM B2B_RangingProducts "
	sql = sql & "INNER JOIN  "
	sql = sql & "( "
	sql = sql & "SELECT ProductID, SUM(ColorwayQty) AS ColorwayTotal "
	sql = sql & "FROM B2B_RangingColorways "
	sql = sql & "WHERE RangingID = 0" & pRangingID & "  "
	sql = sql & "GROUP BY ProductID "
	sql = sql & ") s ON s.ProductID = B2B_RangingProducts.ProductID "
	sql = sql & "WHERE RangingID = 0" & pRangingID & " "
	jhAdmin sql	
	db.execute(sql)
		
	'jhstop	
		
	if SomeInvalid then 
	
		recalcRanging = true	
		sql = "INSERT INTO B2B_RangingRecalcs (RangingID, SKUs, ColorwayIDs, ProductIDs) VALUES ("
		sql = sql & "0" & pRangingID & ", "
		sql = sql & "'" & validstr(DelListSKUS) & "', "
		sql = sql & "'" & validstr(DelListColorways) & "', "
		sql = sql & "'" & validstr(DelListProducts) & "') "
		jhAdmin sql
		db.execute(sql)
		
	end if
		
	'jhstop
	
end function

if isjh() and RangingStatusID <> 10 then 
	RangingRecalced = recalcRanging(RangingID)
end if

if RangingRecalced then
	
	jhErrorNice "Invalid products found", "<div class=spacer20>Some products in your ranging proposal have been removed as they are no longer valid for display in your ranging proposal (usually because they have since been discontinued, made dead, or added to clearance)<div>Your ranging proposal totals have been recalculated</div>"
	
end if


if RangingID = "" then
	jhError "NO RANGING PROPOSAL SELECTED - RETURN TO MAIN ADMIN PAGE"
	FormAction = ""
	dbClose()
	response.redirect "/admin/ranging/"
end if

FormAction = request("FormAction")

if request("InitFilter") = "RANGED" then
	session("ProdFilter") = "|STATUS_1|"
end if 
'jhInfo "FormAction = " & FormAction

if FormAction = "xCWAY_SAVE" then
	
	'jhInfo "SAVE CWAYS!"
	RangeChanges = request("RangeChanges")
	'jh "RangeChanges = " & RangeChanges
	
	if RangeChanges <> "" then
	
		ProcessedCodes = "|"
		RangeArr = split(RangeChanges, "|")
		
		for c = ubound(RangeArr) to 0 step - 1
		
			ThisItem = RangeArr(c)
			
			if ThisItem <> "" then
				ItemArr = split(ThisItem, "_")
				'jh "ubound! " & ubound(ItemArr)
				if ubound(ItemArr) = 2 then
					
					ThisProductID = ItemArr(0)
					ThisColorwayID = ItemArr(1)
					ThisQty = convertnum(ItemArr(2))
					
					'jh ThisProductID & "_" & ThisColorwayID & "_" & ThisQty
					
					if instr(1, ProcessedCodes, "|" & ThisColorwayID & "|") = 0 then
						'jhInfo "INSERT - " & ThisColorwayID
						ProcessedCodes = ProcessedCodes & ThisVariantCode & "|"
						
						sql = "DELETE FROM B2B_RangingProducts WHERE RangingID = 0" & RangingID & " AND ProductID = 0" & ThisProductID
						'jh sql
						db.execute(sql)
						
						sql = "DELETE FROM B2B_RangingColorways WHERE RangingID = 0" & RangingID & " AND ProductColorwayID = 0" & ThisColorwayID
						'jh sql
						db.execute(sql)
						
						if ThisQty > 0 then
							
							sql = "INSERT INTO B2B_RangingColorways (RangingID, ProductID, ProductColorwayID, ColorwayQty) VALUES ("
							sql = sql & "0" & RangingID & ", "
							sql = sql & "0" & ThisProductID & ", "
							sql = sql & "0" & ThisColorwayID & ", "
							sql = sql & "0" & ThisQty & ") "
							
							'jh sql
							db.execute(sql)
							
						end if
																
						sql = "INSERT INTO B2B_RangingProducts (RangingID, ProductID) "
						sql = sql & "SELECT DISTINCT 0" & RangingID & ", ProductID FROM B2B_RangingColorways "
						sql = sql & "WHERE RangingID = 0" & validstr(RangingID) & " AND ProductID = 0" & ThisProductID & " "
						sql = sql & "AND NOT EXISTS (SELECT * FROM B2B_RangingProducts WHERE RangingID = 0" & validstr(RangingID) & " AND ProductID = 0" & ThisProductID & ") "
						'jh sql	
						db.execute(sql)								

					end if
					
				end if
			end if
		next
		
		jhInfoNice "Quantitys have been updated!", "See below:"
		
		
		session("LastSavedProductID") = ProductID
		dbClose()
		response.redirect "/utilities/ranging/?Saved=1"
		
	end if
	
end if


if request("all") = "1" then session("ProdFilter") = ""

'jh "session(ProdFilter) = " & session("ProdFilter")

if request("SetFilter") <> "" then
	'jhInfo  "INITIALISE FILTER!"
	FilterStr = request("FilterStr")
	session("ProdFilter")  = FilterStr
	if request("CodeFilter") <> "" then
		session("CodeFilter") = request("CodeFilter")
	end if
	session("ProdFilter_SEARCH") = ""
end if

BrowserSearchVal = session("ProdFilter_SEARCH")

if instr(1, session("ProdFilter"), "ENDUSE_") > 0 then 
	EndUseCollapseClass = "in"	
end if

if instr(1, session("ProdFilter"), "RANGE_") > 0 then 
	CollectionCollapseClass = "in"	
end if

if instr(1, session("ProdFilter"), "NAV_CAT_") > 0 then 
	ProdTypeCollapseClass = "in"	
end if

if instr(1, session("ProdFilter"), "NAV_OUTFIT_") > 0 then 
	OutfitCollapseClass = "in"	
end if



dbConnect()

sql = "SELECT SMLStatusID, SMLStatusName FROM SML_Status s WHERE SMLTypeID = 951 "
if NewSeasonCodes = "" then
	sql = sql & "AND SMLStatusID <> 3 "
end if
sql = sql & "ORDER BY StatusZOrder"
jhAdmin sql
x = getrs(sql, RangeStatusArr, RangeStatusC)

sql = "SELECT RangeID, RangeName FROM SML_Ranges r WHERE RangeStatusID = 1 ORDER BY RangeZORder"
x = getrs(sql, RangeArr, RangeC)

sql = "SELECT CategoryID, CategoryName FROM SML_Categories c WHERE CategoryID IN (1,2,18,14) ORDER BY CategoryZORder"
x = getrs(sql, EndUseArr, EndUseC)

sql = "SELECT OutfitID, OutfitName FROM SML_ProductOutfits spo WHERE spo.OutfitStatusID IN (1) ORDER BY OutfitZOrder, OutfitID"
x = getrs(sql, OutfitArr, OutfitC)

sql = "SELECT Params, SMLStatusName FROM SML_Status s WHERE SMLTypeID = 120 AND StatusZOrder <> 99 ORDER BY StatusZOrder"
x = getrs(sql, OtherArr, OtherC)

'jh "RangeC = " & RangeC

sql = "SELECT d.DemographicID, d.DemographicName, TranslatedValue "
sql = sql & "FROM SML_Demographics d "
sql = sql & "LEFT JOIN (SELECT IDValue, TranslatedValue FROM SML_ListsItems_TRANSLATED WHERE LanguageID = 0" & SelectedLanguageID & " AND ListTypeID = 120) tra ON tra.IDValue = d.DemographicID "
sql = sql & "ORDER BY d.DemographicZOrder "
'jh sql
x = getrs(sql, DemogArr, DemogC)

sql = "SELECT nc.NavClassificationID, nc.NavClassificationName, nc.NavClassificationTopLevel, TranslatedValue "
sql = sql & "FROM SML_NavClassifications nc "
sql = sql & "LEFT JOIN (SELECT IDValue, TranslatedValue FROM SML_ListsItems_TRANSLATED WHERE LanguageID = 0" & SelectedLanguageID & " AND ListTypeID = 100) tra ON tra.IDValue = nc.NavClassificationID "
sql = sql & "ORDER BY nc.NavClassificationZOrder,  nc.NavClassificationID "

'jh sql
'jhstop
x = getrs(sql, NavArr, NavC)

sql = "SELECT cwls.SimpleColorID, cwls.SimpleColorName, cwls.SimpleWebColor, cwls.ColorBar, TranslatedValue "
sql = sql & "FROM SML_ColorwayList_SIMPLE cwls "
sql = sql & "LEFT JOIN (SELECT IDValue, TranslatedValue FROM SML_ListsItems_TRANSLATED WHERE LanguageID = 0" & SelectedLanguageID & " AND ListTypeID = 110) tra ON tra.IDValue = cwls.SimpleColorID "
sql = sql & "ORDER BY cwls.SimpleColorZOrder, cwls.SimpleColorID "
'jh sql
x = getrs(sql, SimpleArr, SimpleC)

'jh "SimpleC = " & SimpleC

'jh "NavC = " & NavC


if request("cf") <> "" then 
	session("showcurrentfilter") = "1"
end if


%>

    <div id="">
	
		
	
		<div id="FilterOutput">
		
		</div>
		

		
    </div>

</div>

<%



%>

<div class="container spacer40">	
	
	
<div class=row>
	<div class=span12>
		

	
	
	
<%


'jhstop
%>			
	</div>
</div>

<div class=row>
		<div class=span12>		
		
			<hr>
				<div class=spacer20>
					<span class="label label-inverse"><%=AcDetails.AcCode%></span> <span class=legible><%=AcDetails.CompanyName%></span>
					<h3><%=RangingTitle%><br><small><%=RangingDescription%></small></h3>
					<%
					if isAdminUser() then
					%>
						<a class=OrangeLink href="/admin/ranging/">Ranging Admin</a> | 
						<a class=OrangeLink href="/admin/ranging/ranging-detail/?RangingID=<%=RangingID%>"><i class="icon icon-cog"></i></a> | 
						<a class=OrangeLink href="/admin/ranging/price-list/?RangingID=<%=RangingID%>"><i class="icon icon-list"></i></a> | 
					<%
					end if
					%>
					
					<a class=OrangeLink href="/utilities/ranging/gbb-report/"><%=lne("ranging_gbb_report")%></a> | <a class=OrangeLink href="/utilities/ranging/ranging-report/"><%=lne("ranging_summary_report")%></a>
				</div>				
				
				<form class="hidden" name=cwayForm id=cwayForm action="/utilities/ranging/default.asp#ProdListing" method=post>
					<textarea class=form-control name="RangeChanges" id="RangeChanges"></textarea>
					<input class=FormHidden type=hidden name=FormAction value="CWAY_SAVE">
					
				</form>
				
				<div id="AjaxFeedback">
				
				</div>
			<hr>
		
		
		</div>
	</div>
	
	<div class="row">
		
		<div class="span3 leftFilter">

			<div class=well>
				<div><%=lne_B2C("search_for")%></div>
				
				<div class="input-append">
				  <input style="width:70%;" class="span2" id="BrowserSearchVal" name=BrowserSearchVal type="text" value="<%=BrowserSearchVal%>">
				  <button id="BrowserSearchVal_GO" class="btn" type="button">&nbsp;<i class="icon icon-search"></i>&nbsp;</button>
				</div>				
				
			</div>		
		
			<div class="accordion" id="accordionRangingStatus">
			  <div class="accordion-group">
				<div class="accordion-heading">
				  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionRangingStatus" href="#collapseRangingStatus">
					<strong><i class="icon icon-chevron-down"></i> <%=lne_B2C("product_nav_ranging_status")%></strong>
				  </a>
				</div>
				<div id="collapseRangingStatus" class="accordion-body collapse in">
					<%
					=getLeftNavClassifications("STATUS")
					%>		  
				</div>
			  </div>
			</div>			


			<div class="accordion" id="accordionGender">
			  <div class="accordion-group">
				<div class="accordion-heading">
				  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionGender" href="#collapseGender">
					<strong><i class="icon icon-chevron-down"></i> <%=lne_B2C("product_nav_gender")%></strong>
				  </a>
				</div>
				<div id="collapseGender" class="accordion-body collapse in">
					<%
					=getLeftNavClassifications("DEMOGRAPHIC")
					%>		  
				</div>
			  </div>
			</div>

			<div class="accordion" id="accordionEndUse">
			  <div class="accordion-group">
				<div class="accordion-heading">
				  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionEndUse" href="#collapseEndUse">
					<strong><i class="icon icon-chevron-down"></i> <%=lne("ranging_filter_end_use")%></strong>
				  </a>
				</div>
				<div id="collapseEndUse" class="accordion-body collapse <%=EndUseCollapseClass%>">
					<%
					=getLeftNavClassifications("ENDUSE")
					%>		  
				</div>
			  </div>
			</div>		

			<div class="accordion" id="accordionOther">
			  <div class="accordion-group">
				<div class="accordion-heading">
				  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionOther" href="#collapseOther">
					<strong><i class="icon icon-chevron-down"></i> <%=lne_b2c("product_nav_other")%></strong>
				  </a>
				</div>
				<div id="collapseOther" class="accordion-body collapse <%=OtherCollapseClass%>">
					<%
					=getLeftNavClassifications("OTHER")
					%>		  
				</div>
			  </div>
			</div>				
			
			<div class="accordion" id="accordionRange">
			  <div class="accordion-group">
				<div class="accordion-heading">
				  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionRange" href="#collapseRange">
					<strong><i class="icon icon-chevron-down"></i> <%=lne_B2C("product_nav_collections")%></strong>
				  </a>
				</div>
				<div id="collapseRange" class="accordion-body collapse <%=CollectionCollapseClass%>">
					<%
					=getLeftNavClassifications("RANGE")
					%>		  
				</div>
			  </div>
			</div>			

			<div class="accordion" id="accordionPRODTYPES">
			  <div class="accordion-group">
				<div class="accordion-heading">
				  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionPRODTYPES" href="#collapsePRODTYPES">
					<strong><i class="icon icon-chevron-down"></i> <%=lne_b2c("product_nav_prod_types")%></strong>
				  </a>
				</div>
				<div id="collapsePRODTYPES" class="accordion-body collapse <%=ProdTypeCollapseClass%>">
				
					<div style="background:#f0f0f0;" class="accordion-inner form-inline">				
<input <%=getAllChecks("ALL_TOPS")%> type="checkbox" class="CheckAllTypes" data-lookfor="ALL_TOPS" id="CHECK_ALL_TOPS"> <label for="CHECK_ALL_TOPS"><strong><%=lne_b2c("product_nav_prod_types_tops")%></strong></label>
					</div>					
					<%
					=getLeftNavClassifications("ALL_TOPS")
					%>		  
					<div style="background:#f0f0f0;" class="accordion-inner form-inline">		
<input <%=getAllChecks("ALL_LEGWEAR_BAGGY")%> type="checkbox" class="CheckAllTypes" data-lookfor="ALL_LEGWEAR_BAGGY" id="CHECK_ALL_LEGWEAR_BAGGY"> <label for="CHECK_ALL_LEGWEAR_BAGGY"><strong><%=lne_b2c("product_nav_prod_types_legwear_baggy")%></strong></label>					
						
					</div>					
					<%
					=getLeftNavClassifications("ALL_LEGWEAR_BAGGY")
					%>		
					<div style="background:#f0f0f0;" class="accordion-inner form-inline">		
<input <%=getAllChecks("ALL_LEGWEAR_TIGHT")%> type="checkbox" class="CheckAllTypes" data-lookfor="ALL_LEGWEAR_TIGHT" id="CHECK_ALL_LEGWEAR_TIGHT"> <label for="CHECK_ALL_LEGWEAR_TIGHT"><strong><%=lne_b2c("product_nav_prod_types_legwear_tight")%></strong></label>					
						
					</div>					
					<%
					=getLeftNavClassifications("ALL_LEGWEAR_TIGHT")
					%>						
	
					<div style="background:#f0f0f0;" class="accordion-inner form-inline">		
<input <%=getAllChecks("ALL_HARDWARE")%> type="checkbox" class="CheckAllTypes" data-lookfor="ALL_HARDWARE" id="CHECK_ALL_HARDWARE"> <label for="CHECK_ALL_HARDWARE"><strong><%=lne_b2c("product_nav_prod_types_hardware")%></strong></label>					
						
					</div>					
					<%
					=getLeftNavClassifications("ALL_HARDWARE")
					%>						
					<div style="background:#f0f0f0;" class="accordion-inner form-inline">		
<input <%=getAllChecks("ALL_ACCESSORIES")%> type="checkbox" class="CheckAllTypes" data-lookfor="ALL_ACCESSORIES" id="CHECK_ALL_ACCESSORIES"> <label for="CHECK_ALL_ACCESSORIES"><strong><%=lne_b2c("product_nav_prod_types_accessories")%></strong></label>					
						
					</div>					
					<%
					=getLeftNavClassifications("ALL_ACCESSORIES")
					%>						
				</div>
			  </div>
			</div>		
			
<%
if OutfitC > 0 then
%>			
			<div class="accordion" id="accordionOutfits">
			  <div class="accordion-group">
				<div class="accordion-heading">
				  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionOutfits" href="#collapseOutfits">
					<strong><i class="icon icon-chevron-down"></i> <%=lne("product_nav_outfits")%></strong>
				  </a>
				</div>
				<div id="collapseOutfits" class="accordion-body collapse <%=OutfitCollapseClass%>">
					<%
					=getLeftNavClassifications("OUTFIT")
					%>		  
				</div>
			  </div>
			</div>				
<%
end if
%>			
			<div class="accordion" id="accordionCOLORS">
			  <div class="accordion-group">
				<div class="accordion-heading">
				  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionCOLORS" href="#collapseCOLORS">
					<strong><i class="icon icon-chevron-down"></i> <%=lne_B2C("product_nav_colors")%></strong>
				  </a>
				</div>
				<div id="collapseCOLORS" class="accordion-body collapse in">
					<%
					=getColorClassifications()
					%>		  
				</div>
			  </div>
			</div>					
			
			
		</div>
		
		<div class=span9>
		
			<input class="FormHidden" type=hidden id="ADDSCROLL" value="">

			<div id="ListingHeader">
				
			</div>
		
			<div class="container" id=ProdListing>
			   
			</div>			
			
			<div id="ListingFooter">
				
			</div>
			
		</div>
		
	</div>
	
</div>

<!--#include virtual="/assets/includes/footer.asp" -->


<%

function getLeftNavClassifications_RADIO(pcategory)
	getLeftNavClassifications_RADIO = getLeftNavClassifications(pcategory)
	getLeftNavClassifications_RADIO = replace(getLeftNavClassifications_RADIO, "type=checkbox", "type=radio")
end function

function getLeftNavClassifications(pcategory)
	dim c, ThisID, ThisName, ThisCat, os
	
	os = ""
	
	if pcategory = "STATUS" then
		for c = 0 to RangeStatusC
			ThisID = RangeStatusArr(0,c)
			ThisName = lne("ranged_status_" & ThisID) 
			'RangeStatusArr(1,c)
			'jh "ThisID = " & ThisID
			'jhInfo ThisName
			os = os & "<div class=""accordion-inner form-inline "">"
			os = os & "<input " & isCheckedFilter(pcategory & "_" & ThisID) & " id=""STATUS_" & ThisID & """ value=""" & ThisID & """ onclick=""fClick(this, 'NAV_STATUS', '" & pcategory & "', " & ThisID & ")"" type=checkbox> <label class=leftFilter for=""STATUS_" & ThisID & """ >" & ThisName & "</label>"
			os = os & "</div>"			
			
		next		
	
	elseif pcategory = "RANGE" then
		for c = 0 to RangeC
			ThisID = RangeArr(0,c)
			ThisName = RangeArr(1,c)
			
			'jhInfo ThisName
			os = os & "<div class=""accordion-inner form-inline "">"
			os = os & "<input " & isCheckedFilter(pcategory & "_" & ThisID) & " id=""RANGE_" & ThisID & """ value=""" & ThisID & """ onclick=""fClick(this, 'NAV_RANGE', '" & pcategory & "', " & ThisID & ")"" type=checkbox> <label class=leftFilter for=""RANGE_" & ThisID & """ >" & ThisName & "</label>"
			os = os & "</div>"			
			
		next	
	elseif pcategory = "ENDUSE" then
		for c = 0 to EndUseC
			ThisID = EndUseArr(0,c)
			ThisName = EndUseArr(1,c)
			
			'jhInfo ThisName
			os = os & "<div class=""accordion-inner form-inline "">"
			os = os & "<input name=""NAV_ENDUSE"" " & isCheckedFilter(pcategory & "_" & ThisID) & " id=""ENDUSE_" & ThisID & """ value=""" & ThisID & """ onclick=""fClick(this, 'NAV_ENDUSE', '" & pcategory & "', " & ThisID & ")"" type=checkbox> <label class=leftFilter for=""ENDUSE_" & ThisID & """ >" & ThisName & "</label>"
			os = os & "</div>"			
			
		next	
	elseif pcategory = "OUTFIT" then
		for c = 0 to OutfitC
			ThisID = OutfitArr(0,c)
			ThisName = OutfitArr(1,c)
			
			'jhInfo ThisName
			os = os & "<div class=""accordion-inner form-inline "">"
			os = os & "<input name=""NAV_OUTFIT"" " & isCheckedFilter(pcategory & "_" & ThisID) & " id=""OUTFIT_" & ThisID & """ value=""" & ThisID & """ onclick=""fClick(this, 'NAV_OUTFIT', '" & pcategory & "', " & ThisID & ")"" type=checkbox> <label class=leftFilter for=""OUTFIT_" & ThisID & """ >" & ThisName & "</label>"
			os = os & "</div>"			
			
		next			
		
	elseif pcategory = "OTHER" then
	
		for c = 0 to OtherC
			ThisID = OtherArr(0,c)
			ThisName = OtherArr(1,c)
			
			'jh pcategory & "_" & ThisID
			
			'jhInfo ThisName
			os = os & "<div class=""accordion-inner form-inline "">"
			os = os & "<input " & isCheckedFilter(pcategory & "_" & ThisID) & " id=""OTHER_" & ThisID & """ value=""" & ThisID & """ onclick=""fClick(this, 'NAV_OTHER', '" & pcategory & "', '" & ThisID & "')"" type=checkbox> <label class=leftFilter for=""OTHER_" & ThisID & """ >" & ThisName & "</label>"
			os = os & "</div>"			
			
		next	
			
	
	elseif pcategory = "DEMOGRAPHIC" then
		for c = 0 to DemogC
			ThisID = DemogArr(0,c)
			
			if SelectedLanguageID <> "0" then
				ThisName = trim("" & DemogArr(2,c))
			else
				ThisName = ""
			end if			
			if ThisName = "" then ThisName = DemogArr(1,c)				
			
			'jhInfo ThisName
			os = os & "<div class=""accordion-inner form-inline "">"
			os = os & "<input " & isCheckedFilter(pcategory & "_" & ThisID) & " id=""DEMOGRAPHIC_" & ThisID & """ value=""" & ThisID & """ onclick=""fClick(this, 'NAV_DEMOG', '" & pcategory & "', " & ThisID & ")"" type=checkbox> <label class=leftFilter for=""DEMOGRAPHIC_" & ThisID & """>" & ThisName & "</label>"
			os = os & "</div>"			
			
		next
	else
		for c = 0 to NavC
			ThisID = NavArr(0,c)
			
			if SelectedLanguageID <> "0" then
				ThisName = trim("" & NavArr(3,c))
			else
				ThisName = ""
			end if
			
			if ThisName = "" then ThisName = NavArr(1,c)			
			
			ThisCat = NavArr(2,c)
			'jh "ThisCat = " & ThisCat
			if ThisCat = pcategory then
				'jhInfo ThisName
				os = os & "<div class=""accordion-inner form-inline "">"
				os = os & "<input class=""CHECK_" & pcategory & """ " & isCheckedFilter("NAV_CAT_" & ThisID) & " id=""NAV_CAT_" & ThisID & """ value=""" & ThisID & """ onclick=""fClick(this, 'NAV_CAT', 'NAV_CAT', " & ThisID & ");"" type=checkbox> <label class=leftFilter for=""NAV_CAT_" & ThisID & """>" & ThisName & "</label>"
				os = os & "</div>"			
			end if
		next
	end if
	
	getLeftNavClassifications = os
	
end function


function getColorClassifications()
	dim c, ThisID, ThisName, ThisCat, os
	
	os = ""
	os = os & "<div class=""accordion-inner form-inline"" style=""padding-right:60px;"">"
	'os = os & "<div class=row>"
		for c = 0 to SimpleC
			ThisID = SimpleArr(0,c)
			
			ThisWebColor = SimpleArr(2,c)
			ThisColorBar = SimpleArr(3,c)
			'jh "ThisCat = " & ThisCat
			if SelectedLanguageID <> "0" then
				ThisName = trim("" & SimpleArr(4,c))
			else
				ThisName = ""
			end if			
			if ThisName = "" then ThisName = SimpleArr(1,c)			
			
			'jhInfo ThisName
			
			os = os & "<div title=""" & ThisName & """ style=""background-image:url(http://www.endurasport.com/assets/images/prodbrowser/colorbars/" & ThisColorBar & ".png);background-size:30px;border-radius:3px;display:inline-block;padding:3px 7px 5px 7px;margin:5px 5px!important;xbackground:" & ThisWebColor & " !important;"">"
			os = os & "<input  " & isCheckedFilter("COLOR_" & ThisID) & " id=""COLOR_" & ThisID & """ value=""" & ThisID & """ onclick=""fClick(this, 'COLOR', 'COLOR', " & ThisID & ");"" type=checkbox> "
			os = os & "</div>"
		
		next
	'os = os & "</div>"		
	os = os & "</div>"	
	
	getColorClassifications = os
	
end function

function isCheckedFilter(byval pName)
	
	dim currentFilter
	
	currentFilter = session("ProdFilter")
	'jh "currentFilter = " & currentFilter
	
	if instr(1, currentFilter, "|" & pName & "|") > 0 then
		isCheckedFilter = "CHECKED"
	end if
	
end function

function getAllChecks(pCat)

	dim currentFilter
	
	currentFilter = session("ProdFilter")
	'jh "currentFilter = " & currentFilter
	
	if instr(1, currentFilter, "|CAT_" & pCat & "|") > 0 then
		getAllChecks = "CHECKED"
	end if
	
end function

%>



	<script>
	
	
		$(document).ready( function () {
			///alert('ready!');
			drawProds("")
			//drawProds_TOP();
		});

		
		function drawProds(pURL) {
		
			//alert(pURL)
			
			var hideModal = false;
			var CheckPagePos = false
			var scrollpid = ""
			
			if (pURL == "HIDE") { 
			
				pURL = ""
				hideModal = true;
				CheckPagePos = true;	
				scrollpid = document.getElementById("ModalProductID").value;
				//alert(scrollpid);
				
			}			
			
			if (pURL == "PAGE") { 
				pURL = ""				
				CheckPagePos = true;	
				
				scrollpid = $("#ADDSCROLL").val();
				$("#ADDSCROLL").val("");
				//alert(scrollpid);
			}		
			
			if (CheckPagePos) {
				if (document.getElementById("CurrentPage")) {
					var CurrentPage = document.getElementById("CurrentPage").value;
					var StartPos = document.getElementById("StartPos").value;
					if (CurrentPage == 'null') {CurrentPage = ''}
					if (StartPos == 'null') {StartPos = ''}	
					
					if (CurrentPage!='') {
						pURL = "&CurrentPage=" + CurrentPage + "&StartPos=" + StartPos
						hideModal = true;
						//alert(pURL)				
					}
				}				
			}
			
			//alert(CurrentPage + '_' + StartPos)			
			
			$("#ProdListing").html('<div class=legible><img src="/assets/images/icons/indicator.gif"> getting products...</div>');
			
			

				
			
			filterURL = pURL
			//alert(filterURL);
			

			
			$.ajax({

				scriptCharset: "ISO-8859-1" ,
				contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
				url: "ranging_server.asp?RangingID=<%=RangingID%>&RangingAcCode=<%=RangingAcCode%>&CallType=PROD_LIST" + filterURL,				
				cache: false, 
				
				}).done( function( html ) {
						//alert('done...')						
						$("#ProdListing").html(html);						
						
						if (scrollpid != '') {
							//alert("OK, SCROLL TO: " + scrollpid)							
							var scrollElement = document.getElementById("PRODSCROLL_"+scrollpid);
							//alert(jQuery(scrollElement).position().top)
							if(jQuery(scrollElement).position()){
								
								jQuery('html,body').animate({scrollTop:jQuery(scrollElement).position().top}, 0);
															
							}									
							
						} else {						
						
							if(jQuery("#ListingFooter").position()){
								if(jQuery("#ListingFooter").position().top < jQuery(window).scrollTop()){
									//scroll up
									jQuery('html,body').animate({scrollTop:jQuery("#ListingHeader").position().top}, 1000);
								}							
							}						
						}
						if (hideModal) {
							$('#rangingModal').modal('hide');			
						}
						

						
			});			
					
			
		}
		
	
		function sortClick(pSortType) {
			drawProds('&SortDir=' + pSortType) 	
		}
				
				

		function fClick(pObj, pType, pCategory, pid) {			
			var context = '';
			
			//alert('fClick');
			
			if ( $(pObj).prop('checked') ) {
				context = 'ADD'
			} else {
				context = 'REMOVE'
			}					
			
			var filterURL = ''
			
			if (pType!='') {
				filterURL += '&FilterType=' + pType
			}
			if (pCategory!='') {
				filterURL += '&FilterCategory=' + pCategory
			}	
			if (pid!='') {
				filterURL += '&FilterID=' + pid
			}				
			if (context!='') {
				filterURL += '&FilterContext=' + context
			}							
			
			//alert(filterURL);
			drawProds(filterURL);
 			
			//alert('DONE...' + pid);	

		   //$('html, body').animate({
			//	scrollTop: $("#ProdListing").offset().top
			//}, 500);
			
		}
		
		function removeFilter(pType, pCategory, pid)  {
			
			var filterURL = ''						
			filterURL += '&FilterType=' + pType			
			filterURL += '&FilterCategory=' + pCategory			
			filterURL += '&FilterID=' + pid
			filterURL += '&FilterContext=REMOVE'
					
			$("#" + pCategory + "_" + pid).prop("checked", false);
			
			drawProds(filterURL) ;
		}
		
		function removeFilter_SEARCH()  {
			
			var filterURL = ''						
			filterURL += '&FilterType=SEARCH'		
			filterURL += '&FilterCategory=SEARCH'			
					
			$("#BrowserSearchVal").val('')
			
			drawProds(filterURL) ;
		}		
		
		$("#searchVal_HEADER").keyup(function (e) {
			//alert(e.keyCode);
			if (e.keyCode == 13) {
				window.location.assign('/search/?SearchVal=' + $("#searchVal_HEADER").val())
			}
		});		
		
		
		function pageMove(pPage, pStartPos) {
			
			var filterURL = ''						
			filterURL += '&CurrentPage=' + pPage			
			filterURL += '&StartPos=' + pStartPos					
			drawProds(filterURL) ;
		
		}
		
		$("#BrowserSearchVal_GO").click( function () {
			//alert('search text')
			browserSearch()
		});
		
		$("#BrowserSearchVal").keyup(function (e) {
			//alert(e.keyCode);
			if (e.keyCode == 13) {
				browserSearch()
			}
		});		
		
		function browserSearch() {
			var browserSearchVal = $( "#BrowserSearchVal" ).val()
			//alert(browserSearchVal)
			
			var filterURL = '&FilterContext=SEARCH&FilterType=SEARCH&browserSearchVal=' + browserSearchVal
			drawProds(filterURL) 		
			//alert(filterURL)
		}
		
		
		function colorwayQtyChanged(pobj) {
			
			//alert('changed');
			
			var cwayqty = $(pobj).val()
			var productid = $(pobj).data("productid")
			var variantcode = $(pobj).data("variantcode")
			var colorwayid = $(pobj).data("colorwayid")
		
			//alert(cwayqty + '_' + productid + '_' + variantcode)
			
			var currentList = $("#RangeChanges").html()
			
			if (currentList == '') {currentList = '|'}
			
			//currentList.replace(variantcode, "xxx_" + variantcode);
			
			currentList = currentList + productid + '_' + colorwayid + '_' + cwayqty + '|'
			
			$("#RangeChanges").html(currentList)
			
			if (cwayqty==999) {
				//alert('submit');
				//$("#cwayForm").submit();
			}
			
		}
		
		function checkEnter(e) {
				
			if (!e) e = window.event;
			var keyCode = e.keyCode || e.which;
			if (keyCode == '13'){
			  // Enter pressed
				//$("#cwayForm").submit();
				return false;
			}
					
		}
		
		function checkEnterPopup(e) {
				
			if (!e) e = window.event;
			var keyCode = e.keyCode || e.which;
			if (keyCode == '13'){
			  // Enter pressed
			  //var rForm = document.getElementById("rangingForm");
			  //$( rForm  ).submit();
			  saveModal()
			  return false;
			}
					
			
		}		
		
		
	</script>
	
	<script>
		
		function swapBrowserImage(pobj, pid, psrc) {
			//alert('swapBrowserImage');
			document.getElementById('ProdBrowserThumb_' + pid).src = psrc;
			
			var productid = $( pobj ).data('productid');
			var variantcode = $( pobj ).data('variantcode');
			
			//alert(productid + '_' + variantcode);
			
			var ProdLink = document.getElementById('ProdLink_' + productid);
			var newURL = "/products/?ProductID=" + productid + "&InitCode=" + variantcode
			//alert(newURL);
			
			$( ProdLink ).attr("href", newURL) 
			
		}	

		$(".CheckAllTypes").click( function () {
		
			var CatList = '|';
			var setContext = '';
			
			//alert('check all click!')
			var lookFor = $( this ).data("lookfor")
			var thisChecked = $( this ).prop("checked")
			//alert(thisChecked);
			$( ".CHECK_" + lookFor ).each(function( index ) {
				$( this ).prop("checked", thisChecked);
				CatList = CatList + $( this ).val() + '|';
			});
			
			if (thisChecked) {
				setContext = "ADD"
			} else {
				setContext = "REMOVE"
			}
			
			var filterURL = '&FilterContext=' + setContext + '&FilterType=ALL_CATS&CatList=' + CatList + '&CatName=' + lookFor
			//alert(filterURL);
			drawProds(filterURL); 		
			
		});	
		
	</script>
	
	
<script>
	
	
function rangingCalc() {

	 
			var someSKUS = false;
			var totalNum = 0;
			var thisQty = 0			
			var thisPrice = 0
			var totalVal = 0;
			
			var list = document.getElementsByClassName("SKUQty");
			for (var i = 0; i < list.length; i++) {
				// list[i] is a node with the desired class name
				thisQty = $(list[i]).val();
				thisPrice = $(list[i]).data('sku-price');
				
				if ($.isNumeric( thisQty )) {
					
					thisQty = parseInt(thisQty)
					thisPrice = parseFloat(thisPrice,2)
					
					totalNum = totalNum + thisQty
										
					totalVal = totalVal + (thisQty * thisPrice)
					
					someSKUS = true;
				}
			}			 
			 
			
			
			if (!someSKUS && totalNum == 0) {
				list = document.getElementsByClassName("ColorwayQty");
				for (var i = 0; i < list.length; i++) {
				
					
					thisPrice = $(list[i]).data('colorway-price');
					
					// list[i] is a node with the desired class name
					thisQty = $(list[i]).val();
					if ($.isNumeric( thisQty )) {
						thisQty = parseInt(thisQty)
						thisPrice = parseFloat(thisPrice,2)						
						totalNum = totalNum + thisQty											
						totalVal = totalVal + (thisQty * thisPrice)						
						someSKUS = true;
						someSKUS = true;
					}
				}	
			}
			 			
			 //alert(totalNum);
			 
			 //var totalVal = (totalNum*19.99)
			 
			 $("#NotionalUnitCount").html(totalNum);
			 $("#NotionalUnitValue").html(totalVal.toFixed(2));

}	
	
		$(document).on("change", '.RangingQty', function() {
			 //alert('done')
			rangingCalc();
			 
		});
		
		function addRemoveRangingModal (pid, pCurrent, pContext) {
		
			//alert('add/remove = ' + pContext)
			addRemove(pid, pContext);
			showRangingModal (pCurrent);
			
		}		
		
		function showRangingModal (pid) {
		
			//alert(pid);
		
			$.ajax({

				scriptCharset: "ISO-8859-1" ,
				contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
				url: "/utilities/ranging/ranging_server.asp?CallType=RANGING_GRID&ProductID=" + pid + "&RangingID=<%=RangingID%>",				
				cache: false, 
				
				}).done( function( html ) {
						//alert('done...')						
						$("#AjaxModalDiv").html(html);	
						
						//$("#AjaxModalDiv").show();	
						
						$("#rangingModal").modal();
						
						var firstSKU = document.getElementById("SKUQty_1");
						
						//alert(firstSKU.name)	
												
						setTimeout(function (){
							$(firstSKU).focus()
						}, 1000);						
						
						//alert('done');
						
						rangingCalc();	
				
			});				
		
			
			

			
			
			//$('[autofocus]').focus()
			
		}
		
		function showNotesModal (pid) {
		
			//alert(pid);
		
			$.ajax({

				scriptCharset: "ISO-8859-1" ,
				contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
				url: "/utilities/ranging/ranging_server.asp?CallType=MODAL_NOTES&ProductID=" + pid + "&RangingID=<%=RangingID%>",				
				cache: false
				
				}).done( function( html ) {
						//alert('done...')						
						$("#NotesModalDiv").html(html);	
						
						//$("#AjaxModalDiv").show();	
						
						$("#notesModal").modal();
						
			});				
		
		}		
		
		function saveNotesModal (pid) {
		
			//alert(pid);
		
			var obj = document.getElementById(pid);
			var NoteText = $( obj ).val();
			var CallContext = $( obj ).data("context");
			var productid = $( obj ).data("pid");
		
			var ajurl = "/utilities/ranging/ranging_server.asp?CallType=SAVE_NOTES&CallContext=" + CallContext + "&NoteText=" + NoteText + "&ProductID=" + productid + "&RangingID=<%=RangingID%>"
			//alert(ajurl)
		
			$.ajax({

				scriptCharset: "ISO-8859-1" ,
				contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
				url: ajurl,				
				cache: false
				
				}).done( function( html ) {

						$("#notesModal").modal("hide");
						
			});				
		
		}			
		
		function saveModal() {

			//alert('save modal');
			//var rangingForm = document.getElementById("rangingForm")
			//$(rangingForm).submit();
			
			var ProductID = ''
			var qty = '';
			var sku = '';
			var variant = ''
			var qtyList = '|';
			var skuList = '|';
			var variantList = '|';
			
			$(".ModalQty").each(function() {
				//alert("mq");
				ProductID =  $( this ).data('product-id');
				qty = $( this ).val();
				sku = $( this ).data('sku');
				variant = $( this ).data('variant-id');
				
				if (qty!='') {
					//alert(qty + '_' + sku);
					qtyList = qtyList + qty + '|';
					skuList = skuList + sku + '|';
					variantList = variantList + variant + '|';
				}
			});
			
			if (qtyList!='') {
			
				$("#SKUGridContainer").html('<div class=legible><img src="/assets/images/icons/indicator.gif"> saving...</div>');
				
				$.ajax({

						scriptCharset: "ISO-8859-1" ,
						contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
						url: "ranging_server.asp?CallType=SKU_ADD&ProductID=" + ProductID + "&RangingID=<%=RangingID%>&SKUList=" + skuList + "&QtyList=" + qtyList + "&VariantList=" + variantList,				
						cache: false, 
						
						}).done( function( html ) {
								//alert('done...')						
								$("#AjaxFeedback_MODAL").html(html);						
								//$("#AjaxModalDiv").show();
								//drawProds("")
								//showRangingModal (ProductID);
															
								
								
								
								
					});				
			
				//alert(qtyList);
			}		
		
		}
		
		$(document).on("click", '#btnSaveRanging', function() {		
			
			//alert('scroll-to?')
			saveModal();
			drawProds("HIDE");
			
		});
		
		$(document).on("click", '.addRange', function() {		
			//alert('addRange');
			var context = $( this ).data('context');
			var productid = $( this ).data('product-id');
			//alert(productid + '_' + context)
			
			addRemove(productid, context);
			
		});
			

<%
if AllowDealerChanges = 1 or AllowAdminChanges  = 1 then
%>		
		
		function addRemove(pid, pContext)	{
			//alert("]]]" + pid + "_" + pContext)
			$.ajax({

				scriptCharset: "ISO-8859-1" ,
				contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",			
				url: "/utilities/ranging/ranging_server.asp?CallType=PROD_ADD&ProductID=" + pid + "&context=" + pContext + "&RangingID=<%=RangingID%>",				
				cache: false, 
				
				}).done( function( html ) {
						//alert('done...')						
						$("#ADDSCROLL").val(pid);	
						$("#AjaxFeedback").html(html);						
						//$("#AjaxModalDiv").show();
						drawProds("PAGE");
						
			});		
		}
<%
else
%>
		function addRemove(pid, pContext)	{
			//alert('no changes allowed')
		}
<%
end if
%>		
		
	</script>	

	<!-- Modal -->
	<div id="rangingModal" class="modal hide fade large" style="" tabindex="-1" role="dialog" aria-labelledby="rangingModalLabel" aria-hidden="true">
		<!--
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
			<h3 id="myModalLabel">Ranging Selector</h3>
		</div>
		-->
		<div class="modal-body">
			
			<div id="AjaxFeedback_MODAL">
				
			</div>

			<div id="AjaxModalDiv">
			
			</div>
			
			
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			<%
			if AllowDealerChanges = 1 or AllowAdminChanges = 1 then
			%>
				<button id=btnSaveRanging class="btn btn-primary">Save & Close</button>
			<%
			end if
			%>
		</div>
	</div>
	
	<div id="notesModal" class="modal hide fade" style="" tabindex="-1" role="dialog" aria-labelledby="notesModalLabel" aria-hidden="true">

		<div class="modal-body">
		
			<div id="NotesModalDiv">
			
			</div>
			
		</div>

	</div>