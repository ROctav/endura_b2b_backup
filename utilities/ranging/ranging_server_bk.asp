<%
Response.ContentType = "text/html"
Response.AddHeader "Content-Type", "text/html;charset=ISO-8859-1"
'Response.CodePage = 65001
Response.CharSet = "ISO-8859-1"

Response.Expires = 0
Response.Expiresabsolute = Now() - 1442
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"

%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	</head>
	<body>
	

<!--#include virtual="/assets/includes/global_functions.asp" -->
<%

'jhAdmin "PROD SERVER"

set acDetails = new DealerAccountDetails
set AdminDetails = new AdminAccountDetails

CallType = request("CallType")

RangingID = cleannum(request("RangingID"))
LastSavedProductID = session("LastSavedProductID")
session("LastSavedProductID") = ""

'jhInfo "RangingID = " & RangingID

'jhInfo "CallType = " & CallType
ThisScript = request.servervariables("SCRIPT_NAME") & "?" & request.servervariables("QUERY_STRING")
'jhInfo ThisScript
'jhstop

'on error resume next

if RangingID <> "" then
	sql = "SELECT r.*, sta.SMLStatusName "
	sql = sql & "FROM B2B_Ranging r "
	sql = sql & "INNER JOIN (SELECT SMLStatusID, SMLStatusName FROM SML_Status WHERE SMLTypeID = 376) sta ON sta.SMLStatusID = r.RangingStatusID " 
	sql = sql & "WHERE RangingID = 0" & RangingID & " "
	if not isAdminUser then
		sql = sql & "AND AcCode = '" & AcDetails.AcCode & "' "
		sql = sql & "AND AllowDealerView = 1 "
	end if
	jhadmin sql
	
	set rs = db.execute(sql)
	
	if not rs.eof then
	
		RangingTitle = rs("RangingTitle")
		RangingAcCode = rs("AcCode")
		RangingDescription = rs("RangingDescription")
		
		RangingStatusID = rs("RangingStatusID")
		RangingStatusName = rs("SMLStatusName")
		
		AllowDealerView = convertnum(rs("AllowDealerView"))
		AllowDealerChanges = convertnum(rs("AllowDealerChanges"))
		AllowDealerPrices = convertnum(rs("AllowDealerPrices"))
		AllowDealerBudget = convertnum(rs("AllowDealerBudget"))
		
		if isAdminUser() and RangingStatusID = 0 then AllowAdminChanges = 1 else AllowAdminChanges = 0
		if RangingStatusID > 0 then AllowDealerChanges = 0
		
		'jh "AllowDealerChanges = " & AllowDealerChanges
		'jh "AllowAdminChanges = " & AllowAdminChanges
		
		NewSeasonCodes = ucase(trim("" & rs("NewSeasonCodes")))
		
		if 1 = 0 then
			jhInfo "AllowDealerView = " & AllowDealerView
			jhInfo "AllowDealerChanges = " & AllowDealerChanges
			jhInfo "AllowDealerPrices = " & AllowDealerPrices
			jhInfo "AllowDealerBudget = " & AllowDealerBudget
			jhInfo "NewSeasonCodes = " & NewSeasonCodes
		end if	
		session("RangingID") = RangingID
	else
		dbClose() 
		AllStop
	end if
	
	rsClose()
end if

if CallType = "" then

elseif CallType = "SAVE_NOTES" then	

	'jh "NOW!"
	ProductID = convertnum(request("ProductID"))
	CallContext = request("CallContext")
	'jh "CallContext = " & CallContext
	NoteText = left("" & request("NoteText"),1000)
	'jh "NoteText = " & NoteText & " (" & len(NoteText) & ")"
	
	dbConnect()
	
	sql = "INSERT INTO B2B_RangingNotes (ProductID, RangingID) SELECT ProductID, 0" & RangingID & " AS RangingID FROM SML_Products "
	sql = sql & "WHERE ProductID = 0" & ProductID & " AND NOT EXISTS (SELECT * FROM B2B_RangingNotes WHERE ProductID = 0" & ProductID & " AND RangingID = 0" & RangingID & ") "
	'jh sql
	db.execute(sql)
	'CallContext = "DEALER"
	if CallContext = "ACM" then
		sql = "UPDATE B2B_RangingNotes SET AdminNotes = '" & validstr(NoteText) & "', AdminUpdatedBy = 0" & AdminDetails.AdminID & ", AdminUpdatedDate = GETDATE() WHERE ProductID = 0" & ProductID & " AND RangingID = 0" & RangingID & ""
		'jh sql
		db.execute(sql)
	elseif CallContext = "DEALER" then
		sql = "UPDATE B2B_RangingNotes SET DealerNotes = '" & validstr(NoteText) & "', DealerUpdatedDate = GETDATE() WHERE ProductID = 0" & ProductID & " AND RangingID = 0" & RangingID & ""
		'jh sql
		db.execute(sql)
		
		call updateRanging_DEALER(RangingID)
	end if
	
	response.write "<span class=""label label-warning""><i class=""icon icon-white icon-ok""></i> NOTES UPDATED</span>"
	
	dbClose()

elseif CallType = "MODAL_NOTES" then	
	'jh "NOW!"
	ProductID = convertnum(request("ProductID"))
	'jh "ProductID = " & ProductID
	sql = "SELECT sp.*, rn.* FROM SML_Products sp "
	sql = sql & "LEFT JOIN (SELECT ProductID, AdminNotes, AdminUpdatedDate, DealerNotes, DealerUpdatedDate FROM B2B_RangingNotes WHERE RangingID = 0" & RangingID & ") rn ON rn.ProductID = sp.ProductID "
	sql = sql & "WHERE sp.ProductID = 0" & ProductID & " AND dbo.spB2BRangingActive(sp.ProductID) > 0 "
	'jh sql
	dbConnect
	set rs = db.execute(sql)
	if not rs.eof then
		'jh "FOUND!"
		SelProdName = rs("ProductName")
		SelProdCode = rs("ProductCode")
		
		AdminNotes = "" & Rs("AdminNotes")
		DealerNotes = "" & Rs("DealerNotes")
		
		AdminUpdatedDate = "" & Rs("AdminUpdatedDate")
		DealerUpdatedDate = "" & Rs("DealerUpdatedDate")
		
	end if
	rsClose()
	
	if isAdminUser then	
		EditAdminNotes = true
	elseif isDealer then 
		EditDealerNotes = true
	end if
	
	if isjh() then
		EditAdminNotes = true
		EditDealerNotes = true
	end if
	
	'jh "EditAdminNotes = " & EditAdminNotes
	'jh "EditDealerNotes = " & EditDealerNotes
	
	%>
	<h3>Comments</h3>
	<div>
	<span class="label label-important"><%=SelProdCode%></span> <%=SelProdName%>
	</div>
	
<%
if EditAdminNotes then
%>	
	<div class=small>Enter any product comments you want to enter here...</div>
	<textarea style="width:90%;height:120px;" data-context=ACM data-pid="<%=ProductID%>" name=AdminNotes id=AdminNotes class=form-control><%=AdminNotes%></textarea>
<%
elseif AdminNotes <> "" then	
	%>
	<hr>
	<div><span class="label label-default">ACM: <%=AdminUpdatedDate%></span></div>
	<div>"<%=AdminNotes%>"</div>
	<%
	'jhInfoNice AdminUpdatedDate, """" & AdminNotes & """"
end if
if EditDealerNotes then
%>	
	<div class=small>Enter any product comments you want to enter here...</div>
	<textarea style="width:90%;height:120px;" data-context=DEALER data-pid="<%=ProductID%>" name=DealerNotes id=DealerNotes class=form-control><%=DealerNotes%></textarea>
<%
elseif DealerNotes <> "" then	
	%>
	<hr>
	
	<div><span class="label label-default">DEALER: <%=DealerUpdatedDate%></span></div>
	<div>"<%=DealerNotes%>"</div>
	<%
	'jhInfoNice DealerUpdatedDate, """" & DealerNotes & """"
end if

%>	

	
	<div class=spacer10></div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			<%
			if EditAdminNotes and AllowAdminChanges = 1 then
			%>
				<button id=btnSaveNotes onclick="saveNotesModal('AdminNotes') " class="btn btn-primary">Save Comments</button><span class="subtle">acm</span>
			<%
			end if
			
			if EditDealerNotes then
			%>
				<button id=btnSaveNotes onclick="saveNotesModal('DealerNotes') " class="btn btn-primary">Save Comments</button><span class="subtle">dealer</span>
			<%
			end if			
			%>
		</div>	
	
	<%
	
elseif CallType = "PL_UPDATE" then
	
	'jh "CallType = " & CallType
	
	RangingID = convertnum(request("RangingID"))
	SetSKU = trim("" & request("SetSKU"))
	SetQty = convertnum(request("SetQty"))
	ProductID = request("ProductID")
	ProductColorwayID = request("ProductColorwayID")
	
	if 1 = 0 then
		jhInfo "RangingID = " & RangingID
		jh "SetSKU = " & SetSKU
		jh "SetQty = " & SetQty
		jh "ProductID = " & ProductID
		jh "ProductColorwayID = " & ProductColorwayID
	end if
	
	dbConnect()
	
	sql = "DELETE FROM B2B_RangingSKUS WHERE RangingID = 0" & RangingID & " AND SKU = '" & validstr(SetSKU) & "' "
	'jh sql
	db.execute(sql)
	
	if SetQty > 0 then
		
		sql = "INSERT INTO B2B_RangingSKUS (RangingID, ProductID, ProductColorwayID, SKU, SKUQty) VALUES ("
		sql = sql & "0" & RangingID & ", "
		sql = sql & "0" & ProductID & ", "
		sql = sql & "0" & ProductColorwayID & ", "
		sql = sql & "'" & validstr(SetSKU) & "', "
		sql = sql & "0" & SetQty & ") "
		
		'jh sql
		db.execute(sql)
			
	end if
	
	'sql = "DELETE FROM B2B_RangingColorways WHERE RangingID = 0" & RangingID & " AND ProductColorwayID = 0" & ProductColorwayID & " "
	
	'update colorways
	
	
	sql = "INSERT INTO B2B_RangingColorways (RangingID, ProductID, ProductColorwayID) "
	sql = sql & "SELECT 0" & RangingID & ", ProductID, ProductColorwayID "
	sql = sql & "FROM SML_ProductColorways spc "
	sql = sql & "WHERE ProductColorwayID = 0" & ProductColorwayID & " "
	sql = sql & "AND NOT EXISTS (SELECT * FROM B2B_RangingColorways WHERE RangingID = 0" & RangingID & " AND ProductColorwayID = 0" & ProductColorwayID & ") "
	
	'jh sql
	db.execute(sql)
	
	sql = "UPDATE B2B_RangingColorways SET ColorwayQty = "
	sql = sql & "(SELECT SUM(SKUQty) AS s FROM B2B_RangingSKUS WHERE RangingID = 0" & RangingID & " AND ProductColorwayID = 0" & ProductColorwayID & " ) "
	sql = sql & "WHERE RangingID = 0" & RangingID & " AND ProductColorwayID = 0" & ProductColorwayID & " "
	
	'jh sql
	db.execute(sql)
	
	'sql = sql & "LEFT JOIN (SELECT ProductColorwayID, SUM(SKUQty) AS SKUSum FROM B2B_RangingSKUS WHERE RangingID = 0" & RangingID & " AND ProductColorwayID = 0" & ProductColorwayID & " GROUP BY ProductColorwayID) skuqtys ON skuqtys.ProductColorwayID = rcw.ProductColorwayID "	
		
	'update products
	
	sql = "INSERT INTO B2B_RangingProducts (RangingID, ProductID) "
	sql = sql & "SELECT 0" & RangingID & ", ProductID "
	sql = sql & "FROM SML_Products sp "
	sql = sql & "WHERE ProductID = 0" & ProductID & " "
	sql = sql & "AND NOT EXISTS (SELECT * FROM B2B_RangingProducts WHERE RangingID = 0" & RangingID & " AND ProductID = 0" & ProductID & ") "
	
	'jh sql
	db.execute(sql)
	
	sql = "UPDATE B2B_RangingProducts SET UnitQty = "
	sql = sql & "(SELECT SUM(SKUQty) AS s FROM B2B_RangingSKUS WHERE RangingID = 0" & RangingID & " AND ProductID = 0" & ProductID & " ) "
	sql = sql & "WHERE RangingID = 0" & RangingID & " AND ProductID = 0" & ProductID & " "
	
	'jh sql	
	db.execute(sql)

	sql = "DELETE FROM B2B_RangingColorways WHERE RangingID = 0" & RangingID & " AND ISNULL(ColorwayQty,0) = 0 AND ProductColorwayID = 0" & ProductColorwayID & "" 
	'jh sql
	db.execute(sql)
	
	sql = "DELETE FROM B2B_RangingProducts WHERE RangingID = 0" & RangingID & " AND ISNULL(UnitQty,0) = 0 AND ProductID = 0" & ProductID & "" 
	'jh sql
	db.execute(sql)
	
	if isDealer() and not isAdminUser() then call updateRanging_DEALER(RangingID)
	
	dbClose()
	
	response.write "<span class=""label label-warning""><i class=""icon icon-white icon-ok""></i> UPDATED</span>"
	
elseif CallType = "PROD_ADD" then

	'jh "//"
	

	ShowOutput = request("ShowOutput")
	RangingID = cleannum(request("RangingID"))
	
	addContext = request("context")
	
	ProductID = cleannum(request("ProductID"))
	
	dbConnect()
	
	
	
	if addContext = "REMOVE" then
		sql = "DELETE FROM B2B_RangingProducts WHERE RangingID = 0" & RangingID & " AND ProductID = 0" & ProductID		
		db.execute(sql)		
		
		sql = "DELETE FROM B2B_RangingColorways WHERE RangingID = 0" & RangingID & " AND ProductID = 0" & ProductID		
		db.execute(sql)				
		
		sql = "DELETE FROM B2B_RangingSKUS WHERE RangingID = 0" & RangingID & " AND ProductID = 0" & ProductID		
		db.execute(sql)				
		
		if ShowOutput = "TERSE" then
			response.write "<i class=""icon icon-remove""></i>"
		else
			jhErrorNice "", "Product has been DELETED from ranging proposal!"
		end if
	else
		sql = "INSERT INTO B2B_RangingProducts (RangingID, ProductID) SELECT 0" & RangingID & ", ProductID FROM SML_Products "
		sql = sql & "WHERE ProductID = 0" & ProductID & " "
		sql = sql & "AND NOT EXISTS ( SELECT * FROM B2B_RangingProducts WHERE RangingID = 0" & RangingID & " AND ProductID = 0" & ProductID & ")"
		
		'jh sql
		
		
		db.execute(sql)					
	
		if ShowOutput = "TERSE" then
			response.write "<i class=""icon icon-ok""></i>"
		else
			jhInfoNice "", "Product has been ADDED to ranging proposal!"	
		end if
		
		
	end if
	
	'jh "isAdmin() = " & isAdminUser
	
	'if isDealer() and not isAdminUser() then call updateRanging_DEALER(RangingID)
	
	'jh sql
	
	
elseif CallType = "SKU_ADD" then

	RangingID = cleannum(request("RangingID"))
	ProductID = cleannum(request("ProductID"))
	SKUList = request("SKUList")
	QtyList = request("QtyList")
	VariantList = request("VariantList")
	
	if 1 = 0 then
		jh "RangingID = " & RangingID
		jh "ProductID = " & ProductID
		jh "SKUList = " & SKUList
		jh "QtyList = " & QtyList
		jh "VariantList = " & VariantList
		'jhstop
	end if
	

	dbConnect()
	
	SKUArr = split(SKUList, "|")
	QtyArr = split(QtyList, "|")
	VariantArr = split(VariantList, "|")
	
	'jhInfo ThisScript
	
	if ubound(SKUArr) = ubound(QtyArr) then
	
		sql = "DELETE FROM B2B_RangingProducts WHERE ProductID = 0" & ProductID & " AND RangingID = 0" & RangingID
		'jh sql
		db.execute(sql)
		sql = "DELETE FROM B2B_RangingColorways WHERE ProductID = 0" & ProductID & " AND RangingID = 0" & RangingID
		'jh sql
		db.execute(sql)		
		sql = "DELETE FROM B2B_RangingSKUS WHERE ProductID = 0" & ProductID & " AND RangingID = 0" & RangingID
		'jh sql
		db.execute(sql)
	
		for c = 0 to ubound(SKUArr)
			ThisSKU = SKUArr(c)
			ThisQty = convertNum(QtyArr(c))
			ThisVariant = convertNum(VariantArr(c))
			
			if ThisSKU <> "" and ThisQty > 0 and ThisVariant > 0 then
				'jhInfo "OK, " & ThisQty & " of " & ThisSKU
				SomeAdded = true
				sql = "INSERT INTO B2B_RangingSKUS (RangingID, ProductID, ProductColorwayID, SKU, SKUQty) VALUES ("
				sql = sql & "0" & RangingID & ", "
				sql = sql & "0" & ProductID & ", "
				sql = sql & "0" & ThisVariant & ", "
				sql = sql & "'" & ThisSKU & "', "
				sql = sql & "0" & ThisQty & " "
				sql = sql & ")"
				
				'jh sql
				db.execute(sql)
			end if
		next
		
		'jhstop
		
		if SomeAdded then
			'jh "DO INSERTS!"
			
			sql = "INSERT INTO B2B_RangingProducts (RangingID, ProductID, UnitQty) "
			sql = sql & "SELECT RangingID, ProductID, SUM(SKUQty) AS SKUSum "
			sql = sql & "FROM B2B_RangingSKUS "
			sql = sql & "WHERE RangingID = 0" & RangingID & " AND ProductID = 0" & ProductID & " "
			sql = sql & "GROUP BY RangingID, ProductID "
			'jh sql
			db.execute(sql)
			
			sql = "INSERT INTO B2B_RangingColorways (RangingID, ProductID, ProductColorwayID, ColorwayQty) "
			sql = sql & "SELECT RangingID, ProductID, ProductColorwayID, SUM(SKUQty) AS SKUSum "
			sql = sql & "FROM B2B_RangingSKUS "
			sql = sql & "WHERE RangingID = 0" & RangingID & " AND ProductID = 0" & ProductID & " "
			sql = sql & "GROUP BY RangingID, ProductID, ProductColorwayID "
			'jh sql
			db.execute(sql)
			
			jhInfoNice "SKUs Added!", "Your selected sku qtys have been added to the ranging proposal!"
			
		end if
		
	end if
	
	'if isDealer() and not isAdmin() then call updateRanging_DEALER(RangingID)
	
	dbClose()	

elseif CallType = "PROD_LIST" then

	RangingAcCode = request("RangingAcCode")

	sql = "SELECT r.RangingID, r.AcCode, r.BenchmarkStartDate, r.BenchmarkEndDate  "
	sql = sql & "FROM B2B_Ranging r "	
	sql = sql & "WHERE r.RangingID = 0" & RangingID & ""
	'jhadmin sql
	set rs = db.execute(sql)
	if not rs.eof then
		BenchmarkStartDate = sqlDate(rs("BenchmarkStartDate"))
		BenchmarkEndDate = sqlDate(rs("BenchmarkEndDate"))
	end if
	rsClose()
	
	PageMax = 60
	
	StartPos = convertNum(request("StartPos"))
	CurrentPage = convertNum(request("CurrentPage"))
		
	if StartPos = 0 then StartPos = 1
	if CurrentPage = 0 then CurrentPage = 1
	
	'jhInfo "PageMax = " & PageMax
	'jh "StartPos (PAGING) = " & StartPos
	'jh "CurrentPage = " & CurrentPage

	'jhInfo "GET FILTER?"
	
	SortDir = request("SortDir")
	'jh "SortDir = " & SortDir
	if SortDir = "" then SortDir = session("SortDir")
	if SortDir = "" then SortDir = "TYPE"
	
	if SortDir <> "" then
		select case SortDir
			case "HI_LO"
				SortStr = "MaxPrice DESC, "
				SortLabel = lne("ranging_sort_by_price_hi_lo")
			case "LO_HI"
				SortStr = "MaxPrice, "
				SortLabel = lne("ranging_sort_by_price_lo_hi")
			case "NAME"
				SortStr = "ProductName, "
				SortLabel = lne("ranging_sort_by_name")
			case "CATALOGUE"
				SortStr = "CatalogueZOrder, ProductTypeZOrder, MaxPrice DESC, "		
				SortLabel = lne("ranging_sort_by_catalogue")			
			case else
				SortStr = "ProductTypeZOrder, MaxPrice DESC, "		
				SortLabel = lne("ranging_sort_by_type")		
		end select
		session("SortDir") = SortDir
	else
		
	end if

	FilterType = request("FilterType")
	FilterCategory = request("FilterCategory")
	FilterID = request("FilterID")
	FilterContext = request("FilterContext")
	
	if 1 = 0 then
		jhAdmin "FilterType = " & FilterType
		jhAdmin "FilterCategory = " & FilterCategory
		jhAdmin "FilterID = " & FilterID
		jhAdmin "FilterContext = " & FilterContext
	end if
	'session("ProdFilter") = "|"
	
	dbConnect()
	
	ProdFilter = session("ProdFilter")
	'jhAdmin "ProdFilter =  " & ProdFilter
	
	'jh "CURRENT ProdFilter = " & ProdFilter
	if ProdFilter = "" then 
		'jh "NO FILTER - SET INIT VAL"
		ProdFilter = "|"
	end if
	
	ThisFilter = FilterCategory & "_" & FilterID
	'jhError "ThisFilter = " & ThisFilter]
	if FilterCategory  = "xENDUSE" then ' TO ALLOW SINGLE END USE ONLY
		ProdFilter = replace(ProdFilter, "ENDUSE_", "XXX_")
		for c = 1 to 4
			ProdFilter = replace(ProdFilter, "|ENDUSE_" & c & "|", "|")	
		next
	end if
	
	CatName = trim("" & request("CatName"))
	
	if FilterContext = "ADD" then 
		ProdFilter = ProdFilter & "CAT_" & CatName & "|"	
	else
		ProdFilter = replace(ProdFilter, "|CAT_" & CatName & "|", "|")	
	end if
		
	
	if FilterType = "ALL_CATS" then
		
		'jhInfo "SET/REMOVE ALL CATS!"
		CatList = trim("" & request("CatList"))

		CatArr = split(CatList,"|")
		for c = 0 to ubound(CatArr)
			ThisCat = CatArr(c)
			ThisCatFilter = "NAV_CAT_" & ThisCat
			if ThisCat <> "" then
				if FilterContext = "ADD" and instr(1, ProdFilter, ThisCatFilter) = 0 then
					ProdFilter = ProdFilter & ThisCatFilter & "|"
				elseif FilterContext = "REMOVE" then
					ProdFilter = replace(ProdFilter, "|" & ThisCatFilter & "|", "|")		
				end if
			end if
		next
		
	elseif FilterContext = "ADD" then
		'jhInfo "ADD A FILTER!"	
		'jh "ThisFilter = " & ThisFilter
		if instr(1, ProdFilter, "|" & ThisFilter & "|") = 0 then
			ProdFilter = ProdFilter & ThisFilter & "|"
			'jhInfo "ADD!"
		end if
	elseif FilterType = "SEARCH" then		
		'jhInfo "ADD/REMOVE A SEARCH!"
		browserSearchVal = trim("" & request("browserSearchVal"))
		'jh "browserSearchVal = " & browserSearchVal
		if browserSearchVal = "CLEAR_" then
			session("ProdFilter_SEARCH") = ""
			browserSearchVal = ""
			ProdFilter = ""
		end if
		
		if browserSearchVal <> "" then
			SearchMode = "ADD"
			session("ProdFilter_SEARCH") = browserSearchVal		

			ThisIP = request.servervariables("REMOTE_ADDR")		
			sql = "exec spB2CCaptureSearch '" & validstr(browserSearchVal) & "', '" & ThisIP & "'"									
			'jh sql
			db.execute(sql)			
			
		else
			SearchMode = "REMOVE"
			session("ProdFilter_SEARCH") = ""
		end if
	elseif FilterContext = "REMOVE" then		
		'jhError "REMOVE A FILTER!"
		'jh ThisFilter
		ProdFilter = replace(ProdFilter, "|" & ThisFilter & "|", "|")		
	else
		'jh "NO ACTION!"
	end if
		
	'jhInfoNice "PRODFILTER NEW", ProdFilter
	session("ProdFilter") = ProdFilter
	
	FilterArr = split(ProdFilter, "|")
	'jh "ProdFilter= " & ProdFilter
	
	DemogSql = ""
	RangeSql = ""
	EndUseSql = ""
	CategorySql = ""
	ColorSql = ""
	OtherSql = ""
	
	KidsOnly = true
	
	RangeCount = 0
	
	for c = 0 to ubound(FilterArr)
		ThisFilter = FilterArr(c)
		if ThisFilter <> "" then
			'jhInfo "ThisFilter = " & ThisFilter
			LookFor = "RANGE_"
			if left(ThisFilter, len(LookFor)) = LookFor then
				FilterID = right(ThisFilter, len(ThisFilter) - len(LookFor))
				'jh "RANGE FilterID = " & FilterID
				RangeSql = RangeSql & " OR Ranges LIKE '%|" & FilterID & "|%' "
				RangeCount = RangeCount + 1
				SingleRangeID = FilterID
			end if
			LookFor = "NAV_CAT_"
			if left(ThisFilter, len(LookFor)) = LookFor then
				FilterID = right(ThisFilter, len(ThisFilter) - len(LookFor))
				'jh "CATEGORY FilterID = " & FilterID
				CategorySql = CategorySql & ", " & FilterID 
			end if		
			LookFor = "ENDUSE_"
			if left(ThisFilter, len(LookFor)) = LookFor then
				FilterID = right(ThisFilter, len(ThisFilter) - len(LookFor))
				'jh "CATEGORY FilterID = " & FilterID
				EndUseSql = EndUseSql & " OR Categories LIKE '%|" & FilterID & "|%' "
			end if		
			LookFor = "OTHER_"
			if left(ThisFilter, len(LookFor)) = LookFor then
				FilterID = right(ThisFilter, len(ThisFilter) - len(LookFor))
				'jh "CATEGORY FilterID = " & FilterID
				OtherSql = OtherSql & " OR ClassificationTags LIKE '%|" & FilterID & "|%' "
			end if				
			LookFor = "DEMOGRAPHIC_"
			if left(ThisFilter, len(LookFor)) = LookFor then				
				FilterID = right(ThisFilter, len(ThisFilter) - len(LookFor))
				if FilterID <> "2" then KidsOnly = false
				'jh "DEMOGRAPHIC FilterID = " & FilterID
				DemogSql = DemogSql & " OR Demographics LIKE '%|" & FilterID & "|%' "
			end if			
			LookFor = "COLOR_"
			if left(ThisFilter, len(LookFor)) = LookFor then				
				FilterID = right(ThisFilter, len(ThisFilter) - len(LookFor))
				if FilterID <> "2" then KidsOnly = false
				'jh "COLOR FilterID = " & FilterID
				ColorSql = ColorSql & ", " & FilterID & " "
			end if		
			
			LookFor = "OUTFIT_"
			if left(ThisFilter, len(LookFor)) = LookFor then				
				FilterID = right(ThisFilter, len(ThisFilter) - len(LookFor))
				'jhInfo "OUTFIT FilterID = " & FilterID
				OutfitSql = OutfitSql & ", " & FilterID 
			end if		
			
			if left(OutfitSql, 2) = ", " then OutfitSql = right(OutfitSql, len(OutfitSql)-2)
			
			LookFor = "STATUS_"
			if left(ThisFilter, len(LookFor)) = LookFor then
				FilterID = right(ThisFilter, len(ThisFilter) - len(LookFor))
				
				'jh "RANGING FilterID = " & FilterID
				
				if FilterID = "1" then
					RangingSql = RangingSql & " OR ISNULL(ranging.RangingID, '') <> '' "									
				end if
				
				if FilterID = "2" then
					RangingSql = RangingSql & " OR ISNULL(ranging.RangingID, '') = '' "									
				end if	
				
				if FilterID = "4" then
					LimitedSql =  " AND SKUStatusId = 80 "		
					'LimitedSql =  " AND LimitedID > 0 "									
				end if					

				if FilterID = "3" then
					'BenchMarkRequired = true
					npSql = ""
					NewProductsOnly = true
					NewSeasonCodes = cleancheck(NewSeasonCodes)
					npArr = split(NewSeasonCodes, "|")
					for z = 0 to ubound(npArr)
						ThisSeason = trim("" & npArr(z))
						if ThisSeason <> "" then
							'jh "ThisSeason = " & ThisSeason
							npSql = npSql & "'" & ThisSeason & "', "
						end if
					next
					if npSql <> "" then
						npSql = "sp.ProductSeason IN (" & npSql & " 'XXXX')"
					end if
					
					'jh npSql
				end if						
				
			end if				
			
			if instr(1, ThisFilter, "CODELIST_") > 0 then
				'jhInfo "CL"
			
				CodeFilter = replace(ThisFilter, "CODELIST_", "")
				
				CodeArr = split(CodeFilter, ",")
				for z = 0 to ubound(CodeArr)
					ThisCode = CodeArr(z)
					if ThisCode <> "" then
						CodeSql = CodeSql & "'" & ThisCode & "', "
					end if
				next
				
				if right(CodeSql, 2) = ", " then CodeSql = left(CodeSql, len(CodeSql)-2)
			end if
			
		end if
	next

	dbConnect()
	
	if RangeCount = 1 and 1 = 0 then

		sql = "SELECT ISNULL(RangeNameShort, RangeName) AS RangeName, Range1Liner, RangeDescription, RangeImage, RangeImageBrowser FROM SML_Ranges "
		sql = sql & "WHERE RangeID = 0" & SingleRangeID & " "
		if session("B2C_AdminUserID") = "" then
			sql = sql & "AND RangeStatusID = 1 "
		end if
		'jh sql
		
		if 1 = 1 then
			set rs = db.execute(sql)
			if not rs.eof then
				SingleRangeName = rs("RangeName")
				Range1Liner = "" & rs("Range1Liner")				
				RangeImage = "" & rs("RangeImage")
				RangeImageBrowser = "" & rs("RangeImageBrowser")	
				
			end if
			
			if RangeImageBrowser <> "" then
				ShowRangeHeader = true
			end if	
		end if
	end if
	
	set ls = new strconcatstream
	

	BenchmarkAcCode = RangingAcCode
	
	BenchC = -1
	RotationC = -1	
	
	if isdate(BenchmarkStartDate) and isdate(BenchmarkEndDate)  then
		BenchmarkStartDate_EX = exDate(BenchmarkStartDate)
		BenchmarkEndDate_EX = exDate(BenchmarkEndDate)
		
		sql = ""
		sql = sql & "SELECT ss.ProductID, SUM(QtyDelivered) AS BenchMarkSum "
		sql = sql & "FROM END_SalesOrderLineValues esv INNER JOIN SML_SKUS ss ON ss.SKU = esv.stockcode "
		sql = sql & "WHERE AccountCode = '" & BenchmarkAcCode & "' "
		sql = sql & "AND DueDate > " & BenchmarkStartDate_EX & " AND DueDate < " & BenchmarkEndDate_EX & " "
		sql = sql & "GROUP BY ss.ProductID "
		'jhInfo sql
		
		x = getrs(sql, BenchArr, BenchC)
		'jhAdmin "BenchC = " & BenchC
		
	end if
	
	if isjh() or glShowRotationFigures then
	
		RotationWindowStartDate = dateadd("m", -6, now)	
		RotationWindowStartDate_EX = ExDate(RotationWindowStartDate)
		
		sql = "SELECT ss.ProductID, SUM(esf.Qty) AS RotationSum " 
		sql = sql & "FROM END_SalesFigures esf "
		sql = sql & "INNER JOIN transactions t ON t.thOurRef = esf.tlOurRef "
		sql = sql & "INNER JOIN SML_SKUS ss ON ss.SKU = esf.idxStockCode "
		sql = sql & "WHERE esf.idxAcCode = '" & BenchmarkAcCode & "' AND (t.thYourRef = 'STOCK ROTATION' OR t.thYourRefLong = 'STOCK ROTATION/B2B')  "
		sql = sql & "AND Transdate >= " & RotationWindowStartDate_EX & " "
		sql = sql & "GROUP BY ss.ProductID "
		
		'jhAdmin sql
		x = getrs(sql, RotationArr, RotationC)
		'jhAdmin "RotationC = " & RotationC
	else	
		
	end if		
	
	if session("ProdFilter_SEARCH") <> "" and 1 = 1 then
	
		ThisSearchVal = session("ProdFilter_SEARCH")
		OrigSearchVal = ThisSearchVal
		sql = "SELECT TOP 1 p.ProductID, p.ProductCode FROM SML_Products p "
		sql = sql & "INNER JOIN SML_ProductColorways spc ON spc.ProductID = p.ProductID "
		sql = sql & "INNER JOIN SML_SKUS ss ON ss.ProductColorwayID = spc.ProductColorwayID "
		sql = sql & "WHERE spc.VariantCode = '" & validstr(ThisSearchVal) & "' OR ss.SKU = '" & validstr(ThisSearchVal) & "' "
		'jh sql
		x = getrs(sql, vararr, varc)
		'jh varc
		if varc = 0 then
			'jhInfo "FOUND FROM VARIANT/SKU - " & vararr(1,0)
			ThisSearchVal = vararr(1,0)
		end if
		
	end if	
	
	sql = "SELECT ss.ProductID, ss.SKU, MAX(pbl.Band" & AcDetails.CostBand & ") AS MaxPrice FROM SML_SKUS ss INNER JOIN end_pricebandlookup pbl ON ss.SKU = pbl.SKU WHERE ss.SKU LIKE 'E9078%' GROUP BY ss.ProductID"
	'jhadmin sql
	sql = "SELECT ss.ProductID, ss.SKU, (pbl.Band" & AcDetails.CostBand & ") FROM SML_SKUS ss INNER JOIN end_pricebandlookup pbl ON ss.SKU = pbl.SKU WHERE ss.SKU LIKE 'E9078%' "
	'jhadmin sql
	
	
	sql = "SELECT sp.ProductID, sp.ProductCode, sp.ProductName, "
	sql = sql & "MaxPrice, "
    'sql = sql & " (SELECT TOP 1 Band" & AcDetails.CostBand & " FROM sml_skus ss INNER JOIN end_pricebandlookup  pbl ON pbl.stockcode =  ss.sku WHERE ss.sku like variantcode + '%' ) AS MaxPrice, "
    sql = sql & "VariantCode, ColorName, DefaultProductImage, ColorCode, SimpleColorID, ProductName_TRANSLATED, "
	sql = sql & "ranging.RangingID, ActiveVariants.ProductColorwayID, ColorwayQty, SKUSum, 0 AS BenchMarkSum, ProductSeason, " 
    sql = sql & " (SELECT TOP 1 " & AcDetails.CostBand & "_FO FROM sml_skus ss INNER JOIN end_pricebandlookup  pbl ON pbl.stockcode =  ss.sku WHERE ss.sku like variantcode + '%' ) AS MaxPrice_FO "
	sql = sql & ", AdminNotes, AdminUpdatedDate, DealerNotes, DealerUpdatedDate, SKUStatusID "
	sql = sql & "FROM SML_Products sp "
	sql = sql & "INNER JOIN SML_ProductTypes spt ON spt.ProductTypeID = sp.ProductTypeID "
		
		
	sql = sql & "INNER JOIN ( "
	sql = sql & "SELECT DISTINCT spc.ProductID, spc.ProductColorwayID, spc.VariantCode, spc.ColorName, DefaultProductImage, spc.ColorCode, mx.SimpleColorID, LimitedID, sta.SKUStatusID  "
	sql = sql & "FROM SML_ProductColorways spc "
	sql = sql & "INNER JOIN SML_SKUS ss ON ss.ProductColorwayID = spc.ProductColorwayID "
	sql = sql & "INNER JOIN SML_SKUStatus sta on sta.SKUStatusID = ss.SKUStatusID "
	sql = sql & "LEFT JOIN SML_ColorwayList_SIMPLE_MATRIX mx ON mx.ColorCode = spc.ColorCode "
	sql = sql & "WHERE sta.B2BRangingVisible = 1 "
	sql = sql & ") ActiveVariants ON ActiveVariants.ProductID = sp.ProductID "
	
    'sql = sql & "LEFT JOIN (SELECT ProductID, Band" & AcDetails.CostBand & " AS MaxPrice, " & AcDetails.CostBand & "_FO AS MaxPrice_FO FROM END_PriceBandLookup pbl INNER JOIN SML_SKUS ss ON ss.SKU = pbl.StockCode GROUP BY ProductID, Band" & AcDetails.CostBand & ", " & AcDetails.CostBand & "_FO ) mp ON mp.ProductID = sp.ProductID "
	'sql = sql & "LEFT JOIN (SELECT ProductID, MAX(Band" & AcDetails.CostBand & ") AS MaxPrice, MAX(" & AcDetails.CostBand & "_FO) AS MaxPrice_FO FROM END_PriceBandLookup pbl INNER JOIN SML_SKUS ss ON ss.SKU = pbl.StockCode GROUP BY ProductID) mp ON mp.ProductID = sp.ProductID "
	'sql = sql & "LEFT JOIN (SELECT ProductID, MAX(" & AcDetails.CostBand & "_FO) AS MaxPrice_FO FROM END_PriceBandLookup rl INNER JOIN SML_SKUS ss ON ss.SKU = rl.StockCode GROUP BY ProductID) FOPrice ON FOPrice.ProductID = sp.ProductID "	
    	
	sql = sql & "LEFT JOIN (SELECT ProductID, ProductName AS ProductName_TRANSLATED FROM SML_ProductTranslations WHERE LAnguageID = 0" & AcDetails.SelectedLanguageID & ") tra ON tra.ProductID = sp.ProductID "
	
	sql = sql & "LEFT JOIN (SELECT ProductID, RangingID FROM B2B_RangingProducts WHERE RangingID = 0" & RangingID & ") ranging ON ranging.ProductID = sp.ProductID "
	
	sql = sql & "LEFT JOIN (SELECT ProductID, ProductColorwayID, ColorwayQty FROM B2B_RangingColorways WHERE RangingID = 0" & RangingID & ") qtys ON qtys.ProductColorwayID = ActiveVariants.ProductColorwayID "
	
	sql = sql & "LEFT JOIN (SELECT SUM(SKUQty) AS SKUSum, ProductID FROM B2B_RangingSKUS WHERE RangingID = 0" & RangingID & " GROUP BY ProductID) skuQtys ON skuQtys.ProductID = sp.ProductID "
	
	sql = sql & "LEFT JOIN SML_NavClassifications navclass ON navclass.NavClassificationID = spt.NavClassificationID "		
	
	sql = sql & "LEFT JOIN SML_ProductOutfitDetails spod ON spod.ProductColorwayID = ActiveVariants.ProductColorwayID "
	
	sql = sql & "LEFT JOIN (SELECT ProductID, AdminNotes, AdminUpdatedDate, DealerNotes, DealerUpdatedDate FROM B2B_RangingNotes WHERE RangingID = 0" & RangingID & ") rn ON rn.ProductID = sp.ProductID "
	sql = sql & "LEFT JOIN (SELECT ss.ProductID, MAX(pbl.Band" & AcDetails.CostBand & ") AS MaxPrice FROM SML_SKUS ss INNER JOIN end_pricebandlookup pbl ON ss.SKU = pbl.StockCode GROUP BY ss.ProductID) mp ON mp.ProductID = sp.ProductID "
	
	sql = sql & "WHERE 1 = 1 "
	'sql = sql & "AND PRoductName LIKE '%JACKET%' "
		
	sql = sql & " AND dbo.spB2BRangingActive(sp.ProductID) > 0 "  	
	'sql = sql & " AND sp.PublishOnB2B = 1 "
		
	if ThisSearchVal <> "" then
		
		sql = sql & "AND (ProductCode LIKE '%" & validstr(ThisSearchVal) & "%' OR ProductName LIKE '%" & validstr(ThisSearchVal) & "%' OR BulletPoints LIKE '%" & validstr(ThisSearchVal) & "%' OR ProductName_TRANSLATED LIKE '%" & validstr(ThisSearchVal) & "%' OR ProductKeywords LIKE '%" & validstr(ThisSearchVal) & "%' OR ProductTypeName LIKE '%" & validstr(ThisSearchVal) & "%' OR NavClassificationName LIKE '%" & validstr(ThisSearchVal) & "%' ) "
	end if	
	
	if RangingSql <> "" then
		sql = sql & "AND (1 = 0 " & RangingSql & ") "
	end if		
	
	if NewProductsOnly and npSql <> "" then
		'jhAdmin "NewProductsOnly = " & NewProductsOnly
		'jhStop
		sql = sql & "AND (" & npSql & ") "
	end if	
	
	if LimitedSql <> "" then	
		'jhInfo "LIMITED!"
		sql = sql & LimitedSql
	end if
	if session("FILTER_ProductType") <> "" then
		sql = sql & "AND ProductTypes LIKE '%|" & session("FILTER_ProductType") & "|%' "
	end if
			
	if EndUseSql <> "" then
		sql = sql & "AND (1 = 0 " & EndUseSql & ") "
	end if	

	if OtherSql <> "" then
		sql = sql & "AND (1 = 0 " & OtherSql & ") "
	end if		
			
	if RangeSql <> "" then
		sql = sql & "AND (1 = 0 " & RangeSql & ") "
	end if
	
	if ColorSql <> "" then
		sql = sql & "AND (SimpleColorID IN (-1 " & ColorSql & ")) "
	end if	
	
	if CodeSql <> "" then
		sql = sql & "AND (sp.ProductCode IN (" & CodeSql & ")) "
	end if
	
	'jh "KidsOnly = " & KidsOnly
	
	if DemogSql <> "" then			
		if KidsOnly then
			sql = sql & "AND (1 = 0 " & DemogSql & ") "
		else
			sql = sql & "AND (Demographics = '|' " & DemogSql & ") "
		end if
	end if

	if OutfitSql <> "" then			
		
		sql = sql & "AND (spod.OutfitID IN(" & OutfitSql & ")) "
	
	end if	
	
	
	
	if CategorySql <> "" then
		sql = sql & "AND spt.NavClassificationID IN (-1" & CategorySql & ") "
	end if	
	
	sql = sql & "ORDER BY " & SortStr & " ProductCode, DefaultProductImage DESC, VariantCode "
	
	
	jhadmin "Filtered Items" & sql
	'jhstop 
	x = getrs(sql, ProdArr, ProdC)	
	'jhAdmin "ProdC = " & ProdC
	
	TotalValue_FILTERED = 0
	TotalUnits_FILTERED = 0
	
	LastVariantCode = ""
	
	for c = 0 to ProdC
		ThisVariantCode = ProdArr(4,c) 	
		
		if ThisVariantCode <> LastVariantCode then
			
			ThisUnits = Convertnum(ProdArr(12,c))
			
			ThisPrice = Convertnum(ProdArr(3,c))
			ThisPrice_FO = Convertnum(ProdArr(16,c))
			
			'jh ThisPrice & "_" & ThisPrice_FO
			if ThisPrice_FO <> 0 and glRangingUseFOPrices then ThisPrice = ThisPrice_FO

			TotalUnits_FILTERED = TotalUnits_FILTERED + ThisUnits
			
			if ThisUnits > 0 and ThisPrice > 0 then	
				jhAdmin "ThisVariantCode = " & ThisVariantCode & "(" & ThisUnits & " * " & ThisPrice & "), " & TotalUnits_FILTERED
				TotalValue_FILTERED = TotalValue_FILTERED + (ThisUnits * ThisPrice)
         	end if
			
			LastVariantCode = ThisVariantCode
			
		end if
	
	next
	
	'jhAdmin "TotalUnits_FILTERED = " & TotalUnits_FILTERED
	LastVariantCode = ""
	
	'jhstop
	DisplayCount = 0
	
	if "" & AcDetails.SelectedLanguageID <> "0" then UseTransName = true
	

	
	for c = 0 to ProdC
		ThisProductID = ProdArr(0,c)				
		ThisProdCode = ProdArr(1,c)	
		InitVariant = ProdArr(4,c)	
		
		ThisRanged = "" & ProdArr(10,c)	
		RangedStyle = ""
		ScrollToProd = ""
		
		ThisProductMaxPrice = ProdArr(16,c)	
		'jhAdmin "ThisProductMaxPrice = " & ThisProductMaxPrice
		
		NewProduct = false
		npStr = ""
		ThisProductSeason = ucase("" & ProdArr(15,c))
		'jhAdmin "ThisProductSeason = " & ThisProductSeason & " (" & NewSeasonCodes & ")"
		if NewSeasonCodes <> "" and ThisProductSeason <> "" then	
			if instr(1, NewSeasonCodes, ThisProductSeason) > 0 then
				NewProduct = true
				npStr = "" & ucase(ThisProductSeason) & ""
			end if
		end if
		'jh "npStr = " & npStr
		'jh "ThisProductSeason = " & ThisProductSeason
		
		if ThisRanged <> "" then
		
			HasQtys = false
			InitVarChanged = false
			
			for z = c to ProdC
				if ProdArr(0,z)	= ThisProductID then
					if convertnum(ProdArr(12,z)) > 0 then
						
						HasQtys = true
						if not InitVarChanged then 
							InitVariant = ProdArr(4,z)
							InitVarChanged = true
							'jh "CHANGED INIT VARIANT!"	
							'jh "ThisProductID = " & ThisProductID & "_" & ProdArr(4,z)	
						end if
						'jh "HAS QTYS!"
						exit for
					end if
				else				
					exit for
				end if
			next	
			
		
			if not HasQtys then
				RangedStyle = "border:3px solid red;"			
			else
				RangedStyle = "border:3px solid green"		
			end if
			if "" & LastSavedProductID = "" & ThisProductID then
				
				if HasQtys then
					RangedStyle = "border:3px solid lime"	
				else
					RangedStyle = "border:3px solid hotpink"	
				end if
				
				ScrollToProd = "ScrollToProd"
			end if
			CheckOption = "<span class=""label label-success""><i data-product-id=""" & ThisProductID & """ data-context=""REMOVE"" class=""addRange pointer icon icon-ok icon-white""></i></span>"
			
			'jhInfo "CHECK THIS PROD HAS QTYS?"
			
		else
			CheckOption = "<span class=""label label-important""><i data-product-id=""" & ThisProductID & """ data-context=""ADD"" class=""addRange pointer icon icon-plus icon-white""></i></span>"
		end if
		
		ThisProdName = ""
		
		if UseTransName then 
			ThisProdName = trim("" & ProdArr(9,c))
		end if
		
		if ThisProdName = "" then ThisProdName = ProdArr(2,c)	
		
		
		if ThisProductID <> LastProductID then
		
			'AdminNotes, AdminUpdatedDate, DealerNotes, DealerUpdatedDate 
			ThisAdminNotes = trim("" & ProdArr(17,c))
			
			ThisAdminNotes = ProdArr(17,c)
			ThisAdminNotesUpdated = ProdArr(18,c)
			ThisDealerNotes = ProdArr(19,c)
			ThisDealerNotesUpdated = ProdArr(20,c)			
			
			'jh "ThisAdminNotes = " & ThisAdminNotes & "_" & ThisAdminNotesUpdated
		
			ThisImg = getImageDefault(ThisProdCode, InitVariant)
			'jh ThisImg
			ShowThis = true
			
			ThisBenchmarkSum = 0
			if BenchC >= 0 then
				for y = 0 to BenchC
					if BenchArr(0,y) = ThisProductID then
						ThisBenchmarkSum = convertNum(BenchArr(1,y))
						exit for
					end if
				next
			end if
			
			ThisRotationSum = 0
			if RotationC >= 0 then
				for y = 0 to RotationC
					if RotationArr(0,y) = ThisProductID then
						ThisRotationSum = convertNum(RotationArr(1,y))
						exit for
					end if
				next
			end if
			
			ListItems = ListItems + 1
			
			if DisplayCount >= PageMax then
				ShowThis = false
			elseif ListItems >= StartPos then
				Started = true
			end if				
			
			if not started then
				ShowThis = false
			end if			
						
			'jh "...ListItems = " & ListItems
			
			LastProductID = ThisProductID
			
			if Showthis then
				
				DisplayCount = DisplayCount + 1	
				
				
				ls.add "<div id=""" & ScrollToProd & """ class=""span3 "">"
				ls.add "<div id=""PRODSCROLL_" & ThisProductID & """></div>"
				
				ls.add "<div class=pull-left>"				
				ls.add CheckOption
				ls.add "</div>"
				
				if npStr <> "" then
					ls.add "<div class=pull-left>"				
					ls.add "&nbsp;<span title=""NEW PRODUCT"" class=""label label-warning"" style=""xfont-size:120%;""><i class=""icon icon-white icon-asterisk""></i> " & npStr & "</span>"
					ls.add "</div>"	
				end if
				if ThisBenchmarkSum > 0 then
					ls.add "<div class=pull-left>"				
					ls.add "&nbsp;<span title=""Qty ordered in prior window"" class=""label label-info"" style=""font-size:120%;"">" & ThisBenchmarkSum & "</span>"
					ls.add "</div>"				
				end if		
				if ThisRotationSum > 0 then
					ls.add "<div class=pull-left>"				
					ls.add "&nbsp;<span title=""Qty rotated in the last 6 months"" class=""label label-important"" style=""font-size:120%;"">" & ThisRotationSum & "</span>"
					ls.add "</div>"				
				end if					
				
				ls.add "<div style=""height:230px;"">"
				ls.add "<div style=""" & RangedStyle & """>"
				ls.add "<div class=text-center>"
				ls.add "<a class=pointer onclick=""showRangingModal('" & ThisProductID & "');"">"
				ls.add "<img id=""ProdBrowserThumb_" & ThisProductID & """ class=xProdThumb60 style=""height:120px;"" src=""" & ThisImg & """>"		
				ls.add "</a>"
				'ls.add "<div><strong>" & ThisProdCode & "</strong></div>"


				ls.add "<div class=""text-center"">"
								
				ColorsDisplayed = "|"
				
				for z = c to ProdC
					if ThisProductID = ProdArr(0,z) then
						'jh ProdArr(4,z) 
						ThisVariant = ProdArr(4,z) 
						ThisColorName = ProdArr(5,z) 
						ThisColorCode = "" & ProdArr(7,z) 
						
						ThisColorQty = convertnum(ProdArr(12,z))
						ThisSKUSum = convertnum(ProdArr(13,z))
						ThisBenchmarkSum = convertnum(ProdArr(14,z))
						
						'jh "ThisBenchmarkSum = " & ThisBenchmarkSum
						
						ThisColorwayID = ProdArr(11,z)	
						
						OverrideColorName = ""
						
						ThisColorCode = altColorBar(ThisColorCode, ThisVariant, OverrideColorName)
						'jhAdmin "ThisColorCode = " & ThisColorCode
						
						if ThisColorCode = "" then ThisColorCode = "BK"
						if OverrideColorName <> "" then ThisColorName = OverrideColorName
						
						'jh "ThisColorCode = " & ThisColorCode
						
						if instr(1, ColorsDisplayed, "|" & ThisColorCode & "|") = 0 then
						
							AltImage = getImageProdThumb(ThisProdCode, ThisVariant)
							'jh "AltImage = " & AltImage
							
							'ls.add "<div class=VariantTile data-productid=""" & ThisProductID & """ data-variantcode=""" & ThisVariant & """ onclick=""swapBrowserImage(this, " & ThisProductID & ", '" & AltImage & "')"" style="""">"
							'ls.add "<img class=VariantTileImage style="""" title=""" & ThisVariant & ", " & ThisColorName & """ src=""/assets/images/prodbrowser/colorbars/" & ThisColorCode & ".png"">" 
							'ls.add "</div>"
							
							if ThisColorQty > 0 then ColorQtyStr = ThisColorQty else ColorQtyStr = "&nbsp;"
							
							ls.add "<span title=""" & ThisVariant & " - " & ThisColorName & """ onclick=""swapBrowserImage(this, " & ThisProductID & ", '" & AltImage & "')"" style=""background-image:url(/assets/images/prodbrowser/colorbars/" & ThisColorCode & ".png);"" class=""pointer label label-inverse"">" 
							
							if ThisSKUSum = 0 and 1 = 0 then
								ls.add "<input data-productid=""" & ThisProductID & """ data-variantcode=""" & ThisVariant & """ data-colorwayid=""" & ThisColorwayID & """ onkeyup=""checkEnter();"" onchange=""colorwayQtyChanged(this);"" onfocus=""swapBrowserImage(this, " & ThisProductID & ", '" & AltImage & "')"" value=""" & ColorQtyStr & """ type=text style=""width:20px;border-radius:0;padding:1px !important;border:1px solid #fff;"" class=""numeric legible colorwayQty"">"
							else
								ls.add "<span style=""padding:8px 4px !important;font-size:12pt;"">" & ColorQtyStr & "</span>"
							end if
							
							ls.add "</span>&nbsp;"
							
							ColorsDisplayed = ColorsDisplayed & ThisColorCode & "|"
						end if							
					else
						exit for
					end if
				next
				
				
				ls.add "<div class=""spacer10""></div>"		
				ls.add "<div><strong>" & ThisProdName & "</strong><br><small class=legible>" & ThisProdCode & "</small> "
				
				whoStr = ""
				if ThisAdminNotes <> "" or ThisDealerNotes <> "" then
					if ThisAdminNotes <> "" then whoStr = whoStr & "/ACM"
					if ThisDealerNotes <> "" then whoStr = whoStr & "/Dealer"
					
					ls.add "&nbsp;<span onclick=""showNotesModal('" & ThisProductID & "')"" data-c=""" & c & """ class=""label label-info pointer NoteLink""><i class=""icon icon-white icon-comment""></i> " & whoStr & "</span>"
				elseif ThisRanged <> "" then
					ls.add "&nbsp;<i onclick=""showNotesModal('" & ThisProductID & "')"" data-c=""" & c & """ class=""pointer NoteLink icon icon-comment""></i>"
				end if				
				ls.add "<div class=""spacer5""></div>"		
				
				ls.add "</div>"
				
				'ls.add "<i onclick=""showNotesModal('" & ThisProductID & "')"" class=""pointer icon " & noteClass & """></i></div>"	
				'ls.add "<div class=subtle></div>"	
				ls.add "</div>"
				ls.add "</div>"				
				
				ls.add "</div>"
				ls.add "</div>"
				ls.add "<div class=spacer20></div>"		
				'ls.add "<hr>"
				ls.add "</div>"		

			end if
			
		end if	
	next
		
	dbClose()
	
	ProdOS = ls.value 
	
	'jh "ListItems = " & ListItems
		
	PageCount = ListItems \ PageMax
	'jh "PageCount = " & PageCount
	Remainder = ListItems MOD PageMax
	if Remainder > 0 then
		'jh "REMAINDER (" & Remainder & ")"
		PageCount = PageCount + 1
	end if
	'jh "PageCount = " & PageCount
	
	if DisplayCount = 0 then 
		ProdOS = "<div class=span12><span class=""label label-important"">" & lne("ranging_no_prods") & "</span></div>"
	end if
	
	'on error resume next
	
	CurrentStr = getCurrentFilter()

	
	'jh "SortDir = " & SortDir
	

	
	%>
	
	
	 <div class="row" >
				


		<div class="span9 intro spacer40">
								
				
			
				<div id=ProdOS>
				
					
<%



sql = "SELECT r.RangingID, sp.ProductID, sp.ProductCode, sp.ProductName, ColorwaySum, SKUSum, NotionalUnitPrice, r.BenchmarkStartDate, r.BenchmarkEndDate, NotionalUnitPrice_FO  "

sql = sql & "FROM B2B_Ranging r "
sql = sql & "INNER JOIN B2B_RangingProducts rp ON rp.RangingID = r.RangingID "
sql = sql & "INNER JOIN SML_Products sp ON sp.ProductID = rp.ProductID "
sql = sql & "LEFT JOIN ( "
sql = sql & "SELECT ProductID, SUM(ColorwayQty) AS ColorwaySum FROM B2B_RangingColorways  "
sql = sql & "WHERE RangingID = 0" & RangingID & " "
sql = sql & "GROUP BY ProductID "
sql = sql & ") colors ON colors.ProductID = sp.ProductID "

sql = sql & "LEFT JOIN ( "
sql = sql & "SELECT ProductID, SUM(SKUQty) AS SKUSum FROM B2B_RangingSKUS "
sql = sql & "WHERE RangingID = 0" & RangingID & " "
sql = sql & "GROUP BY ProductID "
sql = sql & ") skus ON skus.ProductID = sp.ProductID "

sql = sql & "LEFT JOIN ( "
sql = sql & "SELECT ProductID, MAX(Band" & AcDetails.CostBand & ") AS NotionalUnitPrice, MAX(" & AcDetails.CostBand & "_FO) AS NotionalUnitPrice_FO    "
sql = sql & "FROM END_PriceBandLookup pbl "
sql = sql & "INNER JOIN SML_SKUS ss ON pbl.StockCode = ss.SKU	 "
sql = sql & "GROUP BY ProductID "
sql = sql & ") prices ON prices.ProductID = sp.ProductID "

'sql = sql & "LEFT JOIN ( "
'sql = sql & "SELECT ProductID, MAX(" & AcDetails.CostBand & "_FO) AS NotionalUnitPrice_FO  "
'sql = sql & "FROM END_PriceBandLookup rl "
'sql = sql & "INNER JOIN SML_SKUS ss ON rl.StockCode = ss.SKU	 "
'sql = sql & "GROUP BY ProductID "
'sql = sql & ") prices_FO ON prices_FO.ProductID = sp.ProductID "

sql = sql & "WHERE r.RangingID = 0" & RangingID & " "
sql = sql & "ORDER BY sp.ProductCode "

jhAdmin "asdf: " & sql
'jhstop
x = getrs(sql, TotalArr, TotalC)
'jhAdmin "TotalC = " & TotalC

TotalStyles = 0
TotalUnits = 0
TotalValue = 0

if TotalC >= 0 then
	BenchmarkStartDate = sqlDate(TotalArr(7,0))
	BenchmarkEndDate = sqlDate(TotalArr(8,0))
	'jh "BenchmarkStartDate = " & BenchmarkStartDate
	
end if

for c = 0 to TotalC
	TotalStyles = TotalStyles + 1
	ThisUnits = convertnum(TotalArr(4,c))
	if ThisUnits > 0 then
		TotalUnits = TotalUnits + ThisUnits
		
		ThisUnitCost = convertnum(TotalArr(6,c))
		ThisUnitCost_FO = convertnum(TotalArr(9,c))
		
		'jhInfo ThisUnits & "_" & dispCurrency(ThisUnitCost) & "_" & dispCurrency(ThisUnitCost_FO)

		if ThisUnitCost_FO <> 0 and glRangingUseFOPrices then ThisUnitCost = ThisUnitCost_FO
		
		TotalValue = TotalValue + (ThisUnits * ThisUnitCost)
		
		'jhInfo ThisUnits & "_" & dispCurrency(ThisUnitCost) & "_" & dispCurrency(ThisUnitCost_FO) & "_" & TotalValue
	else
		UnitError = true
	end if
next

if TotalValue < (RangingBudget*.8) then
	BudgetClass = "label-success"
elseif TotalValue < RangingBudget then
	BudgetClass = "label-warning"
else
	BudgetClass = "label-important"
end if


    'SP - Use the function to get the total value for proposal as summing the collection is inaccurate due to pricing variances between styles on sam product ( E8024C & E8024G has a deffirecnt prices.)
    jhAdmin AcDetails.CostBand
	sql = "SELECT dbo.fnB2BRangingTotalValueForProposal(0" & RangingID &" , 1, '" & AcDetails.CostBand & "') "
	jhAdmin sql
    x = getrs(sql, RangingTotalArr, RangingTotalC)
    for c = 0 to RangingTotalC
        TotalValue = RangingTotalArr(0,c)
    next
%>

					
					
					<div class="row xspacer20">					
						<div class="span9">				
			
							<%
							if RangingStatusID = 0 then
							
							elseif isAdminUser() then
								jhErrorNice "Cannot edit proposal", "The status for this proposal is <strong>" & RangingStatusName & "</strong> - please reset to draft via the ranging admin"
							end if
							%>
			
							<div class="legible alert alert-info" >
								<%=lne("ranging_styles")%> <span id="ajaxStyleCount" class="label label-inverse"><%=TotalStyles%></span> | 
								<%=lne("ranging_units")%> <span id="ajaxUnitCount" class="label label-inverse"><%=TotalUnits%></span> | 
								
								<%=lne("ranging_value_in_filter")%>: <span class="label label-inverse"><%=dispCurrency(TotalValue_FILTERED)%></span> | 
								<%=lne("ranging_total_value_proposal")%>: <span class="label label-success"><%=dispCurrency(TotalValue)%></span>
								
								<%
								if UnitError then
								%>
									<div><span class="label label-important"><%=lne("ranging_some_no_qtys")%></span></div>	
								<%
								end if
								%>
							</div>
						</div>								
					</div>				
								

<%
function drawPageList ()
	dim c
%>
								<div class="pagination">
								  <ul>
									<li><a class=legible href="#">Pages</a></li>
									<%
									for c = 1 to PageCount
										ThisStartPos = ((c * PageMax) - PageMax) + 1
										if c = CurrentPage then pageClass = "active" else pageClass = "" 
									%>
										<li class="<%=pageClass%>"><a class=legible onclick="pageMove(<%=c%>, <%=ThisStartPos%>)" href="#"><%=c%></a></li>
									<%
									next
									%>		
									
								  </ul>
								</div>
<%
end function
%>								
								
					<div class="row spacer20">
					
						<div class="span7 text-left">
<%
							if CurrentStr <> "" then		
%>						
								<div class=spacer20>
									<%
									=CurrentStr
									%>
								</div>
<%
							end if
							
							if ListItems > 0 then
								EndPos = StartPos + PageMax - 1
								if ListItems < EndPos then EndPos = ListItems
%>
								<div>
									<Span class="label label-inverse"><%=lne_REPLACE("ranging_showing_count", StartPos & "|" & EndPos & "|" & ListItems)%></span>
								</div>
<%	
							end if

							if PageCount > 1 then
								call drawPageList ()
								
							end if
%>											
							
						</div>
						<div class="span2 text-right">

							<div class="btn-group text-left">
							  <a class="btn dropdown-toggle btn-small btn-inverse" data-toggle="dropdown" href="#">
								<%=lne("ranging_sort_by")%>: <strong><%=SortLabel%></strong>
								<span class="caret"></span>
							  </a>
							  
							  <ul class="dropdown-menu">
									<li><a onclick="sortClick('HI_LO');" href="#"><%=selSort("HI_LO")%> <%=lne("ranging_sort_by_price_hi_lo")%></a></li>
									<li><a onclick="sortClick('LO_HI');" href="#"><%=selSort("LO_HI")%> <%=lne("ranging_sort_by_price_lo_hi")%></a></li>
									<li><a onclick="sortClick('TYPE');" href="#"><%=selSort("TYPE")%> <%=lne("ranging_sort_by_type")%></a></li>
									<li><a onclick="sortClick('CATALOGUE');" href="#"><%=selSort("CATALOGUE")%> <%=lne("ranging_sort_by_catalogue")%></a></li>
									<li><a onclick="sortClick('NAME');" href="#"><%=selSort("NAME")%> <%=lne("ranging_sort_by_name")%></a></li>
							  </ul>
							  
							</div>	
						
							<input id="StartPos" class=FormHidden type=hidden value="<%=StartPos%>">
							<input id="CurrentPage" class=FormHidden type=hidden value="<%=CurrentPage%>">

						
						</div>
									
					</div>
					

					

					
					
<%			

ShowRangeHeader = false
if ShowRangeHeader then
	'jhAdmin "RangeImageBrowser = " & RangeImageBrowser 
%>
	<div class=spacer10></div>
	<div class=row style="margin-left:0px;color:white;border-radius:5px;background:url('/assets/images/prodbrowser/ranges/<%=RangeImageBrowser%>');background-repeat:no-repeat;background-position:top right;background-color:#000;">
		<div style="margin:0;color:white;border-radius:5px 0 0 5px;" class=span7>
			<div style="padding:10px;;margin:0;fonts-size:8pt;">
				<%=lne_B2C("RANGE_1LINER_" & SingleRangeID)%>
				
				<div style="font-weight:bold;font-size:14pt;padding-bottom:5px;">
					<%
					=SingleRangeName
					%>				
				</div>				
			</div>
		</div>		
		<div class=span5 style="padding-right:0px;margin:0;border:0px solid green;">
			<div class=pull-right style="text-align:right;padding-top:20px;">
				&nbsp;
			</div>
		</div>

	</div>	
	<div class=spacer10></div>
<%
end if
%>						
				
					<div class=row>
						
		
			
						<%
						=ProdOS
						%>
					</div>		
					
				</div>
		   
		</div>	
		<div class=row>
			<div class=span12>
<%
							if PageCount > 1 then
								call drawPageList ()
								
							end if
%>	
			</div>		
		</div>		
	</div>		
	
<%
elseif CallType = "RANGING_GRID" then

	ProductID = cleannum(request("ProductID"))

	%>
		<input class=FormHidden name="ModalProductID" id="ModalProductID" type=hidden value="<%=ProductID%>">
	<%
	
	dbConnect()

	'jhInfo "RANGING GRID!"

	sql = "SELECT sp.ProductID, sp.ProductCode, sp.ProductName, spc.VariantCode, spc.ColorName, spc.ProductColorwayID, ColorwayQty, sp.Demographics, sp.Categories, sp.ProductTypeID, sp.ClassificationTags "
	sql = sql & "FROM SML_Products sp "
	sql = sql & "INNER JOIN SML_ProductColorways spc ON spc.ProductID = sp.ProductID "
	sql = sql & "LEFT JOIN (SELECT ProductColorwayID, ColorwayQty FROM B2B_RangingColorways WHERE ProductID = 0" & ProductID & " AND RangingID = 0" & RangingID & ") rcw ON rcw.ProductColorwayID = spc.ProductColorwayID "
	sql = sql & "WHERE sp.ProductID = 0" & ProductID & " "
    sql = sql & " AND EXISTS ( SELECT ss.SKUStatusID FROM SML_SKUS ss JOIN SML_SKUStatus st ON st.SKUStatusID = ss.SKUStatusID WHERE ss.SKU like spc.variantcode + '%' AND st.b2brangingvisible = 1 )	"
	sql = sql & "ORDER BY spc.DefaultProductImage DESC, spc.VariantCode "

	'jhAdmin sql
	x = getrs(sql, variantArr, variantC)
	'jh variantC

	sql = "SELECT ss.ProductID, ss.ProductColorwayID, ss.SKU, ss.SizeID, sz.SizeName, sz.SizeType, SKUPrice, SKUQty "
	sql = sql & "FROM SML_SKUS ss "
	sql = sql & "INNER JOIN SML_SKUStatus sta ON sta.SKUStatusID = ss.SKUStatusID "
	sql = sql & "INNER JOIN SML_SizeList sz ON sz.SizeID = ss.SizeID "
	sql = sql & "LEFT JOIN ("
	sql = sql & "SELECT StockCode, SKUPrice = "
    sql = sql & "CASE  "
	sql = sql & "WHEN " & AcDetails.CostBand & "_fon > 0 THEN b_fon "
	sql = sql & "WHEN " & AcDetails.CostBand & "_fo > 0 THEN b_fo  "  
	sql = sql & "ELSE Band" & AcDetails.CostBand 
	sql = sql & " END "
	sql = sql & "FROM END_PriceBandLookup ) pbl ON pbl.StockCode = ss.SKU "
	
	
	sql = sql & "LEFT JOIN (SELECT SKU, SKUQty FROM B2B_RangingSKUS WHERE ProductID = 0" & ProductID & " AND RangingID = 0" & RangingID & ") rs ON rs.SKU = ss.SKU "
	sql = sql & "WHERE  "
	sql = sql & "ss.ProductID = 0" & ProductID & " "
	sql = sql & "AND B2BRangingVisible = 1  "
	sql = sql & "ORDER BY ProductColorwayID, SizeID "

	jhAdmin sql
	x = getrs(sql, SKUArr, SKUC)
	'jh SKUC

	if SKUC >= 0 then
		thenlastSizeType = ""
		sizeTypeSQL = ""
		for count = 0 to SKUC
			SizeType = SKUArr(5,count)
			
			if lastSizeType <> SizeType then
				if sizeTypeSQL <> "" then
					sizeTypeSQL = sizeTypeSQL & " or "
				end if
				sizeTypeSQL = sizeTypeSQL & "SizeType = 0" & SizeType & " "
				lastSizeType = SizeType
			end if
		next
		
		sql = "SELECT SizeID, SizeName FROM SML_SizeList WHERE " & sizeTypeSQL & " ORDER BY SizeZOrder, SizeID "
		jhAdmin sql
		x = getrs(sql, SizeArr, SizeC)
		'jh SizeC
		
	end if	
	
	'jh AcDetails.CostBand
		
	sql = "SELECT ss.ProductID, "
	sql = sql & "MIN(CASE WHEN " & AcDetails.CostBand & "_fon > 0 THEN " & AcDetails.CostBand & "_fon  WHEN " & AcDetails.CostBand & "_fo > 0 THEN " & AcDetails.CostBand & "_fo ELSE band" & AcDetails.CostBand & " END) AS MinPrice, "
	sql = sql & "MAX(CASE WHEN " & AcDetails.CostBand & "_fon > 0 THEN " & AcDetails.CostBand & "_fon  WHEN " & AcDetails.CostBand & "_fo > 0 THEN " & AcDetails.CostBand & "_fo ELSE band" & AcDetails.CostBand & " END) AS MaxPrice "
	sql = sql & "FROM END_PriceBandLookup rp "
	sql = sql & "INNER JOIN SML_SKUS ss ON ss.SKU = rp.StockCode "
	sql = sql & "INNER JOIN SML_SKUStatus sta ON ss.SKUStatusID = sta.SKUStatusID "
	sql = sql & "WHERE ss.ProductID = 0" & ProductID & " AND "
	sql = sql & "("
	sql = sql & "ISNULL(" & AcDetails.CostBand & "_FON, 0) > 0 "
	sql = sql & "OR ISNULL(" & AcDetails.CostBand & "_FO, 0) > 0 "
	sql = sql & "OR ISNULL(Band" & AcDetails.CostBand & ", 0) > 0 "
	sql = sql & ")"
	sql = sql & "AND B2BRangingVisible = 1 "
	sql = sql & "GROUP BY ss.ProductID "
	jhAdmin sql	
	
	set rs = db.execute(sql)
	if not rs.eof then
		MinPrice = convertnum(rs("MinPrice"))
		MaxPrice = convertnum(rs("MaxPrice"))
		
		if MinPrice <> MaxPrice then
			PriceStr = dispCurrency(MinPrice) & " - " & dispCurrency(MaxPrice)
		else
			PriceStr = dispCurrency(MaxPrice) 
		end if
	else
		PriceStr = "?"
	end if
	rsClose()
		
	sql = "SELECT r.RangingID, r.AcCode, r.BenchmarkStartDate, r.BenchmarkEndDate  "
	sql = sql & "FROM B2B_Ranging r "	
	sql = sql & "WHERE r.RangingID = 0" & RangingID & ""
	'jh sql
	set rs = db.execute(sql)
	if not rs.eof then
		BenchmarkAcCode = rs("AcCode")
		BenchmarkStartDate = sqlDate(rs("BenchmarkStartDate"))
		BenchmarkEndDate = sqlDate(rs("BenchmarkEndDate"))
	end if
	rsClose()	
	
	if isdate(BenchmarkStartDate) and isdate(BenchmarkEndDate)  then
		BenchmarkStartDate_EX = exDate(BenchmarkStartDate)
		BenchmarkEndDate_EX = exDate(BenchmarkEndDate)
		
		sql = ""
		sql = sql & "SELECT ss.ProductID, spc.VariantCode, SUM(QtyDelivered) AS BenchMarkSum "
		sql = sql & "FROM END_SalesOrderLineValues esv INNER JOIN SML_SKUS ss ON ss.SKU = esv.stockcode "
		sql = sql & "INNER JOIN SML_ProductColorways spc ON spc.ProductColorwayID = ss.ProductColorwayID "
		sql = sql & "WHERE ss.ProductID = 0" & ProductID & " AND AccountCode = '" & BenchmarkAcCode & "' "
		sql = sql & "AND DueDate > " & BenchmarkStartDate_EX & " AND DueDate < " & BenchmarkEndDate_EX & " "
		sql = sql & "GROUP BY ss.ProductID, spc.VariantCode "
		'jhInfo sql
		
		x = getrs(sql, BenchArr, BenchC)	
		
		'jhAdmin "BenchC = " & BenchC
	else
		BenchC = -1
	end if	

	if isjh() or glShowRotationFigures then
	
		RotationWindowStartDate = dateadd("m", -6, now)	
		RotationWindowStartDate_EX = ExDate(RotationWindowStartDate)
		
		sql = "SELECT ss.ProductID, spc.VariantCode, SUM(esf.Qty) AS RotationSum " 
		sql = sql & "FROM END_SalesFigures esf "
		sql = sql & "INNER JOIN transactions t ON t.thOurRef = esf.tlOurRef "
		sql = sql & "INNER JOIN SML_SKUS ss ON ss.SKU = esf.idxStockCode "
		sql = sql & "INNER JOIN SML_ProductColorways spc ON spc.ProductColorwayID = ss.ProductColorwayID "
		sql = sql & "WHERE ss.ProductID = 0" & ProductID & " AND esf.idxAcCode = '" & BenchmarkAcCode & "' AND (t.thYourRef = 'STOCK ROTATION' OR t.thYourRefLong = 'STOCK ROTATION/B2B') "
		sql = sql & "AND Transdate >= " & RotationWindowStartDate_EX & " "
		sql = sql & "GROUP BY ss.ProductID, spc.VariantCode "
		
		'jhAdmin sql
		x = getrs(sql, RotationArr, RotationC)
		jhAdmin "RotationC = " & RotationC
		
	else	
		RotationC = -1	
	end if			

	if variantC >= 0 then
		ThisProductID = variantArr(0,c)
		ThisProductCode = variantArr(1,c)
		ThisProductName = variantArr(2,c)
		
		ThisDemographics = variantArr(7,c)
		ThisCategories = variantArr(8,c)
		ThisProductTypeID = variantArr(9,c)
		ThisClassificationTags = "" & variantArr(10,c)
		
		ColorwayCount = 0
		SKUCount = 0
		
		
	%>
	

	
		<div class=row-fluid>
			<div class=span12 style="padding:10px 0px;">
				<strong><small>GOOD / BETTER / BEST <span class="label label-success">(&pound; &gt; &pound;&pound;&pound;)</span></small></strong>
			
				
					
					
					<%
					'jh "ThisDemographics = " & ThisDemographics
					'jh "ThisCategories = " & ThisCategories
					'jh "ThisProductTypeID = " & ThisProductTypeID
					'jh "ThisClassificationTags = " & ThisClassificationTags
				
					gbbWP = ""
					if instr(1, ThisClassificationTags, "|WP|") > 0 then
						
						'jh "ThisClassificationTags = " & ThisClassificationTags
						gbbWP = 1
					else
						gbbWP = 0
					end if
			
					sql = "exec [spB2BRangingGoodBetterBest] 0" & RangingID & ", 0" & ThisProductTypeID & ", '" & ThisCategories & "', '" & ThisDemographics & "', " & gbbWP & ""
					'jh sql
			
					x = getrs(sql, gbbArr, gbbc)
					
					gbbLastID = ""
					gbbFound = false
					gbbProductCount = 0
					
					
					
					'jh "ProductID = " & ProductID
					if gbbc >= 0 then
					
						redim gbbReportArr(5, gbbc+1)
					
						for c = 0 to gbbc
							gbbThisProductID = gbbArr(0,c)
							gbbThisProductCode = gbbArr(1,c)
							gbbThisVariantCode = gbbArr(2,c)
							gbbThisProductName = gbbArr(3,c)
							gbbThisRanged = convertnum(gbbArr(4,c))
							'jh "gbbThisRanged = " & gbbThisRanged
							
							
							if gbbLastID <> gbbThisProductID then
							
								gbbProductCount = gbbProductCount + 1
								gbbReportArr(0, gbbProductCount) = gbbThisProductID
								gbbReportArr(1, gbbProductCount) = gbbThisProductCode
								gbbReportArr(2, gbbProductCount) = gbbThisVariantCode
								gbbReportArr(3, gbbProductCount) = gbbThisProductName
								gbbReportArr(4, gbbProductCount) = gbbThisRanged
							
								'jh "gbbThisProductCode = " & gbbThisProductCode
								if "" & gbbThisProductID = "" & ProductID then
									'jhInfo "gbbThisProductCode = " & gbbThisProductCode
									gbbFound = true
									gbbThisProductPosition = gbbProductCount
								end if
								
								gbbLastID = gbbThisProductID
							end if
							
							
						next
						
						
						if gbbFound then
							
							gbbos = "<div class=row>"
						
							'jhError "gbbProductCount = " & gbbProductCount
							gbbStart = gbbThisProductPosition - 2
							gbbEnd = gbbThisProductPosition + 3
							if gbbStart < 1 then gbbStart = 1
							if gbbEnd > gbbProductCount then gbbEnd = gbbProductCount
							
							for c = gbbStart to gbbEnd
							
								gbbProductID =  gbbReportArr(0, c) 
								gbbProductCode =  gbbReportArr(1, c) 
								gbbVariantCode =  gbbReportArr(2, c) 
								gbbProductName =  gbbReportArr(3, c) 
								gbbThisRanged =  gbbReportArr(4, c) 
								
								gbbImgURL = getImageDefault(gbbProductCode, gbbVariantCode)
								
								gbbos = gbbos & "<div class=""span2 text-center"" style=""height:100px"">"
								
								if gbbThisRanged > 0 then
									CheckOption = "<a onclick=""addRemoveRangingModal(" & gbbProductID & ", " & ProductID & ", 'REMOVE')"" class=""label label-success""><i xdata-product-id=""" & ThisProductID & """ class=""xaddRange xpointer icon icon-ok icon-white""></i></a>"			
								else
									'CheckOption = "<span class=""label label-important""><i data-product-id=""" & ThisProductID & """ data-context=""ADD"" class=""addRange pointer icon icon-remove icon-white""></i></span>"				
									CheckOption = "<a onclick=""addRemoveRangingModal(" & gbbProductID & ", " & ProductID & ", 'ADD')"" class=""label label-important""><i xdata-product-id=""" & ThisProductID & """ class=""xaddRange xpointer icon icon-plus icon-white""></i></a>"
								end if
			
								if c = gbbThisProductPosition then
								
									gbbos = gbbos & "<div style=""border:2px solid black;background:white;padding:5px;"">"
									gbbos = gbbos & "<div class=pull-left>"
									gbbos = gbbos & CheckOption
									gbbos = gbbos & "</div>"
									gbbos = gbbos & "<div><img style=""width:60px;"" class=ProdThumb_60 src=""" & gbbImgURL & """></div>"
									gbbos = gbbos & "<div style=""color:green;font-weight:bold;"">" & gbbProductName & "</div>"
									gbbos = gbbos & "</div>"
			
								else
									
									gbbos = gbbos & "<div style=""border:2px solid #f0f0f0;;padding:5px;"">"								
									gbbos = gbbos & "<div class=pull-left>"
									gbbos = gbbos & CheckOption
									gbbos = gbbos & "</div>"									
									gbbos = gbbos & "<xa class=pointer onclick=""saveModal();showRangingModal('" & gbbProductID & "');"">"
									gbbos = gbbos & "<div><img style=""width:60px;"" class=ProdThumb_60 src=""" & gbbImgURL & """></div>"
									gbbos = gbbos & "<div xstyle=""color:green;font-weight:bold;"">" & gbbProductName & "</div>"
									gbbos = gbbos & "</xa>"
									gbbos = gbbos & "</div>"
									
									
								end if
								
								gbbos = gbbos & "</div>"
								

							next
							
							gbbos = gbbos & "</div>"
							
							
						end if
					end if
					
					if gbbProductCount <= 1 then 
						response.write "<div class=""legible subtle"">No good/better/best products found</div>"
					else
					%>
						<div class="legible well" style="padding-left:20px">	
						<%
						=gbbos
						%>
						</div>
					<%
					end if
					%>
										
			</div>
		</div>	
	
			<div class=row>
				<div class=span9>
		
				
					<h5><%=ThisProductName%></h5>
					<span class="label label-inverse"><%=PriceStr%></span>&nbsp;&nbsp;<a class="label label-info" href="/products/?ProductID=<%=ThisProductID%>"><i class=""></i> <%=lne("ranging_prod_detail_page")%></a>
				</div>
			</div>
			<div class=row>
				<div class=span9>
					
					<div id="SKUGridContainer">
					
						<table border=1 class="ReportTable legible">
							<tr>
								<th>
									
								</th>
								<th>
									Code
								</th>	
								<th>
									Colour
								</th>	
								
								<th>
									Qtys
								</th>		

					<%
					for c = 0 to SizeC
						ThisSizeName = SizeArr(1,c)
					%>
								<th>
									<%=ThisSizeName%>
								</th>
					<%
					next
					%>			
								
								<th style="background:royalblue;color:white;">
									Last
								</th>		
								<th title="# Rotated within the last 6 months" style="background:crimson;color:white;">
									Rotated
								</th>									
							</tr>
							<%
							
							for c = 0 to variantC
								
								ThisProductColorwayID = variantArr(5,c)
								ThisVariantCode = variantArr(3,c)
								ThisColorwayQty = convertnum(variantArr(6,c))
								if ThisColorwayQty = 0 then ThisColorwayQty = ""
								
								if LastVariantCode <> ThisVariantCode then
									
									ThisBenchmarkSum = ""
									if BenchC >= 0 then
										for y = 0 to BenchC
											if BenchArr(1,y) = ThisVariantCode then
												ThisBenchmarkSum = convertNum(BenchArr(2,y))
												exit for
											end if
										next
									end if
									
									ThisRotationSum = ""
									if RotationC >= 0 then
										for y = 0 to RotationC
											if RotationArr(1,y) = ThisVariantCode then
												ThisRotationSum = convertNum(RotationArr(2,y))
												exit for
											end if
										next
									end if									
								
									ColorwayCount = ColorwayCount + 1
								
									imgURL = getImageDefault(ThisProductCode, ThisVariantCode)
									ThisColorName = variantArr(4,c)
					%>
									<tr>
										<td>
											<img style="width:30px;" src="<%=imgURL%>">
										</td>
										<td>
											<strong><%=ThisVariantCode%></strong>
										</td>
										
										<td>
											<%=ThisColorName%>
										</td>
									
										<td style="background:lightyellow;text-align:right;">
											<!--<input data-colorway-price="<%=MaxPrice%>" AUTOCOMPLETE=off style="width:20px;" type=text class="form-control ColorwayQty RangingQty legible" name="ColorwayQty_<%=ColorwayCount%>" value="<%=ThisColorwayQty%>">
											<input class=FormHidden type=hidden name="ProductColorwayID_<%=ColorwayCount%>" value="<%=ThisProductColorwayID%>">-->
											<h3>
												<%
												=ThisColorwayQty
												%>
											</h3>
										</td>	
					<%
							for x = 0 to SizeC
								ThisSizeID = SizeArr(0,x)
								ThisSizeName = SizeArr(1,x)
								ThisSKU = ""
								ThisSKUPrice = 0
								ShowSKU = false
								for z = 0 to SKUC
									if "" & ThisProductColorwayID = "" & SKUArr(1,z) and "" & ThisSizeID = "" & SKUArr(3,z) then 
										ThisSKU = SKUArr(2,z)
										ThisSKUPrice = SKUArr(6,z)
										ThisSKUQty = convertnum(SKUArr(7,z))
										if ThisSKUQty = 0 then ThisSKUQty = ""
										
										ShowSKU = true
										SKUCount = SKUCount + 1
										exit for
										
									end if
								next
						
					%>
								<td width=20>
									<%
									if ShowSKU then
										if SKUCount = 1 then afStr = "autofocus" else afStr = ""
										'jh ThisSKUPrice
										if AllowDealerChanges or AllowAdminChanges then
									%>
										<input title="<%=ThisSKU%>" <%=afStr%> onkeyup="checkEnterPopup();" data-sku="<%=ThisSKU%>" AUTOCOMPLETE=off data-sku-price="<%=ThisSKUPrice%>" data-product-id="<%=ProductID%>" data-variant-id="<%=ThisProductColorwayID%>" style="width:20px;" type=text class="form-control numeric SKUQty RangingQty  legible ModalQty" name="SKUQty_<%=SKUCount%>" id="SKUQty_<%=SKUCount%>" xplaceholder="<%=ThisSizeName%>" value="<%=ThisSKUQty%>">
										<input class=FormHidden type=hidden name="SKU_<%=SKUCount%>" value="<%=ThisSKU%>">
										<input class=FormHidden type=hidden name="SKUProductColorwayID_<%=SKUCount%>" value="<%=ThisProductColorwayID%>">
										
									<%
										else
										%>
											<span style="" class="label label-inverse"><%=ThisSKUQty%></span>
										<%
										end if
									end if
									%>
									
								</td>
					<%
					next
					%>	
										<td style="text-align:right;background:royalblue;color:white;">
											<%=ThisBenchmarkSum%>
										</td>	
										<td style="text-align:right;background:Crimson;color:white;">
											<%=ThisRotationSum%>
										</td>										
									</tr>
					<%				
									LastVariantCode = ThisVariantCode
								end if
							next
						%>
						
					</table>	
					
					<div class=spacer10>
					</div>
					
					<input class=FormHidden type=hidden name=RangingID value="<%=RangingID%>">
					<input class=FormHidden type=hidden name=ProductID value="<%=ProductID%>">
					<input class=FormHidden type=hidden name=ColorwayCount value="<%=ColorwayCount%>">
					<input class=FormHidden type=hidden name=SKUCount value="<%=SKUCount%>">
					<input class=FormHidden type=hidden name=FormAction value="QTY_SAVE">
					
				
				</div>

				
			</div>

<%
if 1 = 0 then
%>			
			<div class="span3 text-right">
				

					
				<div class="legible well text-left">
					
					<div class=spacer20>
					Units:<br>
					<span class="label label-inverse" id="NotionalUnitCount">???</span>
					</div>
					
					<div class=spacer20>
					Value:<br>
					<span class="label label-success" id="NotionalUnitValue">???</span>
					</div>
					<!--
					<div class=spacer20>
						Budget ROAD<br>
						<span class="label label-success" id="NotionalBudget">???</span>
					</div>		
					
					<div class=spacer20>
						Current ROAD<br>
						<span class="label label-success" id="NotionalBudget">???</span>
					</div>							
					-->
				</div>
				
			</div>
		
		</div>
		
<%
end if
%>		
		

			

<%
	end if

end if

rsClose()
dbClose()

set ls = nothing

function getTopNavClassifications(pcategory)
	dim c, ThisID, ThisName, ThisCat, os
	for c = 0 to NavC
		ThisID = NavArr(0,c)
		ThisName = NavArr(1,c)
		ThisCat = NavArr(2,c)
		'jh "ThisCat = " & ThisCat
		if ThisCat = pcategory then
			'jhInfo ThisName
			os = os & "<div class=TopNavSep>"
			os = os & "<a onclick=""topClick('NAV_CAT', " & ThisID & ")"" href=""#"">" & ThisName & "</a>"
			os = os & "</div>"			
		end if
	next
	getTopNavClassifications = os
end function

function getCurrentFilter()

	dim CurrentFilter, CurrentSearchVal, c, sql, x, os
	
	'jh session("ProdFilter")
	CurrentFilter = session("ProdFilter")
	CurrentSearchVal = session("ProdFilter_SEARCH")
	
	'jh "CurrentFilter = " & CurrentFilter
	dbConnect
	
	sql = "SELECT d.DemographicID, d.DemographicName, TranslatedValue "
	sql = sql & "FROM SML_Demographics d "
	sql = sql & "LEFT JOIN (SELECT IDValue, TranslatedValue FROM SML_ListsItems_TRANSLATED WHERE LanguageID = 0" & SelectedLanguageID & " AND ListTypeID = 120) tra ON tra.IDValue = d.DemographicID "
	sql = sql & "ORDER BY d.DemographicZOrder "
	'jh sql
	x = getrs(sql, DemogArr, DemogC)

	sql = "SELECT RangeID, RangeName FROM SML_Ranges WHERE RangeStatusID = 1 ORDER BY RangeZOrder "
	'jh sql
	x = getrs(sql, RangeArr, RangeC)	
	
	sql = "SELECT CategoryID, CategoryName FROM SML_Categories c WHERE CategoryID IN (1,2,18,14) ORDER BY CategoryZORder"
	x = getrs(sql, EndUseArr, EndUseC)
	
	sql = "SELECT OutfitID, OutfitName FROM SML_ProductOutfits spod WHERE OutfitStatusID = 1 ORDER BY OutfitZOrder, OutfitID"
	x = getrs(sql, OutfitArr, OutfitC)	
	
	sql = "SELECT Params, SMLStatusName FROM SML_Status s WHERE SMLTypeID = 120 ORDER BY StatusZORder"
	x = getrs(sql, OtherArr, OtherC)	
	
	sql = "SELECT nc.NavClassificationID, nc.NavClassificationName, nc.NavClassificationTopLevel, TranslatedValue "
	sql = sql & "FROM SML_NavClassifications nc "
	sql = sql & "LEFT JOIN (SELECT IDValue, TranslatedValue FROM SML_ListsItems_TRANSLATED WHERE LanguageID = 0" & SelectedLanguageID & " AND ListTypeID = 100) tra ON tra.IDValue = nc.NavClassificationID "
	sql = sql & "ORDER BY nc.NavClassificationZOrder,  nc.NavClassificationID "
	'jh sql
	x = getrs(sql, NavArr, NavC)

	sql = "SELECT cwls.SimpleColorID, cwls.SimpleColorName, cwls.SimpleWebColor, cwls.ColorBar, TranslatedValue "
	sql = sql & "FROM SML_ColorwayList_SIMPLE cwls "
	sql = sql & "LEFT JOIN (SELECT IDValue, TranslatedValue FROM SML_ListsItems_TRANSLATED WHERE LanguageID = 0" & SelectedLanguageID & " AND ListTypeID = 110) tra ON tra.IDValue = cwls.SimpleColorID "
	sql = sql & "ORDER BY cwls.SimpleColorZOrder, cwls.SimpleColorID "
	'jh sql
	x = getrs(sql, SimpleArr, SimpleC)	
	
	sql = "SELECT SMLStatusID, SMLStatusName FROM SML_Status s WHERE SMLTypeID = 951 ORDER BY StatusZOrder"
	x = getrs(sql, RangeStatusArr, RangeStatusC)	
	
	for c = 0 to RangeStatusC
		ThisID = RangeStatusArr(0,c)
		ThisName = RangeStatusArr(1,c)
		if instr(1, CurrentFilter, "|STATUS_" & ThisID & "|") > 0 then
			
			labelClass = "label-default" 
			if ThisID = "1" then 
				labelClass = "label-info" 
			elseif ThisID = "2" then 
				labelClass = "label-important" 
			end if
			
			lblText = lne("ranged_status_" & ThisID)
			
			os = os & "<span class=""label " & labelClass & """>" & lblText & " <i onclick=""removeFilter('STATUS', 'STATUS', " & ThisID & ");"" class=""pointer icon icon-white icon-remove""></i></span>&nbsp;"
		end if
	next		
	
	'jhInfo "CurrentFilter = " & CurrentFilter
	'jhInfo "CurrentSearchVal = " & CurrentSearchVal
	
	if CurrentSearchVal <> "" then
		os = os & "<span class=""label label-default"">""" & CurrentSearchVal & """ <i onclick=""removeFilter_SEARCH();"" class=""pointer icon icon-white icon-remove""></i></span>&nbsp;"
	end if
	
	
	for c = 0 to DemogC
		ThisID = DemogArr(0,c)
		
		if SelectedLanguageID <> "0" then
			ThisName = trim("" & DemogArr(2,c))
		else
			ThisName = ""
		end if			
		if ThisName = "" then ThisName = DemogArr(1,c)			
		
		if instr(1, CurrentFilter, "|DEMOGRAPHIC_" & ThisID & "|") > 0 then
			os = os & "<span class=""label label-default"">" & ThisName & " <i onclick=""removeFilter('NAV_DEMOG', 'DEMOGRAPHIC', " & ThisID & ");"" class=""pointer icon icon-white icon-remove""></i></span>&nbsp;"
		end if
	next

	for c = 0 to EndUseC
		ThisID = EndUseArr(0,c)
		ThisName = EndUseArr(1,c)
		if instr(1, CurrentFilter, "|ENDUSE_" & ThisID & "|") > 0 then
			os = os & "<span class=""label label-default"">" & ThisName & " <i onclick=""removeFilter('ENDUSE', 'ENDUSE', " & ThisID & ");"" class=""pointer icon icon-white icon-remove""></i></span>&nbsp;"
		end if
	next			
	
	for c = 0 to RangeC
		ThisID = RangeArr(0,c)
		ThisName = RangeArr(1,c)
		if instr(1, CurrentFilter, "|RANGE_" & ThisID & "|") > 0 then
			os = os & "<span class=""label label-default"">" & ThisName & " <i onclick=""removeFilter('RANGE', 'RANGE', " & ThisID & ");"" class=""pointer icon icon-white icon-remove""></i></span>&nbsp;"
		end if
	next	
	
	for c = 0 to OutfitC
		ThisID = OutfitArr(0,c)
		ThisName = OutfitArr(1,c)
		if instr(1, CurrentFilter, "|OUTFIT_" & ThisID & "|") > 0 then
			os = os & "<span class=""label label-success"">Outfit: " & ThisName & " <i onclick=""removeFilter('OUTFIT', 'OUTFIT', " & ThisID & ");"" class=""pointer icon icon-white icon-remove""></i></span>&nbsp;"
		end if
	next	
	
	for c = 0 to NavC
		ThisID = NavArr(0,c)

		if SelectedLanguageID <> "0" then
			ThisName = trim("" & NavArr(3,c))
		else
			ThisName = ""
		end if
		
		if ThisName = "" then ThisName = NavArr(1,c)			
		
		if instr(1, CurrentFilter, "|NAV_CAT_" & ThisID & "|") > 0 then
			os = os & "<span class=""label label-default"">" & ThisName & " <i onclick=""removeFilter('NAV_CAT', 'NAV_CAT', " & ThisID & ")"" class=""pointer icon icon-white icon-remove""></i></span>&nbsp;"
		end if
	next	
	
	'jh "SelectedLanguageID = " & SelectedLanguageID
	
	for c = 0 to OtherC
		ThisID = OtherArr(0,c)
		ThisName = OtherArr(1,c)
		if instr(1, CurrentFilter, "|OTHER_" & ThisID & "|") > 0 then
			os = os & "<span class=""label label-default"">" & ThisName & " <i onclick=""removeFilter('OTHER', 'OTHER', '" & ThisID & "');"" class=""pointer icon icon-white icon-remove""></i></span>&nbsp;"
		end if
	next		
	
	for c = 0 to SimpleC
		ThisID = SimpleArr(0,c)
		if SelectedLanguageID <> "0" then
			ThisName = trim("" & SimpleArr(4,c))
		else
			ThisName = ""
		end if				
		if ThisName = "" then ThisName = SimpleArr(1,c)		
		ThisColorBar = SimpleArr(3,c)
		if instr(1, CurrentFilter, "|COLOR_" & ThisID & "|") > 0 then
			os = os & "<span class=""label label-default""><img style=""height:8px;width:10px;"" src=""http://www.endurasport.com/assets/images/prodbrowser/colorbars/" & ThisColorBar & ".png"">&nbsp;" & ThisName & " <i onclick=""removeFilter('COLOR', 'COLOR', " & ThisID & ")"" class=""pointer icon icon-white icon-remove""></i></span>&nbsp;"
		end if
	next		
	
	FilterArr = split(CurrentFilter, "|")
	
	for c = 0 to ubound(FilterArr) 
		ThisFilter = FilterArr(c)
		if instr(1, ThisFilter, "CODELIST_") > 0 then
			CodeFilter = replace(ThisFilter, "CODELIST_", "")
			os = os & "<span class=""label label-default"">" & CodeFilter & "<i onclick=""removeFilter('CODELIST', 'CODELIST', '" & CodeFilter & "')"" class=""pointer icon icon-white icon-remove""></i></span>&nbsp;"
			exit for
		end if
	next
	
	getCurrentFilter = os
	'response.write os
	
end function


public colorBarArr, colorBarC

function altColorBar(byval pColorCode, byval pVariant, byref rOverRideColorName )
	dim x
	if not isarray(ColorBarArr) and colorBarC <> -1 then
		sql = "SELECT ColorOverrideID, Variantcode, ColorBarCode, ColorOverrideName FROM B2C_ColorOverrides "
		'jh sql
		x = getrs(sql, colorBarArr, colorBarC)
	end if
		
	altColorBar = pColorCode
	OverrideColorName = ""
	
	for x = 0 to colorBarC
		if "" & pVariant = "" & colorBarArr(1,x) then
			altColorBar = colorBarArr(2,x)
			rOverRideColorName = colorBarArr(3,x)
			exit for
		end if
	next
	
	if altColorBar = "" then altColorBar = "BK"

end function

function selSort(byval pSort)
	if pSort = SortDir then
		selSort = "<i class=""icon icon-ok""></i> "
	end if
end function

%>


