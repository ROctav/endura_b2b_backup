<!--#include virtual="/assets/includes/global_functions.asp" -->

<%

set fs = server.createobject("Scripting.FileSystemObject")

set acDetails = new DealerAccountDetails

DownloadType = request("Type")

if DownloadType = "INVOICE" then

	InvoiceNo = request("i")

	'sql = "SELECT tlOurRef FROM vwInvoiceReport WHERE tlOurRef = '" & cleansql(InvoiceNo) & "' AND idxAcCode = '" & session("ENDURA_UserID") & "' "

	sql = "exec [spB2BRecentInvoices] '" & AcDetails.AcCode & "', '" & InvoiceNo & "', " & ExDate(now)
	'jhAdmin sql

	dbConnect()
	set rs = db.execute(sql)
	if not rs.eof then
		jhInfo  "FOUND!"
		InvoiceAllowed = true
	end if
	rsClose()
	
	if InvoiceAllowed then
		InvoiceLocated = getInvoiceLink(InvoiceNo, InvoiceFN)
	end if
	
	if InvoiceLocated then
		
		PDFName = InvoiceNo & ".PDF"
		
		Response.clear
		Response.Buffer = true
		
		Response.AddHeader "Content-Disposition", "attachment; filename=" & PDFName
		'Response.AddHeader "Content-Length", strFileSize
		Response.Charset = "UTF-8"
		'Response.ContentType = ContentType
		
		Response.ContentType = "application/pdf"
					
		Set FileSystem = CreateObject("ADODB.Stream")

		FileSystem.Type = 1

		FileSystem.Open
		'jh FName

		FileSystem.LoadFromFile InvoiceFN
		
		Response.BinaryWrite (FileSystem.read)

		set FileSystem = nothing
		set DataStream = nothing		
	else
		jhError "NOT FOUND!"
	end if
	
	'jhstop
elseif DownloadType = "STATEMENT" then
    StatementNo = request("i")

	'sql = "SELECT tlOurRef FROM vwInvoiceReport WHERE tlOurRef = '" & cleansql(InvoiceNo) & "' AND idxAcCode = '" & session("ENDURA_UserID") & "' "

	'sql = "exec [spB2BRecentInvoices] '" & AcDetails.AcCode & "', '" & InvoiceNo & "', " & ExDate(now)
	'jhAdmin sql

	'dbConnect()
	'set rs = db.execute(sql)
	'if not rs.eof then
		'jhInfo  "FOUND!"
		'InvoiceAllowed = true
	'end if
	'rsClose()
	
	'if InvoiceAllowed then
		StatementLocated = getStatementLink(StatementNo, StatementFN)
	'end if
	
	if StatementLocated then
		
        PDFName = StatementFN
		'PDFName = StatementNo & "_st.PDF"
		
		Response.clear
		Response.Buffer = true
		
		
		'Response.AddHeader "Content-Length", strFileSize
		Response.Charset = "UTF-8"
		'Response.ContentType = ContentType
		
        if Right(StatementFN, 4) = "docx" then
		    Response.ContentType = "application/word"
            Response.AddHeader "Content-Disposition", "attachment; filename=" & StatementNo & "_st.docx"
        else
            Response.ContentType = "application/pdf"
            Response.AddHeader "Content-Disposition", "attachment; filename=" & StatementNo & "_st.pdf"
        end if
					
		Set FileSystem = CreateObject("ADODB.Stream")

		FileSystem.Type = 1

		FileSystem.Open
		'jh FName

		FileSystem.LoadFromFile StatementFN
		
		Response.BinaryWrite (FileSystem.read)

		set FileSystem = nothing
		set DataStream = nothing		
	else
		jhError "NOT FOUND!"
	end if

elseif DownloadType = "PRICE_LIST" then	
	
	fn = request("fn")
	
	floc = glPriceListLoc  & "Band " & AcDetails.CostBand & "\"
	
	'jh "fn = " & fn
	'jh "floc = " & floc
	
	dFile = floc & fn
	
	set fs = server.createobject("Scripting.FileSystemObject")
	
	if fs.fileexists(dFile) then
	
		ThisContentType = getContentType(fn)
		
		'jhInfo "FOUND!"
		'jh "ThisContentType= "& ThisContentType
		
		if 1 = 1 then
		
			Response.clear
			Response.Buffer = true
			
			Response.AddHeader "Content-Disposition", "attachment; filename=" & fn
			'Response.AddHeader "Content-Length", strFileSize
			Response.Charset = "UTF-8"
			'Response.ContentType = ContentType
			
			Response.ContentType = ThisContentType
						
			Set FileSystem = CreateObject("ADODB.Stream")

			FileSystem.Type = 1

			FileSystem.Open
			'jh FName

			FileSystem.LoadFromFile dFile
			
			Response.BinaryWrite (FileSystem.read)

			set FileSystem = nothing
			set DataStream = nothing				
		
		end if
	end if

elseif DownloadType = "DOWNLOAD_FOLDER" then	

	fn = request("fn")
	
	floc =  getAccountDownloadFolder()
	
	'jh "fn = " & fn
	'jh "floc = " & floc
	
	dFile = floc & fn
	
	set fs = server.createobject("Scripting.FileSystemObject")
	
	if fs.fileexists(dFile) then
	
		ThisContentType = getContentType(fn)
		
		'jhInfo "FOUND!"
		'jh "ThisContentType= "& ThisContentType
		
		if 1 = 1 then
		
			Response.clear
			Response.Buffer = true
			
			Response.AddHeader "Content-Disposition", "attachment; filename=" & fn
			'Response.AddHeader "Content-Length", strFileSize
			Response.Charset = "UTF-8"
			'Response.ContentType = ContentType
			
			Response.ContentType = ThisContentType
						
			Set FileSystem = CreateObject("ADODB.Stream")

			FileSystem.Type = 1

			FileSystem.Open
			'jh FName

			FileSystem.LoadFromFile dFile
			
			Response.BinaryWrite (FileSystem.read)

			set FileSystem = nothing
			set DataStream = nothing				
		
		end if
	end if
		
end if

set acDetails = nothing
dbClose()
set fs = nothing

function getContentType(pfn)
	dim ThisExt
	if len(pfn) = 3 then 
		pfn = "." & pfn
	elseif len(pfn) < 3 then 
		getContentType = "application/octet-stream"	
		exit function
	else

	end if	
	
	ThisExt = lcase("" & right(pfn, 4))
	
	select case ThisExt
		case ".pdf"
			getContentType = "application/pdf"
		case ".xls", "xlsm", "xlsx"
			getContentType = "application/vnd.ms-excel"
		case "docx", ".doc"
			getContentType = "application/msword"
		case ".zip"
			getContentType = "application/zip"
		case ".jpg", "jpeg", ".gif", ".png"
			getContentType = "application/octet-stream"			
		case else
			getContentType = "application/octet-stream"							
	end select		
end function
%>