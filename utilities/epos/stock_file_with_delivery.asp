﻿
<!--#include virtual="/assets/includes/global_functions.asp" -->
<!--#include virtual="/admin/masterproductlist/sml_functions.asp" -->
<%



public FieldArr

jhAdmin "CHECK LOGGED IN?"

dbConnect()

ThisUserID = session("ENDURA_UserID")
AuthCodeRequest = request("ac")

FeedAuthenticated = false



if AuthCodeRequest = "CLIME433" then 
	FeedAuthenticated = true
	'FeedFn = ""
	'sql = "SELECT TOP 1 NEWID() AS GUID FROM DUAL "
	'jh  sql
	'set rs = db.execute(sql)
	'if not rs.eof then
	'	guid = mid("" & rs("GUID"), 2, 8)
	'else	
	'	guid = day(now)
	'end if
	'rsClose()
	
	
	
	'jh fn	
	'if request("x") = "1" then
	'	jhStop
	'end if
end if

if session("ENDURA_UserID") <> "" or session("ENDURA_AdminUserID") <> "" or FeedAuthenticated then
	AllowDownload = true
end if

if AllowDownload then

	FinalDelimiter = "|"
	
	DelimSetting = ucase("" & request("Delim"))
	if DelimSetting = "COMMA" then
		FinalDelimiter = ","
	elseif DelimSetting = "TAB" then
		FinalDelimiter = chr(9)
	end if

	ThisCostBand = session("ENDURA_CostBand")
	ThisLanguageID = session("SelLang")

	SetLanguage = request("lang")
	SetBand = request("band")
	SearchFor = request("l")

	if SetLanguage <> "" then
		sql = "SELECT LanguageID FROM END_Language WHERE LanguageISO = '" & validstr(SetLanguage) & "' AND SMLActive = 1 "
		'jhAdmin sql
		set rs = db.execute(sql)
		if not rs.eof then
			jhAdmin "FOUND LANGUAGE - " & SetLanguage
			ThisLanguageID = rs("LanguageID")
		end if
		rsClose()
	end if

	if session("ENDURA_AdminUserID") <> "" then
		ORCostBand = request("BAND")
		if ORCostBand <> "" and len(ORCostBand) = 1 then
			ThisCostBand = ucase(ORCostBand)
			jhAdmin "COST BAND OVERRIDDEN - " & ThisCostBand
		end if
	end if
	
	'jhAdmin ThisCostBand
	ThisCostBand = rnull(ThisCostBand)
	if ThisCostBand = "%" or ThisCostBand = "" then ThisCostBand = "A"	

	select case ThisCostBand

		case "C"
			TradeCol = 11
			RRPCol = 17
		case "D"
			TradeCol = 32
			RRPCol = 0		
		case "E"
			TradeCol = 12
			RRPCol = 18	
		case "F"
			TradeCol = 13
			RRPCol = 19
		case "G"
			TradeCol = 14
			RRPCol = 20
		case "H"
			TradeCol = 15
			RRPCol = 21		
		case else
			TradeCol = 10
			RRPCol = 16
	end select

	jhAdmin "ThisCostBand = " & ThisCostBand
	jhAdmin "ThisLanguageID = " & ThisLanguageID

	jhAdmin "TradeCol = " & TradeCol
	jhAdmin "RRPCol = " & RRPCol
		
	if ThisLanguageID = "0" then	
		NameCol = 1
		OneLinerCol = 2
		BulletCol = 3
	else
		NameCol = 27
		OneLinerCol = 28
		BulletCol = 29
		ColourNameCol = 30
		ProdTypeCol = 31
	end if
		
	jhAdmin "NameCol = " & NameCol
	jhAdmin "OneLinerCol = " & OneLinerCol	
	jhAdmin "BulletCol = " & BulletCol	

	jhAdmin "HERE"
	
	sql = "exec [spSML_StockFile] 0" & ThisLanguageID  & ", '" & SearchFor & "'"

	jhAdmin sql

	
	GetFieldNames = true
	x = getrs(sql, RecArr, rc)

	jhAdmin "rc = " & rc

	'rc = 250

	dbClose()

	set ls = new strconcatstream

	delim = "]["

	ImageRoot = "http://extranet.endura.co.uk/assets/SML/media/productimages/"

	'jh "rc = " & rc
	
	if rc >= 0 then
		'jhInfo "OUTPUT"
		'ID	STOCKCODE	DESCRIPTION	COST	RRP	BARCODE	BRAND	MANUFACTURERPARTNO	DIVISION	PRODUCTIMAGE	VARIANTIMAGE		ShortDescription	stVATCode

		'ls.add "ID" & delim & "STOCKCODE" & delim & "DESCRIPTION" & delim & "COST" & delim & "RRP" & delim & "BARCODE" & delim & "BRAND" & delim & "MANUFACTURERPARTNO" & delim & "DIVISION" & delim & "PRODUCTIMAGE" & delim & "VARIANTIMAGE" & delim & "PRODUCT NAME" & delim & "1 LINER" & delim & "VAT CODE" & delim & "BULLET POINTS" & delim & "PRODUCT CODE" & delim & "COLOUR" & delim & "SIZE" & delim & "PRODUCT TYPE"

		ls.add "STYLE NUMBER" & delim 
		ls.add "PRODUCT NAME" & delim
		ls.add "VENDOR SKU" & delim
		ls.add "UPC/EAN" & delim
		ls.add "UNIT COST" & delim
		ls.add "UNIT MSRP" & delim
		ls.add "COLOUR CODE" & delim
		ls.add "COLOUR" & delim
		ls.add "SIZE 1" & delim
		ls.add "SIZE 2" & delim
		ls.add "IMAGE" & delim
		ls.add "WEIGHT" & delim
		ls.add "SHORT DESCRIPTION" & delim
		ls.add "LONG DESCRIPTION" & delim
		ls.add "CATEGORY" & delim
		ls.add "BRAND" & delim
		ls.add "YEAR" & delim
		ls.add "SEASON" & delim
		ls.add "MANUFACTURER PART CODE" & delim
		ls.add "OTHER BARCODE" & delim
		ls.add "VAT" & delim
		ls.add "PACK QTY" & delim
		ls.add "STOCK COUNT" & delim
		ls.add "PRICE BAND 1" & delim
		ls.add "PRICE BAND 2" & delim
					
		ls.add vbcrlf
		
		LastProductCode = ""
		
		if request("x") = "1" then 
			StopOutput = true
			rc = 100
		end if
		
		'rc = 20
		
		for c = 0 to  rc
			ThisProductCode = RecArr(0,c)
			
			ThisSKU = RecArr(4,c)
			ThisEAN = ThisSKU
			
			ThisProductName = RecArr(1,c)
			ThisOneLiner = RecArr(2,c)
			ThisBullets = RecArr(3,c)		
			
			if replace(ThisBullets, "|", "") = "" then ThisBullets = ""
			
			ThisProductName_TRANS = "" & RecArr(NameCol,c)
			ThisOneLiner_TRANS = "" & RecArr(OneLinerCol,c)
			ThisBullets_TRANS = "" & RecArr(BulletCol,c)
			
			if replace(ThisBullets_TRANS, "|", "") = "" then ThisBullets_TRANS = ""
						
			'jhAdmin "ThisProductName_TRANS = " & ThisProductName_TRANS
			
			if ThisProductName_TRANS = "" and ThisProductName <> "" then ThisProductName_TRANS = ThisProductName
			if ThisOneLiner_TRANS = "" and ThisOneLiner <> "" then ThisOneLiner_TRANS = ThisOneLiner
			if ThisBullets_TRANS = "" and ThisBullets <> "" then ThisBullets_TRANS = ThisBullets
			
			ThisVariantCode = RecArr(8,c)	
			ThisStyleNumber = ThisVariantCode	
			
			if LastProductCode <> ThisProductCode then
				DefaultImageCode = ThisVariantCode
				LastProductCode = ThisProductCode
			end if		
			
			ProductImageURL = ImageRoot & ThisProductCode & "/" & DefaultImageCode & ".jpg"
			VariantImageURL = ImageRoot & ThisProductCode & "/" & ThisVariantCode & ".jpg"
			
			ThisProductImage = VariantImageURL
						
			if left(ThisBullets_TRANS,1) = "|" then ThisBullets_TRANS = right(ThisBullets_TRANS, len(ThisBullets_TRANS)-1)
			ThisBullets_TRANS = replace(ThisBullets_TRANS, "|", "::")
			
			'ProductImageURL = "@B"
			'VariantImageURL = "@B"
			
			ThisSize = RecArr(5,c)
			ThisBarCode = trim("" & RecArr(6,c))
			ThisVatCode = RecArr(7,c)
			if ThisVatCode = "S" then
				ThisVatRate = glVatRate
			else
				ThisVatRate = 0
			end if
			
			if TradeCol > 0 then Trade = "" & RecArr(TradeCol,c) else Trade = ""
			if RRPCol > 0 then RRP = "" & RecArr(RRPCol,c) else RRP = ""
			
			ProductType = RecArr(23,c)
			Brand = RecArr(24,c)
			Division = RecArr(25,c)
			
			ShowThis = true
			
			if RRP = "" or RRP = "0" or Trade = "" or Trade = "0" then
				ShowThis = false
				'jhAdmin "HIDE 1"
			end if			
			
			'jhAdmin "ThisSKU = " & ThisSKU
			'jhAdmin "Trade = " & Trade
			'jhAdmin "RRP = " & RRP
			
			if ThisCostBand = "D" then				
				RRP = ""
				if Trade = "" or Trade = "0" then 
					ShowThis = false
					'jhAdmin "HIDE 2"
				else					
					ShowThis = true					
				end if
			end if
			
			SKUDesc = ThisProductName_TRANS & ": " & ThisColorName & " - " & ThisSize
						
			if ThisLanguageID <> "0" then
				if "" & RecArr(ColourNameCol,c) <> "" then ThisColorName = RecArr(ColourNameCol,c)
				if "" & RecArr(ProdTypeCol,c) <> "" then ProductType = RecArr(ProdTypeCol,c)
			end if
			
			'jhAdmin "<img style=""width:40px;"" src=""" & ProductImageURL & """>"
			'jhAdmin "<img style=""width:40px;"" src=""" & VariantImageURL & """>"
			
			ThisColorCode = trim("" & RecArr(33,c))			
						
			ThisColorName = trim("" & RecArr(9,c))
			ThisColorName = stripNonNumeric(ThisColorName)
			
			if ThisColorCode = "" then 
				ThisColorCode = "Black"
				ThisColorName = "Black"
			end if
			
			ThisUnitWeight = trim("" & RecArr(34,c))
			
			ThisSize_2 = ""
			
			ThisBrand = "Endura"
			ThisYear = ""
			ThisSeason = ""
			ThisPackQty = trim("" & RecArr(35,c))
			if ThisPackQty = "" then ThisPackQty = 1
			
			ThisQtyInStock = trim("" & RecArr(26,c))
			
			ThisQtyInStock = getRoundedStockFigure(ThisQtyInStock)
			
			'jh "ThisQtyInStock = " & ThisQtyInStock
			
			Discount1 = ""
			Discount2 = ""
			
			ThisManufacturerPartCode = ""					
			ThisOtherBarcode = ""
			
			ThisCategories = trim("" & RecArr(36,c))
			
			ThisProductCategory = trim("" & RecArr(37,c))
			if ThisProductCategory = "" then ThisProductCategory = "Clothing"
			
			if ShowThis then
				
				'ls.add ThisProductCode
				ls.add ThisStyleNumber
				ls.add delim
				ls.add ThisProductName_TRANS
				ls.add delim	
				ls.add ThisSKU
				ls.add delim	
				ls.add ThisBarCode
				ls.add delim	
				ls.add Trade
				ls.add delim	
				ls.add RRP
				ls.add delim				
				ls.add ThisColorCode
				ls.add delim	
				ls.add ThisColorName
				ls.add delim		
				ls.add ThisSize
				ls.add delim	
				ls.add ThisSize_2 ' NOT USED
				ls.add delim			
				ls.add ThisProductImage	
				ls.add delim			
				ls.add ThisUnitWeight	

				ls.add delim			
				ls.add ThisOneLiner_TRANS	
				ls.add delim			
				ls.add ThisBullets_TRANS					
				ls.add delim			
				ls.add ThisProductCategory				
				ls.add delim			
				ls.add ThisBrand								
				ls.add delim

				ls.add ThisYear								
				ls.add delim
				ls.add ThisSeason								
				ls.add delim		
				ls.add ThisManufacturerPartCode								
				ls.add delim		
				ls.add ThisOtherBarcode								
				ls.add delim		
				
				ls.add ThisVatRate								
				ls.add delim	
				ls.add ThisPackQty							
				ls.add delim	
				
				ls.add ThisQtyInStock							
				ls.add delim	

				ls.add Discount1							
				ls.add delim		
				ls.add Discount2							
				ls.add delim					
							
				ls.add vbcrlf
				'ls.add "<hr color=white>"
				
			end if
		next
		
		StockOS = ls.value
		
		if FinalDelimiter = "," then
			StockOS = replace(StockOS, ",", ".")
		end if
		
		'StockOS = replace(StockOS, "|", "+")
		
		StockOS = replace(StockOS, delim, FinalDelimiter)
		
		if StopOutput then
			jhAdmin addbr(StockOS)	
			response.end
		end if
			
		'jhInfo "ENDS"
		
		StockOkay = true
		
	end if
end if

'response.end

EPOSFName = "EPOS_FILE.csv"


function CharDecoder(pText)
	
	CharDecoder = pText
	CharDecoder = replace(CharDecoder, "&#261;", "ą")
	CharDecoder = replace(CharDecoder, "&#322;", "ł")
	CharDecoder = replace(CharDecoder, "&#380;", "ż")
	CharDecoder = replace(CharDecoder, "&#378;", "ź")
	CharDecoder = replace(CharDecoder, "&#281;", "ę")
	CharDecoder = replace(CharDecoder, "&#347;", "ś")
	CharDecoder = replace(CharDecoder, "&#263;", "ć")
	CharDecoder = replace(CharDecoder, "&#357;", "ť")
	CharDecoder = replace(CharDecoder, "&#269;", "č")
	CharDecoder = replace(CharDecoder, "&#283;", "ě")
	CharDecoder = replace(CharDecoder, "&#345;", "ř")
	CharDecoder = replace(CharDecoder, "&#367;", "ů")
	CharDecoder = replace(CharDecoder, "&#941;", "έ")
	CharDecoder = replace(CharDecoder, "&#324;", "ń")
	CharDecoder = replace(CharDecoder, "&#317;", "Ľ")
	CharDecoder = replace(CharDecoder, "&#328;", "ň")
	CharDecoder = replace(CharDecoder, "&#314;", "ĺ")
	CharDecoder = replace(CharDecoder, "&#318;", "ľ")
	CharDecoder = replace(CharDecoder, "&#271;", "ď")
	CharDecoder = replace(CharDecoder, "&#341;", "ŕ")
	CharDecoder = replace(CharDecoder, "&#268;", "Č")
	CharDecoder = replace(CharDecoder, "&#321;", "Ł")
	CharDecoder = replace(CharDecoder, "&#379;", "Ż")
	CharDecoder = replace(CharDecoder, "&#280;", "Ę")
	CharDecoder = replace(CharDecoder, "&#356;", "Ť")
	CharDecoder = replace(CharDecoder, "&#616;", "ɨ")

end function

if StockOkay then

		'response.clear
		
		'Response.Buffer = true

		'Response.AddHeader "Content-Disposition", "attachment; filename=" & EPOSFName
		'Response.ContentType = "text/csv"			
		'Response.Charset = "UTF-16"		
		'Response.AddHeader "Content-Length", strFileSize
		
		'Response.ContentType = ContentType
	
		'StockOS = CharDecoder(StockOS)

		'response.write StockOS	
		
		RootDir = server.mappath("/") & "\reports\epos\DOWNLOADS\"
		if FeedAuthenticated then
			FName = AuthCodeRequest & ".csv"
		elseif session("ENDURA_AdminUSerID") <> "" then
			FName = session("ENDURA_AdminUSerID") & "_ADMIN_" & ThisCostBand & ".csv"
		elseif ThisUserID <> "" then
			FName = "STOCK_" & ThisUserID & "_" & ThisCostBand & ".csv"		
		end if
		
		'jh "FName = "  & FName
		
		if FName <> "" then
			DownloadFN = RootDir & FName
		
			FileCreated = ExportBE(DownloadFN, StockOS)
			
			jh "WRITE FILE?"
			
			if FileCreated then
				dbClose()
				
				response.redirect "/reports/epos/DOWNLOADS/" & FName
			end if
			
		end if
		
else
	jhError "COULD NOT RETRIEVE STOCK DATA - CHECK LOGGED IN STATUS!"
end if


function ExportBE(byval pName, byval pContents)

	const adTypeBinary = 1
	const adSaveCreateOverwrite = 2
	const adModeReadWrite = 3

	'pContents = HTMLDecode(pContents)
	
	'jh pName
	'jhAdmin pContents
	
	if 1 = 1 then
		DecodedContents = CharDecoder(pContents)
	end if	
	
	Set objStream = server.CreateObject("ADODB.Stream")
	objStream.Open 
	objStream.CharSet = "UTF-8"
	objStream.WriteText(DecodedContents)
	
	objStream.SaveToFile pName , adSaveCreateOverWrite
	objStream.Close
	set objStream = nothing
	
	ExportBE = true

end function
%>

