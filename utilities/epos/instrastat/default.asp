﻿
<!--#include virtual="/assets/includes/global_functions.asp" -->

<%

set AcDetails = new DealerAccountDetails

public FieldArr

jhAdmin "CHECK LOGGED IN?"

dbConnect()

if isDealer() then
	AllowDownload = true
end if

if AllowDownload then

	jhAdmin "HERE"
	
	sql = "exec [spSML_EPOSFile_INSTRASTAT] 0"

	jhAdmin sql

	GetFieldNames = true
	x = getrs(sql, RecArr, rc)

	jhAdmin "rc = " & rc

	sql = "exec spSML_EPOSFile_INSTRASTAT_COMPOSITION 0"
	jhAdmin sql
	
	'jhstop
	
	GetFieldNames = true
	x = getrs(sql, CompArr, CompC)
	
	dbClose()

	set ls = new strconcatstream
	set compls = new strconcatstream	

	delim = "]["
	
	if rc >= 0 then
		
		ls.add "SKU" & delim & "VARIANTCODE" & delim & "PRODUCTCODE" & delim & "NAME" & delim & "COLOR" & delim & "SIZE" & delim & "PACKAGED WEIGHT (g)" & delim & "UNPACKAGED WEIGHT (g)" & delim & "BARCODE" & delim & "COMMODITY CODE" & delim & "COUNTRY OF MANUFACTURE" & delim & "CONSTRUCTION" & delim
		
		for c = 1 to 10			
			
			ls.add "MATERIAL " & c & delim
			ls.add "% " & delim
		next

		ls.add vbcrlf
		
		LastSKU = ""
		
		for c = 0 to rc
		
			compls.clear
		
			ThisProduct = RecArr(0,c)
			if ThisProduct <> LastProduct then
				for y = 0 to CompC
					if CompArr(0,y) = ThisProduct then						
						compls.add CompArr(3,y)						
						compls.add delim					
						compls.add CompArr(1,y)
						compls.add delim

					end if
				next
				LastProduct = ThisProduct
				
				CompStr = compls.value
			end if
			
			ThisSKU = RecArr(1,c)
			
			
			if ThisSKU <> LastSKU then
			
				for y = 1 to 12
					
					ls.add RecArr(y,c)
					ls.add delim
				next
				
				ls.add CompStr
				
				ls.add vbcrlf
				
				LastSKU = ThisSKU
				
			end if
			
		next
		
		eposOS = ls.value
		eposOS = replace(eposOS, ",", ".")
		eposOS = replace(eposOS, delim, ",")		
		
		
		dbClose() 		
			
		EPOSOkay = true
		
	end if
end if

if EPOSOkay then

	'response.write eposOS

'jhstop	
			
	FileLoc = getAccountDownloadFolder()
	'jh "FileLoc = " & FileLoc
	
	fName = "INTRASTAT.csv"
	
	saveFN = FileLoc & fName
	
	fnCreation = ExportBE(saveFN, eposOS)
	
	dbClose() 
	
	EPOSUrl = getAccountDownloadURL() & fName
	
	response.redirect EPOSUrl
	
else
	jhError "COULD NOT RETRIEVE EPOS DATA - CHECK LOGGED IN STATUS!"
end if



function CharDecoder(pText)
	
	CharDecoder = pText
	CharDecoder = replace(CharDecoder, "&#261;", "ą")
	CharDecoder = replace(CharDecoder, "&#322;", "ł")
	CharDecoder = replace(CharDecoder, "&#380;", "ż")
	CharDecoder = replace(CharDecoder, "&#378;", "ź")
	CharDecoder = replace(CharDecoder, "&#281;", "ę")
	CharDecoder = replace(CharDecoder, "&#347;", "ś")
	CharDecoder = replace(CharDecoder, "&#263;", "ć")
	CharDecoder = replace(CharDecoder, "&#357;", "ť")
	CharDecoder = replace(CharDecoder, "&#269;", "č")
	CharDecoder = replace(CharDecoder, "&#283;", "ě")
	CharDecoder = replace(CharDecoder, "&#345;", "ř")
	CharDecoder = replace(CharDecoder, "&#367;", "ů")
	CharDecoder = replace(CharDecoder, "&#941;", "έ")
	CharDecoder = replace(CharDecoder, "&#324;", "ń")
	CharDecoder = replace(CharDecoder, "&#317;", "Ľ")
	CharDecoder = replace(CharDecoder, "&#328;", "ň")
	CharDecoder = replace(CharDecoder, "&#314;", "ĺ")
	CharDecoder = replace(CharDecoder, "&#318;", "ľ")
	CharDecoder = replace(CharDecoder, "&#271;", "ď")
	CharDecoder = replace(CharDecoder, "&#341;", "ŕ")
	CharDecoder = replace(CharDecoder, "&#268;", "Č")
	CharDecoder = replace(CharDecoder, "&#321;", "Ł")
	CharDecoder = replace(CharDecoder, "&#379;", "Ż")
	CharDecoder = replace(CharDecoder, "&#280;", "Ę")
	CharDecoder = replace(CharDecoder, "&#356;", "Ť")
	CharDecoder = replace(CharDecoder, "&#616;", "ɨ")

end function
%>

